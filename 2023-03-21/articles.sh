#!/bin/sh

# Wrap the given files' contents in HTML articles.

set -e

unix_to_iso() {
    local ticks="$1"
    date --date @$ticks --utc +'%Y-%m-%dT%H:%M:%SZ'
}

xml_escape_text() {
    sed \
        -e 's & \&amp; g' \
        -e 's < \&lt; g' \
        -e 's > \&gt; g'
}

article() {
    local name="$1"
    local dir="$PATH_WITHIN_PROJECT"
    if [ "$dir" != . ]; then
        local show_dir="$dir/"
    fi
    local extra=''
    if [ -x "$name" ]; then
        extra="$extra"' data-executable="yes"'
    fi
    echo '<article name="'$name'">'
    echo '<details>'
    echo '<summary>'$show_dir$name'</summary>'
    if [ -f "$name" ]; then
        read born changed size << EOF
$(stat --format='%W %Z %s' "$name")
EOF
        if [ "$born" -gt 0 ]; then extra="$extra data-file-born='$(unix_to_iso $born)'"; fi
        extra="$extra data-file-changed='$(unix_to_iso $changed)'"
        extra="$extra data-file-size='$size'"
        # ASSUMES text file
        echo -n '<pre data-project-file="'$dir/$name'"'$extra'>'
        cat "$name" | xml_escape_text
        echo '</pre>'
    else
        echo "<p>‘$(echo $name | xml_escape_text)’ is not a file!<p>"
    fi
    echo '</details>'
    echo '</article>'
    echo
}

articles() {
    for file in "$@"; do
        article "$file"
    done
}

project_relative_dir() {
    realpath --relative-to="$PROJECT_ROOT" .
}

PATH_WITHIN_PROJECT="$(project_relative_dir)"
articles "$@"

exit 0
