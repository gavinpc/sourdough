#!/bin/bash
# Redirection in xslt_html requires bash

# Unpack an HTML file that was packed with the system.

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"

__xslt_html() {
    xsltproc --encoding utf-8 --html "$@" \
             2> >(sed '/\(Tag.*invalid\|Unexpected end tag\)/,+2d' >&2)
}

emit_unpack_xsl() {
    cat << '__EOF'
<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0'
               xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
               xmlns:ex='http://exslt.org/common'
               extension-element-prefixes="ex">
  <xsl:output method="text" omit-xml-declaration="yes" />
  <xsl:param name="target-dir" />
  <xsl:variable name="CR" select="'&#xA;'" />
  
  <xsl:template mode="unpack" match="text()|@*" />
  <xsl:template mode="unpack" match="*">
    <xsl:apply-templates select="node()|@*" mode="unpack" />
  </xsl:template>
  
  <xsl:template mode="unpack" match="*[@data-project-file]">
    <xsl:variable name="file" select="concat($target-dir, '/', @data-project-file)" />
    <xsl:message>
      <xsl:value-of disable-output-escaping="yes" select="concat(@data-project-file, $CR)" />
    </xsl:message>
    <xsl:variable name="text" select="." />
    <ex:document
      href="{$file}"
      method="text" 
      omit-xml-declaration="yes"
      encoding="utf-8">
      <xsl:value-of disable-output-escaping="yes" select="$text" />
    </ex:document>
    <!-- For empty files.  ex:document does nothing if the output is empty. -->
    <xsl:if test="$text = ''">
      <xsl:value-of
        disable-output-escaping="yes"
        select="concat('exists ', $file, $CR)" />
    </xsl:if>
    <xsl:if test="@data-executable='yes'">
      <xsl:value-of
        disable-output-escaping="yes"
        select="concat('executable ', $file, $CR)" />
    </xsl:if>
  </xsl:template>

  <xsl:template match="/">
    <xsl:apply-templates select="." mode="unpack" />
  </xsl:template>
</xsl:transform>
__EOF
}

unpack() {
    local source_file="$1"      # may be a URL (including file protocol)
    local target_dir="$2"

    if [ -z "$source_file" ]; then
        if [ ! -z "$SOURCE_NOTEBOOK" ]; then
            source_file="$SOURCE_NOTEBOOK"
        else
            source_file='notebook.html'
        fi
    fi
    
    if [ -z "$target_dir" ]; then
        target_dir=$(mktemp --directory --tmpdir=.. sourdough-XXXXXX)
    elif [ -d "$target_dir" ]; then
        >&2 echo "I won't use the pre-existing target directory ‘$target_dir’"
        return 1
    fi

    >&2 echo "Unpacking $source_file to ‘$target_dir’"
    mkdir -p "$target_dir"

    __xslt_html \
        --stringparam target-dir "$target_dir" \
        <(emit_unpack_xsl) "$source_file" \
        | while read line; do
        local directive="${line%% *}"
        local file="${line#* }"
        case "$directive" in
            exists) touch "$file" ;;
            executable) chmod +x "$file" ;;
            ctime) asdf ;;
            mtime) asdf ;;
            *) 
                >&2 echo "Unknown directive in line ‘$line’"
                return 1
        esac
    done
}

unpack "$@"

exit 0
