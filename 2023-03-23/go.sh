#!/bin/sh

set -e

unfinished_business() {
    tup parse 2>&1 >/dev/null # undocumented
    tup todo "$@" 2>&1 | grep -q 'will be executed'
}

tup_fixpoint() {
    while unfinished_business "$@"
    do
        tup
    done
}

tup_poll() {
    while true
    do
        tup_fixpoint "$@"
        sleep 2s
    done
}

# ASSUMES you're generating a .gitignore
# and, I suppose, that this is a git repo
# although ideally you could clean using .gitignore without an actual repo
tup_clean() {
    local target="${1:-.}"
    git clean "$target" -Xf
}

main() {
    case "$1" in
        clean) tup_clean "${@:2}" ;;
        fix*) tup_fixpoint "${@:2}" ;;
        mon*) tup_poll "${@:2}" ;;
        *) tup_fixpoint "${@:2}" ;;
    esac
}

main "$@"

exit 0
