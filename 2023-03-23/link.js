function get_download_unpack_link(source = document.location) {
  const ele = document.querySelector('[data-project-file="./unpack.sh"]');
  if (!ele) throw new Error(`Couldn't find unpack script!`);
  const unpack_script = ele.innerText;
  const binding = `SOURCE_NOTEBOOK='${source}';\n`;
  const unpack_bound = unpack_script.replace(/^#!.*?\n/, `$&\n${binding}`);
  const unpack_blob = new Blob([unpack_bound], { type: "text/plain" });
  const a = document.createElement("a");
  const url = window.URL.createObjectURL(unpack_blob);
  a.download = "unpack.sh";
  a.innerText = "download unpack.sh";
  a.href = url;
  return a;
}

function ensure_download_link() {
  function onload() {
    if (document.querySelector('a[download="unpack.sh"]')) return;
    const a = get_download_unpack_link();
    document.body.prepend(a);
    globalThis.removeEventListener("load", onload);
  }
  globalThis.addEventListener("load", onload);
}
