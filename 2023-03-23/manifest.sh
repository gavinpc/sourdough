#!/bin/sh

# Write an HTML fragment (list items) representing the given files.

set -e

unix_to_iso() {
    local ticks="$1"
    date --date @$ticks --utc +'%Y-%m-%dT%H:%M:%SZ'
}

xml_escape_text() {
    sed \
        -e 's & \&amp; g' \
        -e 's < \&lt; g' \
        -e 's > \&gt; g'
}

xml_escape_attribute() {
    xml_escape_text | sed \
        -e "s ' \&apos; g" \
        -e 's " \&quot; g'
}

project_relative_dir() {
    realpath --relative-to="$PROJECT_ROOT" .
}

PATH_WITHIN_PROJECT="$(project_relative_dir)"

entry() {
    local name="${1#./}"
    local dir="$PATH_WITHIN_PROJECT"
    local extra=''
    local path="$dir/$name"
    if [ "$dir" = '.' ]; then
        path="$name"
    fi
    if [ -x "$name" ]; then
        extra="$extra"' data-executable="yes"'
    fi
    if [ -f "$name" ]; then
        read born changed size << EOF
$(stat --format='%W %Z %s' "$name")
EOF
        if [ "$born" -gt 0 ]; then extra="$extra data-file-born='$(unix_to_iso $born)'"; fi
        if [ "$changed" -gt 0 ]; then extra="$extra data-file-changed='$(unix_to_iso $changed)'"; fi
        extra="$extra data-file-size='$size'"
    fi
    echo -n '<li resource="'$(echo "$path" | xml_escape_attribute)'"'$extra'>'
    echo -n "$path"
    echo '</li>'
}

manifest() {
    for file in "$@"
    do
        entry "$file"
    done
}

manifest "$@"

exit 0
