tup.creategitignore()

tup.include('wiki-timeline.lua')

-- ========== Document bundling stuff ======================

-- This weird formulation lets this rule work when main-in.html exists and not
-- crash when it doesn't

tup.definerule{
  extra_inputs = {"xsl/index.xsl", "*main-in.html"},
  command = "if [ -f '%2i' ]; then xsltproc --html main.xsl '%2i' > '%o'; else touch '%o'; fi",
  outputs = {"main.html"},
}

tup.definerule{
  extra_inputs = {"main.html", "<bundles>"},
  command = "./pack.sh %<bundles> > '%o'",
  outputs = {"notebook.html"}
}
