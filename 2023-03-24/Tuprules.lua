ROOT = tup.getcwd()

function shell_quote(s)
  return "'"..s:gsub("'", "'\"'\"'").."'"
end

function debug_table(coll, label)
  if label then print(label) end
  print(string.format('%s items', #coll))
  for k, v in pairs(coll) do
    print(string.format("%s → %s", k, v))
  end
end

-- ASSUMES glob is compatible between Tup and find
--
-- Utility for creating file listings that should work all filenames.
--
-- This is needed in order for any rule to correctly read a list of filenames
-- where the names may contain spaces.
--
-- Tup's and find's ideas about what *.xyz means might diverge when one or more
-- *.xyz files are generated.
function list_matching(pattern, to_file)
  tup.definerule{
    inputs = {pattern},
    -- HACK: put %f in the command so that the rule depends on the list.
    -- Otherwise (I find) tup might not trigger this rule when new .html files are added.
    -- Using `*` instead of `.` avoids a redundant `./` starting each name.
    command = "^ index %d/"..pattern.."^ find * -maxdepth 1 -name '"..pattern.."' > %o # %f",
    outputs = {to_file}
  }

end

function compose_html()
  list_matching("*.html", "index.txt")
end
