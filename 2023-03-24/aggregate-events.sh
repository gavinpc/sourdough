#!/bin/sh

# Superficially combine all events from the listed files into one XML doc.

set -e

files="$1"                      # ASSUMES space-delimited list in first arg

echo '<?xml version="1.0" encoding="utf-8"?>'
echo '<events>'
for file in $files
do
    xsltproc just-the-events.xsl "$file"
done
echo '</events>'

exit 0
