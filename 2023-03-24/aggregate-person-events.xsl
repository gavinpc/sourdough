<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0'
               xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
               xmlns:str='http://exslt.org/strings'
               xmlns:ex='http://exslt.org/common'
               exclude-result-prefixes='ex str'
               >
  <xsl:output encoding="utf-8" indent="yes" />
  
  <xsl:key name="person" match="*[@who]" use="@who" />
  <xsl:key name="birth" match="birth" use="@who" />
  <xsl:key name="death" match="death" use="@who" />

  <xsl:template match="/">
    <xsl:variable name="all-data">
      <xsl:for-each select="str:split($files, ' ')">
        <xsl:copy-of select="document(.)" />
      </xsl:for-each>      
    </xsl:variable>
    <xsl:variable name="all" select="ex:node-set($all-data)" />
    <xsl:variable name="persons">
      <xsl:for-each select="$all/persons/*[generate-id() = generate-id(key('person', @who)[1])]">
        <xsl:variable name="birth" select="key('birth', @who)" />
        <xsl:variable name="death" select="key('death', @who)" />

        <xsl:if test="$birth and $death and $birth/@when &gt;= $death/@when">
          <xsl:message>WARN: Birthdate not before death date! <xsl:value-of select="concat(@who, ' ', $birth/@when, ' ', $death/@when)" /></xsl:message>
        </xsl:if>

        <xsl:choose>
          <xsl:when test="$birth or $death">
            <person who="{@who}">
              <xsl:if test="@summary != ''">
                <xsl:attribute name="summary">
                  <xsl:value-of select="@summary" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="$birth">
                <xsl:attribute name="born">
                  <xsl:value-of select="$birth/@when" />
                </xsl:attribute>
              </xsl:if>
              <xsl:if test="$death">
                <xsl:attribute name="died">
                  <xsl:value-of select="$death/@when" />
                </xsl:attribute>
              </xsl:if>
            </person>
          </xsl:when>
          <xsl:otherwise>
            <xsl:message>No birth or death for <xsl:value-of select="@who" /></xsl:message>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
    </xsl:variable>
    <persons>
      <xsl:for-each select="ex:node-set($persons)/*">
        <xsl:sort select="concat(@born, ' ', @died)" />
        <xsl:copy-of select="." />
      </xsl:for-each>
    </persons>
  </xsl:template>
</xsl:transform>
