// THESE lousy tests never hit the case where put needs awaited
async function test_channel() {
  const { make_ring_buffer, make_channel } = IDB;
  {
    const buffer = make_ring_buffer(0);
    const channel = make_channel(buffer);
    assert(!channel.can_put(), `can never put to zero-length buffer!`);
  }
  {
    const buffer = make_ring_buffer(1);
    const channel = make_channel(buffer);
    assert(channel.can_put());
    await channel.put("hello");
    const taken = channel.take();
    assert_cheap_equals(taken, "hello");
  }
  {
    const buffer = make_ring_buffer(3);
    const channel = make_channel(buffer);
    channel.put("curly");
    channel.put("larry");
    channel.put("moe");
    assert_cheap_equals(await channel.take(), "curly");
    assert_cheap_equals(await channel.take(), "larry");
    assert_cheap_equals(await channel.take(), "moe");
  }
  {
    const buffer = make_ring_buffer(5);
    const channel = make_channel(buffer);
    assert(channel.can_put());
    assert(!channel.can_take());
    const take_5 = Promise.all([
      channel.take(),
      channel.take(),
      channel.take(),
      channel.take(),
      channel.take(),
    ]);
    assert(channel.can_put());
    assert(!channel.can_take());
    for (const v of "aeiou") await channel.put(v);
    assert(channel.can_put()); // b/c there were already takers
    assert(!channel.can_take()); // "
    assert_cheap_equals(await take_5, ["a", "e", "i", "o", "u"]);
    assert(channel.can_put());
    assert(!channel.can_take());
  }
  console.info("Channel tests complete");
}
