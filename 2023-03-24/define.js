(function def_init() {
  globalThis[Symbol("https://def.codes")] = def_init;
  if (globalThis.def) {
    console.log("Pssst, conflict def");
    return;
  }

  const { create, assign, freeze, defineProperty, entries, hasOwn } = Object;
  const { construct, get } = Reflect;
  const { toPrimitive, toStringTag } = Symbol;
  const Symbol_for = Symbol.for;

  // name is an IRI (percent encoded, no space)
  // does not currently sanitize names, but the mint wrapper does
  const metaconstructor_tagged = metaclass => name => {
    const constructor = {
      [name]: function (...args) {
        return construct(metaclass, args, constructor);
      },
    }[name];
    const prototype = create(metaclass.prototype);
    defineProperty(constructor, "prototype", { value: prototype });
    defineProperty(prototype, "constructor", { value: constructor });
    defineProperty(prototype, toStringTag, { value: name });
    return constructor;
  };

  function coiner(fun) {
    const target = create(null);
    const prototype = new Proxy(target, {
      get(target, key, receiver) {
        if (typeof key === "string") {
          if (!hasOwn(target, key)) {
            const value = fun(key);
            defineProperty(target, key, { value, enumerable: true });
            return value;
          }
        }
        // system reserves the right to intercept symbols
        return get(target, key, receiver);
      },
    });
    return create(prototype);
  }
  coiner.comment = `Make an object where every (string) property is answered by the same function, which is given the requested key.  Once read, properties are remembered as readonly values.  The returned object thus has an “index signature” described by the function.`;

  function memoize1(fun, name = fun.name, cache = new Map()) {
    return {
      [name]: function (k) {
        if (cache.has(k)) return cache.get(k);
        const result = fun(k);
        cache.set(k, result);
        return result;
      },
    }[name];
  }
  memoize1.comment = `A no-eviction cache for synchronous unary functions.`;

  // does NOT recognize prefixes!  anything before colon is treated as scheme
  class Name extends URL {} // MUTABLE!!
  const name = s => new Name(s);

  const mint = {
    comment: `Some experimental tools`,
    in(ns) {
      name(ns); // throws if the name is not acceptable
      return {
        names: coiner(k => `${ns}${k}`),
        urls: coiner(k => name(`${ns}${k}`)),
        symbols: coiner(k => Symbol_for(`${ns}${k}`)),
        tuples: coiner(memoize1(k => TupleKind(`${ns}${k}`, k))),
        records: coiner(memoize1(k => RecordKind(`${ns}${k}`, k))),
        coiners: coiner(memoize1(k => coiner(`${ns}${k}`, k))),
        subclasses: superclass =>
          coiner(memoize1(metaconstructor_tagged(superclass))),
      };
    },
  };

  const errors = mint.in("https://def.codes/ns/errors/").subclasses(Error);
  // HTTP equiv 300, 400, 404, 409, 501
  // TODO: `prototype.name` property should be this local name
  // see also rdfjs
  const { MultipleChoices, BadRequest, NotFound, Conflict, NotImplemented } =
    errors;

  // described by, etc
  // https://www.rfc-editor.org/rfc/rfc9110.html#name-400-bad-request
  // once again, MDN does it right:
  // https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/100
  // hence
  // x∈HttpStatusCode → x describedBy `https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/${x}`
  function extend(o, ps) {
    for (const [k, value] of entries(ps))
      defineProperty(o, k, { value, enumerable: true });
    return o;
  }

  class Record {
    constructor(initial) {
      if (initial) assign(this, initial); // for read-only on just these, use extend
    }
  }
  class Tuple extends Array {
    constructor(...args) {
      // Unlike `Array` constructor, args are *always* elements, never `length`.
      super();
      this.push(...args);
    }
    // WIP
    toString(...args) {
      return `def.TupleKind("${name}")(${this.map(function (x) {
        return x.toString(...args);
      }).join(",")})`;
    }
    // Prevent crash on `map`, `filter`, etc due to object being frozen
    // But see warning at https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/@@species
    // also freeze is gone and will be opt-in
    /*
    static get [Symbol.species]() {
      return Array;
    }
    */
  }

  const RecordKind = metaconstructor_tagged(Record);
  const TupleKind = metaconstructor_tagged(Tuple);

  const AMD = "https://github.com/amdjs/amdjs-api/";
  // def.prefixes.amd = AMD

  const RELATIVE_PATH = /^\.\.?(?:$|\/)/;
  const is_relative_path = path => RELATIVE_PATH.test(path);
  is_relative_path.seeAlso = name(`${AMD}blob/master/AMD.md#module-id-format-`);
  is_relative_path.comment = `Answer whether the path is relative, according to the AMD definition of module id's.`;

  function combine_url_paths(path, from) {
    if (!is_relative_path(path)) return path;
    const url1 = new URL(`http://duty/${from}`);
    const url2 = new URL(`http://duty${url1.pathname}/../${path}`);
    return url2.pathname.replace(/^\//, "");
  }
  combine_url_paths.seeAlso = name(
    `${AMD}blob/master/AMD.md#module-id-format-`
  );
  combine_url_paths.comment = `Answer the qualified version of ‘path’ against ‘from’, using standard URL resolution`;

  const amd = { quote: mint.in(`${AMD}wiki/AMD#`).tuples };

  // the synchronous define/require doesn't speak unless spoken to
  function make_world(ns, defines = new Map()) {
    const goals = []; // public... should be a buffer with some limit
    const completed = []; // also let history roll off
    const cache = new Map();
    const defined = x => defines.has(x);

    const is_special = need => need === "exports" || need === "module";

    const requiring = [];
    function require_with_check(id) {
      if (requiring.includes(id))
        throw Error(`cycle! ${[...requiring, id].join(" → ")}`);
      requiring.push(id);
      try {
        return require(id);
      } finally {
        requiring.pop();
      }
    }
    function amd_construct(needs, factory, stack) {
      const exports = {};
      const module = { exports };
      // TODO: needed for AMD compatibility
      // but could also use to track dependencies dynamically
      //const require = make_contextualized_require(context);
      const special = { module, exports, require };
      const imports = needs.map(id => special[id] ?? require_with_check(id));
      // > If the factory argument is an object, that object should be assigned as
      // > the exported value of the module.
      // https://github.com/amdjs/amdjs-api/blob/master/AMD.md#factory-
      // Factory may write to `exports`; it must run even if result is about to be discarded
      const result =
        typeof factory === "function" ? factory(...imports) : factory;
      return needs.some(is_special) ? exports : result;
    }

    const dlq = new Set();

    // discharge results where needs can now be met
    // called after a host value is added to the cache for some name.
    // could be more surgical knowing what name has been added
    function revisit_goals() {
      for (let i = goals.length - 1; i !== -1; i--) {
        const goal = goals[i];
        if (dlq.has(goal)) continue;
        if (goal instanceof amd.quote.require) {
          const [needs, formula] = goal;
          if (needs.every(defined)) {
            try {
              require(needs, formula);
              goals.splice(i, 1);
            } catch (error) {
              dlq.add(goal);
              console.error(goal, error);
            }
          }
        }
      }
    }

    function define_or_throw_conflict(name, term) {
      // RequireJS does not throw for a redefinition, but also doesn't redefine
      if (defines.has(name)) throw Conflict(`Already defined: ${name}`);
      defines.set(name, amd.quote.define(...term)); // only place this is done
      revisit_goals();
    }

    function define(...term) {
      switch (term.length) {
        case 1:
          // PROVISIONAL: define₁ returns a normalized name
          return name(term[0]); // + base path & prefixes...
        case 2: {
          const [name, formula] = term;
          if (typeof name !== "string") {
            // until there's a loader
            throw NotImplemented("anonymous define");
          }
          // If formula is not a function, retain the host value eagerly (w/o require)
          if (typeof formula !== "function") {
            define_or_throw_conflict(name, term);
            return;
          }
          return define(name, [], formula);
        }
        case 3: {
          const [name, _needs, formula] = term;
          if (typeof formula !== "function")
            throw BadRequest(`define₃ expects f'n formula`);
          // lazy: remember the form to evaluate later (even if no needs)
          define_or_throw_conflict(name, term);
          break;
        }
        default:
          throw BadRequest(`no such arity ${term.length}`);
      }
    }

    function evaluate_require(term) {
      const [needs, formula] = term;
      const result = formula(...needs.map(require1));
      completed.push(amd.quote.require(...term));
      return result;
    }

    function require1(it) {
      if (isArray(it)) return it.map(require1); // distribute over array
      if (typeof it !== "string") throw BadRequest("expected string or array");
      if (cache.has(it)) return cache.get(it);
      if (!defines.has(it)) throw NotFound(it);

      const define_term = defines.get(it);
      let result;
      if (define_term.length === 2) {
        [, result] = define_term;
      } else if (define_term.length === 3) {
        const [, needs, formula] = define_term;
        result = amd_construct(needs, formula);
      }
      // shouldn't this also go under completed? if this was the top-level require
      cache.set(it, result); // only place this is done
      return result;
    }

    const { isArray } = Array;
    function require(...term) {
      switch (term.length) {
        case 1:
          return require1(term[0]);
        case 2:
          return evaluate_require(term);
        case 3: {
          try {
            return evaluate_require(term);
          } catch (error) {
            const [needs, formula, errback] = term;
            const goal = amd.quote.require(needs, formula);
            // this can be resolved by later definitions
            if (error instanceof NotFound) {
              goals.push(goal);
              return;
            }
            if (typeof errback === "function") {
              console.warn(needs, formula, error);
              errback(error);
            } else {
              throw error;
            }
          }
          break;
        }
        default:
          throw BadRequest(`no such arity ${term.length}`);
      }
    }

    const { get } = Reflect;
    return {
      define: new Proxy(extend(define, { amd, definitions: defines }), {
        get(target, key, receiver) {
          if (hasOwn(target, key)) return get(target, key, receiver);
          // undefined behavior
          return { "marry, what's a": key };
        },
      }),
      // goals & completed, and cache should be read-only views
      require: new Proxy(
        extend(require, {
          goals,
          completed,
          cache,
          resolve: combine_url_paths,
          async: make_require_async({ require }),
        }),
        {
          get(target, key, receiver) {
            if (hasOwn(target, key)) return get(target, key, receiver);
            // undefined behavior
            return { "I should like to find you a": key };
          },
        }
      ),
    };
  }

  function make_require_async({ require }) {
    const { isArray } = Array;
    const { assert } = console;
    function require_async(...term) {
      try {
        return require(...term);
      } catch {}
      switch (term.length) {
        case 1:
          return new Promise(resolve => {
            if (isArray(term[0])) {
              require(term[0], (...imports) => resolve(imports));
            } else {
              require(term, import_ => resolve(import_));
            }
          });
        // Just so `require` and `require.async` have the same semantics...
        case 2:
          return new Promise((resolve, reject) => {
            const [needs, callback] = term;
            assert(isArray(needs), `require async expected array needs`);
            require(needs, (...imports) =>
              resolve(callback(...imports)), reject);
          });
        case 3:
          return new Promise(resolve => {
            const [needs, callback, errback] = term;
            assert(isArray(needs), `require async expected array needs`);
            require(needs, (...imports) =>
              resolve(callback(...imports)), error => errback(error));
          });
      }
      throw BadRequest(`no such arity ${term.length}`);
    }
    require_async.comment =
      "an async extension of `require` that returns promises that resolves after all needs were met";
    return require_async;
  }

  const { define, require } = make_world();

  // AMD terms -> JS interpretation
  function write_define_JS(term) {
    switch (term.length) {
      case 2: {
        const [id, value] = term;
        // HACK: we should `write` value here
        return `define(${JSON.stringify(id)}, ${JSON.stringify(value)});`;
      }
      case 3: {
        const [id, needs, formula] = term;
        return `define(${JSON.stringify(id)}, ${JSON.stringify(
          needs
        )}, ${formula});`;
      }
    }
  }
  function write_require_JS([needs, formula]) {
    return `def.require(${JSON.stringify(needs)}, ${formula});`;
  }

  function io_javascript_write(term) {
    if (term instanceof def.amd.quote.define) return write_define_JS(term);
    if (term instanceof def.amd.quote.require) return write_require_JS(term);
    throw NotImplemented(`JS i/o for ${term}`);
  }
  io_javascript_write.comment = `Answer with JavaScript code that represents the given term`;

  const io = { js: { write: io_javascript_write } };

  function prompt_download(file, data) {
    const a = document.createElement("a");
    a.style.display = "none";
    a.download = file;
    a.href = data;
    a.target = "_blank";
    document.body.appendChild(a);
    a.click();
    a.remove();
  }
  prompt_download.seeAlso = `https://gist.github.com/cosmospham/7330466`;
  prompt_download.comment = `Offer to download a file called ‘file’ containing the given data URL.`;

  // The file is going to be saved to the user's downloads folder (whatever that is)
  // we here have no control over that
  // In previous testing I recall I was using a file monitor to move these into another place
  function prompt_download_document(file = `uspace.html`) {
    // special preprocess: needed to persist changes to textarea content
    for (const textarea of document.querySelectorAll("textarea")) {
      try {
        textarea.replaceChildren(document.createTextNode(textarea.value));
      } catch (error) {
        console.warn("Couldn't persist textarea content", error);
      }
    }

    let url;
    const type = "text/html";
    // XMLSerializer incorrectly encodes script (and probably style) content
    const data0 = new XMLSerializer().serializeToString(document);
    const data = document.documentElement.outerHTML;
    const blob = new Blob([data], { type });
    try {
      url = window.URL.createObjectURL(blob);
      prompt_download(file, url);
    } finally {
      URL.revokeObjectURL(url);
    }
  }
  prompt_download_document.comment = `Offer the user a download of the current document.  Currently browser-only.`;

  function prompt_download_def(file = "def-reconstituted.js") {
    let url;
    const type = "application/javascript";
    const all_terms = [
      `(${def.init})();`,
      // HACK: will throw Conflict because def_init defines it
      ...Array.from(define.definitions.values())
        .filter(([name]) => name !== "def")
        .map(term => io_javascript_write(term)),
      // shouldn't this include goals?
      ...Array.from(require.completed.values(), io_javascript_write),
    ];
    const data = all_terms.join("\n\n");
    const blob = new Blob([data], { type });
    try {
      url = window.URL.createObjectURL(blob);
      prompt_download(file, url);
    } finally {
      URL.revokeObjectURL(url);
    }
  }
  prompt_download_document.comment = `Offer the user a download of the current document.  Currently browser-only.`;

  const def = define;
  const globals = { define, require, def };

  const userland = {
    prompt_download,
    prompt_download_document,
    prompt_download_def,
  };

  // no, this won't roundtrip
  // nested define cannot be a thing, if you want I/O
  // define("def:map", () => map);

  Object.assign(def, globals, {
    TupleKind, // needed so their toString can emit runnable JS
    errors,
    init: def_init,
    io,
    userland,
    make_world,
    mint,
    // PROVISIONAL: so you can use in method constraints
    Record,
    Tuple,
  });
  define("def", () => def);
  extend(globalThis, globals);
  // everything after this should use define
})();
