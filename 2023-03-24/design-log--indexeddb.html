<article>
  <meta charset="utf-8" />
  <h1>
    <time datetime="2023-05-28T16:05:42Z"></time>design log for indexedDB
    catalog
  </h1>
  <section>
    <h2><time datetime="2023-05-28T16:12:07Z"></time>background</h2>
    <p>
      indexedDB is nobody's favorite API. Here are some findings from my
      experiences attempting to create an indexedDB abstraction with the
      following high-level goals:
    </p>
    <ul>
      <li>
        Provide basic read &amp; write operations without exposing the
        underlying API. By <q>basic operations</q> I mean the ability to get
        data into and out of tables with specific indexes.
      </li>
      <li>
        Use an existing, composable process abstraction that supports a cost
        model. By <q>supports a cost model</q> I mean you never just hand over
        the reins to a thing with no way of throttling or stopping it.
        Translation: use async iterables.
      </li>
    </ul>
    <p>
      At the moment, what I propose is a <q>catalog</q> interface that has
      knowledge of a desired database configuration <strong>and</strong> how to
      at least initialize the data. At the moment I'm mainly concerned about
      read-only data, so an initial write is sufficient.
    </p>
  </section>
  <section>
    <h2><time datetime="2023-05-28T16:09:08Z"></time>contention</h2>
    <blockquote>
      <p>
        The <code>blocked</code> handler is executed when an open connection to
        a database is blocking a <code>versionchange</code> transaction on the
        same database.
      </p>
      <cite
        ><a
          href="https:developer.mozilla.org/en-US/docs/Web/API/IDBOpenDBRequest/blocked_event"
          >MDN: IDBOpenDBRequest: blocked event</a
        ></cite
      >
    </blockquote>
    <p>
      When we get `blocked` while attempting to open a connection, there are two
      possibilities:
    </p>
    <ul>
      <li>we can do something about it</li>
      <li>we can't do anything about it</li>
    </ul>
    <p>
      When can we do anything about it, what can we do about it, and how can we
      tell the difference?
    </p>
    <p>
      <time datetime="2023-05-28T16:17:11Z"></time>Given that this is a
      <q>triple-A</q> database (Anyone can talk to it at Any time for Any
      reason, etc), when could we ever hope to do anything about it? We might
      know nothing about these blocking parties.
    </p>
    <p>
      Of course, we always have the option to wait around and keep retrying
      until we're not blocked.
    </p>
    <aside>
      <p>
        <time datetime="2023-05-28T16:22:01Z"></time>Actually hang on, could we
        use the <code>versionchange</code> event to get notified of this, rather
        than resorting to arbitrary retries?
      </p>
    </aside>
    <p>
      <time datetime="2023-05-28T16:35:00Z"></time
      ><a href="https://javascript.info/indexeddb#parallel-update-problem"
        >The Modern JavaScript Tutorial</a
      >
      makes the... contention that
      <q>these update collisions happen rarely.</q> Its code illustrating
      <code>onblocked</code> says
    </p>
    <blockquote>
      <pre>
  // it means that there's another open connection to the same database
  // and it wasn't closed after db.onversionchange triggered for it
      </pre>
      <cite
        ><a href="https://javascript.info/indexeddb#parallel-update-problem"
          >§ Parallel update problem</a
        ></cite
      >
    </blockquote>
    <p>
      <time datetime="2023-05-28T16:40:40Z"></time>The thing is, this is not
      just about understanding the spec. What happens is clear enough (barely).
      The question is what should you <em>do</em> about it, and what should you
      expect others to be doing?
    </p>
    <p>
      More generally, how should we treat connections? I started pooling
      connections because there was a noticeable delay when I didn't. So I've
      been operating with the assumption that it's expensive to open new
      connections. Yet it seems that the only way to even begin to avoid this
      kind of problem is for each operation to manage its own connections.
    </p>
  </section>
  <section>
    <h2>
      <time datetime="2023-05-29T03:29:01Z"></time>quotes from users about
      indexedDB API
    </h2>
    <blockquote>
      <p>I'm confused by how complex that code can be.</p>
      <p>...</p>
      <p>
        I guess part of my confusion is due to the fact that a transaction is
        made up of a number of separate async processes and I don't control how
        they are grouped into one transaction event. I'd have to use promises or
        Promise.all or async functions or generators to do something similar in
        the rest of the code. And it's taxing my little brain to feel confident
        that I won't miss some type of error that occurs late.
      </p>
      <cite
        ><a href="https://stackoverflow.com/q/50843803"
          >proper code structure of versionchange transaction in onupgradeneeded
          event</a
        ></cite
      >
    </blockquote>
    <p>
      <time datetime="2023-05-29T03:36:24Z"></time>The answer to that question
      is indeed clarifying.
    </p>
  </section>
  <section>
    <h2>
      <time datetime="2023-05-29T14:56:00Z"></time>appendix: indexedDB's API
      sucks
    </h2>
    <p>
      As early as <time datetime="2013">2013</time> the W3 had gotten copious
      feedback that people found the indexedDB API hard to program against, and
      they
      <a href="https://lists.w3.org/Archives/Public/www-tag/2013Feb/0003.html"
        >aired this finding publicly</a
      >. Marcos Caceres concludes his review by asking
    </p>
    <blockquote>
      <p>
        is it a goal or failure to have to rely on other's [sic] to build JS
        libraries on top of a standardised API to make it useful? It seems that
        it was the goal of IndexDB that it would not be developer friendly:
      </p>
      <p>
        "The goal of IndexedDB was to allow multiple different query processors
        to be built in JS. We expected few people to use it directly."
        <a href="https://twitter.com/adrianba/status/236249951049502720"
          >https://twitter.com/adrianba/status/236249951049502720</a
        >
      </p>
    </blockquote>
    <p>
      <time datetime="2023-05-29T15:02:26Z"></time>I find most of the cited
      comments to be childish and unspecific, though I'm particularly sensitive
      right now to the observation that only people
      <q>with unlimited time</q> could use this.
    </p>
    <p>
      The closest thing to a substantive critique is that, even in supposedly
      exemplary indexedDB code,
      <q
        >The key facts of the program are obscured under layers of callbacks and
        come-froms.</q
      >
      (The example code can be found on the
      <a
        href="https://web.archive.org/web/20130109104047/http://www.html5rocks.com/en/tutorials/indexeddb/todo/"
        >Wayback Machine</a
      >, though the comments are gone.)
    </p>
    <p>
      This comment gets at the design problem. What are the <q>key facts</q> of
      a program that uses indexedDB? If we accept that this is a low-level API
      intended for library developers (the W3
      <q>expected few people to use it directly</q>), then we wouldn't expect to
      see indexedDB code mixed with application code, i.e. domain code. So the
      <q>key facts</q>
      about such programs must be, how to maintain their own invariants on top
      of the API.
    </p>
    <p>
      For me, what has proven elusive in this effort is
      <em>flow of control</em>, particularly when database upgrades are
      involved—which they must be before you can do anything. And the way that
      this always manifests is in connection management. I am surprised to find
      that <time datetime="2023-05-29T16:18:46Z">as of this writing</time> the
      following question has only 3 votes:
    </p>
    <blockquote>
      <p>
        I'm trying to understand how to work properly with IndexedDb and one
        thing I can't understand is how are we supposed to manage the
        connection.
      </p>
      <p>
        Can someone please explain to me the best practice when working with
        IndexedDb ?
      </p>
      <cite
        ><a href="https://stackoverflow.com/q/18009596"
          >How to manage connexion with IndexedDb in Single Page Application</a
        ></cite
      >
    </blockquote>
    <p>
      The question is answered by Kristof Degrave, whom you will find answering
      many such questions. It appears that he became an expert on indexedDB
      after attempting to write unit tests against his database code and ended
      up publishing
      <a href="https://github.com/kristofdegrave/indexedDBmock/"
        >his own mock library for indexedDB testing</a
      >. I hope I am not in process of suffering the same fate.
    </p>
    <p>But witness:</p>
    <figure>
      <figcaption>create a store with no usage</figcaption>
      <script style="display: block; white-space: pre">
        {
          const figure = document.currentScript.closest("figure");
          const output = figure.appendChild(document.createElement("ol"));
          output.insertAdjacentHTML("afterbegin", "<p>Results:</p>");
          const wrap = (...things) => {
            const ele = document.createElement("li");
            ele.append(...things);
            return ele;
          };
          const say = (...x) => output.append(wrap(...x));
          async function init_database_for(who, { name, version }, upgrade) {
            return new Promise((resolve, reject) => {
              const request = indexedDB.open(name);
              request.onupgradeneeded = e => {
                say(`${who} upgrade`);
                const db = request.result;
                upgrade?.(db);
                resolve(db);
              };
              request.onblocked = e => {
                say(`${who} blocked`);
                reject("blocked");
              };
              request.onsuccess = e => {
                say(`${who} success`);
                resolve(request.result);
              };
              request.onerror = e => {
                say(`${who} error`);
                reject("error");
              };
            });
          }
          async function main() {
            const name = `idb-tests-${Math.round(Math.random() * 1e10)}`;
            const alice = await init_database_for("alice", { name }, db => {
              db.createObjectStore("alice");
            });
          }
          main();
        }
      </script>
    </figure>
    <p>
      <time datetime="2023-05-29T16:19:11Z"></time>In this example, we create a
      database and a store. So far so good.
    </p>
    <p>
      Presumably, this is preparing the database for usage by any clients who
      want to use it. This code cannot hope to be in charge of any such clients.
      And those clients may also have upgrades of their own, in which case it
      will need all existing connections to be closed.
    </p>
    <p>Fine, we'll just close the connection.</p>
    <p>
      I'll save you the trouble of adding a <code>db.close()</code> in the
      <code>upgradeneeded</code> handler, as this will fire an error against the
      connection request. The Chromium team implementing the spec found this to
      be an <q>oddity</q> and reported this to
      <a href="mailto:public-webapps@w3.org">public-webapps@w3.org</a>:
    </p>
    <blockquote>
      <p>
        It does seem to make sense to produce an error from a .open() call
        unless the database was successfully opened. I don't feel strongly
        though and don't recall why we defined it this way.
      </p>
      <cite
        ><a
          href="https://lists.w3.org/Archives/Public/public-webapps/2013JanMar/0873.html"
          >Re: [IndexedDB] Closing connection in a versionchange transaction</a
        >
        on <time datetime="2013-03-16">Sat, 16 Mar 2013</time></cite
      >
    </blockquote>
    <p>
      <time datetime="2023-05-29T16:45:32Z"></time>So what do we do instead? Can
      we close the connection in the <code>success</code> handler? No. That
      gives the same error as above, presumably for the same reason.
    </p>
    <p>
      Fine. We'll wait until all the handlers have completed before attempting
      to close the database. Surely we can do <em>that</em>, right? No.
    </p>
    <figure>
      <figcaption>close connection right after creating a database</figcaption>
      <script style="display: block; white-space: pre">
        {
          const figure = document.currentScript.closest("figure");
          const output = figure.appendChild(document.createElement("ol"));
          output.insertAdjacentHTML("afterbegin", "<p>Results:</p>");
          const wrap = (...things) => {
            const ele = document.createElement("li");
            ele.append(...things);
            return ele;
          };
          const say = (...x) => output.append(wrap(...x));
          async function init_database_for(who, { name, version }, upgrade) {
            return new Promise((resolve, reject) => {
              const request = indexedDB.open(name);
              request.onupgradeneeded = e => {
                say(`${who} upgrade`);
                const db = request.result;
                upgrade?.(db);
                // resolve(db);
              };
              request.onblocked = e => {
                say(`${who} blocked`);
                reject("blocked");
              };
              request.onsuccess = e => {
                say(`${who} success`);
                resolve(request.result);
              };
              request.onerror = e => {
                say(`${who} error`);
                reject("error");
              };
            });
          }
          async function main() {
            const name = `idb-tests-${Math.round(Math.random() * 1e10)}`;
            const alice = await init_database_for("alice", { name }, db => {
              db.createObjectStore("alice");
            });
            alice.close();
          }
          main();
        }
      </script>
    </figure>
    <p>
      <time datetime="2023-05-29T16:50:47Z"></time>That's right, this
      <em>still</em> fires an error at the connection.
    </p>
    <p>
      <time datetime="2023-05-29T16:52:09Z"></time>Sigh. I found the bug. I
      shouldn't <code>resolve</code> the promise from
      <code>upgradeneeded</code>.
    </p>
  </section>
  <section>
    <h2>
      <time datetime="2023-05-29T17:43:22Z"></time>transactions' transience
    </h2>
    <blockquote>
      <p>
        This can easily be solved by starting requests in the same epoch of the
        javascript event loop (the same tick) instead of deferring the start of
        a request.
      </p>
      <cite
        ><a href="https://stackoverflow.com/a/50858601"
          >Answer to
          <q
            >TransactionInactiveError: Failed to execute 'get' on
            'IDBObjectStore': The transaction is inactive or finished</q
          ></a
        ></cite
      >
    </blockquote>
    <p>
      It's been a long time since I've been willing to call anything in
      computers <q>easy</q>.
    </p>
    <p>
      But the point remains: you <em>always</em> risk this error whenever you
      yield control while a transaction has no requests active.
    </p>
    <p>What follows from this?</p>
    <p>
      It follows that the owner of a transaction must also know what it's going
      to be used for.
    </p>
    <p>
      So... how are you supposed to encapsulate any of this stuff?<time
        datetime="2023-05-29T17:48:15Z"
      ></time>
    </p>
    <p>
      <time datetime="2023-05-29T17:59:34Z"></time>Like, does it ever make sense
      to return a <code>IDBObjectStore</code> from a function? How did you come
      by that store, and how are you managing the transaction?
    </p>
    <p>I think I need to start over. Here's what I see:</p>
    <ul>
      <li>
        there are three levels of things that need to be completely managed:
        they are, ni norder, connections, transactions, and requests. You do
        <strong>not</strong> want higher-level code worried about (or meddling
        in) this.
      </li>
      <li>
        it <em>should</em> be possible to make a well-behaved set of managed
        things, but that well-behaved thing can always be trounced by some other
        things, which it must simply consider acts of God. Specifically, if you
        are blocked for some reason outside of your well-defined world, there is
        <strong>nothing you can do about it</strong> except fail as gracefully
        as you can, because you have no knowledge of why the situation has
        occurred.<time datetime="2023-05-29T18:09:04Z"></time>
      </li>
    </ul>
    <p>
      <time datetime="2023-05-29T18:17:13Z"></time>I really don't want to be
      this heavy-handed, though. The problem is clearly worst while
      initializing. Once the database is configured and populated, my
      <code>read</code> method has been working fine.<time
        datetime="2023-05-29T18:18:31Z"
      ></time>
    </p>
  </section>
</article>
