// see also https://developer.mozilla.org/en-US/docs/Web/API/TreeWalker
const get_dom_stepper = (() => {
  function preorder_next(/** @type Node */ my) {
    if (!my) return;
    if (my.firstChild) return my.firstChild;
    do if (my.nextSibling) return my.nextSibling;
    while ((my = my.parentNode));
  }
  function preorder_previous(/** @type Node */ my) {
    if (!my) return;
    if (my.lastChild) return my.lastChild;
    do if (my.previousSibling) return my.previousSibling;
    while ((my = my.parentElement));
  }
  function element_preorder_next(/** @type Node */ my) {
    if (!my) return;
    if (my.firstElementChild) return my.firstElementChild;
    do if (my.nextElementSibling) return my.nextElementSibling;
    while ((my = my.parentElement));
  }
  function element_preorder_previous(/** @type Node */ my) {
    if (!my) return;
    if (my.lastElementChild) return my.lastElementChild;
    do if (my.previousElementSibling) return my.previousElementSibling;
    while ((my = my.parentElement));
  }
  // i must have needed these for something...
  /*
    function element_shallow_next(my) {
      if (!my) return;
      do if (my.nextElementSibling) return my.nextElementSibling;
      while ((my = my.parentNode));
    }
    function element_shallow_previous(my) {
      if (!my) return;
      do if (my.previousElementSibling) return my.previousElementSibling;
      while ((my = my.parentNode));
    }
*/
  return function get_reader(spec) {
    const element = spec?.element ?? false;
    const dir = spec?.dir ?? "next";
    if (element) {
      if (dir === "next") return element_preorder_next;
      if (dir === "prev") return element_preorder_previous;
    }
    if (dir === "next") return preorder_next;
    if (dir === "prev") return preorder_previous;
    console.assert(false, `get reader got bad dir ${dir}`);
  };
})();

// extensions
{
  const base_next = get_dom_stepper({ element: true, dir: "next" });
  const base_prev = get_dom_stepper({ element: true, dir: "prev" });

  // yeah this is just map + filter
  function dom_next_matching(ele, where) {
    while ((ele = base_next(ele))) if (ele.matches(where)) return ele;
  }
  function dom_previous_matching(ele, where) {
    while ((ele = base_prev(ele))) if (ele.matches(where)) return ele;
  }
}

function* walk(step, init) {
  let current = init;
  while (current !== void 0) {
    yield current;
    current = step(current);
  }
}

const NODE_AXIS = get_dom_stepper();
function* visit_text_nodes_in(element) {
  for (const node of walk(NODE_AXIS, element)) {
    if (node.nodeType === Node.TEXT_NODE) {
      yield node;
    }
  }
}
