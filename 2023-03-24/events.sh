#!/bin/sh

# Extract the `event` records from the indicated Wikipedia article
#
# ADAPTED from persons.sh

set -e

file="$1"
xref="$2"

name="${file##*/}"
title="${name%.html}"

# >&2 echo name = $name
# >&2 echo title = $title

# https://www.pcre.org/original/doc/html/pcrepattern.html#SEC5
# --fixed-strings doesn't quite do it because we must only match at start of line
set +e
line="$(grep -P --max-count 1 "^\Q$title\E " "$xref")"
set -e
date="${line##* }"

# >&2 echo line = $line
# >&2 echo date = $date

if [ -z "$date" ]
then
    >&2 echo "No date for $file ($title)"
    echo "<events/>"            # prevent error trying to open empty document
    exit 0                      # ignore, it's not a timeline article
fi

xsltproc --html --stringparam date "$date" events.xsl "$file"

exit 0
