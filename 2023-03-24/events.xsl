<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
  <xsl:output indent="yes" encoding="utf-8" />
  <xsl:include href="./wiki-timeline-extract-events.xsl" />
  <xsl:param name="date" />

  <xsl:template match="/">
    <events>
      <xsl:apply-templates mode="extract-events" />
    </events>
  </xsl:template>

</xsl:transform>
