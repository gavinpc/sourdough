<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0'
               xmlns:notebook="https://gavinpc.com/composition-notebook"
               xmlns:func="http://exslt.org/functions"
               xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
               xmlns:str="http://exslt.org/strings"
               exclude-result-prefixes="notebook str"
               extension-element-prefixes="func"
               >
  <xsl:output method="xml" omit-xml-declaration="yes" indent="yes" />

  <xsl:template match="/">
    <xsl:apply-templates select="(//*[@id='Ratified_amendments']/following::table)[1]" />
  </xsl:template>

  <xsl:template match="table">
    <us-constitution>
      <xsl:apply-templates mode="amendment" select="tbody/tr[td]" />
    </us-constitution>
  </xsl:template>

  <xsl:template mode="amendment" match="*">
    <xsl:variable name="proposed">
      <xsl:apply-templates mode="en-long-to-iso" select="td[3]" />
    </xsl:variable>
    <xsl:variable name="completed">
      <xsl:apply-templates mode="en-long-to-iso" select="td[4]" />
    </xsl:variable>
    <!-- I'd expect translating 0123456789 → 0123456789 would be safer... -->
    <amendment
      wiki="{substring-after(td[1]//a/@href, './')}"
      number="{number(translate(td[1]//@data-sort-value, ' !', ''))}"
      proposed="{$proposed}"
      completed="{$completed}"
      >
      <description>
      <xsl:apply-templates mode="wiki" select="td[2]/node()" />
      </description>
      <!-- ratification, etc -->
    </amendment>
  </xsl:template>

  <!-- very ad hoc, but sadly this table has no sorting on this col -->
  <xsl:template mode="en-long-to-iso" match="node()|@*">
    <xsl:variable name="year" select="substring-after(., ', ')" />
    <xsl:variable name="month-name" select="substring-before(., ' ')" />
    <xsl:variable name="day-year" select="substring-after(., ' ')" />
    <xsl:variable name="day" select="substring-before($day-year, ', ')" />
    <xsl:variable name="mm" select="notebook:en-month-name-mm($month-name)" />
    <xsl:variable name="dd" select="notebook:pad0(2, $day)" />
    <xsl:value-of select="concat($year, '-', $mm, '-', $dd)" />
  </xsl:template>

  <xsl:template mode="wiki" match="@id | @title" />
  <xsl:template mode="wiki" match="node()|@*">
    <xsl:copy>
      <xsl:apply-templates mode="wiki" select="node()|@*" />
    </xsl:copy>
  </xsl:template>
  <xsl:template mode="wiki" match="@rel[.='mw:WikiLink']" />
  <xsl:template mode="wiki" match="@href[starts-with(., './')]">
    <xsl:attribute name="wiki">
      <xsl:value-of select="substring-after(., './')" />
    </xsl:attribute>
  </xsl:template>

  <xsl:template mode="sort-to-iso4" match="node()|@*">
    <xsl:value-of select="substring-before(substring-after(., '00000000'), '-0000')" />
  </xsl:template>

  <func:function name="notebook:pad0">
    <xsl:param name="length" />
    <xsl:param name="value" />
    <xsl:variable name="zeros" select="str:padding($length, '0')" />
    <xsl:variable name="text" select="string($value)" />
    <func:result select="str:align($text, $zeros, 'right')" />
  </func:function>

  <func:function name="notebook:en-month-name-mm">
    <xsl:param name="name" select="." />
    <func:result>
      <xsl:choose>
        <xsl:when test="$name = 'January'">01</xsl:when>
        <xsl:when test="$name = 'February'">02</xsl:when>
        <xsl:when test="$name = 'March'">03</xsl:when>
        <xsl:when test="$name = 'April'">04</xsl:when>
        <xsl:when test="$name = 'May'">05</xsl:when>
        <xsl:when test="$name = 'June'">06</xsl:when>
        <xsl:when test="$name = 'July'">07</xsl:when>
        <xsl:when test="$name = 'August'">08</xsl:when>
        <xsl:when test="$name = 'September'">09</xsl:when>
        <xsl:when test="$name = 'October'">10</xsl:when>
        <xsl:when test="$name = 'November'">11</xsl:when>
        <xsl:when test="$name = 'December'">12</xsl:when>
      </xsl:choose>
    </func:result>
  </func:function>

</xsl:transform>
