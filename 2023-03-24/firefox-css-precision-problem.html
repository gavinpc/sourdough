<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="initial-scale=1" />
    <link rel="icon" href="data:," />
    <title>Firefox CSS precision problem</title>
  </head>
  <body>
    <main>
      <h1>
        <time datetime="2023-09-10T19:42:33Z"></time>Firefox CSS precision
        problem
      </h1>
      <p>
        This is a standalone document for demonstrating CSS precision problems
        in Firefox and testing mitigations.
      </p>
      <p>
        The tests in this document use timeline displays as an example, but the
        problem applies to any CSS custom properties.
      </p>
      <section>
        <h2>
          <time datetime="2023-09-10T19:44:44Z"></time>illustration of the
          problem
        </h2>
        <p>
          The problem can be illustrated using a simple timeline targeting
          arbitrary periods representable as dates.
        </p>
        <p>
          Here are some minimal style rules for positioning along an arbitrary
          line:
        </p>
        <style style="display: block; white-space: pre">
          [data-intent="timeline"] {
            block-size: 4em;
            --timeline-duration: calc(
              var(--timeline-to) - var(--timeline-from)
            );
          }
          [data-intent="timeline"] > * {
            --timeline-r: calc(
              (var(--timestamp) - var(--timeline-from)) /
                var(--timeline-duration)
            );
            position: absolute;
            left: calc(100% * var(--timeline-r));
          }
        </style>
        <p>
          On the timeline, we'll show pointer and keyboard activity from the
          current browser session.. Here are some styles for marking the items
          on the timeline
        </p>
        <style style="display: block; white-space: pre">
          [data-type]::before {
            content: attr(data-type);
            display: inline-block;
          }
          [data-type] {
            font-size: 0;
            background-color: var(--mark-color);
            color: initial;
            width: 1rem;
            height: 1rem;
            border-radius: 50%;
            overflow: hidden;
            transform: translateY(calc(0.66rem * var(--rank)));
          }
          [data-type="timer"] {
            --mark-color: #7777;
            --rank: 1;
          }
          [data-type="keydown"] {
            --mark-color: #0f07;
            --rank: 2;
          }
          [data-type="keyup"] {
            --mark-color: #f0f7;
            --rank: 3;
          }
          [data-type="mousemove"] {
            --mark-color: #00f7;
            --rank: 4;
          }
        </style>

        <p>
          <time datetime="2023-09-10T19:59:13Z"></time>We'll need to compare
          different methods, so let's make a routine to create a test figure.
        </p>
        <script style="display: block; white-space: pre">
          {
            function make_test_figure(spec) {
              const { label, normalize } = spec;
              const script = document.currentScript;
              console.assert(script);
              const element = document.createElement("figure");

              if (label) {
                const caption = document.createElement("figcaption");
                caption.append(label);
                element.append(caption);
              }

              const timeline = document.createElement("div");
              timeline.dataset.intent = "timeline";
              element.append(timeline);

              const now = new Date();
              const later = new Date();
              later.setUTCSeconds(later.getUTCSeconds() + 60);
              const from = now.getTime();
              const to = later.getTime();
              timeline.style.setProperty("--timeline-from", normalize(from));
              timeline.style.setProperty("--timeline-to", normalize(to));

              function sink(event) {
                // event objects have timestamps, but they don't map straight to wall time
                const now = new Date();
                const iso = now.toISOString();
                const ticks = now.getTime();
                const time = document.createElement("span");
                // time.dateTime = iso;
                time.innerText = iso;
                time.dataset.type = event.type;
                time.style.setProperty("--timestamp", normalize(ticks));
                timeline.appendChild(time);
              }

              const timer = setInterval(() => sink(new Event("timer")), 1000);
              document.addEventListener("mousemove", sink);
              document.addEventListener("keydown", sink);
              document.addEventListener("keyup", sink);
              function dispose() {
                clearInterval(timer);
                document.removeEventListener("mousemove", sink);
                document.removeEventListener("keydown", sink);
                document.removeEventListener("keyup", sink);
              }

              script.insertAdjacentElement("afterend", element);
              return { dispose };
            }
          }
        </script>
        <p>
          We'll start with the obvious thing. The <q>Unix</q> timestamp is
          unique integer representing the point in time, down to the millisecond
          level. Can we just use that?
        </p>
        <script style="display: block; white-space: pre">
          {
            const identity = x => x;
            make_test_figure({
              label: `no normalization (just ticks)`,
              normalize: identity,
            });
          }
        </script>
        <p>
          That works in Chrome with no visible quantization. But it doesn't work
          at all in Firefox at this scale.
        </p>
        <p>
          This example uses a normalization that reduces the overall magnitude
          by using 2022 instead of 1970 as the zero point.
        </p>
        <script style="display: block; white-space: pre">
          {
            const BASE_TICKS = new Date(2022, 1, 1).getTime();
            const normalize_2022 = t => t - BASE_TICKS;
            make_test_figure({
              label: `no normalization (just ticks)`,
              normalize: normalize_2022,
            });
          }
        </script>
        <p>
          This is unchanged in Chrome, but it's much improved in Firefox. You
          can still see quantization—on my system, about half of the one-second
          ticks are sharing a spot.
        </p>
        <p>
          We can further compress the range by dividing by 1000 after rebasing.
        </p>
        <script style="display: block; white-space: pre">
          {
            const BASE_TICKS = new Date(2022, 1, 1).getTime();
            const normalize_2022_1000 = t => (t - BASE_TICKS) / 1000;
            make_test_figure({
              label: `no normalization (just ticks)`,
              normalize: normalize_2022_1000,
            });
          }
        </script>
        <p>Maybe a slight difference, but no improvement.</p>
        <p>
          <time datetime="2023-09-10T20:50:35Z"></time>Where this is headed, I'm
          afraid, is that there is no normalization that will work across the
          representable range, that <q>fixing</q> it in one place will only
          break it in another.
        </p>
        <p>
          If so, that means you need to adjust the timeline's normalization to
          its content, which is quite unfortunate.
        </p>
        <script style="display: block; white-space: pre">
          {
            const BASE_TICKS = new Date().getTime();
            const normalize_now = t => (t - BASE_TICKS) / 1000;
            make_test_figure({
              label: `no normalization (just ticks)`,
              normalize: normalize_now,
            });
          }
        </script>
        <p>
          <time datetime="2023-09-12T03:30:05Z"></time>Yes, I find that anything
          less than <strong>now</strong> is giving artifacting in Firefox when
          looking at millisecond differences.<time
            datetime="2023-09-12T03:30:48Z"
          ></time>
        </p>
      </section>
    </main>
  </body>
</html>
