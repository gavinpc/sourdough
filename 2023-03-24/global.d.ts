// This is not a TypeScript project!
// But we can get editor assistance & checking in JS
// Hence also the tsconfig.json

// Eventually I may move these types into some scope, or I may not.

/** something with knowledge of indexeddb schema stuff */
declare interface DefCatalog {
  configure();
  read();
  write();
}

declare interface IDBIndexDescription
  extends Pick<IDBIndex, "keyPath">,
    Partial<Pick<IDBIndex, "unique" | "multiEntry">> {}

declare interface IDBStoreDescription
  extends Partial<Pick<IDBObjectStore, "autoIncrement" | "keyPath">> {
  readonly indexes?: Readonly<Record<string, IDBIndexDescription>>;
}

declare interface IDBRequestPromiseWrapper {
  <T>(request: IDBRequest<T>): Promise<T>;
}

// database deltas
declare type IDBKeyPath = string | readonly string[] | null;
declare type IDBStoreDelta =
  | { readonly type: "key"; readonly keyPath: IDBKeyPath }
  | {
      // i.e. has index
      readonly type: "createIndex";
      readonly name: string;
      readonly index: IDBIndexDescription;
    }
  // i.e. does *not* have index... is this an essential function?
  // nothing is going to break because you have an index that you don't need
  // or at least, if you have code that breaks due to the presence of an index that
  // you're not using... well
  //
  // aka drop index
  // delete X + create X = alter X
  | { readonly type: "deleteIndex"; readonly name: string };

// You can rename a store (without dropping/recreating data)
// You can rename an index (")

// absolute controller
// do we have a notion of "who's asking"?
declare interface KnowledgeableIDBState {
  // key is database name?  or asker?
  // also this should be an “observable” collection, right?
  readonly connections: Map<string, KnowledgeableConnection>;
}
declare interface KnowledgeableConnection {
  readonly transactions: Set<KnowledgeableTransaction>;
  close(): void;
}
declare interface KnowledgeableTransaction {
  readonly requests: Set<KnowledgeableRequest>;
}
