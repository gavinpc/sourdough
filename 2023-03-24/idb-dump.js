// I'm not 100% sure that the new version is better, but it's almost 2/3 shorter and more linear
{
  // STATES in which we might be observed, i.e. how we leave things on yield

  // initial state. waiting for a value from cursor
  //      receive eof -> eof
  //      when buffer not full: receive value -> pending read
  //      when buffer full: receive value -> pending write
  const PENDING_READ = Symbol("pending read");

  // waiting for a put to complete, i.e. buffer full
  //       write completed -> pending read
  const PENDING_WRITE = Symbol("pending write");

  // pick up where we left off with a new cursor
  //       receive value -> skip next
  const RESUME = Symbol("resume");

  // ignore the next incoming value and advance 1
  //       receive value -> pending read
  //       receive eof?
  const SKIP_NEXT = Symbol("skip next");

  // terminal state. reached end of data
  const EOF = Symbol("eof");

  // TRANSITIONS:
  const RECEIVE_VALUE = Symbol("receive value");
  const WRITE_COMPLETED = Symbol("write completed");
  const RECEIVE_EOF = Symbol("receive eof");

  function old_pipe_cursor_to(thunk, channel, is_index /* TEMP */) {
    const trace = false ? (...x) => console.debug(`|`, ...x) : null;

    // but where do we get source...
    // const is_index = source instanceof IDBIndex;

    function make_new_cursor_request() {
      const request = thunk();
      // WHEN does this ever happen?
      request.onerror = error => {
        console.error(`just curious, do we get to see`, error);
        throw error;
      };
      request.onsuccess = event => {
        const cursor = event.target.result;
        if (cursor) {
          step(RECEIVE_VALUE, cursor);
        } else {
          step(RECEIVE_EOF);
        }
      };
      return request;
    }

    {
      let _state = PENDING_READ; // see above
      let _frontier = null; // which key values (main & maybe index) were last put
      /** @type {null | Promise<void>} */
      let _pager = null; // a promise telling when last pending put completed

      // as for the arg... not sure how else to do this
      function step(transition, cursor) {
        assert?.(_state !== EOF, `activity in terminal state`);
        // trace?.(`in ${_state.toString()} got → ${transition.toString()}`);
        switch (transition) {
          case RECEIVE_EOF:
            _state = EOF;
            assert?.(!_pager, `EOF with pager in-flight?`);
            channel.put(DONE); // or close the channel if it had such
            break;

          case RECEIVE_VALUE: {
            assert?.(!_pager, `Receive value with pager in-flight?`);
            if (_state === RESUME) {
              assert?.(_frontier !== null, `Resume without frontier`);
              if (is_index) {
                cursor.continuePrimaryKey(_frontier.key, _frontier.pk);
              } else {
                cursor.continue(_frontier.key);
              }
              _state = SKIP_NEXT;
              return;
            }
            if (_state === SKIP_NEXT) {
              cursor.advance(1);
              _state = PENDING_READ;
              return;
            }

            const { value } = cursor; // HOTSPOT
            // assume the put will succeed
            _frontier = { key: cursor.key, pk: cursor.primaryKey }; // HOTSPOT
            if (channel.can_put_now()) {
              channel.put(value);
              // also done after put, but we don't expect an error here
              cursor.continue(); // HOTSPOT
              _state = PENDING_READ;
              return;
            }

            _pager = channel.put(value);
            _state = PENDING_WRITE;
            _pager.then(() => {
              _pager = null;
              step(WRITE_COMPLETED, cursor);
            });
            return;
          }

          case WRITE_COMPLETED: {
            assert?.(_state === PENDING_WRITE, `Should be pending write`);
            try {
              cursor.continue(); // HOTSPOT
              _state = PENDING_READ;
            } catch (error) {
              // this is the only place we really expect this error
              if (error.name === "TransactionInactiveError") {
                _state = RESUME;
                // but what do we do with this?
                make_new_cursor_request(channel);
              }
            }
            return;
          }
          default:
            assert?.(`Unknown transition`, transition);
        }
      }
    }
    make_new_cursor_request();
  }
}

const bugged_promise = () => {
  let resolve, reject;
  const promise = new Promise((res, rej) => {
    resolve = res;
    reject = rej;
  });
  return { promise, resolve, reject };
};

// you'd think this would be a thing but in practice no
/** @type {IDBRequestPromiseWrapper} */
const async_request = async request => {
  return new Promise((resolve, reject) => {
    request.onerror = _error => reject(request.error);
    request.onsuccess = _event => resolve(request.result);
  });
};

let _traceid = 0;
const trace_with = (yes, prefix, id = _traceid++) =>
  yes ? (...args) => console.debug(id, prefix, ...args) : null;

async function does_versionchange_work_within_a_tab_or_not() {
  {
    const random = Math.round(Math.random() * 1e12);
    const db_name = `test-versionchange-${random}`;
    let it_happened = false;

    const reader = await IDB.safe_connection(db_name, undefined, db => {
      db.createObjectStore("ariel");
    });
    reader.addEventListener("versionchange", e => {
      it_happened = true;
      reader.close();
      console.debug("hey it happened", e);
    });
    try {
      await new Promise((resolve, reject) => {
        const request = indexedDB.open(db_name, 2);
        request.onsuccess = () => resolve(request.result);
        request.onblocked = () => reject("blocked");
        request.onerror = error => reject(`${error}`);
        request.onupgradeneeded = () => {
          console.debug("blah blah 2");
        };
      });
    } catch (error) {
      console.log(error);
    }
    assert(it_happened, "gee it didn't happen");
  }

  // However, it does *not* happen if two try going to the same version at the same time?
  {
    const random = Math.round(Math.random() * 1e12);
    const db_name = `test-versionchange-${random}`;
    let it_happened = false;

    const reader = await IDB.safe_connection(db_name, 2, db => {
      db.createObjectStore("ariel");
    });
    reader.addEventListener("versionchange", e => {
      it_happened = true;
      reader.close();
      console.debug("hey it happened", e);
    });
    try {
      await new Promise((resolve, reject) => {
        const request = indexedDB.open(db_name, 2);
        request.onsuccess = () => resolve(request.result);
        request.onblocked = () => reject("blocked");
        request.onerror = error => reject(`${error}`);
        request.onupgradeneeded = () => {
          console.debug("blah blah 3");
        };
      });
    } catch (error) {
      console.log(error);
    }
    assert(it_happened, "gee it didn't happen this time");
  }

  console.info("versionchange tests done");
}

/**
 * @see https://stackoverflow.com/a/47007746
 * @param connection {IDBDatabase}
 * @returns {boolean | undefined}
 */
function is_open(db) {
  if (!(db instanceof IDBDatabase)) return false;
  const [store_name] = db.objectStoreNames;
  if (store_name === undefined) return undefined;
  try {
    db.transaction(store_name).abort();
    return true;
  } catch (_) {
    return false;
  }
}

async function test_is_open() {
  const random = Math.round(Math.random() * 1e12);
  const db_name = `test-is-open-${random}`;
  {
    const db = await new Promise(resolve => {
      const request = indexedDB.open(db_name);
      request.onsuccess = () => {
        resolve(request.result);
        // i suspect this will fail though...
        assert(IDB.is_open(request.result), `Expected open during onsuccess?`);
      };
      // you need at least one store for is_open to work smh
      request.onupgradeneeded = () => {
        const db = request.result;
        db.createObjectStore("any");
        // is_open will fail from here because you can't start a transaction
        // during versionchange.
      };
    });
    await sleep(1);
    assert(IDB.is_open(db), `Expected open after yield`);
    db.close();
    assert(!IDB.is_open(db), `Expected not open after explicit close call!`);
    await sleep(1);
    assert(!IDB.is_open(db), `Expected still not open after yield!`);
    console.info(`is_open tests complete`);
  }
}

async function test_async_queue_1() {
  const gated = IDB.async_queue({ max_concurrent: 1 });

  async function demand_attention(why, how_long = 500) {
    console.log(`I want your attention for ${how_long}ms because ${why}`);
    await sleep(how_long);
    if (why === "climate") throw new Error("sorry bud you're out of luck");
    console.log(`Okay, I'm finished with your attention because ${why}`);
  }

  const reasons = [
    "reasons",
    "no good reason",
    "climate",
    "economy",
    "nuclear",
  ];
  for (const reason of reasons)
    gated(() =>
      demand_attention(reason).catch(error => {
        console.log(`I failed to hold your attention for ${reason}`, error);
      })
    );
}

async function test_async_queue_2() {
  const random = Math.round(Math.random() * 1e12);
  const db_name = `test-async-queue-${random}`;
  const things = [
    [undefined, db => db.createObjectStore("batman")],
    [2, db => db.createObjectStore("robin")],
    [4, db => db.createObjectStore("joker")],
  ];
  for (const [version, upgrade] of things) {
    const conn = IDB.safe_connection(db_name, version, upgrade).catch(error => {
      console.warn(`I failed to open at version ${version}`, error);
    });
  }
}

async function test_diff() {
  {
    const diffs = [...diff_any()];
    assert(diffs.length === 0, `both undefined`);
  }
  {
    const diffs = [...diff_any(null)];
    assert(diffs.length === 1, `both undefined`);
    assert(diffs[0].type === "typeof", `null vs undefined`);
  }
  {
    const diffs = [...diff_any([], [])];
    assert(diffs.length === 0, `empty arrays`);
  }
  {
    const diffs = [...diff_any([], [{ a: 1 }])];
    assert(diffs.length === 1, `empty array vs 1 ele`, diffs);
    assert(diffs[0].type === "key#", diffs);
  }
}

// re https://bugzilla.mozilla.org/show_bug.cgi?id=1458237
async function is_there_a_null_value_in_store() {
  for (const store of ["en_wiki_events", "en_wiki_persons"]) {
    console.debug(`Checking ${store} for nulls...`);
    for await (const result of IDB.read({ db: "cronwall", store })) {
      if (result == null) debugger;
    }
  }
  console.debug(`I guess not?`);
}
