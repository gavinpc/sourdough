(async function () {
  // answering some questions about indexedDB process

  // creates the db
  if (false) {
    const name = `idb-tests-${Math.round(Math.random() * 1e10)}`;
    const alice = indexedDB.open(name);
  }

  // creates the db
  if (false) {
    const name = `idb-tests-${Math.round(Math.random() * 1e10)}`;
    const alice = indexedDB.open(name);
    const bob = indexedDB.open(name);
  }

  // creates the db
  // alice upgrade 0 -> 1
  // alice success
  // bob success
  if (false) {
    const name = `idb-tests-${Math.round(Math.random() * 1e10)}`;
    const alice = indexedDB.open(name);
    const bob = indexedDB.open(name);
    alice.onupgradeneeded = e => console.debug("alice upgrade", e);
    alice.onblocked = e => console.debug("alice blocked", e);
    alice.onsuccess = e => console.debug("alice success", e);
    alice.onerror = e => console.error("alice error", e);
    bob.onupgradeneeded = e => console.debug("bob upgrade", e);
    bob.onblocked = e => console.debug("bob blocked", e);
    bob.onsuccess = e => console.debug("bob success", e);
    bob.onerror = e => console.error("bob error", e);
  }

  // creates db
  // creates table alicia
  // alice upgrade 0 -> 1
  // alice success
  // bob success
  if (false) {
    const name = `idb-tests-${Math.round(Math.random() * 1e10)}`;
    const alice = indexedDB.open(name);
    const bob = indexedDB.open(name);
    alice.onupgradeneeded = e => {
      console.debug("alice upgrade", e);
      alice.result.createObjectStore("alicia");
    };
    alice.onblocked = e => console.debug("alice blocked", e);
    alice.onsuccess = e => console.debug("alice success", e);
    alice.onerror = e => console.error("alice error", e);
    bob.onupgradeneeded = e => console.debug("bob upgrade", e);
    bob.onblocked = e => console.debug("bob blocked", e);
    bob.onsuccess = e => console.debug("bob success", e);
    bob.onerror = e => console.error("bob error", e);
  }

  // creates db
  // creates store alicia
  // does *not* create store roberto
  // alice upgrade 0 -> 1
  // alice success
  // bob success
  if (false) {
    const name = `idb-tests-${Math.round(Math.random() * 1e10)}`;
    const alice = indexedDB.open(name);
    const bob = indexedDB.open(name);
    alice.onupgradeneeded = e => {
      console.debug("alice upgrade", e);
      alice.result.createObjectStore("alicia");
    };
    alice.onblocked = e => console.debug("alice blocked", e);
    alice.onsuccess = e => console.debug("alice success", e);
    alice.onerror = e => console.error("alice error", e);
    bob.onupgradeneeded = e => {
      console.debug("bob upgrade", e);
      bob.result.createObjectStore("roberto");
    };
    bob.onblocked = e => console.debug("bob blocked", e);
    bob.onsuccess = e => console.debug("bob success", e);
    bob.onerror = e => console.error("bob error", e);
  }

  // creates db
  // creates store alicia
  // does *not* create store roberto
  // alice upgrade 0 -> 1
  // alice success
  // bob success
  // bob2 blocked
  if (false) {
    const name = `idb-tests-${Math.round(Math.random() * 1e10)}`;
    const alice = indexedDB.open(name);
    const bob = indexedDB.open(name);
    alice.onupgradeneeded = e => {
      console.debug("alice upgrade", e);
      alice.result.createObjectStore("alicia");
    };
    alice.onblocked = e => console.debug("alice blocked", e);
    alice.onsuccess = e => console.debug("alice success", e);
    alice.onerror = e => console.error("alice error", e);
    bob.onupgradeneeded = e => console.debug("bob upgrade", e);
    bob.onblocked = e => console.debug("bob blocked", e);
    bob.onsuccess = e => {
      console.debug("bob success", e);
      const { name, version } = bob.result;
      const req = indexedDB.open(name, version + 1);
      req.onblocked = e => console.debug("bob2 blocked", e);
      req.onsuccess = e => console.debug("bob2 success", e);
      req.onerror = e => console.error("bob2 error", e);
      req.onupgradeneeded = e => {
        console.debug("bob2 upgrade", e);
        req.result.createObjectStore("roberto");
      };
    };
    bob.onerror = e => console.error("bob error", e);
  }

  // creates db
  // creates store alicia
  // does *not* create store roberto
  // alice upgrade
  // alice error <-------- what's the error?
  // Here: https://lists.w3.org/Archives/Public/public-webapps/2013JanMar/0873.html
  //
  // > It does seem to make sense to produce an error from a .open() call unless
  // > the database was successfully opened. I don't feel strongly though and
  // > don't recall why we defined it this way.
  //
  // bob success
  // bob2 blocked
  if (false) {
    const name = `idb-tests-${Math.round(Math.random() * 1e10)}`;
    const alice = indexedDB.open(name);
    const bob = indexedDB.open(name);
    alice.onupgradeneeded = e => {
      console.debug("alice upgrade", e);
      alice.result.createObjectStore("alicia");
      alice.result.close();
    };
    alice.onblocked = e => console.debug("alice blocked", e);
    alice.onsuccess = e => console.debug("alice success", e);
    alice.onerror = e => console.error("alice error", e);
    bob.onupgradeneeded = e => console.debug("bob upgrade", e);
    bob.onblocked = e => console.debug("bob blocked", e);
    bob.onsuccess = e => {
      console.debug("bob success", e);
      const { name, version } = bob.result;
      const req = indexedDB.open(name, version + 1);
      req.onblocked = e => console.debug("bob2 blocked", e);
      req.onsuccess = e => console.debug("bob2 success", e);
      req.onerror = e => console.debug("bob2 error", e);
      req.onupgradeneeded = e => {
        console.debug("bob2 upgrade", e);
        req.result.createObjectStore("roberto");
      };
    };
    bob.onerror = e => console.debug("bob error", e);
  }

  // creates db
  // creates store alicia
  // does *not* create store roberto
  // alice upgrade
  // alice success
  // bob success
  // bob2 blocked
  if (false) {
    const name = `idb-tests-${Math.round(Math.random() * 1e10)}`;
    const alice = indexedDB.open(name);
    const bob = indexedDB.open(name);
    alice.onupgradeneeded = e => {
      console.debug("alice upgrade", e);
      alice.result.createObjectStore("alicia");
    };
    alice.onblocked = e => console.debug("alice blocked", e);
    alice.onsuccess = e => {
      console.debug("alice success", e);
      alice.result.close();
    };
    alice.onerror = e => console.debug("alice error", e);
    bob.onupgradeneeded = e => console.debug("bob upgrade", e);
    bob.onblocked = e => console.debug("bob blocked", e);
    bob.onsuccess = e => {
      console.debug("bob success", e);
      const { name, version } = bob.result;
      const req = indexedDB.open(name, version + 1);
      req.onblocked = e => console.debug("bob2 blocked", e);
      req.onsuccess = e => console.debug("bob2 success", e);
      req.onerror = e => console.debug("bob2 error", e);
      req.onupgradeneeded = e => {
        console.debug("bob2 upgrade", e);
        req.result.createObjectStore("roberto");
      };
    };
    bob.onerror = e => console.debug("bob error", e);
  }

  // creates db & alicia
  // alice upgrade
  // alice error
  // bob success
  if (false) {
    async function foo(who, name, upgrade) {
      return new Promise((resolve, reject) => {
        const request = indexedDB.open(name);
        request.onupgradeneeded = e => {
          console.debug(`${who} upgrade`);
          const db = request.result;
          upgrade?.(db);
          resolve(db);
        };
        request.onblocked = e => {
          console.debug(`${who} blocked`, e);
          reject("blocked");
        };
        request.onsuccess = e => {
          console.debug(`${who} success`, e);
          resolve(request.result);
        };
        request.onerror = e => {
          console.debug(`${who} error`, e);
          reject("error");
        };
      });
    }
    const name = `idb-tests-${Math.round(Math.random() * 1e10)}`;
    const alice = await foo("alice", name, db => {
      db.createObjectStore("alicia");
    });
    alice.close();
    const bob = await foo("bob", name, db => {
      if (!db.objectStoreNames.contains("roberto")) {
        return foo("bob2", name, db => {
          db.createObjectStore("roberto");
        });
      }
    });
  }

  // creates db & alicia but NOT roberto!!
  // alice upgrade
  // alice error <------------------what is the error and why??
  // bob success
  // bob2 success
  if (false) {
    async function foo(who, name, upgrade) {
      return new Promise((resolve, reject) => {
        const request = indexedDB.open(name);
        request.onupgradeneeded = e => {
          console.debug(`${who} upgrade`);
          const db = request.result;
          upgrade?.(db);
          resolve(db);
        };
        request.onblocked = e => {
          console.debug(`${who} blocked`, e);
          reject("blocked");
        };
        request.onsuccess = e => {
          console.debug(`${who} success`, e);
          resolve(request.result);
        };
        request.onerror = e => {
          console.debug(`${who} error`, e);
          reject("error");
        };
      });
    }
    const name = `idb-tests-${Math.round(Math.random() * 1e10)}`;
    const alice = await foo("alice", name, db => {
      db.createObjectStore("alicia");
    });
    alice.close();
    const bob = await foo("bob", name);
    if (!bob.objectStoreNames.contains("roberto")) {
      bob.close();
      foo("bob2", name, db => {
        db.createObjectStore("roberto");
      });
    }
  }

  // creates db & alicia but NOT roberto!!
  // alice upgrade
  // alice error <------------------what is the error and why??
  // bob success
  // bob2 success
  if (false) {
    async function foo(who, { name, version }, upgrade) {
      return new Promise((resolve, reject) => {
        const request = indexedDB.open(name);
        request.onupgradeneeded = e => {
          console.debug(`${who} upgrade`);
          const db = request.result;
          upgrade?.(db);
          resolve(db);
        };
        request.onblocked = e => {
          console.debug(`${who} blocked`, e);
          reject("blocked");
        };
        request.onsuccess = e => {
          console.debug(`${who} success`, e);
          resolve(request.result);
        };
        request.onerror = e => {
          console.debug(`${who} error`, e);
          reject("error");
        };
      });
    }
    const name = `idb-tests-${Math.round(Math.random() * 1e10)}`;
    const alice = await foo("alice", { name }, db => {
      db.createObjectStore("alicia");
    });
    alice.close();
    const bob = await foo("bob", { name });
    if (!bob.objectStoreNames.contains("roberto")) {
      bob.close();
      const { name, version } = bob;
      foo("bob2", { name, version: version + 1 }, db => {
        db.createObjectStore("roberto");
      });
    }
  }

  // creates db and alicia
  // alice upgrade
  // alice error
  if (false) {
    async function foo(who, { name, version }, upgrade) {
      return new Promise((resolve, reject) => {
        const request = indexedDB.open(name);
        request.onupgradeneeded = e => {
          console.debug(`${who} upgrade`);
          const db = request.result;
          console.log(`line 334`, db);
          upgrade?.(db);
          console.log(`line 336`);
          resolve(db);
          console.log(`line 338`);
        };
        request.onblocked = e => {
          console.debug(`${who} blocked`, e);
          reject("blocked");
        };
        request.onsuccess = e => {
          console.debug(`${who} success`, e);
          resolve(request.result);
        };
        request.onerror = e => {
          console.debug(`${who} error`, e);
          reject("error");
        };
      });
    }
    const name = `idb-tests-${Math.round(Math.random() * 1e10)}`;
    const alice = await foo("alice", { name }, db => {
      db.createObjectStore("alicia");
    });
    // THIS causes an error to be thrown on the open request.
    alice.close();
  }

  // resolves the above problem by not resolving from upgradeneeded
  if (false) {
    async function foo(who, { name, version }, upgrade) {
      return new Promise((resolve, reject) => {
        const request = indexedDB.open(name);
        request.onupgradeneeded = e => {
          console.debug(`${who} upgrade`);
          const db = request.result;
          upgrade?.(db);
          // do NOT resolve here, but only from onsuccess
        };
        request.onblocked = e => {
          console.debug(`${who} blocked`, e);
          reject("blocked");
        };
        request.onsuccess = e => {
          console.debug(`${who} success`, e);
          resolve(request.result);
        };
        request.onerror = e => {
          console.debug(`${who} error`, e);
          reject("error");
        };
      });
    }
    const name = `idb-tests-${Math.round(Math.random() * 1e10)}`;
    const alice = await foo("alice", { name }, db => {
      db.createObjectStore("alicia");
    });
    // THIS causes an error to be thrown on the open request.
    // when can
    alice.close();
  }

  // creates db and alicia and roberto
  // HOWEVER you don't see any of it in FF storage until connections are closed
  // alice upgrade
  // alice success
  // bob success
  // bob2 upgrade
  // bob2 success
  if (false) {
    async function foo(who, { name, version }, upgrade) {
      return new Promise((resolve, reject) => {
        const request = indexedDB.open(name, version);
        request.onupgradeneeded = e => {
          console.debug(`${who} upgrade`);
          upgrade?.(request.result);
        };
        request.onblocked = e => {
          console.debug(`${who} blocked`, e);
          reject("blocked");
        };
        request.onsuccess = e => {
          console.debug(`${who} success`, e);
          resolve(request.result);
        };
        request.onerror = e => {
          console.debug(`${who} error`, e);
          reject("error");
        };
      });
    }
    const name = `idb-tests-${Math.round(Math.random() * 1e10)}`;
    const alice = await foo("alice", { name }, db => {
      db.createObjectStore("alicia");
    });
    alice.close();
    const bob = await foo("bob", { name });
    if (!bob.objectStoreNames.contains("roberto")) {
      bob.close();
      const { name, version } = bob;
      foo("bob2", { name, version: version + 1 }, db => {
        db.createObjectStore("roberto");
      });
    }
  }

  // creates db and alicia and roberto
  // HOWEVER you don't see roberto in FF storage until connections are closed
  // alice upgrade
  // alice success
  // bob success
  // bob2 upgrade
  // bob2 success
  if (false) {
    async function foo(who, { name, version }, upgrade) {
      return new Promise((resolve, reject) => {
        const request = indexedDB.open(name, version);
        request.onupgradeneeded = e => {
          console.debug(`${who} upgrade`);
          upgrade?.(request.result);
        };
        request.onblocked = e => {
          console.debug(`${who} blocked`, e);
          reject("blocked");
        };
        request.onsuccess = e => {
          console.debug(`${who} success`, e);
          resolve(request.result);
        };
        request.onerror = e => {
          console.debug(`${who} error`, e);
          reject("error");
        };
      });
    }
    const name = `idb-tests-${Math.round(Math.random() * 1e10)}`;
    const alice = await foo("alice", { name }, db => {
      db.createObjectStore("alicia");
    });
    alice.close();
    const bob = await foo("bob", { name });
    if (!bob.objectStoreNames.contains("roberto")) {
      bob.close();
      const { name, version } = bob;
      const bob2 = await foo("bob2", { name, version: version + 1 }, db => {
        db.createObjectStore("roberto");
      });
      bob2.close();
    }
  }
})();
