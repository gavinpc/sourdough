async function test_actuator() {
  const wait = ms => new Promise(r => setTimeout(r, ms));
  async function* some_things() {
    yield "orange";
    await wait(100);
    yield "pear";
    yield "banana";
    yield "pineapple";
    await wait(200);
    yield "apple";
    await wait(300);
    yield "persimmon";
    yield "plum";
  }
  async function* async_log(seq, note = "") {
    for await (const item of seq) {
      console.log(note, item);
      yield item;
    }
  }
  const act = async_timed_actuator(1000);
  act(async_log(some_things()));
}

async function test_combination() {
  const wait = ms => new Promise(r => setTimeout(r, ms));
  async function* some_things() {
    yield "orange";
    await wait(100);
    yield "pear";
    yield "banana";
    yield "pineapple";
    await wait(200);
    yield "apple";
    await wait(300);
    yield "persimmon";
    yield "plum";
  }
  const input = some_things();
  const filtered = async_filter(input, s => s.startsWith("p"));
  const limited = async_take(filtered, 3);
  for await (const item of limited) console.debug("AAND", item);
}
