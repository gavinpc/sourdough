// this normalization makes positioning work in Firefox (Ubuntu), albeit with visible quantizing
const BASE_TICKS = new Date(2023, 0, 1).getTime();
// needed only for Firefox, else this could be identity
const css_ticks = t => (t - BASE_TICKS) / 1000;

function dialog_in_timeframe(spec) {
  const lang = "en";
  const TIMESTAMP_FORMAT = new Intl.DateTimeFormat(lang, {
    hour: "numeric",
    minute: "2-digit",
    second: "2-digit",
  });

  let current_turn;
  let current_timeout;

  const { container } = spec;
  const doc = document; // container may not have an owner

  const dialog = doc.createElement("section");
  const heading = doc.createElement("h2");
  const form = doc.createElement("form");
  const list = doc.createElement("ol");
  const submit = doc.createElement("input");

  dialog.classList.add("chat");
  dialog.dataset.timeframe = true;
  // if you don't have a submit button, Enter stops working after 1st time
  submit.type = "submit";
  submit.value = "send";
  form.prepend(submit);
  heading.textContent = spec.title ?? "Dialog:";
  dialog.append(heading, form);

  // this point marks the loading of the page, it doesn't change
  {
    const now = new Date();
    const ticks = now.getTime();
    dialog.style.setProperty("--timeframe-from-point", css_ticks(ticks));
  }

  function take_turn() {
    const turn = doc.createElement("div");
    const new_input = doc.createElement("input");
    const fieldset = doc.createElement("fieldset");
    const legend = doc.createElement("legend");
    const time = doc.createElement("time");

    const now = new Date();
    const ticks = now.getTime();

    turn.classList.add("turn");
    time.dateTime = now.toISOString();
    time.textContent = TIMESTAMP_FORMAT.format(now);
    turn.style.setProperty("--time", css_ticks(ticks));
    legend.append("Prompted @ ", time);
    fieldset.append(legend, new_input);
    form.append(turn);
    turn.append(fieldset);

    new_input.focus();
    current_turn = turn;
  }

  function say(what) {
    const output = doc.createElement("output");
    const saying = doc.createElement("p");
    saying.textContent = `${what}`;
    output.append(saying);
    current_turn.append(output);
    take_turn();
  }

  function interpret(text) {
    if (!text) return say(`I got nothin`);
    // interpret as date range & search terms
    const recognized = ISO8601.recognize(text);
    if (recognized) {
      const date = recognized.value;
      const label = ISO8601.label(date, lang);
      say(`It's a date! 📅 ${label}`);
    } else say(`Okay it's about “${text}”`);
  }

  // tracking “now” @ 1Hz
  function tick() {
    const now = new Date();
    const ticks = now.getTime();
    // dialog.dataset.timeframe = `${new_timeframe}`
    dialog.style.setProperty("--timeframe-to-point", css_ticks(ticks));
    current_timeout = setTimeout(tick, 1000);
  }

  function submitted(event) {
    const form = event.target;
    if (!(form instanceof HTMLFormElement)) throw "assert fail";
    const active = doc.activeElement;
    const input = active.closest("input");
    const text = input?.value;
    interpret(text);
    event.preventDefault();
    event.stopPropagation();
  }
  function start() {
    tick();
    take_turn();
    form.addEventListener("submit", submitted, true);
  }
  function stop() {
    form.removeEventListener("submit", submitted);
    clearTimeout(current_timeout);
  }

  container.append(dialog);
  return { start, stop };
}

// what happens if we get another next/prev request while awaiting one
// that wouldn't be this thing's problem, you'd use a buffer in front
// but the next/prev here is like a buffer, a 3-element window
// the current is the center (middle element)
// the previous is the first
// the next is the last

// Array(3)
// [∅, ∅, ∅]
// [A, ∅, ∅]
// [B, A, ∅]
// [C, B, A]

// what does [A, ∅, ∅] mean? there's no current item
// and the A is not observable to the thing looking just at the middle...
// so it's equivalent to [∅, ∅, ∅], right?
// more importantly, what if the input sequence is just [A]?
// then you'll never see it because your terminal state is [A, ∅, ∅]
// since you can't read any more
// also remember you can go back

function reversible_reader(get_reader) {
  let reader, dir, curr_key, curr_value;

  function forward() {
    if (dir !== "next") {
      dir = "next";
      reader?.["return"]();
      reader = get_reader(dir, curr_key);
    }
  }
  function backward() {
    if (dir !== "prev") {
      dir = "prev";
      reader?.["return"]();
      reader = get_reader(dir, curr_key);
    }
  }

  async function get_next() {
    const { done, value } = await reader.next();
    if (done) {
      // what?
    } else {
      curr_key = get_key(value); // this might not be possible
      return value;
    }
  }
}

const list = spec.list ?? [
  "John_F._Kennedy",
  "Christopher_Columbus",
  "Deng_Xiaoping",
  "Prince_Philip,_Duke_of_Edinburgh",
  "Władysław_Sikorski",
  "John_Cage",
  "Adolf_Hitler",
  "Charlemagne",
  "Augustus",
  "Franklin_D._Roosevelt",
  "Henry_VIII_of_England",
  "Charles_II_of_England",
  "Old_Style_and_New_Style_dates",
  "George_Washington",
  "Charles_I_of_England",
  "Ptolemy_I_Soter",
  "Charles_V,_Holy_Roman_Emperor",
  "Old_Style",
  "Henry_III_of_England",
  "Queen_Victoria",
];

async function* async_cat(...seqs) {
  for await (const seq of seqs) for await (const item of seq) yield item;
}
async function* async_identity(seq) {
  for await (const item of seq) yield item;
}
async function* async_cat(seqs) {
  for (const seq of seqs) for await (const item of seq) yield item;
}
async function* async_merge_roundrobin(seqs) {
  const gens = Array.from(seqs, seq => seq[Symbol.asyncIterator]());

  return {
    [Symbol.asyncIterator]() {
      return {
        next() {},
        catch() {},
        return() {},
      };
    },
  };
}

/*
    // for some reason matches stop working these 2 are shared
    // const rex2 = regexp_clone(rex);
    /// const regexp_clone = r => new RegExp(r.pattern, r.flags);

  const describe = what => {
    if (what === undefined) return "No answer";
    if (what === null) return "Nothing";
    const _typeof = typeof what;
    if (_typeof === "symbol" && !Symbol.for(what))
      return `I'm afraid I can't share that information with you.`;
    if (_typeof === "string") return `the text “${what}”`;
    if (_typeof === "number") return `the number “${what}”`;
    console.info(what);
    return `it's a ${_typeof}`;
    // etc
  };

  function memoize_Map(f, cache = new Map()) {
    return function (key) {
      if (cache.has(key)) return cache.get(key);
      const value = f(key);
      cache.set(key, value);
      return value;
    };
  }
*/

/*
  function dialog_process_spec() {
    // If the handlers are generators functions, they will get connected to power
    const spec = {
      // Thing that gets notified as the text input changes.  Every single one?  Maybe.
      *saying(what) {},
      // Thing that gets notified only when the text is submitted
      *said(what) {},
    };
  }

  const monotonic_append = (source, render, parent) => {
    const container = parent ?? document.createElement("div");
    return {
      container,
      process: async_map(source, it => container.append(render(it))),
    };
  };
  */
