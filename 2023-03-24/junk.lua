
-- the timeline index is a collection of the timeline
-- articles for the wikipedia in a given language
-- first the list of links, then the content
--
-- This requires:
-- - a language code
-- - a seed list of links
-- - a logical (intensional) mapping from link to ISO
-- - a place to put the mapping results






--

-- ah impure rules
-- this is fun.
-- so, a rule can write to a file-- no scratch that
-- you can “beat the system”
-- and have a formula that is both computed and carried
-- okay enough, how does it work?

-- you have a way of producing a certain file
-- it requires some processing of its own
-- that is, it's also a formula

-- ugh.  I want to make rules that produce rules

-- and this build system ordinarily doesn't let you do that

-- all the rules that I want to say (based on the given “real” file inputs),
-- I have to get out right now
-- I can't await the execution of the rule to know the next steps

-- so for example, if I want to project 3,000 files based on
-- whatever wikipedia answers for the list of years
-- then I either need to
--   - carry the whole list
--     - or the original answer in full and derive the list (or both etc)
--   - OR, rely on getting the list from Wikipedia before reading it
--
-- Tup rules can't say the latter.  Because fetching the file
-- must be done by a command, which ipso facto cannot precede any rule production -- I mean the collection of any more rules.

-- I mean, we have a process for getting A
-- and based on A, we can say more things B, C, D....
--
-- So in terms of what we carry:
-- - a formula for getting A
-- - a formula for turning the result of A into formulas B, C and D

-- in the case of the history corpus, A is this
-- get_wiki('en', 'List_of_years')

-- And the process P for turning A into another set of rules
-- that must be in Lua, right?

-- it's a mapping over some regular file input
-- Remember it has to be a regular file because normal rule production occurs before any rule execution.

-- But these are not regular regular files.
-- I've been calling them “gray” files because they are not clearly one or the other

-- From Tup's viewpoint, they are regular files which are also the “ignored”
-- outputs of some rule.  I have found a few gotchas in working with these
-- But if we can avoid those, “gray” files can be used for great good!

-- But I want to see how they are abstracted

-- You need at least one filename, let's say exactly one
-- And you need a way to get from that file to zero or more rules

-- Yeah that has to be done in Lua.  And you can't call shell from Lua.

-- In fact, that's one limitation that this helps get around.
