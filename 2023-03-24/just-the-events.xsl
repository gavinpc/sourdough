<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
  <xsl:output indent="yes" omit-xml-declaration="yes" encoding="utf-8" />
  <!-- basically just omit the document element -->
  <xsl:template match="/">
    <xsl:copy-of select="//event" />
  </xsl:template>
</xsl:transform>
