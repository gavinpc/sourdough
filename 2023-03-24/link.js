function get_download_unpack_link(source = document.location) {
  const SHEBANG = /^#!.*?\n/;
  // right but what if this is added later?
  const ele = document.querySelector('[data-project-file="./unpack.sh"]');
  if (!ele) throw new Error(`Couldn't find unpack script!`);
  const script = ele.innerText;
  const binding = `SOURCE_NOTEBOOK='${source}';\n`;
  const bound = script.replace(SHEBANG, `$&\n${binding}`);
  const blob = new Blob([bound], { type: "text/plain" });
  const a = document.createElement("a");
  const url = window.URL.createObjectURL(blob);
  a.download = "unpack.sh";
  a.innerText = "download unpack.sh";
  a.href = url;
  return a;
}

function ensure_download_link() {
  function onload() {
    if (document.querySelector('a[download="unpack.sh"]')) return;
    try {
      const a = get_download_unpack_link();
      document.body.prepend(a);
    } catch (error) {
      console.warn(`I failed to make a download link!`, error);
    }
    globalThis.removeEventListener("load", onload);
  }
  // TODO: this needs revisited. unpack.sh is not in listing for some reason
  // globalThis.addEventListener("load", onload);
}
