const ISO8601 = (function () {
  // operations on extended ISO 8601 representations
  // Uses a four-year sign-optional format, WHICH is not really valid ISO 8601
  // supports:
  // - dates from year -9999 (10,000 BC) through 9999
  // - precision Day, Month, Year, Decade, Century
  // - Millennia as special units using ranges
  const { assert } = console;
  function self_tests() {
    // prettier-ignore
    const CASES = [
      ["day_in_year", ["-0004-03-01"], 31 + 29], // called 5 BC but a leap year
      ["day_in_year", ["-0003-03-01"], 31 + 28], // called 4 BC but not a leap year
      ["day_in_year", ["-0001-03-01"], 31 + 28],
      ["day_in_year", ["0000-03-01"], 31 + 29], // called 1 BC but a leap year
      ["day_in_year", ["0001-03-01"], 31 + 28],
      ["day_in_year", ["0003-03-01"], 31 + 28],
      ["day_in_year", ["0004-03-01"], 31 + 29],
      ["day_in_year", ["1920-01-01"], 0],
      ["day_in_year", ["1920-01-02"], 1],
      ["day_in_year", ["1920-02-01"], 31],
      ["day_in_year", ["1919-12-30"], 363],
      ["day_in_year", ["1920-12-31"], 365], // leap year
      ["day_in_year", ["1900-12-31"], 364], // NOT a leap year
      ["day_in_year", ["1600-12-31"], 365], // leap year
      // parse(-0429/0000) throws “ill-formed millennium” but it's not intended as one
      // PROVISIONAL
      ["recognize", ["1920s-40"], "192/194"],
      ["recognize", ["first mille", 'en'], "0001/1000"],
      ["recognize", ["first millen bc", 'en'], "-0999/0000"],
      ["recognize", ["2nd M bc", 'en'], "-1999/-1000"],
      // ohhhhh this looks like march 1990
      // ["recognize", ["1990-03"], "1993/2004"],
      // ["recognize", ["1978-2023"], "1978/2023"], // later
      ["label", ["-9999"], "10000 BC"],
      ["label", ["-9998"], "9999 BC"],
      ["label", ["-0002"], "3 BC"],
      ["label", ["-0001"], "2 BC"],
      ["label", ["-0000"], "1 BC"], // though -0000 isn't valid...
      ["label", ["0000"], "1 BC"],
      ["label", ["0001"], "1"], // could also be ‘1 CE’
      ["label", ["0002"], "2"], // etc
      ["label", ["0012"], "12"],
      ["label", ["0123"], "123"],
      ["label", ["9999"], "9999"],
      ["label", ["1564-04", "en"], "April 1564"],
      // rn day is using short months
      // ["label", ["-0009-12", "en"], "December 10 BC"],
      ["label", ["1957-05-06"], "May 6, 1957"],
      // ["label", ["1957-07-06"], "July 6, 1957"],
      ["label", ["-0400-07-14"], "Jul 14, 401 BC"],
      ["label", ["-1999/-1000", "en"], "2nd millennium BC"],
      ["label", ["-0999/0000", "en"], "1st millennium BC"],
      ["label", ["0001/1000", "en"], "1st millennium"],
      ["label", ["1001/2000", "en"], "2nd millennium"],
      ["label", ["1902-03/1988"], "March 1902 – 1988"], // EN DASH, not hyphen
      // FAILS (rn) getting “August 18, 294 BC”... is this some time zone bs?
      // ["label", ["-0294-08-19"], "August 19, 294 BC"],
      ["compare_units", ["Year", "Year"], 0],
      ["compare_units", ["Year", "Month"], 1],
      ["compare_units", ["Month", "Year"], -1],
      ["compare_units", ["Decade", "Year"], 1],
      ["compare_units", ["Century", "Year"], 2],
      ["compare", ["1234", "1234"], 0], // indeed, ∀ x compare(x,x)=0
      ["compare", ["0234", "1234"], -1],
      ["compare", ["0234", "0230"], 1],
      ["compare", ["-0234", "0230"], -1],
      ["compare", ["-0234", "-0230"], -1],
      ["compare", ["-0234", "-0238"], 1],
      ["compare", ["-0100-06", "-0100-07"], -1],
      ["compare", ["-0100-06", "-0100-05"], 1],
      ["compare", ["-0100-06-11", "-0100-06-15"], -1],
      ["compare", ["-0100-06-19", "-0100-06-15"], 1],
      ["compare", ["-0100-06-19", "-0099-06-15"], -1],
      ["between", ["1234", "-0234", "1238"], true],
      ["between", ["1234", "-0234", "-1238"], false],
      // ["distance", ["-12", "-04"], {unit: "Century", n: 8 }],
      // ["distance", ["-01", "05"], {unit: "Century", n: 5 }],
      ["distance", ["-01", "01"], { unit: "Century", n: 1 }],
      ["distance", ["01", "05"], { unit: "Century", n: 4 }],
      ["distance", ["07", "18"], { unit: "Century", n: 11 }],
      ["distance", ["18", "18"], { unit: "Century", n: 0 }],
      ["distance", ["-0001", "0001"], { unit: "Year", n: 2 }],
      ["distance", ["-0001", "0001"], { unit: "Year", n: 2 }],
      ["distance", ["-0001", "0000"], { unit: "Year", n: 1 }],
      ["distance", ["0000", "0001"], { unit: "Year", n: 1 }],
      ["distance", ["0024", "0142"], { unit: "Year", n: 118 }],
      ["distance", ["1234", "1234"], { unit: "Year", n: 0 }],
      ["distance", ["1234", "2345"], { unit: "Year", n: 1111 }],
      ["distance", ["1234-01", "1234-01"], { unit: "Month", n: 0 }],
      ["distance", ["1234-01", "1234-02"], { unit: "Month", n: 1 }],
      ["distance", ["1234-01", "1235-01"], { unit: "Month", n: 12 }],
      ["distance", ["-0700-06", "-0695-06"], { unit: "Month", n: 60 }],
      ["distance", ["-0700-06", "-0695-07"], { unit: "Month", n: 61 }],
      ["distance", ["-0000-01", "0001-01"], { unit: "Month", n: 12 }], // but -0000 is invalid
      ["distance", ["-0001-01", "0000-01"], { unit: "Month", n: 12 }],
      ["distance", ["1066-02-01", "1066-02-01"], { unit: "Day", n: 0 }],
      ["distance", ["1066-02-01", "1066-02-03"], { unit: "Day", n: 2 }],
      ["distance", ["1066-02-01", "1066-03-01"], { unit: "Day", n: 28 }],
      ["distance", ["1066-02-01", "1067-03-01"], { unit: "Day", n: 28 + 365 }],
      ["next", ["-0099-12-31"], "-0098-01-01"],
      ["next", ["-0009-02-28"], "-0009-03-01"],
      ["next", ["-0001-12-31"], "0000-01-01"],
      ["next", ["0099-12-31"], "0100-01-01"],
      ["next", ["1432-11-30"], "1432-12-01"],
      ["next", ["-0088-11"], "-0088-12"],
      ["next", ["-0088-12"], "-0087-01"],
      ["next", ["-0001-12"], "0000-01"],
      ["next", ["0000-01"], "0000-02"],
      ["next", ["1607-08"], "1607-09"],
      ["next", ["9998-12"], "9999-01"],
      ["next", ["-9999"], "-9998"],
      ["next", ["-0001"], "0000"],
      ["next", ["0000"], "0001"],
      ["next", ["9998"], "9999"],
      // ["next", ["9999"], undefined], // for worse or better this returns "10000"
      // THESE fail, but narrower/broader assume 0s & 0s BC -- how are they notated?
      // ["next", ["-001"], "-000"],
      // ["next", ["-000"], "000"],
      ["next", ["000"], "001"],
      ["next", ["-02"], "-01"],
      ["next", ["-01"], "00"],
      ["next", ["00"], "01"],
      ["next", ["01"], "02"],
      ["next", ["-2999/-2000"], "-1999/-1000"],
      ["next", ["-1999/-1000"], "-0999/0000"],
      ["next", ["-0999/0000"], "0001/1000"],
      ["next", ["0001/1000"], "1001/2000"],
      ["next", ["1001/2000"], "2001/3000"],
      ["broader", ["2023-05-13"], "2023-05"],
      ["broader", ["-0294-08-19"], "-0294-08"],
      ["broader", ["2023-05"], "2023"],
      ["broader", ["2023"], "202"],
      // FAILS with 000 but this is 1BC and that is 0's AD
      // ["broader", ["0000"], "-000"],
      ["broader", ["-0001"], "-000"],
      ["broader", ["202"], "20"],
      ["broader", ["000"], "00"],
      ["broader", ["-001"], "-01"],
      ["broader", ["-040"], "-05"],
      ["broader", ["01"], "0001/1000"],
      ["broader", ["07"], "0001/1000"],
      ["broader", ["19"], "1001/2000"],
      ["broader", ["20"], "2001/3000"],
      ["broader", ["-01"], "-0999/0000"],
      ["broader", ["-02"], "-0999/0000"],
      ["broader", ["-10"], "-0999/0000"],
      ["broader", ["-11"], "-1999/-1000"],
      ["broader", ["-19"], "-1999/-1000"],
      ["broader", ["-20"], "-1999/-1000"],
      ["broader", ["0001"], "000"],
      ["narrower", ["2001/3000"], "20 21 22 23 24 25 26 27 28 29".split(" ")],
      ["narrower", ["-0999/0000"], "-10 -09 -08 -07 -06 -05 -04 -03 -02 -01".split(" ")],
      ["narrower", ["20"], "200 201 202 203 204 205 206 207 208 209".split(" ")],
      ["narrower", ["-01"], "-009 -008 -007 -006 -005 -004 -003 -002 -001 -000".split(" ")],
      ["narrower", ["-08"], "-079 -078 -077 -076 -075 -074 -073 -072 -071 -070".split(" ")],
      ["narrower", ["001"], "0010 0011 0012 0013 0014 0015 0016 0017 0018 0019".split(" ")],
      ["narrower", ["202"], "2020 2021 2022 2023 2024 2025 2026 2027 2028 2029".split(" ")],
      ["narrower", ["-001"], "-0018 -0017 -0016 -0015 -0014 -0013 -0012 -0011 -0010 -0009".split(" ")],
      // SPECIAL CASES: “0-to-9” decades
      ["narrower", ["-000"], "-0008 -0007 -0006 -0005 -0004 -0003 -0002 -0001 0000".split(" ")],
      ["narrower", ["000"], "0001 0002 0003 0004 0005 0006 0007 0008 0009".split(" ")],
      ["narrower", ["0030"], "0030-01 0030-02 0030-03 0030-04 0030-05 0030-06 0030-07 0030-08 0030-09 0030-10 0030-11 0030-12".split(" ")],
      ["narrower", ["0000"], "0000-01 0000-02 0000-03 0000-04 0000-05 0000-06 0000-07 0000-08 0000-09 0000-10 0000-11 0000-12".split(" ")],
      ["narrower", ["0000-02"], "0000-02-01 0000-02-02 0000-02-03 0000-02-04 0000-02-05 0000-02-06 0000-02-07 0000-02-08 0000-02-09 0000-02-10 0000-02-11 0000-02-12 0000-02-13 0000-02-14 0000-02-15 0000-02-16 0000-02-17 0000-02-18 0000-02-19 0000-02-20 0000-02-21 0000-02-22 0000-02-23 0000-02-24 0000-02-25 0000-02-26 0000-02-27 0000-02-28 0000-02-29".split(" ")],
      ["narrower", ["-0003-02"], "-0003-02-01 -0003-02-02 -0003-02-03 -0003-02-04 -0003-02-05 -0003-02-06 -0003-02-07 -0003-02-08 -0003-02-09 -0003-02-10 -0003-02-11 -0003-02-12 -0003-02-13 -0003-02-14 -0003-02-15 -0003-02-16 -0003-02-17 -0003-02-18 -0003-02-19 -0003-02-20 -0003-02-21 -0003-02-22 -0003-02-23 -0003-02-24 -0003-02-25 -0003-02-26 -0003-02-27 -0003-02-28".split(" ")],
      ["coerce", ["00", "Year"], "0001"],
      // THIS fails with -0098 but it's close enough for positioning
      // ["coerce", ["-01", "Year"], "-0099"],
      // THIS fails with 2000 for a similar reason, the enclosures are irregular
      // ["coerce", ["2001/3000", "Year"], "2001"],
      ["coerce", ["12", "Year"], "1200"],
      ["coerce", ["12", "Year", { align: "start" }], "1200"],
      ["coerce", ["12", "Year", { align: "end" }], "1299"],
      ["coerce", ["123", "Year"], "1230"],
      ["coerce", ["123", "Year", { align: "end" }], "1239"],
      ["coerce", ["1234", "Year", { align: "end" }], "1234"],
      ["coerce", ["1656-02", "Day", {align: "end"}], "1656-02-29"],
      ["coerce", ["1234", "Decade"], "123"],
      // align has no effect when broadening
      ["coerce", ["1234", "Decade", { align: "end" }], "123"],
      ["coerce", ["1234", "Century"], "12"],
      ["coerce", ["12", "Decade"], "120"],
      ["coerce", ["2023", "Millennium"], "2001/3000"],
    ];
    const equals = (a, b) => a === b || JSON.stringify(a) === JSON.stringify(b);
    for (const [op, args, expect] of CASES) {
      let got = ISO8601[op](...args);
      if (op === "recognize") got = got.value;
      assert?.(
        equals(got, expect),
        `${op}(${args}) got ${JSON.stringify(got)} vs ${JSON.stringify(expect)}`
      );
      // all returned dates should parse
      // all dates should have labels
      // all labels should be recognized as the thing that was their label
      // ∀ x,y broader(x)=y → y∈narrower(x)
      if (op === "broader") {
        const [x] = args;
        const narr = ISO8601.narrower(got);
        assert?.(narr.includes(x), `${x} ∉ narr(${got})`);
      }
      // ∀ x,y,z compare(x, y)=z ↔ compare(y, x)=-z
      if (op === "compare") {
        const [x, y] = args;
        const inv = ISO8601.compare(y, x);
        assert?.(expect === -inv, `comp(${y},${x}) ≠ ${-inv}`);
      }
      // ∀ x,y distance(x,y)=d ↔ distance(y,x)=-d
      if (op === "distance") {
        const [x, y] = args;
        const d = expect;
        const inv = ISO8601.distance(y, x);
        assert?.(d.n === -inv.n, `dist(${y},${x}) ≠ ${JSON.stringify(inv)}`);
      }
      // TODO: when plus is implemented for more stuff
      // ∀ x,y: next(x)=y → plus(x, 1)=y
      // ∀ x,y: previous(x)=y → plus(x, -1)=y
      if (op === "next") {
        // ∀ x,y next(x)=y ↔ previous(y)=x (i.e. previous inverseOf next)
        {
          const [x] = args;
          const y = expect;
          const py = ISO8601.previous(y);
          assert?.(equals(py, x), `prev(${y}) = ${x}, got ${py}`);
        }
        // ∀ x,y next(x)=y → x<y
        {
          const [x] = args;
          const cmp = ISO8601.compare(x, got);
          assert?.(cmp < 0, `${x} ≮ ${got} = next(${x})`);
        }
      }
    }
  }

  const MONTH_NAME = new Intl.DateTimeFormat(undefined, {
    month: "long",
    timeZone: "UTC",
  });
  const DAY_LABEL = new Intl.DateTimeFormat(undefined, {
    year: "numeric",
    month: "short",
    day: "numeric",
    timeZone: "UTC",
  });

  const int = s => parseInt(s, 10);
  const pad = len => n =>
    (n < 0 ? "-" : "") + Math.abs(n).toString().padStart(len, "0");
  const pad2 = pad(2);
  const pad3 = pad(3);
  const pad4 = pad(4);
  const pad6 = pad(6);

  // see https://vocabs.acdh.oeaw.ac.at/unit_of_time/
  // and https://vocabs.acdh.oeaw.ac.at/date/
  const _TIME_UNITS = {
    Millisecond: { rank: 0 },
    Second: { rank: 1 },
    Minute: { rank: 2 },
    Hour: { rank: 3 },
    Day: { rank: 4 },
    Week: { rank: 5 },
    Month: { rank: 6 },
    Year: { rank: 7 },
    Decade: { rank: 8 },
    Century: { rank: 9 },
    Millennium: { rank: 10 },
  };
  // larger = broader
  function compare_units(a, b) {
    assert?.(a in _TIME_UNITS, `unknown unit ${a}`);
    assert?.(b in _TIME_UNITS, `unknown unit ${b}`);
    return _TIME_UNITS[a].rank - _TIME_UNITS[b].rank;
  }
  const EN_UNITS = Object.freeze({
    Millisecond: { singular: "millisecond", plural: "milliseconds" },
    Second: { singular: "second", plural: "seconds" },
    Minute: { singular: "minute", plural: "minutes" },
    Hour: { singular: "hour", plural: "hours" },
    Day: { singular: "day", plural: "days" },
    Week: { singular: "week", plural: "weeks" },
    Month: { singular: "month", plural: "months" },
    Year: { singular: "year", plural: "years" },
    Decade: { singular: "decade", plural: "decades" },
    Century: { singular: "century", plural: "centurys" },
    Millennium: { singular: "millennium", plural: "millenniums" },
  });
  function label_unit(unit, plural, lang) {
    if (lang !== "en") throw new Error(`Not implemented: lang ${lang}`);
    const record = EN_UNITS[unit];
    if (!record) throw new Error(`Bad request: unknown unit ${unit}`);
    return plural ? record.plural : record.singular;
  }

  // ====== I/O

  {
    const DATE =
      /^(?<day>(?<month>(?<year>(?<decade>(?<bc>-?)(?<century>\d\d)(?<d>\d)?)(?<y>\d)?)(?:-(?<mm>\d\d))?)(?:-(?<dd>\d\d))?)$/;
    const MILLENNIUM = /^((-?)\d{4,5})\/-?(\d0000?)$/;

    function parse(iso) {
      if (typeof iso !== "string") return;

      // e.g. 2nd millennium is 1001/2000
      const mill = iso.match(MILLENNIUM);
      if (mill) {
        const [, from, bc, to] = mill;
        const n = int(`${bc}${to}`);
        // and we know from pattern that “to” ends in 000
        if (n === int(from) + 999)
          // `millennium` is the ending year (so, 1000 for first millennium)
          return { input: iso, unit: "Millennium", bc, millennium: n };
      }

      // Um, for now millennia are special
      let end;
      if (iso.includes("/")) {
        const [from, to] = iso.split("/", 2);
        end = parse(to);
        iso = from; // rosary
      }

      const match = iso.match(DATE);
      if (!match) return;

      const { dd, mm, y, d } = match.groups;
      // prettier-ignore
      const unit = dd ? "Day"
          : mm ? "Month"
          : y ? "Year"
          : d ? "Decade"
          : "Century"

      return { input: iso, unit, end, ...match.groups }; // HOTSPOT
    }

    // return date in the signed, six-digit format used by `Date`
    // ASSUMES the date is a well-formed date string (4-digit year, sign optional)
    // ASSUMES unit >= Year
    function _to_Date_ISO(date) {
      // allow for signed four-year dates, though those aren't recognized elsewhere yet
      if (date.startsWith("-") || date.startsWith("+"))
        return `${date.charAt(0)}00${date.slice(1)}`;
      return `+00${date}`;
    }

    // ASSUMES same as `_to_Date_ISO`
    function Date_from(date) {
      const js_iso = _to_Date_ISO(date);
      const ticks = Date.parse(js_iso);
      if (isNaN(ticks)) return;
      return new Date(ticks);
    }
  }

  // ====== English recognizing and labeling

  const languages = (function () {
    function make_recognizer(recognizers) {
      return function recognize(message) {
        for (const [pattern, transform] of recognizers) {
          const match = message.match(pattern);
          if (!match) continue;
          const value = transform(match, message);
          if (value === undefined) continue;
          // I'm sorry but I need to do this
          return { input: message, format: "iso8601", value, match };
        }
      };
    }
    // could make one giant regex w/distinctly named groups to avoid making substrings
    function make_recognize_all(recognizers) {
      return function* recognize_all(message) {
        let text = message;
        for (const [pattern, transform] of recognizers) {
          const match = message.match(pattern);
          if (!match) continue;
          const value = transform(match, message);
          if (value === undefined) continue;
          // I'm sorry but I need to do this
          yield { input: message, format: "iso8601", value, match };
          text = text.substring(match.index + match.length);
        }
      };
    }

    const ORDINAL_SUFFIXES_EN = { 1: "st", 2: "nd", 3: "rd" };
    const ordinal_en = z => {
      const n = Math.abs(z);
      const c = n % 100;
      const suffix =
        c >= 11 && c <= 13 ? "th" : ORDINAL_SUFFIXES_EN[n % 10] ?? "th";
      return `${n}${suffix}`;
    };

    const RECOGNIZERS_EN = [
      [{ [Symbol.match]: parse }, parsed => parsed.input],
      // Roman numerals...
      // even with caps this will overlap with term searches
      [/^M+$/, ([s]) => millennium(s.length)],
      // ranges...
      [
        /(?<from>-?\s*\d+)[ths]*\s*(c\w*)?\s*(-|to|through|thru|\.\.+)\s*(?<to>-?\s*\d+)\s*(c\w*)?/i,
        ({ groups: { from, to } }, text) => {
          if (!text.toLowerCase().includes("c")) return;
          const first = int(from);
          const second = int(to);

          if (first > 10 && second < 10)
            // e.g. 15-6c
            // prettier-ignore
            return `${pad2(first - 1)}/${pad2(first - (first % 10) + second - 1)}`;

          return `${pad2(first - 1)}/${pad2(second - 1)}`;
        },
      ],
      [
        /(?<from>-?\s*\d+)\s*(['"]*s)?\s*.*\b(-|to|through|thru|\.\.+)[-\D]*(?<to>-?\s*\d+)\s*(['"]*s)?/,
        (match, text) => {
          const { from, to } = match.groups;
          if (!text.toLowerCase().includes("s")) return;
          const first = int(from);
          const second = int(to);

          if (first > second) {
            if (first >= 100 && second < 100) {
              const yy = first % 100;
              if (yy < second) {
                // e.g. 1640-50's
                // prettier-ignore
                return `${pad3(first / 10)}/${pad3((first - yy + second) / 10)}`;
              }

              if (second < 10)
                // e.g. 1690s-2's → 1690s -1720s
                return `${pad3(first)}/${pad3(first - yy + 100 + second * 10)}`;
            }
            // this makes no sense though
            return `${pad3(second)}/${pad3(first)}`;
          }

          return `${pad3(first)}/${pad3(second)}`;
        },
      ],
      [
        /(?<from>-?\s*\d+)\s*.*(-|to|through|thru|\.\.+).*(?<to>-?\s*\d+)/,
        ({ groups: { from, to } }) => {
          const first = int(from);
          const second = int(to);

          if (first > second) {
            if (first >= 100 && second < 100) {
              if (second < 10)
                // e.g. 1941-5
                return `${pad4(first)}/${pad4(first - (first % 10) + second)}`;
              // e.g. 580-90
              return `${pad4(first)}/${pad4(first - (first % 100) + second)}`;
            }
            return `${pad4(second)}/${pad4(first)}`;
          }
          return `${pad4(first)}/${pad4(second)}`;
        },
      ],
      [
        /^\s*-?(first|second|third|fourth|fifth|sixth|seventh|\d?1st|\d?2nd|\d?3rd|\d?[04-9]th|\d{1,2})\s*c+(e(n(t(u(r(y)?)?)?)?)?)?\s*(b+c*e*)?$/,
        message => {
          const bc = message.includes("-") || message.includes("b");
          const n = Math.abs(int(message));
          const cc = bc ? n : n - 1;
          return `${bc ? "-" : ""}${pad2(cc)}`;
        },
      ],
      [
        /^\s*-?\d{1,4}'?s\s*(b+c*e*)?$/,
        message => {
          const bc = message.includes("-") || message.includes("b");
          const n = int(message);
          const year = bc && n > 0 ? -n : n;
          const decade = Math.floor(year / 10);
          return `${bc ? "-" : ""}${pad3(decade)}`;
        },
      ],
      [
        /^\s*-?\d{1,4}\s*(b+c*e*)?$/,
        message => {
          const bc = message.includes("-") || message.includes("b");
          const n = int(message);
          const year = bc && n > 0 ? -n : n;
          return `${bc ? "-" : ""}${pad4(year)}`;
        },
      ],
    ];

    function label_en(iso, simple) {
      const parsed = parse(iso);
      assert?.(parsed, `label en got bad date ${iso}`);
      // millennia is a special case of ranges which has its own unit handling
      if (!simple && iso.includes("/") && parsed.unit !== "Millennium") {
        const [lo, hi] = iso.split("/", 2);
        // TBD dependent ranges, e.g. 1920's – 40's, or 1st – 2nd millennia
        // else label independently (which note is not language specific)
        return `${label_en(lo, true)} – ${label_en(hi, true)}`;
      }
      const { unit, bc } = parsed;
      const maybe_bc = bc ? " BC" : ""; // yeah yeah BCE
      switch (unit) {
        case "Millennium": {
          const m = parsed.millennium / 1000;
          const mm = bc ? 1 - m : m;
          return `${ordinal_en(mm)} millennium${maybe_bc}`;
        }
        case "Century": {
          const c = Math.abs(int(parsed.century)) + (bc ? 0 : 1);
          return `${ordinal_en(c)} century${maybe_bc}`;
        }
        case "Decade":
          // maybe add (decade) for first decade of a century
          return `${Math.abs(int(parsed.decade)) * 10}’s${maybe_bc}`;

        case "Year": {
          const year = int(parsed.year);
          // could also put AD or CE for small numbers
          return year > 0 ? `${year}` : `${Math.abs(year) + 1} BC`;
        }
        case "Month": {
          const year_label = label_en(parsed.year);
          const iso = bc ? parsed.input.slice(1) : parsed.input;
          const ticks = Date.parse(iso);
          const first = new Date(ticks);
          const month_name = MONTH_NAME.format(first);
          return `${month_name} ${year_label}`;
        }
        case "Day": {
          const js_iso = _to_Date_ISO(iso);
          const ticks = Date.parse(js_iso);
          return `${DAY_LABEL.format(ticks)}${maybe_bc}`;
        }
      }
      // console.warn(`Not implemented, labeling ${unit}`);
      return iso;
    }

    const en_recognizer = make_recognizer(RECOGNIZERS_EN);

    function recognize(text, lang = "en") {
      if (lang === "en") return en_recognizer(text);
      console.info(`Not implemented ${lang}`);
    }

    function label(iso, lang = "en") {
      if (lang === "en") return label_en(iso);
      console.warn(`Language ‘${lang}’ is not supported for ISO labeling`);
      return iso; // fallback to ISO
    }

    return { recognize, label };
  })();

  const range = (n, f) => {
    const ret = Array(n);
    for (let i = 0; i < n; i++) ret[i] = f(i);
    return ret;
  };

  // inclusive --- though we generally want end exclusive
  const between = (date, a, b) =>
    compare(a, date) <= 0 && compare(b, date) >= 0;

  // >0 means sort a after b
  // <0 means sort a before b
  function compare(a, b) {
    if (a === b) return 0;
    if (a.startsWith("-")) {
      if (b.startsWith("-")) {
        // in BC comparisons, years (and *only* years) are reversed
        const a_year = a.slice(0, 5); // allocation
        // do a within-year comparison... and more allocation
        if (b.startsWith(a_year)) return a.slice(5) < b.slice(5) ? -1 : 1;
        return a < b ? 1 : -1;
      }
      return -1;
    }
    if (b.startsWith("-")) return 1;
    return a < b ? -1 : 1;
  }

  const get_c = parsed => int(`${parsed.bc}${parsed.century}`);
  const get_m = (parsed, c = get_c(parsed)) => Math.floor(c / 10) + 1;
  const m2 = e => `${pad4(e - 999)}/${pad4(e)}`;
  const millennium = m => m2(m * 1000);

  // ====== sequence: next & previous

  function next(date) {
    const the = parse(date);
    assert?.(the, `next got bad date`, date);
    switch (the.unit) {
      case "Millennium":
        return m2(the.millennium + 1000);
      case "Century":
        return pad2(get_c(the) + 1);
      case "Decade":
        return pad3(int(the.decade) + 1);
      case "Year":
        return pad4(int(the.year) + 1);
      case "Month":
        if (the.mm === "12") return `${next(the.year)}-01`;
        return `${the.year}-${pad2(int(the.mm) + 1)}`;
      case "Day": {
        const js_date = Date_from(date);
        js_date.setUTCDate(js_date.getUTCDate() + 1);
        return _Date_to_Day(js_date);
      }
    }
    throw new Error(`next not implemented for ${the.unit}`);
  }

  function _Date_to_Day(js_date) {
    const js_iso = js_date.toISOString();
    if (js_iso.startsWith("-") || js_iso.startsWith("+"))
      return `${js_iso.charAt(0)}${js_iso.substring(3, 13)}`;
    return js_iso.slice(0, 10);
  }

  function previous(date) {
    const the = parse(date);
    assert?.(the, `previous got bad date`, date);
    switch (the.unit) {
      case "Millennium":
        return m2(the.millennium - 1000);
      case "Century":
        return pad2(get_c(the) - 1);
      case "Decade":
        return pad3(int(the.decade) - 1);
      case "Year":
        return pad4(int(the.year) - 1);
      case "Month":
        if (the.mm === "01") return `${previous(the.year)}-12`;
        return `${the.year}-${pad2(int(the.mm) - 1)}`;
      case "Day": {
        // Same as `next` except `-1` instead of `+1`
        const js_date = Date_from(date);
        js_date.setUTCDate(js_date.getUTCDate() - 1);
        return _Date_to_Day(js_date);
      }
    }
    throw new Error(`previous not implemented for ${the.unit}`);
  }
  // ====== Taxonomical: broader & narrower

  function broader(iso) {
    const parsed = parse(iso);
    if (!parsed) return;
    switch (parsed.unit) {
      case "Century":
        return millennium(get_m(parsed));
      case "Decade":
        return pad2(parsed.bc ? -1 - int(parsed.century) : int(parsed.century));
      case "Year":
        return parsed.decade;
      case "Month":
        return parsed.year;
      case "Day":
        return parsed.month;
    }
  }

  function narrower(iso) {
    const parsed = parse(iso);
    if (!parsed) return undefined;
    switch (parsed.unit) {
      case "Millennium" /* → Centuries */: {
        const n = int(parsed.millennium) / 1000;
        if (n !== Math.floor(n)) throw new Error(`BAD MILLENNIUM ${iso}`);
        return range(10, i => pad2((n - 1) * 10 + i + (parsed.bc ? 0 : 0)));
      }
      case "Century" /* → Decades */: {
        const { bc, century } = parsed;
        const n = int(century);
        return range(10, i => `${bc}${pad3(10 * n + (bc ? -1 - i : i))}`);
      }
      case "Decade" /* → Years */: {
        if (parsed.input === "000") return range(9, i => pad4(i + 1)); // 0s
        if (parsed.input === "-000") return range(9, i => pad4(i - 8)); // 0s BC
        if (parsed.bc) {
          const n = int(parsed.decade);
          return range(10, i => pad4(10 * n - 8 + i));
        }
        return range(10, i => `${parsed.input}${i}`);
      }
      case "Year" /* → Months */:
        return range(12, i => `${parsed.input}-${pad2(i + 1)}`);

      case "Month" /* → Days */: {
        const ret = [];
        const year = int(parsed.year);
        const js_date = new Date(Date.UTC(year, int(parsed.mm) - 1));
        js_date.setUTCFullYear(year); // account for Date.UTC's quirk re 1900's
        const month = js_date.getUTCMonth();
        while (month === js_date.getUTCMonth()) {
          const day = js_date.getUTCDate();
          ret.push(`${parsed.month}-${pad2(day)}`);
          js_date.setUTCDate(day + 1);
        }
        return ret;
      }
    }
  }

  // returns {unit, n}.  is `n` an integer then?
  const distance = (iso_from, iso_to) => {
    const a = parse(iso_from);
    const b = parse(iso_to);
    const unit = a?.unit;
    if (unit !== b?.unit)
      throw new Error(`Not supported: ${iso_to}-${iso_from}`);

    // A surer way to maintain this property
    // as it is, the below must deal with a ≶ b
    // if (compare(iso_from, iso_to) > 0)
    //   return { unit, n: -distance(iso_to, iso_from).n };

    switch (unit) {
      case "Millennium":
        return { unit, n: int(b.millennium) / 1e3 - int(a.millennium) / 1e3 };
      case "Century": {
        // There is no 0th century; skip it
        // WRONG! there is a 0 century, we just don't call it that
        const aa = get_c(a);
        const bb = get_c(b);
        if (aa < 0 && bb > 0) return { unit, n: bb - aa - 1 };
        if (bb < 0 && aa > 0) return { unit, n: bb - aa + 1 };
        return { unit, n: bb - aa };
      }
      case "Decade":
        return { unit, n: int(b.decade) - int(a.decade) };
      case "Year":
        return { unit, n: int(b.year) - int(a.year) };
      case "Month": {
        const years = int(b.year) - int(a.year);
        return { unit, n: years * 12 + int(b.mm) - int(a.mm) };
      }
      case "Day": {
        const a_ticks = Date_from(a.input).getTime();
        const b_ticks = Date_from(b.input).getTime();
        return { unit, n: Math.floor((b_ticks - a_ticks) / TICKS_PER_DAY) };
      }
    }
    throw new Error(`Distance is not implemented for ${unit}`);
  };

  // PRELIMINARY
  function plus(date, units, in_unit) {
    const parsed = parse(date);
    if (!parsed) throw new Error(`Bad request: plus got bad date`);
    const unit = in_unit ?? parsed.unit;
    if (units !== Math.floor(units))
      throw new Error(`Bad request: +non-integer ${units}`);
    if (unit !== parsed.unit)
      throw new Error(`Not implemented: +${unit} for ${parsed.unit}`);

    switch (unit) {
      case "Day": {
        const js_date = Date_from(date);
        js_date.setUTCDate(js_date.getUTCDate() + units);
        return _Date_to_Day(js_date);
      }
    }
    throw new Error(`Not implemented: +${unit}`);
  }

  // return a date with `unit` precision related to date
  // if unit is less specific, then take the broader containing
  // if unit is more specific, then align start (i.e. take first narrower)
  // KNOWN ISSUE: gets first year of last decade when end-aligning from century to year
  function coerce(date, unit, options) {
    const align = options?.align ?? "start";
    assert?.(align === "start" || align === "end", `bad align ${align}`);
    const parsed = parse(date);
    if (!parsed) throw new Error(`Coerce got bad date ${date}`);
    if (parsed.unit === unit) return date;
    if (compare_units(parsed.unit, unit) < 0)
      return coerce(broader(date), unit, options);
    const parts = narrower(date);
    const index = align === "start" ? 0 : -1;
    const part = parts.at(index);
    if (!part) throw new Error(`Failed coercing ${date} to ${unit}`);
    return coerce(part, unit, options);
  }

  const TICKS_PER_DAY = 24 * 60 * 60 * 1000;
  function day_in_year(date) {
    const js_date = Date_from(date);
    const year = js_date.getUTCFullYear();
    // Unfortunately,
    //     new Date(Date.UTC(0, 0, 1)).toISOString()
    //     "1900-01-01T00:00:00.000Z"
    // even though
    //     new Date(Date.UTC(-1, 0, 1)).toISOString()
    //     "-000001-01-01T00:00:00.000Z"
    // const first_of_year = new Date(Date.UTC(year, 0, 1));
    // TODO: does this need +1 for BC?
    const first_of_year = Date_from(`${pad4(year)}-01-01`);
    const start_ticks = first_of_year.getTime();
    const end_ticks = js_date.getTime();
    const delta_ticks = end_ticks - start_ticks;
    const days = delta_ticks / TICKS_PER_DAY;
    const day = Math.round(days);
    return day;
  }
  // heavy... we could get close enough leap year with modulos
  // yes: https://tc39.es/ecma262/multipage/numbers-and-dates.html#sec-daysinyear
  function days_in_year(date) {
    const parsed = parse(date);
    const year_end = `${parsed.year}-12-31`; // ≍ coerce(date, "Day", {align: "end"})
    return day_in_year(year_end) + 1;
  }

  const io = { parse, Date_from };
  const ord = { compare, between, distance, plus }; // poset & metric space
  const seq = { next, previous };
  const skos = { broader, narrower };
  const units = { compare_units, label_unit };
  const calendar = { days_in_year, day_in_year }; // scalar support
  const other = { ...units, self_tests, coerce, ...calendar };
  const ISO8601 = { ...io, ...ord, ...seq, ...skos, ...languages, ...other };
  Object.assign(globalThis, { ISO8601 });
  return ISO8601;
})();

// indexeddb wrapper stuff
// good info about indexedDB state here https://stackoverflow.com/q/33441956

(function () {
  if (!indexedDB) {
    console.warn("Your browser doesn't support a stable version of IndexedDB!");
    return;
  }
  const { stringify } = JSON;
  const { keys } = Object;
  const { assert } = console;

  const NULL_BUFFER = { is_empty: () => true, is_full: () => true, length: 0 };
  const make_ring_buffer = size => {
    if (size === 0) return NULL_BUFFER;
    const _buffer = new Array(size);
    let start = 0; // aka head, where the first item is (when ≠ end)
    let end = 0; // aka tail, where the next item will go
    let length = 0;

    return {
      is_empty: () => length === 0,
      is_full: () => length === size,
      push(value) {
        assert?.(length < size, `Full!`);
        ++length;
        _buffer[end] = value;
        end = (end + 1) % size;
      },
      pop() {
        assert?.(length !== 0, `Empty!`);
        --length;
        const value = _buffer[start];
        start = (start + 1) % size;
        return value;
      },
      get length() {
        return length;
      },
    };
  };

  // ===== async helpers
  const sleep = ms => new Promise(r => setTimeout(r, ms));

  /**
   * @param options {{max_concurrent?: number}}
   */
  const async_queue = options => {
    const queue = []; // promise thunks
    const inflight = new Set(); // promises
    const max = options?.max_concurrent ?? 1;

    const launch = thunk => {
      const promise = thunk();
      inflight.add(promise);
      promise.finally(() => {
        inflight.delete(promise);
        if (queue.length > 0) launch(queue.shift());
      });
      return promise;
    };

    /**
     * @template T
     * @param thunk {() => Promise<T>}
     * @returns {Promise<T>}
     */
    return thunk => {
      if (inflight.size < max) {
        assert?.(
          !queue.length,
          `${queue.length} in queue ${inflight.size}<${max}!`
        );
        return launch(thunk);
      }
      return new Promise(resolve => {
        queue.push(() => {
          const promise = thunk();
          resolve(promise);
          return promise;
        });
      });
    };
  };

  /**
   * @template T
   * @param array {T[]}
   * @returns {array is T[] & { shift(): T; pop(): T }}
   */
  const is_not_empty = array => array.length > 0;

  /**
   * A minimal blocking queue.
   *
   * @param buffer {ReturnType<typeof make_ring_buffer>}
   */
  function make_channel(buffer) {
    const pending_put_values = [];
    const pending_put_resolves = [];
    const pending_takes = [];
    return {
      can_put_now: () => !buffer.is_full(),
      can_take_now: () => !buffer.is_empty(),
      put(value) {
        if (is_not_empty(pending_takes)) {
          const n = pending_takes.length;
          assert?.(buffer.is_empty(), `${n} pending takes but non-∅ buffer `);
          pending_takes.shift()(value);
        } else if (!buffer.is_full()) buffer.push(value);
        else
          return new Promise(resolve => {
            pending_put_values.push(value);
            pending_put_resolves.push(resolve);
            // if count exceeds expected # of (well-behaved) writers
            // console.debug(`${pending_put_resolves.length} pending puts`);
          });
      },
      take() {
        if (buffer.is_empty()) {
          const n = pending_put_values.length;
          assert?.(n === 0, `∅ buffer but ${n} pending puts `);
          return new Promise(resolve => pending_takes.push(resolve));
        }

        const value = buffer.pop();
        if (is_not_empty(pending_put_values)) {
          const resolve = pending_put_resolves.shift();
          const value = pending_put_values.shift();
          buffer.push(value);
          resolve();
        }
        return value;
      },
    };
  }

  // ASSUMES this is called during a versionchange transaction
  /**
   * @param db {IDBDatabase}
   * @param name {string}
   * @param description {IDBStoreDescription}
   */
  function create_store_from(db, name, description) {
    const { keyPath, autoIncrement, indexes } = description;
    // we could create the store with neither a key nor autoincrement
    // but we won't be able to use our I/O with it (which has no ID channel)
    if (keyPath == null && autoIncrement == null)
      throw new Error(`Out-of-line keys are not supported`);
    const store = db.createObjectStore(name, { keyPath, autoIncrement });
    if (indexes) {
      for (const index_name of keys(indexes)) {
        const spec = indexes[index_name];
        store.createIndex(index_name, spec.keyPath, spec);
      }
    }
    return store;
  }

  // ASSUMES this is called during a versionchange transaction
  /**
   * @param store {IDBObjectStore}
   * @param description {IDBStoreDescription}
   */
  function update_store_from(store, description) {
    const upgrades = [...diff_store(store, description)];
    for (const difference of upgrades) {
      if (difference.type === "missing") {
        const { index: index_name, description } = difference;
        const { keyPath, ...options } = description;
        store.createIndex(index_name, keyPath, options);
      } else throw new Error(`Not implemented: ${difference.type} diffs`);
    }
  }

  /**
   * @param store {IDBObjectStore}
   * @param description {IDBStoreDescription}
   */
  function* diff_store(store, description) {
    const { keyPath, autoIncrement, indexes } = description;

    if (
      store.autoIncrement !== (autoIncrement ?? false) ||
      stringify(store.keyPath) !== stringify(keyPath ?? null)
    )
      yield { type: "pk" };

    if (indexes) {
      for (const name of keys(indexes)) {
        const spec = indexes[name];
        if (!store.indexNames.contains(name)) {
          yield { type: "missing", index: name, description: spec };
          continue;
        }
        const index = store.index(name);
        if (
          index.unique !== (spec.unique ?? false) ||
          index.multiEntry !== (spec.multiEntry ?? false) ||
          stringify(index.keyPath) !== stringify(spec.keyPath)
        )
          yield { type: "index", index: name, description: spec };
      }
    }
  }

  const gate = async_queue({ max_concurrent: 1 });

  /**
   * The *only* place to open a connection.
   *
   * @param db_name {string}
   * @param version {number | undefined}
   * @param upgrade {((db: IDBDatabase, txn: IDBTransaction) => void) | undefined}
   * @returns {Promise<IDBDatabase>}
   */
  async function connect(db_name, version, upgrade) {
    return gate(() => {
      return new Promise(async (resolve, reject) => {
        await sleep(1); // avoid `versionchange` race condition in Chrome
        const request = indexedDB.open(db_name, version);
        request.onerror = error => reject(`${error}`);
        request.onblocked = e => {
          reject(`BLOCKED → ${e.newVersion}`);
        };
        request.onsuccess = () => {
          const db = request.result;
          db.addEventListener("versionchange", () => {
            db.close(); // the closest thing to a best practice in this API
          });
          resolve(db);
        };
        if (typeof upgrade === "function") {
          request.onupgradeneeded = () => {
            upgrade(request.result, request.transaction);
          };
        }
      });
    });
  }

  const target_versions = new Map();

  /**
   * Resolve to an open connection to the indicated database in which the
   * indicated store was definitely configured as described when you left it.
   *
   * @param db_name {string}
   * @param store_name {string}
   * @param description {IDBStoreDescription}
   * @returns {Promise<IDBDatabase>}
   */
  async function database_with(db_name, store_name, description) {
    // If you open at default version & get upgrade, the database didn't exist
    let created = false;
    const existing_db = await connect(db_name, undefined, db => {
      created = true;
      create_store_from(db, store_name, description);
    });
    if (created) return existing_db; // since we know we configured store

    // The database existed already
    if (existing_db.objectStoreNames.contains(store_name)) {
      const txn = existing_db.transaction(store_name);
      const store = txn.objectStore(store_name);
      const [any_difference] = diff_store(store, description);
      if (!any_difference) return existing_db;
    }

    // The store didn't exist or needs configuration
    existing_db.close();
    const last_version = target_versions.get(db_name) ?? existing_db.version;
    const next_version = last_version + 1;
    target_versions.set(db_name, next_version);

    return connect(db_name, next_version, (db, txn) => {
      if (db.objectStoreNames.contains(store_name)) {
        const store = txn.objectStore(store_name);
        update_store_from(store, description);
      } else {
        create_store_from(db, store_name, description);
      }
    });
  }

  /**
   * Resolve to a store in the indicated database that has the described
   * configuration, attached to a transaction in the indicated mode.
   *
   * @param db_name {string}
   * @param store_name {string}
   * @param description {IDBStoreDescription}
   * @param mode {Exclude<IDBTransactionMode, "versionchange">}
   * @returns {Promise<IDBObjectStore>}
   */
  async function get_store_with(db_name, store_name, description, mode) {
    const db = await database_with(db_name, store_name, description);
    const txn = db.transaction(store_name, mode); // NEW TRANSACTION
    return txn.objectStore(store_name);
  }

  // need cursor reader at least for backwards traversal
  // can do paginated for forwards smh
  // what a cluster https://github.com/w3c/IndexedDB/issues/130

  /**
   * Feed a cursor into a given channel.
   *
   * @param thunk {() => Promise<IDBRequest<IDBCursor>>}
   * @param channel {ReturnValue<typeof make_channel>}
   */
  async function pipe_cursor_to(thunk, channel, is_index) {
    let resolve, reject, cursor;

    function make_new_cursor_request() {
      const request = thunk();
      // have yet to see this happen though & most don't apply to cursors
      request.onerror = error => reject(`${error}`);
      request.onsuccess = event => resolve(event.target.result);
      return request;
    }

    const next_read = () =>
      new Promise((_resolve, _reject) => {
        resolve = _resolve;
        reject = _reject;
      });

    make_new_cursor_request();
    while (true) {
      cursor = await next_read();
      if (!cursor) break;
      const { value } = cursor; // HOTSPOT
      // remember in case we need to resume
      const frontier = { key: cursor.key, pk: cursor.primaryKey }; // HOTSPOT
      if (channel.can_put_now()) {
        channel.put(value);
        cursor.continue(); // HOTSPOT
        continue;
      }
      await channel.put(value);
      if (cursor.request.readyState !== "done") {
        try {
          cursor.continue(); // HOTSPOT
          return;
        } catch (error) {
          if (error.name !== "TransactionInactiveError") throw error;
        }
      }
      make_new_cursor_request();
      cursor = await next_read();
      if (!cursor) break;
      if (is_index) {
        cursor.continuePrimaryKey(frontier.key, frontier.pk);
      } else {
        cursor.continue(frontier.key);
      }
      // we returned to the item we already put, so ignore & move to the next
      if (!(await next_read())) break;
      cursor.advance(1);
    }
    channel.put(DONE);
  }

  // unlike write, this is not responsible for the db/store existing
  // “batching” here amounts to read-ahead, which keeps txn alive between yields
  // still yields single records
  const DONE = Symbol("done");
  async function* read(spec) {
    const trace = false ? (...x) => console.debug(`ℝ`, ...x) : null;
    const db_name = spec.db;
    const store_name = spec.store;
    const index_name = spec.index;
    const batch_size = spec.batch_size ?? 16;
    const __buffer = make_ring_buffer(batch_size); // don't reference this though
    const channel = make_channel(__buffer);

    let connection = await connect(db_name);

    function make_cursor_request() {
      trace?.(`creating new transaction & everything`);
      const transaction = connection.transaction(store_name); // NEW TRANSACTION
      const store = transaction.objectStore(store_name);
      const source = index_name ? store.index(index_name) : store;
      const request = source.openCursor(spec.query, spec.dir);
      return request;
    }

    const is_index = !!index_name; // TEMP
    pipe_cursor_to(make_cursor_request, channel, is_index);

    // a generic channel consume
    try {
      while (true) {
        const result = channel.can_take_now()
          ? channel.take()
          : await channel.take();
        if (result === DONE) break;
        yield result;
      }
    } finally {
      connection.close();
    }
  }

  async function* read_batched(spec) {
    const { db, store } = spec;
    const limit = spec.limit ?? 16;
    const __buffer = make_ring_buffer(limit); // don't reference this though
    const channel = make_channel(__buffer);

    let _connection = await connect(db);
    let _last_key;

    async function read_next_batch() {
      // maybe check connection
      const source = { db, store };
      let query = undefined;
      // this is not quite right, though... what if you're not on the PK?
      // you also need to do 2 calls to know whether there are more,
      // if the result equals the batch size. so how would you resume in that case?
      if (_last_key) query = IDBKeyRange.lowerBound(_last_key, true);
      const batch = await get_all(source, query, limit, _connection);
    }
    while (true) {
      const batch = await read_next_batch();
      if (!batch) break;
      _last_key = batch.at(-1);
      yield batch;
    }

    try {
      while (true) {
        const result = channel.can_take_now()
          ? channel.take()
          : await channel.take();
        if (result === DONE) break;
        yield result;
      }
    } finally {
      _connection.close();
    }
  }

  async function* write(source, destination, options) {
    const trace = false ? (...x) => console.debug("WRITE", ...x) : null;
    if (!source) throw new Error(`Write requires a source!`);
    if (!destination) throw new Error(`Write requires a destination!`);
    const { database: db, store: store_name, description } = destination;
    if (!description) throw new Error(`Destination should have a description!`);
    let size = options?.batch_size ?? 16;
    assert?.(typeof size === "number", `Write got a ${typeof size} batch size`);
    const store = await get_store_with(
      db,
      store_name,
      description,
      "readwrite"
    );
    let total_written = 0;
    let written_this_round = 0;
    let errors = null;
    try {
      // don't we need to do this at some point?  and check success?
      // store.transaction.commit();
      for (const item of source) {
        trace?.("item", item);
        try {
          store.add(item);
        } catch (error) {
          if (
            error.type === "TransactionInactiveError" ||
            error.toString().startsWith("TransactionInactiveError")
          ) {
            trace?.("caught TransactionInactiveError", error);
            // okay now what?
            throw error;
          }
          errors ??= [];
          errors.push(`${error}`);
          // shouldn't we still count this against size?
          continue;
        }
        written_this_round++;
        trace?.(`${written_this_round} written this round`);
        total_written++;
        if (written_this_round >= size) {
          const status = { total_written, written_this_round, errors };
          trace?.(`in loop yielding and resetting`, status);
          yield status;
          written_this_round = 0;
          errors = null;
        }
      }
      if (written_this_round > 0 || errors.length > 0) {
        const final_status = { total_written, written_this_round, errors };
        trace?.(`yielding final status`, final_status);
        yield final_status;
      }
    } finally {
      store.transaction.db.close();
      trace?.(`closed connection`);
    }
  }

  async function get_by_key(spec, key) {
    const connection = await connect(spec.db, undefined);
    const transaction = connection.transaction(spec.store);
    const store = transaction.objectStore(spec.store);
    const source = spec.index ? store.index(spec.index) : store;
    const request = source.get(key);
    return new Promise((resolve, reject) => {
      request.onerror = reject;
      request.onsuccess = () => resolve(request.result);
    });
  }

  async function get_all(view, query, limit, connection) {
    connection ??= await connect(view.db, undefined);
    const transaction = connection.transaction(view.store);
    const store = transaction.objectStore(view.store);
    const source = view.index ? store.index(view.index) : store;
    const request = source.getAll(query, limit);
    return new Promise((resolve, reject) => {
      request.onerror = reject;
      request.onsuccess = () => resolve(request.result);
    });
  }

  async function count(spec) {
    const connection = await connect(spec.db);
    const transaction = connection.transaction(spec.store);
    const store = transaction.objectStore(spec.store);
    const source = spec.index ? store.index(spec.index) : store;
    const request = source.count(spec.query);
    return new Promise((resolve, reject) => {
      request.onerror = error => reject(`${error}`);
      request.onsuccess = () => resolve(request.result);
    });
  }

  const io = { read, write };
  const expedient = { get_by_key, get_all, count };
  const lldb = { async_queue, database_with, diff_store, connect }; // mainly for debug
  const proc = { make_ring_buffer, make_channel }; // for testing
  const IDB = { ...io, ...lldb, ...proc, ...expedient };
  Object.assign(globalThis, { IDB });
})();

function assert_equals(a, b, ...messages) {
  if (messages.length) console.log(...messages);
  const diffs = [...diff_any(a, b)];
  if (diffs.length === 0) {
    console.info(`Results match!`);
  } else {
    console.error(`Results do not match!`);
    console.group(`Diffs:`);
    for (const { type, path, ...diff } of diffs)
      console.log(`${type} @ ${path.join(".")}`, diff);
    console.groupEnd();
  }
}

// ===Diffing tools
const { is, keys } = Object;
const { isArray } = Array;

function* diff_objects(a, b, path = []) {
  if (a === null || b === null) {
    if (a !== null || b !== null) yield { type: "null", path, a, b };
    return;
  }
  if (isArray(a) !== isArray(b)) {
    yield { type: "array", path, a: isArray(a), b: isArray(b) };
    return;
  }
  const a_keys = isArray(a) ? [...a.keys()] : keys(a);
  const b_keys = isArray(b) ? [...b.keys()] : keys(b);
  if (a_keys.length !== b_keys.length) {
    yield { type: "key#", path, a: a_keys, b: b_keys };
  }
  for (const key of a_keys) {
    if (!b.hasOwnProperty(key)) yield { type: "key-", path, key };
  }
  for (const key of b_keys) {
    if (!a.hasOwnProperty(key)) yield { type: "key+", path, key };
  }
  for (const key of a_keys) {
    const a_item = a[key];
    const b_item = b[key];
    yield* diff_any(a_item, b_item, [...path, key]);
  }
}

function* diff_any(a, b, path = []) {
  if (is(a, b)) return;
  const a_type = typeof a;
  const b_type = typeof b;
  if (a_type !== b_type) {
    yield { type: "typeof", path, a: a_type, b: b_type };
    return;
  }
  if ((a === null) !== (b === null)) {
    yield { type: "null", path, a, b };
    return;
  }
  switch (a_type) {
    case "undefined":
    case "boolean":
    case "number":
    case "bigint":
    case "string":
    case "symbol":
      yield { type: "value", path, a, b }; // we already checked equals
      break;
    case "object":
      yield* diff_objects(a, b, path);
      break;
    case "function": {
      throw new Error(`Not supported: diffing function`);
    }
  }
}

(function test_catalog() {
  const { assert } = console;
  const { read, write } = IDB;

  const { stringify } = JSON;
  const cheap_equals = (a, b) => stringify(a) == stringify(b);

  // actually we have diff functions now...
  function assert_cheap_equals(a, b, ...messages) {
    if (cheap_equals(a, b)) return;
    console.error(...messages);
    throw new Error(`${a} ≠ ${b}`);
  }

  const sleep = ms => new Promise(r => setTimeout(r, ms));

  // US states example data
  const US_STATES = {
    keyPath: "fips",
    indexes: [
      ["name", "name", { unique: true }],
      // this is an ISO date, where do we say that?
      ["joined", "joined", { unique: false }],
    ],
    object: {
      AL: { name: "Alabama", joined: "1819-12-14" },
      AK: { name: "Alaska", joined: "1959-01-03" },
      AZ: { name: "Arizona", joined: "1912-02-14" },
      AR: { name: "Arkansas", joined: "1836-06-15" },
      CA: { name: "California", joined: "1850-09-09" },
      CO: { name: "Colorado", joined: "1876-08-01" },
      CT: { name: "Connecticut", joined: "1788-01-09" },
      DE: { name: "Delaware", joined: "1959-01-03" },
      FL: { name: "Florida", joined: "1845-03-03" },
      GA: { name: "Georgia", joined: "1788-01-02" },
      HI: { name: "Hawaii", joined: "1959-01-03" },
      ID: { name: "Idaho", joined: "1890-07-03" },
      IL: { name: "Illinois", joined: "1818-12-03" },
      IN: { name: "Indiana", joined: "1816-12-11" },
      IA: { name: "Iowa", joined: "1846-12-28" },
      KS: { name: "Kansas", joined: "1861-01-29" },
      KY: { name: "Kentucky", joined: "1792-06-01" },
      LA: { name: "Louisiana", joined: "1812-04-30" },
      ME: { name: "Maine", joined: "1820-03-15" },
      MD: { name: "Maryland", joined: "1788-04-28" },
      MA: { name: "Massachusetts", joined: "1788-01-02" },
      MI: { name: "Michigan", joined: "1837-01-26" },
      MN: { name: "Minnesota", joined: "1858-05-11" },
      MS: { name: "Mississippi", joined: "1817-12-10" },
      MO: { name: "Missouri", joined: "1821-08-10" },
      MT: { name: "Montana", joined: "1889-11-08" },
      NE: { name: "Nebraska", joined: "1867-03-01" },
      NV: { name: "Nevada", joined: "1864-10-31" },
      NH: { name: "New Hampshire", joined: "1788-06-21" },
      NJ: { name: "New Jersey", joined: "1787-12-18" },
      NM: { name: "New Mexico", joined: "1912-01-06" },
      NY: { name: "New York", joined: "1788-07-26" },
      NC: { name: "North Carolina", joined: "1789-11-21" },
      ND: { name: "North Dakota", joined: "1889-11-02" },
      OH: { name: "Ohio", joined: "1803-03-01" },
      OK: { name: "Oklahoma", joined: "1907-11-16" },
      OR: { name: "Oregon", joined: "1819-12-14" },
      PA: { name: "Pennsylvania", joined: "1819-12-14" },
      PR: { name: "Puerto Rico", joined: "1820-03-15" },
      RI: { name: "Rhode Island", joined: "1790-05-29" },
      SC: { name: "South Carolina", joined: "1788-05-23" },
      SD: { name: "South Dakota", joined: "1889-11-02" },
      TN: { name: "Tennessee", joined: "1796-06-01" },
      TX: { name: "Texas", joined: "1845-12-29" },
      UT: { name: "Utah", joined: "1896-01-04" },
      VT: { name: "Vermont", joined: "1791-03-04" },
      VA: { name: "Virginia", joined: "1819-12-14" },
      WA: { name: "Washington", joined: "1889-11-11" },
      WV: { name: "West Virginia", joined: "1863-06-20" },
      WI: { name: "Wisconsin", joined: "1848-05-29" },
      WY: { name: "Wyoming", joined: "1890-07-10" },
    },
    *read(object) {
      for (const [fips, rec] of Object.entries(object)) yield { fips, ...rec };
    },
  };

  async function ensure_test_data() {
    const place = { db: "test-facts", store: "us-states" };
    const source = [...US_STATES.read(US_STATES.object)];
    const database = place.db;
    const store = place.store;
    /** @type IDBStoreDescription */
    const description = {
      keyPath: "fips",
      indexes: { joined: { keyPath: "joined" } },
    };
    const destination = { database, store, description };
    const writer = write(source, destination);
    for await (const status of writer)
      console.debug("loading test data", status);
  }

  const BASIC_READER_TESTS = [
    {
      spec: {},
      expect: `AK AL AR AZ CA CO CT DE FL GA HI IA ID IL IN KS KY LA MA MD ME MI MN MO MS MT NC ND NE NH NJ NM NV NY OH OK OR PA PR RI SC SD TN TX UT VA VT WA WI WV WY`,
    },
    {
      spec: { index: "joined" },
      expect:
        "NJ GA MA CT MD SC NH NY NC RI VT KY TN OH LA IN MS IL AL OR PA VA ME PR MO AR MI FL TX IA WI CA MN KS WV NV NE CO ND SD MT WA ID WY UT OK NM AZ AK DE HI",
    },
    {
      label: "bound",
      spec: { query: IDBKeyRange.bound("G", "N") },
      expect: `GA HI IA ID IL IN KS KY LA MA MD ME MI MN MO MS MT`,
    },
    {
      label: "index + bound",
      spec: { query: IDBKeyRange.lowerBound("1900"), index: "joined" },
      expect: `OK NM AZ AK DE HI`,
    },
    {
      label: "upper bound",
      spec: { query: IDBKeyRange.upperBound("B") },
      expect: "AK AL AR AZ",
    },
    {
      label: "larger bound",
      spec: { query: IDBKeyRange.upperBound("L") },
      expect: "AK AL AR AZ CA CO CT DE FL GA HI IA ID IL IN KS KY",
    },
    {
      label: "upper bound, early return",
      spec: { query: IDBKeyRange.upperBound("B") },
      limit: 3,
      expect: "AK AL AR",
    },
  ];

  async function test_read() {
    const read = IDB.read;
    const TEST_DB = { db: "test-facts", version: 1 };
    const US_STATES = { ...TEST_DB, store: "us-states" };

    await ensure_test_data();

    async function test(title, thunk, open) {
      console[open ? "group" : "groupCollapsed"](title);
      // prettier-ignore
      try { await thunk() }
      catch (error) { console.error(error) }
      finally { console.groupEnd() }
    }
    // duplicated
    async function* async_take(seq, n) {
      for await (const item of seq) {
        if (n-- <= 0) break;
        yield item;
      }
    }

    async function basic_reader_test(what) {
      const { expect, pause, ...rest } = what;
      const { skip, query, limit, spec } = rest;
      const label = `pause ${pause}; ${spec.label ?? JSON.stringify(rest)}`;
      if (skip) return console.info(`SKIPPED!!! ${label}`);
      return test(label, async () => {
        const reader = read({ ...US_STATES, ...spec });
        const results = [];
        const source = limit === undefined ? reader : async_take(reader, limit);
        for await (const item of source) {
          console.debug(`Test got`, item.fips);
          if (pause) await sleep(typeof pause === "number" ? pause : 1);
          results.push(item);
        }
        console.log(`results`, results);
        const fips = results.map(x => x.fips);
        assert_equals(fips, expect.split(" "));
        // const got = fips.join(" ");
        // assert?.(got === expect, `Expected ${expect}; got ${got}`);
      });
    }

    for (const spec of BASIC_READER_TESTS)
      for (const pause of [false, 1, 100])
        await basic_reader_test({ pause, ...spec });

    console.log("All tests complete!");
  }

  const ROUNDTRIP_CASES = [
    { given: [{ a: 11 }, { a: 42 }], description: { keyPath: "a" } },
    {
      skip: `the read/write doesn't support out-of-line keys`,
      given: Array.from([8, 6, 7, 5, 3, 0, 9], a => ({ a })),
      description: { autoIncrement: true },
    },
    {
      comment: `read back in order by primary key`,
      given: Array.from([8, 6, 7, 5, 3, 0, 9], a => ({ a })),
      description: { keyPath: "a" },
      expect: Array.from([0, 3, 5, 6, 7, 8, 9], a => ({ a })),
    },
    {
      comment: `read back in order by autoincrementing key`,
      given: Array.from([8, 6, 7, 5, 3, 0, 9], b => ({ b })),
      description: { keyPath: "a", autoIncrement: true },
      expect: Array.from([8, 6, 7, 5, 3, 0, 9], (b, i) => ({ a: i + 1, b })),
    },
  ];

  async function test_roundtrip() {
    const any_only = Object.values(ROUNDTRIP_CASES).some(_ => _.only);
    if (any_only) console.info("Running only tests marked with `only`!");
    for (const spec of ROUNDTRIP_CASES) {
      if (spec.skip) {
        console.warn(`Skipping because`, spec.skip);
        continue;
      }
      if (any_only && !spec.only) continue;
      for (const batch_size of [undefined, 4]) {
        const options = { ...spec.options, batch_size };
        await test_roundtrip_case({ ...spec, options });
      }
    }

    console.info("Roundtrip tests complete");
  }

  async function test_roundtrip_case(test_spec) {
    const read = IDB.read;
    const { given, description, options, expect } = test_spec;
    const say = console.debug.bind(console, "TEST");
    say?.("Roundtrip test ===========================");

    // === write
    const now = new Date();
    const ticks = now.getTime();
    const iso = now.toISOString();
    const date = iso.substring(0, 10);
    const database = `test-${date}`;
    const store = `test-${ticks}-${Math.round(Math.random() * 1000)}`;
    const destination = { database, store, description };
    let total;
    say?.(`create writer`);
    const writer = write(given, destination, options);
    say?.(`begin iteration`);
    for await (const result of writer) {
      say?.(`write result:`, result);
      total = result.total_written;
    }
    assert?.(total === given.length, `reported ${total} vs ${given.length}`);

    // === now read back and ensure that you got the original records
    const read_back = [];
    const spec = { db: database, store };
    say?.(`read back...`);
    for await (const result of read(spec)) {
      say?.(`read result!`, result);
      // if (result.data) read_back.push(...result.data);
      read_back.push(result);
    }
    // check the result
    assert_equals(read_back, expect ?? given, `Read result`);
    /*
    if (description.autoIncrement) {
      console.warn(`Errr, need more work to check autoincrement`);
    } else {
      assert?.(
        JSON.stringify(read_back) === JSON.stringify(expect ?? given),
        `not the same`
      );
    }
*/
  }

  async function test_database_with(db_name, store_name, description) {
    let db;
    try {
      db = await IDB.database_with(db_name, store_name, description);
      db.transaction(store_name);
      assert_cheap_equals(db.name, db_name, `db name`);
      const has_store = db.objectStoreNames.contains(store_name);
      assert(has_store, `${store_name} ∉ ${db_name}`);
      const txn = db.transaction(store_name);
      const store = txn.objectStore(store_name);
      // note this makes no *negative* assertions
      const diffs = [...IDB.diff_store(store, description)];
      assert_cheap_equals(diffs.length, 0, `diffs`, ...diffs);
      console.debug(
        `completed test for ${db_name}:${store_name} ${stringify(description)}`
      );
    } catch (error) {
      console.error(
        `error in test for ${db_name}:${store_name} ${stringify(description)}`,
        error
      );
    } finally {
      if (db) {
        console.debug("Test CLOSING db, version", db.version);
        db.close();
      }
    }
  }

  /** @type {readonly (readonly [string, IDBStoreDescription])[]} */
  const DATABASE_WITH_CASES = [
    ["x", { keyPath: "a", indexes: { b: { keyPath: "b" } } }],
    ["x", { keyPath: "a", indexes: { c: { keyPath: "c" } } }],
    ["x", { keyPath: "a", indexes: { b: { keyPath: "b" } } }],
    ["x", { keyPath: "a", indexes: { d: { keyPath: "d" } } }],

    ["x", { keyPath: "a", indexes: { e: { keyPath: "e" } } }],
    ["x", { keyPath: "a", indexes: { f: { keyPath: "f" } } }],
    ["x", { keyPath: "a", indexes: { g: { keyPath: "g" } } }],
    ["statements", { autoIncrement: true }],
    ["statements", { autoIncrement: true }],
    ["statements", { autoIncrement: true }],
    ["statements", { autoIncrement: true, indexes: { s: { keyPath: "s" } } }],
    [
      "statements",
      {
        autoIncrement: true,
        indexes: { s: { keyPath: "s" }, p: { keyPath: "p" } },
      },
    ],
    ["statements", { autoIncrement: true, indexes: { s: { keyPath: "s" } } }],
    [
      "statements",
      {
        autoIncrement: true,
        indexes: {
          s: { keyPath: "s" },
          p: { keyPath: "p" },
          o: { keyPath: "o" },
        },
      },
    ],
    // Note this doesn't remove the earlier indexes!
    [
      "statements",
      {
        autoIncrement: true,
        indexes: {
          spo: { keyPath: ["s", "p", "o"] },
          pos: { keyPath: ["p", "o", "s"] },
        },
      },
    ],
    ["nooks", { keyPath: "id" }],
    ["nooks", { keyPath: "id" }],
    ["crannies", { keyPath: ["a", "id"] }],
    ["nooks", { keyPath: "id" }],
    ["crannies", { keyPath: ["a", "id"] }],
    ["merps", { autoIncrement: true, indexes: { who: { keyPath: "what" } } }],
  ];

  async function database_with_tests() {
    const random = Math.round(Math.random() * 1e12);
    const db_name = `test-db-with-${random}`;
    for (const [store_name, description] of DATABASE_WITH_CASES) {
      test_database_with(db_name, store_name, description);
    }
    console.info(`Launched dem tests`);
  }

  async function self_tests() {
    // await database_with_tests();
    await test_roundtrip();
    await test_read();
  }

  Object.assign(IDB, { self_tests });
})();

const { assert } = console;

{
  const { fromEntries, entries } = Object;
  function map_object(o, f) {
    return fromEntries(Array.from(entries(o), ([k, v]) => [k, f(v, k, o)]));
  }
}

// yes, for ad-hoc date stuff.  copped from ISO8601
const pad = len => n =>
  (n < 0 ? "-" : "") + Math.abs(n).toString().padStart(len, "0");
const pad4 = pad(4);
const pad2 = pad(2);

// ==== process primitives

{
  const NOTHING = Symbol("∅");
  function make_subscribable() {
    let _value = NOTHING;
    const subscribers = new Set();

    function subscribe(subscriber) {
      subscribers.add(subscriber);
      function unsubscribe() {
        subscribers.delete(subscriber);
      }
      const subscription = { unsubscribe };
      return subscription;
    }

    // 1. this operation MUST be synchronous
    // 2. one subscriber MUST NOT be able to break others
    // 3. errors MUST NOT be lost
    // 4. we SHOULD NOT have to look at subscriber.catch unless there's an error
    // 5. error SHOULD appear (in debugger) where it occurs
    //
    // This achieves all but #5.  In chrome & FF, error appears at re-throw
    //
    // In fact these are incompatible.  The only way the error can be thrown at
    // the original location is if it is not caught at all.  But an uncaught
    // error will break any loop where it occurs.  If you define “synchronous”
    // to mean “by the next tick” rather than “by the end of this call”, then
    // you could do all `next` calls from microtasks (without try/catch when
    // there is no subscriber catch), but that would incur high overhead.
    function next(value) {
      _value = value;
      let errors = null;
      for (const subscriber of subscribers) {
        try {
          subscriber.next(value);
        } catch (error) {
          if (typeof subscriber.catch === "function") subscriber.catch(error);
          else {
            if (errors === null) errors = [error];
            else errors.push(error);
          }
        }
      }
      if (errors !== null) {
        console.error("uhandled errors in broadcast", ...errors);
        // This throws all errors without resorting to console, but it's
        // actually worse .  The console at least links you to the source.
        /*
        for (const error of errors) {
          queueMicrotask(() => {
            throw error;
          });
        }
        */
      }
    }
    function has_cache() {
      return _value !== NOTHING;
    }
    function cache() {
      return _value;
    }
    const public = { subscribe };
    return { public, next, has_cache, cache };
  }

  // this is not quite right, and not quite what I want
  // what I want is something that will always process the last one after delay
  function throttle_subscribable(subscribable, length) {
    const outer = make_subscribable();
    let _last_seen = NaN;
    // BUT you need to kill this when outer is done
    const inner = subscribable.subscribe({
      next(value) {
        const now = performance.now();
        if (isNaN(_last_seen) || now - _last_seen > length) outer.next(value);
        // else console.debug("THROTTLING");
        _last_seen = now;
      },
    });
    return outer.public;
  }
}

// https://github.com/tc39/proposal-regex-escaping/blob/main/polyfill.js
// seeAlso https://github.com/tc39/proposal-regex-escaping/issues/37
// This is safe for whole patterns.  The objections are to certain joins.
const RegExp_escape = s => String(s).replace(/[\\^$*+?.()|[\]{}]/g, "\\$&");
// The grouping isn't needed as long as all parts are simple
// const RegExp_wrap = pattern => `(?:${pattern})`;
const RegExp_wrap = pattern => pattern;
const RegExp_alternatives = patterns =>
  Array.from(patterns, RegExp_wrap).join("|");

// async helpers

const sleep = ms => new Promise(r => setTimeout(r, ms));

async function* async_map(seq, f) {
  for await (const item of seq) yield f(item);
}
async function* async_filter(seq, p) {
  for await (const item of seq) if (p(item)) yield item;
}
async function* async_take(seq, n) {
  for await (const item of seq) {
    if (n-- <= 0) break;
    yield item;
  }
}

async function* async_merge_roundrobin(seqs) {
  if (seqs.length === 0) return;
  if (seqs.length === 1) seqs[0];
  const gens = Array.from(seqs, seq => seq[Symbol.asyncIterator]());

  while (true) {
    for (let i = gens.length - 1; i >= 0; i--) {
      const gen = gens[i];
      const { done, value } = await gen.next();
      if (done) gens.splice(i, 1);
      else yield value;
    }
    if (gens.length === 0) break;
  }
  // stop remaining? i.e.
  for (const gen of gens) gen.return();
}

// === markup tools

const ident = x => x;
function* match_and_mark_text(rex, text, map, doc) {
  const f = map ?? ident;
  if (!rex) {
    if (text) yield f(text);
    return;
  }
  let pos = 0;
  if (!text) return;
  for (const match of text.matchAll(rex)) {
    const sep = text.substring(pos, match.index);
    if (sep) yield f(sep);
    const mark = doc.createElement("mark");
    const [needle] = match;
    mark.textContent = f(needle);
    yield mark;
    pos = match.index + needle.length;
  }
  if (pos === 0) yield f(text);
  else if (pos < text.length) {
    const tail = text.substring(pos);
    if (tail) yield f(tail);
  }
}
// helper for below, from ./dom-stepper.js
function preorder_next(/** @type Node */ my) {
  if (!my) return;
  if (my.firstChild) return my.firstChild;
  do if (my.nextSibling) return my.nextSibling;
  while ((my = my.parentNode));
}
// mark matches on an element's text in-place
// Won't hit the unhandled cases b/c *predicates* don't match across markup...
/** @param rex {RegExp} */
function match_and_mark_html(rex, /** @type Element */ element, doc) {
  if (!rex) return;
  // THIS is schlemel the painter. could use state since we go only forward
  function slice_containing(index) {
    let node = element;
    let pos = 0;
    while (node) {
      if (node instanceof Text) {
        const len = node.textContent.length;
        if (pos + len > index) return { node, offset: index - pos };
        pos += len;
      }
      node = preorder_next(node);
    }
  }
  for (const match of element.textContent.matchAll(rex)) {
    const start_index = match.index;
    const end_index = match.index + match.length - 1;
    const start = slice_containing(start_index);
    const end = slice_containing(end_index);
    assert?.(start, `mark html expected to find start`);
    assert?.(end, `mark html expected to find end`);
    if (start.node === end.node) {
      const node_text = start.node.textContent;
      const match_text = match[0];
      const mark = doc.createElement("mark");
      mark.textContent = match_text;
      const text_before = node_text.substring(0, start.offset);
      const text_after = node_text.substring(end.offset + match_text.length);
      start.node.replaceWith(text_before, mark, text_after);
    } else if (start.node.parentElement === end.node.parentElement) {
      // a single mark spanning the html
      throw new Error(`Not implemented: match including markup`);
    } else {
      // need multiple marks broken up over tree
      throw new Error(`Not implemented: oblique match`);
    }
  }
}

// === application stuff

const wiki_href = (wiki, lang) => `https://${lang}.wikipedia.org/wiki/${wiki}`;
const wiki_unslug = s => s.replace(/_/g, " ");

// kind of general time thing
function timestamp(date, lang, label) {
  const time = document.createElement("time");
  time.dateTime = date;
  time.textContent = label ?? ISO8601.label(date, lang);
  return time;
}

{
  // idea to support the dynamic alteration of precision in presented dates
  // trouble is, this doesn't consider how Intl would have arranged the parts
  const { parse, Date_from } = ISO8601;
  const UTC = { timeZone: "UTC" };
  // should memoize by language
  const DAY = new Intl.DateTimeFormat(undefined, { ...UTC, day: "numeric" });
  const MONTH = new Intl.DateTimeFormat(undefined, { ...UTC, month: "long" });
  const YEAR = new Intl.DateTimeFormat(undefined, { ...UTC, year: "numeric" });

  function structured_timestamp(date, lang) {
    const parsed = parse(date);
    if (!parsed) throw new Error(`structured timestamp got bad date: ${date}`);

    const stamp = document.createElement("time");
    const year = parsed.y && document.createElement("span");
    const month = parsed.mm && document.createElement("span");
    const day = parsed.dd && document.createElement("span");

    stamp.dateTime = date;

    const js_date = Date_from(date);
    if (year) {
      year.textContent = YEAR.format(js_date);
      stamp.append(year, " ");
    }
    if (month) {
      month.textContent = MONTH.format(js_date);
      stamp.append(month, " ");
    }
    if (day) {
      day.textContent = DAY.format(js_date);
      stamp.append(day, " ");
    }
    return stamp;
  }
}

function timestamp(date, lang, label) {
  const time = document.createElement("time");
  time.dateTime = date;
  time.textContent = label ?? ISO8601.label(date, lang);
  return time;
}

const KNOWN_CATALOG = {
  en_us_constitutional_amendments: {
    locale: "en",
    source: "wiki-us-amendments-en.js",
    xml_name: "XML_us_amendments",
    store: {
      keyPath: "number",
      indexes: {
        wiki: { keyPath: "wiki" },
        refs: { keyPath: "refs", multiEntry: true },
        proposed: { keyPath: "proposed" },
        completed: { keyPath: "completed" },
      },
    },
    from_dom(/** @type Element */ dom) {
      const eles = dom.querySelectorAll("us-constitution > amendment");
      return Array.from(eles, ele => {
        const p = ele.querySelector("description");
        console.assert(p, `No content element for item`, ele);
        return {
          wiki: ele.getAttribute("wiki"),
          number: parseInt(ele.getAttribute("number"), 10),
          proposed: ele.getAttribute("proposed"),
          completed: ele.getAttribute("completed"),
          refs: Array.from(p.querySelectorAll("a[wiki]"), _ =>
            _.getAttribute("wiki")
          ),
          description: p.innerHTML,
        };
      });
    },
  },
  en_us_presidents: {
    locale: "en",
    source: "wiki-us-presidents-en.js",
    xml_name: "XML_us_presidents",
    store: {
      keyPath: "number",
      indexes: {
        incumbent: { keyPath: "incumbent" },
        termStart: { keyPath: "termStart" },
        termEnd: { keyPath: "termEnd" },
        electionDates: { keyPath: "electionDates", multiEntry: true },
        party: { keyPath: "party" },
      },
    },
    from_dom(/** @type Element */ dom) {
      const eles = dom.querySelectorAll("us-presidents > term");
      return Array.from(eles, ele => {
        return {
          number: parseInt(ele.getAttribute("number"), 10),
          incumbent: ele.getAttribute("incumbent"),
          termStart: ele.getAttribute("term-start"),
          termEnd: ele.getAttribute("term-end"),
          portrait: ele.getAttribute("portrait"),
          party: ele.getAttribute("party"),
          // in addition to elections because we can index on this
          electionDates: Array.from(ele.querySelectorAll("election"), node =>
            node.getAttribute("dates")
          ),
          elections: Array.from(ele.querySelectorAll("election"), node => {
            const ref = node.getAttribute("ref");
            const date = node.getAttribute("dates");
            return { ref, date };
          }),
        };
      });
    },
  },
  en_wiki_events: {
    locale: "en",
    source: "wiki-events-en.js",
    xml_name: "XML_events",
    store: {
      keyPath: "id",
      autoIncrement: true,
      indexes: {
        aboutTime: { keyPath: "aboutTime" },
        topics: { keyPath: "topics", multiEntry: true },
        refs: { keyPath: "refs", multiEntry: true },
      },
    },
    // This should be done lazily too, there's no need to make all at once
    from_dom(/** @type Element */ dom) {
      const eles = dom.querySelectorAll("events > event");
      return Array.from(eles, ele => {
        const p = ele.querySelector("p");
        console.assert(p, `No content element for item`, ele);
        return {
          aboutTime: ele.getAttribute("date"),
          topics: Array.from(ele.querySelectorAll("topic"), _ => _.textContent),
          refs: Array.from(p.querySelectorAll("a[wiki]"), _ =>
            _.getAttribute("wiki")
          ),
          html: p.innerHTML,
        };
      });
    },
  },
  en_wiki_persons: {
    locale: "en",
    source: "wiki-persons-en.js",
    xml_name: "XML_persons",
    store: {
      keyPath: "wiki",
      indexes: {
        born: { keyPath: "born", unique: false },
        died: { keyPath: "died", unique: false },
        // "words", "words", { multiEntry: true }
      },
    },
    from_dom(/** @type Element */ dom) {
      const eles = dom.querySelectorAll("persons > person");
      return Array.from(eles, ele => ({
        wiki: ele.getAttribute("who"),
        summary: ele.getAttribute("summary"),
        born: ele.getAttribute("born"),
        died: ele.getAttribute("died"),
      }));
    },
  },
};

const WORD_BOUNDARY = /[-a-z0-9']+/i; // hyphens, yea or nay?
const SLUG_WORD_BOUNDARY = /_|\W+/g;
const words_in = text => text.split(WORD_BOUNDARY);

const with_words = p => ({ ...p, words: words_in(`${p.wiki} ${p.summary}`) });

// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
// Concepts

// TODO: use binary search
function first_child_after(date, container) {
  for (const child of container.children) {
    const other = child.dataset.time;
    if (other && ISO8601.compare(other, date) > 0) return child;
  }
}

{
  const { next } = ISO8601;
  function timeframe_bounding_dates(date) {
    return date.includes("/") ? date.split("/", 2) : [date, next(date)];
  }
}

function make_time_backdrop() {
  const doc = document;
  const segments = new Map();
  const element = doc.createElement("div");
  const backdrop = doc.createElement("div");
  element.classList.add("time-fixed-item-collection");
  element.append(backdrop);
  backdrop.classList.add("time-backdrop");

  function ensure_segment_for(date) {
    if (segments.has(date)) return segments.get(date);
    const parsed = ISO8601.parse(date);
    assert?.(date && parsed, `add segment got bad date ${date}`);

    // Two elements are needed because we want to set the position of the segment
    // *and* support (transparent) positioning of the content within it
    const segment = doc.createElement("div");
    const inner = doc.createElement("div");
    segment.append(inner);
    segment.classList.add("time-backdrop-segment");
    set_time_item_style(date, segment.style);
    set_timeframe_style(date, inner.style);
    segment.setAttribute("data-time", date);
    const ref = first_child_after(date, backdrop);
    backdrop.insertBefore(segment, ref);
    segments.set(date, segment);
    return inner;
  }
  return { element, ensure_segment_for };
}

// in order of preference:
// scale + offset <= 1
// # of periods between min_periods & max periods
// observe hard bounds

function make_time_vantage_base(context) {
  let _sub = null;
  let scale, offset, units, purview; // for deduplication
  const { timeframe, unit } = context;

  // soft constraints
  const MIN_PERIODS = 3;
  // const MAX_PERIODS = 5; PROVISIONAL

  const MULTIPLIER = YEAR_MULTIPLIERS[unit];
  const MIN_UNITS_TO_VIEW = MULTIPLIER * MIN_PERIODS;

  const purview_stream = make_subscribable();
  const point_stream = make_subscribable();

  function _vantage_timeframe_scalars_changed([orig_from, orig_to]) {
    const real_size = orig_to - orig_from;
    const from = orig_from / YEARS_PER_MAJOR_HACK;
    const to = orig_to / YEARS_PER_MAJOR_HACK;
    const timeframe_size = to - from;
    const proposed_scale = timeframe_size / MIN_UNITS_TO_VIEW;
    const new_scale = Math.min(1, proposed_scale);
    const new_offset = 0;

    const midpoint = (from + to) / 2; // aka origin
    const scale_was_clamped = proposed_scale > new_scale;
    const diameter = scale_was_clamped ? timeframe_size : MIN_UNITS_TO_VIEW;
    const new_units = diameter / MULTIPLIER;
    if (new_scale !== scale || new_offset !== offset || new_units !== units) {
      scale = new_scale;
      offset = new_offset;
      units = new_units;
      // point_stream.next({ scale, offset, units, from, to });
    }
    assert?.(offset >= 0 && offset < 1, `offset must be in [0, 1)`, offset);
    assert?.(scale > 0 && scale <= 1, `scale must be in (0, 1]`, scale);
    assert?.(offset + scale <= 1, `offset + scale must be ≤ 1`, offset, scale);

    const radius = diameter / 2;
    const purview_from = midpoint - radius * (1 + offset);
    const purview_to = midpoint + radius * (1 - offset);

    point_stream.next({
      scale: new_scale,
      offset: new_offset,
      units: new_units,
      from: purview_from * YEARS_PER_MAJOR_HACK,
      to: purview_to * YEARS_PER_MAJOR_HACK,
    });

    let from_date, to_date;
    // We don't *want* any more precision for unit >= Year
    if (ISO8601.compare_units(unit, "Year") >= 0) {
      from_date = pad4(Math.floor(purview_from));
      to_date = pad4(Math.ceil(purview_to));
    } else {
      // TODO: there's sometimes still a small gap for Day
      // coercing these to `unit` should cover the above, but it's more work...
      from_date = major_scalar_to_day(purview_from * YEARS_PER_MAJOR_HACK);
      // re `next`: end bound is *exclusive*; this is like `ceil` above
      to_date = ISO8601.next(
        major_scalar_to_day(purview_to * YEARS_PER_MAJOR_HACK)
      );
    }
    const new_purview = `${from_date}/${to_date}`;
    if (new_purview !== purview) {
      purview = new_purview;
      purview_stream.next(purview);
    }
  }

  function start() {
    _sub = timeframe.watch_major_scalars(_vantage_timeframe_scalars_changed);
  }
  function stop() {
    _sub?.unsubscribe();
  }
  return {
    start,
    stop,
    purview: purview_stream.public,
    point: point_stream.public,
  };
}

function link_lens_with_element(lens, /** @type {Element} */ element) {
  assert?.(element instanceof Element, `link lens bad element`, element);
  const { style } = element;
  let point_sub;
  function start() {
    assert?.(!point_sub, `link lens already started`);
    point_sub = lens.point.subscribe({
      next({ scale, offset, units, from, to }) {
        // BUT for days in month, we can allow up to 31 markers
        if (units > 12)
          element.setAttribute("data-time-lens-state", "overloaded");
        else element.removeAttribute("data-time-lens-state");
        // style.setProperty("--timeframe-lens-scale", scale);
        // style.setProperty("--timeframe-lens-offset", offset);
        style.setProperty("--timeframe-from-major", from);
        style.setProperty("--timeframe-to-major", to);
      },
    });
  }
  function stop() {
    point_sub?.unsubscribe();
    point_sub = null;
  }
  return { start, stop };
}

function make_time_vantage(context) {
  const { timeframe, unit, visit, element } = context;

  const base_vantage = make_time_vantage_base(context);
  const chart = time_chart_by({ unit, visit, element });

  base_vantage.purview.subscribe({ next: chart.set_bounds });
  const lens_link = link_lens_with_element(base_vantage, chart.element);

  function start() {
    base_vantage.start();
    chart.start();
    lens_link.start();
  }
  function stop() {
    base_vantage.stop();
    chart.stop();
    lens_link.stop();
  }
  return { ...base_vantage, start, stop, element: chart.element };
}

const YEAR_MULTIPLIERS = {
  Millennium: 1000,
  Century: 100,
  Decade: 10,
  Year: 1,
  Month: 1 / 12,
  Day: 1 / 365, // this is for a rule of thumb, so edge case is immaterial
};

// === “major” scale
// for units ∈ [Millennium, Day]
// year is indeed the base unit, but *all conversions should use these functions*
// decoupling scale from year *per se* lets us get precision where we need it
// Firefox in particular runs out of floating point space in css vars at Day level
const YEARS_PER_MAJOR_HACK = 100;
{
  const _YEARS_PER_MAJOR = YEARS_PER_MAJOR_HACK;

  const int = s => parseInt(s, 10);
  const { parse, coerce, compare_units, day_in_year, days_in_year } = ISO8601;

  const _year_scalar_from = date => {
    const parsed = parse(date);
    assert?.(parsed, `major_scalar_from got bad date`, date);
    let year_text = parsed.year;
    if (compare_units(parsed.unit, "Year") > 0)
      year_text = coerce(date, "Year");
    const year = int(year_text);
    switch (parsed.unit) {
      case "Month":
      case "Day": {
        const day = day_in_year(date);
        const days = days_in_year(date);
        const fraction = day / days;
        return year + fraction;
      }
    }
    return year;
  };
  function major_scalar_from(date) {
    return _year_scalar_from(date) * _YEARS_PER_MAJOR;
  }

  // convert from a major scalar value to the day that contains it
  // THIS should roundtrip dates with `major_scalar_from` but doesn't always

  // Scalar year values:
  // 1 means Jan 1, 1 AD
  // 1.5 means the middle of 1 AD (~July 1 AD)
  // 0 means Jan 1, 1 BC
  // 0.5 means... ~July, 1 BC
  // -1 means Jan 1, 2 BC
  // -0.5 means ~July, 2 BC
  function major_scalar_to_day(major) {
    // not really worried about the precision problem here, so divide now
    const years = major / _YEARS_PER_MAJOR;
    // this is also wrong for BC
    const year = Math.floor(years);
    // THIS is still untested for BC
    const signed_yyyy = pad4(year < 1 ? year + 1 : year);
    const fraction = Math.abs(years) % 1;
    const year_days = days_in_year(signed_yyyy);
    const day_ratio = 1 / year_days;
    // Because of precision loss we may get something like
    //     0.3945205479451488 / 0.0027397260273972603
    //     143.9999999999793
    //
    // so to get the date “containing” this point, I'd expect `floor` to be the
    // right operation here, in that case, the result I want is 144, not 143.
    //
    // Right but what was the problem?
    const days = Math.floor(fraction / day_ratio);
    // const days = Math.round(fraction / day_ratio);
    const js_date = new Date(Date.UTC(year, 0, days + 1));
    js_date.setUTCFullYear(year); // account for Date.UTC's quirk re 1900's
    const month_number = js_date.getUTCMonth() + 1;
    const day_number = js_date.getUTCDate();
    return `${signed_yyyy}-${pad2(month_number)}-${pad2(day_number)}`;
  }

  const year_to_major = x => x * YEARS_PER_MAJOR_HACK;

  // find a date given 2 scalars
  // defines an equivalence class between the continuous major scale
  // and the ISO dates we recognize as timeframe
  // completely partitions the space such that the named dates fall fully into view
  const { compare, previous, next } = ISO8601;
  const { assign } = Object;
  const FACTORS = Object.entries(map_object(YEAR_MULTIPLIERS, year_to_major));

  function major_scalars_to_timeframe(from, to) {
    const size = to - from;
    if (size === 0) return;
    const from_day = major_scalar_to_day(from);
    const to_day = major_scalar_to_day(to);

    // you must *fully* encompass at least one unit, or keep looking
    for (const [unit, major] of FACTORS) {
      const partial = major > size;
      if (partial && unit !== "Day") continue;
      const container = coerce(from_day, unit);
      const boundary = from === major_scalar_from(container);
      const first = boundary ? container : next(container);
      if (partial) return first;

      const ends_in = coerce(to_day, unit);
      if (unit === "Day" && first === ends_in) return first;
      const last = previous(ends_in);
      const comparison = compare(first, last);
      if (comparison > 0) continue;
      if (comparison === 0) return first;
      return `${first}/${last}`;
    }
  }
  function test_major_scalars_to_timeframe() {
    const CASES = [
      [[123400, 123400], undefined], // division by zero
      [[123400, 123400.001], "1234-01-01"], // day is bottom
      [[123400, 123500], "1234"], // end exclusive
      [[123400, 123510], "1234"], // end extension
      [[123400, 123599], "1234"], // up to
      [[123400, 123499], "1234-01/1234-11"], // span multiple
      [[123400, 123600], "1234/1235"], // but not including next
      [[123390, 123510], "1234"], // start & end extension
      [[123410, 123510], "1234-03/1235-01"], // refinement
      [[123410, 123510.00001], "1234-03/1235-01"], // distance, not precision
      [[123400, 123499.9], "1234-01/1234-11"],
      [[123410, 123600], "1235"], // largest contained unit
      [[123410, 123610], "1235"], // right?
      [[123390, 123610], "1234/1235"],
      [[123400, 124400], "1234/1243"],
      [[123000, 124000], "123"],
      [[123000, 124100], "123"],
      [[123000, 124140], "123"],
      [[123000, 124150], "123"],
      [[123000, 133000], "123/132"],
      [[123000.01, 133000], "124/132"],
      [[120000, 130000], "12"],
      [[120000, 130100], "12"],
      [[119900, 130100], "12"],
      [[120100, 130100], "121/129"],
      [[158289.67465753408, 158289.98287671216], "1582-11-25"],
    ];
    for (const [[from, to], expect] of CASES) {
      const got = major_scalars_to_timeframe(from, to);
      assert(got === expect, `[${from}, ${to})→${expect} got ${got}`);
    }
  }
  const api = { self_tests: test_major_scalars_to_timeframe };
  assign(major_scalars_to_timeframe, api);
}

function render_person_into(person, /** @type Element */ element, rex) {
  const lang = "en";
  const doc = element.ownerDocument;
  const { wiki, summary, born, died } = person;
  const { dataset, style } = element;

  element.setAttribute("data-wiki", wiki);

  const time = document.createElement("time");
  {
    const born_label = born && ISO8601.label(born);
    const died_label = died && ISO8601.label(died);
    const dates = ` (${born_label ?? ""}${died ? ` – ${died_label}` : ""})`;
    const iso = born && died ? `${born}/${died}` : born || died;
    time.classList.add("dates");
    time.dateTime = iso;
    time.textContent = dates;
  }

  const heading = doc.createElement("a");
  {
    assert?.(wiki, `who has no wiki?`, person);
    heading.href = wiki_href(wiki, lang);
    heading.classList.add("title");
    heading.append(...match_and_mark_text(rex, wiki, wiki_unslug, doc));
  }

  const comment = doc.createElement("span");
  {
    comment.classList.add("summary");
    comment.append(...match_and_mark_text(rex, summary, null, doc));
  }

  element.append(heading, " ", time, " ", comment);
  return element;
}

// intensional is too lossy; extensional is too big
function hack_en_wiki_iso_to_timeline(wiki) {
  const parsed = ISO8601.parse(wiki);
  if (!parsed) return;
  switch (parsed.unit) {
    case "Millennium": {
      // get ordinal etc
      return parsed.input;
    }
    case "Century": {
      // get ordinal etc
      return parsed.input;
    }
    // current catalog doesn't have any decade-specific content though
    case "Decade": {
      const decade = parseInt(parsed.decade, 10);
      const year = decade * 10;
      return `${year}s`;
    }
    case "Year": {
      const year = parseInt(parsed.year, 10);
      if (parsed.bc) return `${Math.abs(year) + 1}_BC`;
      if (year < 50) return `${year}_AD`;
      return `${year}`;
    }
  }
  throw new Error(`Bad request: ${parsed.unit}`);
}

function render_event_into(event, /** @type Element */ element, rex) {
  const lang = "en";
  const doc = element.ownerDocument;
  const { aboutTime, topics, refs, html } = event;

  const time = timestamp(aboutTime, lang);
  const description = doc.createElement("blockquote");
  description.innerHTML = html; // hotspot for sure
  // marking up the html is more complicated
  match_and_mark_html(rex, description, doc);
  for (const wiki_ele of description.querySelectorAll("[wiki]")) {
    const wiki = wiki_ele.getAttribute("wiki");
    wiki_ele.href = wiki_href(wiki, lang);
  }

  const citation = doc.createElement("cite");
  const source_link = doc.createElement("a");
  // event SHOULD have a source reference (aka provenance), but in the present
  // catalog (timeline articles) title is *nearly* redundant with this date.
  let date = aboutTime;
  // There are no timeline articles more specific than year
  if (ISO8601.compare_units(ISO8601.parse(date).unit, "Year") < 0)
    date = ISO8601.coerce(date, "Year");
  const wiki = hack_en_wiki_iso_to_timeline(date);
  const href = wiki_href(wiki, lang);
  const title = doc.createElement("q");
  title.textContent = wiki_unslug(wiki);
  source_link.href = href;
  source_link.append(`from `, title, ` in English Wikipedia`);
  citation.append(source_link);
  description.cite = href; // does this do anything anywhere?

  let topic_list;
  if (topics?.length > 0) {
    topic_list = doc.createElement("div");
    topic_list.classList.add("topics");
    for (const topic of topics) {
      const item = doc.createElement("span");
      item.setAttribute("data-topic", topic);
      item.textContent = topic;
      topic_list.append(item, " ");
    }
  }

  // We don't want to do this for backdrops because it's redundant
  // getting that preference here is another matter
  const include_time = false;
  if (include_time) element.append(time, " ");
  element.append(description, citation);
  if (topic_list) element.append(topic_list);
  return element;
}

/** @param {Element} element */
function describe_us_constitutional_amendment(amendment) {
  return wiki_unslug(amendment.wiki); // this labels, not describes
}
function render_us_constitutional_amendment_into(amendment, element, rex) {
  // this marks the creation process; the dates imply the effective period
  const lang = "en";
  const { number, wiki, description } = amendment;
  const from = amendment.proposed;
  const to = amendment.completed;
  assert(number, `which amendment is this?`, amendment);
  assert(from, `when was this amendment?`, amendment);
  set_time_style_and_attributes(element, from, to);
  element.setAttribute("data-amendment-number", amendment.number);
  if (wiki) {
    const link = document.createElement("a");
    link.href = wiki_href(wiki, lang);
    link.textContent = wiki_unslug(wiki);
    element.append(link);
  }
  if (description) {
    const phrase = document.createElement("p");
    phrase.innerHTML = description; // HOTSPOT
    element.append(phrase);
  }
}
function render_us_state_into(state, element, rex) {
  element.textContent = `${state.name}, a US State`;
}
function describe_us_president(term) {
  const { number, incumbent } = term;
  const name = wiki_unslug(incumbent);
  const ordinal = number; // ordinal_en(number);
  return `${ordinal} US presidency, of ${name}`;
}
function render_us_president_into(term, element, rex) {
  const from = term.termStart;
  let to = term.termEnd;
  assert(from, `when was this term?`, term);
  if (!to) {
    to = new Date().toISOString().slice(0, 10);
    element.setAttribute("data-time-to-present", "true");
  }
  // weird, this isn't working. but it shouldn't be done here anyway
  if (term.portrait) {
    const img = document.createElement("img");
    img.src = term.portrait;
    element.append(img);
  }
  set_time_style_and_attributes(element, from, to);
  element.textContent = describe_us_president(term);
}

const METHODS = {
  get_id: {
    en_wiki_persons: record => `wiki:${record.wiki}`,
    en_wiki_events: record => `en_wiki_event:${record.id}`,
    en_us_constitutional_amendments: record => `wiki:${record.wiki}`,
    en_us_states: record => `us-state:${record.name}`, // need wiki or fips
    en_us_presidents: record => `us-president:Term${record.number}`,
  },
  // iterator would be more general but more expensive huh?
  date_fields: {
    en_wiki_persons: ["born", "died"],
    en_wiki_events: ["aboutTime"],
    en_us_constitutional_amendments: ["proposed", "completed"],
    en_us_states: ["joined"],
    en_us_presidents: [
      "termStart",
      "termEnd",
      "electionDates" /*which is multi*/,
    ],
  },
  regex_test: {
    en_wiki_persons: (rex, person) =>
      rex.test(person.wiki) || rex.test(person.summary),
    en_wiki_events: (rex, event) =>
      event.topics?.some(it => rex.test(it)) || rex.test(event.html),
    en_us_constitutional_amendments: (rex, amendment) =>
      rex.test(amendment.wiki) ||
      rex.test(amendment.number.toString()) || // title won't since numerals are spelled out
      rex.test(amendment.description /*which is html*/),
    en_us_states: (rex, state) => rex.test(state.name),
    en_us_presidents: (rex, term) =>
      rex.test(term.incumbent) || rex.test(term.party),
  },
  make_scans: {
    // the date bits of this should be superseded by `date_scan_segments`
    *en_wiki_persons({ date, ref }) {
      if (typeof ref === "string") {
        // supersedes date.  will have 0 or 1 results
        yield { query: IDBKeyRange.only(ref) };
      } else if (typeof date === "string") {
        // 2 dates: list births after the min and deaths before the end
        const [lower, upper] = timeframe_bounding_dates(date);
        const query = safe_date_bounds(lower, upper);
        yield { index: "born", query, dir: "next" };
        yield { index: "died", query, dir: "prev" };
      } else {
        yield { index: "born", dir: "next" };
        yield { index: "died", dir: "prev" };
      }
    },
    *en_wiki_events({ date, ref }) {
      // IGNORES date filter when a ref is present
      if (typeof ref === "string") {
        yield { index: "refs", query: IDBKeyRange.only(ref) };
      } else if (typeof date === "string") {
        const [from, to] = timeframe_bounding_dates(date);
        const query = safe_date_bounds(from, to);
        yield { index: "aboutTime", dir: "next", query };
      } else {
        yield { index: "aboutTime", dir: "next" };
      }
    },
  },
  render_into: {
    en_wiki_persons: render_person_into,
    en_wiki_events: render_event_into,
    en_us_constitutional_amendments: render_us_constitutional_amendment_into,
    en_us_states: render_us_state_into,
    en_us_presidents: render_us_president_into,
  },
};

// === global event intercepts

function make_time_markers_navigational() {
  const scope = document.body;
  const what = "click";
  function handler(/** @type Event */ event) {
    const { target } = event;
    const date =
      target.closest("time")?.dateTime ??
      target.closest(`button[data-time]`)?.dataset.time;
    if (!date) return;
    const parsed = ISO8601.parse(date);
    if (!parsed) return console.warn(`time had bad date ${date}`);
    const timeframe = target.closest(`[data-timeframe]`);
    if (!timeframe) return console.warn(`okay ${date} but no timeframe`);
    timeframe.setAttribute("data-timeframe", date);
    // TODO: ctrl+click extends vs replaces
    // actually that might be a better default
  }
  function start() {
    scope.addEventListener(what, handler);
  }
  function stop() {
    scope.removeEventListener(what, handler);
  }

  return { start, stop };
}

// pan & zoom
//
// 1. you never get stuck
// 2. movement is stable (all operations are monotonic)
// 3. you never violate the limits (size and bounds)
// all operations reach a fixpoint (not NaN)
// start < end

// pan left reaches a fixpoint where start = lo
// pan right reaches a fixpoint where end = hi
// zoom in reaches a fixpoint where size = min_size
// zoom out reaches a fixpoint where size = max_size (or start=lo & end=hi)
// after fixpoint, the inverse operation is still possible
// after zoom-in fixpoint, pans are still possible (usually)
// after pan fixpoints, zooms are still possible (usually)

// if your starting point violates the constraints, coerce with warning

// if max_size = Infinity (or greater than hi - lo) then you can have 3 sides at fixpoint
//   and only zoom-in still operable
{
  const { broader, next, distance, coerce, plus } = ISO8601;
  const { max, min, sign } = Math;

  // pan should NOT start turning into zoom when you hit bound
  // zoom should NOT start turning into pan when you hit bound -- but it does rn
  function make_constrain(bounds) {
    const max_size = bounds.max_size ?? Infinity;
    const min_size = bounds.min_size ?? 0;
    const lo = bounds.lo ?? -Infinity;
    const hi = bounds.hi ?? Infinity;
    if (min_size < 0) throw new Error(`Bad request: need non-negative min`);
    if (hi - lo < min_size) throw new Error(`Bad request: hi-lo<min_size`);
    // but you could want to say this
    // if (hi - lo < max_size) throw new Error(`Bad request: hi-lo<max_size`);
    const actual_max = min(hi - lo, max_size);
    const max2 = actual_max / 2;
    const min2 = min_size / 2;
    return function constrain(from, to) {
      const size = to - from;
      if (size > max_size) {
        const mid = (from + to) / 2;
        return [max(lo, mid - max2), min(hi, mid + max2)];
      }
      if (size < min_size) {
        const mid = (from + to) / 2;
        return [max(lo, mid - min2), min(hi, mid + min2)];
      }
      if (from < lo) return [lo, lo + min(size, actual_max)];
      if (to > hi) return [hi - min(size, actual_max), hi];
      return [from, to];
    };
  }
  function make_mover(α1, α2, bounds) {
    const constrain = make_constrain(bounds);
    return function move(from, to, r = 0.5) {
      const size = to - from;
      const δ1 = α1 * r * size;
      const δ2 = α2 * (1 - r) * size;
      return constrain(from + δ1, to + δ2);
    };
  }
  function make_movers(spec) {
    // I mean you could want different alpha for pan vs zoom
    const α = spec.alpha ?? 1 / 8;
    const { bounds } = spec;
    return {
      zoom_in: make_mover(α, -α, bounds),
      zoom_out: make_mover(-α, α, bounds),
      pan_left: make_mover(-α, -α, bounds),
      pan_right: make_mover(α, α, bounds),
    };
  }

  // just binds the 4 movement operations to timeframe state
  function make_timeframe_mover(context, spec) {
    const { timeframe } = context;
    if (!timeframe) throw new Error(`Bad request: expected timeframe`);
    const ops = make_movers({ bounds: spec });
    const frame = () => timeframe.get_major_scalars();
    function submit(target) {
      if (target) timeframe.set_major_scalars(...target);
    }
    const api = map_object(
      ops,
      op =>
        (...args) =>
          submit(op(...frame(), ...args))
    );
    const get = () => timeframe.get_major_scalars();
    // `get` is for tests only
    return { ...api, get };
  }
}

// pan & zoom tests
{
  // was seeing floating-point artifacting in Firefox CSS variables...

  // monotonic: until reaching bounds,
  // - every pan moves both bounds strictly in alpha direction
  // - pans always move by the same amount
  // - 2nd order: successive zooms always move by increasing/descreasing amounts

  // α=1 makes identity, where everything is a fixpoint.  ∀ x identity(x) = x

  // pan left/right are inverses
  // zoom left/right are inverses
  // EXCEPT where you hit bounds...
  // ∀ x,y: pan_left(x, y) = is_fixpoint(pan_left(x, y)) ∨ pan_right(pan_left(x, y))
  // ∀ x,y: zoom_in(x, y) = is_fixpoint(zoom_in(x, y)) ∨ zoom_out(zoom_in(x, y))

  // the only reason you need a mock is that real timeframe has an element
  // though it should make no difference
  function _make_mock_mover(spec, [from, to]) {
    let [_from, _to] = [from, to];
    const get_major_scalars = () => [_from, _to];
    const set_major_scalars = (from, to) => {
      _from = from;
      _to = to;
    };
    const phony = { get_major_scalars, set_major_scalars };
    return make_timeframe_mover({ timeframe: phony }, spec);
  }

  const _is_fixpoint = (input, output) =>
    input[0] === output[0] && input[1] === output[1];

  function _is_place_within_bounds(place, bounds) {
    const [start, end] = place;
    const { lo, hi, min_size, max_size } = bounds;
    const size = end - start;
    if (end >= start) throw new Error(`${start} ≮ ${end}`);
    return start >= lo && end <= hi && size >= min_size && size <= max_size;
  }

  // const g = major_scalar_from("1582-11-29"); // 1582909.589041096;
  // const h = major_scalar_from("1582-11-30"); // 1582912.3287671232;

  const _DELTAS = {
    zoom_in: [1, -1],
    zoom_out: [-1, 1],
    pan_left: [-1, -1],
    pan_right: [1, 1],
  };

  // Interpretation: run the op to fixpoint (or max #)
  // These should always be safe when there are finite bounds
  const _CASES = [
    [["zoom_in"], ["zoom_out"]],
    [["zoom_out"], ["zoom_in"]],
    [["zoom_out"], ["pan_left"], ["pan_right"]],
    [["zoom_out"], ["pan_right"], ["pan_left"]],
    [["zoom_in"], ["pan_left", 100], ["pan_right", 100]],
  ];
  {
    const HARD_MAX = 500;
    function run_universal_case(steps) {
      const lo = -10;
      const hi = 10;
      const min_size = 0.1;
      const max_size = 100;
      const mover = _make_mock_mover({ lo, hi, min_size, max_size }, [0, 1]);

      let count = 0;
      let op = null;
      function set_op(name) {
        op = name;
        count = 0;
      }

      for (const [op, limit = Infinity] of steps) {
        set_op(op);
        while (true) {
          const input = mover.get();
          if (!input) debugger;
          mover[op]();
          const output = mover.get();
          if (_is_fixpoint(input, output)) break;

          const deltas = _DELTAS[op];
          // if (Math.sign(output[0] - input[0]) !== deltas[0]) debugger;
          // if (Math.sign(output[1] - input[1]) !== deltas[1]) debugger;
          // a few of these started failing when I added `r`
          assert(Math.sign(output[0] - input[0]) === deltas[0], `Δ₀`);
          assert(Math.sign(output[1] - input[1]) === deltas[1], `Δ₁`);

          ++count;
          if (count >= limit) break;
          if (count >= HARD_MAX) throw new Error(`killed ${op} @ ${count}`);
        }
      }
    }
  }
  function self_tests() {
    _CASES.forEach(run_universal_case);
  }

  Object.assign(make_timeframe_mover, { self_tests });
}

{
  const doc = document;
  const lang = "en";
  const { coerce, distance, plus, label } = ISO8601;

  function support_timeframe_pointer_echo(context) {
    let _sub;
    let _from_day = null;
    let _timeframe_days = NaN;
    const { timeframe } = context;
    if (!timeframe) throw new Error(`Bad request: timeframe expected`);
    /** @type Element */ const element = timeframe.element;

    const selector = '[data-part="caption"]';
    const caption = element.querySelector(selector);
    if (!caption) throw new Error(`Invalid state: expected caption`);
    const pointer_label = doc.createElement("p");
    caption.insertAdjacentElement("afterend", pointer_label);

    function timeframe_changed(date) {
      const [from, to] = timeframe_bounding_dates(date);
      _from_day = coerce(from, "Day");
      const to_day = coerce(to, "Day");
      const days = distance(_from_day, to_day);
      _timeframe_days = days.n - 1; // because exclusive
    }

    function pointermove(/** @type {PointerEvent} */ event) {
      if (isNaN(_timeframe_days) || _from_day === null)
        return console.warn(`timeframe not set`);
      // unlike clientLeft, accounts for padding
      // BUT that could be affected by box-sizing, so, need getClientRects?
      const frame = element.getBoundingClientRect();
      const timeframe_left = frame.left;
      const timeframe_width = frame.width;
      const x = event.clientX;
      const offset = x - timeframe_left;
      const r = offset / timeframe_width;
      // interpolate r between timeframe from & to
      const days_from_left = Math.round(r * _timeframe_days);
      const pointed_date = plus(_from_day, days_from_left, "Day");
      const pointed_date_label = label(pointed_date, lang);
      pointer_label.textContent = `${pointed_date_label} r = ${r}`;
    }

    function start() {
      const date = timeframe.get();
      if (date) timeframe_changed(date);
      _sub = timeframe.watch(timeframe_changed);
      element.addEventListener("pointermove", pointermove, { passive: true });
    }
    function stop() {
      _sub?.unsubscribe();
      element.removeEventListener("pointermove", pointermove);
    }
    return { start, stop };
  }
}

function hijack_wikipedia_links(ele = document.body, lang = "en") {
  function handler(/** @type MouseEvent */ event) {
    // `event.buttons` is giving 0 always in FF??
    if (event.ctrlKey || event.altKey || event.metaKey || event.shiftKey)
      return;

    const { target } = event;
    const tagged = target.closest("[data-wiki],[wiki]");
    if (tagged) {
      // use default handling for buttons inside of wiki things...
      if (tagged !== target && target instanceof HTMLButtonElement) {
        event.stopPropagation();
        return;
      }
      const wiki = tagged.dataset.wiki ?? tagged.getAttribute("wiki");
      if (wiki) {
        event.preventDefault();

        if (context_refs.has(wiki)) context_refs.remove(wiki);
        else context_refs.add(wiki);
        if (context_href.has_cache() && context_href.cache() === wiki)
          context_href.next(null);
        else context_href.next(wiki);

        const show_inline_wiki_iframe = false;
        if (show_inline_wiki_iframe) {
          const iframe = document.createElement("iframe");
          const url = wiki_href(wiki, lang);
          iframe.src = url;
          tagged.append(iframe);
        }
        event.preventDefault();
      }
    }
  }
  function start() {
    ele.addEventListener("click", handler);
  }
  function stop() {
    ele.addEventListener("click", handler);
  }

  return { start, stop };
}

// a drain
function CONSUME_POWER_at_rate(source, interval) {
  let _stopped = false;
  let _timeout = NaN;
  let _resolve = null;

  async function go() {
    for await (const item of source) {
      if (_stopped) break;
      const promise = new Promise(resolve => {
        _resolve = () => {
          _timeout = NaN;
          _resolve = null;
          resolve();
        };
        _timeout = setTimeout(_resolve, interval);
      });
      await promise;
      if (_stopped) break;
    }
    console.debug("consume power complete");
  }

  function start() {
    go();
  }
  function stop() {
    _stopped = true;
    if (!isNaN(_timeout)) {
      clearTimeout(_timeout);
      _resolve();
    }
  }
  return { start, stop };
}

// like `CONSUME_POWER_at_rate` but instrumented
function FEED_POWER_with_metering(source, interval) {
  let _stopped = false;
  let _timeout = NaN;
  let _resolve = null;
  let _when_unpaused = null; // null↔¬paused, else a Promise
  let _unpause = null; // meaningful only if above is not null

  const stdout = make_subscribable();

  async function start() {
    while (true) {
      const started = performance.now();
      const { done } = await source.next(); // throw away value
      if (_when_unpaused) await _when_unpaused;
      const finished = performance.now();
      const waited = finished - started;
      stdout.next({ at: started, waited });
      if (done) break;
      await new Promise(resolve => {
        _resolve = () => {
          _timeout = NaN;
          _resolve = null;
          resolve();
        };
        _timeout = setTimeout(_resolve, interval);
      });
      if (_when_unpaused) await _when_unpaused;
      if (_stopped) break;
    }
    console.debug("feed power complete");
  }
  function stop() {
    _stopped = true;
    if (!isNaN(_timeout)) {
      clearTimeout(_timeout);
      _resolve();
    }
  }

  const is_paused = () => !!_when_unpaused;
  function pause() {
    if (_when_unpaused !== null) return;
    _when_unpaused = new Promise(resolve => {
      _unpause = function _unpause() {
        _when_unpaused = null;
        resolve();
      };
    });
  }
  function unpause() {
    if (is_paused()) _unpause();
  }
  const pause_api = { is_paused, pause, unpause };
  return { start, stop, stream: stdout.public, ...pause_api };
}

{
  const { coerce, compare, distance, next } = ISO8601;
  const trace = false ? (...args) => console.debug(`visitor`, ...args) : null;

  // start inclusive / end exclusive
  function time_range_visitor(spec) {
    const { unit, visit } = spec;
    const capacity = spec.capacity ?? 20;

    return function* scan(initial_bounds) {
      let current, lower;
      let [min, max] = initial_bounds?.split("/", 2) ?? [];

      while (true) {
        const new_bounds = yield current;

        if (new_bounds) {
          if (!new_bounds.includes("/")) return "need range";
          const [lo, hi] = new_bounds.split("/", 2);
          if (hi && hi !== max) max = hi;
          if (lo && lo !== min) min = lo;
        }

        const old_lower = lower;
        lower = coerce(min, unit);
        const upper = coerce(max, unit);

        assert?.(lower, `no lower for ${unit} ${min}`);
        assert?.(upper, `no upper for ${unit} ${max}`);

        const targets = distance(lower, upper);
        assert?.(targets, `${unit} distance failed`, { lower, upper });
        if (targets.n > capacity) {
          trace?.(`overloaded @ ${targets.n}`);
          return `overloaded`;
        }
        trace?.(`capacity ${targets.n} okay`);

        current ??= lower;

        // if the starting point moves, start over
        if (lower !== old_lower) {
          trace?.(`reset`, { old_lower, lower, upper, targets });
          current = lower; // undefined would also work
          continue;
        }

        assert?.(current, `No current?!`);
        visit(current, spec);

        const next_time = next(current);
        if (next_time === undefined) {
          trace?.(`escaped domain at ${current}`);
          return "edge";
        }

        current = next_time;

        // comparing to max can get one too many
        // comparing to upper can get one too few
        // upper is just for checking overload; it is not meant to change the goal
        if (compare(current, max) >= 0) {
          trace?.(`passed goal ${max} at ${current}`);
          return "done";
        }
      }
    };
  }
}

// THIS should use CONSUME_POWER_at_rate (or some actuator) vs its own timer
function time_chart_by(spec) {
  const { unit } = spec;
  const DELAY = 50;

  let timeout;
  let _timeframe;
  let gen;

  const element = spec.element ?? document.createElement("div");

  // // spec.visit.bind(null, element);
  const visit = (...args) => spec.visit(element, ...args);
  const unvisit = spec.unvisit?.bind(null, element);
  const step = time_range_visitor({ ...spec, visit });

  const trace = spec?.trace ? (...x) => console.debug(unit, ...x) : null;

  element.removeAttribute("data-chart-state");

  const restart_generator = () => {
    if (gen) gen.return();
    gen = step(_timeframe)[Symbol.iterator]();
    gen.next();
  };

  function maybe_work() {
    timeout = NaN;
    if (!gen) restart_generator();
    const { done, value } = gen.next(_timeframe);
    if (done) {
      trace?.(`generator returned ${value}`);
      if (value === "overloaded")
        element.setAttribute("data-chart-state", "overloaded");
      gen = null;
      return;
    }
    if (value === undefined) return;
    trace?.(`yielded ${value}...`);
    element.removeAttribute("data-chart-state");
    timeout = setTimeout(maybe_work, DELAY);
  }

  function set_bounds(date) {
    trace?.(`set bounds`, date);
    assert?.(date, `${unit} set bounds without timeframe`);
    // DUPLICATES logic in timeframe_bounding_dates
    if (date.includes("/")) _timeframe = date;
    else _timeframe = `${date}/${ISO8601.next(date)}`;
    if (!timeout) maybe_work();
  }

  let _started = false;
  function start() {
    trace?.(`start`);
    assert?.(!_started, `${unit} start was called again, bad!`);
    _started = true;
  }
  function stop() {
    if (gen) gen.return("stopped");
    clearTimeout(timeout);
    timeout = NaN;
  }

  return { start, stop, element, set_bounds };
}

// timeframe implies existence of a process
// visit:assert :: unvisit:retract
// traversing from the bounds of the timeframe X → Y
// when the cost of completing that process would be < max
// spend up to max credits every time timeframe changes
//
// example state sequence
// ∅
// start({unit: "Year", max: 12})
// - do nothing yet, because still no bounds
// set_bounds("19")
// which means 20c so [1900, 2000)
// compute distance = 100
// ✗ out of budget, so die
// --- but, we were also communicating "overloaded"
// set_bounds("184")
// which means 1840's so [1840, 1850)
// compute distance = 10
// ✓ within budget so start at 1840
// set_bounds("1840/1846")
// compute distance = 6
// within budget
// lower bound hasn't changed
// upper bound has changed
// if it's still going, then it's still within range
// if not, it should be done anyway

const UNITS = ["Millennium", "Century", "Decade", "Year", "Month", "Day"];
const REVERSE_UNITS = [...UNITS].reverse();
function make_time_walkers(spec) {
  const { timeframe, maker } = spec;
  const units = spec.units ?? UNITS;
  const tag = spec?.tag ?? "nav";
  const element = document.createElement(tag);
  const walks = UNITS.map(unit => maker({ unit, ...spec }));
  timeframe.watch(date => {
    for (const walk of walks) walk.set_bounds(date);
  });
  element.append(...walks.map(walk => walk.element));
  function start() {
    for (const walk of walks) walk.start();
  }
  function stop() {
    for (const walk of walks) walk.stop();
  }
  return { start, stop, element };
}

function attribute_effects(element, specs) {
  let observer;
  function start() {
    observer = new MutationObserver(function _attribute_effect(changes) {
      for (const { attributeName: name } of changes) {
        const value = element.getAttribute(name);
        specs[name](value);
      }
    });
    const names = Object.keys(specs);
    observer.observe(element, { attributeFilter: names });
  }
  function stop() {
    observer?.disconnect();
  }
  return { start, stop };
}

function _set_time_style(is_item, date, style) {
  const [start, end] = timeframe_bounding_dates(date).map(major_scalar_from);
  style.setProperty(is_item ? "--major" : "--timeframe-from-major", start);
  style.setProperty(is_item ? "--major-to" : "--timeframe-to-major", end);
}
const set_timeframe_style = _set_time_style.bind(null, false);
const set_time_item_style = _set_time_style.bind(null, true);

function set_time_style_and_attributes(element, at, to) {
  if (at) {
    element.setAttribute("data-time", at);
    element.style.setProperty("--major", major_scalar_from(at));
  }
  if (to) {
    element.setAttribute("data-time-to", to);
    element.style.setProperty("--major-to", major_scalar_from(to));
  }
}

function make_timeframe(options) {
  const tag = options?.tag ?? "figure";
  if (options?.element && !(options.element instanceof Element))
    throw new Error(`bad element`);
  let _watcher;
  let _date = ""; // empty ≍ “any time”; must always equal data-timeframe value
  let _from = ""; // cache of _date start
  let _to = ""; // cache of _date end
  let _from_major = NaN; // scalar timeframe start; always = --timeframe-from-major (when not NaN)
  let _to_major = NaN; // scalar timeframe end; always = --timeframe-to-major (when not NaN)
  const { parse, previous, next } = ISO8601;

  const date_changes = make_subscribable();
  const _scalar_changes = make_subscribable();

  const element = options?.element ?? document.createElement(tag);
  const { style } = element;
  {
    const from_css = style.getPropertyValue("--timeframe-from-major");
    const to_css = style.getPropertyValue("--timeframe-to-major");
    const from = parseFloat(from_css);
    const to = parseFloat(to_css);
    if (!isNaN(from) && !isNaN(to)) set_major_scalars(from, to);
  }
  if (element.hasAttribute("data-timeframe")) {
    const date = element.getAttribute(`data-timeframe`);
    if (date === "") {
      const date = major_scalars_to_timeframe(_from_major, _to_major);
      if (date) set(date);
    } else if (date) _attr_changed(date);
  } else set(_date);

  function _scalars_and_dates_are_consistent() {
    if (isNaN(_from_major)) return false;
    assert?.(!isNaN(_to_major), `should be same as from`);
    const scalar_date = major_scalars_to_timeframe(_from_major, _to_major);
    return _date === scalar_date;
  }
  // we don't know whether this is from our change or anyone else's
  function _attr_changed(date) {
    if (_date === date) return console.info("no-op @", date);
    _date = date;
    [_from, _to] = timeframe_bounding_dates(_date);
    if (!_scalars_and_dates_are_consistent()) _set_scalars_from_dates();
    date_changes.next(_date);
  }
  function _update_scalars(from, to) {
    _from_major = from;
    _to_major = to;
    style.setProperty("--timeframe-from-major", _from_major);
    style.setProperty("--timeframe-to-major", _to_major);
    _scalar_changes.next([_from_major, _to_major]);
  }

  function _set_scalars_from_dates() {
    if (!_date) return console.warn(`missing date`);
    assert?.(_from && _to, `expected dates`);
    _update_scalars(major_scalar_from(_from), major_scalar_from(_to));
  }

  function get() {
    return _date;
  }
  function set(date) {
    if (date !== _date) element.setAttribute("data-timeframe", date); // slight hotspot, b/c mutation observers?
  }
  function get_major_scalars() {
    assert?.(typeof _date === "string", `expected string _date`);
    if (_date === "") return;
    if (isNaN(_from_major)) _set_scalars_from_dates();
    return [_from_major, _to_major];
  }
  function set_major_scalars(from, to) {
    _update_scalars(from, to);
    if (!_scalars_and_dates_are_consistent()) {
      const date = major_scalars_to_timeframe(_from_major, _to_major);
      if (!date) return console.warn(`no date for these scalars!?`, from, to);
      set(date);
    }
  }
  function start() {
    _watcher = attribute_effects(element, { "data-timeframe": _attr_changed });
    _watcher.start();
  }
  function stop() {
    _watcher?.stop();
  }
  function watch(callback) {
    return date_changes.public.subscribe({ next: callback });
  }
  function watch_major_scalars(callback) {
    return _scalar_changes.public.subscribe({ next: callback });
  }

  const date_api = { get, set, watch };
  const scalar_api = {
    major_scalars: _scalar_changes.public,
    // that should replace these
    get_major_scalars,
    set_major_scalars,
    watch_major_scalars,
  };
  const timeframe = { start, stop, element, ...date_api, ...scalar_api };
  TIMEFRAMES_BY_ELEMENT.set(element, timeframe); // see note to
  return timeframe;
}

{
  const COMMANDS = [
    "pan_left",
    "extend_left",
    "zoom_in",
    "now",
    "random",
    "clear",
    "zoom_out",
    "extend_right",
    "pan_right",
  ];
  const other = {
    extend_left() {
      // move timeframe start earlier but keep end
    },
    extend_right() {
      // move timeframe end later but keep start
    },
    clear() {
      // clear the stage.  this shouldn't break anything!
      if (!main_stage) return console.warn(`no main stage!`);
      main_stage.innerHTML = "";
    },
    now() {
      // center on “now” at whatever is current scale
      if (!timeframe) return console.warn(`no timeframe!`);
      const frame = timeframe.get_major_scalars();
      const now = new Date();
      const today = now.toISOString().slice(0, 10);
      const major = major_scalar_from(today);
      let h = 1 * YEARS_PER_MAJOR_HACK;
      if (frame) {
        const [from, to] = frame;
        const size = to - from;
        h = size / 2;
      }
      timeframe.set_major_scalars(major - h, major + h);
    },
    random() {},
  };
  function make_time_form(context) {
    const { mover } = context;
    if (!mover) throw new Error(`Bad request: missing mover`);
    const form = document.createElement("form");
    const text_input = document.createElement("input");
    text_input.type = "text";
    form.append(text_input);
    form.classList.add("time-form");
    for (const command of COMMANDS) {
      const button = document.createElement("button");
      button.setAttribute("data-command", command);
      button.textContent = wiki_unslug(command); // hack
      form.append(button);
    }
    function clicked(event) {
      const command = event.target.getAttribute("data-command");
      if (command) {
        event.preventDefault();
        if (command in mover) mover[command]();
        else if (command in other) other[command]();
        else console.warn(`Not implemented`, command);
      }
    }
    function start() {
      form.addEventListener("click", clicked);
    }
    function stop() {
      form.removeEventListener("click", clicked);
    }
    return { start, stop, element: form };
  }
}

function make_timeframe_labeler(context) {
  const { timeframe, element } = context;
  if (!timeframe) throw new Error(`Bad request: missing timeframe`);
  if (!element) throw new Error(`Bad request: missing element`);
  let _sub;
  const lang = context.lang ?? "en";
  const { label } = ISO8601;
  function _labeler_timeframe_changed(date) {
    // what we want is the date in the center
    const date_to_show = date; // approximate_timeframe(date) ?? date;
    const text = label(date_to_show, lang);
    element.textContent = `Timeframe: ${text}`;
  }
  function start() {
    _sub = timeframe.watch(_labeler_timeframe_changed);
    element.textContent = `Timeframe: any time`;
  }
  function stop() {
    _sub?.unsubscribe();
  }
  return { start, stop };
}

// lexical order ≠ extended ISO order. index will complain with
// DOMException: Data provided to an operation does not meet requirements
// this lets us get key ranges that should be equivalent
// though to cover the general case of spanning neg/pos you'd need cat
{
  const { compare } = ISO8601;
  function safe_date_bounds(lower, upper) {
    if (lower.startsWith("-") !== upper.startsWith("-"))
      console.warn(`Not implemented mixed bounds ${lower}/${upper}`);
    const in_order = compare(lower, upper) <= 0;
    const range = in_order ? [lower, upper] : [upper, lower];
    return IDBKeyRange.bound(...range, !in_order, in_order);
  }

  // work around the fact that BC is backwards
  // this would need modification to handle dates starting with ‘+’
  //   which sorts before both ‘-’ and ‘0’
  //
  // in general, “from” is closed and “to” is open
  const { bound, lowerBound, upperBound } = IDBKeyRange;
  const MAX_BC = "-000000"; // a lexical bound though not a distinct date
  const MIN_BC = "-999999";
  const MIN_CE = "0"; // ditto
  const ALL_OF_BC = bound(MAX_BC, MIN_BC);
  const ALL_OF_CE = lowerBound(MIN_CE);
  const _date_scan_segments = function (date) {
    // all time
    if (!date)
      return [{ query: ALL_OF_BC, reverse: true }, { query: ALL_OF_CE }];

    const [from, to] = timeframe_bounding_dates(date);
    assert?.(compare(from, to) <= 0, `Bad range ${from}/${to}`);

    if (!from) {
      // ending in BC
      if (to.startsWith("-")) {
        assert?.(indexedDB.cmp(to, MIN_BC) === -1);
        return { query: bound(to, MIN_BC) };
      }
      assert?.(to);
      // ending in CE
      return [
        { query: ALL_OF_BC, reverse: true },
        { query: bound(MIN_CE, to, false, true) },
      ];
    }

    if (!to) {
      // starting in BC
      if (from.startsWith("-")) {
        return [
          { query: bound(MAX_BC, from), reverse: true },
          { query: ALL_OF_CE },
        ];
      }
      // starting in CE
      if (from) return { query: lowerBound(from) };

      throw new Error("never gets here");
    }

    // from & to CE
    if (!from.startsWith("-")) {
      assert?.(!to.startsWith("-"), `¬bc(from) → ¬bc(to)`);
      return { query: bound(from, to, false, true) };
    }
    // from & to BC
    if (to.startsWith("-")) {
      return { query: bound(to, from, true, false), reverse: true };
    }
    // from BC to CE
    return [
      { query: bound(MAX_BC, from), reverse: true },
      { query: bound(MIN_CE, to) },
    ];
  };
  const opposite = dir => (dir === "next" ? "prev" : "next");
  function date_scan_segments(date, reverse = false) {
    function make(it) {
      const { query } = it;
      const dir = reverse === !!it.reverse ? "next" : "prev";
      return { query, dir };
    }
    const result = _date_scan_segments(date);
    const items = Array.isArray(result) ? result : [result];
    if (reverse) items.reverse();
    return items.map(make);
  }
}

function make_timeframe_article(context) {
  const { timeframe } = context;
  if (!timeframe) throw new Error(`Bad request: missing timeframe`);
  const element = document.createElement("article");
  const controls = document.createElement("div");
  const content = document.createElement("div");
  const button = document.createElement("button");
  element.classList.add("timeframe-article");
  controls.classList.add("article-controls");
  content.classList.add("article-content");
  button.classList.add("article-button");
  button.append("♥");
  controls.append(button);
  element.append(controls, content);
  function start() {}
  function stop() {}
  return { start, stop, element, content };
}

// ALMOST superseded by make_dynamic_listing.  still used in backdrops
// a bit more streamlined to call
function make_listing(context) {
  assert?.(context && typeof context === "object", `bad arg`, context);
  const doc = document;
  const lang = context.lang ?? "en";
  const { collection } = context;
  if (!collection) throw new Error("Listing what collection?");

  function get_method(name) {
    const method = context[name] ?? METHODS[name][collection];
    if (!method) throw new Error(`Teach me to ${name} a ${collection}`);
    return method;
  }

  const render_into = get_method("render_into");
  const make_scans = get_method("make_scans");
  const regex_test = get_method("regex_test");

  const { date, visit } = context;
  const budget = context.budget ?? 10;
  const terms = context.terms ?? (context.term && [context.term]);
  let pattern;
  if (terms) {
    if (terms.length === 1) {
      pattern = RegExp_escape(terms[0]);
    } else if (terms.length > 1) {
      pattern = RegExp_alternatives(Array.from(terms, RegExp_escape));
    }
  }
  // we seem to need separate instances of this for search & marking result
  const rex = pattern && new RegExp(pattern, "gi");
  const rex2 = pattern && new RegExp(pattern, "gi");

  const predicate = context.predicate ?? (rex && regex_test.bind(null, rex));

  const element = doc.createElement("article");
  const details = doc.createElement("details");
  const summary = doc.createElement("summary");
  const list = doc.createElement("ol");

  const phrases = terms?.map(term => `“${term}”`).join(" and ");
  const what = phrases && `${phrases} in ${collection}`;
  summary.textContent = `Listing: ${phrases ?? collection}`;
  details.open = true;
  details.classList.add("listing"); // presentational
  details.append(summary, list);
  element.append(details);

  if (date) {
    const time = timestamp(date, lang);
    summary.append(" in ", time);
  }

  const partials = make_scans?.(context) ?? [];
  const sources = Array.from(partials, spec => {
    return search_collection({ db: "cronwall", collection, ...spec });
  });

  const merged = async_merge_roundrobin(sources);
  const limited = async_take(merged, budget);
  const dom_sink = async_map(limited, list_item);
  const scan = CONSUME_POWER_at_rate(dom_sink, 100);

  function list_item(record) {
    const li = doc.createElement("li");
    render_into(record, li, rex2);
    visit?.(record);
    list.append(li);
  }

  function start() {
    scan.start();
  }
  function stop() {
    scan.stop();
  }
  return { context, start, stop, element };
}

//  When multiple terms are bound with a particular type, we can combine them
//  into a single predicate.  Sort of equivalent to merging with separate
//  predicates.

// `term` is a shorhand notation; `terms` is the normal form
/**
 * @param query {{term?: string, terms?: string[]}}
 */
function as_predicate(query) {
  const { collection, term, ref, date, predicate } = query;
  if (!collection) throw new Error(`as predicate requires collection`);
  if (!(collection in KNOWN_CATALOG)) throw new Error(`404 ${collection}`);
  const terms = query.terms ?? (term && [term]);

  if (typeof predicate === "function") return predicate; // kind of a hack

  // HERE we'd like a way to ignore markup
  let pattern;
  if (terms) {
    if (terms.length === 1) {
      pattern = RegExp_escape(terms[0]);
    } else if (terms.length > 1) {
      pattern = RegExp_alternatives(Array.from(terms, RegExp_escape));
    }
  }
  if (pattern) {
    // THIS should be a property of the type, if anything...
    const regex_test = METHODS.regex_test[collection];
    assert?.(typeof regex_test === "function", `terms without test`);
    const rex = new RegExp(pattern, "gi");
    return regex_test?.bind(null, rex);
  } else if (collection === "en_wiki_events" && ref && date) {
    // VERY SPECIAL case because we can't do date and ref at the same time
    // so we assume ref is already handled and do date here
    // (I'm assuming we get more out of the ref index than we would the date one)
    const [from, to] = timeframe_bounding_dates(date);
    const { compare } = ISO8601;
    return record => {
      const date = record.aboutTime;
      return date && compare(date, from) >= 0 && compare(date, to) < 0;
    };
  }
}

{
  const { defineProperty } = Object;
  function with_type(collection, record) {
    return function (record) {
      defineProperty(record, TYPE, { value: collection });
      return record;
    };
  }
}

// creates a filtered scan over a catalog AND extends results with metadata
// parameters the same as read?
/** @param spec {typeof METHODS["make_scans"][keyof typeof METHODS["make_scans"]]} */
{
  function search_collection(query) {
    const { db, collection, ...rest } = query;
    const spec = { db, store: collection, ...rest };
    const reader = IDB.read(spec);
    const predicate = as_predicate(query);
    const filtered = predicate ? async_filter(reader, predicate) : reader;
    const tagged = async_map(filtered, with_type(collection));
    return tagged;
  }
}

// like `search_collection` but focused on time index
{
  const { defineProperty } = Object;
  const True = () => true;
  var scan_collection = async function* scan_collection(spec) {
    const { db, collection, ...options } = spec;
    const date = options.date; // if the collection answers `date_index` with a real index)
    const limit = options.limit ?? 32; // which is twice the default batch size
    const predicate = options.predicate ?? True;
    const index = date_index(collection);
    const reverse = false;
    const partials = date_scan_segments(date, reverse).flatMap(x => x);

    let i = 0;
    for (const partial of partials) {
      const view = { db, store: collection, index, ...partial };
      const reader = IDB.read(view);
      for await (const record of reader) {
        if (predicate(record)) {
          if (i >= limit) break; // actually we want to “pause” here
          ++i;
          defineProperty(record, TYPE, { value: collection });
          yield record;
        }
      }
    }
  };
}

function _collection_search_sources(collection_query) {
  const { collection } = collection_query;
  assert?.(collection, `make_collection_search expected collection`);

  // THIS should be a property of the type, if anything...
  const make_scans = METHODS.make_scans[collection];
  assert?.(typeof make_scans === "function", `make_scans ain't a function`);

  const partials = make_scans?.(collection_query) ?? [];
  return Array.from(partials, clauses => {
    return search_collection({ ...collection_query, ...clauses });
  });
}

function make_collection_search(collection_query) {
  const sources = _collection_search_sources(collection_query);
  return async_merge_roundrobin(sources);
}

const TYPE = Symbol();

// generic
function render_into(...args) {
  const [record, element, highlight] = args;

  const local = record.render_into;
  if (typeof local === "function") return local(...args);

  const type = record[TYPE];
  const global = type && METHODS["render_into"][type];
  if (typeof global === "function") return global(...args);

  throw new Error(`generic render into not implemented`);
}

// HACK: stubs for generics
const get_start_date = record => {
  // prettier-ignore
  switch (record[TYPE]) {
      case "en_wiki_persons": return record.born;
      case "en_wiki_events": return record.aboutTime;
    }
};
const get_end_date = record => {
  // prettier-ignore
  switch (record[TYPE]) {
      case "en_wiki_persons": return record.died;
      case "en_wiki_events": return ISO8601.next(record.aboutTime);
    }
};
const get_date = record => record.aboutTime ?? record.born ?? record.died;
// well, it's not really a range...
const get_is_range = record => record[TYPE] === "en_wiki_persons"; // || record[TYPE] === "en_wiki_events";

// the enter/exit bits of this are being subsumed by memoize_over_key_changes
function make_dynamic_listing(context) {
  let sub, scan;
  let cleanup = NaN; // timeout for cleaning up old list items
  const { timeframe, make_source, mark } = context;
  if (!timeframe) throw new Error(`Bad request: missing timeframe`);

  const doc = document;
  const lang = context.lang ?? "en";
  const budget = context.budget ?? 10; // should be dynamic though

  function do_cleanup() {
    const exit = list.querySelectorAll(`:scope > [data-member-state="exit"]`);
    for (const element of exit) element.remove();
    cleanup = NaN;
  }
  function queue_cleanup() {
    setTimeout(do_cleanup, 5000);
  }
  function bump_cleanup() {
    if (!isNaN(cleanup)) {
      clearTimeout(cleanup);
      queue_cleanup();
    }
  }

  // all of this is just so that we can provide a pattern for render to highlight
  let rex;
  if (mark) {
    // duplicates normalization
    const terms = mark.terms ?? (mark.term && [mark.term]);
    let pattern;
    if (terms) {
      if (terms.length === 1) {
        pattern = RegExp_escape(terms[0]);
      } else if (terms.length > 1) {
        pattern = RegExp_alternatives(Array.from(terms, RegExp_escape));
      }
      // we seem to need separate instances of this for search & marking result
      if (pattern) rex = new RegExp(pattern, "gi");
    }
  }

  const element = doc.createElement("article");
  const details = doc.createElement("details");
  const summary = doc.createElement("summary");
  const list = doc.createElement("ol");
  const trail = doc.createElement("span");
  const description = doc.createElement("span");
  element.classList.add("listing", "resource-listing");
  element.classList.add("with-enter-items", "with-exit-items");
  element.classList.add("with-hover-item");
  element.append(details);
  list.classList.add("with-time-markers");
  details.open = true;
  details.append(summary, list);
  trail.classList.add("time-location-trail");
  summary.append(`Listing`, description, trail);

  // describe the listing source
  {
    const { terms, ref, refs } = context;
    const phrases =
      terms?.map(term => `“${term}”`).join(" and ") ??
      (ref ? [ref] : refs)?.map(ref => `${ref}`).join(" and ");
    description.textContent = phrases ?? " something ";
  }

  function list_item(record) {
    bump_cleanup();
    if (record === undefined) return;
    // these should either be generics or prototype methods
    const collection = record[TYPE];
    const get_id = METHODS.get_id[collection];
    const render_into = METHODS.render_into[collection];
    // deduplicating via dom lookup
    const id = get_id(record);
    const existing = list.querySelector(`:scope > [data-resource="${id}"]`);
    if (existing) {
      existing.setAttribute("data-member-state", "enter");
      return;
    }

    const item = doc.createElement("li");
    const resource = document.createElement("div");
    const article = document.createElement("article");
    const special = doc.createElement("div");
    const general = doc.createElement("div");

    let from, to, date;
    {
      const is_range = get_is_range(record);
      if (is_range) {
        from = get_start_date(record);
        to = get_end_date(record);
        set_time_style_and_attributes(item, from, to);
      } else {
        date = get_date(record);
        if (date) {
          const end = ISO8601.next(date);
          item.setAttribute("data-time", date);
          item.setAttribute("data-time-to", end); // well... not really but I want to mark it this way
          item.style.setProperty("--major", major_scalar_from(date));
          item.style.setProperty("--major-to", major_scalar_from(end));
        }
      }
    }

    resource.classList.add("resource-finding");
    article.classList.add("resource-article", "mark-time--margin");
    item.classList.add("item");
    item.setAttribute("data-member-state", "enter");
    item.setAttribute("data-resource", id);
    special.classList.add("resource-finding-special");
    special.append(article);
    general.classList.add("resource-finding-general");
    render_into(record, article, rex);
    {
      const marker = doc.createElement("div");
      marker.classList.add("time-block-marker", "mark-time--margin");
      if (date) {
        const label = ISO8601.label(date, lang);
        marker.textContent = label;
      } else if (from && to) {
        const from_label = ISO8601.label(from, lang);
        const to_label = ISO8601.label(to, lang);
        marker.textContent = `${from_label} – ${to_label} `;
      } else if (from) {
        marker.textContent = `${ISO8601.label(from, lang)} –`;
      } else if (to) {
        marker.textContent = `– ${ISO8601.label(to, lang)}`;
      } else {
        marker.textContent = `date unknown`;
      }
      general.append(marker);
    }
    resource.append(general, special);
    item.append(resource);
    list.append(item);
  }

  function _set_date(date) {
    if (date) {
      const time = doc.createElement("time");
      const label = ISO8601.label(date, lang);
      time.dateTime = date;
      time.textContent = ` in ${label}`;
      trail.append(time);
    }
    // support exit transition
    // list.innerHTML = "";
    queue_cleanup();
    for (const child of list.children) {
      child.setAttribute("data-member-state", "exit");
    }
    scan?.stop();

    const source = make_source({ date });
    const limited = async_take(source, budget);
    const dom_sink = async_map(limited, list_item);
    scan = CONSUME_POWER_at_rate(dom_sink, 100);
    scan.start();
  }

  function start() {
    sub = timeframe.watch(_set_date);
  }
  function stop() {
    sub?.unsubscribe();
    scan?.stop();
  }
  return { start, stop, element };
}

// describe scan
/*
      const phrases = terms?.map(term => `“${term}”`).join(" and ");
      const what = phrases && `${phrases} in ${collection}`;
      */

// or search?
function make_time_scan(context) {
  let scan;
  assert?.(context && typeof context === "object", `bad arg`, context);
  const { collection, visit } = context;
  if (!collection) throw new Error("Listing what collection?");
  if (typeof visit !== "function") throw new Error("Scan needs f'n visitor");

  function get_method(name) {
    const method = context[name] ?? METHODS[name][collection];
    if (!method) throw new Error(`Teach me to ${name} a ${collection}`);
    return method;
  }

  const make_scans = get_method("make_scans");
  const budget = context.budget ?? 10;
  const rate = context.rate ?? 100;

  const partials = make_scans?.(context) ?? [];
  const sources = Array.from(partials, spec => {
    return search_collection({ ...context, ...spec });
  });
  const merged = async_merge_roundrobin(sources);
  const limited = async_take(merged, budget); // SHOULD be caller's job
  const sink = async_map(limited, visit);

  function start() {
    scan = CONSUME_POWER_at_rate(sink, rate); // SHOULD be caller's job
    scan.start();
  }
  function stop() {
    scan?.stop();
  }
  return { start, stop };
}

// =====================================================
// text in the prompt area is interpreted either
// - non-monotonically: you're just “thinking” it
// - monotonically: you officially said it
{
  const WORD_BOUNDARY = /\W+/g;
  const lang = "en";
  // scrap code for removing things recognized as dates from prompt
  function remove_match(text, match) {
    const end = match.index + match[0].length;
    if (match.index === 0) return text.substring(end);
    const before = text.substring(0, match.index);
    if (end < text.length) return before;
    const after = text.substring();
    return `${before}${after}`;
  }
  let text = "";
  const recognized = ISO8601.recognize(text, lang);
  if (recognized) {
    const date = recognized.value;
    // pin(date);
    // text = remove_match(text, recognized.match);
  }
}

// =========================================

{
  const lang = "en";
  const { compare, coerce, distance, label } = ISO8601;

  const classify_date_and_range = (date, begin, end) => {
    if (!date) return "unknown";
    if (compare(date, begin) < 0) return "before";
    if (compare(date, end) > 0) return "after";
    return "during";
  };

  const years_apart = (date1, date2) => {
    const year1 = coerce(date1, "Year");
    const year2 = coerce(date2, "Year");
    const delta = distance(year1, year2);
    return Math.round(delta.n);
  };

  // classify relation, for alignment
  function describe_relation(born, died, from, to) {
    if (!born && !died) return `dates unknown`;
    const born_rel = classify_date_and_range(born, from, to);
    const died_rel = classify_date_and_range(died, from, to);
    let start_label, end_label;
    let years = NaN;
    if (died_rel === "before") {
      years = years_apart(died, from);
      start_label = `died ${years} years earlier`;
    } else if (born_rel === "after") {
      years = years_apart(to, born);
      end_label = `born ${years} years later`;
    } else {
      if (born) {
        if (born_rel === "before") {
          years = years_apart(born, from);
          start_label = `≈${years} y/o`;
        } else if (born_rel === "during") {
          start_label = `born ${label(born, lang)}`;
        } else assert?.(false, "unexpected born", { born, died, from, to });
      }
      if (died) {
        if (died_rel === "after") {
          years = years_apart(born, to);
          end_label = `≈${years} y/o`;
        } else if (died_rel === "during") {
          // what about at age...
          end_label = `died ${label(died, lang)}`;
        } else assert?.(false, "unexpected died", { born, died, from, to });
      }
    }

    const ret = { born_rel, died_rel, years };
    if (start_label) ret.start_label = start_label;
    if (end_label) ret.end_label = end_label;
    return ret;
  }
}

{
  const { broader, compare_units } = ISO8601;
  function approximate_timeframe(date) {
    const [from, to] = timeframe_bounding_dates(date);
    if (from.unit !== to.unit) {
      const narrower = compare_units(from.unit, to.unit);
    }
    // if it is a singular unit already...
    return broader(broader(date));
  }
}

{
  const doc = document;
  function make_time_range_block() {
    const block = doc.createElement("div");
    const start = doc.createElement("span");
    const end = doc.createElement("span");
    block.classList.add("time-range-block");
    block.append(start, end);
    start.setAttribute("data-affinity", "start");
    end.setAttribute("data-affinity", "end");
    return { block, start, end };
  }
}

{
  const { distance, parse, compare_units, label_units } = ISO8601;
  function time_comparative_label_en(date1, date2) {
    // compare at precision. need labeling of units with plural
    const parsed1 = parse(date1);
    if (!parsed1) throw new Error(`comparative label got bad date ${date1}`);
    const parsed2 = parse(date2);
    if (!parsed2) throw new Error(`comparative label got bad date ${date2}`);
    const result = compare_units(parsed1.unit, parsed2.unit);
    const greater_unit = result < 0 ? asdf : asdf;
    const coerced1 = coerce(greater_unit);
    const dist = distance(coerced1, coerced2);
    const z = dist.n;
    const count = Math.floor(Math.abs(z));
    const units = label_unit(unit, "en");
    const relation = z < 0 ? "earlier" : "later";
    return `${count} ${units} ${relation}`;
  }
}

function make_marker_block(...classes) {
  const block = document.createElement("div");
  const marker = document.createElement("span");
  block.append(marker);
  block.classList.add("time-marker-block");
  marker.classList.add(...classes);
  return { block, marker };
}

// dynamic marker representing a person's birth & death
// depicts and describes in terms of current timeframe
function make_person_lifetime_mark(context) {
  const { timeframe, wiki } = context;
  if (!timeframe) throw new Error(`Bad request: timeframe expected`);
  if (!wiki) throw new Error(`Bad request: (wiki) ref expected`);
  let _stopped = false;
  let _sub_scalars = null;
  let _sub_dates = null;
  let _born_major = NaN;
  let _died_major = NaN;
  let _person = null;
  let person_found;
  const person_promise = new Promise(r => (person_found = r));

  const doc = document;
  const lang = "en";
  const { coerce, compare, distance } = ISO8601;
  const is_number = x => typeof x === "number" && !isNaN(x);

  const element = doc.createElement("div");
  const label = make_marker_block("time-block-label");
  const marker = make_marker_block("time-block-marker");
  const dates = make_time_range_block();
  element.classList.add("person-lifetime");
  element.setAttribute("wiki", wiki);
  label.block.classList.add("label-block");
  marker.block.classList.add("marker-block");
  dates.block.classList.add("dates-block");
  element.append(label.block, dates.block, marker.block);

  const name_label = document.createElement("span");
  const age_now_label = document.createElement("span");
  const final_age_label = document.createElement("span");
  name_label.textContent = wiki_unslug(wiki);
  name_label.classList.add("name");
  age_now_label.classList.add("age-now");
  final_age_label.classList.add("final-age");
  label.marker.append(name_label, " @ ", age_now_label, final_age_label);

  function classify_scalar_and_range(point, [from, to]) {
    if (!is_number(point)) return "unknown";
    if (point < from) return "before";
    if (point >= to) return "after";
    return "during";
  }

  function _update_ages(frame) {
    if (!is_number(_born_major)) {
      console.warn(`got scalar change before person`, wiki);
      return;
    }

    const [_from_major] = frame;
    if (
      is_number(_born_major) &&
      is_number(_died_major) &&
      is_number(_from_major)
    ) {
      if (_born_major <= _from_major && _from_major < _died_major) {
        const age_at_from = (_from_major - _born_major) / YEARS_PER_MAJOR_HACK;
        const text = ` ${Math.floor(age_at_from)} / `;
        if (age_now_label.textContent !== text)
          age_now_label.textContent = text;
      } else {
        age_now_label.textContent = "";
      }
    }

    const born_rel = classify_scalar_and_range(_born_major, frame);
    const died_rel = classify_scalar_and_range(_died_major, frame);
    element.setAttribute("data-born-rel", born_rel);
    element.setAttribute("data-died-rel", died_rel);
  }

  async function start() {
    const person = await get_person(wiki);
    if (_stopped) return;
    if (!person) return console.warn(`no such person?`, wiki);
    person_found(person);
    _person = person;

    const { born, died, summary } = person;
    if (born) _born_major = major_scalar_from(born);
    if (died) _died_major = major_scalar_from(died);
    set_time_style_and_attributes(element, born, died);

    if (summary) {
      const tagline = document.createElement("span");
      tagline.classList.add("person-summary");
      tagline.append(summary);
      name_label.insertAdjacentElement("afterend", tagline);
      name_label.insertAdjacentText("afterend", ", ");
    }

    function _age_at(major) {
      if (!is_number(_born_major)) throw new Error(`expected born date`);
      return (major - _born_major) / YEARS_PER_MAJOR_HACK;
    }

    if (is_number(_born_major) && is_number(_died_major)) {
      const final_age = _age_at(_died_major);
      if (is_number(final_age))
        final_age_label.textContent = `${Math.floor(final_age)}`;
    }

    const throttled = throttle_subscribable(timeframe.major_scalars, 150);
    throttled.subscribe({ next: _update_ages });
    // the watchers don't do this...
    {
      const scalars = timeframe.get_major_scalars();
      if (scalars) _update_ages(scalars);
    }

    // fixed labels for born and died dates
    {
      const start_timestamp = born && structured_timestamp(born, lang);
      const end_timestamp = died && structured_timestamp(died, lang);

      dates.start.append(start_timestamp);
      dates.end.append(end_timestamp);
    }
  }
  function stop() {
    _stopped = true;
    _sub_dates?.unsubscribe();
    _sub_scalars?.unsubscribe();
  }
  return { start, stop, element };
}

function make_event_line(context) {
  const { id } = context;
  if (!id) throw new Error(`event line requires id`);
  const element = document.createElement("div");
  element.classList.add("event-line");
  function start() {}
  function stop() {}
  return { element };
}

function make_person_at_age_listing(context) {
  const { wiki } = context;
  if (!wiki) throw new Error(`Bad request: (wiki) ref expected`);
  let event_listing;
  let person_found;
  const person_promise = new Promise(r => (person_found = r));

  const doc = document;
  const lang = "en";
  const { coerce, compare, distance } = ISO8601;

  const element = doc.createElement("article");
  const header = doc.createElement("header");
  const body = doc.createElement("div");

  element.classList.add("person-at-age-listing");

  const label = wiki_unslug(wiki);

  function list_events_with_age(person) {
    const { born, died } = person;
    const known_lifespan = born && died;
    const birth_year = parseFloat(ISO8601.coerce(born, "Year"));
    const { compare, coerce } = ISO8601;

    event_listing = make_listing({
      collection: "en_wiki_events",
      index: "refs",
      budget: 30,
      // ref: wiki, // with this, other default scans may be included
      *make_scans() {
        yield { index: "refs", query: IDBKeyRange.only(wiki) };
      },
      render_into(record, element, rex) {
        const date = record.aboutTime;
        set_time_style_and_attributes(element, date);

        const wrapper = doc.createElement("div");
        wrapper.setAttribute("data-part", "event");
        const age = doc.createElement("i");
        age.setAttribute("data-part", "age");
        if (
          known_lifespan &&
          compare(date, born) >= 0 &&
          compare(date, died) <= 0
        ) {
          // WRONG crossing BC
          const event_year = parseFloat(coerce(date, "Year"));
          const years = Math.floor(event_year - birth_year);
          const doc = element.ownerDocument ?? document;
          // could also say years after death...
          // ALSO, when event and birthdate units are Day, we don't need “about”
          const desc =
            years === 0
              ? `the year ${wiki_unslug(wiki)} was born `
              : `at about age ${years} `;
          age.textContent = desc;
        }
        element.append(age, wrapper);
        return METHODS.render_into.en_wiki_events(record, wrapper, rex);
      },
    });
    event_listing.start();
    event_listing.element.classList.add("at-age-listing");
    element.append(event_listing.element);
  }

  async function start() {
    const person = await get_person(wiki);
    person_found(person);
    if (!person) return console.warn(`no such person?`, wiki);
    const { born, died } = person;
    // set this on head only; body may have other time items
    set_time_style_and_attributes(header, born, died);
    list_events_with_age(person);
  }
  function stop() {
    event_listing?.stop();
  }

  return { start, stop, element };
}

// ===========================================

{
  const doc = document;

  // only tells you the script is *loaded*, not executed!
  function load_script(src) {
    return new Promise((resolve, reject) => {
      const script = document.createElement("script");
      script.onload = resolve;
      script.onerror = reject;
      script.src = src;
      doc.head.append(script);
    });
  }

  function get_definition_sync(name) {
    return globalThis[name];
  }
  function defined_now(name) {
    return name in globalThis;
  }
  let timeout;
  function when_defined(name, interval = 1000) {
    if (defined_now(name)) return get_definition_sync(name);
    return new Promise(() => {
      function tick() {
        if (defined_now(name)) return get_definition_sync(name);
        timeout = setTimeout(tick, interval);
      }
      tick();
    });
  }

  function loader_in_timeframe(spec) {
    let _stopped;
    const { source, disposition, schema } = spec;
    const { db, store } = disposition;

    async function start() {
      const items = "what";
      const spec = { db, store, items };
      const write_op = IDB.write(spec);
      for await (const result of write_op) {
        if (_stopped) break;
        say?.(`Progress! possibly...`, result);
      }
    }
    function stop() {
      _stopped = true;
    }
    return { start, stop };
  }
}

// ===========================================

async function reify_collection(db_name, store_name, spec) {
  const { say } = this;
  const lang = "en";
  const doc = document;
  const container = doc.querySelector("main") ?? doc.body;

  const element = doc.createElement("div");

  const heading = doc.createElement("h2");
  const form = doc.createElement("form");
  const delete_button = doc.createElement("button");

  heading.append(`Collection: ${store_name}`);
  delete_button.textContent = "Delete this store!";
  // this creates an interactive process... now something is waiting and depends on invisible state to maintain this
  delete_button.onclick = async event => {
    event.preventDefault();
    say?.(`Sigh.  You really want me to delete ${store_name}, eh?`);
    try {
      // DELETE operation not monotonic
      // this is all kinds of race condition etc
      let db = await IDB.connect(db_name);
      say?.(`Okay, well I have ${db_name} at version ${db.version}`);
      db.close();
      db = await IDB.connect(db_name, db.version + 1, db => {
        say?.(`I'm in an upgrade transaction!`);
        db.deleteObjectStore(store_name);
        say?.(`Aaaaand I guess that worked?`);
      });
      db?.close();
    } catch (error) {
      console.error(`Couldn't delete store`, error);
    }
  };
  form.append(delete_button);
  element.append(heading, form);

  // make sure it has the latest indices if any are added
  // so that is updated even if there is no data update
  const description = spec.store;
  const db = await IDB.database_with(db_name, store_name, description);
  db.close();
  await ensure_catalog_data(db_name, store_name, { ...spec, say });
}

// ******************************************************************

function word_cloud(context) {
  const ctx = context && typeof context === "object" ? context : {};
  const lang = ctx.lang ?? "en";
  const doc = ctx.document ?? document;
  const container = ctx.container ?? doc.querySelector("main") ?? doc.body;

  const date = typeof context === "string" ? context : context.date;
  assert?.(date && ISO8601.parse(date), `word cloud got bad date ${date}`);
  const counts = new Map();

  const element = doc.createElement("figure");
  const heading = doc.createElement("figcaption");
  const cloud = doc.createElement("ol");
  const label = ISO8601.label(date, lang);

  heading.append(`Word cloud for ${label}:`);
  cloud.classList.add("word-cloud");
  element.append(heading, cloud);

  function add_to_word_cloud(wiki) {
    if (counts.has(wiki)) {
      // increment
      const item = cloud.querySelector(`[data-wiki="${wiki}"]`);
      const count = counts.get(wiki);
      const new_count = count + 1;
      counts.set(wiki, new_count);
      item.setAttribute("data-count", new_count);
      item.style.setProperty("--count", new_count);
      let cut = false;
      let sibling = item.previousElementSibling;
      if (sibling) {
        while (new_count > sibling.dataset.count) {
          cut = true;
          const prev = sibling.previousElementSibling;
          if (!prev) break;
          sibling = prev;
        }
        if (cut) item.parentElement.insertBefore(item, sibling);
      }
    } else {
      // add with count of 1
      const item = doc.createElement("li");
      item.setAttribute("data-wiki", wiki);
      item.textContent = `${wiki_unslug(wiki)}`; // domain knowledge
      cloud.append(item);
      counts.set(wiki, 1);
      item.setAttribute("data-count", 1);
      item.style.setProperty("--count", 1);
    }
  }

  async function start() {
    const read_spec = {
      db: "cronwall",
      store: "en_wiki_events",
      index: "aboutTime",
      query: IDBKeyRange.only(date),
    };
    let budget = 20;
    for await (const result of IDB.read(read_spec)) {
      if (!--budget) break;
      if (result.refs) {
        for (const wiki_ref of result.refs) add_to_word_cloud(wiki_ref);
        await sleep(50);
      }
    }
  }

  function stop() {}
  container.append(element);
  return { start, stop, element };
}

// ****************************************************************************

async function ensure_catalog_data(db_name, store_name, spec) {
  const say = spec.say ?? console.debug.bind(console);
  const { source, xml_name } = spec;
  say?.(`First of all, is there already data in ${db_name}:${store_name}?`);
  try {
    const count = await IDB.count({ db: db_name, store: store_name });
    if (count > 0) {
      say?.(`Yes! ${count} things, in fact.  So I say we're done here`);
      return;
    }
    say?.(`No.`);
  } catch (error) {
    console.error(`Well I had a problem counting`, error);
  }
  say?.(`Can I load the source?`);
  await load_script(source);
  say?.(`Well I loaded the source.  I'll wait now for ${xml_name}`);
  const xml = await when_defined(xml_name);
  if (xml) say?.(`Oh! That exists too now.  It's ${xml.length}`);
  else {
    say?.(`For some reason that still doesn't exist`);
    say?.(`K bye!`);
    return;
  }

  const parser = new DOMParser();
  let /** @type Document */ doc;
  try {
    // but this won't catch parse errors, see https://stackoverflow.com/a/70824527
    doc = parser.parseFromString(xml, "text/xml");
  } catch (error) {
    say?.(`Wouldn't you know but that failed with ${error}`);
    say?.(`Bye!`);
    return;
  }
  say?.(`Did that parse *actually* succeed?`);
  const parseerror = doc.querySelector("parseerror");
  if (parseerror) {
    say?.(`Well friend that failed with ${parseerror.textContent}`);
    say?.(`Bye!`);
    return;
  }
  say?.(`And I parsed it!  Now, do we have a reader?`);
  const reader = spec.from_dom;
  if (!reader) {
    say?.(`No, sadly we don't. Bye!`);
    return;
  }
  say?.(`Yes, we do.  Let's read it!`);
  const results = reader(doc.documentElement);
  say?.(`I just read ${results.length} things!`);
  say?.(`Now it just remains to write them to the store.`);
  const description = spec.store;
  const options = { batch_size: 100 };
  const destination = { database: db_name, store: store_name, description };
  for await (const status of IDB.write(results, destination, options)) {
    say(status);
  }
  say?.(`All done!`);
}

// ====================================

function make_future_underlay() {
  const element = document.createElement("div");
  const now = new Date();
  const today = now.toISOString().slice(0, 10);
  const max_time = "9999-12-31";
  const date = `${today}/${max_time}`;
  element.setAttribute("data-time-underlay", "future");
  set_time_item_style(date, element.style);
  return element;
}

function make_timeframe_underlay(context) {
  const { timeframe } = context;
  if (!timeframe) throw new Error(`Bad request: missing timeframe`);
  let _sub;
  const element = document.createElement("div");
  const { style } = element;
  element.setAttribute("data-time-underlay", "timeframe");

  function _underlay_scalars_changed([from, to]) {
    style.setProperty("--major", from);
    style.setProperty("--major-to", to);
  }
  function start() {
    _sub = timeframe.watch_major_scalars(_underlay_scalars_changed);
  }
  function stop() {
    _sub?.unsubscribe();
  }
  return { start, stop, element };
}

function ensure_marker_in(ruler, date) {
  const seen = !!ruler.querySelector(`:scope>[data-time="${date}"]`);
  if (!seen) {
    const button = document.createElement("button");
    const stamp = timestamp(date);
    button.append(stamp);
    button.setAttribute("data-time", date);
    set_time_item_style(date, button.style);
    const ref = first_child_after(date, ruler);
    ruler.insertBefore(button, ref);
  }
}

// ====================================

function make_time_layers(context) {
  const doc = document;
  assert?.(context && typeof context === "object", `expected object context`);
  const { timeframe } = context;
  assert?.(timeframe, `expected timeframe`);
  const units = context.units ?? UNITS;

  const listings = new Map();
  const backdrops = new Map();
  const underlays = new Map();
  const vantages = new Map();
  const rulers = new Map();

  const layers = new Map(
    units.map(unit => {
      const outer = doc.createElement("div");
      const inner = doc.createElement("div");
      outer.classList.add("time-layer");
      inner.classList.add("time-layer-bands");
      outer.append(inner);
      outer.setAttribute("data-time-layer", unit);
      return [unit, [outer, inner]];
    })
  );

  const element = doc.createElement("div");
  element.classList.add("time-layers");
  // element.classList.add("time-layers--open");
  element.classList.add("with-child-index", "with-last-child-index");
  element.append(...Array.from(layers, ([, [ele]]) => ele));

  for (const [unit, [outer, inner]] of layers) {
    {
      const heading = doc.createElement("b");
      heading.append(`${unit}`);
      inner.append(heading);
    }

    // ruler
    {
      // breaks Day marker positioning
      // ruler_container.classList.add("time-fixed-item-collection");
      // ruler_container.classList.add("time-fixed-item-collection--margin");
      // ruler_container.classList.add("marker-band");
      // ruler_container.append(ruler);
      const ruler = doc.createElement("div");
      const ruler_container = doc.createElement("div");
      ruler.classList.add("time-markers");
      rulers.set(unit, ruler);
      inner.append(ruler);
    }

    // vantage
    {
      const lens_spec = { timeframe, unit, visit, element: outer };
      const vantage = make_time_vantage(lens_spec);
      vantages.set(unit, vantage);
    }

    // backdrop
    {
      const backdrop = make_time_backdrop();
      backdrops.set(unit, backdrop);
      inner.append(backdrop.element);
    }

    // underlays
    inner.classList.add("with-underlay");
    inner.append(make_future_underlay()); // this one will become a process too...

    // timeframe underlay
    {
      const timeframe_underlay = make_timeframe_underlay({ timeframe });
      underlays.set(unit, timeframe_underlay);
      inner.append(timeframe_underlay.element);
    }
  }

  function _ensure_finding_list_for(unit, group, element) {
    const selector = `[data-findings-unit="${unit}"][data-findings-group="${group}"]`;
    const existing = element.querySelector(selector);
    if (existing) return existing;
    const markers = doc.createElement("ul");
    markers.dataset.findingsUnit = unit;
    markers.dataset.findingsGroup = group;
    markers.classList.add("finding-markers");
    element.append(markers);
    return markers;
  }

  function visit_finding(group, record) {
    // HACK! need a generic method for this
    const date = record.aboutTime ?? record.born ?? record.died;
    if (!date) return console.warn(`no date`, record);
    for (const [unit, [outer, inner]] of layers) {
      // if (unit !== "Millennium") continue;
      const markers = _ensure_finding_list_for(unit, group, inner);
      const marker = doc.createElement("li");
      set_time_item_style(date, marker.style);
      // would like a smaller indicator than render_into
      const span = doc.createElement("span");
      span.dataset.part = "text";
      span.append(`@ ${date}`);
      marker.append(span);
      markers.append(marker);
    }
  }

  function visit(_____element, date) {
    const parsed = ISO8601.parse(date);
    assert?.(parsed, `hierarchy explorer visit got bad date ${date}`);
    if (listings.has(date)) return;
    const { unit } = parsed;
    assert?.(units.includes(unit), `hierarchy explorer bad unit`, date);

    // hardcoded here because we're interested in events specifically... yah
    const collection = "en_wiki_events";
    const index = "aboutTime";
    const query = IDBKeyRange.only(date);
    function* make_scans() {
      yield { index, query };
    }
    const listing = make_listing({ collection, make_scans });
    const time = timestamp(date);
    const div = doc.createElement("div");
    div.append(time);
    const backdrop = backdrops.get(unit);
    // DISABLED backdrops while working on other stuff
    if (false && ISO8601.compare_units(unit, "Year") >= 0) {
      const segment = backdrop.ensure_segment_for(date);
      segment.append(div, listing.element);
      listing.start();
      listings.set(date, listing);
    }

    ensure_marker_in(rulers.get(unit), date);
  }

  function start() {
    for (const vantage of vantages.values()) vantage.start();
    for (const underlay of underlays.values()) underlay.start();
  }
  function stop() {
    for (const vantage of vantages.values()) vantage.stop();
    for (const listing of listings.values()) listing.stop();
    for (const backdrop of backdrops.values()) backdrop.stop();
    for (const underlay of underlays.values()) underlay.stop();
  }

  return { start, stop, layers, element, visit, visit_finding };
}

// distribute context to start- and end-aligned layer sets
// runs only one scan at a time
function make_fringes(context) {
  const scans = new Map();
  const { timeframe, ...opts } = context;

  const head = opts.head ? make_time_layers({ timeframe }) : null;
  const foot = opts.foot
    ? make_time_layers({ timeframe, units: REVERSE_UNITS })
    : null;
  if (head) head.element.dataset.magnet = "start";
  if (foot) foot.element.dataset.magnet = "end";

  // what we're really adding is a plot (vs listing) of a growing set
  // TODO: this should be a subscription to ref deltas
  function add_ref(ref) {
    const group_name = ref; // using this as a proxy
    const scan = make_time_scan({
      // STILL HARDCODED... this should be the same as the listing source
      // except without date constraints
      db: "cronwall",
      collection: "en_wiki_events",
      ref,
      visit: visit_finding.bind(null, group_name),
      budget: 100,
      rate: 16,
    });
    scan.start();
    scans.set(ref, scan);
  }

  function visit_finding(...args) {
    head?.visit_finding(...args);
    foot?.visit_finding(...args);
  }

  function start() {
    head?.start();
    foot?.start();
  }
  function stop() {
    head?.stop();
    foot?.stop();
    for (const scan of scans.values()) scan.stop();
  }
  return { start, stop, head, foot, add_ref };
}

const get_person = (() => {
  const PERSONS = { db: "cronwall", store: "en_wiki_persons" };
  return wiki => IDB.get_by_key(PERSONS, wiki);
})();

const get_event = (() => {
  const EVENTS = { db: "cronwall", store: "en_wiki_events" };
  return id => IDB.get_by_key(EVENTS, id);
})();

// set timeframe to person's lifetime, if it's a person
async function maybe_visit_person(ref, timeframe) {
  // we have no in-memory test of whether this is in fact a person
  const maybe_person = await get_person(ref);
  if (maybe_person) {
    const { born, died } = maybe_person;
    if (born && died) {
      if (ISO8601.compare(born, died) > 0) {
        console.warn(ref, `has reversed dates:`, maybe_person);
        return;
      }
      const margin = 25; // years
      const born_year_date = ISO8601.coerce(born, "Year");
      const died_year_date = ISO8601.coerce(died, "Year");
      const born_year = parseInt(born_year_date, 10);
      const died_year = parseInt(died_year_date, 10);
      const age_years = died_year - born_year;
      const born_padded = pad4(born_year - margin);
      const died_padded = pad4(died_year + margin + age_years);
      timeframe.set(`${born_padded}/${died_padded}`);
    }
  }
}

function make_dynamic_set() {
  const set = new Set();
  const deltas = make_subscribable();
  const collection = make_subscribable();
  const has = key => set.has(key);
  function add(key) {
    if (!set.has(key)) {
      deltas.next({ delta: 1, key });
      set.add(key);
      collection.next(set);
    }
  }
  function remove(key) {
    if (set.has(key)) {
      deltas.next({ delta: -1, key });
      set.delete(key);
      collection.next(set);
    }
  }
  function dispose() {
    // close all connections
  }
  return {
    dispose,
    has,
    add,
    remove,
    deltas: deltas.public,
    stream: collection.public,
  };
}

function make_dynamic_map() {
  const map = new Map();
  const deltas = make_subscribable();
  const collection = make_subscribable();
  const has = key => map.has(key);
  const get = key => map.get(key);
  function set(key, value) {
    if (!map.has(key)) {
      deltas.next({ delta: 1, key, value });
      map.set(key, value);
      collection.next(map);
    }
  }
  function remove(key) {
    if (map.has(key)) {
      // wait till you need this...
      // const value = map.get(value)
      deltas.next({ delta: -1, key /*, value */ });
      map.delete(key);
      collection.next(map);
    }
  }
  function dispose() {
    // close all connections
  }
  return {
    dispose,
    has,
    get,
    set,
    remove,
    deltas: deltas.public,
    stream: collection.public,
    [Symbol.iterator]() {
      return map[Symbol.iterator]();
    },
  };
}

// support marking of links whose refs are a topic in the context
function support_marking_refs(context) {
  const { refs_stream, scope } = context;
  if (!refs_stream) throw new Error(`Bad request: missing refs`);
  if (!scope) throw new Error(`Bad request: missing scope`);
  let sub;

  const style = document.createElement("style");
  document.head.append(style);

  function refs_changed(/** @type Iterable<string> */ refs) {
    const selectors = Array.from(refs, ref => `${scope}[wiki="${ref}"]`);
    if (selectors.length > 0) {
      const selector = selectors.join(", ");
      const css = `${selector} { --selected: 1 }`;
      // can we not set css text directly
      style.innerHTML = css;
    } else style.innerHTML = "";
  }

  function start() {
    sub = refs_stream.subscribe({ next: refs_changed });
  }
  function stop() {
    sub?.unsubscribe();
  }
  return { start, stop };
}

// ====================================================

// For all values seen in the delta stream, keep the product of `make`
// if result is a process, start it on first sight (and stop with this lifecycle)
// if result has an element, add it to the given container
//
// ADD enter/exit set
// NOW I want something like this but with sorted insert
function memoize_over_key_changes(context) {
  const { deltas, make, element } = context;
  if (typeof make !== "function") throw new Error(`Bad request: no f'n make`);
  if (!(element instanceof Element)) throw new Error(`Bad request: !element`);
  let _cleanup = NaN; // timeout for cleaning up old items
  let sub;
  const things = new Map();
  // TODO: apply this, or some way of scoping
  const key_attribute = "people"; // kind of namespace

  {
    function do_cleanup() {
      // we might be sharing this element...
      // which means this might clean up someone else's items early
      const selector = `:scope > [data-member-state="exit"]`;
      const exit = element.querySelectorAll(selector);
      for (const element of exit) element.remove();
      _cleanup = NaN;
      // for convenience... update this here
      const count = element.childElementCount;
      element.setAttribute("data-members", count);
      element.style.setProperty("--members", count);
    }
    function queue_cleanup() {
      setTimeout(do_cleanup, 5000);
    }
    function bump_cleanup() {
      if (!isNaN(_cleanup)) {
        clearTimeout(_cleanup);
        queue_cleanup();
      }
    }
  }

  let i = 0;
  function _add(key) {
    bump_cleanup();
    if (!key) throw new Error(`Key must be truthy`);
    if (things.has(key)) return console.info(key, `already a thing`);
    const thing = make(key);
    if (!thing) return console.warn(`I got nothing`, key);
    if (thing.element) {
      element?.append(thing.element);
      thing.element.setAttribute("data-member-state", "enter");
    }
    thing.start?.();
    things.set(key, thing);
  }
  function _remove(key) {
    if (!things.has(key)) return console.info(key, `not a thing`);
    const thing = things.get(key);
    if (thing.element) thing.element.setAttribute("data-member-state", "exit");
    queue_cleanup();
    thing.stop();
    things.delete(key);
  }

  function start() {
    element.classList.add("with-enter-items", "with-exit-items");
    if (deltas) {
      sub = deltas.subscribe({
        next(the) {
          // prettier-ignore
          switch (the.delta) {
            case 1: _add(the.key); break;
            case -1: _remove(the.key); break;
          }
        },
      });
    }
  }
  function stop() {
    // ASSUMES no one else added these
    element.classList.remove("with-enter-items", "with-exit-items");
    for (const thing of things.values()) thing.stop?.();
    sub?.unsubscribe();
  }
  return { start, stop, map: things };
}

// a heaping mess that complects the state machine & dom representation.
// a sequence in which, unless it's empty, you have a position
// and you can step forward or backward
function make_sequence(get_reader) {
  let _all = [];
  let _stopped = false;
  let position = NaN; // which it is IFF _all is empty
  const stream = make_subscribable();
  const doc = document;
  const element = doc.createElement("div");
  const next = doc.createElement("button");
  const prev = doc.createElement("button");
  const item = doc.createElement("div");
  const nav = doc.createElement("nav");
  element.classList.add("time-sequence");
  item.classList.add("sequence-item");
  next.setAttribute("rel", "next"); // not official on `button`
  prev.setAttribute("rel", "prev"); // not official on `button`
  next.textContent = "next";
  prev.textContent = "prev";
  nav.classList.add("sequence-nav");
  const number = doc.createElement("span");
  /*
  const place = doc.createElement("span");
  place.append(`Item `, number);
  */
  nav.append(prev, next);
  function _ensure_elements() {
    element.append(nav, item);
  }

  // const findings = document.createElement("")

  const bof = () => position <= 0;
  const eof = () => position >= _all.length - 1;
  const get_value = () => _all[position];

  function step_forward() {
    if (eof()) return;
    ++position;
    number.textContent = position + 1;
    stream.next({ position, value: get_value() });
  }
  function step_backward() {
    if (bof()) return;
    --position;
    number.textContent = position + 1;
    stream.next({ position, value: get_value() });
  }

  function clicked(event) {
    if (!(event.target instanceof Element)) return;
    const link = event.target.closest(`[rel]`);
    if (!link) return;
    const rel = link.getAttribute("rel");
    if (rel === "prev") step_backward();
    else if (rel === "next") step_forward();
    else console.info(`unknown rel`, rel);
  }

  async function start() {
    element.addEventListener("click", clicked);

    // HACK: just buffer the whole thing
    // doing this lazily introduces too many irrelevant complications
    // I just want to see what this is like
    // so I'm going to buffer the whole sequence
    // then it really doesn't need to know about how the sequence is made
    // and it doesn't need to be reversible at the source
    element.dataset.loading = "true";
    const reader = get_reader();
    for await (const result of reader) {
      _ensure_elements();
      if (_stopped) break;
      _all.push(result);
    }
    delete element.dataset.loading;
    if (_all.length > 0) {
      position = 0;
      stream.next({ position, value: get_value() });
    }
  }
  function stop() {
    _stopped = true;
    element.removeEventListener("click", clicked);
  }
  return { start, stop, element, visited: stream.public, item };
}

// =======================================

// create a slot in the first row where you have room
// makes elements for rows but not for slots (just blocks them)
function make_time_ribbons() {
  const rows = [];
  const doc = document;
  const { compare } = ISO8601;

  const element = doc.createElement("div");
  const ribbons = doc.createElement("div");
  element.append(ribbons);
  element.classList.add("time-fixed-item-collection");
  ribbons.classList.add("time-ribbons");

  function new_row(n) {
    const ribbon = doc.createElement("div");
    ribbon.classList.add("time-ribbon");
    ribbons.append(ribbon);
    const it = { element: ribbon, slots: [] };
    rows.push(it);
    return it;
  }

  const are_non_overlapping = (a_from, a_to, b_from, b_to) =>
    compare(a_to, b_from) <= 0 || compare(b_to, a_from) <= 0;

  function can_accept(row, from, to) {
    for (const slot of row.slots)
      if (!are_non_overlapping(slot.from, slot.to, from, to)) return false;
    return true;
  }

  function first_accepting_row(from, to) {
    for (const row of rows) if (can_accept(row, from, to)) return row;
  }

  /** block in the first available slot */
  function append(from, to) {
    const row = first_accepting_row(from, to) ?? new_row();
    row.slots.push({ from, to });
    return row.element;
  }

  // but this is not a process...
  function start() {}
  function stop() {}
  return { start, stop, element, append };
}

// but it doesn't recognize a dynamic collection
function with_people_ribbons_in_centuries(context) {
  const { timeframe, list } = context;
  if (!timeframe) throw new Error("Bad request: missing timeframe");
  if (!Array.isArray(list)) throw new Error("Bad request: need array list");
  let _stopped = false;
  const PERSONS = Object.freeze({ db: "cronwall", store: "en_wiki_persons" });
  const doc = document;
  const ribbons = make_time_ribbons();

  async function start() {
    ribbons.start?.();
    for (const wiki of list) {
      const person = await IDB.get_by_key(PERSONS, wiki);
      if (_stopped) break;
      if (!person) {
        console.warn("no such person?");
        continue;
      }
      const from = person.born;
      const to = person.died;
      if (!from || !to) {
        console.warn(person, "missing dates?");
        continue;
      }
      const row = ribbons.append(from, to);
      const link = doc.createElement("a");
      link.title = `${person.summary ?? ""} (${from} – ${to})`;
      link.setAttribute("wiki", wiki);
      link.textContent = wiki_unslug(wiki);
      row.append(link, " ");
      set_time_style_and_attributes(link, from, to);
    }
  }
  function stop() {
    ribbons.stop?.();
    _stopped = true;
  }

  // const container = document.querySelector(".time-main-stage");
  const fringe = timeframe.element.querySelector(
    `[data-magnet="start"] [data-time-layer="Century"] .time-markers`
  );
  fringe?.insertAdjacentElement("afterend", ribbons.element);
  return { start, stop };
}

function visit_people_when_ref_added(context) {
  const { timeframe, ref_deltas } = context;
  if (!timeframe) throw new Error(`Bad request: timeframe expected`);
  if (!ref_deltas) throw new Error(`Bad request: ref deltas expected`);
  let sub;
  function start() {
    sub = ref_deltas.subscribe({
      next(the) {
        if (the.delta === 1) {
          maybe_visit_person(the.key, timeframe);
        }
      },
    });
  }
  function stop() {
    sub.unsubscribe();
  }
  return { start, stop };
}

async function rank_people_by_refs_impl() {
  const keys = [];
  const counts = new Map(); // wiki->ref count
  // get all people who have both birth and death dates
  const PERSONS = { db: "cronwall", store: "en_wiki_persons" };
  const EVENTS = { db: "cronwall", store: "en_wiki_events" };
  const query = IDBKeyRange.lowerBound(""); // seems to eliminate nulls
  const reader_spec = { ...PERSONS, index: "born", query };
  for await (const person of IDB.read(reader_spec)) {
    assert?.(person.born !== null, `missing birth date`, person);
    if (!person.died) continue;
    keys.push(person.wiki);
  }
  let i = 0;
  // should be a sorted set (by count)
  for (const wiki of keys) {
    i++;
    if (i % 1000 === 0) console.debug(i);
    const query = IDBKeyRange.only(wiki);
    const count_spec = { ...EVENTS, index: "refs", query };
    const count = await IDB.count(count_spec);
    counts.set(wiki, count);
  }
  const entries = [...counts];
  entries.sort(([, a], [, b]) => b - a);
  return entries;
}

async function rank_people_by_refs() {
  const KEY = "rank_people_by_refs";
  try {
    const text = localStorage.getItem(KEY);
    if (text) {
      const parsed = JSON.parse(text);
      if (parsed) return parsed;
    }
  } catch {}
  const results = await rank_people_by_refs_impl();
  try {
    const json = JSON.stringify(results);
    localStorage.setItem(KEY, json);
  } catch {}
  return results;
}

// ====================================

{
  const doc = document;

  /** @returns {string | Node} */
  function render(something) {
    return [`${something}`];
  }

  function dom_console(/** @type Element */ log) {
    log ??= document.createElement("ol");
    log.classList.add("console");
    const alt = map_object(
      console,
      name =>
        function (...args) {
          const item = doc.createElement("li");
          item.dataset.channel = name;
          item.append(...args.flatMap(render));
          log.append(item);
        }
    );
    Object.assign(alt, { element: log });
    return alt;
  }
}

// no: do stage and fringes separately
function make_main_stage({ timeframe, ...options }) {
  const use_fringes = options?.fringes ?? false;
  const element = document.createElement("div");
  const fringes = use_fringes
    ? make_fringes({ timeframe, head: true, foot: true })
    : null;

  element.classList.add("time-main-stage");
  if (fringes?.head) timeframe.element.append(fringes.head.element);
  timeframe.element.append(element);
  if (fringes?.foot) timeframe.element.append(fringes.foot.element);

  function start() {
    fringes?.start();
  }
  function stop() {
    fringes?.stop();
  }

  return { start, stop, element, fringes };
}

function add_fringe({ timeframe, align }) {}

{
  // nominate people who lived during the current time
  // element is informational
  // - one scan at a time
  // - promise would be that the current period will eventually be covered?
  const lang = "en";
  const BUDGET = 7;
  const RATE = 25;
  const { compare, between, label } = ISO8601;
  function make_person_lifespan_nominator(context) {
    const { timeframe } = context;
    if (!timeframe) throw new Error(`Bad request: missing timeframe`);
    let _from = null;
    let _to = null;
    let _sub = null;
    let _scan = null;

    const nominations = make_dynamic_map();
    const element = document.createElement("aside");
    const header = document.createElement("header");
    const heading = document.createElement("h2");
    const description = document.createElement("p");
    const status = document.createElement("output");
    element.classList.add("person-nominator");
    description.append(
      `My job is to nominate people for view.  Give me a time range.`
    );
    header.append(heading, description, status);
    element.append(header);

    function _completely_in_timeframe(person) {
      const { born, died } = person;
      return (
        born && died && compare(died, _to) <= 0 && compare(_from, born) <= 0
      );
    }
    const _born_in_frame = you => you.born && between(you.born, _from, _to);
    const _died_in_frame = you => you.died && between(you.died, _from, _to);
    const _fully_in_frame = you => _born_in_frame(you) && _died_in_frame(you);
    function _audition(person) {
      if (_from === null) return false;
      assert?.(_to !== null, `should be same as from`);
      const { born, died } = person;
      // because we stop the scan and should expect no more
      // ...but these fail a lot
      // assert?.(born, `expected born date`);
      // assert?.(between(born, _from, _to), `born ${born} ∉ ${_from}/${_to}`);
      if (!born) {
        console.warn(`expected born date`);
        return false;
      }
      if (!between(born, _from, _to)) {
        // console.warn(`born ${born} ∉ ${_from}/${_to}`);
        return false;
      }
      return died && between(died, _from, _to);
    }
    function _nominate(person) {
      if (!nominations.has(person.wiki)) nominations.set(person.wiki, person);
    }

    const persons_born = {
      db: "cronwall",
      store: "en_wiki_persons",
      index: "born",
      dir: "next", // the default
    };
    function _populate_stage_jump_to(from, to) {
      _scan?.stop();
      [_from, _to] = [from, to];
      // BAD! only okay for small N but we should use a sorted map by date
      for (const [key, person] of nominations) {
        if (!(_born_in_frame(person) || _died_in_frame(person))) {
          // removes the person “abruptly”, but downstream can provide exit
          nominations.remove(key);
        }
      }
      const within_dates = { query: safe_date_bounds(_from, _to) };
      const source = IDB.read({ ...persons_born, ...within_dates });
      const filtered = async_filter(source, _audition);
      const limited = async_take(filtered, BUDGET);
      const sink = async_map(limited, _nominate);
      _scan = CONSUME_POWER_at_rate(sink, RATE);
      _scan.start();
    }
    function _timeframe_date_changed(date) {
      // but this doesn't mean we're changing the scan...
      const [from, to] = timeframe_bounding_dates(date);
      status.textContent = `looking for people in ${label(date, lang)}`;
      // if the current scan is subsumed by etc...
      const subsumed = false;
      if (!subsumed) _populate_stage_jump_to(from, to);
    }
    function start() {
      _sub = timeframe.watch(_timeframe_date_changed);
    }
    function stop() {
      _scan?.stop();
      _sub?.unsubscribe();
    }
    return { start, stop, element, collection: nominations };
  }
}

{
  function with_show_nominations(context) {
    const { nominator } = context;
    if (!nominator) throw new Error(`Bad request: missing thing`);
    let _sub;
    const element = document.createElement("ul");
    element.classList.add("nomination-list");

    function make_item(wiki, person) {
      const item = document.createElement("li");
      const link = document.createElement("a");
      const { born, died } = person;
      item.setAttribute("wiki", wiki);
      link.setAttribute("wiki", wiki);
      link.textContent = `${wiki_unslug(wiki)} (${born} – ${died})`;
      item.append(link);
      return item;
    }

    function _delta(the) {
      const wiki = the.key;
      switch (the.delta) {
        case 1: {
          const person = the.value;
          const item = make_item(wiki, person);
          element.append(item);
          break;
        }
        case -1: {
          const existing = element.querySelector(`[wiki="${wiki}"]`);
          // console.debug(`REMOVE ${wiki}`, existing);
          assert?.(existing, `expected element for ${wiki}`);
          existing?.remove(); // abrupt, would go into exit group here
          break;
        }
        default:
          throw new Error(`Unexpected Δ: ${the.delta}`);
      }
    }
    function start() {
      _sub = nominator.collection.deltas.subscribe({ next: _delta });
    }
    function stop() {
      _sub?.unsubscribe();
    }
    return { start, stop, element };
  }
}

{
  const compare = (a, b) => (a < b ? -1 : a > b ? 1 : 0);
  const to_scalar = major_scalar_from;
  function extend_to_include(timeframe, date) {
    const [from, to] = timeframe_bounding_dates(timeframe).map(to_scalar);
    const [start, end] = timeframe_bounding_dates(date).map(to_scalar);
    const lesser = compare(start, from) < 0 ? start : from;
    const greater = compare(to, end) < 0 ? end : to;
    // return `${lesser}/${greater}`;
    return major_scalars_to_timeframe(lesser, greater);
  }
}

{
  const { entries } = Object;
  function date_index(collection) {
    // using global kb... and yes these objects are weird
    const indexes = KNOWN_CATALOG[collection]?.store.indexes;
    const date_fields = METHODS.date_fields[collection];
    if (indexes && date_fields) {
      for (const [index, spec] of entries(indexes)) {
        if (date_fields.includes(spec.keyPath)) return index;
      }
    }
  }
}

function make_multi_term_search(context) {
  const { db, collection, terms, ...options } = context;
  if (typeof db !== "string") throw new Error(`Missing db`);
  if (typeof collection !== "string") throw new Error(`Missing collection`);
  if (!terms) throw new Error(`Bad request: missing terms`);
  if (!(collection in KNOWN_CATALOG)) throw new Error(`404 ${collection}`);
  const { parse, compare_units } = ISO8601;
  const { min, max } = Math;
  const { timeframe, icon_for } = options;
  const budget = options.budget ?? 50;
  const RATE = 16;
  let _scan = null;
  let _paused = false;

  const element = document.createElement("div");
  element.classList.add("multi-term-search");
  element.classList.add("special-list");
  element.classList.add("platform--collapsible");

  element.classList.add("with-enter-items");
  const predicates = new Map();
  for (const term of terms) {
    predicates.set(term, as_predicate({ collection, term }));
  }

  function _ensure_row(term) {
    const selector = `:scope > [data-term="${term}"]`; // SANITIZE
    const existing = element.querySelector(selector);
    if (existing) return existing;
    const row = document.createElement("div");
    row.setAttribute("data-term", term);
    row.setAttribute("data-member-state", "enter");
    element.append(row);
    return row;
  }

  function major_scalars_from(date) {
    const [from, to] = timeframe_bounding_dates(date);
    const start = major_scalar_from(from);
    const end = major_scalar_from(to);
    return [start, end];
  }

  let i = 0;
  function _add_inline(term, result) {
    const row = _ensure_row(term);
    const representation = document.createElement("span");
    representation.textContent = icon_for(term) ?? `“${term}”`;
    const date = get_date(result);
    if (date) {
      // if (++i % 25 === 0) console.debug(date);
      // set_time_style_and_attributes(representation, date);
      // we don't really know whether this is for a date or a range
      representation.setAttribute("data-time", date);
      const [start, end] = major_scalars_from(date);
      representation.style.setProperty("--major", start);

      // extend timeframe to include date
      // BUT this should be done by firing an event anyway
      if (timeframe) {
        // we could also set end for single dates, but may represent precision specially
        const frame = timeframe.get_major_scalars();
        if (frame) {
          const [from, to] = frame;
          if (start < from || end > to) {
            timeframe.set_major_scalars(min(start, from), max(to, end));
          }
        } else timeframe.set_major_scalars(start, end);
      }
    }
    row.append(representation);
  }

  function _visit(result) {
    for (const [term, predicate] of predicates)
      if (predicate(result)) _add_inline(term, result);
  }

  function start() {
    const index = date_index(collection);
    if (!index) throw new Error(`No index for ${collection}`);
    const matches = as_predicate({ collection, terms });
    const predicate = it => {
      const date = get_date(it);
      if (date) {
        const parsed = parse(date);
        // things associated to larger periods aren't well represented rn
        if (parsed && compare_units(parsed.unit, "Year") > 0) return false;
      }
      return matches(it);
    };

    const spec = { db, collection, predicate, limit: budget };
    const source = scan_collection(spec);
    const sink = async_map(source, _visit);
    _scan = FEED_POWER_with_metering(sink, RATE);
    _scan.stream.subscribe({
      next(value) {
        // console.debug(value);
      },
    });
    _scan.start();
  }
  function stop() {
    _scan.stop();
  }

  function pause() {
    _scan.pause();
  }
  function unpause() {
    _scan.unpause();
  }
  function is_paused() {
    return _scan.is_paused();
  }

  const pause_api = { pause, unpause, is_paused };
  return { start, stop, element, ...pause_api };
}

let context_href = null; // stream of string or nullish, current anchor
let context_refs; // this is referenced by link interception
async function old_stuff_with_shared_timeframe() {
  context_refs = make_dynamic_set();
  context_href = make_subscribable();

  const timeframe = make_timeframe();
  timeframe.element.id = `t${Math.floor(Math.random() * 1e12)}`; // to scope css
  {
    const container = default_container();
    container.append(timeframe.element);
  }

  const main_stage = make_main_stage({ timeframe, fringes: true });
  main_stage.start();

  // support_timeframe_pointer_echo({ timeframe }).start();

  // I got rid of this but want something like it for command buttons
  // const { mover } = pan_and_zoom;

  // ORDER MATTERS between these 2
  if (true) {
    const context = { timeframe, mover, context_refs, context_href };
    const form = make_time_form(context);
    form.start();
    main_stage.element.insertAdjacentElement("beforebegin", form.element);
  }
  if (true) {
    const caption = document.createElement("header");
    caption.dataset.part = "caption";
    main_stage.element.insertAdjacentElement("beforebegin", caption);
    make_timeframe_labeler({ timeframe, element: caption }).start();
  }

  // if you don't do this, your initial timeframe is empty
  if (true) {
    const ref_deltas = context_refs.deltas;
    const it = visit_people_when_ref_added({ timeframe, ref_deltas });
    it.start();
  }

  if (true) {
    const scope = `#${timeframe.element.id} `;
    const refs_stream = context_refs.stream;
    const it = support_marking_refs({ refs_stream, scope });
    it.start();
  }

  if (false) {
    const SOURCES = ["en_wiki_persons", "en_wiki_events"];
    memoize_over_key_changes({
      element: main_stage.element,
      deltas: context_refs.deltas,
      make: ref => {
        return make_dynamic_listing({
          timeframe,
          mark: { term: wiki_unslug(ref) },
          make_source({ date }) {
            const all = SOURCES.flatMap(collection => {
              const query = { db: "cronwall", collection, ref, date };
              return _collection_search_sources(query);
            });
            return async_merge_roundrobin(all);
          },
        });
      },
    }).start();
  }

  if (false) {
    function make_source({ date }) {
      const query = { db: "cronwall", collection: "en_wiki_events", date };
      const sources = _collection_search_sources(query);
      return async_merge_roundrobin(sources);
    }
    const news = make_dynamic_listing({ timeframe, make_source });
    main_stage.element.append(news.element);
    news.start();
  }

  if (false) {
    memoize_over_key_changes({
      element: main_stage.element,
      deltas: context_refs.deltas,
      make: wiki => make_person_lifetime_mark({ timeframe, wiki }),
    }).start();
  }

  if (false) {
    memoize_over_key_changes({
      element: main_stage.element,
      deltas: context_refs.deltas,
      make: wiki => make_person_at_age_listing({ wiki }),
    }).start();
  }

  if (false) {
    memoize_over_key_changes({
      element: main_stage.element,
      deltas: context_refs.deltas,
      make: ref => {
        let _sub;
        const SOURCE = { db: "cronwall", collection: "en_wiki_events" };

        const life = make_person_lifetime_mark({ timeframe, wiki: ref });
        const sequence = make_sequence(() => {
          const query = IDBKeyRange.only(ref);
          const spec = { ...SOURCE, index: "refs", query };
          return search_collection(spec);
        });

        const element = document.createElement("article");
        const time = make_marker_block("date-block");
        const marker = make_marker_block("time-block-marker");
        const article = make_timeframe_article({ timeframe });
        const body = article.content;
        element.classList.add("person-composition");
        article.element.classList.add("composition-body");
        element.append(life.element, sequence.element);
        sequence.item.append(time.block, marker.block, article.element);

        function start() {
          life.start(); // NEW PROCESS, we have no dispose here
          sequence.start(); // NEW PROCESS, we have no dispose here

          _sub = sequence.visited.subscribe({
            next(item) {
              const { position, value } = item;
              const date = get_date(value);
              if (date) {
                const extended = extend_to_include(timeframe.get(), date);
                if (extended) timeframe.set(extended);
                const [from, to] = timeframe_bounding_dates(date);
                set_time_style_and_attributes(sequence.item, from, to);
              }
              // could just change the date content...
              time.marker.innerHTML = "";
              time.marker.append(timestamp(date));
              body.innerHTML = "";
              render_into(value, body);
            },
          });
        }
        function stop() {
          life.stop();
          sequence.stop();
          _sub?.unsubscribe();
        }
        return { start, stop, element };
      },
    }).start();
  }

  // order matters: if you want the pinned one to be *outermost*
  if (true) {
    const people_by_refs = await rank_people_by_refs();
    const list = people_by_refs.slice(0, 500).map(entry => entry[0]);
    const it = with_people_ribbons_in_centuries({ timeframe, list });
    it.start();
  }

  if (true) {
    const pinned = [];
    pinned.push("Emmett_Till");
    pinned.push("Rosa_Parks");
    pinned.push("William_Shakespeare");
    pinned.push("John_Lennon");
    pinned.push("Elizabeth_I_of_England");
    pinned.push("William_Blake");
    pinned.push("Miles_Davis");
    pinned.push("Galileo_Galilei");
    pinned.push("Isaac_Newton");
    pinned.push("Tycho_Brahe");
    pinned.push("Nicolaus_Copernicus");
    pinned.push("Johannes_Kepler");
    pinned.forEach(ref => context_refs.add(ref));
  }

  main_stage.element.classList.add("with-underlay", "time-main-stage--fixed");
  main_stage.element.classList.add("with-enter-items", "with-exit-items");
  main_stage.element.classList.add("with-hover-item");
  main_stage.element.append(make_future_underlay());

  if (true) {
    const deltas = make_subscribable(); // {delta: 1 | -1, key: wiki ref}
    const nominator = make_person_lifespan_nominator({ timeframe, deltas });
    const shower = with_show_nominations({ nominator });
    nominator.start();
    // nominator.element.append(shower.element);
    // shower.start();
    // stage.element.append(nominator.element);

    if (true) {
      memoize_over_key_changes({
        element: main_stage.element,
        deltas: nominator.collection.deltas,
        make: wiki => {
          return make_person_lifetime_mark({ timeframe, wiki });
        },
      }).start();
    }
  }

  // there can be only one href at a time
  if (true) {
    const lang = "en";
    const SOURCE = { db: "cronwall", collection: "en_wiki_events" };
    const container = main_stage.element;
    context_href.public.subscribe({
      next(href) {
        const existing = container.querySelector(`.href`);
        if (existing) {
          existing.classList.remove("href");
          const sequence = existing.querySelector(".time-sequence");
          container.classList.remove("has-href");
          sequence.remove();
        }

        const now = container.querySelector(`[wiki="${href}"]`);
        now?.classList.add("href");
        if (now) {
          container.classList.add("has-href");
          const sequence = make_sequence(() => {
            const query = IDBKeyRange.only(href);
            const spec = { ...SOURCE, index: "refs", query };
            return search_collection(spec);
          });

          const element = document.createElement("article");
          const time = make_marker_block("date-block");
          const marker = make_marker_block("time-block-marker");
          const article = make_timeframe_article({ timeframe });
          const body = article.content;
          sequence.item.append(time.block, marker.block, article.element);
          let _sub = sequence.visited.subscribe({
            next(item) {
              const { position, value } = item;
              const date = get_date(value);
              if (date) {
                const extended = extend_to_include(timeframe.get(), date);
                if (extended) timeframe.set(extended);
                const [from, to] = timeframe_bounding_dates(date);
                set_time_style_and_attributes(sequence.item, from, to);
              }
              // could just change the date content...
              time.marker.innerHTML = "";
              time.marker.append(timestamp(date));
              body.innerHTML = "";
              render_into(value, body);
            },
          });

          now.append(sequence.element);
          article.start();
          sequence.start();
        }
        if (false && now) {
          const split = document.createElement("div");
          const one = document.createElement("div");
          const two = document.createElement("div");
          split.append(one, two);
          /*
          const iframe = document.createElement("iframe");
          iframe.src = wiki_href(href, lang);
          one.append(iframe);
*/
          now.append(split);
        }
      },
    });
  }

  timeframe.start();
  const mainiac = { timeframe, mover, main_stage };
  Object.assign(globalThis, mainiac);
}
const ICONS_WITH_TERMS = [
  ["✝", "latin cross"],
  ["📻", "radio"],
  ["📺", "television"],
  ["📟", "pager"],
  ["📹", "video", "video camera"],
  ["🎞", "film", "film frames"],
  ["📽", "projector", "film projector"],
  ["📀", "dvd"],
  ["💾", "floppy disk"],
  ["📷", "camera"],
  ["📞", "telephone", "telephone receiver"],
  ["🕑", "clock", "clock face two oclock"],
  ["🚗", "automobile"],
  ["🚲", "bicycle"],
  ["🏍", "motorcycle", "racing motorcycle"],
  ["🚂", "locomotive", "steam locomotive"],
  // TEMP, need to use regex \bships?\b
  // ["🚢", "ship"],
  ["⛵", "sailboat"],
  ["✈", "airplane"],
  ["🚎", "trolley", "trolleybus"],
  ["💡", "lightbulb", "electric light bulb"],
  ["🪛", "screwdriver"],
  ["🔨", "hammer"],
  ["🏎", "racecar", "racing car"],
  ["💻", "computer", "personal computer"],
  ["🖥", "desktop computer"],
  ["🏀", "basketball", "basketball and hoop"],
  ["🏈", "football", "american football"],
  ["⚽", "soccer", "soccer ball"],
  ["⚾", "baseball"],
  ["🗽", "statue of liberty"],
  ["🌳", "tree", "deciduous tree"],
  ["🌊", "ocean"],
  ["🚽", "toilet"],
  ["🧼", "soap", "bar of soap"],
  ["🗺", "map", "world map"],
  ["🔥", "fire"],
  // ["☉", "sun"],
  ["☀️", "sun", "black sun with rays"],
  // the above is some composition, versus just ☀:
  // Composed with the following character(s) "️" using this font:
  //   ftcrhb:-GOOG-Noto Color Emoji-normal-normal-normal-*-43-*-*-*-m-0-iso10646-1
  // by these glyphs:
  //   [0 1 9728 59 54 0 54 40 11 nil]
  /*
    ["☿", "mercury"], // the planet? the god? the element?
    ["♃", "jupiter"],
    ["♄", "saturn"],
    ["♆", "neptune"],
    ["♅", "uranus"],
    ["♇", "pluto"], // will include plutonium
*/
  // ["🌍", "Africa"],
  // https://www.reddit.com/r/vinyl/comments/45neoa/there_is_no_emoji_for_vinyl_records_or_record
];
const ICONS_WITH_REGEXES = [
  ["⚔", /\bwars?\b/, "crossed swords"],
  ["📯", /\bhorns?\b/, "postal horn"],
];

function make_iconic_term_search(context) {
  // but it could be either one!
  const SOURCE = { db: "cronwall", collection: "en_wiki_events" };

  // `make_multi_term_search` only accepts a static set
  const icon_for = s => ICONS_WITH_TERMS.find(([, t]) => t === s)?.[0];
  const terms = ICONS_WITH_TERMS.map(([, term]) => term);
  const spec = { ...SOURCE, ...context, terms, icon_for };
  return make_multi_term_search(spec);
}

// intercept additions to container/collection
// and shunt them into, e.g. centuried, decades, etc, & bump count
// idea being to hide children, reduce layout burden
// ideally this would apply to true clusters, irresp any grid
// but there is some upside to the grid (it's orienting)
// yes, intercepting would be less thrashy than moving after the fact
// but how do you intercept?  if an event handler, then how to be sure you win?
// but I know for sure that I can detect mutations

// classify or quantize?
{
  function tally_groups(spec) {
    const { element, attribute, classify } = spec;
    if (!(element instanceof Element)) throw new Error(`Bad or no element`);
    if (typeof attribute !== "string") throw new Error(`need string attribute`);
    if (typeof classify !== "function") throw new Error(`no f'n classify`);
    let _observer = null;

    function _quantize(node) {
      const group = classify(node);
      return group;
    }

    function _ensure_container(key) {
      const selector = `[${attribute}="${key}"]`; // SANITIZE
      const matched = element.querySelector(selector);
      if (matched) return matched;
      const block = document.createElement("div");
      block.setAttribute(attribute, key);
      element.append(block);
      return block;
    }

    function _ensure_count_element(key) {
      const group = _ensure_container(key);
      const existing = group.querySelector(`:scope > [data-part="tally"]`);
      if (existing) return existing;
      const count = document.createElement("span");
      count.setAttribute("data-part", "tally");
      group.prepend(count);
      return count;
    }

    const COUNT = "data-count";
    const COUNT_FORMAT = new Intl.NumberFormat(undefined, {
      useGrouping: true,
    });
    function _bump_count(key, by = 1) {
      const count_element = _ensure_count_element(key);
      let old_count = 0;
      if (count_element.hasAttribute(COUNT)) {
        const text = count_element.getAttribute(COUNT);
        old_count = parseInt(text, 10);
        if (isNaN(old_count)) return console.warn(`bad ${COUNT}: ${text}`);
      }
      const new_count = old_count + by;
      count_element.textContent = COUNT_FORMAT.format(new_count);
      count_element.setAttribute(COUNT, new_count);
    }

    function _delta(node, sign) {
      if (node.hasAttribute(attribute)) return; // it's one of ours
      const key = _quantize(node);
      if (!key) return;
      if (sign === 1) {
        _ensure_container(key).append(node);
        _bump_count(key, sign);
      } else {
        // ignore.  so... this is asymmetrical and we have no business removing nodes.
      }
    }

    function start() {
      _observer = new MutationObserver(function _children_changed(records) {
        for (const record of records) {
          if (record.type === "childList") {
            const { addedNodes, removedNodes } = record;
            for (const node of addedNodes) _delta(node, 1);
            for (const node of removedNodes) _delta(node, -1);
          }
        }
      });
      _observer.observe(element, { childList: true });
      for (const child of element.children) _delta(child, 1);
    }
    function stop() {
      _observer?.disconnect();
    }
    return { start, stop };
  }
}

const default_container = (d = document) => d.querySelector("main") ?? d.body;

{
  const lang = "en";
  function group_containers_have_labels(context) {
    const { element, attribute } = context;
    if (!(element instanceof Element)) throw new Error(`Bad or no element`);
    if (typeof attribute !== "string") throw new Error(`need string attribute`);
    let _observer = null;

    const _get_group = key => element.querySelector(`[${attribute}="${key}"]`);

    function _get_existing_label(key) {
      const group = _get_group(key);
      if (!group) throw new Error(`I kinda expected this to exist`);
      return group.querySelector(`:scope > [data-part="label"]`);
    }

    function _ensure_label(key) {
      const existing = _get_existing_label(key);
      if (existing) return existing;
      const group = _get_group(key);
      if (!group) throw new Error(`I kinda expected this to exist`);
      const label = document.createElement("span");
      label.setAttribute("data-part", "label");
      group.append(label);
      return label;
    }

    function _delta(element, sign) {
      if (!element.hasAttribute(attribute)) return;
      const key = element.getAttribute(attribute);
      if (sign === 1) {
        const label = _ensure_label(key);
        const date = key; // um... hehe just assume this...
        const date_label = ISO8601.label(date, lang);
        const tally = element.querySelector(`[data-part="tally"]`);
        if (tally) {
          label.textContent = ` in ${date_label}`;
          tally.insertAdjacentElement("afterend", label);
        } else {
          label.textContent = date_label;
          element.prepend(label);
        }
      } else _get_existing_label(key)?.remove();
    }

    function start() {
      _observer = new MutationObserver(function _standby(records) {
        for (const record of records) {
          if (record.type === "childList") {
            const { addedNodes, removedNodes } = record;
            for (const node of addedNodes) _delta(node, 1);
            for (const node of removedNodes) _delta(node, -1);
          }
        }
      });
      _observer.observe(element, { childList: true });
      for (const child of element.children) _delta(child, 1);
    }
    function stop() {
      _observer?.disconnect();
    }
    return { start, stop };
  }
}

function make_tally_groups_for_all_terms(context) {
  const { element } = context;
  if (!(element instanceof Element)) throw new Error(`bad element`);
  // no more books I say!  but for now...
  const talliers = new Map();
  const UNIT = "Century";
  const attribute = `data-term`;
  let _observer;

  function classify(element) {
    if (!element) return;
    const date = element.getAttribute("data-time");
    if (!date) return;
    const coerced = ISO8601.coerce(date, UNIT);
    if (coerced) return coerced;
  }

  function _delta(node, sign) {
    if (!node.hasAttribute(attribute)) return;
    const key = node.getAttribute(attribute);
    if (sign === 1) {
      const attribute = `data-tally-for`;
      const tallier = tally_groups({ element: node, attribute, classify });
      talliers.set(key, tallier);
      tallier.start();
      // off books!
      group_containers_have_labels({ element: node, attribute }).start();
    } else {
      // if you really do this... then I guess the tallier would have to restore
      // the elements to the parent container??
      const tallier = talliers.get(key);
      talliers.delete(key);
      if (!tallier) return;
      tallier.stop();
      tallier.element.remove();
    }
  }

  function _mutation(records) {
    for (const record of records) {
      if (record.type === "childList") {
        const { addedNodes, removedNodes } = record;
        for (const node of addedNodes) _delta(node, 1);
        for (const node of removedNodes) _delta(node, -1);
      }
    }
  }

  function start() {
    const _observer = new MutationObserver(_mutation);
    _observer.observe(element, { childList: true });
    for (const child of element.children) _delta(child, 1);
  }
  function stop() {
    _observer?.disconnect();
  }
  return { start, stop };
}

function old_main() {
  const timeframe = make_timeframe();
  const stage = document.createElement("div");

  stage.classList.add("time-main-stage");
  stage.classList.add("time-main-stage--fixed", "fullscreen");
  timeframe.element.append(stage);
  timeframe.start();

  {
    // default_container().append(timeframe.element);
    const header = document.querySelector("main > header");
    header.insertAdjacentElement("afterend", timeframe.element);
  }

  const iconic_terms = make_iconic_term_search({ timeframe });
  iconic_terms.start();
  stage.append(iconic_terms.element);

  {
    const { element } = iconic_terms;
    const it = make_tally_groups_for_all_terms({ element });
    // it.start();
  }

  function forall_children(element) {}
  forall_children({ in: iconic_terms.element });

  {
    const pause_button = document.createElement("button");
    pause_button.textContent = "pause";
    pause_button.onclick = () => {
      if (iconic_terms.is_paused()) {
        iconic_terms.unpause();
        pause_button.textContent = "pause";
      } else {
        iconic_terms.pause();
        pause_button.textContent = "unpause";
      }
    };
    iconic_terms.element.insertAdjacentElement("beforebegin", pause_button);
  }

  {
    const COLLAPSED = "platform--collapsed";
    const button = document.createElement("button");
    button.textContent = "collapse";
    button.onclick = () => {
      const collapsed = iconic_terms.element.classList.contains(COLLAPSED);
      button.textContent = collapsed ? "uncollapse" : "collapse";
      iconic_terms.element.classList.toggle(COLLAPSED);
    };
    iconic_terms.element.insertAdjacentElement("beforebegin", button);
  }
  {
    const COLLAPSED = "platform--collapsed-1";
    const button = document.createElement("button");
    button.textContent = "collapse 1";
    button.onclick = () => {
      const collapsed = iconic_terms.element.classList.contains(COLLAPSED);
      button.textContent = collapsed ? "uncollapse 1" : "collapse 1";
      iconic_terms.element.classList.toggle(COLLAPSED);
    };
    iconic_terms.element.insertAdjacentElement("beforebegin", button);
  }
  // support_timeframe_pointer_echo({ timeframe }).start();
}

function broaden_search(prompt) {
  // case-insensitive match (slugified) against (wiki) refs
  // baseball → Baseball, james dean → James_Dean
  // combined with a plain-text search
  // that rank matches by word affinity (start, end, middle)
}

function they_went_thataway() {
  // when multiple actors are moved offstage
  // lump them all together under one
}
const JAMES_DEAN_TEXT = `
Main menu

Wikipedia The Free Encyclopedia
Search

Personal tools

Toggle the table of contents

James Dean

    Article
    Talk

Tools

Page semi-protected
From Wikipedia, the free encyclopedia
This article is about the American actor. For other uses, see James Dean (disambiguation).
James Dean
Dean in Rebel Without a Cause wearing a bomber jacket and Lee jeans, circa 1955
Dean in Rebel Without a Cause, 1955
Born	James Byron Dean

February 8, 1931
Marion, Indiana, U.S.
Died	September 30, 1955 (aged 24)
Cholame, California, U.S.
Cause of death	Car accident
Resting place	Park Cemetery, Fairmount, Indiana
Education	

    Santa Monica College
    UCLA

Occupation	Actor
Years active	1950–1955
Website	jamesdean.com
Signature

James Byron Dean (February 8, 1931 – September 30, 1955) was an American actor with a career that lasted five years. He is regarded as a cultural icon of teenage disillusionment and social estrangement, as expressed in the title of his most celebrated film Rebel Without a Cause (1955), in which he starred as troubled teenager Jim Stark. The other two roles that defined his stardom were loner Cal Trask in East of Eden (1955) and surly ranch hand Jett Rink in Giant (1956).

Dean died died in a car crash on September 30, 1955[1] and became the first actor to receive a posthumous Academy Award nomination for Best Actor for his role in East of Eden. He received a second nomination for his role in Giant the following year, making him the only actor to have had two posthumous acting nominations.[2] In 1999, the American Film Institute ranked him the 18th best male movie star of Golden Age Hollywood in AFI's 100 Years...100 Stars list.[3]
Early life and education

James Byron Dean was born on February 8, 1931, at the Seven Gables apartment on the corner of 4th Street and McClure Street in Marion, Indiana,[4] the only child of Mildred Marie Wilson and Winton Dean. He claimed that his mother was partly Native American, and that his father belonged to a "line of original settlers that could be traced back to the Mayflower".[5] Six years after his father had left farming to become a dental technician, Dean moved with his family to Santa Monica, California. He was enrolled at Brentwood Public School in the Brentwood neighborhood of Los Angeles, but transferred soon afterward to the McKinley Elementary School.[6] The family spent several years there, and by all accounts, Dean was very close to his mother. According to Michael DeAngelis, she was "the only person capable of understanding him".[7] In 1938, Dean's mother was suddenly struck with acute stomach pain and quickly began to lose weight. She died of uterine cancer when Dean was nine years old.[6] Unable to care for his son, Dean's father sent him to live with his aunt and uncle, Ortense and Marcus Winslow, on their farm in Fairmount, Indiana,[8] where he was raised in their Quaker household.[9] Dean's father served in World War II and later remarried.[citation needed]

In his adolescence, Dean sought the counsel and friendship of a local Methodist pastor, the Rev. James DeWeerd, who seems to have had a formative influence upon Dean, especially upon his future interests in bullfighting, car racing, and theater.[10] According to Billy J. Harbin, Dean had "an intimate relationship with his pastor, which began in his senior year of high school and endured for many years".[11][12] An alleged sexual relationship was suggested in Paul Alexander's 1994 book Boulevard of Broken Dreams: The Life, Times, and Legend of James Dean.[13] In 2011, it was reported that Dean once confided in Elizabeth Taylor that he was sexually abused by a minister approximately two years after his mother's death.[14] Other reports on Dean's life also suggest that he was sexually abused by DeWeerd either as a child or as a late teenager.[12][13]

Dean's overall performance in school was exceptional and he was a popular student. He played on the baseball and varsity basketball teams, studied drama, and competed in public speaking through the Indiana High School Forensic Association.[15][16] After graduating from Fairmount High School in May 1949,[17] he moved back to California with his dog, Max, to live with his father and stepmother.[citation needed] Dean enrolled in Santa Monica College and majored in pre-law. He transferred to University of California, Los Angeles (UCLA) for one semester[18] and changed his major to drama,[19] which resulted in estrangement from his father. He pledged the Sigma Nu fraternity but was never initiated.[20] While at UCLA, Dean was picked from a group of 350 actors to portray Malcolm in Macbeth.[21] At that time, he also began acting in James Whitmore's workshop. In January 1951, he dropped out of UCLA to pursue a full-time career as an actor.[22][23]
Acting career
Early career
Dean in 1953 (aged 22)

Dean's debut television appearance was in a Pepsi commercial.[24][25][26] He quit college to act full-time and was cast in his first speaking part, as John the Apostle in Hill Number One, an Easter television special dramatizing the Resurrection of Jesus.[27] Dean worked at the widely filmed Iverson Movie Ranch in the Chatsworth area of Los Angeles during production of the program, for which a replica of the tomb of Jesus was built on location at the ranch. Dean subsequently obtained three walk-on roles in movies: as a soldier in Fixed Bayonets! (1951), a boxing cornerman in Sailor Beware (1952),[28] and a youth in Has Anybody Seen My Gal? (1952).[29] While struggling to gain roles in Hollywood, Dean also worked as a parking lot attendant at CBS Studios, during which time he met Rogers Brackett,[30] a radio director for an advertising agency, who offered him professional help and guidance in his chosen career, as well as a place to stay.[31][32] Brackett opened doors for Dean and helped him land his first starring role on Broadway in See the Jaguar.[33]

In July 1951, Dean appeared on Alias Jane Doe, which was produced by Brackett.[34][32] In October 1951, following the encouragement of actor James Whitmore and the advice of his mentor Rogers Brackett, Dean moved to New York City. There, he worked as a stunt tester for the game show Beat the Clock, but was subsequently fired for allegedly performing the tasks too quickly.[35] He also appeared in episodes of several CBS television series, The Web, Studio One, and Lux Video Theatre, before gaining admission to the Actors Studio to study method acting under Lee Strasberg.[36] In 1952, he had a nonspeaking bit part as a pressman in the movie Deadline – U.S.A., starring Humphrey Bogart.[37][38]

Proud of these accomplishments, Dean referred to the Actors Studio in a 1952 letter to his family as "the greatest school of the theater. It houses great people like Marlon Brando, Julie Harris, Arthur Kennedy, Mildred Dunnock, Eli Wallach... Very few get into it ... It is the best thing that can happen to an actor. I am one of the youngest to belong."[31] There, he was classmates and close friends with Carroll Baker, alongside whom he would eventually star in Giant (1956). Dean's career picked up and he performed in further episodes of such early 1950s television shows as Kraft Television Theatre, Robert Montgomery Presents, The United States Steel Hour, Danger, and General Electric Theater. One early role, for the CBS series Omnibus in the episode "Glory in the Flower", saw Dean portraying the type of disaffected youth he would later portray in Rebel Without a Cause (1955). This summer 1953 program featured the song "Crazy Man, Crazy", one of the first dramatic TV programs to feature rock and roll.

Positive reviews for Dean's 1954 theatrical role as Bachir, a pandering homosexual North African houseboy, in an adaptation of André Gide's book The Immoralist (1902), led to calls from Hollywood.[39] During the production of The Immoralist, Dean had an affair with actress Geraldine Page.[40] Angelica Page said of their relationship,

    "According to my mother, their affair went on for three-and-a-half months. In many ways my mother never really got over Jimmy. It was not unusual for me to go to her dressing room through the years, obviously many years after Dean was gone, and find pictures of him taped up on her mirror. My mother never forgot about Jimmy -- never. I believe they were artistic soul mates."[40]

Page remained friends with Dean until his death and kept a number of personal mementos from the play—including several drawings by him.[41]
Dean in East of Eden (1955)
East of Eden

In 1953, director Elia Kazan was looking for a substantive actor to play the emotionally complex role of Cal Trask, for screenwriter Paul Osborn's adaptation of John Steinbeck's 1952 novel East of Eden. This book deals with the story of the Trask and Hamilton families over the course of three generations, focusing especially on the lives of the latter two generations in Salinas Valley, California, from the mid-19th century through the 1910s. In contrast to the book, the film script focused on the last portion of the story, predominantly with the character of Cal. Though he initially seems more aloof and emotionally troubled than his twin brother Aron, Cal is soon seen to be more worldly, business savvy, and even sagacious than their pious and constantly disapproving father (played by Raymond Massey) who seeks to invent a vegetable refrigeration process. Cal is bothered by the mystery of their supposedly dead mother, and discovers she is still alive and a brothel-keeping 'madam'; the part was played by actress Jo Van Fleet.[42]

Before casting Cal, Elia Kazan said that he wanted "a Brando" for the role and Osborn suggested Dean, a relatively unknown young actor. Dean met with Steinbeck, who did not like the moody, complex young man personally, but thought him to be perfect for the part. Dean was cast in the role and on April 8, 1954, left New York City and headed for Los Angeles to begin shooting.[43][44][45]

Much of Dean's performance in the film was unscripted,[46] including his dance in the bean field and his fetal-like posturing while riding on top of a train boxcar (after searching out his mother in nearby Monterey). The best-known improvised sequence of the film occurs when Cal's father rejects his gift of $5,000, money Cal earned by speculating in beans before the US became involved in World War I. Instead of running away from his father as the script called for, Dean instinctively turned to Massey and in a gesture of extreme emotion, lunged forward and grabbed him in a full embrace, crying. Kazan kept this and Massey's shocked reaction in the film. Dean's performance in the film foreshadowed his role as Jim Stark in Rebel Without A Cause. Both characters are angst-ridden protagonists and misunderstood outcasts, desperately craving approval from their fathers.[47] In recognition of his performance in East of Eden, Dean was nominated posthumously for the 1956 Academy Awards as Best Actor in a Leading Role of 1955, the first official posthumous acting nomination in Academy Awards history.[48] (Jeanne Eagels was nominated for Best Actress in 1929,[49] when the rules for selection of the winner were different.) East of Eden was the only film starring Dean released in his lifetime.[50][51]
Rebel Without a Cause, Giant and planned roles
Natalie Wood and James Dean in Rebel Without a Cause
Natalie Wood and Dean in Rebel Without a Cause (1955)

Dean quickly followed up his role in Eden with a starring role as Jim Stark in Rebel Without a Cause (1955), a film that would prove to be hugely popular among teenagers. The film has been cited as an accurate representation of teenage angst.[52][53] Following East of Eden and Rebel Without a Cause, Dean wanted to avoid being typecast as a rebellious teenager like Cal Trask or Jim Stark, and hence took on the role of Jett Rink, a Texan ranch hand who strikes oil and becomes wealthy, in Giant, a posthumously released 1956 film. The movie portrays a number of decades in the lives of Bick Benedict, a Texas rancher, played by Rock Hudson; his wife, Leslie, played by Elizabeth Taylor; and Rink.[54] To portray an older version of his character in the film's later scenes, Dean dyed his hair gray and shaved some of it off to give himself a receding hairline.

Giant would prove to be Dean's last film. At the end of the film, Dean was supposed to make a drunken speech at a banquet; this is nicknamed the 'Last Supper' because it was the last scene before his sudden death. Due to his desire to make the scene more realistic by actually being inebriated for the take, Dean mumbled so much that director George Stevens decided the scene had to be overdubbed by Nick Adams, who had a small role in the film, because Dean had died before the film was edited.

Dean received his second posthumous Best Actor Academy Award nomination for his role in Giant at the 29th Academy Awards in 1957 for films released in 1956.[2]

Having finished Giant, Dean was set to star as Rocky Graziano in a drama film, Somebody Up There Likes Me (1956), and, according to Nicholas Ray himself, he was going to do a story called Heroic Love with the director.[55] Dean's death terminated any involvement in the projects but Somebody Up There Likes Me still went on to earn both commercial and critical success, winning two Oscars and grossing $3,360,000, with Paul Newman playing the role of Graziano.
Personal life

Screenwriter William Bast was one of Dean's closest friends, a fact acknowledged by Dean's family.[56] According to Bast, he was Dean's roommate at UCLA and later in New York, and knew Dean throughout the last five years of his life.[57] While at UCLA, Dean dated Beverly Wills, an actress with CBS, and Jeanette Lewis, a classmate. Bast and Dean often double-dated with them. Wills began dating Dean alone, later telling Bast, "Bill, there's something we have to tell you. It's Jimmy and me. I mean, we're in love."[58]: 71  They broke up after Dean "exploded" when another man asked her to dance while they were at a function.[58]: 74  Bast, who was also Dean's first biographer,[59] would not confirm whether he and Dean had a sexual relationship until 2006.[57][60][61] In his book Surviving James Dean, Bast was more open about the nature of his relationship with Dean, writing that they had been lovers one night while staying at a hotel in Borrego Springs.[62] In his book, Bast also described the difficult circumstances of their involvement.

In 1996, actress Liz Sheridan detailed her relationship with Dean in New York in 1952, saying it was "just kind of magical.[63] It was the first love for both of us."[64] Sheridan published her memoir, Dizzy & Jimmy: My Life with James Dean; A Love Story, in 2000.

While living in New York, Dean was introduced to actress Barbara Glenn by their mutual friend Martin Landau.[65] They dated for two years, often breaking up and getting back together.[65] In 2011, their love letters were sold at auction for $36,000.[66]

Early in Dean's career, after Dean signed his contract with Warner Brothers, the studio's public relations department began generating stories about Dean's liaisons with a variety of young actresses who were mostly drawn from the clientele of Dean's Hollywood agent, Dick Clayton. Studio press releases also grouped Dean together with two other actors, Rock Hudson and Tab Hunter, identifying each of the men as an 'eligible bachelor' who had not yet found the time to commit to a single woman: "They say their film rehearsals are in conflict with their marriage rehearsals."[67]

Dean's best-remembered relationship was with young Italian actress Pier Angeli. He met Angeli while she was shooting The Silver Chalice (1954)[68] on an adjoining Warner lot, and they exchanged items of jewelry as love tokens.[69] Angeli, during an interview fourteen years after their relationship ended, described their times together:

    We used to go together to the California coast and stay there secretly in a cottage on a beach far away from prying eyes. We'd spend much of our time on the beach, sitting there or fooling around, just like college kids. We would talk about ourselves and our problems, about the movies and acting, about life and life after death. We had a complete understanding of each other. We were like Romeo and Juliet, together and inseparable. Sometimes on the beach we loved each other so much we just wanted to walk together into the sea holding hands because we knew then that we would always be together.[58]: 196 

Dean was quoted saying about Angeli, "Everything about Pier is beautiful, especially her soul. She doesn't have to be all gussied up. She doesn't have to do or say anything. She's just wonderful as she is. She has a rare insight into life."[70]
Dean in 1955

Those who believed Dean and Angeli were deeply in love claimed that a number of forces led them apart. Angeli's mother disapproved of Dean's casual dress and what were, for her at least, unacceptable behavior traits: his T-shirt attire, late dates, fast cars, drinking, and the fact that he was not a Catholic. Her mother said that such behavior was not acceptable in Italy. In addition, Warner Bros., where he worked, tried to talk him out of marrying and he himself told Angeli that he did not want to get married.[58]: 197  Richard Davalos, Dean's East of Eden co-star, claimed that Dean in fact wanted to marry Angeli and was willing to allow their children to be brought up Catholic.[71] An Order for the Solemnization of Marriage pamphlet with the name "Pier" lightly penciled in every place the bride's name is left blank was found amongst Dean's personal effects after his death.[72]

Some commentators, such as William Bast and Paul Alexander, believe the relationship was a mere publicity stunt.[73][74] In his autobiography, Elia Kazan, the director of East of Eden, dismissed the notion that Dean could possibly have had any success with women, although he remembered hearing Dean and Angeli loudly making love in Dean's dressing room.[75] Kazan was quoted by author Paul Donnelley as saying about Dean, "He always had uncertain relations with girlfriends."[76] Pier Angeli talked only once about the relationship in her later life in an interview, giving vivid descriptions of romantic meetings at the beach. Dean biographer John Howlett said these read like wishful fantasies,[77] as Bast claims them to be.[31]

After finishing his role for East of Eden, Dean took a brief trip to New York in October 1954.[58]: 197  While he was away, Angeli unexpectedly announced her engagement to Italian-American singer Vic Damone. The press was shocked and Dean expressed his irritation.[78] Angeli married Damone the following month. Gossip columnists reported that Dean watched the wedding from across the road on his motorcycle, even gunning the engine during the ceremony, although Dean later denied doing anything so "dumb".[58] Joe Hyams, in his 1992 biography of Dean, James Dean: Little Boy Lost, claims that he visited Dean just as Angeli, then married to Damone, was leaving his home. Dean was crying and allegedly told Hyams she was pregnant, with Hyams concluding that Dean believed the child might be his. Angeli, who divorced Damone and then her second husband, the Italian film composer Armando Trovajoli, was said by friends in the last years of her life to claim that Dean was the love of her life. She died from an overdose of barbiturates in 1971, at the age of 39.[79]

Dean also dated Swiss actress Ursula Andress.[80] "She was seen riding around Hollywood on the back of James's motorcycle," writes biographer Darwin Porter. She was also seen with Dean in his sports cars, and was with him on the day he bought the car that he died in.[81]
Death
Main article: Death of James Dean
Auto racing hobby
Dean and his Porsche Super Speedster 23F at Palm Springs Races March 1955

In 1954, Dean became interested in developing a career in motorsport. He purchased various vehicles after filming for East of Eden had concluded, including a Triumph Tiger T110 and a Porsche 356.[82][83] Just before filming began on Rebel Without a Cause, he competed in his first professional event at the Palm Springs Road Races, which was held in Palm Springs, California, on March 26–27, 1955. Dean achieved first place in the novice class, and second place at the main event. His racing continued in Bakersfield a month later, where he finished first in his class and third overall.[84] Dean hoped to compete in the Indianapolis 500, but his busy schedule made it impossible.[85]

Dean's final race occurred in Santa Barbara on Memorial Day, May 30, 1955. He was unable to finish the competition due to a blown piston.[84][86] His brief career was put on hold when Warner Brothers barred him from all racing during the production of Giant.[87] Dean had finished shooting his scenes and the movie was in post-production when he decided to race again.
Accident and aftermath
The intersection of State Route 46 and State Route 41 was renamed "James Dean Memorial Junction". However the actual accident location is approximately 100 feet (0.019 mi) to the south, due to road realignment.

Longing to return to the "liberating prospects" of motor racing, Dean traded in his Speedster for a new, more powerful and faster 1955 Porsche 550 Spyder and entered the upcoming Salinas Road Race event scheduled for October 1–2, 1955.[88] Accompanying the actor on his way to the track on September 30 were stunt coordinator Bill Hickman, Collier's photographer Sanford Roth, and Rolf Wütherich, the German mechanic from the Porsche factory who maintained Dean's Spyder, "Little Bastard" car.[89][90] Wütherich, who had encouraged Dean to drive the car from Los Angeles to Salinas to break it in, accompanied Dean in the Porsche. At 3:30 p.m., Dean was ticketed for speeding, as was Hickman, who was following behind in another car.[91]

As the group was driving westbound on U.S. Route 466 (currently SR 46) near Cholame, California, at approximately 5:45 p.m.,[92] a 1950 Ford Tudor, driven by 23-year-old California Polytechnic State University student Donald Turnupseed, was travelling east. Turnupseed made a left turn onto Highway 41 headed north, toward Fresno[93] ahead of the oncoming Porsche.[89][94][95]

Dean, unable to stop in time, slammed into the passenger side of the Ford, resulting in Dean's car bouncing across the pavement onto the side of the highway. Dean's passenger, Wütherich, was thrown from the Porsche, while Dean was trapped in the car and sustained numerous fatal injuries, including a broken neck.[96] Turnupseed exited his damaged vehicle with minor injuries.

The accident was witnessed by a number of passersby who stopped to help. Dean's biographer George Perry wrote that a woman with nursing experience attended to Dean and detected a weak pulse, but he also contrarily wrote that "death appeared to have been instantaneous".[96] Dean was pronounced dead on arrival shortly after he arrived by ambulance at the Paso Robles War Memorial Hospital at 6:20 p.m.[97]

Though initially slow to reach newspapers in the Eastern United States, details of Dean's death rapidly spread via radio and television. By October 2, his death had received significant coverage from domestic and foreign media outlets.[98][99] Dean's funeral was held on October 8, 1955, at the Fairmount Friends Church in Fairmount, Indiana. The coffin remained closed to conceal his severe injuries. An estimated 600 mourners were in attendance, while another 2,400 fans gathered outside of the building during the procession.[98] He is buried at Park Cemetery in Fairmount.[100]
James Dean monument at Cholame, half a mile from the site of the fatal accident

An inquest placed fault for the accident entirely with Dean.[101] There is a James Dean monument, financed by a Japanese businessman, in front of the Cholame post office one half mile from the site of the accident.[102][103][104]
Legacy
Cinema and television

American teenagers of the mid-1950s, when Dean's major films were first released, identified with Dean and the roles he played, especially that of Jim Stark in Rebel Without a Cause. The film depicts the dilemma of a typical teenager of the time, who feels that no one, not even his peers, can understand him. Humphrey Bogart commented after Dean's death about his public image and legacy: "Dean died at just the right time. He left behind a legend. If he had lived, he'd never have been able to live up to his publicity."[105]

Joe Hyams says that Dean was "one of the rare stars, like Rock Hudson and Montgomery Clift, whom both men and women find sexy".[106] According to Marjorie Garber, this quality is "the undefinable extra something that makes a star".[107] Dean's appeal has been attributed to the public's need for someone to stand up for the disenfranchised young of the era,[108] and to the air of androgyny that he projected onscreen.[109]

Dean has been a touchstone of many television shows, films, books and plays. The film September 30, 1955 (1977) depicts the ways various characters in a small Southern town in the US react to Dean's death.[110] The play Come Back to the Five and Dime, Jimmy Dean, Jimmy Dean, written by Ed Graczyk, depicts a reunion of Dean fans on the 20th anniversary of his death. It was staged by the director Robert Altman in 1982, but was poorly received and closed after only 52 performances. While the play was still running on Broadway, Altman shot a film adaptation that was released by Cinecom Pictures in November 1982.[111]

On April 20, 2010, a long "lost" live episode of the General Electric Theater called "The Dark, Dark Hours" featuring Dean in a performance with Ronald Reagan was uncovered by NBC writer Wayne Federman while working on a Ronald Reagan television retrospective.[112] The episode, originally broadcast December 12, 1954,[113] drew international attention and highlights were featured on numerous national media outlets including: CBS Evening News, NBC Nightly News, and Good Morning America. It was later revealed that some footage from the episode was first featured in the 2005 documentary, James Dean: Forever Young.[114]

James Dean's estate still earns about $5,000,000 per year, according to Forbes magazine.[115] On November 6, 2019, it was announced that Dean's likeness would be used, via CGI, for a Vietnam War film called Finding Jack, based on the Gareth Crocker novel. Prior to being shelved,[116] the movie was to have been directed by Anton Ernst and Tati Golykh and another actor would voice Dean's part.[117] Although the directors obtained the rights to use Dean's image from his family, the announcement was met with derision by people in the industry.[117][118]

Martin Sheen has been vocal throughout his career about being influenced by James Dean.[119] Speaking of the impact Dean had on him, Sheen stated, "All of his movies had a profound effect on my life, in my work and all of my generation. He transcended cinema acting. It was no longer acting, it was human behavior."[120] For Terrence Malick's debut film Badlands, Sheen based his characterization of Kit Carruthers, a spree killer loosely inspired by Charles Starkweather, on Dean.[121]

Johnny Depp credited Dean as the catalyst that made him want to become an actor.[122] Nicolas Cage also said he wanted to go into acting because of Dean.[123] "I started acting because I wanted to be James Dean. I saw him in Rebel Without a Cause, East of Eden. Nothing affected me – no rock song, no classical music – the way Dean affected me in Eden. It blew my mind. I was like, 'That's what I want to do'," Cage said.[124] Robert De Niro cited Dean as one of his acting inspirations in an interview.[125] Leonardo DiCaprio also cited Dean as one of his favorite and most influential actors.[126] When asked about which performances stayed with him the most in an interview, DiCaprio responded, "I remember being incredibly moved by Jimmy Dean, in East of Eden. There was something so raw and powerful about that performance. His vulnerability…his confusion about his entire history, his identity, his desperation to be loved. That performance just broke my heart."[127]
Youth culture and music

Numerous commentators have asserted that Dean had a singular influence on the development of rock and roll music. According to David R. Shumway, a researcher in American culture and cultural theory at Carnegie Mellon University, Dean was the first notable figure of youthful rebellion and "a harbinger of youth-identity politics". The persona Dean projected in his movies, especially Rebel Without a Cause, influenced Elvis Presley[128] and many other musicians who followed,[129] including the American rockers Eddie Cochran and Gene Vincent.

In their book, Live Fast, Die Young: The Wild Ride of Making Rebel Without a Cause, Lawrence Frascella and Al Weisel wrote, "Ironically, though Rebel had no rock music on its soundtrack, the film's sensibility—and especially the defiant attitude and effortless cool of James Dean—would have a great impact on rock. The music media would often see Dean and rock as inextricably linked [...] The industry trade magazine Music Connection even went so far as to call Dean 'the first rock star'."[130]

As rock and roll became a revolutionary force that affected the culture of countries around the world,[131] Dean acquired a mythic status that cemented his place as a rock and roll icon.[132] Dean himself listened to music ranging from African tribal music[133] to the modern classical music of Stravinsky[134] and Bartók,[135] as well as to contemporary singers such as Frank Sinatra.[134] While the magnetism and charisma manifested by Dean onscreen appealed to people of all ages and sexuality,[136] his persona of youthful rebellion provided a template for succeeding generations of youth to model themselves on.[137][138]

In his book, The Origins of Cool in Postwar America, Joel Dinerstein describes how Dean and Marlon Brando eroticized the rebel archetype in film,[139] and how Elvis Presley, following their lead, did the same in music. Dinerstein details the dynamics of this eroticization and its effect on teenage girls with few sexual outlets.[140] Presley said in a 1956 interview with Lloyd Shearer for Parade magazine, "I've made a study of Marlon Brando. And I've made a study of poor Jimmy Dean. I've made a study of myself, and I know why girls, at least the young 'uns, go for us. We're sullen, we're broodin', we're something of a menace. I don't understand it exactly, but that's what the girls like in men. I don't know anything about Hollywood, but I know you can't be sexy if you smile. You can't be a rebel if you grin."[141]

Dean and Presley have often been represented in academic literature and in journalism as embodying the frustration felt by young white Americans with the values of their parents,[142][143] and depicted as avatars of the youthful unrest endemic to rock and roll style and attitude. The rock historian Greil Marcus characterized them as symbols of tribal teenage identity which provided an image that young people in the 1950s could relate to and imitate.[144][145] In the book Lonely Places, Dangerous Ground: Nicholas Ray in American Cinema, Paul Anthony Johnson wrote that Dean's acting in Rebel Without a Cause provided a "performance model for Presley, Buddy Holly, and Bob Dylan, all of whom borrowed elements of Dean's performance in their own carefully constructed star personas".[146] Frascella and Weisel wrote, "As rock music became the defining expression of youth in the 1960s, the influence of Rebel was conveyed to a new generation."[130]

Rock musicians as diverse as Buddy Holly,[147] Bob Dylan, and David Bowie regarded Dean as a formative influence.[148] The playwright and actor Sam Shepard interviewed Dylan in 1986 and wrote a play based on their conversation, in which Dylan discusses the early influence of Dean on him personally.[149] A young Bob Dylan, still in his folk music period, consciously evoked Dean visually on the cover of his album, The Freewheelin' Bob Dylan (1963),[150] and later on Highway 61 Revisited (1965),[151] cultivating an image that his biographer Bob Spitz called "James Dean with a guitar".[152] Dean has long been invoked in the lyrics of rock songs, famously in songs such as "A Young Man Is Gone" by the Beach Boys (1963),[153][154] "James Dean" by the Eagles (1974),[155][156] and "James Dean" by the Goo Goo Dolls (1989).[157][158] American musician Taylor Swift referenced him in "Style" (2014).[159] Canadian singer The Weeknd mentioned Dean and his early death in "Ordinary Life" (2016).
Sexuality

Dean is often considered a sexual icon because of his perceived experimental take on life, which included his ambivalent sexuality. The Gay Times Readers' Awards cited him as the greatest male gay icon of all time.[160] When questioned about his sexual orientation, Dean is reported to have said, "No, I am not a homosexual. But I'm also not going to go through life with one hand tied behind my back."[161]

Journalist Joe Hyams suggests that any gay activity Dean might have been involved in appears to have been strictly "for trade", as a means of advancing his career. Some point to Dean's involvement with Rogers Brackett as evidence of this. William Bast referred to Dean as Brackett's "kept boy" and once found a grotesque depiction of a lizard with the head of Brackett in a sketchbook belonging to Dean.[162] Brackett was quoted saying about their relationship, "My primary interest in Jimmy was as an actor—his talent was so obvious. Secondarily, I loved him, and Jimmy loved me. If it was a father-son relationship, it was also somewhat incestuous."[163] James Bellah, the son of American Western author James Warner Bellah, was a friend of Dean's at UCLA, and later stated, "Dean was a user. I don't think he was homosexual. But if he could get something by performing an act....Once...at an agent's office, Dean told me that he had spent the summer as a 'professional house guest' on Fire Island."[164] Mark Rydell also stated, "I don't think he was essentially homosexual. I think that he had very big appetites, and I think he exercised them."[165]

However, the "trade only" notion is contradicted by several Dean biographers.[166] Aside from Bast's account of his own relationship with Dean, Dean's fellow motorcyclist and "Night Watch" member, John Gilmore, claimed that he and Dean "experimented" with gay sex on multiple occasions in New York, describing their sexual encounters as "Bad boys playing bad boys while opening up the bisexual sides of ourselves."[167] Gilmore later stated that he believed Dean was more gay than bisexual.[168]

On the subject of Dean's sexuality, Rebel director Nicholas Ray is on record saying, "James Dean was not straight, he was not gay, he was bisexual. That seems to confuse people, or they just ignore the facts. Some—most—will say he was heterosexual, and there's some proof for that, apart from the usual dating of actresses his age. Others will say no, he was gay, and there's some proof for that too, keeping in mind that it's always tougher to get that kind of proof. But Jimmy himself said more than once that he swung both ways, so why all the mystery or confusion?"[169][170] Martin Landau, a good friend of Dean's whom he met at the Actors Studio, stated, "A lot of people say Jimmy was hell-bent on killing himself. Not true. A lot of gay guys make him out to be gay. Not true. When Jimmy and I were together we'd talk about girls. Actors and girls. We were kids in our early 20s. That was what we aspired to."[171] Elizabeth Taylor, with whom Dean had become friends with while working together on Giant, referred to Dean as gay along with Montgomery Clift and Rock Hudson during a speech at the GLAAD Media Awards in 2000.[172] When questioned about Dean's sexuality by the openly gay journalist Kevin Sessums for POZ magazine, Taylor responded, "He hadn't made up his mind. He was only 24 when he died. But he was certainly fascinated by women. He flirted around. He and I … twinkled."[173]
Stage credits
Broadway

    See the Jaguar (1952)
    The Immoralist (1954) – based on the book by André Gide

Off-Broadway

    The Metamorphosis (1952) – based on the short story by Franz Kafka
    The Scarecrow (1954)
    Women of Trachis (1954) – translation by Ezra Pound

Filmography
	
This section needs additional citations for verification. Please help improve this article by adding citations to reliable sources in this section. Unsourced material may be challenged and removed.
Find sources: "James Dean" – news · newspapers · books · scholar · JSTOR (September 2018) (Learn how and when to remove this template message)
Film
Year 	Title 	Role 	Director 	Notes
1951 	Fixed Bayonets! 	Doggie 	Samuel Fuller 	Uncredited
1952 	Sailor Beware 	Boxing Trainer 	Hal Walker 	Uncredited
1952 	Deadline – U.S.A. 	Copyboy 	Richard Brooks 	Uncredited
1952 	Has Anybody Seen My Gal? 	Youth at Soda Fountain 	Douglas Sirk 	Uncredited
1953 	Trouble Along the Way 	Football Spectator 	Michael Curtiz 	Uncredited
1955 	East of Eden 	Cal Trask 	Elia Kazan 	Golden Globe Special Achievement Award for Best Dramatic Actor
Jussi Award for Best Foreign Actor
Nominated – Academy Award for Best Actor
Nominated – BAFTA Award for Best Foreign Actor
1955 	Rebel Without a Cause 	Jim Stark 	Nicholas Ray 	Nominated – BAFTA Award for Best Foreign Actor
Released posthumously
1956 	Giant 	Jett Rink 	George Stevens 	Nominated – Academy Award for Best Actor
Filmed in 1955; released posthumously. Final role.
2017 	The Disaster Artist 	Jim Stark 	James Franco 	Archive footage, released posthumously, uncredited
Television
Year 	Title 	Role 	Notes
1951 	Family Theater 	John the Apostle 	Episode: "Hill Number One: A Story of Faith and Inspiration"
1951 	The Bigelow Theatre 	Hank 	Episode: "T.K.O."
1951 	The Stu Erwin Show 	Randy 	Episode: "Jackie Knows All"
1952 	CBS Television Workshop 	G.I. 	Episode: "Into the Valley"
1952 	Hallmark Hall of Fame 	Bradford 	Episode: "Forgotten Children"
1952 	The Web 	Himself 	Episode: "Sleeping Dogs"
1952–1953 	Kraft Television Theatre 	Various Characters 	Episodes: "Prologue to Glory", "Keep Our Honor Bright" and "A Long Time Till Dawn"
1952–1955 	Lux Video Theatre 	Various Characters 	Episodes: "The Foggy, Foggy Dew" and "The Life of Emile Zola"
1953 	The Kate Smith Hour 	The Messenger 	Episode: "The Hound of Heaven"
1953 	You Are There 	Robert Ford 	Episode: "The Capture of Jesse James"
1953 	Treasury Men in Action 	Various Characters 	Episodes: "The Case of the Watchful Dog" and "The Case of the Sawed-Off Shotgun"
1953 	Tales of Tomorrow 	Ralph 	Episode: "The Evil Within"
1953 	Westinghouse Studio One 	Various Characters 	Episodes: "Ten Thousand Horses Singing", "Abraham Lincoln" and "Sentence of Death"
1953 	The Big Story 	Rex Newman 	Episode: "Rex Newman, Reporter for the Globe and News"
1953 	Omnibus 	Bronco Evans 	Episode: "Glory in the Flower"
1953 	Campbell Summer Soundstage 	Various Characters 	Episodes: "Something for an Empty Briefcase" and "Life Sentence"
1953 	Armstrong Circle Theatre 	Joey Frasier 	Episode: "The Bells of Cockaigne"
1953 	Robert Montgomery Presents 	Paul Zalinka 	Episode: "Harvest"
1953–1954 	Danger 	Various Characters 	Episodes: "No Room", "Death Is My Neighbor", "The Little Woman" and "Padlocks"
1954 	The Philco Television Playhouse 	Rob 	Episode: "Run Like a Thief"
1954 	General Electric Theater 	Various Characters 	Episodes: "I'm a Fool" and "The Dark, Dark Hours"
1955 	The United States Steel Hour 	Fernand Lagarde 	Episode: "The Thief"
1955 	Schlitz Playhouse 	Jeffrey Latham 	Episode: "The Unlighted Road"
Biographical films

    James Dean also known as James Dean: Portrait of a Friend (1976) with Stephen McHattie as James Dean
    James Dean: The First American Teenager (1976), a television biography that includes interviews with Sal Mineo, Natalie Wood and Nicholas Ray.
    Forever James Dean (1988), Warner Home Video (1995)
    James Dean: The Final Day features interviews with William Bast, Liz Sheridan and Maila Nurmi. Dean's bisexuality is openly discussed. Episode of Naked Hollywood television miniseries produced by The Oxford Film Company in association the BBC, aired in the US on the A&E Network, 1991.
    James Dean: Race with Destiny (1997) directed by Mardi Rustam, starring Casper Van Dien as James Dean.
    James Dean (fictionalized TV biographical film) (2001) with James Franco as James Dean
    James Dean – Outside the Lines (2002), episode of Biography, US television documentary includes interviews with Rod Steiger, William Bast, and Martin Landau (2002).
    Living Famously: James Dean, Australian television biography includes interviews with Martin Landau, Betsy Palmer, William Bast, and Bob Hinkle (2003, 2006).
    James Dean – Kleiner Prinz, Little Bastard aka James Dean – Little Prince, Little Bastard, German television biography, includes interviews with William Bast, Marcus Winslow Jr, Robert Heller (2005)
    Sense Memories (PBS American Masters television biography) (2005)
    James Dean – Mit Vollgas durchs Leben, Austrian television biography includes interviews with Rolf Weutherich and William Bast (2005).
    Two Friendly Ghosts (2012)
    Joshua Tree, 1951: A Portrait of James Dean (2012), with James Preston as James Dean.[174]
    Life (2015). Directed by Anton Corbijn, starring Dane DeHaan as Dean.

See also

    List of oldest and youngest Academy Award winners and nominees – Youngest nominees for Best Actor in a Leading Role
    List of LGBTQ Academy Award winners and nominees — Best Actor in a Leading Role nominees alleged to be LGBTQ
    List of actors with Academy Award nominations
    List of actors with two or more Academy Award nominations in acting categories

References

Goodman, Ezra (September 24, 1956). "Delirium over dead star". Life. Vol. 41, no. 13. pp. 75–88.
David S. Kidder; Noah D. Oppenheim (October 14, 2008). The Intellectual Devotional Modern Culture: Revive Your Mind, Complete Your Education, and Converse Confidently with the Culturati. Rodale. p. 228. ISBN 978-1-60529-793-4. Retrieved July 21, 2013. "Dean was the first to receive a posthumous Academy Award nomination for acting and is the only actor to have received two such posthumous nominations."
"AFI's 100 Years...100 Stars". American Film Institute. Archived from the original on January 13, 2013. Retrieved February 25, 2016.
Chris Epting (June 1, 2009). The Birthplace Book: A Guide to Birth Sites of Famous People, Places, & Things. Stackpole Books. p. 163. ISBN 978-0-8117-4018-0.
David Dalton (2001). James Dean: The Mutant King : a Biography. Chicago Review Press. p. 2. ISBN 978-1-55652-398-4.
George C. Perry (2005). James Dean. DK Publishing, Incorporated. p. 27. ISBN 978-0-7566-0934-4.
Michael DeAngelis (August 15, 2001). Gay Fandom and Crossover Stardom: James Dean, Mel Gibson, and Keanu Reeves. Duke University Press. p. 97. ISBN 0-8223-2738-4.
Val Holley (September 1991). James Dean: Tribute to a Rebel. Publications International. p. 18. ISBN 978-1-56173-148-0.
Robert Tanitch (1997). The Unknown James Dean. Batsford. p. 114. ISBN 978-0-7134-8034-4.
Marie Clayton (January 1, 2004). James Dean: A Life in Pictures. Barnes and Noble Books. ISBN 978-0-7607-5614-0.
Billy J. Harbin; Kim Marra; Robert A. Schanke (2005). The Gay & Lesbian Theatrical Legacy: A Biographical Dictionary of Major Figures in American Stage History in the Pre-Stonewall Era. University of Michigan Press. pp. 133–134. ISBN 0-472-06858-X.
See also Joe and Jay Hyams, James Dean: Little Boy Lost (1992), p. 20, who present an account alleging Dean's molestation as a teenager by his early mentor DeWeerd and describe it as Dean's first homosexual encounter (although DeWeerd himself largely portrayed his relationship with Dean as a completely conventional one).
Paul Alexander, Boulevard of Broken Dreams: The Life, Times, and Legend of James Dean, Viking, 1994, p. 44.
Sessums, Kevin (March 23, 2011). "Elizabeth Taylor Interview About Her AIDS Advocacy, Plus Stars Remember". The Daily Beast. Retrieved March 24, 2011.
Ferguson, Michael S. (2003). Idol Worship: A Shameless Celebration of Male Beauty in the Movies. STARbooks Press. p. 106. ISBN 978-1-891855-48-1.
Roberts, Paul G. (2014). Style Icons Vol 1 Golden Boys. Fashion Industry Broadcast. p. 42. ISBN 978-1-62776-032-4.
Michael Ferguson (2003). Idol Worship: A Shameless Celebration of Male Beauty in the Movies. STARbooks Press. p. 106. ISBN 978-1-891855-48-1.
"Notable Actors | UCLA School of Theater, Film and Television". Tft.ucla.edu. February 11, 2010. Archived from the original on July 13, 2010. Retrieved October 16, 2010.
Karen Clemens Warrick (2006). James Dean: Dream as If You'll Live Forever. Enslow Publishers, Inc. p. 44. ISBN 978-0-7660-2537-0.
Richard Alleman (2005). Hollywood: The Movie Lover's Guide : The Ultimate Insider Tour To Movie Los Angeles. Broadway Books. p. 330. ISBN 978-0-7679-1635-6.
Joyce Chandler (September 27, 2007). James Dean: A Rebel with a Cause: A Fans Tribute. AuthorHouse. p. 73. ISBN 978-1-4670-9575-4.
"The unseen James Dean". The Times. London. March 6, 2005. Retrieved January 6, 2010.
"Notable Alumni Actors". UCLA School of Theater, Film and Television. Archived from the original on October 6, 2014. Retrieved September 29, 2014.
Claudia Springer (March 1, 2007). James Dean Transfigured: The Many Faces of Rebel Iconography. University of Texas Press. p. 14. ISBN 978-0-292-71444-1.
Keith Elliot Greenberg (August 1, 2015). Too Fast to Live, Too Young to Die - James Dean's Final Hours: James Dean's Final Hours. Applause Theatre & Cinema Books. p. 69. ISBN 978-1-4950-5041-1.
LIFE James Dean: A Rebel's Life in Pictures. Time Incorporated Books. October 1, 2016. p. 25. ISBN 978-1-68330-550-7.
Bleiler, David, ed. (2013). TLA Film and Video Guide 2000-2001: The Discerning Film Lover's Guide. St. Martin's Publishing Group. p. 1344. ISBN 978-1-4668-5940-1.
Tony Curtis (October 6, 2009). American Prince: A Memoir. Crown Publishing Group. p. 124. ISBN 978-0-307-40856-3.
R. Barton Palmer (2010). Larger Than Life: Movie Stars of the 1950s. Rutgers University Press. p. 79. ISBN 978-0-8135-4766-4.
David Wallace (April 1, 2003). Hollywoodland. Thorndike Press. p. 105. ISBN 978-0-7862-5203-9.
Bast 2006
On Dean's relationship with Brackett, see also Hyams, James Dean: Little Boy Lost, p. 79.
"What James Dean could teach Matt Damon about keeping your sexuality "one of those mysteries"". salon.com. September 30, 2015.
Warrick, Karen Clemens (2006). James Dean: Dream as If You'll Live Forever. Enslow Publishers, Inc. p. 140. ISBN 978-0-7660-2537-0. Retrieved October 5, 2016.
David Dalton (2001). James Dean: The Mutant King : a Biography. Chicago Review Press. p. 79. ISBN 978-1-55652-398-4.
Claudia Springer (May 17, 2013). James Dean Transfigured: The Many Faces of Rebel Iconography. University of Texas Press. pp. 14–15. ISBN 978-0-292-75288-7.
Lou Lumenick (April 8, 2010). "Revival Circuit: Stopping the presses at Film Forum". New York Post. Archived from the original on August 12, 2020. Retrieved August 12, 2020.
Leonard Maltin (September 29, 2015). Turner Classic Movies Presents Leonard Maltin's Classic Movie Guide: From the Silent Era Through 1965: Third Edition. Penguin Publishing Group. p. 135. ISBN 978-0-698-19729-9.
Reise, R. The Unabridged James Dean, 1991
"The Woman Who Made James Dean a Star". huffpost.com. October 2, 2015.
Ivy Press (2006). Heritage Music and Entertainment Dallas Signature Auction Catalog #634. Heritage Capital Corporation. p. 380. ISBN 978-1-599-67081-2.
Michael J. Meyer; Henry Veggian (2013). East of Eden.: New and Recent Essays. Rodopi. p. 168. ISBN 978-94-012-0968-7.
Holley, pp. x–196.
Perry, pp. 109–226.
Rathgeb, Douglas L. (2004). The Making of Rebel Without a Cause. Jefferson, N.C.: McFarland. p. 20. ISBN 0-7864-1976-8.
Bruce Levene (1994). James Dean in Mendocino: The Filming of East of Eden. Pacific Transcriptions. p. 70. ISBN 978-0-933391-13-0.
Karen Clemens Warrick (2006). James Dean: Dream as If You'll Live Forever. Enslow Publishers, Inc. p. 6. ISBN 978-0-7660-2537-0.
Perry 2005, p. 203
Robert A. Osborne (1979). Academy Awards Oscar Annual. ESE California. p. 60.
Murray Pomerance (2010). "James Stewart and James Dean". In R. Barton Palmer (ed.). Larger Than Life: Movie Stars of the 1950s. Rutgers University Press. p. 80. ISBN 978-0-8135-4766-4.
Films and Filming. Hansom Books. 1986. p. 9.
Claudia Springer (May 17, 2013). James Dean Transfigured: The Many Faces of Rebel Iconography. University of Texas Press. p. 2. ISBN 978-0-292-75288-7.
Kenneth Krauss (May 1, 2014). Male Beauty: Postwar Masculinity in Theater, Film, and Physique Magazines. SUNY Press. p. 171. ISBN 978-1-4384-5001-8.
Davidson Sorkin, Amy (March 24, 2011). "How Elizabeth Taylor and James Dean Grew Old". The New Yorker. Retrieved October 14, 2018.
Ray, Nicholas (February 10, 2016). "James Dean, the Actor as a Young Man: 'Rebel Without a Cause' Director Nicholas Ray Remembers the 'Impossible' Artist". The Daily Beast. Retrieved October 14, 2018.
Perry, George, James Dean, London, New York: DK Publishing, 2005, p. 68 ("Authorized by the James Dean Estate")
Bast 2006, pp. 133, 183–232
Dalton, David. James Dean: The Mutant King: A Biography, Chicago Review Press (1974) p. 151
William Bast, James Dean: a Biography, New York: Ballantine Books, 1956
Riese, Randall, The Unabridged James Dean: His Life from A to Z, Chicago: Contemporary Books, 1991, pp. 41, 238
Alexander, Paul, Boulevard of Broken Dreams: The Life, Times, and Legend of James Dean, New York: Viking, 1994, p. 87
Bast 2006, pp. 133, 150, 183
Liz Sheridan, Dizzy & Jimmy (ReganBooks HarperCollins, 2000), pp. 144–151.
Lipton, Michael A. "An Affair to Remember; Seinfeld's Mom, Liz Sheridan, Calls Her 1952 Romance with James Dean". People. Archived from the original on April 6, 2016. Retrieved December 20, 2014.
David Dalton (2001). James Dean: The Mutant King: A Biography. p. 140. ISBN 9781556523984.
"James Dean – James Dean Letters Sell For $36,000", Contactmusic.com, November 25, 2011
Michael DeAngelis, Gay Fandom and Crossover Stardom: James Dean, Mel Gibson and Keanu Reeves, p. 98.
"AFI Catalog of Feature Films: The Silver Chalice". Afi.com. American Film Institute. 2016. Archived from the original on March 5, 2016. Retrieved June 18, 2016.
In his 1992 biography, James Dean: Little Boy Lost, Hollywood gossip columnist Joe Hyams, who claims to have known Dean personally, devotes an entire chapter to Dean's relationship with Angeli.
Van Holley (1995). James Dean: The Biography. p. 204. ISBN 9780312132491.
Allen, Jane (2002). Pier Angeli: A Fragile Life. Jefferson, NC: McFarland. p. 93. ISBN 978-0-7864-1392-8.
Joe Hyams (1992). James Dean: Little Boy Lost. Warner Books. p. 298. ISBN 978-0712657402.
Alexander, Paul, Boulevard of Broken Dreams: The Life, Times, and Legend of James Dean, New York: Viking, 1994
Bast 2006, p. 197
Jane Allen (September 16, 2015). Pier Angeli: A Fragile Life. McFarland. pp. 88–89. ISBN 978-1-4766-0357-5.
Paul Donnelley (2000). Fade to Black: A Book of Movie Obituaries. Omnibus. p. 24. ISBN 978-0-7119-7984-0.
John Howlett, James Dean: A Biography, Plexus 1997
Bast 2006, p. 196
Greer, Germaine (May 14, 2005). "Mad about the boy". The Guardian. Retrieved December 21, 2014.
"Photo of James Dean and Ursula Andress dining out". Archived from the original on March 2, 2019. Retrieved January 28, 2019.
Porter, Darwin. Brando Unzipped, Blood Moon Productions, Ltd, (2006) p. 484
Wasef and Leno (2007) pp. 13–19.
Perry, p. 151.
Raskin (2005) pp. 47–48; 68–71; 73–74; 78–81; 83–86
Perry (2012) p. 162.
"Racing Record". jamesdean.com. Archived from the original on January 1, 2015. Retrieved December 21, 2014.
Raskin (2005) pp. 101–02.
Raskin (2007) pp. 111–15.
Perry (2012) pp. 11–12.
Thomas Ammann; Stefan Aust (September 21, 2012). Die Porsche-Saga: Geschichte einer PS-Dynastie. Bastei Entertainment. p. 233. ISBN 978-3-8387-1202-4.
Middlecamp, David (September 30, 2005). "Photos From the Vault". SanLuisObispo.com. San Luis Obispo Tribune. Archived from the original on October 6, 2013. Retrieved October 6, 2013.
"James Dean dies in car accident". A&E Television Networks. November 13, 2009.
Keith Elliot Greenberg (August 1, 2015). Too Fast to Live, Too Young to Die: James Dean's Final Hours. Applause Theatre & Cinema Books. p. 144. ISBN 978-1-4950-5041-1.
Moda, Scuderia. "Information about James Dean from historicracing.com". www.historicracing.com.
"Remembering James Dean's death on Highway 46". September 30, 2019.
Perry (2012) pp. 14–15.
Raskin (2005) p. 129.
Perry (2012) pp. 194–95
Obituary Variety, October 5, 1955.
Wilson, Scott. Resting Places: The Burial Sites of More Than 14,000 Famous Persons, 3d ed.: 2 (Kindle Locations 11495-11496). McFarland & Company, Inc., Publishers. Kindle Edition.
Warren N. Beath (December 1, 2007). The Death of James Dean. Grove/Atlantic, Incorporated. p. 60. ISBN 978-0-8021-9611-8.
Springer, Claudia (2013). James Dean Transfigured: The Many Faces of Rebel Iconography. University of Texas Press. p. 16. ISBN 978-0-292-75288-7.
Beath, Warren Newton (2007). The Death of James Dean. Grove/Atlantic, Inc. pp. 6–7. ISBN 978-0-8021-9611-8.
Riese, Randall (1991). The Unabridged James Dean: His Life and Legacy from A to Z. Contemporary Books. p. 268. ISBN 978-0-8092-4061-6.
"The James Dean Story: Introduction". www.americanlegends.com.
Joe Hyams (January 1, 1994). James Dean: Little Boy Lost. Grand Central Pub. p. 209. ISBN 978-0-446-36529-1.
Marjorie B. Garber, Bisexuality and the Eroticism of Everyday Life (2000), p. 140. See also "Bisexuality and Celebrity." In Rhiel and Suchoff, The Seductions of Biography, p. 18.
Perry, G., James Dean, p. 204, New York, DK Publishing, Inc., 2005
David Burner (January 11, 1998). Making Peace with the 60s. Princeton University Press. p. 244. ISBN 0-691-05953-5.
James Monaco (1981). How to Read a Film: The Art, Technology, Language, History, and Theory of Film and Media. Oxford University Press. p. 223. ISBN 978-0-19-502802-7.
Robert Niemi (February 2, 2016). The Cinema of Robert Altman: Hollywood Maverick. Columbia University Press. p. 197. ISBN 978-0-231-85086-5.
"Rare Film of Ronald Reagan, James Dean Unearthed (April 21, 2010)". CBS News. April 21, 2010. Retrieved October 16, 2010.
Robert Paul Metzger (January 1, 1989). Reagan: American Icon. University of Pennsylvania Press. p. 106. ISBN 0-916279-05-7.
Brian Williams (April 22, 2010). "A confession and a plea". Dailynightly.msnbc.msn.com. Archived from the original on November 20, 2011. Retrieved October 16, 2010.
Lisa DiCarlo (October 25, 2004). "The Top Earners For 2004". Forbes. Retrieved February 24, 2006.
Garza, Joe (September 1, 2022). "How Hollywood Really Felt About James Dean's CGI Resurrection In Finding Jack". Grunge. Archived from the original on September 1, 2022.
Ritman, Alex (November 6, 2019). "James Dean Reborn in CGI for Vietnam War Action-Drama (Exclusive)". The Hollywood Reporter.
Sharareh Drury (November 6, 2019). "Chris Evans, Elijah Wood and More Criticize James Dean CGI Casting: "This Shouldn't Be a Thing"". The Hollywood Reporter. Archived from the original on November 7, 2019. Retrieved November 7, 2019.
"Throwback: Martin Sheen remembers James Dean and his 'overwhelming impact'". wrtv.com. September 26, 2019.
"Friends of James Dean remember iconic star". today.com. February 9, 2005.
"Badlands: An Oral History". gq.com. May 26, 2011.
"Hooked on Dean, says Johnny Depp". BBC.
"Nicolas Cage on the rise of the 'celebutard': 'It sucks to be famous right now'". The Independent. March 11, 2014.
"Nicolas Cage on the rise of the 'celebutard': 'It sucks to be famous right now'". The Independent. March 11, 2014.
"Robert De Niro reveals all about his rivalry with Al Pacino". mirror.co.uk. November 11, 2018.
"Leonardo DiCaprio On 'Once Upon A Time In Hollywood' And Looking For Positives In Disruption That Has Turned The Movie Business On Its Ear – The Deadline Q&A". Deadline. December 19, 2019.
"Leonardo DiCaprio On The Hard-Knock Film Education That Led To 'The Revenant': Q&A". February 10, 2016.
Burton W. Peretti (February 1, 1998). Jazz in American Culture. Ivan R. Dee. p. 128. ISBN 978-1-4617-1304-3. "One of them, Elvis Presley, brilliantly blended black blues and gospel with the white actor James Dean's movie persona."
David R. Shumway (January 19, 2015). "Rock Stars as Icons". In Andy Bennett; Steve Waksman (eds.). The SAGE Handbook of Popular Music. SAGE Publications. p. 304. ISBN 978-1-4739-1099-7.
Lawrence Frascella; Al Weisel (October 4, 2005). Live Fast, Die Young: The Wild Ride of Making Rebel Without a Cause. Simon and Schuster. p. 291. ISBN 978-0-7432-9118-7.
Ralph Brauer (1989). "Iconic Modes: The Beatles". In Timothy E. Scheurer (ed.). American Popular Music: The age of rock. Popular Press. p. 155. ISBN 978-0-87972-468-9.
Yuwu Song (March 26, 2015). "James Dean (1931–1955)". In Gina Misiroglu (ed.). American Countercultures: An Encyclopedia of Nonconformists, Alternative Lifestyles, and Radical Ideas in U.S. History. Routledge. p. 200. ISBN 978-1-317-47729-7.
Nicholas Ray (September 10, 1993). I Was Interrupted: Nicholas Ray on Making Movies. University of California Press. p. 111. ISBN 978-0-520-91667-8.
Peter Winkler; George Stevens (August 1, 2016). Real James Dean: Intimate Memories from Those Who Knew Him Best. Chicago Review Press. p. 365. ISBN 978-1-61373-474-2.
Beath (2005) p. 21
Robert Tanitch (October 30, 2014). The Unknown James Dean. Pavilion Books. p. 21. ISBN 978-1-84994-249-2.
Claudia Springer (May 17, 2013). James Dean Transfigured: The Many Faces of Rebel Iconography. University of Texas Press. p. 17. ISBN 978-0-292-75288-7.
Wayne Robins (March 31, 2016). A Brief History of Rock, Off the Record. Routledge. p. 40. ISBN 978-1-135-92345-7.
Michael D. Dwyer (June 10, 2015). Back to the Fifties: Nostalgia, Hollywood Film, and Popular Music of the Seventies and Eighties. Oxford University Press. p. 160. ISBN 978-0-19-935685-0.
Joel Dinerstein (May 17, 2017). The Origins of Cool in Postwar America. University of Chicago Press. pp. 341–342. ISBN 978-0-226-15265-3.
Peter Guralnick (December 20, 2012). Last Train to Memphis: The Rise of Elvis Presley. Little, Brown. p. 338. ISBN 978-0-316-20677-8.
Doug Owram (June 1997). Born at the Right Time: A History of the Baby-boom Generation. University of Toronto Press. p. 196. ISBN 978-0-8020-8086-8. "The sense of alienation from society and distrust of authority that was inherent in the leather jacket of James Dean or the blue jeans of Elvis Presley was incorporated into the modern sensibility of youth"
Stephen Glynn (May 7, 2013). "The Primitive Pop Music Film: Coffee Bars, Cosh Boys and Cliff". The British Pop Music Film: The Beatles and Beyond. Palgrave Macmillan UK. p. 10. ISBN 978-0-230-39223-6.
Wayne Robins (March 31, 2016). A Brief History of Rock, Off the Record. Routledge. pp. 31–32. ISBN 978-1-135-92346-4.
Jason Gross (October 1, 2012). "1997". In Joe Bonomo (ed.). Conversations with Greil Marcus. Univ. Press of Mississippi. p. 107. ISBN 978-1-61703-623-1.
Paul Anthony Johnson; Will Scheibel (February 1, 2014). ""You Can't Be a Rebel If You Grin": Masculinity, Performance, and Anxiety in 1950s Rock-and-Roll and the Films of Nicholas Ray". In Steven Rybin, Will Scheibel (ed.). Lonely Places, Dangerous Ground: Nicholas Ray in American Cinema. SUNY Press. p. 140. ISBN 978-1-4384-4981-4.
John Howlett (November 1, 2016). James Dean: Rebel Life. Plexus Publishing. p. 5. ISBN 978-0-85965-867-6.
Marc Spitz (October 2010). Bowie: A Biography. Crown/Archetype. pp. 25–26. ISBN 978-0-307-71699-6.
Lee Marshall (April 24, 2013). Bob Dylan: The Never Ending Star. John Wiley & Sons. pp. 17–18. ISBN 978-0-7456-3974-1.
David Dalton (2001). James Dean: The Mutant King, a Biography. Chicago Review Press. p. 333. ISBN 978-1-55652-398-4.
David Dalton (June 1, 2012). Who Is That Man? In Search of the Real Bob Dylan. Omnibus Press. p. 183. ISBN 978-0-85712-779-2.
Bob Spitz (1991). Dylan: A Biography. Norton. p. 270. ISBN 978-0-393-30769-6.
"The Beach Boys – A Young Man Is Gone". genius.com. Genius Media Group. Archived from the original on July 13, 2016. Retrieved November 10, 2017.
Larry Birnbaum (December 14, 2012). Before Elvis: The Prehistory of Rock 'n' Roll. Scarecrow Press. p. 367. ISBN 978-0-8108-8629-2.
"Eagles – James Dean". genius.com. Genius Media Group. Archived from the original on February 18, 2017. Retrieved November 10, 2017.
Sam Riley (2010). Star Struck: An Encyclopedia of Celebrity Culture. ABC-CLIO. p. 186. ISBN 978-0-313-35813-5.
"Goo Goo Dolls – James Dean". genius.com. Genius Media Group. Archived from the original on December 4, 2015. Retrieved November 10, 2017.
Keith Elliot Greenberg (August 1, 2015). Too Fast to Live, Too Young to Die: James Dean's Final Hours. Applause Theatre & Cinema Books. p. 29. ISBN 978-1-4950-5041-1.
Kornhaber, Spencer (February 13, 2015). "Reading Taylor Swift's Lips". The Atlantic. Archived from the original on April 15, 2019. Retrieved April 15, 2019.
Howes, Keith (2005). "James Dean". In Aldrich, Robert; Wotherspoon, Garry (eds.). Who's Who in Contemporary Gay and Lesbian History Vol.2: From World War II to the Present Day. Routledge. p. 268. ISBN 978-1-134-58313-3.
Randall Riese (1991). The Unabridged James Dean: His Life and Legacy from A to Z. McGraw-Hill/Contemporary. p. 239. ISBN 978-0-8092-4061-6.
William Blast (2006). Surviving James Dean. p. 160. ISBN 9781569802984.
Ronald Martinetti (1996). The James Dean Story: A Myth-Shattering Biography of a Hollywood Legend. p. 37. ISBN 0806580046.
"American Legends Interviews..... James Dean at UCLA". Archived from the original on December 25, 2018. Retrieved June 7, 2015.
"CNN.com - Transcripts". transcripts.cnn.com.
Donald Spoto, Rebel: The Life and Legend of James Dean (HarperCollins, 1996), pp. 150–151. See also Val Holley, James Dean: The Biography, pp. 6, 7, 8, 78, 80, 85, 94, 153.
John Gilmore (1997). Live Fast, Die Young: Remembering the Short Life of James Dean. Thunder's Mouth Press. pp. 119–120. ISBN 978-1-56025-146-0.
"Hard-Boiled Hollywood". vice.com. September 9, 2011.
Lawrence Frascella and Al Weisel (2005). Live Fast, Die Young: The Wild Ride of Making Rebel Without a Cause. p. 181. ISBN 9780743260824.
H. Paul Jeffers (2000). Sal Mineo: His Life, Murder, and Mystery. p. 28. ISBN 9780786707775.
"Martin Landau: From North by Northwest to Frankenweenie". The Guardian. October 18, 2012.
Elizabeth Taylor at the GLAAD Media Awards, March 25, 2011, archived from the original on December 21, 2021, retrieved December 17, 2017
"AIDS: Elizabeth Taylor tells the truth". windycitytimes.com. April 27, 2011.

    "A Portrait Of James Dean; Written and Directed by Matthew Mishory". Joshua Tree, 1951. Retrieved August 23, 2012.

Further reading

    Alexander, Paul: Boulevard of Broken Dreams: The Life, Times, and Legend of James Dean . Viking, 1994. ISBN 0-670-84951-0
    Bast, William: James Dean: A Biography. Ballantine Books, 1956.
    Bast, William (2006). Surviving James Dean. New Jersey: Barricade Books. ISBN 1-56980-298-X.
    Beath, Warren: Death of James Dean. Grove Press, 1986. ISBN 0-394-55758-1
    Beath, Warren, with Wheeldon, Paula; James Dean in Death: A Popular Encyclopedia of a Celebrity Phenomenon, McFarland & Co., Inc., 2005. ISBN 0-7864-2000-6
    Dalton, David: James Dean-The Mutant King: A Biography. Chicago Review Press, 2001. ISBN 1-55652-398-X
    DeAngelis, Michael (2001). Gay Fandom and Crossover Stardom: James Dean, Mel Gibson, and Keanu Reeves. Durham: Duke University Press. ISBN 0-8223-2728-7.
    Frascella, Lawrence and Weisel, Al: Live Fast, Die Young: The Wild Ride of Making Rebel Without a Cause. Touchstone, 2005. ISBN 0-7432-6082-1
    Gilmore, John : Live Fast-Die Young: Remembering the Short Life of James Dean. Thunder's Mouth Press, 1998. ISBN 1-56025-169-7
    Gilmore, John: The Real James Dean. Pyramid Books, 1975. ISBN 0-515-03814-8
    Heinrichs, Steve; Marinello, Marco; Perrin, Jim; Raskin, Lee; Stoddard, Charles A; Zigg, Donald; Porsche Speedster TYP540: Quintessential Sports Car, 2004, Big Lake Media, Inc. ISBN 0-9746468-0-6
    Turiello, James : 'James Dean The Quest for an Oscar' 360 pages Publisher: Sandy Beach, A (February 26, 2018) ISBN 0-692-08182-8 ISBN 978-0-692-08182-2

	

    Holley, Val: James Dean: The Biography. St. Martin's Griffin, 1996. ISBN 0-312-15156-X
    Turiello, James: The James Dean Collection, 1993. Biography on fifty trading cards with photographs from The James Dean Gallery in Fairmount Indiana.
    Hopper, Hedda and Brough, James: "James Dean Was a Rebel With a Cause" in The Whole Truth and Nothing But. Doubleday. 1963.
    Howell, John: James Dean: A Biography. Plexus Publishing, 1997. Second Revised Edition. ISBN 0-85965-243-2
    Hyams, Joe; Hyams, Jay: James Dean: Little Boy Lost. Time Warner Publishing, 1992. ISBN 0-446-51643-0
    Martinetti, Ronald: The James Dean Story, Pinnacle Books, 1975. ISBN 0-523-00633-0
    Morrissey (1983). James Dean is Not Dead. Manchester: Babylon Books. ISBN 0-907188-06-0.
    Perry, George: James Dean. DK Publishing, 2005. ISBN 1-4053-0525-8
    Riese, Randall: The Unabridged James Dean: His Life from A to Z. Contemporary Books, 1991. ISBN 0-8092-4061-0
    Raskin, Lee: James Dean: At Speed. David Bull Publishing, 2005. ISBN 1-893618-49-8
    Sheridan, Liz: Dizzy & Jimmy: My Life With James Dean : A Love Story. HarperCollins Canada / Harper Trade, 2000. ISBN 0-06-039383-1
    Spoto, Donald: Rebel: The Life and Legend of James Dean. HarperCollins, 1996. ISBN 0-06-017656-3

External links
James Dean
at Wikipedia's sister projects

    Media from Commons
    Data from Wikidata

    James Dean at Curlie
    James Dean at IMDb
    James Dean at the TCM Movie Database
    James Dean at the Internet Broadway Database Edit this at Wikidata
    James Dean at the Internet Off-Broadway Database
    JamesDean.com
    James Dean Archives Seita Ohnishi Collection,Kobe Japan

Authority control Edit this at Wikidata
International	

    FAST
        2 ISNI VIAF

National	

    Norway Spain France BnF data Catalonia Germany Israel United States Sweden Japan Czech Republic Australia Greece Korea Netherlands Poland Portugal

Artists	

    MusicBrainz

People	

    Deutsche Biographie Trove

Other	

    SNAC IdRef

Portals:

     Biography
    flag United States
    flag Indiana
     Film

Categories:

    James Dean1931 births1955 deaths20th-century American male actors20th-century QuakersAmerican male film actorsAmerican male stage actorsAmerican male television actorsAmerican QuakersBest Drama Actor Golden Globe (film) winnersBurials in IndianaLGBT male actorsLGBT people from IndianaMale actors from IndianaMethod actorsPeople from Marion, IndianaRoad incident deaths in CaliforniaSanta Monica College alumniUCLA Film School alumniWarner Bros. contract players

    This page was last edited on 15 August 2023, at 16:44 (UTC).
    Text is available under the Creative Commons Attribution-ShareAlike License 4.0; additional terms may apply. By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization.

    Privacy policy
    About Wikipedia
    Disclaimers
    Contact Wikipedia
    Code of Conduct
    Mobile view
    Developers
    Statistics
    Cookie statement

    Wikimedia Foundation
    Powered by MediaWiki

`;
const LINE_BREAKS = /[\r\n]+/g;
{
  const { recognize } = ISO8601;
  function analyze_text(text, lang) {
    const result = recognize(text, lang);
    return result;
  }
  function exploration_analyze_text(options) {
    const text = options?.text ?? JAMES_DEAN_TEXT;
    const lang = options?.lang ?? "en";
    const lines = text.split(LINE_BREAKS);
    const analyzed = Array.from(lines, line => analyze_text(line, lang));
    const nonempty = analyzed.filter(x => !!x);
    for (const { input, value, match } of nonempty) {
      // console.debug(value, match[0], input.slice(0, 20));
    }
  }
}

function global_sync_ensure_timeframe_on(element) {
  assert(element);
  if (TIMEFRAMES_BY_ELEMENT.has(element))
    return TIMEFRAMES_BY_ELEMENT.get(element);
  return make_timeframe({ element });
}

// I don't know how this is going to be done but it's got to be done
const TIMEFRAMES_BY_ELEMENT = new Map();
function get_closest_timeframe_from(element) {
  const tf = element?.closest(`[data-timeframe]`);
  if (tf) return global_sync_ensure_timeframe_on(tf);
}

function maybe_interpret_element_as_timeframe(element) {
  const timeframe = global_sync_ensure_timeframe_on(element);
  timeframe.start();
}

function maybe_interpret_element_as_iconic_search(element) {
  // but it could be either one!
  const SOURCE = { db: "cronwall", collection: "en_wiki_events" };
  const terms = ICONS_WITH_TERMS.map(([, term]) => term);
  const icon_for = s => ICONS_WITH_TERMS.find(([, t]) => t === s)?.[0];
  const spec = { ...SOURCE, terms, icon_for };
  const timeframe = get_closest_timeframe_from(element);
  const it = make_multi_term_search(spec);
  it.start();
  element.append(it.element);
}

function maybe_interpret_element_as_people_lifetimes(element) {
  const tf = element.closest(`[data-timeframe]`);
  const timeframe = {
    watch(cb) {
      attribute_effects({ "data-timeframe": cb });
    },
  };

  const it = make_person_lifespan_nominator({ timeframe });
  it.start();
  element.append(it.element);
}

function make_term_search(spec) {
  const { db, collection, term } = spec;
  const element = document.createElement("div");
  element.classList.add("term-search");
  // where?
  // what database?
  // what collection?
  // it's temping to just “search everywhere by default”
  // but that is not monotonic
  function start() {}
  function stop() {}
  return { start, stop, element };
}

function maybe_interpret_element_as_term_search(element) {
  const get_a = make_get_a(element);
  const term = get_a("term");
  const database = get_a("database");
  const collection = get_a("collection");

  if (!term) return element.setAttribute("data-warn", `which term?`);
  if (!database) return element.setAttribute("data-warn", `which database?`);
  if (!collection) return element.setAttribute("data-warn", `what collection?`);
  const it = make_term_search({ term });
  if (!it) {
    element.setAttribute("data-warn", `no process to search for ‘${term}’ !`);
    return;
  }
  it.start();
  if (!it.element) {
    element.setAttribute(
      "data-warn",
      `no representation of search for ‘${term}’ !`
    );
    return;
  }
  element.append(it.element);
}

async function async_first(gen) {
  for await (const item of gen) return item;
}
async function* async_concat(...gens) {
  for (const gen of gens) for await (const x of gen) yield x;
}

async function* make_tree_map(spec) {
  const { db, collection, index, unit, element } = spec;
  const lang = "en";
  const { coerce, compare, next } = ISO8601;
  const source = { db, store: collection, index };
  const container = document.createElement("div");
  container.setAttribute("data-part", "items");
  element.append(container);
  const read = partial => IDB.read({ ...partial, ...source });
  let from, to;
  {
    const pre = Array.from(date_scan_segments(null, false), read);
    const earliest = await async_first(async_concat(...pre));
    // should be, get the index field
    from = get_date(earliest);
  }
  {
    const post = Array.from(date_scan_segments(null, true), read);
    const latest = await async_first(async_concat(...post));
    to = get_date(latest);
  }
  const first = coerce(from, unit);
  const last = coerce(to, unit);
  let period = first;
  let total = 0;
  while (compare(period, last) <= 0) {
    const following = next(period);
    // needs reverse for bc...
    const frame = [period, following];
    const open = [false, true];
    if (indexedDB.cmp(...frame) > 0) {
      frame.reverse();
      open.reverse();
    }
    const query = IDBKeyRange.bound(...frame, ...open);
    const spec = { db, store: collection, index, query };
    const count = await IDB.count(spec);
    total += count;
    console.debug({ from: frame[0], to: frame[0], count });
    const item = document.createElement("div");
    // item.setAttribute("typeof", "tree-map");
    item.textContent = ISO8601.label(period, lang);
    item.setAttribute("data-count", count);
    item.setAttribute("data-count-for", period);
    item.style.setProperty("--count", count);
    container.append(item);
    period = following;
  }
  container.style.setProperty("--total", total);
}

function make_get_a(element) {
  return function get_a(name) {
    const attribute = `data-${name}`;
    const value = element.getAttribute(attribute);
    if (value) return value;
    element.append(`I will have to get a ${name}. `);
  };
}

function make_player_from_async_generator(thunk) {
  const element = document.createElement("form");
  const step_button = document.createElement("button");
  const gen = thunk();
  // const gen = it[Symbol.iterator]();
  step_button.textContent = "step";
  step_button.setAttribute("data-command", "step");
  element.append(step_button);

  async function step() {
    const { done } = await gen.next();
    if (done) return;
    //
  }

  function handler(event) {
    if (event.target instanceof HTMLButtonElement) {
      const command = event.target.getAttribute("data-command");
      if (command === "step") {
        event.preventDefault();
        step();
      }
    }
  }
  function start() {
    element.addEventListener("click", handler);
  }
  function stop() {
    element.removeEventListener("click", handler);
  }
  return { start, stop, element };
}

function maybe_interpret_element_as_tree_map(element) {
  const get_a = make_get_a(element);
  const db = get_a("database");
  const collection = get_a("collection");
  const index = get_a("index");
  const unit = get_a("unit");

  if (!db) return element.setAttribute("data-warn", `which database?`);
  if (!collection) return element.setAttribute("data-warn", `what collection?`);
  if (!index) return element.setAttribute("data-warn", `which index?`);
  if (!unit) return element.setAttribute("data-warn", `which unit?`);
  const thunk = () => make_tree_map({ db, collection, index, unit, element });
  const it = make_player_from_async_generator(thunk);
  if (!it) {
    element.setAttribute("data-warn", `no process to make ‘${unit}’ tree map!`);
    return;
  }
  if (!it.element) {
    element.setAttribute(
      "data-warn",
      `no representation of tree map for ‘${unit}’ !`
    );
    return;
  }
  element.append(it.element);
  it.start();
}

function as_player(/** @type Element */ element) {
  return make_player({ element });
}

// will be a generic using default kb
function interpreter_of(type) {
  if (type === "timeframe") return maybe_interpret_element_as_timeframe;
  if (type === "iconic-search") return maybe_interpret_element_as_iconic_search;
  if (type === "people-lifetimes")
    return maybe_interpret_element_as_people_lifetimes;
  if (type === "term-search") return maybe_interpret_element_as_term_search;
  if (type === "tree-map") return maybe_interpret_element_as_tree_map;
  if (type === "catalog") return maybe_interpret_element_as_catalog;
  if (type === "database") return maybe_interpret_element_as_database;
  if (type === "store") return maybe_interpret_element_as_store;
  if (type === "collection") return maybe_interpret_element_as_collection;
}

async function maybe_interpret_element_as_catalog(element) {
  const say = element.append.bind(element);
  if (!indexedDB) return say(`No indexedDB here, what a shame.  `);
  say(`You do have indexedDB here.  `);
  if (typeof indexedDB.databases !== "function")
    return say(`Sorry, mate, we can't enumerate the databases. `);
  say(`Lovely, chap, we can enumerate the databases. `);
  const databases = await this.async(indexedDB.databases());
  say(`I got a word of ${databases.length} databases, as follows:`);
  const list = document.createElement("ul");
  for (const { name } of databases) {
    // notate link
    const item = document.createElement("li");
    item.setAttribute("typeof", "database"); // IDBDatabase
    item.setAttribute("data-database", name);
    item.textContent = `Database: ${name}`;
    list.append(item);
  }
  say(list);
}

// this is just nav... what does this link to
async function maybe_interpret_element_as_database(element) {
  const get_a = make_get_a(element);
  const db_name = get_a("database");
  if (!db_name) return element.setAttribute("data-warn", `which database?`);
  const say = element.append.bind(element);

  say?.(`So, you want to know about ${db_name}, eh?`);
  try {
    // system should manage this resource so don't worry
    const db = await this.async(IDB.connect(db_name));
    // process could be stopped now
    // also connection could die in between
    say?.(`I see here a database ${db.name} with version ${db.version}.`);

    const store_names = db.objectStoreNames;
    const count = store_names.length;
    const things = `store${count === 1 ? "" : "s"}`;
    say?.(`I claim that ${db_name} has ${count} ${things}. `);
    for (const store_name of store_names) {
      say?.(`I'll tell you of a store called “${store_name}”. `);
    }
  } catch (error) {
    // host's job too?
    say?.(`I had a problem connecting to ${db_name}: ${error}. `);
  } finally {
    // this seems not your job now...
    // _db.close();
  }
}

// suppose this to be a generic, outputting to html
function notate(thing) {
  const element = document.createElement("span");
  const typeof_ = typeof thing;
  element.textContent = `a ${typeof_} ${JSON.stringify(thing)}`;
  return element;
}

// generics
function iri_of(thing) {}
function describe(thing) {
  if (thing === undefined) return `⊥`;
  if (thing === null) return `∅`;
  if (thing[TYPE]) return `a ${thing[TYPE].toString()} record`;
  return `a ${typeof thing}. `;
}

// generic, out to HTML but not necessarily an anchor
function link_to(thing) {
  const element = document.createElement("span");
  const iri = iri_of(thing);
  if (iri) element.setAttribute("resource", iri);
  const description = describe(thing);
  element.textContent = description ?? `a virtual link to something. `;
  return element;
}

async function maybe_interpret_element_as_store(element) {
  const get_a = make_get_a(element);
  const db = get_a("database");
  const store = get_a("store");
  if (!db) return element.setAttribute("data-warn", `which database?`);
  if (!store) return element.setAttribute("data-warn", `which store?`);
  // get the count and if it's small, then just get them alleged
  const say = element.append.bind(element);
  say(
    `In a database called “${db}”, I'm looking for a store called “${store}”. `
  );
  const source = { db, store };
  let count = NaN;
  try {
    count = await this.async(IDB.count(source)); // POWER
  } catch (error) {
    say(`Sorry, there was a problem counting: ${error}.`);
  }
  if (typeof count !== "number" || isNaN(count))
    return say(
      `Well I can't even tell how big this store is, so I think we're done here. `
    );
  say(`I say that there are ${count} items in ${store}. `);
  // just emit the things in an event, as a collection
  // also, if we're willing to get the first 50, we could do that regardless of size
  // and also emit a continuation
  if (count < 50) {
    say(`I mean, ${count} is not too many to list, right?. `);
    const items = await this.async(IDB.get_all(source)); // POWER x50
    items.forEach(with_type(store));
    say(`All right, I got ${items.length} items. `);
    // okay not that... I mean we want the process on this element
    // this.emit(items, "found");
    const event = new CustomEvent("found", { detail: items, bubbles: true });
    element.dispatchEvent(event);
  }
}
function maybe_interpret_element_as_collection() {}

const SPACE = /\s+/g;
const dom_string_list = s => s.split(SPACE);

// maybe do nothing yet, maybe waiting until you come into view
function maybe_interpret_element_as_player_sync(element) {
  const { say } = this;
  if (element.childNodes.length === 0) {
    element.setAttribute("data-warn", `empty player!`);
    return; // here async would listen for changes
  }
  // if (Math.random() < 0.25) throw new Error(`chaos`);
  for (const child of element.children) {
    const typeof_ = child.getAttribute("typeof");
    if (typeof_) {
      const types = dom_string_list(typeof_);
      for (const type of types) {
        const interpret = interpreter_of(type); // a generic
        if (!interpret) {
          const warning = `not implemented: ${type}`;
          child.setAttribute("data-warn", warning);
          return; // here async would await an interpreter
        }
        assert?.(typeof interpret === "function");
        this.spawn(interpret, [child]);
      }
    }
  }
}

function play_means_work() {
  const element = document.documentElement;
  function handle(event) {
    const { target } = event;
    if (!(target instanceof HTMLButtonElement)) return;
    if (target.textContent === "play") {
      // find the closest process
    }
  }
  function start() {
    element.addEventListener("click", handle);
  }
  function stop() {
    element.removeEventListener("click", handle);
  }
  return { start, stop };
}

// inert.  doesn't speak unless spoken to
// where everything has RDF I/O
function make_knowledge_base() {
  function tell() {}
  function ask() {}
  return { tell, ask };
}

{
  const { defineProperty } = Object;
  const now = () => performance.now();
  function make_dom_system(context) {
    const { kb } = context;
    if (!kb) throw new Error(`need knowledge base`);
    const doc = context.document ?? globalThis.document;
    const element = context.element ?? doc.createElement("div");
    const processes = make_dynamic_set(); // might not be needed
    const promises = make_dynamic_map();
    let _next_pid = 0;

    function make_host(pid, element) {
      return {
        get pid() {
          return pid;
        },
        spawn: (...args) => _beget(pid, ...args),
        // provisional: I don't want spawn to create a promise by default
        async(arg) {
          return arg;
        },
        emit: (detail, type = "default") => {
          const event = new CustomEvent(type, { detail, bubbles: true });
          element.dispatchEvent(event);
        },
        say: thing => {
          const saying = notate(thing);
          if (saying) {
            if (saying instanceof Element)
              saying.setAttribute("data-time", now());
            // this.element.append(saying);
            element.append(saying);
          }
        },
        kill() {
          throw new Error(`kill is not implemented!`);
        },
      };
    }

    // what I really mean here though is "ensure"
    // i.e. if I had something equivalent already, then it would refer to that
    function _beget(parent, description, args, name) {
      const pid = _next_pid;
      processes.add(pid);
      _next_pid++;

      // ∀x∈Process ∃y∈Element: notates(y, x) ∧ visible(y)
      const room = document.createElement("div");
      room.setAttribute("data-notation", "process");
      room.setAttribute("data-process-born", now());
      room.textContent = `process ${pid}. `;
      if (name) room.name = name;
      element.append(room);
      // annotate(room)

      const host = make_host(pid, room);
      if (typeof description === "function") {
        try {
          const result = description.apply(host, args);
          room.setAttribute("data-process-accepted", "");
          room.setAttribute("data-process-died", now());
          // console.info(description, `accepted with`, result);
          if (result instanceof Promise) {
            const existing = promises.get(pid);
            if (existing) {
              console.warn(`already pending for ${pid}??`);
              return;
            }
            console.info(`unboxing promise...`);
            promises.set(pid, existing);
            Promise.resolve(result).then(
              value => {
                // console.info(description, `async accepted with `, value);
                room.setAttribute("data-process-died", now());
                room.setAttribute("data-process-accepted", "");
                promises.remove(pid);
              },
              error => {
                console.error(description, `async rejected with`, error);
                room.setAttribute("data-process-died", now());
                room.setAttribute("data-process-rejected", "");
                promises.remove(pid);
              }
            );
          }
        } catch (error) {
          console.error(description, `rejected with`, error);
          room.setAttribute("data-process-died", now());
          room.setAttribute("data-process-rejected", "");
        }
      } else if (typeof description === "object") {
        // distribute over “plain” object...
        return map_object(description, (v, k) =>
          spawn(v, args, `${name ?? ""}${k}`)
        );
      } else {
        throw new Error(`No method for coercing to process`, description);
      }
      // we also want a promise representing the completion of the process
      return pid;
    }

    function spawn(description, args, name) {
      return _beget(null, description, args, name);
    }
    function kill() {
      for (const process of processes.values()) process.kill();
    }
    return { kill, spawn, element, processes };
  }
}

function as_line(thing) {
  const type = thing?.[TYPE];
  if (!type) throw new Error(`I can't display this thing as a line`);
  const element = document.createElement("div");
  element.setAttribute("typeof", type); // but this is collection...
  const notator = METHODS.render_into[type];
  if (typeof notator !== "function") throw new Error(`no notator for ${type}`);
  notator(thing, element);
  return element;
}

// ============================================== FACTS
// multi-term-search(E, T) → ∀ t∈T ∃ y: has(E, x) ∧ shunt-findings(T, y)

const kb = make_knowledge_base();
const home = document.querySelector(`#system`);
const system = make_dom_system({ kb, element: home });

function main() {
  function init() {
    this.spawn({
      help() {
        this.say(`What's this all about?`);
        this.say(`Where do things go?`);
        this.say(`Why write javascript?`);
      },
      // ∀x∈Collection ∃y∈Store: consistent-with(y, x)
      reify_persistence() {
        const { say } = this;
        const database_name = "cronwall";
        const expectations = KNOWN_CATALOG;

        say(
          `I will try to bring the database ${database_name} in line with the given catalog description.  The catalog description is a dictionary of table descriptions which can be processed completely independently.`
        );
        for (const [store, description] of Object.entries(expectations)) {
          say?.(`So there's a thing called ${store}`);
          this.spawn(
            reify_collection,
            [database_name, store, description],
            store
          );
        }
      },
      // ∀x∈Element ∃y∈Interpreter: interprets(y, x)
      one_time_create_players() {
        const { say, spawn } = this;
        say(`I look through the document for players to activate.`);
        const all_players = document.querySelectorAll("[data-player]");
        for (const element of all_players) {
          spawn(maybe_interpret_element_as_player_sync, [element]);
        }
      },
    });
  }

  // I just want to say things like:

  function what_else(thing) {
    // but thing is the record, not the representation...
    const type = thing?.[TYPE];
    if (!type) return;
    // - whenever you're adding a presidency, add the president too (with lifespan)
    if (type === "en_us_presidents") {
      return thing.incumbent;
    }

    // - whenever you see an amendment, also the refs in it
    // - whenever you add a ref, add the first event mentioning it, maybe the last too
  }

  // top-level event handlers: things that we can catch when they bubble to the top
  // and which thereby allow us to maintain invariants across larger scopes which can be inert
  // such as:
  // - command buttons
  // - input prompts
  // - findings/results

  function click_handler(event) {
    event.preventDefault();
    const selector = `.special-list > *`; // exploratory
    const touchable = event.target?.closest(selector);
    touchable?.classList.toggle("touched");
  }
  // regarding UI events for pan & zoom
  // see also https://stackoverflow.com/q/54621987
  // see also https://web.dev/mobile-touchandmouse/
  // see also https://danburzo.github.io/ok-zoomer/demos/logger
  // see also https://firefox-source-docs.mozilla.org/performance/scroll-linked_effects.html
  function wheel_handler(/** @type {WheelEvent} */ event) {
    // Ctrl+wheel indicates a zoom action (see https://kenneth.io/post/detecting-multi-touch-trackpad-gestures-in-javascript)
    // but preventing default here raises other issues (and can't be done reliably anyway)
    // also my system doesn't appear to support pinch &c
    // https://community.frame.work/t/osx-style-trackpad-gestures-in-linux/4689
    // also I never saw this happen on any browser on Windows either
    if (event.ctrlKey) return;

    const { deltaX, deltaY, shiftKey } = event;
    if (deltaX && deltaY) return; // ignore diagonals

    const timeframe = get_closest_timeframe_from(event.target);
    if (!timeframe) return;

    const frame = timeframe.get_major_scalars();
    if (!frame) return; // the timeframe is “any time”
    const [from, to] = frame;

    const box = timeframe.element.getBoundingClientRect();
    const offset = event.clientX - box.left;
    const r = offset / box.width;

    const DEFAULT_MOVER_SPEC = {
      pan_alpha: 1 / 16,
      zoom_in_alpha: 7 / 8,
      zoom_out_alpha: 9 / 8,
      max_size: 8000 * YEARS_PER_MAJOR_HACK,
      min_size: (1 / 365) * YEARS_PER_MAJOR_HACK,
      hi: 5000 * YEARS_PER_MAJOR_HACK,
      lo: -9000 * YEARS_PER_MAJOR_HACK,
    };
    const movers = make_movers({ bounds: DEFAULT_MOVER_SPEC });

    // Ignore all magnitudes from the event, as they are highly variant
    let target;
    // or we could fire a pan/zoom event at timeframe (call it a request)
    if (deltaY < 0 && shiftKey) target = movers.zoom_in(from, to, r);
    else if (deltaY > 0 && shiftKey) target = movers.zoom_out(from, to, r);
    else if (deltaX < 0) target = movers.pan_left(from, to);
    else if (deltaX > 0) target = movers.pan_right(from, to);

    if (!target) return;
    const [target_from, target_to] = target;
    timeframe.set_major_scalars(target_from, target_to);
  }

  function default_handler(event) {
    if (event instanceof CustomEvent) {
      const { type, detail } = event;
      console.log("generic dispatch", type, detail);
      // put this somewhere that we know additions will be handled responsibly
      // link_to(detail);
      if (Array.isArray(detail)) {
        const list = document.createElement("ul");
        // but get rid of one liners
        list.classList.add("special-list", "with-margin-inline", "one-liners");
        for (const value of detail) {
          const item = document.createElement("li");
          const line = as_line(value);
          // item.append(link_to(value) ?? "undescribable");
          item.append(line ?? "undescribable");
          list.append(item);
          const more = what_else(detail);
          if (more) {
            // what is more?  a ref usually, right
            // const other_thing
            // list.append(more);
          }
        }
        event.target.append(list);
      }
    } else {
      console.log("unrecognized event type", event.type);
    }
  }
  document.documentElement.addEventListener("wheel", wheel_handler);
  document.documentElement.addEventListener("click", click_handler);
  document.documentElement.addEventListener("default", default_handler);
  document.documentElement.addEventListener("found", default_handler);

  system.spawn(init);

  // Things that we want to know to be true at the end of main:
  // - any ongoing work is divided exclusively into processes
  // - I can see what processes are still running and why
  // - I can stop all processes
}

main();

// get the commands that you can do on a X
