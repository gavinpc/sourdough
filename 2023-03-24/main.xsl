<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0'
               xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
               xmlns:ex='http://exslt.org/common'
               xmlns:date="http://exslt.org/dates-and-times"
               exclude-result-prefixes='ex date'>
  <xsl:output omit-xml-declaration="yes" />
  <xsl:include href="xsl/index.xsl" />

  <!-- HTML5 script does not expect escapes -->
  <!-- this is also the case in style at least for > -->
  <xsl:template mode="main" match="script|style">
    <xsl:copy>
      <xsl:apply-templates mode="main" select="@*" />
      <xsl:value-of select="text()" disable-output-escaping="yes" />
    </xsl:copy>
  </xsl:template>

  <!-- prevent early-close on most elements -->
  <xsl:template
    mode="main"
    match="*[not(node())][not(self::br or self::hr or self::link or self::img or self::meta or self::input)]">
    <xsl:copy>
      <xsl:apply-templates mode="main" select="@*" />
      <xsl:comment />
    </xsl:copy>
  </xsl:template>
  
  <!-- moult container -->
  <xsl:template mode="main" match="*|@*">
    <xsl:copy>
      <xsl:apply-templates mode="main" select="node()|@*" />
    </xsl:copy>
  </xsl:template>
  <xsl:template mode="main" match="html | body | main">
    <xsl:apply-templates mode="main" select="node()|@*" />
  </xsl:template>
  <xsl:template mode="main" match="head" />

  <xsl:template match="/">
    <xsl:apply-templates mode="main" select="." />
  </xsl:template>

</xsl:transform>
