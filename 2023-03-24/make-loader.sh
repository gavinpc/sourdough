#!/bin/bash

# Given an XML file, emit JS that will dump its contents somewhere retrievable
# by other scripts.

set -e

name="$1"

echo '{'
echo -n ' globalThis.XML_'${name}' = `'
sed 's/`/\\`/g'
echo '`'
echo '}'

exit 0
