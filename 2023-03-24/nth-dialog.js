function make_dialog(spec) {
  const doc = document;
  // If you want to use an actual HTML dialog element
  // then opening & styling it are your job
  const dialog = doc.createElement("div");
  dialog.setAttribute("role", "dialog");
  let _stopped = false;

  const DEFAULTS = {
    read: read_new_form,
    print: basically_print,
    interpret: echo,
  };

  const { read, interpret, print } = { ...DEFAULTS, ...spec };

  async function read_new_form({ container }) {
    const doc = container.ownerDocument;
    const form = doc.createElement("form");
    container.append(form);
    return new Promise(resolve => {
      const input = doc.createElement("input");
      form.append(input);
      input.focus(); // hmmm
      form.onsubmit = event => {
        event.preventDefault();
        resolve(form.querySelector("input").value);
      };
    });
  }

  function basically_print(what) {
    const text = describe(what);
    return text;
  }

  function echo(x) {
    return x;
  }

  async function take_turns_talking() {
    while (!_stopped) {
      const prompt = await read({ container: dialog });
      if (_stopped) break;
      if (!prompt) continue;
      const interpretation = await interpret(prompt);
      if (_stopped) break;
      if (!interpretation) continue;
      const representation = print(interpretation);
      const doc = dialog.ownerDocument;
      const output = doc.createElement("output");
      const saying = doc.createElement("p");
      saying.innerText = representation;
      output.append(saying);
      dialog.append(output);
    }
  }

  function start() {
    const promise = take_turns_talking();
  }
  function stop() {
    _stopped = true;
  }
  function dispose() {
    stop();
    dialog.remove();
  }
  return { start, stop, dispose, element: dialog };
}

// ----------------------------------------------------------------------------------------

function test_make_dialog(
  ele = document.documentElement.querySelector("main,body")
) {
  async function interpret() {}
  function print() {}

  // const dialog = make_dialog({ interpret, print });
  const dialog = make_dialog();
  dialog.element.classList.add("chat");
  const title = document.createElement("h2");
  title.innerText = "whatever you say";
  dialog.element.prepend(title);

  function start() {
    // dialog.element.setAttribute("open", "");
    dialog.start();
  }
  function stop() {
    dialog.stop();
  }

  ele.append(dialog.element);
  return { start, stop };
}
