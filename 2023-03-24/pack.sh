#!/bin/sh

# Emit an HTML document that embeds all of the files given as arguments.

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"

# Emit an HTML file that includes all of the listed files.
# Assumes listed files are relative to pwd.
pack_up() {
    echo '<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="initial-scale=1" />
  <link rel="icon" href="data:," />
  <title>composition notebook</title>
</head>
<body>
'
    if [ -f main.html ]; then
        echo '<main>'
        cat main.html;
        echo '</main>'
    fi
    echo '<details>'
    echo '<summary>what do I know?</summary>'
    cat "$@"
    echo '
  <footer>
    <details>
      <summary>ensure download link</summary>
      <script>
{
'
    cat link.js
    echo '
      ensure_download_link();
}
      </script>
      </details>
    </footer>
  </details>
</body>
</html>'
}

pack_up "$@"

exit 0
