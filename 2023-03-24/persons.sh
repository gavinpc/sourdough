#!/bin/sh

# Extract the `person` records from the indicated Wikipedia article

set -e

file="$1"
xref="$2"

name="${file##*/}"
title="${name%.html}"

# https://www.pcre.org/original/doc/html/pcrepattern.html#SEC5
# --fixed-strings doesn't quite do it because we must only match at start of line
set +e
line="$(grep -P --max-count 1 "^\Q$title\E " "$xref")"
set -e
date="${line##* }"

if [ -z "$date" ]
then
    >&2 echo "No date for $file ($title)"
    echo "<persons/>"           # prevent error trying to open empty document
    exit 0                      # ignore, it's not a timeline article
fi

xsltproc --html --stringparam year "$date" persons.xsl "$file"

exit 0
