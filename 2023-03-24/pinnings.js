const PINNINGS = [
  {
    title: "Copernican Revolution",
    seeAlso: "https://en.wikipedia.org/wiki/Category:Copernican_Revolution",
    wiki: [
      "Tycho_Brahe",
      "Nicolaus_Copernicus", // redirects from “copernicus”
      "Galileo_Galilei", // redirects from “galileo”
      "Johannes_Kepler", // redirects from “kepler”
      "Lodovico_delle_Colombe",
      "Hans_Lipperhey",
      "Isaac_Newton", // does *not* redirect from “newton”!
      "Sidereus_Nuncius", // the first published scientific work based on observations made through a telescope
      // other physics stuff...
      "Michael_Faraday", // redirects from “faraday”
      "James_Clerk_Maxwell", // does not redirect from “maxwell”
    ],
  },
  {
    wiki: [
      "Wolfgang_Amadeus_Mozart", // redirects from “mozart”
      "Johann_Sebastian_Bach", // redirects from “bach”
      "Joseph_Haydn", // redirects from “haydn”
      "Ludwig_van_Beethoven", // redirects from “beethoven”
      "Gustav_Mahler", // redirects from “mahler”
    ],
  },
  {
    wiki: [
      "William_Shakespeare", // redirects from “shakespeare”
      "Ben_Jonson",
      "Christopher_Marlowe", // does not redirect from “marlowe”
      // one of these things is not like the others...
      "Samuel_Johnson",
    ],
  },
  {
    title: "Ninja turtles",
    // Leonardo redirects from “da vinci”
    wiki: ["Michelangelo", "Leonardo_da_Vinci", "Donatello", "Raphael"],
  },
];
