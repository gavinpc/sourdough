const NULL_BUFFER = { is_empty: () => true, is_full: () => true, length: 0 };
const make_ring_buffer = size => {
  if (size === 0) return NULL_BUFFER;
  const _buffer = new Array(size);
  // aka head/tail
  let _start = 0; // where the first item is (when ≠ end)
  let _end = 0; // where the next item will go
  let _length = 0;

  return {
    is_empty: () => _length === 0,
    is_full: () => _length === size,
    push(value) {
      console.assert(_length < size, `Full!`);
      ++_length;
      _buffer[_end] = value;
      _end = (_end + 1) % size;
    },
    pop() {
      console.assert(_length !== 0, `Empty!`);
      --_length;
      const value = _buffer[_start];
      _start = (_start + 1) % size;
      return value;
    },
    get length() {
      return _length;
    },
  };
};
const { stringify } = JSON;
const { assert } = console;
const cheap_equals = (a, b) => stringify(a) == stringify(b);
function assert_cheap_equals(a, b, ...messages) {
  if (cheap_equals(a, b)) return;
  console.error(...messages);
  throw new Error(`${a} ≠ ${b}`);
}
function test_ring_buffer() {
  {
    const buffer = make_ring_buffer(0);
    assert_cheap_equals(buffer.length, 0);
    assert(buffer.is_full(), `a zero-length buffer is always full!`);
    assert(buffer.is_empty(), `a zero-length buffer is always empty!`);
  }
  {
    const buffer = make_ring_buffer(1);
    assert_cheap_equals(buffer.length, 0);
    assert(!buffer.is_full(), `new 1-buffer is not full!`);
    assert(buffer.is_empty(), `buffer is initially empty!`);
    buffer.push("x");
    assert(buffer.is_full(), `1-buffer is full after 1 push`);
    assert(!buffer.is_empty(), `1-buffer is not empty after 1 push`);
    const got = buffer.pop();
    assert_cheap_equals(got, "x");
    assert(!buffer.is_full(), `1-buffer is not full after push-pop`);
    assert(buffer.is_empty(), `1-buffer is empty after push-pop`);
  }
  function true_of_empty_2(buffer = make_ring_buffer(2)) {
    assert_cheap_equals(buffer.length, 0);
    assert(!buffer.is_full());
    assert(buffer.is_empty());

    buffer.push(4);
    assert_cheap_equals(buffer.length, 1);
    assert(!buffer.is_full(), `2-buffer is not full after 1 push`);
    assert(!buffer.is_empty(), `2-buffer is not empty after 1 push`);

    buffer.push(2);
    assert_cheap_equals(buffer.length, 2);
    assert(buffer.is_full(), `2-buffer is full after 2 pushes`);
    assert(!buffer.is_empty(), `2-buffer is not empty after 2 pushes`);

    {
      const got = buffer.pop();
      assert_cheap_equals(got, 4);
      assert_cheap_equals(buffer.length, 1);
      assert(!buffer.is_full(), `2-buffer is not full after push-push-pop`);
      assert(!buffer.is_empty(), `2-buffer is not empty after push-push-pop`);
    }

    {
      const got = buffer.pop();
      assert_cheap_equals(got, 2);
      assert_cheap_equals(buffer.length, 0);
      assert(!buffer.is_full(), `2-buffer is not full after push-push-pop-pop`);
      assert(buffer.is_empty(), `2-buffer is empty after push-push-pop-pop`);
    }
  }
  true_of_empty_2();

  // alternate ending...
  {
    const buffer = make_ring_buffer(2);
    buffer.push("butter");
    assert_cheap_equals(buffer.length, 1);
    assert(!buffer.is_full(), `2-buffer is not full after 1 push`);
    assert(!buffer.is_empty(), `2-buffer is not empty after 1 push`);
    {
      const got = buffer.pop();
      assert_cheap_equals(got, "butter");
      assert_cheap_equals(buffer.length, 0);
      assert(!buffer.is_full(), `2-buffer is not full after push-pop`);
      assert(buffer.is_empty(), `2-buffer is empty after push-pop`);
    }
    buffer.push("fly");
    assert_cheap_equals(buffer.length, 1);
    assert(!buffer.is_full(), `2-buffer is not full after push-pop-push`);
    assert(!buffer.is_empty(), `2-buffer is not empty after push-pop-push`);
    {
      const got = buffer.pop();
      assert_cheap_equals(got, "fly");
      assert_cheap_equals(buffer.length, 0);
      assert(!buffer.is_full(), `2-buffer is not full after 2 push-pops`);
      assert(buffer.is_empty(), `2-buffer is empty after 2 push-pops`);
    }
    true_of_empty_2(buffer);
  }
  // another alternate ending...
  {
    const buffer = make_ring_buffer(2);
    buffer.push("butter");
    buffer.push("fly");
    assert_cheap_equals(buffer.pop(), "butter");
    assert_cheap_equals(buffer.pop(), "fly");
    true_of_empty_2(buffer);
  }
}
test_ring_buffer();
