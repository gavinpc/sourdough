#!/bin/bash

# Run a partial build monitor

set -e

# this is also in sourdough.sh
# do tup monitor in foreground
tup_watch() {
    # why not just trap EXIT?
    trap 'tup stop' INT TERM
    tup monitor --foreground --autoupdate "$@" &
    monitor_pid="$!"
    wait "$monitor_pid"
}

tup_watch \
    notebook.html \
    wiki/persons/en/1346.xml \
    wiki/persons/en/177*.xml \
    wiki/persons/en/500*.xml

exit 0
