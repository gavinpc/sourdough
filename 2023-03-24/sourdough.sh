#!/bin/bash

# cli entry point, adapted from earlier notebook

tup-unfinished-business() {
    if ! tup parse # undocumented
    then
        >&2 echo "fatal: tup parse failed with status $?"
        false
    fi
    tup todo "$@" 2>&1 | grep -q 'will be executed'
}

tup-fixpoint() {
    # without error trap on `tup`, this goes into an infinite loop
    set -e
    while tup-unfinished-business "$@"
    do
        tup
    done
}

tup-poll() {
    while true
    do
        tup-fixpoint "$@"
        sleep 2s
    done
}

# ASSUMES you're generating a .gitignore
# and, I suppose, that this is a git repo
# although ideally you could clean using .gitignore without an actual repo
# does this take `git clean` args (e.g. dry run)
# or a list of targets, or both or ,what
tup-clean() {
    local target="${1:-.}"
    git clean "$target" -Xf
}

# run a monitor that dies with this shell
tup-watch() {
    # Run in `foreground` mode as a background job so signals are propagated.
    trap 'tup stop' INT TERM
    # monitor is Linux only, will fail on other systems.  I don't know what they all report
    if [ "$OS" == Windows_NT ]
    then
        tup-poll
    else
        tup monitor --autoupdate --foreground "$@" &
    fi
    monitor_pid="$!"
    wait "$monitor_pid"
}

shell-open() {
    local location="$1"
    if known-command open; then open "$location"
    elif known-command cygstart; then cygstart "$location"
    elif known-command cmd
    then
        # this way madness lies
        cmd /C "start $location"
    fi
}

open-main() {
    if [ -f notebook.html ]; then shell-open notebook.html
    elif [ -f main-in.html ]; then shell-open main-in.html
    fi
}

start-system() {
    # Complete a one-time build without parallelism before starting monitor.  This
    # prevents the build from flooding remote hosts with requests.  It's kind of a
    # hack, since nothing would stop such floods from happening if they were
    # introduced while monitoring.  It also means that if the build is initially
    # broken, then you need to keep re-running this script until you get it working
    # again. Ultimately, commands should handle this.
    tup-fixpoint "$@" -j1;
    # Still, if the above failed on any system, even one having monitor, you
    # don't want to start the monitor with parallelism yet
    open-main &
    tup-watch "$@"
}

new-filename() {
    local prefix="$1"
    local suffix="$2"

    # for granularity in date hours minutes seconds
    # those --iso-8601 options are lovely, but why stop at date?
    # also they leak time zone, a bad privacy default
    # this splits date into year-month-day
    # forms are all ISO-8601
    local format=''
    # should have Z on hour- and minutes-only version too?
    for part in '%Y' '-%m' '-%D' 'T%H' ':%M' ':%sZ'
    do
        format="${format}${part}"
        local date="$(date --utc +$format)"
        local file="${prefix}${date}${suffix}"

        if [ ! -e "$file" ]
        then
            echo -n "$file"
            return
        fi
    done
    mktemp --tmpdir=. "${prefix}${date}-XXXXXX-${suffix}"
}

# Was for zip but superseded
# list-ignored() {
#     git ls-files --others --ignored --exclude-standard
# }

known-command() { command -v "$1" > /dev/null; }

git-root() {
    if known-command git
    then
        git rev-parse --show-toplevel
    else
        # look for a .git directory? but if you don't have git,
        # you probably won't have that either
        # in that case (in the context of tar --exclude-vcs*)
        # we would just use the same context directory as we otherwise would
        >&2 echo 'Not implemented! finding git root without git'
        :
    fi
}

# make a (compressed) snapshot of the current state of the project's essential's
# unlike git archive, includes untracked files and changes in working directory.
zip-project() {
    local to_file="$1"

    if [ -z "$to_file" ]
    then
        to_file="$(new-filename '../' '--snapshot.tar.gz')"
    fi

    # I seem to have a different version of zip than described in the man pages
    # zip --recurse-paths --exclude @<(list-ignored) --output-file "$zip_file" .

    # tar's vcs options only work when you're in the vcs project root (at least for Git).
    # But the sourdough/Tup root might not be the Git root.
    # ALSO... there might not be a git root, or git
    local real_zip="$(realpath "$to_file")"
    local dir="$(realpath .)"
    local git_root="$(git-root)"

    cd "$git_root"
    tar --create --gzip \
        --directory="$git_root" \
        --verbose \
        --file="$real_zip" \
        --exclude="$real_zip" \
        --exclude-vcs \
        --exclude-vcs-ignores \
        "$dir"
    echo "$real_zip"
}

# zip project in its current state, then unpack it to a new directory
snapshot-project() {
    local given_name="$1"

    local zip_name="${given_name:-snapshot}"
    local zip_file="$(new-filename '../' "--$zip_name.tar.gz")"
    zip-project "$zip_file" >/dev/null

    local new_name="${given_name:-`date +'%A'`}"
    local new_dir=$(new-filename '../' "--$new_name")
    local parent="$(dirname "$zip_file")"
    local absolute="$(realpath $zip_file)"
    >&2 echo "zip_name = ${zip_name}"
    >&2 echo "zip_file = ${zip_file}"
    >&2 echo "new_name = ${new_name}"
    >&2 echo "new_dir = ${new_dir}"
    >&2 echo "parent = ${parent}"
    return
    mkdir -p "$new_dir"
    tar --directory="$new_dir" --extract --file="$absolute"
}


# ASSUMES tup and any other deps
main() {
    case "$1" in
        clean) tup-clean "${@:2}" ;;
        fix*) tup-fixpoint "${@:2}" ;;
        poll*) tup-poll "${@:2}" ;;
        watch) tup-watch "${@:2}" ;;
        start) start-system "${@:2}" ;;
        ign*) list-ignored "${@:2}" ;;
        new) new-filename "${@:2}" ;;
        zip) zip-project "${@:2}" ;;
        snap) snapshot-project "${@:2}" ;;
        *) start-system "${@:2}" ;;
    esac
}

script_was_executed() {
    test "$0" == "$BASH_SOURCE"
}

# so you can source the script without effect
if script_was_executed
then
    main "$@"
    exit 0
fi
