define("~/explore/adt/tries", [], () => {
  // the keys have to have reference (Map, Set) identity
  const { defineProperty, defineProperties, hasOwn } = Object;
  const { isArray } = Array;
  const { assert } = console;
  const make_node = () => ({ children: new Map(), size: 0 });

  // data properties are writable though...
  // https://xlinux.nist.gov/dads/HTML/trie.html
  class Trie {
    constructor(iterate, options) {
      const multi = options?.multi ?? false;
      const monotonic = options?.monotonic ?? false;
      this.functional = !multi;
      this.root = make_node();
      if (typeof iterate === "function") this.iterate = iterate;
    }
    has(item) {
      let node = this.root;
      for (const key of this.iterate ? this.iterate(item) : item)
        if (node.children.has(key)) node = node.children.get(key);
      return hasOwn(node, "data");
    }
    get(item) {
      let the = this.root;
      for (const key of this.iterate ? this.iterate(item) : item)
        if (the.children.has(key)) the = the.children.get(key);
      return the?.data;
    }
    add(thing, data) {
      // const path = [];
      const passed = [];
      let node = this.root;
      for (const key of this.iterate ? this.iterate(thing) : thing) {
        // path.push(key);
        passed.push(node);
        const { children } = node;
        if (children.has(key)) node = children.get(key);
        else children.set(key, (node = make_node()));
      }
      assert?.(node, "whither node?");
      if (hasOwn(node, "data")) {
        if (this.functional) return false; // reject/conflict
        assert?.(isArray(node?.data), "bad books");
        node.data.push(thing);
        for (const node of passed) node.size++;
        return true;
      }
      for (const node of passed) node.size++;
      const it = data === void 0 ? thing : data;
      const value = this.functional ? it : [it];
      defineProperty(node, "data", { value, enumerable: true });
      return true;
    }
    // like add but return the data instead of true/false
    // TODO: should avoid the double lookup
    ensure(thing, data) {
      if (this.add(thing)) return data === void 0 ? thing : data;
      return this.get(thing);
    }
    get size() {
      return this.root.size;
    }
    get children() {
      return this.root.children;
    }
    keys() {
      return this.root.children.keys();
    }
    [Symbol.iterator]() {
      return this.root.children.entries();
    }
  }

  function make_trie(...args) {
    return new Trie(...args);
  }
  return { make_trie: (...args) => new Trie(...args) };
});

define("~/explore/adt/tries/test", ["~/def/equal", "~/explore/adt/tries"], (
  equal,
  tries
) => {
  const { make_trie } = tries;
  const { assert } = console;

  {
    const trie = make_trie(undefined, { multi: true });
    assert(equal(trie.size, 0), "initial size is zero");
    assert(equal([...trie.keys()], []), "initial keys are empty");
    assert(trie.add("leaf"), "multiset accepts new value");
    assert(equal(trie.size, 1), "size increments");
    assert(equal([...trie.keys()], ["l"]), "key is consumed");
    assert(trie.add("leaf"), "multiset accepts existing value");
    assert(equal(trie.size, 2), "size increments from repeat values");
  }

  {
    function* spo(triple) {
      yield triple.subject;
      yield triple.predicate;
      yield triple.object;
    }
    const trie = make_trie(spo);
    assert(trie.add({ subject: "G", predicate: "has", object: "O" }), "fact");
    assert(equal(trie.size, 1), "functional size increments");
    assert(!trie.add({ subject: "G", predicate: "has", object: "O" }), "known");
    assert(equal(trie.size, 1), "no size increment after failed add");
    trie.add({ subject: "G", predicate: "has", object: "K" }, "new fact");
    assert(equal(trie.size, 2), "functional size incremented again");
  }

  {
    // use like a map
    const trie = make_trie(s => s.split(/\./g));
    assert(trie.add("def", "the start"), "set at level 1");
    assert(equal(trie.get("def"), "the start"), "get back at level 1");
    assert(trie.add("def.codes", "something"), "set at level 2");
    assert(equal(trie.get("def.codes"), "something"), "get back at level 2");
    assert(equal(trie.get("def"), "the start"), "get back at level 1 again");
  }

  {
    // heterogeneous tree
    function* f(it) {
      const { type } = it;
      yield type;
      if (type === "rgb") yield* [it.r, it.g, it.b];
      else if (type === "hsl") yield* [it.h, it.s, it.l];
    }
    const trie = make_trie(f);
    const rgb1 = { type: "rgb", r: 0, g: 100, b: 10 };
    const rgb1a = { type: "rgb", r: 0, g: 100, b: 10 };
    const rgb2 = { type: "rgb", r: 0, g: 100, b: 50 };
    const hsl1 = { type: "hsl", h: 60, s: 100, l: 20 };
    const hsl2 = { type: "hsl", h: 125, s: 100, l: 30 };
    assert(!trie.has(rgb1));
    assert(trie.add(rgb1));
    assert(trie.has(rgb1));
    assert(trie.get(rgb1) === rgb1);
    assert(trie.has(rgb1a), "already has equivalent");
    assert(!trie.add(rgb1a), "doesn't add duplicate");
    assert(trie.get(rgb1a) === rgb1, "returns equivalent");
    for (const color of [rgb2, hsl1, hsl2]) assert(trie.add(color));
    assert(trie.size === 4);
    const arr = [...trie];
    console.debug(arr);
  }
});

define("~/explore/rdfjs", ["~/explore/adt/tries"], tries => {
  // I thought they had adopted rdf-star, but this is the only indication of it
  // https://github.com/rdfjs/data-model-spec/commit/4312f6f40b50497a8def379e0e5e688836b63fca
  const { defineProperties } = Object;

  // the actual anchors use this form but whatever
  // https://rdf.js.org/data-model-spec/#dom-namednode
  const RDFJS = "https://rdf.js.org/data-model-spec/#";
  const RECS = def.mint.in(RDFJS).records;

  const { NamedNode, BlankNode, Variable, Literal, DefaultGraph, Quad } = RECS;
  for (const C of [NamedNode, BlankNode, Variable, Literal, DefaultGraph, Quad])
    defineProperties(C.prototype, {
      // see also def.errors.  local name should be a thing
      termType: {
        value: C.prototype.constructor.name.split("#")[1], // HACK: we want local name
        enumerable: true,
      },
    });
  NamedNode.prototype.equals = function NamedNode_equals(other) {
    return (
      !!other && other.termType === "NamedNode" && other.value === this.value
    );
  };
  BlankNode.prototype.equals = function BlankNode_equals(other) {
    return (
      !!other && other.termType === "BlankNode" && other.value === this.value
    );
  };
  Variable.prototype.equals = function Variable_equals(other) {
    return (
      !!other && other.termType === "Variable" && other.value === this.value
    );
  };
  DefaultGraph.prototype.equals = function DefaultGraph_equals(other) {
    return !!other && other.termType === "DefaultGraph";
  };
  Literal.prototype.equals = function Literal_equals(other) {
    return (
      !!other &&
      other.termType === "Literal" &&
      this.value === other.value &&
      this.language === other.language &&
      this.datatype.equals(other.datatype)
    );
  };
  Quad.prototype.equals = function Quad_equals(other) {
    return (
      !!other &&
      this.subject.equals(other.subject) &&
      this.predicate.equals(other.predicate) &&
      this.object.equals(other.object) &&
      this.graph.equals(other.graph)
    );
  };

  Object.defineProperties(DefaultGraph.prototype, {
    value: { value: "", enumerable: true },
  });
  Object.defineProperties(Quad.prototype, {
    value: { value: "", enumerable: true },
  });

  const defaultGraph = new DefaultGraph();

  const base_factory = {
    namedNode: value => new NamedNode({ value }),
    blankNode: value =>
      new BlankNode({ value: value || `b${++blankNodeCount}` }),
    literal: (value, languageOrDatatype) => {
      let language, datatype;
      if (typeof languageOrDatatype === "string") {
        language = languageOrDatatype;
        datatype = LANGUAGE_STRING_DATATYPE;
      } else {
        language = "";
        datatype = languageOrDatatype || STRING_DATATYPE;
      }
      return new Literal({ value, language, datatype });
    },
    variable: value => new Variable({ value }),
    defaultGraph: () => defaultGraph,
    triple: (s, p, o) =>
      new Quad({ subject: s, predicate: p, object: o, graph: defaultGraph }),
    quad: (s, p, o, g) =>
      Quad({ subject: s, predicate: p, object: o, graph: g ?? defaultGraph }),
  };

  const RDF = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
  const XSD = "http://www.w3.org/2001/XMLSchema#";
  const STRING_TYPE_IRI = `${XSD}string`;
  const LANGUAGE_STRING_TYPE_IRI = `${RDF}langString`;

  let blankNodeCount = 0;

  const STRING_DATATYPE = new NamedNode({ value: STRING_TYPE_IRI });
  const LANGUAGE_STRING_DATATYPE = new NamedNode({
    value: LANGUAGE_STRING_TYPE_IRI,
  });

  // ============== IDENTITY FACTORY

  /// an rdf.js implementation that keeps reference identity between equivalent
  /// values (excluding triples/quads).  it's built on top of the vanilla impl.

  function* term_keys(term) {
    const type = term.termType;
    yield type;
    if (type === "NamedNode" || type === "BlankNode" || type === "Variable")
      yield term.value;
    else if (type === "Literal") {
      yield term.datatype;
      yield term.value;
      if (term.datatype === LANGUAGE_STRING_DATATYPE) yield term.language;
    } else if (type === "Quad")
      // but we don't actually memoize this
      yield* [term.graph, term.predicate, term.subject, term.object];
  }

  const make_identity_factory = () => {
    const trie = tries.make_trie(term_keys);
    const factory = { ...base_factory };
    for (const [key, base] of Object.entries(base_factory)) {
      // no sense or business memoizing these here
      if (key !== "defaultGraph" && key !== "triple" && key !== "quad") {
        // constructs the term unconditionally, which is not so good
        factory[key] = (...args) => trie.ensure(base(...args));
      }
    }
    return factory;
  };

  return make_identity_factory();
});

define("~/explore/rdfjs/test", ["~/explore/rdfjs"], rdf => {
  function test(name, cb) {
    const t = {
      is(a, b, ...messages) {
        if (!Object.is(a, b)) throw Error(`assert fail`);
      },
      not(a, b, ...messages) {
        if (Object.is(a, b)) throw Error(`assert fail`);
      },
      false(it, ...messages) {
        if (it) throw Error(`assert fail`);
      },
      true(it, ...messages) {
        if (!it) throw Error(`assert fail`);
      },
    };
    cb(t);
  }

  test("‘RDF’ export is a conformant DataFactory", t => {
    t.is(rdf, rdf, "Type assertion");
    t.is(typeof rdf.namedNode, "function");
    t.is(typeof rdf.blankNode, "function");
    t.is(typeof rdf.literal, "function");
    t.is(typeof rdf.variable, "function");
    t.is(typeof rdf.defaultGraph, "function");
    t.is(typeof rdf.quad, "function");
    // t.is(typeof RDF.fromTerm, "function");
    // t.is(typeof RDF.fromQuad, "function");
  });

  // Check for 1-ary construction
  const check = (t, ctor, termType, value1, value2) => {
    const a1 = ctor(value1);
    const a2 = ctor(value1);
    const b = ctor(value2);
    t.is(a1.termType, termType);
    t.is(a2.termType, termType);
    t.is(b.termType, termType);
    t.is(a1.value, value1);
    t.is(a2.value, value1);
    t.is(b.value, value2);
    t.false(a1.equals(null));
    t.true(a1.equals(a1));
    t.true(a1.equals(a2));
    t.true(a2.equals(a1));
    t.false(a1.equals(b));
    t.false(a2.equals(b));
    t.false(b.equals(a1));
    t.false(a1.equals(b));
    return a1;
  };

  test("NamedNode constructor creates a conformant term", t => {
    const nn = check(
      t,
      rdf.namedNode,
      "NamedNode",
      "http://example.com/one",
      "http://example.com/two"
    );
    t.is(nn, nn, "type test");
    t.false(nn.equals(rdf.variable(nn.value)));
    t.false(nn.equals(rdf.blankNode(nn.value)));
    t.false(nn.equals(rdf.literal(nn.value)));
  });

  test("NamedNode constructor does not validate the form of the term", t => {
    t.is(rdf.namedNode("not an IRI").value, "not an IRI");
    t.is(rdf.namedNode("❤").value, "❤");
  });

  test("BlankNode constructor creates a conformant term", t => {
    const bn = check(t, rdf.blankNode, "BlankNode", "one", "two");
    t.is(bn, bn, "type test");
    t.false(bn.equals(rdf.namedNode(bn.value)));
    t.false(bn.equals(rdf.variable(bn.value)));
    t.false(bn.equals(rdf.literal(bn.value)));
  });

  test("BlankNode constructure mints unused names", t => {
    const b1 = rdf.blankNode();
    const b2 = rdf.blankNode();
    t.false(b1.equals(b2));
    t.false(b2.equals(b1));
  });

  test("Literal constructor creates a conformant term", t => {
    const lit = check(t, rdf.literal, "Literal", "Rosencrantz", "Guildenstern");
    t.is(lit, lit, "type test");
    const STRING = rdf.namedNode("http://www.w3.org/2001/XMLSchema#string");
    t.true(lit.datatype.equals(STRING));
    t.false(lit.equals(rdf.namedNode(lit.value)));
    t.false(lit.equals(rdf.blankNode(lit.value)));
    t.false(lit.equals(rdf.variable(lit.value)));
  });

  test("Literal constructor uses xsd:string as default type", t => {
    t.true(
      rdf
        .literal("foo")
        .datatype.equals(
          rdf.namedNode("http://www.w3.org/2001/XMLSchema#string")
        )
    );
  });

  test("Literal constructor uses datatype when provided", t => {
    t.true(
      rdf
        .literal(
          "98.6",
          rdf.namedNode("http://www.w3.org/2001/XMLSchema#float")
        )
        .datatype.equals(
          rdf.namedNode("http://www.w3.org/2001/XMLSchema#float")
        )
    );
  });

  test("Literal constructor uses language tag when provided", t => {
    t.is(rdf.literal("nada").language, "");
    t.is(rdf.literal("mañana", "es").language, "es");
    t.true(rdf.literal("plaza").equals(rdf.literal("plaza")));
    t.true(rdf.literal("plaza", "en").equals(rdf.literal("plaza", "en")));
    t.false(rdf.literal("plaza", "en").equals(rdf.literal("plaza")));
    t.false(rdf.literal("plaza", "en").equals(rdf.literal("plaza", "it")));
    t.false(rdf.literal("hermana", "es").equals(rdf.literal("hermano", "es")));
  });

  test("Variable constructor creates a conformant term", t => {
    const va = check(t, rdf.variable, "Variable", "alpha", "beta");
    t.is(va, va, "type test");
    t.false(va.equals(rdf.namedNode(va.value)));
    t.false(va.equals(rdf.blankNode(va.value)));
    t.false(va.equals(rdf.literal(va.value)));
  });

  test("DefaultGraph constructor creates a conformant term", t => {
    const dg1 = rdf.defaultGraph();
    const dg2 = rdf.defaultGraph();
    t.is(dg1, dg1, "type test");
    t.true(dg1.equals(dg1));
    t.true(dg1.equals(dg2));
    t.is(dg1.termType, "DefaultGraph");
    t.is(dg2.termType, "DefaultGraph");
    t.is(dg1.value, "");
    t.is(dg2.value, "");
    t.false(dg1.equals(rdf.namedNode("defaultGraph")));
    t.false(dg1.equals(rdf.blankNode()));
  });

  test("Quad constructor creates a conformant quad", t => {
    const g1 = rdf.namedNode("ex:graph1");
    const alice = rdf.namedNode("ex:Alice");
    const bob = rdf.namedNode("ex:Bob");
    const rank = rdf.namedNode("ex:rank");
    const float = rdf.namedNode("http://www.w3.org/2001/XMLSchema#float");
    const five = rdf.literal("5.0", float);
    const a1 = rdf.quad(alice, rank, five);
    const a2 = rdf.quad(alice, rank, five);
    const b = rdf.quad(bob, rank, five);
    const c = rdf.quad(bob, rank, five, g1);
    t.is(a1.termType, "Quad");
    t.is(a2.termType, "Quad");
    t.is(b.termType, "Quad");
    t.is(a1.value, "");
    t.is(a2.value, "");
    t.is(b.value, "");
    t.true(a1.subject.equals(alice));
    t.true(a1.predicate.equals(rank));
    t.true(a1.object.equals(five));
    t.true(a1.graph.equals(rdf.defaultGraph()));
    t.true(a2.subject.equals(alice));
    t.true(a2.predicate.equals(rank));
    t.true(a2.object.equals(five));
    t.true(a2.graph.equals(rdf.defaultGraph()));
    t.true(b.subject.equals(bob));
    t.true(b.predicate.equals(rank));
    t.true(b.object.equals(five));
    t.true(b.graph.equals(rdf.defaultGraph()));
    t.true(c.graph.equals(g1));
    t.true(a1.equals(a2));
    t.true(a2.equals(a1));
    t.false(a1.equals(b));
    t.false(a2.equals(b));
    t.false(b.equals(a1));
    t.false(b.equals(a1));
  });

  test("factory creates terms with reference identity", t => {
    t.is(rdf.namedNode("foo"), rdf.namedNode("foo"));
    t.is(rdf.blankNode("foo"), rdf.blankNode("foo"));
    t.not(rdf.namedNode("foo"), rdf.blankNode("foo"));
    t.not(rdf.blankNode("foo"), rdf.namedNode("bar"));
    const b1 = rdf.blankNode();
    t.is(b1, rdf.blankNode(b1.value));
    t.is(rdf.literal("patio"), rdf.literal("patio"));
    t.not(rdf.literal("patio"), rdf.literal("patio", "en"));
    t.is(rdf.literal("patio", "en"), rdf.literal("patio", "en"));
    t.not(rdf.literal("patio", "en"), rdf.literal("patio", "es"));
  });

  /*
  test.skip("fromTerm does what the stupid spec says", t => {
    t.fail("WIP");
  });

  test.skip("fromQuad does what the stupid spec says", t => {
    t.fail("WIP");
  });
*/
});

define("~/explore/weak-ref-thing", ["~/explore/adt/tries"], () => {
  // make a constructor that guarantees reference identity on certain structures
  // without leaking memory
  //
  // to preemptively prune the tree, we need finalization too huh?
  function make_constructor() {
    return function construct_x(properties) {
      // is there already one like this?
      const existing = find(properties);
      if (existing) return existing.deref();
      // else make and register it
      const made = make(properties);
      const wr = new WeakRef(made);
      // and that's the leaf at the end of the trie
      // Object.assign(this, properties);
    };
  }
});

define("~/def/map", ["~/def/generics"], generics => {
  const { make_generic } = generics;
  const map = make_generic({
    name: "map",
    signatures: [(thing, applicable) => {}],
  });
  map.comment = ``;
  return map;
});

define("~/vocab", [], () => {
  `stashing this as it's not yet required in core`;

  const W3 = "http://www.w3.org";
  const W3NS = `${W3}/ns/`;
  const RDFA_INITIAL_CONTEXT = {
    // https://www.w3.org/2011/rdfa-context/rdfa-1.1
    as: `https://www.w3.org/ns/activitystreams#`,
    cc: `http://creativecommons.org/ns#`,
    csvw: `${W3NS}csvw#`,
    ctag: `http://commontag.org/ns#`,
    dc: `http://purl.org/dc/terms/`, // duplicates `dcterms`.
    dc11: `http://purl.org/dc/elements/1.1/`, // a.k.a. `dce` (https://lov.linkeddata.es/dataset/lov/vocabs/dce)
    dcat: `${W3NS}dcat#`,
    dcterms: `http://purl.org/dc/terms/`,
    dqv: `${W3NS}dqv#`,
    duv: `https://www.w3.org/ns/duv#`,
    foaf: `http://xmlns.com/foaf/0.1/`,
    gr: `http://purl.org/goodrelations/v1#`,
    grddl: `${W3}/2003/g/data-view#`,
    ical: `${W3}/2002/12/cal/icaltzd#`,
    jsonld: `${W3NS}json-ld#`,
    ldp: `${W3NS}ldp#`,
    ma: `${W3NS}ma-ont#`,
    oa: `${W3NS}oa#`,
    odrl: `${W3NS}odrl/2/`,
    og: `http://ogp.me/ns#`,
    org: `${W3NS}org#`,
    owl: `${W3}/2002/07/owl#`,
    prov: `${W3NS}prov#`,
    qb: `http://purl.org/linked-data/cube#`,
    rdf: `${W3}/1999/02/22-rdf-syntax-ns#`,
    rdfa: `${W3NS}rdfa#`,
    rdfs: `${W3}/2000/01/rdf-schema#`,
    rev: `http://purl.org/stuff/rev#`,
    rif: `${W3}/2007/rif#`,
    rr: `${W3NS}r2rml#`,
    schema: `http://schema.org/`,
    sd: `${W3NS}sparql-service-description#`,
    sioc: `http://rdfs.org/sioc/ns#`,
    skos: `${W3}/2004/02/skos/core#`,
    skosxl: `${W3}/2008/05/skos-xl#`,
    sosa: `${W3NS}sosa/`,
    ssn: `${W3NS}ssn/`,
    time: `${W3}/2006/time#`,
    v: `http://rdf.data-vocabulary.org/#`,
    vcard: `${W3}/2006/vcard/ns#`,
    void: `http://rdfs.org/ns/void#`,
    wdr: `${W3}/2007/05/powder#`,
    wdrs: `${W3}/2007/05/powder-s#`,
    xhv: `${W3}/1999/xhtml/vocab#`,
    xml: `${W3}/XML/1998/namespace`,
    xsd: `${W3}/2001/XMLSchema#`,
    describedby: `${W3}/2007/05/powder-s#describedby`,
    license: `${W3}/1999/xhtml/vocab#license`,
    role: `${W3}/1999/xhtml/vocab#role`,
  };

  const prefixes = {
    ...RDFA_INITIAL_CONTEXT,
    rdfg: `${W3}/2004/03/trix/rdfg-1/`,
    // Survey of basis for this: https://stackoverflow.com/a/34407652
    html: `${W3}/1999/xhtml`,
    // but wait, what about (xhv above) https://www.w3.org/1999/xhtml/vocab#
    svg: `${W3}/2000/svg`,
    // - module ID's in this space resolve to JSON metadata about the package.
    npm: "https://registry.npmjs.org/",
    // shoutout for xsl
  };

  // provisional: term lists for discovery/completion
  const terms = map_object(
    {
      xsd: "float double int long short byte unsignedByte unsignedShort unsignedInt unsignedLong decimal integer nonPositiveInteger nonNegativeInteger positiveInteger negativeInteger boolean string normalizedString anyURI token Name QName language NMTOKEN ENTITIES NMTOKENS ENTITY ID NCName IDREF IDREFS NOTATION hexBinary base64Binary date time dateTime duration gDay gMonth gYear gYearMonth gMonthDay",
      dc11: "contributor coverage creator date description format identifier language publisher relation rights source subject title type",
      dcterms:
        "Agent AgentClass BibliographicResource Box DCMIType DDC FileFormat Frequency IMT ISO3166 ISO639-2 ISO639-3 Jurisdiction LCC LCSH LicenseDocument LinguisticSystem Location LocationPeriodOrJurisdiction MESH MediaType MediaTypeOrExtent MethodOfAccrual MethodOfInstruction NLM Period PeriodOfTime PhysicalMedium PhysicalResource Point Policy ProvenanceStatement RFC1766 RFC3066 RFC4646 RFC5646 RightsStatement SizeOrDuration Standard TGN UDC URI W3CDTF abstract accessRights accrualMethod accrualPeriodicity accrualPolicy alternative audience available bibliographicCitation conformsTo contributor coverage created creator date dateAccepted dateCopyrighted dateSubmitted description educationLevel extent format hasFormat hasPart hasVersion identifier instructionalMethod isFormatOf isPartOf isReferencedBy isReplacedBy isRequiredBy isVersionOf issued language license mediator medium modified provenance publisher references relation replaces requires rights rightsHolder source spatial subject tableOfContents temporal title type valid",
    },
    function (terms, prefix) {
      return Object.assign(
        Object.create(null),
        Object.fromEntries(
          terms.split(/ /g).map(term => [term, `${prefixes[prefix]}${term}`])
        )
      );
    },
    null
  );

  return { prefixes, terms };
});

define("~/js/is_constructor", [], () => {
  const is_constructor = it =>
    typeof it === "function" && it.prototype?.constructor === it;

  is_constructor.comment = `Answer for some C whether ∃ x: x instanceof C`;
  is_constructor.seeAlso = `https://stackoverflow.com/a/57862312`;
  return is_constructor;
});

define("~/js/is_constructor/tests", ["~/js/is_constructor"], is_constructor => {
  let called = false;
  const YES = [
    class A {},
    function () {}, // very marginal case... name===""
    function A() {},
    new Function(),
    class Bugged {
      constructor() {
        called = true;
      }
    },

    Symbol,
    ...[Boolean, Number, BigInt, String, Object, Function],
    ...[Array, Date, RegExp, ArrayBuffer, URL, Map, Set],
    ...[Blob], // a Node built-in but needs an import, right?
    ...[Element, WebSocket, Worker], // browser-only
    def.mint.in("ex:").tuples.A,
    def.mint.in("ex:").records.B,
    // false negative... this (nightmare) is in fact constructable & works with instanceof
    // it's a `BoundFunctionObject`, which is another hidden built-in
    // class A {}.bind({}), //
    // need to dig to get these
    // GeneratorFunction
    // AsyncFunction
    // AsyncGeneratorFunction?
  ];
  const NO = [
    ...[undefined, null, true, 1, BigInt(1), "", "a", {}, [], () => {}, /^/],
    ...[Symbol(), Symbol.for("a")],
    { a() {} }.a,
    function* A() {},
    async function A() {},
    async function* A() {},
    ...[Math, Math.sin, setTimeout],
    // Function.prototype,  special case?
  ];
  function test_is_constructor() {
    const { assert } = console;
    for (const yes of YES) assert(is_constructor(yes), "s/b ctor", yes);
    for (const no of NO) assert(!is_constructor(no), "s/b !ctor", no);
    assert(!called, "constructor should not have been called");
  }
  test_is_constructor();
});

define("~/methods/equal/RegExp", [], () =>
  function RegExp_equals(a, b) {
    return a.source === b.source && a.flags === b.flags;
  });

define("~/methods/map/object", [], () => {
  const { create, keys } = Object;
  function map_object(input, f, type = Object) {
    const output = create(type);
    for (const key of keys(input)) {
      output[key] = f(input[key], key, input);
    }
    return output;
  }
  map_object.comment = `Map a function over an object's values into a new plain object.`;
  return map_object;
});

define("~/def/ranges", [
  "~/methods/map/object",
  "~/methods/equal/RegExp",
], function (map_object, RegExp_equals) {
  const { mint } = def;
  const { NotImplemented } = def.errors;
  const { is, assign, freeze, keys, entries, hasOwn, getPrototypeOf } = Object;

  const $kind = Symbol("def.ranges.meta.kind");
  const $clauses = Symbol("def.ranges.meta.clauses");
  const $typeof = Symbol("def.ranges.meta.$typeof"); // for JS types only... but see below

  // Classes for the quasi-normal form of constraints
  /**
   * Represents the minimum Constraint.  The one you can't get underneath.
   *
   * `any` is the top type in the _algebra_: anything matches it.
   *
   * But `AnyConstraint` is not the top of the _class_ hierarchy.
   */
  class AnyConstraint {}
  assign(AnyConstraint.prototype, {
    [$kind]: "https://def.codes/ns/constraints/AnyConstraint",
    [$clauses]: {},
  });

  // This is a tuple...
  // the forking path
  // unlabeled alternatives
  // the only core means of composition (others are provided via clauses)
  class AnyOfConstraint {
    constructor(...alternatives) {
      // this.alternatives = alternatives.map(def.constraints.read);
      this.alternatives = alternatives;
      freeze(this);
    }
  }
  assign(AnyOfConstraint.prototype, {
    [$kind]: "https://def.codes/ns/constraints/AnyOfConstraint",
    [$clauses]: {},
  });

  // Look, there's no good way to say this
  class NullConstraint {}
  assign(NullConstraint.prototype, {
    [$kind]: "https://def.codes/ns/constraints/NullConstraint",
    [$clauses]: {},
  });

  // abstract
  class JavaScriptConstraint {
    constructor(clauses) {
      if (clauses) assign(this, clauses);
      freeze(this);
    }
  }

  // has no subdomains
  class UndefinedConstraint extends JavaScriptConstraint {}
  assign(UndefinedConstraint.prototype, { [$typeof]: "undefined" });

  class BooleanConstraint extends JavaScriptConstraint {
    constructor(clauses) {
      super(clauses);
    }
  }
  assign(BooleanConstraint.prototype, { [$typeof]: "boolean", [$clauses]: {} });

  class NumberConstraint extends JavaScriptConstraint {
    constructor(clauses) {
      super(clauses);
    }
  }
  assign(NumberConstraint.prototype, { [$typeof]: "number", [$clauses]: {} });

  class BigIntConstraint extends JavaScriptConstraint {
    constructor(clauses) {
      super(clauses);
    }
  }
  assign(BigIntConstraint.prototype, { [$typeof]: "bigint", [$clauses]: {} });

  class StringConstraint extends JavaScriptConstraint {
    constructor(clauses) {
      super(clauses);
    }
  }
  assign(StringConstraint.prototype, { [$typeof]: "string", [$clauses]: {} });

  class SymbolConstraint extends JavaScriptConstraint {
    constructor(clauses) {
      super(clauses);
    }
  }
  assign(SymbolConstraint.prototype, { [$typeof]: "symbol", [$clauses]: {} });

  class ObjectConstraint extends JavaScriptConstraint {
    constructor(clauses) {
      super(clauses);
    }
  }
  assign(ObjectConstraint.prototype, { [$typeof]: "object", [$clauses]: {} });

  class FunctionConstraint extends JavaScriptConstraint {
    constructor(clauses) {
      super(clauses);
    }
  }
  assign(FunctionConstraint.prototype, {
    [$typeof]: "function",
    [$clauses]: {},
  });

  // Provide discoverable notation equivalent to above
  const $fluent = Symbol();

  const make_fluent_for = (js_type, bindings) => {
    const constructor = JS_CONSTRUCTORS[js_type];
    const clauses_metadata = constructor.prototype[$clauses];

    // The constraint represented by this interface
    // BUT we need to `read` this, right? i.e. normalize the clauses
    const constraint = new constructor(bindings);

    const obj = { [$fluent]: constraint };
    for (const [name, clause_spec] of entries(clauses_metadata)) {
      // Arg should be the type expected by this clause
      // But not necessarily normalized
      obj[name] = function (arg) {
        return make_fluent_for(js_type, { ...bindings, [name]: arg });
      };
    }
    return obj;
  };

  // Meant as a singleton.  Eagerly creates the sub-interfaces.
  class Fluent {
    [$fluent] = "thing";
    undefined = make_fluent_for("undefined");
    boolean = make_fluent_for("boolean");
    number = make_fluent_for("number");
    bigint = make_fluent_for("bigint");
    string = make_fluent_for("string");
    symbol = make_fluent_for("symbol");
    object = make_fluent_for("object");
    function = make_fluent_for("function");
  }

  const JS_CONSTRUCTORS = freeze({
    undefined: UndefinedConstraint,
    boolean: BooleanConstraint,
    number: NumberConstraint,
    bigint: BigIntConstraint,
    string: StringConstraint,
    symbol: SymbolConstraint,
    object: ObjectConstraint,
    function: FunctionConstraint,
  });

  const MAKE = freeze({
    ...JS_CONSTRUCTORS,
    any: AnyConstraint,
    null: NullConstraint,
    // never: AnyOfConstraint,
  });

  // top types with no clauses
  const TOP = map_object(MAKE, construct => new construct());

  const UNDEFINED = TOP.undefined;
  const NULL = TOP.null;
  const ANY = TOP.any;

  const PRIMITIVES_BY_CONSTRUCTOR = new Map([
    [Boolean, TOP.boolean],
    [Number, TOP.number],
    [BigInt, TOP.bigint],
    [String, TOP.string],
    [Symbol, TOP.symbol],
  ]);

  const { construct } = Reflect;
  function is_constructor(f) {
    try {
      construct(String, [], f);
    } catch (e) {
      return false;
    }
    return true;
  }

  /**
   * Map shorthand notation into a tagged tree describing a value range.
   *
   * @returns an instance of one of the `Constraint` classes.
   */
  function read(expr) {
    if (
      expr instanceof AnyConstraint ||
      expr instanceof NullConstraint ||
      expr instanceof AnyOfConstraint ||
      expr instanceof JavaScriptConstraint
    )
      return expr;

    if (expr === null) return NULL;
    if (expr === undefined) return UNDEFINED;

    const expr_type = typeof expr;
    switch (expr_type) {
      case "function":
        const primitive = PRIMITIVES_BY_CONSTRUCTOR.get(expr);
        if (primitive !== undefined) return primitive;

        if (is_constructor(expr))
          return new ObjectConstraint({ instanceof: expr });

        // We could treat these as predicates (opaque boolean maps)
        // my main misgiving is about the classification heuristic
        throw NotImplemented(`non-constructor function`);

      case "undefined":
      case "boolean":
      case "number":
      case "bigint":
      case "symbol":
      case "string":
        return new MAKE[expr_type]({ value: expr });
    }

    console.assert(
      expr_type === "object",
      `Assert fail: unknown typeof ${expr_type}`
    );

    // Could handle Fluent the same way...
    if (expr instanceof Fluent) return ANY;
    if ($fluent in expr) return expr[$fluent];

    if ("anyOf" in expr) {
      if (!Array.isArray(expr.anyOf))
        throw Error(`Invalid constraint: expected array anyOf`);
      return new AnyOfConstraint(...expr.anyOf.map(read));
    }

    // Infer top type based on constant
    if (!("any" in expr)) {
      if (keys(expr).length === 0) return ANY;
      throw Error("Invalid constraint: must have `any` or `anyOf` or be empty");
    }

    if (expr.any === "thing") return ANY;

    if (typeof expr.any === "function") {
      if (is_constructor(expr.any)) {
        return new MAKE.object({ instanceof: expr.any });
      }
      throw NotImplemented(`non-constructor functions`);
    }

    const { any, ...clauses } = expr;

    if (!(any in MAKE)) {
      throw Error(`BadRequest: unrecognized ‘any’ value: ${any}`);
    }

    // Normalize clauses
    const normalized = {};

    const constructor = MAKE[any];
    const clause_schema = constructor.prototype[$clauses];

    const _keys = keys(clauses);
    if (_keys.length === 0) return TOP[any];
    // Optimization: return singletons for top type
    for (const key of _keys) {
      const clause_value = clauses[key];
      const clause_type = clause_schema[key];
      if (clause_type != null && "read" in clause_type) {
        const read_clause = clause_type.read;
        if (typeof read_clause !== "function")
          throw RangeError(`Bad clause metadata for read ${key}`);
        normalized[key] = read_clause(clause_value);
      } else {
        // either no clause type is defined, or there's no special read
        // so just keep the thing as-is
        normalized[key] = clause_value;
      }
    }

    return new constructor(normalized);
  }

  // should be equivalent to generic equality (or made so)
  // BUT generics depend on constraints...
  // do they really, though?  I mean, do they depend on
  // constraint equality?

  // Tell whether two constraints have the same form.
  const equal = (a, b) => {
    if (is(a, b)) return true;

    if (a instanceof AnyConstraint) return b instanceof AnyConstraint;
    if (b instanceof AnyConstraint) return false;

    if (a instanceof AnyOfConstraint) {
      if (!(b instanceof AnyOfConstraint)) return false;
      const a_alts = a.alternatives;
      const b_alts = b.alternatives;
      const a_alts_length = a_alts.length;
      if (a_alts_length !== b_alts.length) return false;
      for (let i = 0; i < a_alts_length; i++)
        if (!equal(a_alts[i], b_alts[i])) return false;
      return true;
    }

    // but these could look up into inverse of JS_CONSTRUCTORS
    const a_type = a[$typeof];
    if (a_type !== b[$typeof]) return false;

    // Clause metadata is on the prototype
    const clause_schema = a[$clauses];

    // check the clauses
    const a_keys = Object.keys(a);
    const b_keys = Object.keys(b);
    if (a_keys.length !== b_keys.length) return false;
    for (const key of a_keys) if (!b_keys.includes(key)) return false;
    for (const key of a_keys) {
      const a_clause = a[key];
      const b_clause = b[key];
      // TODO: first check a_clause===b_clause (or `is`)
      // if true, you can return true
      // see `subsumes`
      const clause_type = clause_schema[key];
      if (clause_type != null) {
        if ("equals" in clause_type) {
          const clause_equals = clause_type.equals;
          if (typeof clause_equals !== "function")
            throw RangeError(`Bad clause metadata for equals`);
          return clause_equals(a_clause, b_clause);
        }
        // the clause type is defined, but there's no comparator --- fail?
      }
      // either no clause type is defined, or there's no comparator
      // so use built-in equality
      if (!is(a_clause, b_clause)) return false;
    }

    return true;
  };

  // Tell whether two constraint expressions are equivalent (equal after read).
  const equivalent = (a, b) => equal(read(a), read(b));

  const constraints_object_keys = constraint => {};

  const object_has_keys = (it, keys) => {
    for (const key of keys) if (!(key in it)) return false;
    return true;
  };

  // Metadata for unary constraint clauses, used by
  // - matching operations
  // - specialized ops for the term algebra (equality &c)
  // - making API discoverable via inspection (e.g. for REPL's)
  // - probably also I/O

  // these are methods
  const CLAUSE_TYPES = {
    undefined: {},
    boolean: {
      where: { satisfies: (x, p) => p(x) },
      value: { satisfies: is },
    },
    number: {
      where: { satisfies: (x, p) => p(x) },
      value: { satisfies: is },
      lt: { satisfies: (n, lt) => n < lt, linear: true },
      lte: { satisfies: (n, lte) => n <= lte, linear: true },
      gt: { satisfies: (n, gt) => n > gt, linear: true },
      gte: { satisfies: (n, gte) => n >= gte, linear: true },
    },
    bigint: {
      where: { satisfies: (x, p) => p(x) },
      value: { satisfies: is },
      lt: { satisfies: (n, lt) => n < lt, linear: true },
      lte: { satisfies: (n, lte) => n <= lte, linear: true },
      gt: { satisfies: (n, gt) => n > gt, linear: true },
      gte: { satisfies: (n, gte) => n >= gte, linear: true },
    },
    string: {
      where: { satisfies: (x, p) => p(x) },
      value: { satisfies: is },
      starting: { satisfies: (s, x) => s.startsWith(x), linear: true },
      ending: { satisfies: (s, x) => s.endsWith(x), linear: true },
      including: { satisfies: (s, x) => s.includes(x), linear: true },
      matching: { satisfies: (s, x) => x.test(s), equals: RegExp_equals },
      before: { satisfies: (s, x) => s < x, linear: true },
      after: { satisfies: (s, x) => s > x, linear: true },
    },
    symbol: {
      where: { satisfies: (x, p) => p(x) },
      token: { satisfies: (s, t) => Symbol.keyFor(s) === t },
    },
    object: {
      where: { satisfies: (x, p) => p(x) },
      instanceof: {
        satisfies: (o, x) => o instanceof x,
        subsumes(a, b) {
          // return b.prototype instanceof a.prototype;
          return a.prototype.isPrototypeOf(b.prototype);
        },
      },
      like: {
        satisfies: (o, like) => {
          if (!is_plain_object(o)) return false;
          // for now, if you want to constrain on symbol properties, use `where`
          for (const [name, rule] of entries(like)) {
            if (!hasOwn(o, name) || !satisfies(o[name], rule)) return false;
          }
          return true;
        },

        equals(a, b) {
          if (
            a == null ||
            b == null ||
            typeof a !== "object" ||
            typeof b !== "object"
          )
            throw RangeError(`‘like’ value must be an object`);

          const a_keys = keys(a);
          if (a_keys.length !== keys(b).length) return false;
          // You might expect this to be `equals` instead of `equivalent`
          // But we don't know that *these* values will have been normalized.
          for (const key of a_keys)
            if (!equivalent(a[key], b[key])) return false;

          return true;
        },
        read(arg) {
          if (arg == null || typeof arg !== "object")
            throw RangeError(`‘like’ value must be an object`);

          // return map_object(arg, def.constraints.read), right?

          const norm = {};
          for (const key of keys(arg)) norm[key] = read(arg[key]);

          return norm;
        },
      },
    },
    function: {
      where: { satisfies: (x, p) => p(x) },
      accepts: {
        satisfies(fun, parameters) {
          throw NotImplemented("function accepts satisfies");
        },
        equals(a, b) {
          throw NotImplemented("function accepts equals");
        },
        // See note re importing `satisfies`.  The same thing applies
        read: parameters => parameters.map(read),
      },
      returns: {
        satisfies(fun, constraint) {
          // we could look at claims about the function
          throw NotImplemented("function returns satisfies");
        },
        // Hmm, we get these as notation so we need to normalize every time?
        // that's unfortunate...
        equals(a, b) {
          return equivalent(a, b);
        },
        read(n) {
          return read(n);
        },
      },
    },
  };

  const Object_prototype = Object.prototype;

  const is_plain_object = x => {
    if (x === null) return false;
    const p = getPrototypeOf(x);
    return p === null || p === Object_prototype;
  };

  {
    const TYPEOFS =
      `undefined boolean number bigint string symbol object function`.split(
        ` `
      );

    // ASSUMES a static set of clauses at load time.
    const make_special = t => {
      const predicates = entries(CLAUSE_TYPES[t]).map(([k, v]) => {
        const { satisfies } = v;
        return (it, constraint) => {
          if (k in constraint) return satisfies(it, constraint[k]);
          return true;
        };
      });

      return (it, constraint) => {
        for (const p of predicates) if (!p(it, constraint)) return false;
        return true;
      };
    };

    const special = {};
    for (const type of TYPEOFS) special[type] = make_special(type);

    function satisfies_normal(it, constraint) {
      if (constraint instanceof AnyConstraint) return true;
      if (constraint instanceof NullConstraint) return it === null;
      // wait what? couldn't null satisfy an AnyOf constraint?
      // move this below next block
      if (it === null) return false;

      if (constraint instanceof AnyOfConstraint) {
        for (const alternative of constraint.alternatives) {
          if (satisfies_normal(it, alternative)) return true;
        }
        return false;
      }

      // see note above, these could look up in well-known map
      const type = constraint[$typeof];
      if (!is(type, typeof it)) return false;
      const specially_satisfies = special[type];
      return specially_satisfies(it, constraint);
    }

    function satisfies(it, constraint) {
      return satisfies_normal(it, read(constraint));
    }
    satisfies.comment = `Answer whether a given thing is in the domain described by a constraint.`;
  }

  {
    const isa = (x, type) => x instanceof type;

    // Same as `subsumes`, but expects arguments in constructed form.
    // BUT... subsumption is way too strong to be practical
    // all you *really* need is deterministic, and “as total as possible”
    function subsumes_normal(a, b) {
      const kind = a[$kind];

      if (isa(a, AnyConstraint)) return true; // `any` subsumes everything
      if (isa(b, AnyOfConstraint)) throw NotImplemented("x-subsumes-union");

      // If `b` is not a union...
      // If any member of `a` subsumes `b`, then `a` subsumes b.
      if (isa(a, AnyOfConstraint)) {
        const ua = a.alternatives;
        // THIS can be `subsumes_normal`, right?
        for (const x of ua) if (subsumes(x, b)) return true;
        return false;
      }

      const b_kind = b[$kind];
      if (kind !== b_kind) return false;

      // === Same-kind

      // Singletons cannot have (material) clauses.  These would be covered by the
      // JS constraint logic below, but we avoid the work of even considering clauses.
      if (isa(a, NullConstraint)) return isa(b, NullConstraint);
      if (isa(a, UndefinedConstraint)) return isa(b, UndefinedConstraint);

      console.assert(isa(a, JavaScriptConstraint));

      // Now we have two JavaScript constraints

      // `[kind]` is not defined across the JS types, or else we could skip this...
      // (and could return `true` for the above nullish cases)
      if (a[$typeof] !== b[$typeof]) return false;

      console.assert(
        isa(a, JavaScriptConstraint),
        a,
        "unknown constraint type"
      );

      const a_keys = keys(a);
      const b_keys = keys(b);

      // optimization: short-circuits for special cases
      // if (a_keys.length === 0) if (b_keys.length === 0) return true;

      const clauses = a[$clauses];
      console.assert(clauses, "Constraint should have clause metadata");

      // proposal: ‘where’ beats all other clauses
      // If you could have said it another way, you would have.  you resorted to the escape hatch, abandoning declaration... the last thing you want is more escape hatch (user preferential ordering)
      // it's no longer subsumption... just an ordering
      // can think of b as more specific in this case
      //
      // Ah, but this means that Iterable beats tuple
      // it's not an ordering between the *methods* so much as between the types
      // I don't know that iterable shouldn't be *less* specific than array
      // if ("where" in b && !("where" in a)) return true;
      // if ("where" in a && !("where" in b)) return false;

      // Make sure that `b` is compatible with all of the clauses in `a`.

      // Ignore keys in `b` that are not in `a`; they can only constrain `b` further.

      // One-way switch saying whether “I don't know” will be the maximal answer.
      // Even after it is set, we keep searching in case of an outright `false`.
      let I_dont_know = false;

      for (const key of a_keys) {
        if (!(key in clauses)) continue;
        // console.assert(key in clauses)

        // If `a` has a clause that `b` doesn't have, then it probably doesn't
        // subsume `b`.
        if (!(key in b)) {
          // Except that if `b` has no clauses then nothing could answer for this one.
          if (b_keys.length === 0) return false;

          // SPECIAL CASE: value absorption
          //
          // `value` is a clause recognized on all types (at least all value
          // types).  `value` is of special interest here as it can answer for
          // any other clause, positively or negatively.
          if ("value" in b) return clauses[key].satisfies(b.value, a[key]);

          return; // but okay, we don't know
        }

        const clause = clauses[key];
        console.assert(clause, key, "Expected clause metadata");

        // can subsumption between constraints be
        // defined merely as satisfaction of the constraint?

        // if the value is acceptable, then we know that b's value lies within `a`'s domain.

        // if the value is *not* acceptable, then we know that `b` admits at least
        // one value that `a` rejects.

        // Note that `gt` is not reflexive (indeed it's anti-reflexive)
        //
        // We want the clause `gt(N)` to subsume `gt(N)` for all values of N,
        // but this will fail because it's marked "linear".

        const ax = a[key];
        const bx = b[key];
        if (ax === bx || clause.equals?.(ax, bx)) continue;
        if (clause.subsumes) {
          const e = clause.subsumes(ax, bx);
          if (e === true) continue; // you don't have to consider the other stuff now
          if (e === false) return false;
          if (e === undefined) I_dont_know = true;
          continue;
        }
        if (clause.linear) {
          // this trick requires `satisfies` to be reflexive
          // note the arguments are flipped here
          const e = clause.satisfies(bx, ax);
          if (e === true) continue;
          if (e === false) return false;
          if (e === undefined) I_dont_know = true; // shouldn't happen for `satisfies`
          continue;
        }
        I_dont_know = true;
      }

      if (!I_dont_know) return true;
    }

    /**
     * Answers, as best it can, whether the constraint `a` subsumes the constraint
     * `b`; that is, whether all values satisfying `b` also satisfy `a`.
     *
     * The subsumption rules are:
     *
     * - `any` subsumes everything
     * - distributes over unions (`anyOf`)
     *   - more or less... some of union handling is still TBD
     * - for non-unions, first match top type
     * - then determined by comparing clauses:
     *   - a clause in `a` and not in `b` means that `a` does not subsume `b`
     *   - a clause in `b` that is not in `a` has no effect
     *   - a clause in both:
     *     - if the values for `a` and `b` are `equal`, the clause succeeds
     *     - if the clause is `linear`, then treat `satisfies` as `subsumes`
     *     - if the clause has a `subsumes` function:
     *       - if it says no, the answer is no for the whole constraint
     *       - if it says unknown, then the answer for the clause is unknown
     *
     * @returns `true` if `a` definitely subsumes `b`, `false` if it definitely does
     *   not, and `undefined` if the constraints are not (known to be) comparable.
     */
    function subsumes(a, b) {
      return subsumes_normal(read(a), read(b));
    }
  }

  // “Register” the clause types on the constructor prototypes.
  const _keys = Object.keys(JS_CONSTRUCTORS);
  for (const type of _keys) {
    const klass = JS_CONSTRUCTORS[type];
    const clause_types = CLAUSE_TYPES[type];
    Object.assign(klass.prototype, { [$clauses]: clause_types });
  }

  const any = new Fluent();

  return { any, read, equal, satisfies, satisfies_normal, subsumes };
});

define("~/def/generics", ["~/def/ranges"], ranges => {
  const { equal, read, subsumes, satisfies_normal } = ranges;

  const { assign, is } = Object;
  const TAG = Symbol.for("http://def.codes/ns/core/GenericFunctionInterface");

  function specificity_ordering(a, b) {
    if (equal(a, b)) return 0;
    // prettier-ignore
    switch (subsumes(a, b)) {
    case true: return -1; // sort `a` before `b`, because it's less specific
    case false: return 1; // sort `a` after `b`, because it's more specific
    case undefined: break;
    default: throw Error(`AssertFail`);
  }

    // make a test case for this though
    // if (subsumes(b, a)) return 1

    // If `a` and `b` are incomparable by subsumption
    // then use clause count as next metric
    const a_size = Object.keys(a).length;
    const b_size = Object.keys(b).length;
    if (a_size > b_size) return 1;
    if (b_size > a_size) return -1;

    return 0; // not equal, but equivalent under this ordering
  }

  {
    const { min } = Math;
    function lex_comparator(compare) {
      return (a, b) => {
        let i = 0;
        const len = min(a.length, b.length);
        while (i < len) {
          const res = compare(a[i], b[i]);
          if (res !== 0) return res;
          i++;
        }
        return 0;
      };
    }
  }

  const COMPARE = lex_comparator(specificity_ordering);

  // 2N compares to get top class...
  // we could remember the ones that bumped the max and review only those on 2nd pass
  function* maxes(items, compare) {
    if (items.length === 0) return;
    let [max] = items;
    for (const item of items) if (compare(item, max) > 0) max = item;
    for (const item of items) if (compare(item, max) === 0) yield item;
  }

  const { BadRequest, MultipleChoices, NotImplemented } = def.errors;
  const make_generic = (...args) => {
    if (args.length !== 1) throw Error(`IllegalArity ${args.length}`);
    const [options] = args;
    if (options === null || typeof options !== "object")
      throw BadRequest(`‘options’ must be an object`);
    if (typeof options.name !== "string")
      throw BadRequest(`options.name must be a string`);
    if (
      !Array.isArray(options.signatures) ||
      !options.signatures.every(x => typeof x === "function")
    )
      throw BadRequest(`options.signatures must be an array of functions`);
    // any.object.like({name: String, signature: {any: Array, where }})

    // compare with bit flags (since N is small)
    const legal_arities = new Set();
    const is_legal_arity = n => legal_arities.has(n);

    if (typeof options === "number") {
      if (options < 1) throw Error(`IllegalArity ${options}`);
      legal_arities.add(options);
    } else if (typeof options === "object") {
      if (!Array.isArray(options?.signatures)) {
        throw BadRequest(`'signatures' must be an array`);
      }
      const { signatures } = options;
      for (const signature of signatures)
        if (typeof signature !== "function")
          throw BadRequest(`signature must be a function`);

      for (const signature of signatures) {
        const arity = signature.length;
        if (legal_arities.has(arity))
          throw BadRequest(`Multiple functions with arity ${arity}`);
        legal_arities.add(arity);
      }
    }

    // number → Map<string → MethodInternal>
    const arities = new Map();
    let id = 0;

    function* applicable_methods(args) {
      const arity = args.length;
      if (!is_legal_arity(arity)) throw Error("IllegalArity");
      const methods = arities.get(arity);
      if (!methods) return;
      for (const method of methods.values()) {
        const { parameters } = method;
        if (
          args.every((arg, index) => {
            const constraint = parameters[index];
            return !constraint || satisfies_normal(arg, constraint);
          })
        ) {
          yield method;
        }
      }
    }

    const dispatch = (...args) => {
      const methods = [...applicable_methods(args)];
      const { length } = methods;
      if (length === 1) return methods[0].function.apply(target, args);
      if (length > 1) {
        // conflict resolution
        const tops = [
          ...maxes(
            methods.map(m => m.parameters),
            COMPARE
          ),
        ];
        if (tops.length > 1)
          // It would be good to list them here as JSON
          // the above doesn't capture the id's though
          throw MultipleChoices(`${tops.length}`);
        const [params] = tops;
        // HACK also due to the lack of id's from above
        const method = methods.find(m => m.parameters === params);
        return method.function.apply(target, args);
      }
      const arity = args.length;
      const { name } = options;
      console.debug(`Not implemented`, name, args);
      console.table(arities.get(arity));
      const description = args.map(x => typeof x);
      throw NotImplemented(
        `${name} has no method for the given ${description}`
      );
    };

    // NOTE: this has a very different meaning than the closest thing in CLOS
    // `call-next-method`
    const target = { recur: dispatch };

    const signatures = Array.from(legal_arities, n => ({ arity: n }));

    function define(...args) {
      if (args.length !== 3) throw Error("IllegalArity");
      const [name, parameters, fun] = args;

      const arity = parameters.length;
      if (!is_legal_arity(arity)) throw BadRequest(`IllegalArity ${arity}`);
      let methods = arities.get(arity);
      if (methods === undefined) arities.set(arity, (methods = new Map()));
      const normalized = parameters.map(read);
      const method = { parameters: normalized, function: fun };

      methods.set(name, method);
      return name;
    }
    // if you don't want public write on the methods, just lop them off
    return assign(dispatch, {
      methods: {
        define,
        add(...args) {
          if (args.length !== 2) throw Error("IllegalArity");
          const [parameters, fun] = args;

          if (!Array.isArray(parameters)) throw Error(`IllegalArgument`);

          const method_id = (++id).toString();
          return define(method_id, parameters, fun);
        },
        remove(id) {
          for (const methods of arities.values()) methods.delete(id);
        },
      },
    });
  };
  make_generic.comment = `A generic function dispatches dynamically based on arguments and methods supplied by the open world.`;

  return { make_generic };
});

define("~/def/generics/test", [
  "~/def/ranges",
  "~/def/generics",
  "~/def/equal",
], (ranges, generics, equal) => {
  const { any } = ranges;
  const { make_generic } = generics;
  const { assert } = console;
  const { MAX_SAFE_INTEGER } = Number;
  {
    const jiggle = make_generic({ name: "jiggle", signatures: [a => any] });
    jiggle.comment = `Another thing like this, but different`;
    jiggle.methods.add([String], s => s.toUpperCase());
    jiggle.methods.add([Number], n => n + 1);
    jiggle.methods.add(
      [{ any: Number, gte: MAX_SAFE_INTEGER }],
      n => n % MAX_SAFE_INTEGER
    );
    jiggle.methods.add([Boolean], b => !b);
    jiggle.methods.add([Array], a => [...a, 1]);
    // `where` is not getting picked up on record notation {any: Array, where: ...}
    jiggle.methods.add(
      [any.object.instanceof(Array).where(a => a.length > 10)],
      a => a.slice(0, 10)
    );

    assert(jiggle("Hello") === "HELLO");
    assert(jiggle(true) === false);
    assert(jiggle(3) === 4);
    assert(equal(jiggle([]), [1]));
  }
  {
    const plainer = make_generic({ name: "plainer", signatures: [a => any] });
    plainer.methods.add([Object], x => x);
    assert(equal(plainer(/^abc$/), {}));
    plainer.methods.add([RegExp], regex => ({
      pattern: regex.source,
      flags: regex.flags,
    }));
    assert(equal(plainer(/^abc$/), { pattern: "^abc$", flags: "" }));
    assert(equal(plainer({ too: true }), { too: true }));
  }
});

define("~/def/multiply", ["~/def/generics"], generics => {
  const { make_generic } = generics;
  const multiply = make_generic({
    name: "multiply",
    signatures: [function (a, b) {}],
  });
  multiply.methods.add([Number, Number], (a, b) => a * b);
  multiply.methods.add([BigInt, BigInt], (a, b) => a * b);
  // add conversions if you date
  multiply.comment = `Generic interpretation of multiply.  a & b SHOULD have same domain`;
  return multiply;
});

define("~/explore/units", ["~/def/multiply"], multiply => {
  // see qudt et al
  const my_vocab = def.mint.in("http://example.com/units/").names;
  const { Bale, Cubit, Span, Spell } = my_vocab;
  // there is a kind of thing called units
  // a multiplication of two units is also a unit
  // but that isn't a method, that has to do with who's looking...
  // multiply.methods.add()
});

define("~/vocab/si-units", [], () => {
  // stuff about actual units and dimensions, this may be loaded from an outside file
});

define("~/explore/dimensional-values", ["~/explore/units", "~/def/multiply"], (
  generics,
  multiply
) => {
  // i.e. values with units
  const { mint } = def;
  // just creating a term to represent the combined units, right?

  multiply.methods.add([unit, unit], (a, b) => {
    return Multiply(unit, unit);
  });
  multiply.methods.add([value_with_units, value_with_units], () => {
    // 1in x
  });
  // this is a separate method... as such it is not really a “rule”
  // since it could be preempted by another method (under CLOS dispatching)
  //
  // simplify: if b is a Divide(X, a) then you can just answer X, and vice versa

  // and so how do we define DimensionalValue?
  // it's a constraint, not necessarily a class
});

define("~/def/equal", ["~/def/generics"], generics => {
  const { make_generic } = generics;
  const equal = make_generic({
    name: "equal",
    signatures: [(a, b) => Boolean],
  });
  const { is, hasOwn, keys } = Object;
  equal.methods.add([{ any: "thing" }, { any: "thing" }], is);
  for (const T of [null, undefined, Boolean, Number, BigInt, String, Symbol])
    equal.methods.add([T, T], is);

  equal.methods.add([Array, Array], function Array_equals(a, b) {
    const len = a.length;
    if (b.length !== len) return false;
    for (let i = 0; i < len; i++) if (!equal(a[i], b[i])) return false;
    return true;
  });

  equal.methods.add([Object, Object], function Object_equals(a, b) {
    const a_keys = keys(a);
    if (a_keys.length !== keys(b).length) return false;
    for (const k of a_keys)
      if (!hasOwn(b, k) || !equal(a[k], b[k])) return false;
    return true;
  });

  equal.methods.add([RegExp, RegExp], function RegExp_equals(a, b) {
    return a.source === b.source && a.flags === b.flags;
  });

  equal.comment = `Answer whether the arguments represent the same value`;
  return equal;
});

define("~/def/equal/test", ["~/def/equal"], equal => {
  const { assert } = console;
  for (const t of [null, undefined, false, 0, 1n, "a", Symbol()]) {
    assert(equal(t, t), t, "=", t);
  }
});

define("~/world/view/tree", [], () => {
  // what can we meaningfully assert short of “everything is always in its place”?
  function make_tree_view() {
    const dlq = [];
    async function* experiment(stream) {
      while (!stream.done()) {
        try {
          const value = await stream.read();
          // adding to collection
          // removing from collection
          // but define is monotonic
          process(value);
        } catch (error) {}
        // any signal values here?
      }
    }
  }
});

define("~/explore/tree-view", [], () => {
  // hierarchical view: express some tree traversal as dom
  // what is a good way to express a tree as a sequence, anyway?
  // you need stateful nodes and signal markers
  // oh also, how *do* we listen to defines?
  // I guess the point re notation is, why do procedural/recursive representation of things?
  // because it's so damn efficient and kinda easy to implement
  // and it's good for small things
});

define("~/explore/css", [], () => {
  const { css } = def.lang;
  function css_read() {}
  function css_write(css_term) {}
  css.AttributeSelector({ attribute, value });
});

define("~/js/regexp/escape", [], () => {
  // https://github.com/tc39/proposal-regex-escaping/blob/main/polyfill.js
  // seeAlso https://github.com/tc39/proposal-regex-escaping/issues/37
  // This is safe for whole patterns.  The objections are to certain joins.
  const RegExp_escape = s => String(s).replace(/[\\^$*+?.()|[\]{}]/g, "\\$&");
  RegExp_escape.comment = `Answer with a RegExp pattern treating all input characters as literals`;
  return RegExp_escape;
});

define("~/vocab/web", [], () => {
  const { mint } = def;
  const prefixes = {
    // this is for elements, not attributes, though HTML does define some attributes
    html: "https://developer.mozilla.org/en-US/docs/Web/HTML/Element/",
    css: "https://developer.mozilla.org/en-US/docs/Web/CSS/",

    // https://developer.mozilla.org/en-US/docs/Web/API/HTMLTimeElement/dateTime
    // https://developer.mozilla.org/en-US/docs/Web/HTML/Element/time#datetime
  };
  const html = mint.in(prefixes.html);
});

define("~/methods/label", ["rdfs:label"], label => {
  label.methods.add([]);
});

define("~/def/io", ["~/def/ranges", "~/def/generics"], (ranges, generics) => {
  const { any } = ranges;
  const { make_generic } = generics;

  // syntax is a type, and you're expected to return that type from the method.
  // syntax is a "class" (instanceof target), NOT an arbitrary constraint

  const write = make_generic({
    name: "write",
    signatures: [(thing, syntax) => any],
  });
  const read = make_generic({
    name: "read",
    signatures: [(thing, syntax) => any],
  });

  return { read, write };
});

define("~/def/map/methods", ["~/def/map"], map => {
  // FOR EFFECTS!
  // can we say that a mapped T will make sense as a T?
  map.methods.add([Array], (a, f) => a.map(f));

  if (is_plain_object(thing)) return map_object(thing, f);
});

define("~/def/io/methods", ["~/def/io"], io => {
  // THIS IS FOR EFFECTS THO!
  // the effect of registering the methods
  const { read, write } = io;
  const { keys } = Object;
  write.methods.add([null, "Element"], s => {
    const span = document.createElement("span");
    span.setAttribute("data-typeof", "null"); // what would be qualified name?
    span.append("∅");
    return span;
  });
  write.methods.add([undefined, "Element"], s => {
    const span = document.createElement("span");
    span.setAttribute("data-typeof", "undefined"); // what would be qualified name?
    span.append("⊥");
    return span;
  });
  for (const type of ["boolean", "number", "bigint", "string"])
    write.methods.add([{ any: type }, "Element"], s => {
      const span = document.createElement("span");
      span.setAttribute("data-typeof", type);
      span.append(s);
      return span;
    });
  write.methods.add(
    // Note `Function` prototype doesn't work here
    [{ any: "function" }, "Element"],
    function Function_to_Element(fun) {
      const details = document.createElement("details");
      const summary = document.createElement("summary");
      const pre = document.createElement("pre");
      summary.append(`a JavaScript function`);
      pre.append(`${fun}`);
      details.append(summary, pre);
      return details;
    }
  );
  write.methods.add([Array, "Element"], function (array) {
    const ol = document.createElement("ol");
    for (const element of array) {
      const li = document.createElement("li");
      // This could be Element or Node
      const definition = this.recur(element, "Element");
      if (definition !== undefined) li.append(definition);
      ol.append(li);
    }
    return ol;
  });
  // iterable iterator...
  const { iterator } = Symbol;
  // There's an ordering problem with this
  /*
  write.methods.add(
    [
      // not this
      // { any: "object", like: { [Symbol.iterator]: { any: "function" } } },
      // but this:
      { any: "object", where: it => typeof it[iterator] === "function" },
      "Element",
    ],
    function (sequence) {
      const container = document.createElement("div");
      for (const item of sequence) container.append(write(item, "Element"));
      return container;
    }
  );
*/

  // thing that has entries
  write.methods.add([Map, "Element"], function (it) {
    const dl = document.createElement("dl");
    dl.classList.add("flex");
    for (const [key, value] of it.entries()) {
      const div = document.createElement("div");
      const dt = document.createElement("dt");
      const dd = document.createElement("dd");
      dt.append(key);
      const definition = this.recur(value, "Element");
      if (definition !== undefined) dd.append(definition);
      div.append(dt, dd);
      dl.append(div);
    }
    return dl;
  });
  write.methods.add([Object, "Element"], function (it) {
    const dl = document.createElement("dl");
    for (const key of keys(it)) {
      const div = document.createElement("div");
      const dt = document.createElement("dt");
      const dd = document.createElement("dd");
      dt.append(key);
      // what if recur fails? is this your problem?
      // shouldn't we be counting depth, passing path?
      const definition = this.recur(it[key], "Element");
      if (definition !== undefined) dd.append(definition);
      div.append(dt, dd);
      dl.append(div);
    }
    return dl;
  });

  write.methods.add(
    [def.Tuple, "Element"],
    function def_Tuple_to_Element(term) {
      // and we “know” that Array will be handled as a less specific thing...
      const ele = document.createElement("div");
      ele.append("A tuple... but is it tagged?");
      return ele;
    }
  );

  write.methods.add(
    [def.amd.quote.define, "Element"],
    function AMD_define_to_Element(term) {
      // and we “know” that Tuple will be handled as a less specific thing...
      const ele = document.createElement("details");
      const summary = document.createElement("summary");
      summary.append(`An AMD definition`);
      ele.append(summary);
      const tag = term[Symbol.toStringTag];
      if (tag) ele.setAttribute("typeof", tag);
      switch (term.length) {
        case 2: {
          const [name, definition] = term;
          try {
            write(definition, "Element")?.appendChild(ele);
          } catch {}
          break;
        }
        case 3: {
          const [name, needs, factory] = term;
          try {
            if (needs.length > 0)
              summary.insertAdjacentHTML(
                "beforeend",
                ` with <b>${needs.length}</b> requirement${
                  needs.length > 1 ? "s" : ""
                }`
              );
            const dict = write({ needs, factory }, "Element");
            if (dict) ele.append(dict);
          } catch {}
          break;
        }
      }
      return ele;
    }
  );
});

// compare/diff is more informative and useful
// and equal can be implemented on top of it
define("~/methods/equal", [], () => {
  function equal_objects(a, b) {}
  return { objects: equal_objects };
});

define("def:self-tests", [], () => {
  const equivalent = (a, b) => {
    if (Object.is(a, b)) return true;
    if (Array.isArray(a) && Array.isArray(b)) return equivalent_arrays(a, b);
    return false;
  };

  const equivalent_arrays = (a, b) => {
    if (a.length !== b.length) return false;
    for (let i = 0; i < a.length; i++)
      if (!equivalent(a[i], b[i])) return false;
    return true;
  };

  const equivalent_tagged_tuples = (a, b) => {
    // but also check tags
    if (a.length !== b.length) return false;
    for (let i = 0; i < a.length; i++)
      if (!equivalent(a[i], b[i])) return false;
    return true;
  };

  return async function selftest_init() {
    const failed = [];
    let _current;
    // prettier-ignore
    function throws(thunk) {
      try { thunk(); return false }
      catch { return true }
    }
    // prettier-ignore
    async function throwsAsync(thunk) {
      try { await thunk(); return false }
      catch { return true }
    }
    const enqueue = (f, ...args) => setTimeout(f, 0, ...args);
    const cheap_equals = (a, b) => JSON.stringify(a) === JSON.stringify(b);

    function with_group(name, do_sync_asserts, ...term) {
      console.group(name || do_sync_asserts.name.replace(/^test_/));
      try {
        return do_sync_asserts(...term);
      } finally {
        console.groupEnd();
      }
    }
    function with_group_collapsed(name, do_sync_asserts, ...term) {
      console.groupCollapsed(name || do_sync_asserts.name.replace(/^test_/));
      try {
        return do_sync_asserts(...term);
      } finally {
        console.groupEnd();
      }
    }

    let _debug_world = globalThis;

    function dump_state(world = _debug_world) {
      with_group("state dump", () => {
        with_group("goals", () => {
          for (const goal of world.require.goals) console.log(goal);
        });
        with_group("definitions", () => {
          for (const [key, value] of world.define.definitions)
            console.log(key, "→", value);
        });
        with_group("cache", () => {
          for (const [key, value] of world.require.cache)
            console.log(key, "→", value);
        });
      });
    }

    function expect_equivalent(a, b) {
      if (equivalent(a, b)) return true;
      console.error(a, "≭", b);
      return false;
    }

    function assert(value, message) {
      console.assert(value, message);
      if (value) console.log("✓", message);
      else {
        failed.push([_current, value, message]);
        dump_state(_debug_world);
        throw Error(`Assert failed`);
      }
      return value;
    }

    // COPIED elsewhere in here
    {
      async function anon() {}
      const AsyncFunction = Object.getPrototypeOf(anon).constructor;
      function is_async(fun) {
        return fun instanceof AsyncFunction;
      }
    }

    function describe(label, proc, opts) {
      _current = label;
      const old_debug = _debug_world;
      const world = (_debug_world = def.make_world());
      const claims = [];
      function it(claim, prove_it) {
        try {
          prove_it();
          console.info(claim);
        } catch (error) {
          const label = prove_it.toString();
          console.error(`Died trying to prove:`, claim, error, label);
        }
        claims.push([claim, prove_it]);
      }
      const TIMED_OUT = Symbol();
      if (is_async(proc)) {
        const timeout = 1000;
        const timed_out = new Promise(resolve =>
          setTimeout(() => resolve(TIMED_OUT), timeout)
        );
        const promise = proc(it, world);
        Promise.race([promise, timed_out]).then(result => {
          if (result === TIMED_OUT) {
            console.error(`badness: timed out`, label);
          }
        });
      } else {
        try {
          const grouper = opts?.open ? with_group : with_group_collapsed;
          grouper(label, proc, it, world);
        } finally {
          _debug_world = old_debug;
        }
      }
    }

    // what about:
    // def.mint.in(NS).tuples.X === def.mint.in(NS).tuples.X
    // I think you can guarantee *that*, but not in general that any two
    // TupleKinds with the same tag are identical

    // also: what would record and tuple mint on the same NS mean?

    describe(
      "def.mint.in(ns).tuples",
      it => {
        it("all properties are tuple constructors", () => {
          const color = def.mint.in("color:").tuples;
          expect_equivalent(typeof color.rgb, "function");
          assert(color.rgb(1, 2, 3) instanceof Array, "instanceof");
          assert(color.rgb(1, 2, 3) instanceof color.rgb, "instanceof");
          assert(!(color.rgb(1, 2, 3) instanceof color.RGB), "instanceof");
        });
        it("all properties have names in the namespace", () => {
          const color = def.mint.in("color:").tuples;
          expect_equivalent(color.RGB.name, "color:RGB");
          expect_equivalent(color.HSL.name, "color:HSL");
        });
        it("all properties have reference identity with themselves", () => {
          const kid = def.mint.in("kid:").tuples;
          assert(kid.A === kid.A, "kid.A === kid.A");
          assert(kid.B === kid.B, "kid.B === kid.B");
          assert(kid.A !== kid.B, "kid.A !== kid.B");
        });
        it("all properties construct arrays", () => {
          const geom = def.mint.in("geometry:").tuples;
          assert(Array.isArray(geom.point()), "geom.point() returns an Array");
        });
        it("can be called with or without `new`", () => {
          const world = def.mint.in("kid:").tuples;
          assert(world.Order() instanceof world.Order);
          assert(new world.Order() instanceof world.Order);
        });
        it("remembers its arguments as elements", () => {
          const color = def.mint.in("color:").tuples;
          assert(
            color.rgb(30, 42, 184).length === 3,
            "color.rgb(30, 42, 184).length === 3"
          );
          expect_equivalent(color.rgb(30, 42, 184)[0], 30);
          expect_equivalent(color.rgb(30, 42, 184)[1], 42);
          expect_equivalent(color.rgb(30, 42, 184)[2], 184);
        });
        it("treats a single argument as an element (not length)", () => {
          const big = def.mint.in("big:").tuples;
          expect_equivalent(big.O(20).length, 1);
          expect_equivalent(big.O(20)[0], 20);
        });
        it("general usage", () => {
          const { rgb, rgba, hsl } = def.mint.in("color:").tuples;
          rgb(0, 20, 55);
          rgba(0, 20, 55, 0.6);

          const fol = def.mint.in("first-order-logic:").tuples;
          const { Not, Or, And, Var } = fol;
          And(Var("X"), Not(Var("Y", true)));
          // equivalences, etc

          {
            const HTML = "http://www.w3.org/1999/xhtml#"; // not much of a destination tho
            const html = def.mint.in(HTML).records;
            const { a, ul, li } = html;
            // the tuple is [attrs, ...children]
            ul();
            ul({});
            ul({}, li(), li());
            ul({}, li(), li({}, `once upon a time`, a({})));
          }
        });
      },
      { open: true }
    );

    describe(
      "def.mint.in(ns).records",
      it => {
        const foaf = def.mint.in(`http://xmlns.com/foaf/0.1/`).records;
        const { keys } = Object;

        assert(typeof foaf === "object");
        assert(!("Person" in foaf)); // questionable
        assert(foaf.Person === foaf.Person);
        assert(foaf.Person !== foaf.PERSON);
        const { Person } = foaf;
        assert("Person" in foaf);
        assert(typeof Person === "function");
        assert(Person.name === "http://xmlns.com/foaf/0.1/Person");
        assert(typeof Person() === "object");
        expect_equivalent(
          Person()[Symbol.toStringTag],
          "http://xmlns.com/foaf/0.1/Person"
        );
        assert(Person() instanceof Person);
        assert(new Person() instanceof Person);
        assert(Person() !== Person());
        assert(Person({ name: "Sharazad" }) instanceof Person);
        expect_equivalent(Person({ name: "Shariyar" }).name, "Shariyar");
        expect_equivalent(keys(Person({ name: "Dervish", no: 1 })).length, 2);
        assert(keys(Person({ name: "Dervish", no: 2 })).includes("no"));
        expect_equivalent(Person({ name: "Dervish", no: 3 }).no, 3);
      },
      { open: true }
    );

    // ============ COPIED to new minimal def until END
    describe(
      "global define & require",
      () => {
        assert(
          typeof define === "function",
          "there's a global function called `define`"
        );
        // but this could be Node's
        assert(
          typeof require === "function",
          "there's a global function called `require`"
        );
        assert(
          typeof define.amd === "object",
          "``amd` is an object` on the global define"
        );
      },
      { open: true }
    );

    describe("define & require basic properties (all worlds)", () => {
      // See note below re `color`.  But for that, this works on the “main” world
      // const { define, require } = def;
      const { define, require } = def.make_world();
      // prettier-ignore
      {
      assert(throws(() => define()), "nullary define throws");
      // unary define is provisionally defined as a URL constructor so...
      assert(throws(() => define("x")), "unary define rejects invalid URL");
      assert(!throws(() => define("x:y")), "unary define accepts URL");
      assert(throws(() => require()), "nullary require throws");
      assert(throws(() => require(3)), "unary require expects string");
      assert(throws(() => require(true)), "unary require expects string");
      assert(throws(() => require({})), "unary require expects string");
      }

      function resolve_case(from, path, expect) {
        const got = require.resolve(path, from);
        if (
          !assert(
            got === expect,
            `if module "${from}" asks for "${path}", that resolves to "${expect}"`
          )
        )
          console.warn(`expected: ‘${expect}’\n     got: ‘${got}’`);
      }
      describe("relative path resolution", () => {
        resolve_case("a/b/c", "d", "d");
        resolve_case("a/b/c", "d/e", "d/e");
        // following 2 cases are from https://github.com/amdjs/amdjs-api/blob/master/AMD.md#module-id-format-
        resolve_case("a/b/c", "../d", "a/d");
        resolve_case("a/b/c", "./e", "a/b/e");
        resolve_case("/plays/Ham", "./Mac", "/plays/Mac");
        // Not sure about either of these
        // resolve_case("a/b/c", ".", "a/b/c")
        // resolve_case("a/b/c", "..", "a/b")
        // WHAT about this? or is a/b/c/ not a valid module id?
        // resolve_case("a/b/c/", "./e", "a/b/e")
        // § 5.4.1 “Normal examples”, from https://datatracker.ietf.org/doc/html/rfc3986#section-5.4.1
        resolve_case("http://a/b/c/d;p?q", "./g", "http://a/b/c/g");
        resolve_case("http://a/b/c/d;p?q", ".", "http://a/b/c/");
        resolve_case("http://a/b/c/d;p?q", "./", "http://a/b/c/");
        resolve_case("http://a/b/c/d;p?q", "..", "http://a/b/");
        resolve_case("http://a/b/c/d;p?q", "../", "http://a/b/");
        resolve_case("http://a/b/c/d;p?q", "../g", "http://a/b/g");
        resolve_case("http://a/b/c/d;p?q", "../..", "http://a/");
        resolve_case("http://a/b/c/d;p?q", "../../", "http://a/");
        resolve_case("http://a/b/c/d;p?q", "../../g", "http://a/g");
      });

      console.info(`define("color", ["module"], module => {...})`);
      // THIS test is problematic after roundtripping the system
      // because `color` will have been defined at the top level and this will be conflict
      define("color", ["module"], module => {
        module.exports.blend = (a, b) => {};
        module.exports.lighten = (x, y) => {};
      });
      assert(
        typeof require("color") === "object",
        "definition importing `module` yields an object"
      );
      assert(
        typeof require("color").blend === "function",
        "color module has `blend` function"
      );
      assert(
        typeof require("color").lighten === "function",
        "color module has `lighten` function"
      );

      console.info(`define("math", ["exports"], exports => {...})`);
      define("math", ["exports"], exports => {
        exports.square = () => {};
        exports.cube = () => {};
      });
      assert(
        typeof require("math") === "object",
        "definition importing `exports` yields an object"
      );
      assert(
        typeof require("math").square === "function",
        "math module has `square` function"
      );
      assert(
        typeof require("math").cube === "function",
        "math module has `cube` function"
      );
    });

    const microtask = () => new Promise(resolve => queueMicrotask(resolve));

    describe("microtask stuff", async (_it, { define, require }) => {
      const resolved = new Map();

      console.info(`require.async("A").then(A => resolved.set("A", A))`);
      require.async("A").then(A => resolved.set("A", A));
      assert(!resolved.has("A"), `!resolved.has("A")`);

      console.info(`define("A", "amicus")`);
      define("A", "amicus");
      assert(!resolved.has("A"), `!resolved.has("A")`);

      await microtask(); // confirm that stuff that should be enqueued is enqueued

      assert(resolved.has("A"), `resolved.has("A")`);
      assert(resolved.get("A") === "amicus", `resolved.get("A") === "amicus"`);

      const hearts = (a, b) => `(${a}♥${b})`;
      define("C", ["D", "E"], hearts);
      define("B", ["C", "D"], hearts);
      define("E", () => "pepper");
      define("D", () => "mill");

      assert(require("A") === "amicus", "A def");
      assert(require("B") === "((mill♥pepper)♥mill)", "B def");
      assert(require("C") === "(mill♥pepper)", "C def");
      assert(require("D") === "mill", "D def");
      assert(require("E") === "pepper", "E def");
    });

    describe("define.definitions", (_it, { define, require }) => {
      assert(
        typeof define.definitions === "object",
        "define.definitions is an object"
      );
      assert(define.definitions instanceof Map, "define.definitions is a Map"); // shortcut
      function confirm_definition(name, needs, formula) {
        assert(
          define.definitions.has(name),
          "define.definitions remembers definition by name"
        );
        const definition = define.definitions.get(name);
        assert(Array.isArray(definition), "definition is an Array (a tuple)");
        assert(
          definition instanceof def.amd.quote.define,
          "definition is an AMD define term"
        );
        assert(definition[0] === name, "definition remembers name as 1st");
        assert(
          definition.length === 2 || definition.length === 3,
          "definition is a 2-tuple or a 3-tuple"
        );
        if (definition.length === 2) {
          assert(definition[1] === formula, "definition₃ has formula in 2nd");
        } else {
          // AND is equivalent to given needs...
          assert(Array.isArray(definition[1]), "definition₃ has needs in 2nd");
          assert(Array.isArray(definition[1]), "definition₃ has needs in 2nd");
          assert(definition[2] === formula, "definition₃ has formula in 3rd");
        }
      }
      function test_needless_definition(name, value) {
        console.info(`${name} → ${value}`);
        const start_size = define.definitions.size;
        if (define.definitions.has(name)) throw Error(`Conflict ${name}`);
        define(name, value);
        assert(
          define.definitions.size === start_size + 1,
          "define.definitions gains one entry immediately after a new definition"
        );
        // assert(define.definitions.get(name).needs.length === 0, "definition `needs` is empty if no requirements")
        confirm_definition(name, [], value);
      }
      function test_needful_definition(name, needs, formula) {
        console.info(`${name} ${needs} → ${formula}`);
        const start_size = define.definitions.size;
        if (define.definitions.has(name)) throw Error(`Conflict ${name}`);
        define(name, needs, formula);
        confirm_definition(name, needs, formula);
        assert(
          define.definitions.size === start_size + 1,
          "define.definitions gains one entry immediately after a new definition"
        );
        // assert(cheap_equals(needs, define.definitions.get(name).needs), "definition remembers needs")
        assert(
          cheap_equals(needs, define.definitions.get(name)[1]),
          "definition remembers needs"
        );
      }
      with_group_collapsed(
        `primitive value`,
        test_needless_definition,
        "foo",
        99
      );
      with_group_collapsed(
        `function value`,
        test_needless_definition,
        "bar",
        () => "banana"
      );
      with_group_collapsed(
        `needs`,
        test_needful_definition,
        "crops",
        ["rain", "soil"],
        function grow(rain, soil) {}
      );
    });

    const has_goal = (r, needle) =>
      r.goals.find(g => equivalent_tagged_tuples(g, needle));

    describe("a complete example", (_it, { define, require }) => {
      let { extend } = define;
      let G = require.goals.length;
      let Q = define.amd.quote;

      const traced = new Map();
      const trace =
        (k, f) =>
        (...t) => {
          const result = f(...t);
          traced.set(k, result);
          return result;
        };

      assert(!define.definitions.has("cookie"), "`cookie` is not defined");
      assert(!define.definitions.has("dough"), "`dough` is not defined");
      assert(!define.definitions.has("oven"), "`oven` is not defined");

      const check_oven = trace(
        "A",
        oven => typeof oven.heat_now === "function"
      );
      console.info(`require(["oven"], check_oven)`, check_oven);
      require(["oven"], check_oven);

      assert(require.goals.length === G + 1, "the world gained a goal");
      assert(
        has_goal(require, Q.require(["oven"], check_oven)),
        "the goal is to `check_oven` when `oven` is available"
      );
      assert(traced.size === 0, "but that has not happened");

      const bake = trace("B", (oven, dough) => `${oven} ⊗ ${dough}`);
      console.info(`require(["oven", "dough"], bake)`, bake);
      require(["oven", "dough"], bake);

      assert(require.goals.length === G + 2, "the world gained another goal");
      assert(
        has_goal(require, Q.require(["oven", "dough"], bake)),
        "the goal is to `bake` when `oven` and `dough` are available"
      );
      assert(traced.size === 0, "no goals have been achieved");

      const oven = {
        toString() {
          return "OVEN";
        },
        heat_now(it) {
          // dummy operation
          if (typeof it?.temperature === "number") it.temperature++;
        },
      };
      console.info(`define("oven", oven)`, oven);
      define("oven", oven);

      assert(require("oven") === oven, `require("oven") === oven`);
      assert(
        !has_goal(require, Q.require(["oven"], check_oven)),
        "goal to `check_oven` is gone"
      );

      assert(traced.has("A"), "callback requiring oven resolved");
      assert(traced.get("A") === true, "`check_oven` returned `true`");

      console.info(`define("dough", "boy")`);
      define("dough", "boy");

      assert(require("dough") === "boy", '`dough` is now defined as "boy"');
      assert(
        !has_goal(require, Q.require(["oven", "dough"], bake)),
        "goal to `bake` pending `oven` and `dough` is gone"
      );

      assert(traced.has("B"), "callback requiring oven and dough resolved");
      if (
        !assert(
          traced.get("B") === `OVEN ⊗ boy`,
          "`bake(oven, dough)` returned expected result"
        )
      )
        console.log(`traced.get("B")`, traced.get("B"));

      // I don't think the remaining tests add anything essential
      const bake_cookie = (oven, dough) => oven && dough && `🍪`;
      console.info(
        `define("cookie", ["oven", "dough"], bake_cookie)`,
        bake_cookie
      );
      define("cookie", ["oven", "dough"], bake_cookie);

      assert(require("cookie") === "🍪", "require(`cookie`) === 🍪");

      const eat = trace("D", (monster, cookie) => `${monster} ⊰ ${cookie}`);
      console.info(`require(["monster", "cookie"], eat)`, eat);
      require(["monster", "cookie"], eat);

      define("monster", "MONSTER");

      assert(
        traced.get("D") === `MONSTER ⊰ 🍪`,
        `traced.get("D") === "MONSTER ⊰ 🍪"`
      );
    });

    describe("amd basic", (_it, { define, require }) => {
      console.log("Show that definition function is called only once");
      let _i = 0;
      define("counter1", () => {
        ++_i;
        return { i: _i };
      });
      assert(require("counter1").i === 1, "counter is 1 on the first call");
      assert(require("counter1").i === 1, "counter is 1 on the second call");

      // I don't think this proves anything beyond the above
      // it just uses only closures instead of extended properties of define/require
      // so should be usable with other AMD implementations
      let _a, _b, _c, _ab, _bc, _ac, _abc;
      let G = require.goals.length;

      const requires = [
        [["a"], a => (_a = a)],
        [["b"], b => (_b = b)],
        [["c"], c => (_c = c)],
        [["a", "b"], (a, b) => (_ab = { a, b })],
        [["b", "c"], (b, c) => (_bc = { b, c })],
        [["a", "c"], (a, c) => (_ac = { a, c })],
        [["a", "b", "c"], (a, b, c) => (_abc = { a, b, c })],
      ];
      for (const [needs, factory] of requires) {
        require(needs, factory);
        console.info(`require(${JSON.stringify(needs)}, ${factory})`);
      }

      assert(require.goals.length === G + 7, "world has 7 new goals");

      define("a", ["b"], b => ({ alpha: b }));
      console.info(`define("a", ["b"], b => ({alpha: b}))`);
      assert(_a === undefined, "`_a` has not been set");
      assert(require.goals.length === G + 7, "world still has 7 new goals");
      define("b", ["c"], c => ({ beta: c }));
      console.info(`define("b", ["c"], c => ({beta: c}))`);
      assert(_a === undefined, "`_a` has still not been set");
      assert(_b === undefined, "`_b` has not been set");
      assert(_c === undefined, "`_c` has not been set");
      assert(require.goals.length === G + 7, "world still has 7 new goals");
      define("c", 3);
      console.info(`define("c", 3)`);
      // This doesn't technically prove that the new goals are the achieved ones
      assert(
        require.goals.length === G + 0,
        "all new goals have been achieved"
      );

      assert(require("c") === 3, `require("c") === 3`);
      assert(
        cheap_equals(require("b"), { beta: 3 }),
        `require("b") is {beta: 3}`
      );
      assert(
        cheap_equals(require("a"), { alpha: { beta: 3 } }),
        `require("a") is {alpha:{beta: 3}}`
      );

      assert(cheap_equals(_c, 3), "_c is set to 3");

      assert(cheap_equals(_b, { beta: 3 }), "_b is set to {beta: 3}");
      assert(
        cheap_equals(_a, { alpha: { beta: 3 } }),
        "_a is set to {alpha:{beta: 3}}"
      );

      assert(
        cheap_equals(_ab, { a: { alpha: { beta: 3 } }, b: { beta: 3 } }),
        `_ab is set to {a: {alpha: {beta: 3}}, b: {beta: 3}}`
      );
      assert(
        cheap_equals(_bc, { b: { beta: 3 }, c: 3 }),
        `_bc is set to {b: {beta: 3}, c: 3}`
      );
      assert(
        cheap_equals(_ac, { a: { alpha: { beta: 3 } }, c: 3 }),
        `_ac is set to {a: {alpha: {beta: 3}}, c: 3}`
      );

      assert(
        cheap_equals(_abc, { a: { alpha: { beta: 3 } }, b: { beta: 3 }, c: 3 }),
        `_abc is set to {a: {alpha: {beta: 3}}, b: {beta: 3}, c: 3}`
      );

      assert(
        cheap_equals(require("a"), { alpha: { beta: 3 } }),
        `require("a") is {alpha: {beta: 3}}`
      );
    });

    await describe("avoid infinite regress", (_it, { define, require }) => {
      // this will cause "too much recursion"
      // unless you forbid the requiring of X during the defining of X
      define("journey", () => {
        require("journey");
      });
      require("journey");
      // test separately for imported require...
      // test separately for indirect cycle
      // is nested define a thing?
      // in requireJS, was getting no clear policy on things like this
      /*
define("y", [], () => {
  define("z", [], () => ({z:1});
  define("./z", [], () => ({z:2});
  // return {y: 2}
}) 
require(['y'], a => console.log('y is', a))
require(['./z'], a => console.log('./z is', a))
*/
    });

    await describe("require.async basic properties", (_it, {
      define,
      require,
    }) => {
      console.log("Things about require.async that we can test synchronously");
      let G = require.goals.length;

      assert(
        typeof require.async === "function",
        "require.async is a function"
      );
      assert(
        typeof require.async("xyz").then === "function",
        "require.async(id) returns a thenable when `id` is unknown"
      );

      define("happy", true);
      console.info(`define("happy", true)`);
      assert(
        require.async("happy") === true,
        `require.async("happy") resolves immediately to true`,
        `successful async lookup is synchronous`
      );

      assert(
        throws(() => define("happy", true)),
        `attempting to redefine throws (Conflict) synchronously`
      );

      assert(!define.definitions.has("color"), "`color` is not defined");

      let _color;
      require.async("color").then(color => (_color = color));
      console.info(`require.async("color").then(color => _color = color)`);

      define("color", "green");
      console.info(`define("color", "green")`);
      queueMicrotask(() => assert(_color === "green", `_color is "green"`));

      define("weapon", "sword");
      define("horcrux", "locket");

      let _wh_result1;
      require(["weapon", "horcrux"], (weapon, horcrux) => {
        console.debug("CALLED WH CALLBACK 1");
        _wh_result1 = `1 ${weapon} ⋇ ${horcrux}`;
      });
      assert(
        _wh_result1 === "1 sword ⋇ locket",
        `_wh_result1 === "1 sword ⋇ locket"`
      );

      let _wh_result2;
      Promise.resolve(require.async(["weapon", "horcrux"])).then(
        ([weapon, horcrux]) => {
          console.debug("CALLED WH CALLBACK 2");
          _wh_result2 = `2 ${weapon} ⋇ ${horcrux}`;
        }
      );
      function prove_it() {
        assert(
          _wh_result2 === "2 sword ⋇ locket",
          `_wh_result2 === "2 sword ⋇ locket"`
        );
      }
      // prove_it() this fails, and that's okay
      queueMicrotask(prove_it);
    });

    // TODO: check for circularity.  AMD tests indicate this is handled through `exports`

    // End of synchronous tests
    // From here on we can't safely use console grouping
    // `await` is not very useful for writing tests anyway

    describe("require.async([], fn)", async (_it, { define, require }) => {
      assert(
        (await require.async([], () => 3)) === 3,
        "require.async resolves to callback's return value"
      );
    });

    describe("require.async(string[]) sync", async (_it, world) => {
      const { define, require } = world;
      define("color", "green");
      define("name", "J");
      assert(
        cheap_equals(require.async(["color", "name"]), ["green", "J"]),
        `require.async(string[]) resolves synchronously to array of predefined values`
      );
    });

    // currently there's no more direct way to ask this
    function assert_not_defined(require, what) {
      assert(
        throws(() => require(what)),
        `expected not defined ${what}`
      );
    }

    describe("require.async(string)", async (_it, { define, require }) => {
      assert_not_defined(require, "🚁");
      const promise = require.async("🚁");
      define("🚁", "HELICOPTER");
      await microtask();
      const resolved = await promise;
      assert(
        resolved === "HELICOPTER",
        "require.async(string) resolves to unary value defined afterwards"
      );
    });

    describe("require.async(string[])", async (_it, { define, require }) => {
      assert_not_defined(require, "simon");
      assert_not_defined(require, "garfunkel");
      const promise = require.async(["simon", "garfunkel"]);
      define("simon", "paul");
      define("garfunkel", "art");
      await microtask();
      const resolved = await promise;
      assert(
        cheap_equals(resolved, ["paul", "art"]),
        "require.async(string[]) resolves to array of values defined afterwards"
      );
    });

    console.log("ALL TESTS ISSUED =====================");
    setTimeout(() => {
      console.log(`${failed.length} assertions failed`);
      for (const failure of failed) console.warn(...failure);
    }, 1);
    // =================== END
  };
});

define("~/process/streams", [], () => {
  `!!!!!!!!!!!!!!!!!!!!!!!!!!! HOME of this is in process-lab for now`;
  const { assert } = console;
  const NOTHING = Symbol("∅");

  function make_subscribable() {
    let _value = NOTHING;
    const subscribers = new Set();

    function subscribe(subscriber) {
      subscribers.add(subscriber);
      function unsubscribe() {
        subscribers.delete(subscriber);
      }
      const subscription = { unsubscribe };
      return subscription;
    }

    // - synchronous
    // - one subscriber cannot break others
    function next(value) {
      _value = value;
      let errors = null;
      for (const subscriber of subscribers) {
        try {
          subscriber.next(value);
        } catch (error) {
          if (typeof subscriber.catch === "function") subscriber.catch(error);
          else {
            if (errors === null) errors = [error];
            else errors.push(error);
          }
        }
      }
      if (errors !== null) {
        throw AggregateError(errors, "uhandled errors in broadcast");
      }
    }
    function has_cache() {
      return _value !== NOTHING;
    }
    function cache() {
      return _value;
    }
    const public = { subscribe };
    function dispose() {
      subscribers.clear(); // and/or make `next` a no-op
    }
    return { dispose, public, next, has_cache, cache };
  }

  return { make_subscribable };
});

define("~/process/channels", [], () => {
  `!!!!!!!!!!!!!!!!!!!!!!!!!!! HOME of this is in process-lab for now`;
  const { assert } = console;
  const is_not_empty = array => array.length > 0;

  function make_channel(buffer) {
    const pending_put_values = [];
    const pending_put_resolves = [];
    const pending_takes = [];
    return {
      can_put_now: () => !buffer.is_full(),
      can_take_now: () => !buffer.is_empty(),
      put(value) {
        if (is_not_empty(pending_takes)) {
          const n = pending_takes.length;
          assert?.(buffer.is_empty(), `${n} pending takes but non-∅ buffer `);
          pending_takes.shift()(value);
        } else if (!buffer.is_full()) buffer.push(value);
        else
          return new Promise(resolve => {
            pending_put_values.push(value);
            pending_put_resolves.push(resolve);
            // if count exceeds expected # of (well-behaved) writers
            // console.debug(`${pending_put_resolves.length} pending puts`);
          });
      },
      take() {
        if (buffer.is_empty()) {
          if (assert) {
            const n = pending_put_values.length;
            assert(n === 0, `∅ buffer but ${n} pending puts `);
          }
          return new Promise(resolve => pending_takes.push(resolve));
        }

        const value = buffer.pop();
        if (is_not_empty(pending_put_values)) {
          const resolve = pending_put_resolves.shift();
          const value = pending_put_values.shift();
          buffer.push(value);
          resolve();
        }
        return value;
      },
    };
  }
  make_channel.comment = `A minimal blocking queue.`;
  return { make_channel };
});

// ==== host facilities

// == indexeddb wrapper stuff
// good info about indexedDB state here https://stackoverflow.com/q/33441956

define("~/io/indexedDB", ["~/process/channels"], ({ make_channel }) => {
  // could implement the host indexedDB as a formal requirement
  if (!indexedDB) {
    console.warn("Your browser doesn't support a stable version of IndexedDB!");
    return;
  }
  const { stringify } = JSON;
  const { keys } = Object;
  const { assert } = console;
  // !!!!!!!!! see adt/ring_buffers
  const NULL_BUFFER = { is_empty: () => true, is_full: () => true, length: 0 };
  const make_ring_buffer = size => {
    if (size === 0) return NULL_BUFFER;
    const _buffer = new Array(size);
    let start = 0; // aka head, where the first item is (when ≠ end)
    let end = 0; // aka tail, where the next item will go
    let length = 0;

    return {
      is_empty: () => length === 0,
      is_full: () => length === size,
      push(value) {
        assert?.(length < size, `Full!`);
        ++length;
        _buffer[end] = value;
        end = (end + 1) % size;
      },
      pop() {
        assert?.(length !== 0, `Empty!`);
        --length;
        const value = _buffer[start];
        start = (start + 1) % size;
        return value;
      },
      get length() {
        return length;
      },
    };
  };

  // ===== async helpers
  const sleep = ms => new Promise(r => setTimeout(r, ms));

  /**
   * @param options {{max_concurrent?: number}}
   */
  const async_queue = options => {
    const queue = []; // promise thunks
    const inflight = new Set(); // promises
    const max = options?.max_concurrent ?? 1;

    const launch = thunk => {
      const promise = thunk();
      inflight.add(promise);
      promise.finally(() => {
        inflight.delete(promise);
        if (queue.length > 0) launch(queue.shift());
      });
      return promise;
    };

    /**
     * @template T
     * @param thunk {() => Promise<T>}
     * @returns {Promise<T>}
     */
    return thunk => {
      if (inflight.size < max) {
        assert?.(
          !queue.length,
          `${queue.length} in queue ${inflight.size}<${max}!`
        );
        return launch(thunk);
      }
      return new Promise(resolve => {
        queue.push(() => {
          const promise = thunk();
          resolve(promise);
          return promise;
        });
      });
    };
  };

  // ASSUMES this is called during a versionchange transaction
  /**
   * @param db {IDBDatabase}
   * @param name {string}
   * @param description {IDBStoreDescription}
   */
  function create_store_from(db, name, description) {
    const { keyPath, autoIncrement, indexes } = description;
    // we could create the store with neither a key nor autoincrement
    // but we won't be able to use our I/O with it (which has no ID channel)
    if (keyPath == null && autoIncrement == null)
      throw Error(`Out-of-line keys are not supported`);
    const store = db.createObjectStore(name, { keyPath, autoIncrement });
    if (indexes) {
      for (const index_name of keys(indexes)) {
        const spec = indexes[index_name];
        store.createIndex(index_name, spec.keyPath, spec);
      }
    }
    return store;
  }

  // ASSUMES this is called during a versionchange transaction
  /**
   * @param store {IDBObjectStore}
   * @param description {IDBStoreDescription}
   */
  function update_store_from(store, description) {
    const upgrades = [...diff_store(store, description)];
    for (const difference of upgrades) {
      if (difference.type === "missing") {
        const { index: index_name, description } = difference;
        const { keyPath, ...options } = description;
        store.createIndex(index_name, keyPath, options);
      } else throw Error(`Not implemented: ${difference.type} diffs`);
    }
  }

  /**
   * @param store {IDBObjectStore}
   * @param description {IDBStoreDescription}
   */
  function* diff_store(store, description) {
    const { keyPath, autoIncrement, indexes } = description;

    if (
      store.autoIncrement !== (autoIncrement ?? false) ||
      stringify(store.keyPath) !== stringify(keyPath ?? null)
    )
      yield { type: "pk" };

    if (indexes) {
      for (const name of keys(indexes)) {
        const spec = indexes[name];
        if (!store.indexNames.contains(name)) {
          yield { type: "missing", index: name, description: spec };
          continue;
        }
        const index = store.index(name);
        if (
          index.unique !== (spec.unique ?? false) ||
          index.multiEntry !== (spec.multiEntry ?? false) ||
          stringify(index.keyPath) !== stringify(spec.keyPath)
        )
          yield { type: "index", index: name, description: spec };
      }
    }
  }

  const gate = async_queue({ max_concurrent: 1 });

  /**
   * The *only* place to open a connection.
   *
   * @param db_name {string}
   * @param version {number | undefined}
   * @param upgrade {((db: IDBDatabase, txn: IDBTransaction) => void) | undefined}
   * @returns {Promise<IDBDatabase>}
   */
  async function connect(db_name, version, upgrade) {
    return gate(() => {
      return new Promise(async (resolve, reject) => {
        await sleep(1); // avoid `versionchange` race condition in Chrome
        const request = indexedDB.open(db_name, version);
        request.onerror = error => reject(`${error}`);
        request.onblocked = e => {
          reject(`BLOCKED → ${e.newVersion}`);
        };
        request.onsuccess = () => {
          const db = request.result;
          db.addEventListener("versionchange", () => {
            db.close(); // the closest thing to a best practice in this API
          });
          resolve(db);
        };
        if (typeof upgrade === "function") {
          request.onupgradeneeded = () => {
            upgrade(request.result, request.transaction);
          };
        }
      });
    });
  }

  const target_versions = new Map();

  /**
   * Resolve to an open connection to the indicated database in which the
   * indicated store was definitely configured as described when you left it.
   *
   * @param db_name {string}
   * @param store_name {string}
   * @param description {IDBStoreDescription}
   * @returns {Promise<IDBDatabase>}
   */
  async function database_with(db_name, store_name, description) {
    // If you open at default version & get upgrade, the database didn't exist
    let created = false;
    const existing_db = await connect(db_name, undefined, db => {
      created = true;
      create_store_from(db, store_name, description);
    });
    if (created) return existing_db; // since we know we configured store

    // The database existed already
    if (existing_db.objectStoreNames.contains(store_name)) {
      const txn = existing_db.transaction(store_name);
      const store = txn.objectStore(store_name);
      const [any_difference] = diff_store(store, description);
      if (!any_difference) return existing_db;
    }

    // The store didn't exist or needs configuration
    existing_db.close();
    const last_version = target_versions.get(db_name) ?? existing_db.version;
    const next_version = last_version + 1;
    target_versions.set(db_name, next_version);

    return connect(db_name, next_version, (db, txn) => {
      if (db.objectStoreNames.contains(store_name)) {
        const store = txn.objectStore(store_name);
        update_store_from(store, description);
      } else {
        create_store_from(db, store_name, description);
      }
    });
  }

  /**
   * Resolve to a store in the indicated database that has the described
   * configuration, attached to a transaction in the indicated mode.
   *
   * @param db_name {string}
   * @param store_name {string}
   * @param description {IDBStoreDescription}
   * @param mode {Exclude<IDBTransactionMode, "versionchange">}
   * @returns {Promise<IDBObjectStore>}
   */
  async function get_store_with(db_name, store_name, description, mode) {
    const db = await database_with(db_name, store_name, description);
    const txn = db.transaction(store_name, mode); // NEW TRANSACTION
    return txn.objectStore(store_name);
  }

  /**
   * Feed a cursor into a given channel.
   *
   * @param thunk {() => Promise<IDBRequest<IDBCursor>>}
   * @param channel {ReturnValue<typeof make_channel>}
   */
  async function pipe_cursor_to(thunk, channel, is_index) {
    let resolve, reject, cursor;

    function make_new_cursor_request() {
      const request = thunk();
      // have yet to see this happen though & most don't apply to cursors
      request.onerror = error => reject(`${error}`);
      request.onsuccess = event => resolve(event.target.result);
      return request;
    }

    const next_read = () =>
      new Promise((_resolve, _reject) => {
        resolve = _resolve;
        reject = _reject;
      });

    make_new_cursor_request();
    while (true) {
      cursor = await next_read();
      if (!cursor) break;
      const { value } = cursor; // HOTSPOT
      // remember in case we need to resume
      const frontier = { key: cursor.key, pk: cursor.primaryKey }; // HOTSPOT
      if (channel.can_put_now()) {
        channel.put(value);
        cursor.continue(); // HOTSPOT
        continue;
      }
      await channel.put(value);
      if (cursor.request.readyState !== "done") {
        try {
          cursor.continue(); // HOTSPOT
          return;
        } catch (error) {
          if (error.name !== "TransactionInactiveError") throw error;
        }
      }
      make_new_cursor_request();
      cursor = await next_read();
      if (!cursor) break;
      if (is_index) {
        cursor.continuePrimaryKey(frontier.key, frontier.pk);
      } else {
        cursor.continue(frontier.key);
      }
      // we returned to the item we already put, so ignore & move to the next
      if (!(await next_read())) break;
      cursor.advance(1);
    }
    channel.put(DONE);
  }

  // unlike write, this is not responsible for the db/store existing
  // yields single records
  const DONE = Symbol("done");
  async function* read(spec) {
    const trace = false ? (...x) => console.debug(`ℝ`, ...x) : null;
    const db_name = spec.db;
    const store_name = spec.store;
    const index_name = spec.index;
    const batch_size = spec.batch_size ?? 16;
    const __buffer = make_ring_buffer(batch_size); // don't reference this though
    const channel = make_channel(__buffer);

    let connection = await connect(db_name);

    function make_cursor_request() {
      trace?.(`creating new transaction & everything`);
      const transaction = connection.transaction(store_name); // NEW TRANSACTION
      const store = transaction.objectStore(store_name);
      const source = index_name ? store.index(index_name) : store;
      const request = source.openCursor(spec.query, spec.dir);
      return request;
    }

    const is_index = !!index_name; // TEMP
    pipe_cursor_to(make_cursor_request, channel, is_index);

    // a generic channel consume
    try {
      while (true) {
        const result = channel.can_take_now()
          ? channel.take()
          : await channel.take();
        if (result === DONE) break;
        yield result;
      }
    } finally {
      connection.close();
    }
  }

  async function* read_batched(spec) {
    const { db, store } = spec;
    const limit = spec.limit ?? 16;
    const __buffer = make_ring_buffer(limit); // don't reference this though
    const channel = make_channel(__buffer);

    let _connection = await connect(db);
    let _last_key;

    async function read_next_batch() {
      // maybe check connection
      const source = { db, store };
      let query = undefined;
      // this is not quite right, though... what if you're not on the PK?
      // you also need to do 2 calls to know whether there are more,
      // if the result equals the batch size. so how would you resume in that case?
      if (_last_key) query = IDBKeyRange.lowerBound(_last_key, true);
      const batch = await get_all(source, query, limit, _connection);
    }
    while (true) {
      const batch = await read_next_batch();
      if (!batch) break;
      _last_key = batch.at(-1);
      yield batch;
    }

    try {
      while (true) {
        const result = channel.can_take_now()
          ? channel.take()
          : await channel.take();
        if (result === DONE) break;
        yield result;
      }
    } finally {
      _connection.close();
    }
  }

  async function* write(source, destination, options) {
    const trace = false ? (...x) => console.debug("WRITE", ...x) : null;
    if (!source) throw Error(`Write requires a source!`);
    if (!destination) throw Error(`Write requires a destination!`);
    const { database: db, store: store_name, description } = destination;
    if (!description) throw Error(`Destination should have a description!`);
    let size = options?.batch_size ?? 16;
    assert?.(typeof size === "number", `Write got a ${typeof size} batch size`);
    const store = await get_store_with(
      db,
      store_name,
      description,
      "readwrite"
    );
    let total_written = 0;
    let written_this_round = 0;
    let errors = null;
    try {
      // don't we need to do this at some point?  and check success?
      // store.transaction.commit();
      for (const item of source) {
        trace?.("item", item);
        try {
          store.add(item);
        } catch (error) {
          if (
            error.type === "TransactionInactiveError" ||
            error.toString().startsWith("TransactionInactiveError")
          ) {
            trace?.("caught TransactionInactiveError", error);
            // okay now what?
            throw error;
          }
          errors ??= [];
          errors.push(`${error}`);
          // shouldn't we still count this against size?
          continue;
        }
        written_this_round++;
        trace?.(`${written_this_round} written this round`);
        total_written++;
        if (written_this_round >= size) {
          const status = { total_written, written_this_round, errors };
          trace?.(`in loop yielding and resetting`, status);
          yield status;
          written_this_round = 0;
          errors = null;
        }
      }
      if (written_this_round > 0 || errors.length > 0) {
        const final_status = { total_written, written_this_round, errors };
        trace?.(`yielding final status`, final_status);
        yield final_status;
      }
    } finally {
      store.transaction.db.close();
      trace?.(`closed connection`);
    }
  }

  async function get_by_key(spec, key) {
    const connection = await connect(spec.db, undefined);
    const transaction = connection.transaction(spec.store);
    const store = transaction.objectStore(spec.store);
    const source = spec.index ? store.index(spec.index) : store;
    const request = source.get(key);
    return new Promise((resolve, reject) => {
      request.onerror = reject;
      request.onsuccess = () => resolve(request.result);
    });
  }

  async function get_all(view, query, limit, connection) {
    connection ??= await connect(view.db, undefined);
    const transaction = connection.transaction(view.store);
    const store = transaction.objectStore(view.store);
    const source = view.index ? store.index(view.index) : store;
    const request = source.getAll(query, limit);
    return new Promise((resolve, reject) => {
      request.onerror = reject;
      request.onsuccess = () => resolve(request.result);
    });
  }

  async function count(spec) {
    const connection = await connect(spec.db);
    const transaction = connection.transaction(spec.store);
    const store = transaction.objectStore(spec.store);
    const source = spec.index ? store.index(spec.index) : store;
    const request = source.count(spec.query);
    return new Promise((resolve, reject) => {
      request.onerror = error => reject(`${error}`);
      request.onsuccess = () => resolve(request.result);
    });
  }

  const io = { read, write };
  const expedient = { get_by_key, get_all, count };
  const lldb = { async_queue, database_with, diff_store, connect }; // mainly for debug
  const proc = { make_ring_buffer }; // for testing
  const IDB = { ...io, ...lldb, ...proc, ...expedient };
  return IDB;
});

// ==== application domain

// operations on extended ISO 8601 representations
// TBD: update this to use the same format as ECMAScript (6-digit signed)
// but CURRENTLY uses a four-year sign-optional format
// - dates from year -9999 (10,000 BC) through 9999
// - precision Day, Month, Year, Decade, Century
// - Millennia as special units using ranges
define("timelab:ISO8601", [], () => {
  const { assert } = console;
  const { BadRequest, NotImplemented } = def.errors;
  function self_tests() {
    // prettier-ignore
    const FACTS = [
      ["day_in_year", ["-0004-03-01"], 31 + 29], // called 5 BC but a leap year
      ["day_in_year", ["-0003-03-01"], 31 + 28], // called 4 BC but not a leap year
      ["day_in_year", ["-0001-03-01"], 31 + 28],
      ["day_in_year", ["0000-03-01"], 31 + 29], // called 1 BC but a leap year
      ["day_in_year", ["0001-03-01"], 31 + 28],
      ["day_in_year", ["0003-03-01"], 31 + 28],
      ["day_in_year", ["0004-03-01"], 31 + 29],
      ["day_in_year", ["1920-01-01"], 0],
      ["day_in_year", ["1920-01-02"], 1],
      ["day_in_year", ["1920-02-01"], 31],
      ["day_in_year", ["1919-12-30"], 363],
      ["day_in_year", ["1920-12-31"], 365], // leap year
      ["day_in_year", ["1900-12-31"], 364], // NOT a leap year
      ["day_in_year", ["1600-12-31"], 365], // leap year
      // parse(-0429/0000) throws “ill-formed millennium” but it's not intended as one
      // PROVISIONAL
      ["recognize", ["1920s-40"], "192/194"],
      ["recognize", ["first mille", 'en'], "0001/1000"],
      ["recognize", ["first millen bc", 'en'], "-0999/0000"],
      ["recognize", ["2nd M bc", 'en'], "-1999/-1000"],
      // ohhhhh this looks like march 1990
      // ["recognize", ["1990-03"], "1993/2004"],
      // ["recognize", ["1978-2023"], "1978/2023"], // later
      ["label", ["-9999"], "10000 BC"],
      ["label", ["-9998"], "9999 BC"],
      ["label", ["-0002"], "3 BC"],
      ["label", ["-0001"], "2 BC"],
      ["label", ["-0000"], "1 BC"], // though -0000 isn't valid...
      ["label", ["0000"], "1 BC"],
      ["label", ["0001"], "1"], // could also be ‘1 CE’
      ["label", ["0002"], "2"], // etc
      ["label", ["0012"], "12"],
      ["label", ["0123"], "123"],
      ["label", ["9999"], "9999"],
      ["label", ["1564-04", "en"], "April 1564"],
      // rn day is using short months
      // ["label", ["-0009-12", "en"], "December 10 BC"],
      ["label", ["1957-05-06"], "May 6, 1957"],
      // ["label", ["1957-07-06"], "July 6, 1957"],
      ["label", ["-0400-07-14"], "Jul 14, 401 BC"],
      ["label", ["-1999/-1000", "en"], "2nd millennium BC"],
      ["label", ["-0999/0000", "en"], "1st millennium BC"],
      ["label", ["0001/1000", "en"], "1st millennium"],
      ["label", ["1001/2000", "en"], "2nd millennium"],
      ["label", ["1902-03/1988"], "March 1902 – 1988"], // EN DASH, not hyphen
      // FAILS (rn) getting “August 18, 294 BC”... is this some time zone bs?
      // ["label", ["-0294-08-19"], "August 19, 294 BC"],
      ["unit_of", ["0001/1000"], "Millennium"],
      ["unit_of", ["19"], "Century"],
      ["unit_of", ["199"], "Decade"],
      ["unit_of", ["1999"], "Year"],
      ["unit_of", ["1999-12"], "Month"],
      ["unit_of", ["-0004-03-01"], "Day"],
      ["unit_of", ["-0004-03-01T11"], "Hour"], // but may be unreachable by scale
      ["unit_of", ["1888-02-19T02:48"], "Minute"], // "
      ["unit_of", ["1981-09-09T08:15:03"], "Second"],
      ["unit_of", ["1981-09-09T08:15:03.100"], "Millisecond"],
      ["unit_of", ["1981-09-09T08:15:03.100Z"], "Millisecond"],
      ["unit_of", ["1981-09-09T08:15:03.100-0415"], "Millisecond"],
      ["unit_of", ["1981-09-09T08:15:03-0415"], "Second"],
      ["unit_of", ["1981-09-09T08:15+0000"], "Minute"],
      ["unit_of", ["1981-09-09T08+0415"], "Hour"],
      ["compare_units", ["Year", "Year"], 0],
      ["compare_units", ["Year", "Month"], 1],
      ["compare_units", ["Month", "Year"], -1],
      ["compare_units", ["Decade", "Year"], 1],
      ["compare_units", ["Century", "Year"], 2],
      ["compare", ["1234", "1234"], 0], // indeed, ∀ x compare(x,x)=0
      ["compare", ["0234", "1234"], -1],
      ["compare", ["0234", "0230"], 1],
      ["compare", ["-0234", "0230"], -1],
      ["compare", ["-0234", "-0230"], -1],
      ["compare", ["-0234", "-0238"], 1],
      ["compare", ["-0100-06", "-0100-07"], -1],
      ["compare", ["-0100-06", "-0100-05"], 1],
      ["compare", ["-0100-06-11", "-0100-06-15"], -1],
      ["compare", ["-0100-06-19", "-0100-06-15"], 1],
      ["compare", ["-0100-06-19", "-0099-06-15"], -1],
      ["between", ["1234", "-0234", "1238"], true],
      ["between", ["1234", "-0234", "-1238"], false],
      // ["distance", ["-12", "-04"], {unit: "Century", n: 8 }],
      // ["distance", ["-01", "05"], {unit: "Century", n: 5 }],
      // Is this correct? we skip the zero century?
      ["distance", ["-01", "01"], { unit: "Century", n: 1 }],
      ["distance", ["01", "05"], { unit: "Century", n: 4 }],
      ["distance", ["07", "18"], { unit: "Century", n: 11 }],
      ["distance", ["18", "18"], { unit: "Century", n: 0 }],
      ["distance", ["-0001", "0001"], { unit: "Year", n: 2 }],
      ["distance", ["-0001", "0001"], { unit: "Year", n: 2 }],
      ["distance", ["-0001", "0000"], { unit: "Year", n: 1 }],
      ["distance", ["0000", "0001"], { unit: "Year", n: 1 }],
      ["distance", ["0024", "0142"], { unit: "Year", n: 118 }],
      ["distance", ["1234", "1234"], { unit: "Year", n: 0 }],
      ["distance", ["1234", "2345"], { unit: "Year", n: 1111 }],
      ["distance", ["1234-01", "1234-01"], { unit: "Month", n: 0 }],
      ["distance", ["1234-01", "1234-02"], { unit: "Month", n: 1 }],
      ["distance", ["1234-01", "1235-01"], { unit: "Month", n: 12 }],
      ["distance", ["-0700-06", "-0695-06"], { unit: "Month", n: 60 }],
      ["distance", ["-0700-06", "-0695-07"], { unit: "Month", n: 61 }],
      ["distance", ["-0000-01", "0001-01"], { unit: "Month", n: 12 }], // but -0000 is invalid
      ["distance", ["-0001-01", "0000-01"], { unit: "Month", n: 12 }],
      ["distance", ["1066-02-01", "1066-02-01"], { unit: "Day", n: 0 }],
      ["distance", ["1066-02-01", "1066-02-03"], { unit: "Day", n: 2 }],
      ["distance", ["1066-02-01", "1066-03-01"], { unit: "Day", n: 28 }],
      ["distance", ["1066-02-01", "1067-03-01"], { unit: "Day", n: 28 + 365 }],
      ["next", ["-0099-12-31"], "-0098-01-01"],
      ["next", ["-0009-02-28"], "-0009-03-01"],
      ["next", ["-0001-12-31"], "0000-01-01"],
      ["next", ["0099-12-31"], "0100-01-01"],
      ["next", ["1432-11-30"], "1432-12-01"],
      ["next", ["-0088-11"], "-0088-12"],
      ["next", ["-0088-12"], "-0087-01"],
      ["next", ["-0001-12"], "0000-01"],
      ["next", ["0000-01"], "0000-02"],
      ["next", ["1607-08"], "1607-09"],
      ["next", ["9998-12"], "9999-01"],
      ["next", ["-9999"], "-9998"],
      ["next", ["-0001"], "0000"],
      ["next", ["0000"], "0001"],
      ["next", ["9998"], "9999"],
      // ["next", ["9999"], undefined], // for worse or better this returns "10000"
      // THESE fail, but narrower/broader assume 0s & 0s BC -- how are they notated?
      // ["next", ["-001"], "-000"],
      // ["next", ["-000"], "000"],
      ["next", ["000"], "001"],
      ["next", ["-02"], "-01"],
      ["next", ["-01"], "00"],
      ["next", ["00"], "01"],
      ["next", ["01"], "02"],
      ["next", ["-2999/-2000"], "-1999/-1000"],
      ["next", ["-1999/-1000"], "-0999/0000"],
      ["next", ["-0999/0000"], "0001/1000"],
      ["next", ["0001/1000"], "1001/2000"],
      ["next", ["1001/2000"], "2001/3000"],
      ["broader", ["2023-05-13"], "2023-05"],
      ["broader", ["-0294-08-19"], "-0294-08"],
      ["broader", ["2023-05"], "2023"],
      ["broader", ["2023"], "202"],
      // FAILS with 000 but this is 1BC and that is 0's AD
      // ["broader", ["0000"], "-000"],
      ["broader", ["-0001"], "-000"],
      ["broader", ["202"], "20"],
      ["broader", ["000"], "00"],
      ["broader", ["-001"], "-01"],
      ["broader", ["-040"], "-05"],
      ["broader", ["01"], "0001/1000"],
      ["broader", ["07"], "0001/1000"],
      ["broader", ["19"], "1001/2000"],
      ["broader", ["20"], "2001/3000"],
      ["broader", ["-01"], "-0999/0000"],
      ["broader", ["-02"], "-0999/0000"],
      ["broader", ["-10"], "-0999/0000"],
      ["broader", ["-11"], "-1999/-1000"],
      ["broader", ["-19"], "-1999/-1000"],
      ["broader", ["-20"], "-1999/-1000"],
      ["broader", ["0001"], "000"],
      ["narrower", ["2001/3000"], "20 21 22 23 24 25 26 27 28 29".split(" ")],
      ["narrower", ["-0999/0000"], "-10 -09 -08 -07 -06 -05 -04 -03 -02 -01".split(" ")],
      ["narrower", ["20"], "200 201 202 203 204 205 206 207 208 209".split(" ")],
      ["narrower", ["-01"], "-009 -008 -007 -006 -005 -004 -003 -002 -001 -000".split(" ")],
      ["narrower", ["-08"], "-079 -078 -077 -076 -075 -074 -073 -072 -071 -070".split(" ")],
      ["narrower", ["001"], "0010 0011 0012 0013 0014 0015 0016 0017 0018 0019".split(" ")],
      ["narrower", ["202"], "2020 2021 2022 2023 2024 2025 2026 2027 2028 2029".split(" ")],
      ["narrower", ["-001"], "-0018 -0017 -0016 -0015 -0014 -0013 -0012 -0011 -0010 -0009".split(" ")],
      // SPECIAL CASES: “0-to-9” decades
      ["narrower", ["-000"], "-0008 -0007 -0006 -0005 -0004 -0003 -0002 -0001 0000".split(" ")],
      ["narrower", ["000"], "0001 0002 0003 0004 0005 0006 0007 0008 0009".split(" ")],
      ["narrower", ["0030"], "0030-01 0030-02 0030-03 0030-04 0030-05 0030-06 0030-07 0030-08 0030-09 0030-10 0030-11 0030-12".split(" ")],
      ["narrower", ["0000"], "0000-01 0000-02 0000-03 0000-04 0000-05 0000-06 0000-07 0000-08 0000-09 0000-10 0000-11 0000-12".split(" ")],
      ["narrower", ["0000-02"], "0000-02-01 0000-02-02 0000-02-03 0000-02-04 0000-02-05 0000-02-06 0000-02-07 0000-02-08 0000-02-09 0000-02-10 0000-02-11 0000-02-12 0000-02-13 0000-02-14 0000-02-15 0000-02-16 0000-02-17 0000-02-18 0000-02-19 0000-02-20 0000-02-21 0000-02-22 0000-02-23 0000-02-24 0000-02-25 0000-02-26 0000-02-27 0000-02-28 0000-02-29".split(" ")],
      ["narrower", ["-0003-02"], "-0003-02-01 -0003-02-02 -0003-02-03 -0003-02-04 -0003-02-05 -0003-02-06 -0003-02-07 -0003-02-08 -0003-02-09 -0003-02-10 -0003-02-11 -0003-02-12 -0003-02-13 -0003-02-14 -0003-02-15 -0003-02-16 -0003-02-17 -0003-02-18 -0003-02-19 -0003-02-20 -0003-02-21 -0003-02-22 -0003-02-23 -0003-02-24 -0003-02-25 -0003-02-26 -0003-02-27 -0003-02-28".split(" ")],
      ["coerce", ["00", "Year"], "0001"],
      // THIS fails with -0098 but it's close enough for positioning
      // ["coerce", ["-01", "Year"], "-0099"],
      // THIS fails with 2000 for a similar reason, the enclosures are irregular
      // ["coerce", ["2001/3000", "Year"], "2001"],
      ["coerce", ["12", "Year"], "1200"],
      ["coerce", ["12", "Year", { align: "start" }], "1200"],
      ["coerce", ["12", "Year", { align: "end" }], "1299"],
      ["coerce", ["123", "Year"], "1230"],
      ["coerce", ["123", "Year", { align: "end" }], "1239"],
      ["coerce", ["1234", "Year", { align: "end" }], "1234"],
      ["coerce", ["1656-02", "Day", {align: "end"}], "1656-02-29"],
      ["coerce", ["1234", "Decade"], "123"],
      // align has no effect when broadening
      ["coerce", ["1234", "Decade", { align: "end" }], "123"],
      ["coerce", ["1234", "Century"], "12"],
      ["coerce", ["12", "Decade"], "120"],
      ["coerce", ["2023", "Millennium"], "2001/3000"],
    ];
    const equals = (a, b) => a === b || JSON.stringify(a) === JSON.stringify(b);
    for (const [op, args, expect] of FACTS) {
      let got = ISO8601[op](...args);
      if (op === "recognize") got = got?.value;
      assert?.(
        equals(got, expect),
        `${op}(${args}) got ${JSON.stringify(got)} vs ${JSON.stringify(expect)}`
      );
      // all returned dates should parse
      // all dates should have labels
      // all labels should be recognized as the thing that was their label
      // ∀ x,y broader(x)=y → y∈narrower(x)
      if (op === "broader") {
        const [x] = args;
        const narr = ISO8601.narrower(got);
        assert?.(narr.includes(x), `${x} ∉ narr(${got})`);
      }
      // ∀ x,y,z compare(x, y)=z ↔ compare(y, x)=-z
      if (op === "compare") {
        const [x, y] = args;
        const inv = ISO8601.compare(y, x);
        assert?.(expect === -inv, `comp(${y},${x}) ≠ ${-inv}`);
      }
      // ∀ x,y distance(x,y)=d ↔ distance(y,x)=-d
      if (op === "distance") {
        const [x, y] = args;
        const d = expect;
        const inv = ISO8601.distance(y, x);
        assert?.(d.n === -inv.n, `dist(${y},${x}) ≠ ${JSON.stringify(inv)}`);
      }
      // TODO: when plus is implemented for more stuff
      // ∀ x,y: next(x)=y → plus(x, 1)=y
      // ∀ x,y: previous(x)=y → plus(x, -1)=y
      if (op === "next") {
        // ∀ x,y next(x)=y ↔ previous(y)=x (i.e. previous inverseOf next)
        {
          const [x] = args;
          const y = expect;
          const py = ISO8601.previous(y);
          assert?.(equals(py, x), `prev(${y}) = ${x}, got ${py}`);
        }
        // ∀ x,y next(x)=y → x<y
        {
          const [x] = args;
          const cmp = ISO8601.compare(x, got);
          assert?.(cmp < 0, `${x} ≮ ${got} = next(${x})`);
        }
      }
    }
  }

  const MONTH_NAME = new Intl.DateTimeFormat(undefined, {
    month: "long",
    timeZone: "UTC",
  });
  const DAY_LABEL = new Intl.DateTimeFormat(undefined, {
    year: "numeric",
    month: "short",
    day: "numeric",
    timeZone: "UTC",
  });

  const int = s => parseInt(s, 10);
  const pad = len => n =>
    (n < 0 ? "-" : "") + Math.abs(n).toString().padStart(len, "0");
  const pad2 = pad(2);
  const pad3 = pad(3);
  const pad4 = pad(4);
  const pad6 = pad(6);

  // see https://vocabs.acdh.oeaw.ac.at/unit_of_time/
  // and https://vocabs.acdh.oeaw.ac.at/date/
  const _TIME_UNITS = {
    Millisecond: { rank: 0 },
    Second: { rank: 1 },
    Minute: { rank: 2 },
    Hour: { rank: 3 },
    Day: { rank: 4 },
    Week: { rank: 5 },
    Month: { rank: 6 },
    Year: { rank: 7 },
    Decade: { rank: 8 },
    Century: { rank: 9 },
    Millennium: { rank: 10 },
  };
  // larger = broader
  function compare_units(a, b) {
    if (!(a in _TIME_UNITS)) throw BadRequest(`unknown unit ${a}`);
    if (!(b in _TIME_UNITS)) throw BadRequest(`unknown unit ${b}`);
    return _TIME_UNITS[a].rank - _TIME_UNITS[b].rank;
  }
  const EN_UNITS = Object.freeze({
    Millisecond: { singular: "millisecond", plural: "milliseconds" },
    Second: { singular: "second", plural: "seconds" },
    Minute: { singular: "minute", plural: "minutes" },
    Hour: { singular: "hour", plural: "hours" },
    Day: { singular: "day", plural: "days" },
    Week: { singular: "week", plural: "weeks" },
    Month: { singular: "month", plural: "months" },
    Year: { singular: "year", plural: "years" },
    Decade: { singular: "decade", plural: "decades" },
    Century: { singular: "century", plural: "centurys" },
    Millennium: { singular: "millennium", plural: "millenniums" },
  });
  function label_unit(unit, plural, lang) {
    if (lang !== "en") throw NotImplemented(`lang ${lang}`);
    const record = EN_UNITS[unit];
    if (!record) throw BadRequest(`unknown unit ${unit}`);
    return plural ? record.plural : record.singular;
  }

  // ====== I/O

  {
    const DATE =
      /^(?<day>(?<month>(?<year>(?<decade>(?<bc>-?)(?<century>\d\d)(?<d>\d)?)(?<y>\d)?)(?:-(?<mm>\d\d))?)(?:-(?<dd>\d\d))?)(T(?<time>(?<hh>\d\d(:(?<min>\d\d(:(?<sec>\d\d(\.(?<ms>\d{1,3}))?))?))?(?<zone>Z|[+-]\d{4})?)))?$/;
    const MILLENNIUM = /^((-?)\d{4,5})\/-?(\d0000?)$/;

    function parse(iso) {
      if (typeof iso !== "string") return;

      // e.g. 2nd millennium is 1001/2000
      const mill = iso.match(MILLENNIUM);
      if (mill) {
        const [, from, bc, to] = mill;
        const n = int(`${bc}${to}`);
        // and we know from pattern that “to” ends in 000
        if (n === int(from) + 999)
          // `millennium` is the ending year (so, 1000 for first millennium)
          return { input: iso, unit: "Millennium", bc, millennium: n };
      }

      // Um, for now millennia are special
      let end;
      if (iso.includes("/")) {
        const [from, to] = iso.split("/", 2);
        end = parse(to);
        iso = from; // rosary
      }

      const match = iso.match(DATE);
      if (!match) return;

      const { dd, mm, y, d, hh, min, sec, ms } = match.groups;
      // prettier-ignore
      const unit = ms ? "Millisecond"
          : sec ? "Second"
          : min ? "Minute"
          : hh ? "Hour"
          : dd ? "Day"
          : mm ? "Month"
          : y ? "Year"
          : d ? "Decade"
          : "Century"

      return { input: iso, unit, end, ...match.groups }; // HOTSPOT
    }

    // return date in the signed, six-digit format used by `Date`
    // ASSUMES the date is a well-formed date string (4-digit year, sign optional)
    // ASSUMES unit >= Year
    function _to_Date_ISO(date) {
      // allow for signed four-year dates, though those aren't recognized elsewhere yet
      if (date.startsWith("-") || date.startsWith("+"))
        return `${date.charAt(0)}00${date.slice(1)}`;
      return `+00${date}`;
    }

    // ASSUMES same as `_to_Date_ISO`
    function Date_from(date) {
      const js_iso = _to_Date_ISO(date);
      const ticks = Date.parse(js_iso);
      if (isNaN(ticks)) return;
      return new Date(ticks);
    }
  }

  // ====== English recognizing and labeling

  const languages = (function () {
    function make_recognizer(recognizers) {
      return function recognize(message) {
        for (const [pattern, transform] of recognizers) {
          const match = message.match(pattern);
          if (!match) continue;
          const value = transform(match, message);
          if (value === undefined) continue;
          // I'm sorry but I need to do this
          return { input: message, format: "iso8601", value, match };
        }
      };
    }
    // could make one giant regex w/distinctly named groups to avoid making substrings
    function make_recognize_all(recognizers) {
      return function* recognize_all(message) {
        let text = message;
        for (const [pattern, transform] of recognizers) {
          const match = message.match(pattern);
          if (!match) continue;
          const value = transform(match, message);
          if (value === undefined) continue;
          // I'm sorry but I need to do this
          yield { input: message, format: "iso8601", value, match };
          text = text.substring(match.index + match.length);
        }
      };
    }

    const ORDINAL_SUFFIXES_EN = { 1: "st", 2: "nd", 3: "rd" };
    const ORDINALS_EN =
      "zeroth first second third fourth fifth sixth seventh eighth ninth tenth eleventh twelfth thirteenth fourteenth fifteenth sixteenth seventeenth eighteenth ninteenth twentieth".split(
        " "
      );
    const ordinal_en = z => {
      const n = Math.abs(z);
      const c = n % 100;
      const suffix =
        c >= 11 && c <= 13 ? "th" : ORDINAL_SUFFIXES_EN[n % 10] ?? "th";
      return `${n}${suffix}`;
    };

    function read_number_en(text) {
      const int = parseInt(text, 10);
      if (!isNaN(int)) return int;
      const lower = text.toLowerCase();
      const index = ORDINALS_EN.indexOf(lower);
      if (index !== -1) return index;
      return NaN;
    }

    const RECOGNIZERS_EN = [
      [{ [Symbol.match]: parse }, parsed => parsed.input],
      // Roman numerals...
      // even with caps this will overlap with term searches
      [/^M+$/, ([s]) => millennium(s.length)],
      // ranges...
      [
        /(?<from>-?\s*\d+)[ths]*\s*(c\w*)?\s*(-|to|through|thru|\.\.+)\s*(?<to>-?\s*\d+)\s*(c\w*)?/i,
        ({ groups: { from, to } }, text) => {
          if (!text.toLowerCase().includes("c")) return;
          const first = int(from);
          const second = int(to);

          if (first > 10 && second < 10)
            // e.g. 15-6c
            // prettier-ignore
            return `${pad2(first - 1)}/${pad2(first - (first % 10) + second - 1)}`;

          return `${pad2(first - 1)}/${pad2(second - 1)}`;
        },
      ],
      [
        /(?<from>-?\s*\d+)\s*(['"]*s)?\s*.*\b(-|to|through|thru|\.\.+)[-\D]*(?<to>-?\s*\d+)\s*(['"]*s)?/,
        (match, text) => {
          const { from, to } = match.groups;
          if (!text.toLowerCase().includes("s")) return;
          const first = int(from);
          const second = int(to);

          if (first > second) {
            if (first >= 100 && second < 100) {
              const yy = first % 100;
              if (yy < second) {
                // e.g. 1640-50's
                // prettier-ignore
                return `${pad3(first / 10)}/${pad3((first - yy + second) / 10)}`;
              }

              if (second < 10)
                // e.g. 1690s-2's → 1690s -1720s
                return `${pad3(first)}/${pad3(first - yy + 100 + second * 10)}`;
            }
            // this makes no sense though
            return `${pad3(second)}/${pad3(first)}`;
          }

          return `${pad3(first)}/${pad3(second)}`;
        },
      ],
      [
        /(?<from>-?\s*\d+)\s*.*(-|to|through|thru|\.\.+).*(?<to>-?\s*\d+)/,
        ({ groups: { from, to } }) => {
          const first = int(from);
          const second = int(to);

          if (first > second) {
            if (first >= 100 && second < 100) {
              if (second < 10)
                // e.g. 1941-5
                return `${pad4(first)}/${pad4(first - (first % 10) + second)}`;
              // e.g. 580-90
              return `${pad4(first)}/${pad4(first - (first % 100) + second)}`;
            }
            return `${pad4(second)}/${pad4(first)}`;
          }
          return `${pad4(first)}/${pad4(second)}`;
        },
      ],
      [
        /^\s*-?(first|second|third|fourth|fifth|sixth|seventh|\d?1st|\d?2nd|\d?3rd|\d?[04-9]th|\d{1,2})\s*c+(e(n(t(u(r(y)?)?)?)?)?)?\s*(b+c*e*)?$/i,
        match => {
          const [, specifier] = match;
          // shouldn't this be match.text?
          const bc = match.includes("-") || match.includes("b");
          const n = read_number_en(specifier);
          const cc = bc ? n : n - 1;
          return `${bc ? "-" : ""}${pad2(cc)}`;
        },
      ],
      [
        /^\s*-?(first|second|third|fourth|fifth|sixth|seventh|\d?1st|\d?2nd|\d?3rd|\d?[04-9]th|\d{1,2})\s*m+(i(l(l(e(n(i(u(m)?)?)?)?)?)?)?)?\s*(b+c*e*)?$/i,
        match => {
          const [text, specifier] = match;
          const bc = text.includes("-") || text.includes("b");
          const number = read_number_en(specifier);
          if (isNaN(number)) return;
          const n = number === 0 ? 1 : number; // there is no 0th millennium; treat as 1st
          if (bc) return `${pad4(-n * 1000 + 1)}/${pad4(-(n - 1) * 1000)}`;
          return `${pad4((n - 1) * 1000 + 1)}/${pad4(n * 1000)}`;
        },
      ],
      [
        /^\s*-?\d{1,4}'?s\s*(b+c*e*)?$/,
        message => {
          const bc = message.includes("-") || message.includes("b");
          const n = int(message);
          const year = bc && n > 0 ? -n : n;
          const decade = Math.floor(year / 10);
          return `${bc ? "-" : ""}${pad3(decade)}`;
        },
      ],
      [
        /^\s*-?\d{1,4}\s*(b+c*e*)?$/,
        message => {
          const bc = message.includes("-") || message.includes("b");
          const n = int(message);
          const year = bc && n > 0 ? -n : n;
          return `${bc ? "-" : ""}${pad4(year)}`;
        },
      ],
    ];

    function label_en(iso, simple) {
      const parsed = parse(iso);
      assert?.(parsed, `label en got bad date ${iso}`);
      // millennia is a special case of ranges which has its own unit handling
      if (!simple && iso.includes("/") && parsed.unit !== "Millennium") {
        const [lo, hi] = iso.split("/", 2);
        // TBD dependent ranges, e.g. 1920's – 40's, or 1st – 2nd millennia
        // else label independently (which note is not language specific)
        return `${label_en(lo, true)} – ${label_en(hi, true)}`;
      }
      const { unit, bc } = parsed;
      const maybe_bc = bc ? " BC" : ""; // yeah yeah BCE
      switch (unit) {
        case "Millennium": {
          const m = parsed.millennium / 1000;
          const mm = bc ? 1 - m : m;
          return `${ordinal_en(mm)} millennium${maybe_bc}`;
        }
        case "Century": {
          const c = Math.abs(int(parsed.century)) + (bc ? 0 : 1);
          return `${ordinal_en(c)} century${maybe_bc}`;
        }
        case "Decade":
          // maybe add (decade) for first decade of a century
          return `${Math.abs(int(parsed.decade)) * 10}’s${maybe_bc}`;

        case "Year": {
          const year = int(parsed.year);
          // could also put AD or CE for small numbers
          return year > 0 ? `${year}` : `${Math.abs(year) + 1} BC`;
        }
        case "Month": {
          const year_label = label_en(parsed.year);
          const iso = bc ? parsed.input.slice(1) : parsed.input;
          const ticks = Date.parse(iso);
          const first = new Date(ticks);
          const month_name = MONTH_NAME.format(first);
          return `${month_name} ${year_label}`;
        }
        case "Day": {
          const js_iso = _to_Date_ISO(iso);
          const ticks = Date.parse(js_iso);
          return `${DAY_LABEL.format(ticks)}${maybe_bc}`;
        }
      }
      // console.warn(`Not implemented, labeling ${unit}`);
      return iso;
    }

    const en_recognizer = make_recognizer(RECOGNIZERS_EN);

    function recognize(text, lang = "en") {
      if (lang === "en") return en_recognizer(text);
      console.info(`Not implemented ${lang}`);
    }

    function label(iso, lang = "en") {
      if (lang === "en") return label_en(iso);
      console.warn(`Language ‘${lang}’ is not supported for ISO labeling`);
      return iso; // fallback to ISO
    }

    return { recognize, label };
  })();

  const range = (n, f) => {
    const ret = Array(n);
    for (let i = 0; i < n; i++) ret[i] = f(i);
    return ret;
  };

  // inclusive --- though we generally want end exclusive
  const between = (date, a, b) =>
    compare(a, date) <= 0 && compare(b, date) >= 0;

  // >0 means sort a after b
  // <0 means sort a before b
  function compare(a, b) {
    if (a === b) return 0;
    if (a.startsWith("-")) {
      if (b.startsWith("-")) {
        // in BC comparisons, years (and *only* years) are reversed
        const a_year = a.slice(0, 5); // allocation
        // do a within-year comparison... and more allocation
        if (b.startsWith(a_year)) return a.slice(5) < b.slice(5) ? -1 : 1;
        return a < b ? 1 : -1;
      }
      return -1;
    }
    if (b.startsWith("-")) return 1;
    return a < b ? -1 : 1;
  }

  const get_c = parsed => int(`${parsed.bc}${parsed.century}`);
  const get_m = (parsed, c = get_c(parsed)) => Math.floor(c / 10) + 1;
  const m2 = e => `${pad4(e - 999)}/${pad4(e)}`;
  const millennium = m => m2(m * 1000);

  // ====== sequence: next & previous

  function next(date) {
    const the = parse(date);
    assert?.(the, `next got bad date`, date);
    switch (the.unit) {
      case "Millennium":
        return m2(the.millennium + 1000);
      case "Century":
        return pad2(get_c(the) + 1);
      case "Decade":
        return pad3(int(the.decade) + 1);
      case "Year":
        return pad4(int(the.year) + 1);
      case "Month":
        if (the.mm === "12") return `${next(the.year)}-01`;
        return `${the.year}-${pad2(int(the.mm) + 1)}`;
      case "Day": {
        const js_date = Date_from(date);
        js_date.setUTCDate(js_date.getUTCDate() + 1);
        return _Date_to_Day(js_date);
      }
    }
    throw NotImplemented(`next ${the.unit}`);
  }

  function _Date_to_Day(js_date) {
    const js_iso = js_date.toISOString();
    if (js_iso.startsWith("-") || js_iso.startsWith("+"))
      return `${js_iso.charAt(0)}${js_iso.substring(3, 13)}`;
    return js_iso.slice(0, 10);
  }

  function previous(date) {
    const the = parse(date);
    assert?.(the, `previous got bad date`, date);
    switch (the.unit) {
      case "Millennium":
        return m2(the.millennium - 1000);
      case "Century":
        return pad2(get_c(the) - 1);
      case "Decade":
        return pad3(int(the.decade) - 1);
      case "Year":
        return pad4(int(the.year) - 1);
      case "Month":
        if (the.mm === "01") return `${previous(the.year)}-12`;
        return `${the.year}-${pad2(int(the.mm) - 1)}`;
      case "Day": {
        // Same as `next` except `-1` instead of `+1`
        const js_date = Date_from(date);
        js_date.setUTCDate(js_date.getUTCDate() - 1);
        return _Date_to_Day(js_date);
      }
    }
    throw NotImplemented(`previous ${the.unit}`);
  }
  // ====== Taxonomical: broader & narrower

  function broader(iso) {
    const parsed = parse(iso);
    if (!parsed) return;
    switch (parsed.unit) {
      case "Century":
        return millennium(get_m(parsed));
      case "Decade":
        return pad2(parsed.bc ? -1 - int(parsed.century) : int(parsed.century));
      case "Year":
        return parsed.decade;
      case "Month":
        return parsed.year;
      case "Day":
        return parsed.month;
    }
  }

  function narrower(iso) {
    const parsed = parse(iso);
    if (!parsed) return undefined;
    switch (parsed.unit) {
      case "Millennium" /* → Centuries */: {
        const n = int(parsed.millennium) / 1000;
        if (n !== Math.floor(n)) throw BadRequest(`BAD MILLENNIUM ${iso}`);
        return range(10, i => pad2((n - 1) * 10 + i + (parsed.bc ? 0 : 0)));
      }
      case "Century" /* → Decades */: {
        const { bc, century } = parsed;
        const n = int(century);
        return range(10, i => `${bc}${pad3(10 * n + (bc ? -1 - i : i))}`);
      }
      case "Decade" /* → Years */: {
        if (parsed.input === "000") return range(9, i => pad4(i + 1)); // 0s
        if (parsed.input === "-000") return range(9, i => pad4(i - 8)); // 0s BC
        if (parsed.bc) {
          const n = int(parsed.decade);
          return range(10, i => pad4(10 * n - 8 + i));
        }
        return range(10, i => `${parsed.input}${i}`);
      }
      case "Year" /* → Months */:
        return range(12, i => `${parsed.input}-${pad2(i + 1)}`);

      case "Month" /* → Days */: {
        const ret = [];
        const year = int(parsed.year);
        const js_date = new Date(Date.UTC(year, int(parsed.mm) - 1));
        js_date.setUTCFullYear(year); // account for Date.UTC's quirk re 1900's
        const month = js_date.getUTCMonth();
        while (month === js_date.getUTCMonth()) {
          const day = js_date.getUTCDate();
          ret.push(`${parsed.month}-${pad2(day)}`);
          js_date.setUTCDate(day + 1);
        }
        return ret;
      }
    }
  }

  // returns {unit, n}.  is `n` an integer then?
  const distance = (iso_from, iso_to) => {
    const a = parse(iso_from);
    const b = parse(iso_to);
    const unit = a?.unit;
    if (unit !== b?.unit) throw NotImplemented(`${iso_to}-${iso_from}`);

    // A surer way to maintain this property
    // as it is, the below must deal with a ≶ b
    // if (compare(iso_from, iso_to) > 0)
    //   return { unit, n: -distance(iso_to, iso_from).n };

    switch (unit) {
      case "Millennium":
        return { unit, n: int(b.millennium) / 1e3 - int(a.millennium) / 1e3 };
      case "Century": {
        const aa = get_c(a);
        const bb = get_c(b);
        if (aa < 0 && bb > 0) return { unit, n: bb - aa - 1 };
        if (bb < 0 && aa > 0) return { unit, n: bb - aa + 1 };
        return { unit, n: bb - aa };
      }
      case "Decade":
        return { unit, n: int(b.decade) - int(a.decade) };
      case "Year":
        return { unit, n: int(b.year) - int(a.year) };
      case "Month": {
        const years = int(b.year) - int(a.year);
        return { unit, n: years * 12 + int(b.mm) - int(a.mm) };
      }
      case "Day": {
        const a_ticks = Date_from(a.input).getTime();
        const b_ticks = Date_from(b.input).getTime();
        return { unit, n: Math.floor((b_ticks - a_ticks) / TICKS_PER_DAY) };
      }
    }
    throw NotImplemented(`Distance of ${unit}`);
  };

  // PRELIMINARY
  function plus(date, units, in_unit) {
    const parsed = parse(date);
    if (!parsed) throw BadRequest(`plus got bad date`);
    const unit = in_unit ?? parsed.unit;
    if (units !== Math.floor(units)) throw BadRequest(`+non-integer ${units}`);
    if (unit !== parsed.unit)
      throw NotImplemented(`+${unit} for ${parsed.unit}`);

    switch (unit) {
      case "Day": {
        const js_date = Date_from(date);
        js_date.setUTCDate(js_date.getUTCDate() + units);
        return _Date_to_Day(js_date);
      }
    }
    throw NotImplemented(`+${unit}`);
  }

  // return a date with `unit` precision related to date
  // if unit is less specific, then take the broader containing
  // if unit is more specific, then align start (i.e. take first narrower)
  // KNOWN ISSUE: gets first year of last decade when end-aligning from century to year
  function coerce(date, unit, options) {
    const align = options?.align ?? "start";
    assert?.(align === "start" || align === "end", `bad align ${align}`);
    const parsed = parse(date);
    if (!parsed) throw BadRequest(`Coerce got bad date ${date}`);
    if (parsed.unit === unit) return date;
    if (compare_units(parsed.unit, unit) < 0)
      return coerce(broader(date), unit, options);
    const parts = narrower(date);
    const index = align === "start" ? 0 : -1;
    const part = parts.at(index);
    if (!part) throw Error(`Failed coercing ${date} to ${unit}`);
    return coerce(part, unit, options);
  }

  const TICKS_PER_DAY = 24 * 60 * 60 * 1000;
  function day_in_year(date) {
    const js_date = Date_from(date);
    const year = js_date.getUTCFullYear();
    // Unfortunately,
    //     new Date(Date.UTC(0, 0, 1)).toISOString()
    //     "1900-01-01T00:00:00.000Z"
    // even though
    //     new Date(Date.UTC(-1, 0, 1)).toISOString()
    //     "-000001-01-01T00:00:00.000Z"
    // const first_of_year = new Date(Date.UTC(year, 0, 1));
    // TODO: does this need +1 for BC?
    const first_of_year = Date_from(`${pad4(year)}-01-01`);
    const start_ticks = first_of_year.getTime();
    const end_ticks = js_date.getTime();
    const delta_ticks = end_ticks - start_ticks;
    const days = delta_ticks / TICKS_PER_DAY;

    const day = Math.round(days);
    return day;
  }
  // heavy... we could get close enough leap year with modulos
  // yes: https://tc39.es/ecma262/multipage/numbers-and-dates.html#sec-daysinyear
  function days_in_year(date) {
    const parsed = parse(date);
    const year_end = `${parsed.year}-12-31`; // ≍ coerce(date, "Day", {align: "end"})
    return day_in_year(year_end) + 1;
  }

  function unit_of(date) {
    const parsed = parse(date);
    if (!parsed) throw BadRequest(`bad date ${date}`);
    return parsed.unit;
  }

  const ord = { compare, between, distance, plus }; // poset & metric space
  const seq = { next, previous };
  const skos = { broader, narrower };
  const units = { unit_of, compare_units, label_unit };
  const calendar = { days_in_year, day_in_year }; // scalar support
  const other = { ...units, self_tests, coerce, ...calendar };
  const ISO8601 = { ...ord, ...seq, ...skos, ...languages, ...other };
  // Object.assign(globalThis, { ISO8601 });
  return ISO8601;
});

define("timelab:major-scale", ["~/methods/map/object", "timelab:ISO8601"], (
  map_object,
  ISO8601
) => {
  // === major time scale, aka counted time
  // The base unit is year.  We recognize to day precision.
  // The multiplier is nothing but a CSS hack.
  // Firefox in particular runs out of floating point space in css vars at Day level
  //
  // Scalar year values:
  // 1 means Jan 1, 1 AD
  // 1.5 means the middle of 1 AD (~July 1 AD)
  // 0 means Jan 1, 1 BC
  // 0.5 means... ~July, 1 BC
  // -1 means Jan 1, 2 BC
  // -0.5 means ~July, 2 BC

  const YEARS_PER_MAJOR = 100;
  const { assert } = console;

  const YEAR_MULTIPLIERS = {
    Millennium: 1000,
    Century: 100,
    Decade: 10,
    Year: 1,
    Month: 1 / 12,
    Day: 1 / 365, // this is for a rule of thumb, so edge case is immaterial
  };

  const int = s => parseInt(s, 10);
  const { coerce, unit_of, compare_units, day_in_year, days_in_year } = ISO8601;

  const years_to_major = x => x * YEARS_PER_MAJOR;

  // lot of redundant work here...
  const _year_scalar_from = date => {
    const unit = unit_of(date);
    let year_text = date;
    if (compare_units(unit, "Year") > 0) year_text = coerce(date, "Year");
    const year = int(year_text); // this happens to work for  years
    switch (unit) {
      case "Month":
      case "Day": {
        const day = day_in_year(date);
        const days = days_in_year(date);
        const fraction = day / days;
        return year + fraction;
      }
    }
    return year;
  };
  function major_scalar_from(date) {
    return _year_scalar_from(date) * YEARS_PER_MAJOR;
  }

  // convert from a major scalar value to the day that contains it
  // THIS should roundtrip dates with `major_scalar_from` but doesn't always
  const pad = len => n =>
    (n < 0 ? "-" : "") + Math.abs(n).toString().padStart(len, "0");
  const pad2 = pad(2);
  const pad4 = pad(4);

  const { floor, abs } = Math;
  const { UTC } = Date;

  function major_scalar_to_day(major) {
    // we still have precision to spare here
    const years = major / YEARS_PER_MAJOR;
    const year = floor(years);
    const signed_yyyy = pad4(year < 1 ? year + 1 : year);
    const fraction = abs(years) % 1;
    const year_days = days_in_year(signed_yyyy);
    const day_ratio = 1 / year_days;
    const days = floor(fraction / day_ratio);
    const js_date = new Date(UTC(year, /*month*/ 0, days + 1));
    js_date.setUTCFullYear(year); // account for Date.UTC's quirk re 1900's
    const month_number = js_date.getUTCMonth() + 1;
    const day_number = js_date.getUTCDate();
    return `${signed_yyyy}-${pad2(month_number)}-${pad2(day_number)}`;
  }

  // find a date given 2 scalars
  // defines an equivalence class between the continuous major scale
  // and the ISO dates we recognize as timeframe
  // completely partitions the space such that the named dates fall fully into view
  // result always has either a single date or a date range with same units
  // we find the smallest unit that has “not too many”
  const { compare, previous, next } = ISO8601;
  const { assign } = Object;
  const FACTORS = Object.entries(map_object(YEAR_MULTIPLIERS, years_to_major));

  function major_scalars_to_timeframe(from, to) {
    const size = to - from;
    if (size === 0) return;
    const from_day = major_scalar_to_day(from);
    const to_day = major_scalar_to_day(to);

    // you must *fully* encompass at least one unit, or keep looking
    for (const [unit, major] of FACTORS) {
      const partial = major > size;
      if (partial && unit !== "Day") continue;
      const container = coerce(from_day, unit);
      const boundary = from === major_scalar_from(container);
      const first = boundary ? container : next(container);
      if (partial) return first;

      const ends_in = coerce(to_day, unit);
      if (unit === "Day" && first === ends_in) return first;
      const last = previous(ends_in);
      const comparison = compare(first, last);
      if (comparison > 0) continue;
      if (comparison === 0) return first;
      return `${first}/${last}`;
    }
  }
  function test_major_scalars_to_timeframe() {
    const FACTS = [
      [[123400, 123400], undefined], // division by zero
      [[123400, 123400.001], "1234-01-01"], // day is bottom
      [[123400, 123500], "1234"], // end exclusive
      [[123400, 123510], "1234"], // end extension
      [[123400, 123599], "1234"], // up to
      [[123400, 123499], "1234-01/1234-11"], // span multiple
      [[123400, 123600], "1234/1235"], // but not including next
      [[123390, 123510], "1234"], // start & end extension
      [[123410, 123510], "1234-03/1235-01"], // refinement
      [[123410, 123510.00001], "1234-03/1235-01"], // distance, not precision
      [[123400, 123499.9], "1234-01/1234-11"],
      [[123410, 123600], "1235"], // largest contained unit
      [[123410, 123610], "1235"], // right?
      [[123390, 123610], "1234/1235"],
      [[123400, 124400], "1234/1243"],
      [[123000, 124000], "123"],
      [[123000, 124100], "123"],
      [[123000, 124140], "123"],
      [[123000, 124150], "123"],
      [[123000, 133000], "123/132"],
      [[123000.01, 133000], "124/132"],
      [[120000, 130000], "12"],
      [[120000, 130100], "12"],
      [[119900, 130100], "12"],
      [[120100, 130100], "121/129"],
      [[158289.67465753408, 158289.98287671216], "1582-11-25"],
      // BC cases...
    ];
    for (const [[from, to], expect] of FACTS) {
      const got = major_scalars_to_timeframe(from, to);
      assert(got === expect, `[${from}, ${to})→${expect} got ${got}`);
    }
  }
  const api = { self_tests: test_major_scalars_to_timeframe };
  assign(major_scalars_to_timeframe, api);

  return { major_scalars_to_timeframe, major_scalar_from, years_to_major };
});

define("timelab:minor-scale", [], () => {
  // === minor time scale, a.k.a. measured time
  // base unit is Unix timestamp
  // adjustment is needed for Firefox CSS, which loses precision
  // see `./firefox-css-precision-problem.html`
  const TICKS_BASE = new Date().getTime();
  const { parse } = Date;
  const { BadRequest } = def.errors;
  function ticks_from(date) {
    if (date instanceof Date) return date.getTime();
    if (typeof date === "string") {
      const ticks = parse(date);
      if (!isNaN(ticks)) return ticks;
    }
    throw BadRequest(`couldn't get ticks from ${date}`);
  }
  function minor_scalar_from(date) {
    return ticks_from(date) - TICKS_BASE;
  }
  return { minor_scalar_from };
});

// ==== application catalog

define("timelab:catalog", [], () => ({
  en_us_constitutional_amendments: {
    locale: "en",
    source: "wiki-us-amendments-en.js",
    xml_name: "XML_us_amendments",
    store: {
      keyPath: "number",
      indexes: {
        wiki: { keyPath: "wiki" },
        refs: { keyPath: "refs", multiEntry: true },
        proposed: { keyPath: "proposed" },
        completed: { keyPath: "completed" },
      },
    },
    from_dom(/** @type Element */ dom) {
      const eles = dom.querySelectorAll("us-constitution > amendment");
      return Array.from(eles, ele => {
        const p = ele.querySelector("description");
        console.assert(p, `No content element for item`, ele);
        return {
          wiki: ele.getAttribute("wiki"),
          number: parseInt(ele.getAttribute("number"), 10),
          proposed: ele.getAttribute("proposed"),
          completed: ele.getAttribute("completed"),
          refs: Array.from(p.querySelectorAll("a[wiki]"), _ =>
            _.getAttribute("wiki")
          ),
          description: p.innerHTML,
        };
      });
    },
  },
  en_us_presidents: {
    locale: "en",
    source: "wiki-us-presidents-en.js",
    xml_name: "XML_us_presidents",
    store: {
      keyPath: "number",
      indexes: {
        incumbent: { keyPath: "incumbent" },
        termStart: { keyPath: "termStart" },
        termEnd: { keyPath: "termEnd" },
        electionDates: { keyPath: "electionDates", multiEntry: true },
        party: { keyPath: "party" },
      },
    },
    from_dom(/** @type Element */ dom) {
      const eles = dom.querySelectorAll("us-presidents > term");
      return Array.from(eles, ele => {
        return {
          number: parseInt(ele.getAttribute("number"), 10),
          incumbent: ele.getAttribute("incumbent"),
          termStart: ele.getAttribute("term-start"),
          termEnd: ele.getAttribute("term-end"),
          portrait: ele.getAttribute("portrait"),
          party: ele.getAttribute("party"),
          // in addition to elections because we can index on this
          electionDates: Array.from(ele.querySelectorAll("election"), node =>
            node.getAttribute("dates")
          ),
          elections: Array.from(ele.querySelectorAll("election"), node => {
            const ref = node.getAttribute("ref");
            const date = node.getAttribute("dates");
            return { ref, date };
          }),
        };
      });
    },
  },
  en_wiki_events: {
    locale: "en",
    source: "wiki-events-en.js",
    xml_name: "XML_events",
    store: {
      keyPath: "id",
      autoIncrement: true,
      indexes: {
        aboutTime: { keyPath: "aboutTime" },
        topics: { keyPath: "topics", multiEntry: true },
        refs: { keyPath: "refs", multiEntry: true },
      },
    },
    // This should be done lazily too, there's no need to make all at once
    from_dom(/** @type Element */ dom) {
      const eles = dom.querySelectorAll("events > event");
      return Array.from(eles, ele => {
        const p = ele.querySelector("p");
        console.assert(p, `No content element for item`, ele);
        return {
          aboutTime: ele.getAttribute("date"),
          topics: Array.from(ele.querySelectorAll("topic"), _ => _.textContent),
          refs: Array.from(p.querySelectorAll("a[wiki]"), _ =>
            _.getAttribute("wiki")
          ),
          html: p.innerHTML,
        };
      });
    },
  },
  en_wiki_persons: {
    locale: "en",
    source: "wiki-persons-en.js",
    xml_name: "XML_persons",
    store: {
      keyPath: "wiki",
      indexes: {
        born: { keyPath: "born", unique: false },
        died: { keyPath: "died", unique: false },
        // "words", "words", { multiEntry: true }
      },
    },
    from_dom(/** @type Element */ dom) {
      const eles = dom.querySelectorAll("persons > person");
      return Array.from(eles, ele => ({
        wiki: ele.getAttribute("who"),
        summary: ele.getAttribute("summary"),
        born: ele.getAttribute("born"),
        died: ele.getAttribute("died"),
      }));
    },
  },
}));

define("timelab:methods", ["timelab:bespoke_renders"], render => {
  const METHODS = {
    get_id: {
      en_wiki_persons: record => `wiki:${record.wiki}`,
      en_wiki_events: record => `en_wiki_event:${record.id}`,
      en_us_constitutional_amendments: record => `wiki:${record.wiki}`,
      en_us_states: record => `us-state:${record.name}`, // need wiki or fips
      en_us_presidents: record => `us-president:Term${record.number}`,
    },
    // iterator would be more general but more expensive huh?
    date_fields: {
      en_wiki_persons: ["born", "died"],
      en_wiki_events: ["aboutTime"],
      en_us_constitutional_amendments: ["proposed", "completed"],
      en_us_states: ["joined"],
      en_us_presidents: [
        "termStart",
        "termEnd",
        "electionDates" /*which is multi*/,
      ],
    },
    regex_test: {
      en_wiki_persons: (rex, person) =>
        rex.test(person.wiki) || rex.test(person.summary),
      en_wiki_events: (rex, event) =>
        event.topics?.some(it => rex.test(it)) || rex.test(event.html),
      en_us_constitutional_amendments: (rex, amendment) =>
        rex.test(amendment.wiki) ||
        rex.test(amendment.number.toString()) || // title won't since numerals are spelled out
        rex.test(amendment.description /*which is html*/),
      en_us_states: (rex, state) => rex.test(state.name),
      en_us_presidents: (rex, term) =>
        rex.test(term.incumbent) || rex.test(term.party),
    },
    make_scans: {
      // the date bits of this should be superseded by `date_scan_segments`
      *en_wiki_persons({ date, ref }) {
        if (typeof ref === "string") {
          // supersedes date.  will have 0 or 1 results
          yield { query: IDBKeyRange.only(ref) };
        } else if (typeof date === "string") {
          // 2 dates: list births after the min and deaths before the end
          const [lower, upper] = normalize(date);
          const query = safe_date_bounds(lower, upper);
          yield { index: "born", query, dir: "next" };
          yield { index: "died", query, dir: "prev" };
        } else {
          yield { index: "born", dir: "next" };
          yield { index: "died", dir: "prev" };
        }
      },
      *en_wiki_events({ date, ref }) {
        // IGNORES date filter when a ref is present
        if (typeof ref === "string") {
          yield { index: "refs", query: IDBKeyRange.only(ref) };
        } else if (typeof date === "string") {
          const [from, to] = normalize(date);
          const query = safe_date_bounds(from, to);
          yield { index: "aboutTime", dir: "next", query };
        } else {
          yield { index: "aboutTime", dir: "next" };
        }
      },
    },
    render_into: {
      en_wiki_persons: render.render_person_into,
      en_wiki_events: render.render_event_into,
      en_us_constitutional_amendments:
        render.render_us_constitutional_amendment_into,
      en_us_states: render.render_us_state_into,
      en_us_presidents: render.render_us_president_into,
    },
  };
  return METHODS;
});

define("timelab:wikitools", [], () => {
  const wiki_href = (wiki, lang) =>
    `https://${lang}.wikipedia.org/wiki/${wiki}`;
  const wiki_unslug = s => s.replace(/_/g, " ");
  return { wiki_href, wiki_unslug };
});

define("timelab:make_timestamp", ["timelab:ISO8601"], ISO8601 => {
  return function make_timestamp(date, lang, label) {
    const time = document.createElement("time");
    time.dateTime = date;
    time.textContent = label ?? ISO8601.label(date, lang);
    return time;
  };
});

define("timelab:timestamp_element", [
  "timelab:minor-scale",
  "timelab:major-scale",
  "timelab:ISO8601",
], (
  { minor_scalar_from },
  { major_scalar_from },
  { unit_of, compare_units }
) => {
  const { assert } = console;
  const is_major = date => {
    const unit = unit_of(date);
    return compare_units(unit, "Day") >= 0;
  };

  return function timestamp_element(element, when, until) {
    if (!when) when = new Date().toISOString();
    const major = is_major(when);
    element.setAttribute("data-time", when);
    const measure = major ? major_scalar_from : minor_scalar_from;
    element.style.setProperty("--time", measure(when));
    if (until) {
      assert(major === is_major(until), `minor/major`);
      element.setAttribute("data-time-to", until);
      element.style.setProperty("--time-to", measure(until));
    }
  };
});

define("timelab:bespoke_renders", [
  "timelab:ISO8601",
  "timelab:wikitools",
  "timelab:timestamp_element",
], (ISO8601, wikitools, timestamp) => {
  const { wiki_href, wiki_unslug } = wikitools;
  const { assert } = console;

  function render_person_into(person, /** @type Element */ element, rex) {
    const lang = "en";
    const doc = element.ownerDocument;
    const { wiki, summary, born, died } = person;
    const { dataset, style } = element;

    element.setAttribute("data-wiki", wiki);

    const time = document.createElement("time");
    {
      const born_label = born && ISO8601.label(born);
      const died_label = died && ISO8601.label(died);
      const dates = ` (${born_label ?? ""}${died ? ` – ${died_label}` : ""})`;
      const iso = born && died ? `${born}/${died}` : born || died;
      time.classList.add("dates");
      time.dateTime = iso;
      time.textContent = dates;
    }

    const heading = doc.createElement("a");
    {
      console.assert?.(wiki, `who has no wiki?`, person);
      heading.href = wiki_href(wiki, lang);
      heading.classList.add("title");
      // DISABLED for the moment, see note to render_event_into
      // heading.append(...match_and_mark_text(rex, wiki, wiki_unslug, doc));
    }

    const comment = doc.createElement("span");
    {
      comment.classList.add("summary");
      // comment.append(...match_and_mark_text(rex, summary, null, doc));
    }

    element.append(heading, " ", time, " ", comment);
    return element;
  }

  function render_event_into(event, /** @type Element */ element, rex) {
    const lang = "en";
    const doc = element.ownerDocument;
    const { aboutTime, topics, refs, html } = event;

    const time = make_timestamp(aboutTime, lang);
    const description = doc.createElement("blockquote");
    description.innerHTML = html; // hotspot for sure
    // marking up the html is more complicated
    // define would be contagious here, anyway I don't really want bespoke renderers per type
    // match_and_mark_dom(rex, description, doc);
    for (const wiki_ele of description.querySelectorAll("[wiki]")) {
      const wiki = wiki_ele.getAttribute("wiki");
      wiki_ele.href = wiki_href(wiki, lang);
    }

    const citation = doc.createElement("cite");
    const source_link = doc.createElement("a");
    // event SHOULD have a source reference (aka provenance), but in the present
    // catalog (timeline articles) title is *nearly* redundant with this date.
    let date = aboutTime;
    // There are no timeline articles more specific than year
    if (ISO8601.compare_units(ISO8601.unit_of(date), "Year") < 0)
      date = ISO8601.coerce(date, "Year");
    const wiki = hack_en_wiki_iso_to_timeline(date);
    const href = wiki_href(wiki, lang);
    const title = doc.createElement("q");
    title.textContent = wiki_unslug(wiki);
    source_link.href = href;
    source_link.append(`from `, title, ` in English Wikipedia`);
    citation.append(source_link);
    description.cite = href; // does this do anything anywhere?

    let topic_list;
    if (topics?.length > 0) {
      topic_list = doc.createElement("div");
      topic_list.classList.add("topics");
      for (const topic of topics) {
        const item = doc.createElement("span");
        item.setAttribute("data-topic", topic);
        item.textContent = topic;
        topic_list.append(item, " ");
      }
    }

    // We don't want to do this for backdrops because it's redundant
    // getting that preference here is another matter
    const include_time = false;
    if (include_time) element.append(time, " ");
    element.append(description, citation);
    if (topic_list) element.append(topic_list);
    return element;
  }
  /** @param {Element} element */
  function describe_us_constitutional_amendment(amendment) {
    return wiki_unslug(amendment.wiki); // this labels, not describes
  }
  function render_us_constitutional_amendment_into(amendment, element, rex) {
    // this marks the creation process; the dates imply the effective period
    const lang = "en";
    const { number, wiki, description } = amendment;
    const from = amendment.proposed;
    const to = amendment.completed;
    console.assert(number, `which amendment is this?`, amendment);
    console.assert(from, `when was this amendment?`, amendment);
    // `completed` is informational but doesn't represent an end
    // but that's currently how tuple timestamps are represented
    timestamp(element, from /*, to */);
    // HACK -- repeal of prohibition
    if (number !== 18) element.setAttribute("data-time-to-present", "true");
    element.setAttribute("data-amendment-number", amendment.number);
    if (wiki) {
      const link = document.createElement("a");
      link.href = wiki_href(wiki, lang);
      link.textContent = wiki_unslug(wiki);
      element.append(link);
    }
    if (description) {
      const phrase = document.createElement("p");
      phrase.innerHTML = description; // HOTSPOT
      for (const link of phrase.querySelectorAll(`[wiki]:not([href])`)) {
        const wiki = link.getAttribute("wiki");
        link.href = wiki_href(wiki, lang);
      }
      element.append(phrase);
    }
  }
  function render_us_state_into(state, element, rex) {
    element.textContent = `${state.name}, a US State`;
  }
  function describe_us_president(term) {
    const { number, incumbent } = term;
    const name = wiki_unslug(incumbent);
    const ordinal = number; // ordinal_en(number);
    return `${ordinal} US presidency, of ${name}`;
  }
  function render_us_president_into(term, element, rex) {
    const from = term.termStart;
    let to = term.termEnd;
    console.assert(from, `when was this term?`, term);
    if (!to) {
      to = new Date().toISOString().slice(0, 10);
      element.setAttribute("data-time-to-present", "true");
    }
    // weird, this isn't working. but it shouldn't be done here anyway
    if (term.portrait) {
      const img = document.createElement("img");
      img.src = term.portrait;
      element.append(img);
    }
    assert(from, `president no from`);
    timestamp(element, from, to);
    element.textContent = describe_us_president(term);
  }
  return {
    render_person_into,
    render_event_into,
    render_us_constitutional_amendment_into,
    render_us_state_into,
    render_us_president_into,
  };
});

define("def:make_dynamic_set", ["~/process/streams"], ({
  make_subscribable,
}) => {
  function make_dynamic_set() {
    const set = new Set();
    const deltas = make_subscribable();
    const collection = make_subscribable();
    const has = key => set.has(key);
    function add(key) {
      if (!set.has(key)) {
        deltas.next({ delta: 1, key });
        set.add(key);
        collection.next(set);
      }
    }
    function remove(key) {
      if (set.has(key)) {
        deltas.next({ delta: -1, key });
        set.delete(key);
        collection.next(set);
      }
    }
    function dispose() {
      // close all connections
    }
    return {
      dispose,
      has,
      add,
      remove,
      deltas: deltas.public,
      stream: collection.public,
    };
  }
  return { make_dynamic_set };
});

define("def:make_dynamic_map", ["~/process/streams"], ({
  make_subscribable,
}) => {
  function make_dynamic_map() {
    const map = new Map();
    const deltas = make_subscribable();
    const collection = make_subscribable();
    const has = key => map.has(key);
    const get = key => map.get(key);
    function set(key, value) {
      if (!map.has(key)) {
        deltas.next({ delta: 1, key, value });
        map.set(key, value);
        collection.next(map);
      }
    }
    function remove(key) {
      if (map.has(key)) {
        // wait till you need this...
        // const value = map.get(value)
        deltas.next({ delta: -1, key /*, value */ });
        map.delete(key);
        collection.next(map);
      }
    }
    function dispose() {
      // close all connections
    }
    return {
      dispose,
      has,
      get,
      set,
      remove,
      deltas: deltas.public,
      stream: collection.public,
      [Symbol.iterator]() {
        return map[Symbol.iterator]();
      },
    };
  }
  return { make_dynamic_map };
});

// - any ongoing work is divided exclusively into processes
// - I can see what processes are still running and why
// - I can stop all processes
define("def:make_dom_system", [
  "~/methods/map/object",
  "def:make_dynamic_set",
  "def:make_dynamic_map",
], (map, { make_dynamic_set }, { make_dynamic_map }) => {
  const now = () => performance.now();

  // COPIED elsewhere in here
  {
    async function anon() {}
    const AsyncFunction = Object.getPrototypeOf(anon).constructor;
    function is_async(fun) {
      return fun instanceof AsyncFunction;
    }
  }

  function make_dom_system(context) {
    // BUT this is unused
    const { kb } = context;
    if (!kb) throw Error(`need knowledge base`);
    const doc = context.document ?? globalThis.document;
    const element = context.element ?? doc.createElement("div");
    const processes = make_dynamic_set(); // might not be needed
    const promises = make_dynamic_map();
    let _next_pid = 0;

    function make_host(pid, element) {
      return {
        get pid() {
          return pid;
        },
        spawn: (...args) => _beget(pid, ...args),
        // provisional: I don't want spawn to create a promise by default
        async(arg) {
          return arg;
        },
        emit: (detail, type = "default") => {
          const event = new CustomEvent(type, { detail, bubbles: true });
          element.dispatchEvent(event);
        },
        say: thing => {
          if (typeof globalThis.notate !== "function") {
            console.warn("`notate` is undefined. trying to say", thing);
            return;
          }
          const saying = notate(thing);
          if (saying) {
            if (saying instanceof Element)
              saying.setAttribute("data-time", now());
            // this.element.append(saying);
            element.append(saying);
          }
        },
        kill() {
          throw Error(`kill is not implemented!`);
        },
      };
    }

    // what I really mean here though is "ensure"
    // i.e. if I had something equivalent already, then it would refer to that
    function _beget(parent, description, args, name) {
      const pid = _next_pid;
      processes.add(pid);
      _next_pid++;

      // ∀x∈Process ∃y∈Element: notates(y, x) ∧ visible(y)
      const room = document.createElement("div");
      room.setAttribute("data-notation", "process");
      room.setAttribute("data-process-born", now());
      room.textContent = `process ${pid}. `;
      if (name) room.name = name;
      element.append(room);
      // annotate(room)

      const host = make_host(pid, room);
      if (typeof description === "function") {
        try {
          const result = description.apply(host, args);
          /*
          let result;
          if (is_async(description)) {
            result = description.apply(host, args).catch(error => {
              console.error("bruh", `${error}`, error);
            });
          } else {
            result = description.apply(host, args);
          }
          */
          room.setAttribute("data-process-accepted", "");
          room.setAttribute("data-process-died", now());
          // console.info(description, `accepted with`, result);
          if (result instanceof Promise) {
            const existing = promises.get(pid);
            if (existing) {
              console.warn(`already pending for ${pid}??`);
              return;
            }
            // console.info(`unboxing promise...`);
            promises.set(pid, existing);
            // Promise.resolve(result).then(
            result.then(
              value => {
                // console.info(description, `async accepted with `, value);
                room.setAttribute("data-process-died", now());
                room.setAttribute("data-process-accepted", "");
                promises.remove(pid);
              },
              error => {
                console.error(description, `async rejected with`, error);
                room.setAttribute("data-process-died", now());
                room.setAttribute("data-process-rejected", "");
                promises.remove(pid);
              }
            );
          }
        } catch (error) {
          console.error(description, `rejected with`, error);
          room.setAttribute("data-process-died", now());
          room.setAttribute("data-process-rejected", "");
        }
      } else if (typeof description === "object") {
        // distribute
        // you do want the generic map here...
        return map(description, (v, k) => spawn(v, args, `${name ?? ""}${k}`));
      } else {
        throw Error(`No method for coercing to process`, description);
      }
      return pid;
    }
    // async version that provides a promise
    // don't just always promise, but require an explicit request

    function spawn(description, args, name) {
      return _beget(null, description, args, name);
    }
    function kill() {
      for (const process of processes.values()) process.kill();
    }
    return { kill, spawn, element, processes };
  }
  return { make_dom_system };
});

// ==== catalog implementation

// helpers for below, but this should use define & require now
// I mean the *loaders* should use define
{
  // only tells you the script is *loaded*, not executed!
  function load_script(src) {
    return new Promise((resolve, reject) => {
      const script = document.createElement("script");
      script.onload = resolve;
      script.onerror = reject;
      script.src = src;
      document.head.append(script);
    });
  }

  function get_definition_sync(name) {
    return globalThis[name];
  }
  function defined_now(name) {
    return name in globalThis;
  }
  let timeout;
  function when_defined(name, interval = 1000) {
    if (defined_now(name)) return get_definition_sync(name);
    return new Promise(() => {
      function tick() {
        if (defined_now(name)) return get_definition_sync(name);
        timeout = setTimeout(tick, interval);
      }
      tick();
    });
  }
}

define("def:reify_collection", ["~/io/indexedDB"], IDB => {
  async function ensure_catalog_data(db_name, store_name, spec) {
    const { say } = this;
    const { source, xml_name } = spec;
    say?.(`First of all, is there already data in ${db_name}:${store_name}?`);
    try {
      const count = await this.async(
        IDB.count({ db: db_name, store: store_name })
      );
      if (count > 0) {
        say?.(`Yes! ${count} things, in fact.  So I say we're done here`);
        return;
      }
      say?.(`No.`);
    } catch (error) {
      console.error(`Well I had a problem counting`, error);
    }
    say?.(`Can I load the source? (which is at ${source})`);
    try {
      await load_script(source);
    } catch (error) {
      say?.(`It appears not.  I got the error ‘${error}’`);
      return;
    }
    say?.(`Well I loaded the source.  I'll wait now for ${xml_name}`);
    const xml = await when_defined(xml_name);
    if (xml) say?.(`Oh! That exists too now.  It's ${xml.length}`);
    else {
      say?.(`For some reason that still doesn't exist`);
      say?.(`K bye!`);
      return;
    }

    const parser = new DOMParser();
    let /** @type Document */ doc;
    try {
      // but this won't catch parse errors, see https://stackoverflow.com/a/70824527
      doc = parser.parseFromString(xml, "text/xml");
    } catch (error) {
      say?.(`Wouldn't you know but that failed with ${error}`);
      say?.(`Bye!`);
      return;
    }
    say?.(`Did that parse *actually* succeed?`);
    const parseerror = doc.querySelector("parseerror");
    if (parseerror) {
      say?.(`Well friend that failed with ${parseerror.textContent}`);
      say?.(`Bye!`);
      return;
    }
    say?.(`And I parsed it!  Now, do we have a reader?`);
    const reader = spec.from_dom;
    if (!reader) {
      say?.(`No, sadly we don't. Bye!`);
      return;
    }
    say?.(`Yes, we do.  Let's read it!`);
    const results = reader(doc.documentElement);
    say?.(`I just read ${results.length} things!`);
    say?.(`Now it just remains to write them to the store.`);
    const description = spec.store;
    const options = { batch_size: 100 };
    const destination = { database: db_name, store: store_name, description };
    for await (const status of IDB.write(results, destination, options)) {
      say(status);
    }
    say?.(`All done!`);
  }

  async function reify_collection(db_name, store_name, spec) {
    const { say } = this;
    const lang = "en";
    const doc = document;
    const container = doc.querySelector("main") ?? doc.body;

    const element = doc.createElement("div");

    const heading = doc.createElement("h2");
    const form = doc.createElement("form");
    const delete_button = doc.createElement("button");

    heading.append(`Collection: ${store_name}`);
    delete_button.textContent = "Delete this store!";
    // this creates an interactive process... now something is waiting and depends on invisible state to maintain this
    delete_button.onclick = async event => {
      event.preventDefault();
      say?.(`Sigh.  You really want me to delete ${store_name}, eh?`);
      try {
        // DELETE operation not monotonic
        // this is all kinds of race condition etc
        let db = await IDB.connect(db_name);
        say?.(`Okay, well I have ${db_name} at version ${db.version}`);
        db.close();
        db = await IDB.connect(db_name, db.version + 1, db => {
          say?.(`I'm in an upgrade transaction!`);
          db.deleteObjectStore(store_name);
          say?.(`Aaaaand I guess that worked?`);
        });
        db?.close();
      } catch (error) {
        console.error(`Couldn't delete store`, error);
      }
    };
    form.append(delete_button);
    element.append(heading, form);

    // make sure it has the latest indices if any are added
    // so that is updated even if there is no data update
    const description = spec.store;
    const db = await IDB.database_with(db_name, store_name, description);
    db.close();
    this.spawn(ensure_catalog_data, [db_name, store_name, spec]);
  }
  return { reify_collection };
});

// ==== DOM stuff

define("~/dom/attribute_effects", [], () => {
  return function attribute_effects(element, specs) {
    const observer = new MutationObserver(function _attribute_effect(changes) {
      for (const { attributeName: name } of changes) {
        const value = element.getAttribute(name);
        specs[name](value);
      }
    });
    const names = Object.keys(specs);
    observer.observe(element, { attributeFilter: names });
    return { dispose: () => observer.disconnect() };
  };
});

// ==== PROVISIONAL: scalar frame
// like timeframe but just the scalars
// timeframe is an extension of this that has a mapping to date

// this is basically a dataflow node with an associated element
//
// its broadcast value should match those of the associated CSS properties on
// the element's inline style
//
// ASSUMES that no one else is making changes to the element's style.  If we
// could get notifications of changed property values (without confusing them
// with our own updates), I would have to: (1) change it back, starting a
// possible edit war, OR (2) change the state to reflect the inline values,
// which I would then broadcast.  The API provides a way to set the values.  I suppose that if I could monitor the inline property... well... but I *can*, can't I?  I mean then you don't need a bloody JS API.

define("~/js/make_scalar_frame", [
  "~/dom/attribute_effects",
], attribute_effects => {
  function make_scalar_frame({ element }) {
    this.spawn(attribute_effects, [
      element,
      {
        style: css => {
          console.debug(`thing changed`, css);
        },
      },
    ]);
  }
});

// ==== timeframes

define("timelab:timeframes", [
  "~/dom/attribute_effects",
  "timelab:ISO8601",
  "timelab:major-scale",
], (attribute_effects, ISO8601, major_scale) => {
  const { assert } = console;
  const { major_scalar_from, major_scalars_to_timeframe, years_to_major } =
    major_scale;
  const is_number = x => typeof x === "number" && !isNaN(x);

  // HACK: put attribute watcher on this property of element
  // need a way to make this true “for all timeframes”
  const PROCESS = Symbol();
  function get_closest_timeframe_from(element) {
    const tf = element?.closest(`[data-timeframe]`);
    if (tf) {
      tf[PROCESS] ??= date_to_scalar_process(tf);
      return tf;
    }
  }

  {
    const { next } = ISO8601;
    // equivalence between single- and split-date notations
    // I think this follows from ISO 8601 though perhaps not stated
    function normalize(date) {
      return date.includes("/") ? date.split("/", 2) : [date, next(date)];
    }
  }

  function date_to_scalar_process(element) {
    // we don't know whether this is from our change or anyone else's
    function _timeframe_attribute_changed(date) {
      if (!date) return;
      const [from, to] = normalize(date);
      const frame = get_scalars(element);
      const scalar_date = frame && major_scalars_to_timeframe(...frame);
      if (scalar_date !== date)
        set_scalars(element, ...normalize(scalar_date).map(major_scalar_from));
    }
    const watcher = attribute_effects(element, {
      "data-timeframe": _timeframe_attribute_changed,
    });
    return { dispose: () => watcher.dispose() };
  }

  function get_scalars(/** @type Element */ { style }) {
    const from = parseFloat(style.getPropertyValue("--timeframe-from-major"));
    if (isNaN(from)) return;
    const to = parseFloat(style.getPropertyValue("--timeframe-to-major"));
    if (isNaN(to)) return;
    return [from, to];
  }

  function set_scalars(/** @type Element */ element, from, to) {
    if (is_number(from) && is_number(to)) {
      element.style.setProperty("--timeframe-from-major", from);
      element.style.setProperty("--timeframe-to-major", to);
    }
  }

  return { get_closest_timeframe_from, get_scalars, set_scalars };
});

define("timelab:scalar-mover", ["timelab:ISO8601"], ISO8601 => {
  // pan left reaches a fixpoint where start = lo
  // pan right reaches a fixpoint where end = hi
  // zoom in reaches a fixpoint where size = min_size
  // zoom out reaches a fixpoint where size = max_size (or start=lo & end=hi)
  // after fixpoint, the inverse operation is still possible
  // after zoom-in fixpoint, pans are still possible (usually)
  // after pan fixpoints, zooms are still possible (usually)

  const { broader, next, distance, coerce, plus } = ISO8601;
  const { max, min, sign } = Math;

  // pan should NOT start turning into zoom when you hit bound
  // zoom should NOT start turning into pan when you hit bound -- but it does rn
  function make_constrain(bounds) {
    const max_size = bounds.max_size ?? Infinity;
    const min_size = bounds.min_size ?? 0;
    const lo = bounds.lo ?? -Infinity;
    const hi = bounds.hi ?? Infinity;
    if (min_size < 0) throw Error(`Bad request: need non-negative min`);
    if (hi - lo < min_size) throw Error(`Bad request: hi-lo<min_size`);
    // but you could want to say this
    // if (hi - lo < max_size) throw Error(`Bad request: hi-lo<max_size`);
    const actual_max = min(hi - lo, max_size);
    const max2 = actual_max / 2;
    const min2 = min_size / 2;
    return function constrain(from, to) {
      const size = to - from;
      if (size > max_size) {
        const mid = (from + to) / 2;
        return [max(lo, mid - max2), min(hi, mid + max2)];
      }
      if (size < min_size) {
        const mid = (from + to) / 2;
        return [max(lo, mid - min2), min(hi, mid + min2)];
      }
      if (from < lo) return [lo, lo + min(size, actual_max)];
      if (to > hi) return [hi - min(size, actual_max), hi];
      return [from, to];
    };
  }
  function make_mover(α1, α2, bounds) {
    const constrain = make_constrain(bounds);
    return function move(from, to, r = 0.5) {
      const size = to - from;
      const δ1 = α1 * r * size;
      const δ2 = α2 * (1 - r) * size;
      return constrain(from + δ1, to + δ2);
    };
  }
  return function make_scalar_mover(spec) {
    // I mean you could want different alpha for pan vs zoom
    const α = spec.alpha ?? 1 / 8;
    const { bounds } = spec;
    return {
      zoom_in: make_mover(α, -α, bounds),
      zoom_out: make_mover(-α, α, bounds),
      pan_left: make_mover(-α, -α, bounds),
      pan_right: make_mover(α, α, bounds),
    };
  };
});

// === markup tools

define("~/match_and_mark_text", [], () => {
  const ident = x => x;
  function* match_and_mark_text(rex, text, map, doc) {
    const f = map ?? ident;
    if (!rex) {
      if (text) yield f(text);
      return;
    }
    let pos = 0;
    if (!text) return;
    for (const match of text.matchAll(rex)) {
      const sep = text.substring(pos, match.index);
      if (sep) yield f(sep);
      const mark = doc.createElement("mark");
      const [needle] = match;
      mark.textContent = f(needle);
      yield mark;
      pos = match.index + needle.length;
    }
    if (pos === 0) yield f(text);
    else if (pos < text.length) {
      const tail = text.substring(pos);
      if (tail) yield f(tail);
    }
  }
});

define("~/match_and_mark_dom", [], () => {
  const { NotImplemented } = def.errors;

  function preorder_next(/** @type Node */ my) {
    if (!my) return;
    if (my.firstChild) return my.firstChild;
    do if (my.nextSibling) return my.nextSibling;
    while ((my = my.parentNode));
  }

  const { assert } = console;
  /** @param rex {RegExp} */
  function match_and_mark_dom(rex, /** @type Element */ element, doc) {
    if (!rex) return;
    // THIS is schlemel the painter. could use state since we go only forward
    function slice_containing(index) {
      let node = element;
      let pos = 0;
      while (node) {
        if (node instanceof Text) {
          const len = node.textContent.length;
          if (pos + len > index) return { node, offset: index - pos };
          pos += len;
        }
        node = preorder_next(node);
      }
    }
    for (const match of element.textContent.matchAll(rex)) {
      const start_index = match.index;
      const end_index = match.index + match.length - 1;
      const start = slice_containing(start_index);
      const end = slice_containing(end_index);
      assert?.(start, `mark html expected to find start`);
      assert?.(end, `mark html expected to find end`);
      if (start.node === end.node) {
        const node_text = start.node.textContent;
        const match_text = match[0];
        const mark = doc.createElement("mark");
        mark.textContent = match_text;
        const text_before = node_text.substring(0, start.offset);
        const text_after = node_text.substring(end.offset + match_text.length);
        start.node.replaceWith(text_before, mark, text_after);
      } else if (start.node.parentElement === end.node.parentElement) {
        // a single mark spanning the html
        throw NotImplemented(`match including markup`);
      } else {
        // need multiple marks broken up over tree
        throw NotImplemented(`oblique match`);
      }
    }
  }
  match_and_mark_dom.comment = `Locate and mark RegExp matches on an DOM subtree.`;
  // mark matches on an element's text in-place
  // Won't hit the unhandled cases b/c *predicates* don't match across markup...

  return { match_and_mark_dom };
});

define("flex time line item", [
  "rdfs:label",
  "timelab:date_fields_of",
  "timelab:make_timestamp",
], (get_label, date_fields_of, make_timestamp) => {
  // like the thing for people, but for records generally
  // given we know which fields to interpret as dates
  function notate(record, lang) {
    const element = document.createElement("li");
    element.setAttribute("data-strategy", "");
    {
      const span = document.createElement("span");
      const label = get_label(record, lang);
      span.append(label);
      span.setAttribute("property", "rdfs:label");
      element.append(span);
    }

    const date_fields = date_fields_of("type");
    if (date_fields) {
      for (const field of date_fields) {
        const date = record[field];
        const timestamp = make_timestamp(date);
        // hack, use field name as a label
        element.append(timestamp, field);
      }
    }

    return element;
  }

  function classify(record) {
    // w.r.t the element?
    // update element if reclassified?
  }
});

define("rdfs:label", ["~/def/ranges", "~/def/generics"], (ranges, generics) => {
  const { NotImplemented } = def.errors;
  const { make_generic } = generics;
  // why string though?
  const label = make_generic({
    name: "label", // rdfs:label
    signatures: [thing => String, (thing, lang) => String],
  });
  label.comment = `Associates a thing with some descriptive labeling, possibly in a specific language`;
  return label;
});

define("timelab:date_fields_of", ["timelab:METHODS"], METHODS => {
  return function date_fields_of(type) {
    if (type in METHODS.date_fields) return METHODS.date_fields[type];
    throw NotImplemented("date_fields_of", type);
  };
});

define("timelab:main", [
  "~/home/new",
  "timelab:timeframes",
  "timelab:catalog",
  "timelab:methods",
  "timelab:major-scale",
  "def:make_dom_system",
  "~/io/indexedDB",
  "def:reify_collection",
], (settle, timeframes, CATALOG, METHODS, major_scale, sys, IDB, reify) => {
  const { reify_collection } = reify;
  const { years_to_major } = major_scale;
  const { make_dom_system } = sys;

  const TYPE = Symbol();
  const kb = {};
  const home = settle();
  const system = make_dom_system({ kb, element: home });

  {
    const { defineProperty } = Object;
    function with_type(collection, record) {
      return function (record) {
        defineProperty(record, TYPE, { value: collection });
        return record;
      };
    }
  }
  const SPACE = /\s+/g;
  const dom_string_list = s => s.split(SPACE);

  // faux generic stuff
  function interpreter_of(type) {
    if (type === "store") return maybe_interpret_element_as_store;
  }
  function make_get_a(element) {
    return function get_a(name) {
      const attribute = `data-${name}`;
      const value = element.getAttribute(attribute);
      if (value) return value;
      element.append(`I will have to get a ${name}. `);
    };
  }

  // get the data first?  if we don't have it, then we create an empty store...
  async function maybe_interpret_element_as_store(element) {
    const get_a = make_get_a(element);
    const db = get_a("database");
    const store = get_a("store");
    if (!db) return element.setAttribute("data-warn", `which database?`);
    if (!store) return element.setAttribute("data-warn", `which store?`);
    // get the count and if it's small, then just get them all
    const say = element.append.bind(element);
    say(
      `In a database called “${db}”, I'm looking for a store called “${store}”. `
    );
    const source = { db, store };
    let count = NaN;
    try {
      count = await IDB.count(source); // POWER
    } catch (error) {
      say(`Sorry, there was a problem counting: ${error}.`);
    }
    if (typeof count !== "number" || isNaN(count))
      return say(
        `Well I can't even tell how big this store is, so I think we're done here. `
      );
    say(`I say that there are ${count} items in ${store}. `);
    // just emit the things in an event, as a collection
    // also, if we're willing to get the first 50, we could do that regardless of size
    // and also emit a continuation
    if (count === 0)
      return say(`Well, the store is empty, so I think we're done here. `);

    if (count < 50) {
      say(`I mean, ${count} is not too many to list, right?. `);
      const items = await this.async(IDB.get_all(source)); // POWER x50
      items.forEach(with_type(store));
      say(`All right, I got ${items.length} items. `);
      // okay not that... I mean we want the process on this element
      // this.emit(items, "found");
      const event = new CustomEvent("found", {
        detail: items,
        bubbles: true,
      });
      element.dispatchEvent(event);
    }
  }

  function maybe_interpret_element_as_player_sync(element) {
    const { say } = this;
    if (element.childNodes.length === 0) {
      element.setAttribute("data-warn", `empty player!`);
      return; // here async would listen for changes
    }
    // if (Math.random() < 0.25) throw Error(`chaos`);
    for (const child of element.children) {
      const typeof_ = child.getAttribute("typeof");
      if (typeof_) {
        const types = dom_string_list(typeof_);
        for (const type of types) {
          const interpret = interpreter_of(type); // a generic
          if (!interpret) {
            const warning = `not implemented: ${type}`;
            child.setAttribute("data-warn", warning);
            return; // here async would await an interpreter
          }
          console.assert?.(typeof interpret === "function");
          this.spawn(interpret, [child]);
        }
      }
    }
  }

  function main() {
    // ============ minimum stuff needed to run some test figures
    const { assert } = console;
    function as_line(thing) {
      const type = thing?.[TYPE];
      if (!type) throw Error(`I can't display this thing as a line`);
      const element = document.createElement("div");
      element.setAttribute("typeof", type); // but this is collection...
      const notator = METHODS.render_into[type];
      if (typeof notator !== "function") throw Error(`no notator for ${type}`);
      notator(thing, element);
      return element;
    }

    // ==========================================

    function init() {
      this.spawn({
        help() {
          this.say(`What's this all about?`);
          this.say(`Where do things go?`);
          this.say(`Why write javascript?`);
        },
        // ∀x∈Collection ∃y∈Store: consistent-with(y, x)
        reify_persistence() {
          const { say } = this;
          const database_name = "cronwall";
          const expectations = CATALOG;

          say(
            `I will try to bring the database ${database_name} in line with the given catalog description.  The catalog description is a dictionary of table descriptions which can be processed completely independently.`
          );
          for (const [store, description] of Object.entries(expectations)) {
            say?.(`So there's a thing called ${store}`);
            this.spawn(
              reify_collection,
              [database_name, store, description],
              store
            );
          }
        },
        // ∀x∈Element ∃y∈Interpreter: interprets(y, x)
        one_time_create_players() {
          const { say, spawn } = this;
          say(`I look through the document for players to activate.`);
          const all_players = document.querySelectorAll("[data-player]");
          for (const element of all_players) {
            spawn(maybe_interpret_element_as_player_sync, [element]);
          }
        },
      });
    }

    // I just want to say things like:

    function what_else(thing) {
      // but thing is the record, not the representation...
      const type = thing?.[TYPE];
      if (!type) return;
      // - whenever you're adding a presidency, add the president too (with lifespan)
      if (type === "en_us_presidents") {
        return thing.incumbent;
      }

      // - whenever you see an amendment, also the refs in it
      // - whenever you add a ref, add the first event mentioning it, maybe the last too
    }

    // top-level event handlers: things that we can catch when they bubble to the top
    // and which thereby allow us to maintain invariants across larger scopes which can be inert
    // such as:
    // - command buttons
    // - input prompts
    // - findings/results

    function click_handler(event) {
      const selector = `.special-list > *`; // exploratory
      const touchable = event.target?.closest(selector);
      if (touchable) {
        touchable.classList.toggle("touched");
        // event.preventDefault();
      }
    }

    function keyup_handler(/** @type {KeyboardEvent} */ event) {
      document.body.classList.toggle("shiftKey", event.shiftKey);
      document.body.classList.toggle("ctrlKey", event.ctrlKey);
    }
    function keydown_handler(/** @type {KeyboardEvent} */ event) {
      document.body.classList.toggle("shiftKey", event.shiftKey);
      document.body.classList.toggle("ctrlKey", event.ctrlKey);
    }

    function default_handler(event) {
      if (event instanceof CustomEvent) {
        const { type, detail } = event;
        console.log("generic dispatch", type, detail);
        // put this somewhere that we know additions will be handled responsibly
        // link_to(detail);
        if (Array.isArray(detail)) {
          const list = document.createElement("ul");
          // but get rid of one liners
          list.classList.add(
            "special-list",
            "with-margin-inline",
            "one-liners"
          );
          for (const value of detail) {
            const item = document.createElement("li");
            const line = as_line(value);
            item.append(line ?? "undescribable");
            list.append(item);
            const more = what_else(detail);
            if (more) {
              // etc
            }
          }
          event.target.append(list);
        }
      } else {
        console.log("unrecognized event type", event.type);
      }
    }
    document.documentElement.addEventListener("keydown", keydown_handler);
    document.documentElement.addEventListener("keyup", keyup_handler);
    document.documentElement.addEventListener("click", click_handler);
    document.documentElement.addEventListener("default", default_handler);
    document.documentElement.addEventListener("found", default_handler);

    system.spawn(init);
  }
  return {
    system,
    main,
    maybe_interpret_element_as_store,
    maybe_interpret_element_as_player_sync,
  };
});

define("~/dom/console", ["as_dom", "timelab:timestamp_element"], (
  as_dom,
  timestamp
) => {
  function make_dom_console() {
    const element = document.createElement("output");
    element.ariaLive = "";

    const _console = {};
    for (const method of Object.keys(console)) {
      if (typeof console[method] !== "function") continue;
      const list = document.createElement("ol");
      const name = `console_${method}`;
      list.setAttribute("data-console", method);
      list.setAttribute("typeof", `https://console.spec.whatwg.org/#${method}`);
      _console[method] = {
        [name]: function (...messages) {
          const item = document.createElement("li");
          // we might also want hr timestamp...
          timestamp(item);
          for (const message of messages) {
            const representation = as_dom(message);
            item.append(representation);
          }
          list.append(item);
        },
      }[name];
      element.append(list);
    }

    return { console: _console, element };
  }
  make_dom_console.comment = `Create a console interface that dumps everything into the current document.`;
  return make_dom_console;
});

define("~/dom/ancestor", [], () => {
  return function dom_is_ancestor_of(a, b) {
    if (!a || !b) return false;
    const p = b.parentNode;
    return a === p || dom_is_ancestor_of(a, p);
  };
});

define("~/blocks/expando", ["~/dom/devolve"], dom_devolve => {
  const { abs, min, max, floor } = Math;
  const ONCE = Object.freeze({ once: true });
  const is_number = x => typeof x === "number" && !isNaN(x);
  // const behavior = "smooth"
  const behavior = undefined;

  return function make_expando_block(given) {
    const outer = given.element ?? document.createElement("div");
    const inner = given.inner ?? document.createElement("div");

    let _interval = NaN;
    const console = given.console ?? globalThis.console;
    outer.classList.add("expando");
    inner.classList.add("inner");

    const scroll_container = document.documentElement;
    {
      const button = document.createElement("button");
      button.setAttribute("data-command", "grow");
      button.classList.add("header");
      button.append("grow");
      button.onclick = event => event.preventDefault(); // in case it's on a form
      outer.prepend(button);
    }

    const body = document.createElement("div");
    body.classList.add("body");

    function set_size(new_size) {
      if (!is_number(new_size)) return console.warn(`expected number size`);
      const effective = max(min_size, new_size);
      outer.style.setProperty("--block-size-px", effective);
      // But we never unset this...
      if (!outer.getAttribute("data-grown"))
        outer.setAttribute("data-grown", new Date().toISOString());
    }

    const alpha = 1 / 11;
    const min_size = 24 + 24; // need extra for borders...
    const STEPS = 64;
    const STEP = 1 / STEPS;

    const identity = x => x;
    // provisional: monad over [0,1] to redistribute, say, towards ends
    const rescale = identity;
    // needs to orbit at 0
    // should be differentiable
    function shrink() {
      const box = outer.getBoundingClientRect();
      const container_size = scroll_container.clientHeight;
      const start = box.top; // for block direction
      const size = box.height; // for block direction
      const max_size = container_size - start; // max it could get without scroll
      if (size > max_size) return;
      const size_range = max_size - min_size;
      const h = size - min_size;
      const t = h / size_range;
      if (t > 1)
        return console.warn("assert fail t>1", { start, size, max_size });
      const s = max(0, t - STEP);
      const r = rescale(s);
      const target = floor(min_size + size_range * r);
      if (target <= min_size) release();
      else set_size(target);
    }
    function grow() {
      const max_size = scroll_container.clientHeight;
      const box = outer.getBoundingClientRect();
      const headroom = box.top;
      const footroom = max_size - box.bottom;
      const size = box.height;
      let grow_by = NaN;

      if (headroom < 0) {
        // we begin before the viewport, our top is clipped
        // but scrolling would be chaotic, so fall through
        // console.debug(`headroom ${headroom} < 0`);
        stop_growing();
      }
      if (footroom < 0) {
        // we end after the viewport, our bottom is clipped
        if (footroom > -1) {
          // console.debug(`footroom ${footroom} ∈ (-1, 0)`);
        } else {
          // console.debug(`footroom ${footroom} ≤ -1`);
        }
      } else if (footroom > 0) {
        console.debug(`footroom ${footroom}`);
        // we end before the view, we have room to grow
        // claim some portion of what footroom remains
        // this may lead to a reflow below
        grow_by = floor(footroom * alpha);
      } else if (headroom > 0) {
        // we start after the view, we have room to move up
        // but scrolling would move the bottom *away* from the viewport end
        // besides programmatic scrolling being generally bad
        stop_growing();
      }

      if (is_number(grow_by)) set_size(size + grow_by);
    }

    function release() {
      outer.removeAttribute("data-grown");
      outer.style.setProperty("--block-size-px", min_size);
    }

    function grow_and_keep_growing(event) {
      if (isNaN(_interval)) {
        grow();
        _interval = setInterval(grow, 50);
      }
    }
    function shrink_and_keep_shrinking(event) {
      if (isNaN(_interval)) {
        shrink();
        _interval = setInterval(shrink, 50);
      }
    }
    function stop_growing(event) {
      clearInterval(_interval);
      _interval = NaN;
    }

    function keydown(event) {
      const button = event.target.closest(`[data-command="grow"]`);
      if (button) {
        if (event.key === "Enter") {
          if (event.shiftKey) shrink();
          // hmm, you get this for free with keyboard repeat
          // which uses the system preference for repeat delay & rate
          // but it won't match what mouse/touch does
          else grow(); //  grow_and_keep_growing();
          event.stopPropagation();
        }
        if (event.target.closest(`[data-command="release"]`)) release();
      }
    }
    function keyup(event) {
      if (event.key === "Enter") {
        if (event.target.closest(`[data-command="grow"]`)) stop_growing();
        event.preventDefault();
      }
    }

    function mousedown(event) {
      if (event.button !== 0) return;
      if (event.target.closest(`[data-command="grow"]`)) {
        if (event.shiftKey) shrink_and_keep_shrinking();
        else grow_and_keep_growing();
        event.stopPropagation();
      }
      if (event.target.closest(`[data-command="release"]`)) release();
    }
    function mouseup(event) {
      if (event.button !== 0) return;
      if (event.target.closest(`[data-command="grow"]`)) stop_growing();
      if (event.target.closest(`[data-command="release"]`)) release();
    }

    const devolver = dom_devolve({ outer, inner });
    outer.addEventListener("mousedown", mousedown);
    outer.addEventListener("mouseup", mouseup);
    outer.addEventListener("keydown", keydown);
    outer.addEventListener("keyup", keyup);
    if (is_number(given.size)) set_size(given.size ?? min_size);
    function dispose() {
      devolver.dispose();
      stop_growing();
      outer.removeEventListener("keydown", keydown);
      outer.removeEventListener("keyup", keyup);
      outer.removeEventListener("mousedown", mousedown);
      outer.removeEventListener("mouseup", mouseup);
    }

    return { dispose, element: outer };
  };
});

define("timelab:test-expando-block-with-static-list", [
  "~/dom",
  "~/blocks/expando",
], (container, make_expando_block) => {
  const wrapper = document.createElement("div");
  const size = 16 * 8;
  make_expando_block({ element: wrapper, size });

  const list = document.createElement("ol");
  const items = "once upon a time".split(" ").map(text => {
    const item = document.createElement("li");
    item.textContent = text;
    return item;
  });
  list.append(...items);

  wrapper.append(list);
  container.prepend(wrapper);
});

define("timelab:test-expando-block-with-collection", [
  "~/home/new",
  "timelab:main",
], (settle, main) => {
  const { system, maybe_interpret_element_as_store } = main;

  const wrapper = document.createElement("div");
  const timeframe = document.createElement("div");

  const store = document.createElement("div");

  timeframe.setAttribute("data-timeframe", "175/210");
  timeframe.style.setProperty("--timeframe-from-major", 175000);
  timeframe.style.setProperty("--timeframe-to-major", 210000);

  store.setAttribute("data-database", "cronwall");
  store.setAttribute("data-store", "en_us_constitutional_amendments");

  system.spawn(maybe_interpret_element_as_store, [store]);

  timeframe.append(store);
  wrapper.append(timeframe);
  settle({ size: 16 * 10 }).prepend(wrapper);
});

define("~/explore/expando-logging-self", [
  "timelab:main",
  "~/dom/home",
  "~/dom/console",
  "~/blocks/expando",
  "timelab:minor-scale",
], (main, container, make_dom_console, make_expando_block, minor_scale) => {
  const { system } = main;
  const { console, element } = make_dom_console();
  const { minor_scalar_from } = minor_scale;

  const expando = document.createElement("div");
  const timeframe = document.createElement("div");
  const size = 16 * 5;

  system.spawn(function () {
    const interval = setInterval(tick, 500);
    function tick() {
      const now = new Date();
      timeframe.style.setProperty("--timeframe-to", minor_scalar_from(now));
    }
    return { dispose: () => clearInterval(interval) };
  });

  system.spawn(make_expando_block, [{ element: expando, console, size }]);
  {
    const now = new Date();
    const shortly = new Date();
    shortly.setUTCSeconds(shortly.getUTCSeconds() + 20);
    const date = `${now.toISOString()}/${shortly.toISOString()}`;
    timeframe.setAttribute("data-timeframe", date);
    timeframe.style.setProperty("--timeframe-from", minor_scalar_from(now));
    timeframe.style.setProperty("--timeframe-to", minor_scalar_from(shortly));
  }
  timeframe.append(element);
  expando.append(timeframe);
  container.prepend(expando);
});

define("def:test-dom-console", [
  "~/dom",
  "~/dom/console",
], async function test_dom_console(container, make_dom_console) {
  `But this does an async append causing reflow`;
  const { console, element } = make_dom_console();
  container.prepend(element);
  console.log("hello world");
  console.log("it's been good to know ya");
  await new Promise(resolve => setTimeout(resolve, 1000));
  console.log("worth the wait");
});

define("as_dom", () => {
  return function as_dom(anything) {
    if (anything === null) return `∅`;
    if (anything === undefined) return `⊥`;
    return `${anything.toString()}`;
  };
});

define(
  "timelab:notes",
  `BUG: page goes into an infinite loop if the define doesn't return anything`
);

define("~/explore/system", ["~/home/new", "as_dom"], (settle, as_dom) => {
  const dl = document.createElement("dl");
  {
    const definitions = define?.definitions;
    console.assert(definitions, "expected definitions");
    for (const [name, definition] of definitions) {
      const div = document.createElement("div");
      const dt = document.createElement("dt");
      const dd = document.createElement("dd");
      const representation = as_dom(definition);
      dt.append(name);
      dd.append(representation);
      div.append(dt, dd);
      dl.append(div);
    }
  }
  const container = settle();
  container.insertAdjacentHTML("afterbegin", `<h2>definitions</h2>`);
  container.append(dl);
  return { dispose: () => {} };
});

define("~/explore/system/2", ["~/dom/home", "~/dom/console", "timelab:main"], (
  container,
  make_dom_console,
  main
) => {
  function directory_shunt({ element, path }) {
    container.prepend(element);
    `This is a generalization of the case where a single candidate is assumed`;
    function ensure_container(name) {
      const selector = `[data-path="${name}"]`;
      const existing = element.querySelector(selector);
      if (existing) return existing;
      const group = document.createElement("div");
    }
    const observer = new MutationObserver(changes => {
      for (const { addedNodes, nextNode } of changes) {
        const prepend = !!nextNode;
        for (const node of addedNodes) {
          const iri = node.getAttribute("data-path");
          const parsed = new URL(iri);
        }
      }
    });
    observer.observe(element, { childList: true });
    return { dispose: () => observer.disconnect };
  }
  directory_shunt.comment = `Shunt new children by directory`;
  return directory_shunt;
});

define("~/test-async-procedure-with-dialog-playback", [
  "~/home/new",
  "~/dom/console",
  "timelab:minor-scale",
  "timelab:main",
], (settle, make_dom_console, minor_scale, main) => {
  const { system } = main;
  const { minor_scalar_from } = minor_scale;
  const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
  async function a_procedure(console) {
    console.debug(`I got your message`);
    await sleep(2500);
    console.debug(`is that what you really think??`);
    await sleep(1000);
    console.debug(`its over?`);
    await sleep(2000);
    console.debug(`no`);
    await sleep(1000);
    console.debug(`not like this`);
    await sleep(4800);
    console.debug(`🍄`);
    await sleep(200);
    console.log(`bug off I'm trying to sleep`);
  }

  const wrapper = document.createElement("div");
  const timeframe = document.createElement("div");
  const { console, element } = make_dom_console();

  wrapper.append(timeframe);
  timeframe.append(element);

  settle({ size: 16 * 6 }).prepend(wrapper);

  a_procedure(console);

  const now = new Date();
  const shortly = new Date();
  shortly.setUTCSeconds(shortly.getUTCSeconds() + 20);
  const date = `${now.toISOString()}/${shortly.toISOString()}`;
  timeframe.setAttribute("data-timeframe", date);
  timeframe.style.setProperty("--timeframe-from", minor_scalar_from(now));
  timeframe.style.setProperty("--timeframe-to", minor_scalar_from(shortly));
});

define("def:test-nested-expandos", [
  "timelab:main",
  "~/dom/home",
  "~/blocks/expando",
], ({ system }, container, make_expando_block) => {
  const boxen = {};
  for (const name of "abcdef") {
    const element = document.createElement("div");
    const size = 16 * 1;
    system.spawn(make_expando_block, [{ element, size }]);
    element.insertAdjacentHTML(
      "afterbegin",
      `<div>my name is <var>${name}</a></var>`
    );
    boxen[name] = element;
  }
  {
    const affinity = "end";
    const outer = document.createElement("article");
    const header = document.createElement("header");
    const inner = document.createElement("div");
    const given = { affinity, outer, inner };

    container.append(outer);
  }
  const { a, b, c, d, e, f } = boxen;
  d.append(e, f);
  a.append(b, c, d);
  container.prepend(a);
});

define("~/dom/devolve", ["~/dom/ancestor"], is_ancestor_of => {
  function dom_devolve(given) {
    const outer = given?.outer ?? document.createElement("div");
    const inner = given?.inner ?? document.createElement("div");
    if (!is_ancestor_of(outer, inner)) outer.append(inner);

    const observer = new MutationObserver(changes => {
      for (const { addedNodes, nextSibling } of changes) {
        for (const node of addedNodes) {
          if (node === inner) continue;
          if (nextSibling) inner.prepend(node);
          else inner.append(node);
        }
      }
    });
    observer.observe(outer, { childList: true });
    return { dispose: () => observer.disconnect() };
  }
  dom_devolve.comment = `Shunts all incoming elements into a descendant container`;
  return dom_devolve;
});

define("~/explore/dom/devolve", [
  "~/dom/home",
  "~/dom/devolve",
  "timelab:main",
], (container, dom_devolve, main) => {
  const { system } = main;

  const affinity = "end";
  const outer = document.createElement("article");
  const header = document.createElement("header");
  const inner = document.createElement("div");
  const given = { affinity, outer, inner };
  system.spawn(dom_devolve, [given]);

  container.append(outer);
});

define("~/test/def/serialize", ["~/dom/home"], container => {
  const fieldset = document.createElement("fieldset");
  const def_button = document.createElement("button");
  const doc_button = document.createElement("button");
  def_button.append(`download def`);
  doc_button.append(`download doc`);
  fieldset.append(def_button, doc_button);
  container.prepend(fieldset);
  const def_clicked = event => def.userland.prompt_download_def();
  const doc_clicked = event => def.userland.prompt_download_document();
  def_button.addEventListener("click", def_clicked);
  doc_button.addEventListener("click", doc_clicked);
  function dispose() {
    def_button.removeEventListener("click", def_clicked);
    doc_button.removeEventListener("click", doc_clicked);
  }
  return { dispose };
});

define("timelab:global-timeframe-pan-and-zoom", [
  "timelab:timeframes",
  "timelab:major-scale",
  "timelab:scalar-mover",
], (timeframes, major_scale, make_scalar_mover) => {
  const { get_closest_timeframe_from, get_scalars, set_scalars } = timeframes;
  const { years_to_major } = major_scale;

  // tied to major scale here...
  const DEFAULT_MOVER_SPEC = {
    pan_alpha: 1 / 16,
    zoom_in_alpha: 7 / 8,
    zoom_out_alpha: 9 / 8,
    max_size: years_to_major(8000),
    min_size: years_to_major(1 / 365),
    hi: years_to_major(5000),
    lo: years_to_major(-9000),
  };
  const mover = make_scalar_mover({ bounds: DEFAULT_MOVER_SPEC });

  function wheeled(/** @type {WheelEvent} */ event) {
    if (event.ctrlKey) return; // has conflicting defaults

    const { deltaX, deltaY, shiftKey } = event;
    if (deltaX && deltaY) return; // ignore diagonals

    const timeframe = get_closest_timeframe_from(event.target);
    if (!timeframe) return;

    const frame = get_scalars(timeframe);
    if (!frame) return; // the timeframe is “any time”

    const box = timeframe.getBoundingClientRect();
    const offset = event.clientX - box.left;
    const r = offset / box.width;

    // Ignore all magnitudes from the event, as they are highly variant
    let target;
    if (deltaY < 0 && shiftKey) target = mover.zoom_in(...frame, r);
    else if (deltaY > 0 && shiftKey) target = mover.zoom_out(...frame, r);
    else if (deltaX < 0) target = mover.pan_left(...frame);
    else if (deltaX > 0) target = mover.pan_right(...frame);

    if (target) set_scalars(timeframe, ...target);
  }

  const scope = document.documentElement;
  scope.addEventListener("wheel", wheeled);
  return { dispose: () => scope.removeEventListener("wheel", wheeled) };
});

define("~/home/new", ["~/dom/home", "~/blocks/expando"], (
  container,
  make_expando
) => {
  function make_home(spec) {
    const tag = typeof spec === "string" ? spec : spec?.tag ?? "div";
    const size = typeof spec === "number" ? spec : spec?.size ?? 24;
    const element = document.createElement("div");
    const inner = document.createElement(tag);
    container.prepend(element);
    make_expando({ element, inner, size });
    // they don't need to know about the expando
    return inner;
  }
  make_home.comment = `Provide housing in the current document.`;
  return make_home;
});

define("~/explore/datafy", ["~/def/ranges", "~/def/generics"], (
  ranges,
  generics
) => {
  const { make_generic } = generics;
  const { any } = ranges;
  const datafy = make_generic({ name: "datafy", signatures: [thing => {}] });
  datafy.comment = `Return or resolve to a portable (JSON-ifiable) projection of the given thing`;
  datafy.methods.add([any], it => it);
  datafy.seeAlso = "https://corfield.org/blog/2018/12/03/datafy-nav/";
  return datafy;
});

define("~/explore/nav", ["~/def/ranges", "~/def/generics"], (
  ranges,
  generics
) => {
  const { make_generic } = generics;
  const { any } = ranges;
  const nav = make_generic({ name: "nav", signatures: [(coll, k, v) => {}] });
  // yeah this doesn't make sense for generics as defined here
  // nav.methods.add([any], (_coll, _k, v) => v);
  nav.comment = `Locate the indicated item in the datafied value... or something`;
  return nav;
});

define("~/explore/datafy-nav/methods", ["~/explore/datafy", "~/explore/nav"], (
  datafy,
  nav
) => {
  datafy.methods.add([Map], map => {
    // what about object/function keys?
    // i mean you just have to skip those, as you can't navigate to them this way
  });

  datafy.methods.add([Storage], storage => {
    // well gee, this is just like any other object
    return Object.assign(Object.keys(storage), {
      [Symbol.for("nav")](coll, k, v) {
        return storage.getItem(k);
      },
    });
  });
  // ahhhhh. you want to implement nav “on Storage”
  // but the signature of nav doesn't involve a Storage
  // *that's* why you need to “embed” the navable objects in the return from datafy
  // and hence the protocol extension via metadata
  // nav.methods.add([Storage], (coll, k, v) => {});

  datafy.methods.add([IDBFactory], factory => {
    console.assert(factory === indexedDB);
    // still in draft, and not in Firefox
    // https://w3c.github.io/IndexedDB/#dom-idbfactory-databases
    // https://developer.mozilla.org/en-US/docs/Web/API/IDBFactory/databases
    if (typeof factory.databases === "function") return factory.databases();
  });
  nav.methods.add([IDBFactory], (coll, k, v) => {
    // the resolved value from factory.datbases() is an array of {name, version}
  });

  datafy.methods.add([IDBDatabase], db => {
    return db.objectStoreNames;
  });
  datafy.methods.add([IDBObjectStore], store => {
    // what, return all the keys?  also async, and potentially huge
  });
});

define("~/explore/navigate", [
  "~/explore/datafy",
  "~/explore/nav",
  "~/explore/datafy-nav/methods", // for effects
], (datafy, nav) => {
  async function* navigator_trampoline(start) {
    let subject = start;
    while (true) {
      const datafied = await datafy(subject);
      // the nav operation can provide both key and value...
      // wait, does it provide the collection too? or is that the datafied...
      const [key, value] = yield datafied;
      subject = await nav(datafied, key, value);
    }
  }
  navigator_trampoline.comment = `A stateful thing that lets you travel around using navigation protocols`;
  return navigator_trampoline;
});

define("~/explore/navigate/try", ["~/home/new", "~/explore/navigate"], (
  settle,
  navigator_trampoline
) => {
  const element = document.createElement("div");
  async function main() {
    const gen = navigator_trampoline();
    while (true) {
      const { value, done } = await gen.next();
      if (done) break;
      console.log("gen", value);
    }
  }
  main();
  settle().append(element);
});

define("~/dom/home", [], () => {
  let home = document.querySelector(`#home`);
  if (!home) {
    home = document.createElement("div");
    home.id = "home";
    const main = document.querySelector("main") ?? document.body;
    main.append(home);
  }
  return home;
});

// require(["timelab:ISO8601"], lib => lib.self_tests());
// require("timelab:test-expando-block-with-collection");
//require.async("~/explore/dom/devolve");
// require(["timelab:main"], ({ main }) => main());
// require.async("def:test-dom-console");
// require.async("~/explore/expando-logging-self");
// require.async("def:test-nested-expandos");
require("timelab:global-timeframe-pan-and-zoom");
// require(["def:self-tests"], self_tests => self_tests());
require.async("~/def/generics/test");
require.async("~/def/equal/test");
// require.async("~/explore/system");
// require.async("~/js/is_constructor/tests");
require.async("~/test/def/serialize");
// require.async("~/explore/adt/tries/test");
require.async("~/explore/rdfjs/test");
// require.async("~/test-async-procedure-with-dialog-playback");
// require.async("~/explore/navigate/try");
