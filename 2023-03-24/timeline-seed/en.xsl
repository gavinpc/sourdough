<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0'
               xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
               xmlns:func="http://exslt.org/functions"
               xmlns:str="http://exslt.org/strings"
               xmlns:math="http://exslt.org/math"
               xmlns:notebook="https://gavinpc.com/composition-notebook"
               exclude-result-prefixes="notebook"
               extension-element-prefixes="func"
               >

  <func:function name="str:ends-with">
    <xsl:param name="haystack" />
    <xsl:param name="needle" />
    <func:result
      select="$needle =
              substring($haystack,
                string-length($haystack) -
                string-length($needle) + 1)" />
  </func:function>

  <func:function name="notebook:is-digit">
    <xsl:param name="text" select="." />
    <func:result select="string-length(translate($text, '1234567890', '')) &lt; string-length($text)" />
  </func:function>
  
  <func:function name="notebook:digits">
    <xsl:param name="text" select="." />
    <func:result select="str:concat(str:tokenize($text, '')[notebook:is-digit(.)])" />
  </func:function>

  <func:function name="notebook:pad">
    <xsl:param name="length" />
    <xsl:param name="value" />
    <!-- use str:align() and str:padding() -->
    <xsl:variable name="zeros" select="str:padding($length, '0')" />
    <xsl:variable name="text" select="string($value)" />
    <func:result select="str:align($text, $zeros, 'right')" />
  </func:function>

  <func:function name="notebook:en-title-to-iso">
    <xsl:param name="title"/>
    <xsl:param name="debug" select="false" />
    
    <xsl:variable name="digits" select="notebook:digits($title)" />
    <xsl:variable name="bc" select="contains($title, '_BC')" />
    <xsl:variable name="z" select="number($digits)" />
    <xsl:variable name="is-number" select="number($z) = $z" />
    <xsl:variable name="n" select="math:abs($z)" />

    <xsl:if test="$debug">
      <xsl:message>
        debug: Given title ‘<xsl:value-of select="$title" />’
        debug: which contains the numerals ‘<xsl:value-of select="$digits" />’
        <xsl:if test="$is-number"> which is the number <xsl:value-of select="$z" /> with magnitude <xsl:value-of select="$n" />
        </xsl:if>
      </xsl:message>
    </xsl:if>
    
    <xsl:if test="$is-number">
      <func:result>
        <xsl:choose>
          <!-- Century -->
          <xsl:when test="contains(translate($title, 'C', 'c'), 'century')">
            <xsl:if test="$debug"><xsl:message>debug: It's a century</xsl:message></xsl:if>
            <xsl:if test="$bc">-</xsl:if>
            <xsl:value-of select="notebook:pad(2, $n)" />
          </xsl:when>

          <!-- Decade -->
          <xsl:when test="str:ends-with($title, '_(decade)') or str:ends-with($title, 's') or str:ends-with($title, 's_BC')">
            <xsl:if test="$debug"><xsl:message>debug: It's a decade</xsl:message></xsl:if>
            <xsl:if test="$bc">-</xsl:if>
            <xsl:value-of select="notebook:pad(3, $n div 10)" />
          </xsl:when>


          <!-- Millennium -->
          <xsl:when test="contains(translate($title, 'M', 'm'), 'millennium')">
            <xsl:if test="$debug"><xsl:message>debug: It's a millenium</xsl:message></xsl:if>
            <xsl:choose>
              <xsl:when test="$bc">
                <xsl:variable name="s">
                  <xsl:if test="$n!=0">-</xsl:if>
                </xsl:variable>
                <xsl:value-of select="concat('-', $n, '999/', $s, $n, '000')" />
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="concat($n - 1, '001/', $n, '000')" />
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>


          <!-- Year -->
          <!-- 
               <xsl:when test="starts-with($title, 'AD_') or ends-with($title, '_BC')">
          -->
          <!-- This will get false positives  -->
          <xsl:otherwise>
            <xsl:if test="$debug"><xsl:message>debug: It's a year</xsl:message></xsl:if>
            <!-- 
                 > While Year Zero does not exist in the Julian and Gregorian calendar,
                 > ISO8601 uses 0000 for 1 BC, and "-" (minus) for Before Christ (-0001
                 > is 2 BC).
            -->
            <!-- that's from something in ACDH.  -->
            <!-- so just reference ISO directly, eh -->
            <!-- except they seem to not publish the spec outright -->
            <xsl:choose>
              <xsl:when test="$bc">
                <xsl:if test="$n != 1">-</xsl:if>
                <xsl:value-of select="notebook:pad(4, $n - 1)" />
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="notebook:pad(4, $n)" />
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </func:result>
      
    </xsl:if>

  </func:function>
</xsl:transform>
