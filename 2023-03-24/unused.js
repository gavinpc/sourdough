// UNUSED now though I still think this has some place here
/*
  function iso8601_midpoint(iso_from, iso_to) {
    const a = ISO8601.parse(iso_from);
    const b = ISO8601.parse(iso_to);
    console.assert(a, `midpoint got bad from date ${iso_from}`);
    console.assert(b, `midpoint got bad to date ${iso_to}`);

    if (ISO8601.compare_units(a.unit, "Year") < 0)
      throw new Error(`Midpoint not implemented for ${a.unit} < Year`);

    const distance = ISO8601.distance(iso_from, iso_to);
    if (distance.unit === "Year") {
      const n1 = parseInt(a.year, 10);
      const n2 = parseInt(b.year, 10);
      const mid = Math.floor((n1 + n2) / 2);
      const { unit } = a;
      let pad;
      if (unit === "Year") pad = 4;
      if (unit === "Decade") pad = 3;
      if (unit === "Century") pad = 2;
      if (!pad) throw new Error(`Not implemented for ${unit}`);
      return `${mid.toString().padStart(pad, "0")}`;
    }

    // actually we don't know if a.unit === b.unit
    // console.assert(distance.unit === a.unit);
    // hacky way for small n
    if (distance.n < 50) {
      let date = iso_from;
      const half = Math.floor(distance.n / 2);
      for (let i = 0; i < half; i++) date = ISO8601.next(date);
      return date;
    }
    throw new Error(`midpoint not implemented for ${distance.n} >= 50`);
  }
  */
