function get_resource(url, file, alias)
  tup.definerule{
    command =
      '$(ROOT)/www/get.sh'
      ..' '..shell_quote(url)
      ..' '..shell_quote(file)
      ..' '..shell_quote(alias),
    outputs = {file}
  }
  return file
end
