tup.include('wiki.lua')

------------- helpers
function map(coll, f)
  local o = {}
  for _,v in ipairs(coll) do
    table.insert(o, f(v))
  end
  return o
end

function foreach_line_in(file, map)
  local file = io.open(file, 'r')
  if file ~= nil then
    for line in file:lines() do
      map(line)
    end
    file:close()
  else
    -- no such file or couldn't open for read
  end
end

function identity(x) return x end
function buffer_lines_in(file, fun)
  local f = fun or identity
  local o = {}
  foreach_line_in(file, function(v) table.insert(o, f(v)) end)
  return o
end

-----------------------------------------------------------------------

-- but transform is not actually used. en is imported
function wikipedia_timeline_index(lang, seed, transform, dir)
  local sources = buffer_lines_in(seed)
  local articles = map(sources, function(title) return wikipedia_article(lang, title) end)
  local gray = 'gray-xref-'..lang
  
  tup.definerule{
    inputs = articles,
    command = "xsltproc --html wiki-time-period-links.xsl %f | sort | uniq > "..shell_quote(gray),
    outputs = {'^'..gray},
  }
  return {sources=sources, articles=articles, gray=gray}
end

-- file is lines of space-delimited Wikipedia slug and an ISO-8601 date
-- f accepts slug & date as arguments
function project_index(file, f)
  foreach_line_in(
    file,
    function(line)
      local slug, iso = line:match'(.*) (.*)'
      if slug and iso then
        f(slug, iso)
      else
        io.write("line ‘"..line.."’\n")
        if slug == nil then io.write("- no slug\n") end
        if iso == nil then io.write("- no iso\n") end
      end
    end
  )
end

function wikipedia_timeline(lang)
  return wikipedia_timeline_index(
    lang,
    'timeline-seed/'..lang,
    'timeline-seed/'..lang..'.xsl',
    'timeline-index/'..lang)
end

limit=''

-- here and below, you MEAN only timeline articles... see scripts
-- shall we just do the title parsing here in Lua??
function project_persons(lang)
  tup.foreach_rule(
    {
      '$(ROOT)/wiki/cache/'..lang..'/'..limit..'*.html',
      -- HACK: exclude non-years.  But these are language specific
      -- and these are assessed against the path, not just the filename
      '^s',                     -- decades, it turns out
      '^mill',
      '^century',
    },
    -- The ISO value isn't in the content as such, so we need the lookup file
    "^b persons of %B^ ./persons.sh '%f' gray-xref-"..lang.." 2> >(sed '/\\(ID.*already defined\\|Tag.*invalid\\|Unexpected end tag\\)/,+2d' >&2) > '%o'",
    {'$(ROOT)/wiki/persons/'..lang..'/%B.xml'}
  )
end

function project_events(lang)
  tup.foreach_rule(
    -- exclude decades, as above, since decades are *mostly* transcluded in centuries
    -- HOWEVER that's not true in all cases I think, esp BC
    {'$(ROOT)/wiki/cache/'..lang..'/'..limit..'*.html', '^s'},
    "^b events of %B^ ./events.sh '%f' gray-xref-"..lang.." 2> >(sed '/\\(ID.*already defined\\|Tag.*invalid\\|Unexpected end tag\\)/,+2d' >&2) > '%o'",
    {'$(ROOT)/wiki/events/'..lang..'/%B.xml'}
  )
end

function wikipedia_timeline_persons(lang, index)
  -- HACK: this directory needs to exist or Tup will object to the next rule
  -- with “Unable to use inputs from a generated directory that isn't written to
  -- by this Tupfile.”  The directory will be created by the gray file when it
  -- exists, but the gray file might not exist on this pass.
  tup.definerule{
    -- Adding the transient file avoids the complaint without polluting the file
    -- system, *but* it breaks “fixpoint” because this will always be seen as
    -- unfinished business.
    --
    -- command = "^t^ touch '%o'",
    command = "touch '%o'",
    outputs = {'$(ROOT)/wiki/persons/'..lang..'/.dummy'}
  }
  local all_xml = '$(ROOT)/wiki/persons/index/'..lang..'-all.xml';
  project_persons(lang)
  tup.definerule{
    inputs = {'$(ROOT)/wiki/persons/'..lang..'/*.xml'},
    command = "^ aggregate person events for "..lang.."^ xsltproc --stringparam files '%f' aggregate-person-events.xsl dummy.xml > '%o'",
    outputs = {'$(ROOT)/wiki/persons/'..lang..'.xml'},
  }
  -- same as above but for this directory
  tup.definerule{
    command = "touch '%o'",
    outputs = {'$(ROOT)/wiki/persons/.dummy-'..lang}
  }
  tup.definerule{
    inputs = {'$(ROOT)/wiki/persons/'..lang..'.xml'},
    command = "cat '%f' | ./make-loader.sh persons > '%o'",
    outputs = {'$(ROOT)/wiki-persons-'..lang..'.js'},
  }
end

function wikipedia_timeline_events(lang, index)
  tup.definerule{
    command = "touch '%o'",
    outputs = {'$(ROOT)/wiki/events/'..lang..'/.dummy'}
  }
  local all_xml = '$(ROOT)/wiki/events/index/'..lang..'-all.xml';
  project_events(lang)
  tup.definerule{
    inputs = {'$(ROOT)/wiki/events/'..lang..'/*.xml'},
    command = "^ aggregate events for "..lang.."^ ./aggregate-events.sh '%f' > '%o'",
    outputs = {'$(ROOT)/wiki/events/'..lang..'.xml'},
  }
  tup.definerule{
    inputs = {'$(ROOT)/wiki/events/'..lang..'.xml'},
    command = "cat '%f' | ./make-loader.sh events > '%o'",
    outputs = {'$(ROOT)/wiki-events-'..lang..'.js'},
  }
end

function wikipedia_timeline_and_persons_and_events(lang)
  local index = wikipedia_timeline(lang)

  function f(title, iso)
    wikipedia_article(lang, title)
  end
  function g(title, iso)
    tup.definerule{
      command = "echo '"..iso.."' > '%o'",
      outputs = {dir..'/'..title}
    }
  end
  print(string.format("index.gray = %s", index.gray))
  project_index(index.gray, f)
  debug_table(index, "index")
  wikipedia_timeline_persons(lang, index)
  wikipedia_timeline_events(lang, index)
end

function us_presidents_en()
  local lang = 'en'
  local title = 'List_of_presidents_of_the_United_States'
  local output = 'us-presidents.xml'
  local article = wikipedia_article(lang, title)
  tup.definerule{
    inputs = {article},
    command = "xsltproc --html extract-us-presidents.xsl %f > "..shell_quote(output),
    outputs = {output},
  }
  tup.definerule{
    inputs = {output},
    command = "cat '%f' | ./make-loader.sh us_presidents > '%o'",
    outputs = {'$(ROOT)/wiki-us-presidents-'..lang..'.js'},
  }
end
function us_constitutional_amendments_en()
  local lang = 'en'
  local title = 'List_of_amendments_to_the_Constitution_of_the_United_States'
  local output = 'us-constitutional-amendments.xml'
  local article = wikipedia_article(lang, title)
  tup.definerule{
    inputs = {article},
    command = "xsltproc --html extract-us-amendments.xsl %f > "..shell_quote(output),
    outputs = {output},
  }
  tup.definerule{
    inputs = {output},
    command = "cat '%f' | ./make-loader.sh us_amendments > '%o'",
    outputs = {'$(ROOT)/wiki-us-amendments-'..lang..'.js'},
  }
end

wikipedia_timeline_and_persons_and_events('en')
us_presidents_en()
us_constitutional_amendments_en()
