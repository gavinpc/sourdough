tup.include('web.lua')

function wikipedia_article(lang, title, dir)
  local url = 'https://api.wikimedia.org/core/v1/wikipedia/'..lang..'/page/'..title..'/html'
  local file = (dir or '$(ROOT)/wiki/cache')..'/'..lang..'/'..title..'.html'
  local alias = 'wiki-'..lang..'-'..title
  return get_resource(url, file, alias)
end

-- example
-- wikipedia_article("en", "Baseball")
