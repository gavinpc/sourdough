// WIP: run database loader tests
{
  const doc = document;
  const container = doc.body.querySelector("main") ?? doc.body;

  const get_temp_database = async () => {
    const random = Math.round(Math.random() * 1e12);
    const name = `temp-${random}`;
    const request = indexedDB.open(name);
    const connection = async_request(request);
    return { name, connection };
  };
  /** @param db {IDBDatabase} */
  const get_temp_store = async db => {
    const random = Math.round(Math.random() * 1e12);
    const name = `temp-${random}`;
    const request = db.objectStore(name);
    return async_request(request);
  };

  const CASES = [{ records: [] }, { records: [{ a: 1 }, { a: 2 }] }];

  async function* run_test_cases() {
    try {
      const { name, connection: db } = await get_temp_database();
      async function run_test(test) {
        const given_records = test.records;
        const where = get_temp_store(db);
        const contents_after = await get_all_objects(where);
        if (equivalent(contents_after, given_records)) {
          say?.(`yay this test passed`);
        } else {
          warn?.(`sorry, this test failed`);
          warn?.(`I expected to get back ${given} but I got ${contents_after}`);
        }

        await something;
        const {} = given;
      }
      for (const test of CASES) run_test(test);
    } finally {
      // clear temp database -- or not
    }

    function run_loader_tests() {
      const element = doc.createElement("output");
      container.append(element);
      const log = doc.createElement("ol");
      const heading = doc.createElement("header");

      const _console = dom_console(log);

      heading.innerText = "IndexedDB loader tests";
      element.append(heading, log);

      const say = _console.log.bind(_console);
      const warn = _console.warn.bind(_console);

      async function start() {}
      function stop() {
        say?.("I have nothing to stop");
      }

      start();
      return { stop };
    }
  }
}
