#!/bin/bash

# Dump the contents of a resource if present.  Same args as find.

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
. "$this_script_dir"/sourceme.sh

cat_resource() {
    local what="$1"
    >&2 echo "Looking for ‘$what’"
    local found="$("$this_script_dir"/find.sh "$what")"
    if [ -z "$found" ]; then
        >&2 echo 'Not found'
        return 1
    fi
    local cache="$WWW_CACHE"
    # ASSUMES the name has no find globbing chars
    # ASSUMES there's only one content file per hash
    local file="$(find "$cache/internet" -maxdepth 1 -name "$found.*")"
    # local file="$cache/internet/$found".*
    # >&2 echo "yon file is $file"
    cat "$file"
}

cat_resource "$@"

exit 0
