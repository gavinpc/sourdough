#!/bin/bash

# Delete a resource from the cache and its metadata files.

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
. "$this_script_dir"/sourceme.sh

delete_resource () {
    local cache="$WWW_CACHE"
    local alias="$1"

    if [ -z "$alias" ]; then
        >&2 echo "Please specify the alias of the resource you want to delete."
        return 1
    fi

    # Assumes a match exists
    for file in $cache/alias/$alias.*; do
        if [ ! -L "$file" ]; then
            >&2 echo "alias $alias is not a link, so I don't know what other files relate to it."
            return 1
        fi

        echo "Alias found: $file..."
        local target=`readlink -f $file`
        
        if [ ! -z "$target" ]; then
            echo "Target is $target..."
            
            local hash="${target##*/}"
            hash="${hash%.*}"
            if [ -z "$hash" ]; then
                echo "Hash is $hash"
                
                local index_entry="$cache/index/$hash"
                if [ -f "$index_entry" ]; then
                    echo "Removing index entry"
                    rm "$index_entry"
                fi
            fi
            
            if [ -f "$target" ]; then
                echo "Removing resource"
                rm "$target"
            fi
        fi
        
        echo "Removing alias"
        rm $file
    done
}

delete_resource "$@"

exit 0
  
