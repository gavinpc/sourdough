#!/bin/bash

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
. "$this_script_dir"/sourceme.sh

purge_orphan_aliases() {
    local alias_dir="$WWW_CACHE/alias"
    find "$alias_dir" -xtype l | while read broken; do
        local alias="${broken##*/}"
        echo "Deleteing alias $alias" â missing $(realpath "$broken")
        rm "$broken"
    done
}

delete_resources() {
    local DEFAULT_CRITERIA='-size 0'
    local criteria="${1:-$DEFAULT_CRITERIA}"
    local cache="$WWW_CACHE"
    local content_dir="$cache/internet"
    local index_dir="$cache/index"

    shopt -s nullglob
    find "$content_dir" -type f $criteria | while read file; do
        local name="${file##*/}"
        local hash="${name%.*}"
        # echo okay I will delete "$file"
        # echo   which has name "$name"
        echo Deleting entry "$hash"...
        rm "$file"
        for index in "$index_dir/$hash".*; do
            rm "$index"
        done
    done
    # this could delete things beyond what was just deleted
    purge_orphan_aliases
}


delete_resources "$@"

exit 0

