#!/bin/bash

# Find a resource by any means.

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
. "$this_script_dir"/sourceme.sh

grep_escape() {
    # TBD: implement
    echo "$1"
}

# I mean, if it's a URL we can like, hash it right?
find_resource() {
    local cache="$WWW_CACHE"
    local what="$1"

    # check whether it's a hash
    if [ -f "$cache/index/$what" ]; then
        if [ ! -f "$cache/internet/$what".* ]; then
            >&2 echo "Assert fail, expected content file"
            return 1
        fi
        echo "$what"
        return
    fi

    # check whether it might be a URL in the store
    # of course, if it is then it would be cheaper to look for its sha directly
    local pat="$(grep_escape "$what")"
    # shouldn't be more than one result
    local file="$(grep --recursive --files-with-matches '^'"$pat"'$' "$cache/index")"
    if [ ! -z "$file" ]; then
        # this should be sha1sum of the URL (which is $what)
        local hash="$(basename "$file")"
        echo "$hash"
    fi
}

find_resource "$@"

exit 0
