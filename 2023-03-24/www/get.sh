#!/bin/bash

# Fetch with cache on local FS.

# BUG when a file has no extension, `type` gets the filename

set -eo pipefail

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
. "$this_script_dir"/sourceme.sh

get_resource() {
    local url="$1"
    local link="$2"
    local alias="$3"

    local cache="$WWW_CACHE"
    local internet_dir="$cache/internet"
    # >&2 echo "url = ${url}"
    # >&2 echo "link = ${link}"
    # >&2 echo "alias = ${alias}"

    local sha_full="$(echo "$url" | sha1sum)"
    local hash="${sha_full%% *}"     # cut off the trailing space and hyphen

    # >&2 echo "sha_full = ${sha_full}"
    # >&2 echo "hash = ${hash}"
    # >&2 echo "response = ${response}"

    # >&2 echo "internet_dir = $internet_dir"
    # >&2 echo "hash = ${hash}"

    
    # ./www/get.sh 'https://api.wikimedia.org/core/v1/wikipedia/en/page/AD_888/html' './wiki/cache/en/AD_888.html' 'wiki-en-AD_888'
    
    local type="${link##*.}"
    local response="$internet_dir/$hash.$type"
    # >&2 echo "type = ${type}"
    
    local index_dir="$cache/index"
    local index_file="$index_dir/$hash"

    # >&2 echo "GPC DEBUG actual file would be $response"
    if [ ! -f "$response" ]; then
        # >&2 echo "GPC DEBUG which doesn't exist"
        mkdir -p "$internet_dir"
        # wget --output-document="$response" "$url"
        >&2 echo "Getting $url"
        >&2 echo "hash: $hash"
        if ! curl --fail --location "$url" > "$response"; then
            rm "$response"
            return 1
        fi
        local did_fetch=yes
    # else
    #     >&2 echo "GPC DEBUG which DOES exist"
    fi

    if [ ! -f "$index_file" ]; then
        >&2 echo "Writing index for $hash"
        mkdir -p "$index_dir"
        echo "$url" > "$index_file"
    fi

    # Rather than copying the file, we just link to it.
    if [ -e "$link" ]; then
        if [ ! -L "$link" ]; then
            >&2 echo "Out file exists but is not a symlink! $link"
            return 1
        fi
        # We could confirm here that the link points to what it should
        # but that would require a call to `realpath`...
    else
        >&2 echo "Linking $hash → $link"
        ln --symbolic "$response" "$link"
    fi

    if [ ! -z "$alias" ]; then
        local alias_dir="$cache/alias"
        local alias_file="$alias_dir/$alias"

        if [ ! -f "$alias_file" ]; then
            >&2 echo "Aliasing $hash as “$alias”"
            mkdir -p "$alias_dir"
            ln --symbolic --relative "$response" "$alias_file"
        fi
    fi

    # When all changes are done, pause
    if [ "$did_fetch" == 'yes' ]; then
        local wait=$((5 + RANDOM % 10))
        >&2 echo "Waiting $wait seconds..."
        sleep $wait
    fi
}

get_resource "$@"

exit 0

