#!/bin/bash

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
. "$this_script_dir"/sourceme.sh

list_orphaned_indexes() {
    local cache="$WWW_CACHE"
    local index_dir="$cache/index"
    local content_dir="$cache/internet"
    find "$index_dir" -type f | while read index; do
        local name="${index##*/}"
        # should have no impact on index file, since they don't use extensions
        local hash="${name%.*}"
        # Assumes a hash will never be the substring of another hash
        # We need the glob because the content file may have an extension
        local content_file=$(compgen -G "$content_dir/$hash"*)
        if [ -z "$content_file" ]; then
            echo "$index"
        fi
    done
}

list_orphaned_indexes

exit 0

