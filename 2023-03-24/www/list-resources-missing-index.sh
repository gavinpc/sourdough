
#!/bin/bash

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
. "$this_script_dir"/sourceme.sh

list_resources_missing_index() {
    local cache="$WWW_CACHE"
    local content_dir="$cache/internet"
    local index_dir="$cache/index"
    find "$content_dir" -type f | while read content_file; do
        local name="${content_file##*/}"
        local hash="${name%.*}"
        # Unlike the reverse lookup, we don't need a glob here
        local index_file="$index_dir/$hash"
        if [ ! -f "$index_file" ]; then
            echo "$content_file"
        fi
    done
}

delete_orphaned_indexes() {
    list_orphaned_indexes | while read file; do
        echo Deleting "$file"
        rm "$file"
    done
}

list_resources_missing_index
# delete_orphaned_indexes

exit 0

