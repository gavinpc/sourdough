WWW_CACHE=$HOME/def.inet

__diff_indices() {
    find ~/def.inet/index -printf '%P\n' | sed 's/\..*$//g' | sort > b
    find ~/def.inet/internet -printf '%P\n' | sed 's/\..*$//g' | sort > a
    diff a b
}
