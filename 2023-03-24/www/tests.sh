#!/bin/bash

# PROVISIONAL.  initial sketches for a correctness test of web cache

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"

# test cases
# - get.sh
#   - happy path (getting an available resource by URL)
#     - puts the content into the /internet directory
#       - under the hash of the URL
#     - puts the URL itself into the /index directory
#       - under the hash of the URL
#   - fail because HTTP error
#     - does not change cache
#     - and exits with error status (the HTTP status?)

exit 0
