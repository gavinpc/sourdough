include_all_xsl() {
    echo '<?xml version="1.0" encoding="utf-8"?>'
    echo '<xsl:transform version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">'
    for file in "$@"
    do
        echo "<xsl:include href=\"$file\" />"
    done
    echo '</xsl:transform>'
}

include_all_xsl "$@"
