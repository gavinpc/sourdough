<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
  
  <xsl:template mode="inline-css" match="*">
    <xsl:for-each select="@*">
      <xsl:if test="position() &gt; 1">; </xsl:if>
      <xsl:value-of select="concat(name(), ': ', .)" />
    </xsl:for-each>
  </xsl:template>
  
  <xsl:template mode="inline-css" match="custom-properties">
    <xsl:for-each select="@*">
      <xsl:if test="position() &gt; 1">; </xsl:if>
      <xsl:if test=".">
        <xsl:value-of select="concat('--', name(), ': ', .)" />
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

</xsl:transform>
