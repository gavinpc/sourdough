<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0'
               xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
               xmlns:ex='http://exslt.org/common'
               xmlns:date='http://exslt.org/dates-and-times'>
  <xsl:import href="inline-css.xsl" />
    
  <xsl:template mode="main" match="time/@datetime">
    <xsl:variable name="date" select="date:date(.)" />
    <xsl:variable name="time" select="date:time(.)" />
    <xsl:variable name="styles">
      <custom-properties
        date-seconds="{date:seconds($date)}"
        date-year="{date:year($date)}"
        date-month-in-year="{date:month-in-year($date)}"
        date-day-in-month="{date:day-in-month($date)}"
        date-hour-in-day="{date:hour-in-day($time)}"
        date-minute-in-hour="{date:minute-in-hour($time)}"
        date-second-in-minute="{date:second-in-minute($time)}"
        />
    </xsl:variable>
    <xsl:copy-of select="." />
    <xsl:attribute name="style">
      <xsl:apply-templates
        select="ex:node-set($styles)/*"
        mode="inline-css"
        />
    </xsl:attribute>
  </xsl:template>

  
</xsl:transform>
