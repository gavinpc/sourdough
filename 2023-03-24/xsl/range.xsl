<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>

  <xsl:template name="range">
    <xsl:param name="from" select="0" />
    <xsl:param name="to" />
    <xsl:if test="$from &lt;= $to">
      <item><xsl:value-of select="$from" /></item>
      <xsl:call-template name="range">
        <xsl:with-param name="from" select="$from + 1" />
        <xsl:with-param name="to" select="$to" />
      </xsl:call-template>  
    </xsl:if>
  </xsl:template>
  
</xsl:transform>
