<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0'
               xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
               xmlns:ex='http://exslt.org/common'
               xmlns:date='http://exslt.org/dates-and-times'>
  <xsl:import href="range.xsl" />

  <xsl:template mode="time-label" match="node()|@*">
    <xsl:value-of select="." />
  </xsl:template>
  
  <!-- but only if empty? -->
  <xsl:template mode="main" match="*[@data-ruler-from-year and @data-ruler-to-year]">
    <xsl:variable name="years">
      <xsl:call-template name="range">
        <xsl:with-param name="from" select="@data-ruler-from-year" />
        <xsl:with-param name="to" select="@data-ruler-to-year" />
      </xsl:call-template>
    </xsl:variable>

    <xsl:copy>
      <xsl:apply-templates mode="main" select="@*" />
      <xsl:for-each select="ex:node-set($years)/*">
        <li>
          <time style="--date-seconds: {date:seconds(.)}" title="{.}">
            <xsl:apply-templates mode="time-label" select="." />
          </time>
        </li>
      </xsl:for-each>  
    </xsl:copy>    

  </xsl:template>

</xsl:transform>
