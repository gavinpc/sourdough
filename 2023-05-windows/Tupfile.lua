tup.include("sha1/init.lua")
print ('sha1 version = '..(sha1._VERSION))
print ('sha1("gavin") = '..sha1.sha1("gavin"))


-- web_resource('https://api.wikimedia.org/core/v1/wikipedia/en/page/2000/html', 'www/cache/test-2000', 'test-2000.html')
web_resource(
  'https://willshake.net/plays/Ham/1.1',
  '$(ROOT)/www/cache/ws/plays/Ham/1.1.html',
  'ws-Ham-1.1.html')

if is_windows then
  tup.definerule {
    command = "powershell ./test.ps1",
    outputs = {'psworld', '^NonInteractive'}
  }
end
