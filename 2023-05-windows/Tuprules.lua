ROOT = tup.getcwd()
tup.creategitignore()

-- For use in commands
tup.export('OS')
-- For use in Lua
is_windows = tup.getconfig('TUP_PLATFORM') == 'win32'

if is_windows then
  print("Running on Windows")
else
  print("Running on something that is not Windows")
end

tup.include("global.lua")
tup.include("graphviz/support.lua")
tup.include("www/support.lua")
tup.include("web/support.lua")
tup.include("xsl/support.lua")

---------------------------
-- Only for things you want EVERYWHERE!
all_dot_to_svg()
