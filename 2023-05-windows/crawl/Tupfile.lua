function foreach_line_in(file, map)
  local file = io.open(file, 'r')
  if file ~= nil then
    for line in file:lines() do
      map(line)
    end
    file:close()
  else
    -- no such file or couldn't open for read
  end
end

function read_links(list_file, line_to_rule)
  
  rule = line_to_rule(line)
  
end

function file_exists(name)
  local f = io.open(name, 'r')
  if f ~= nil then io.close(f) return true else return false end
end

function ignore_output(description)
  assert(description.outputs, 'expected description to have outputs')
  assert(#description.outputs == 1, 'expected 1 output')
  local first_char = string.sub(description.outputs[1], 1, 1)
  assert(first_char ~= '^', 'expected output not already ignored')
  description.outputs[1] = '^'..description.outputs[1]
  print('first output = '..description.outputs[1])
  return description
end

function crawl(url, name)
  local file = web_resource_new(url, name)
  -- for a crawl where we want to have downstream rules, this output needs to be
  -- marked as ignored... BUT, that's not the business of xslt* as such
  local desc = xslt1_description(file, 'get-all-links.xsl', name..'-links')
  local modified = ignore_output(desc)
  local output = desc.outputs[1]
  if (file_exists(output)) then
    print('hey cool the gray file "'..output..'" already exists')
  else
    print('oh noes the gray file "'..output..'" does not already exist!')
    local input = desc.inputs[1]
    print('can I touch one of its inputs like "'..input..'"?')
    tup.definerule({
        command = "touch "..input,
        outputs = {'^'..input}
    })
  end
  
  tup.definerule(modified)
end

crawl('https://en.wikipedia.org/wiki/12_BC', '12bc')
