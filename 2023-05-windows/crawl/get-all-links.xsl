<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
  <xsl:output omit-xml-declaration="yes" />
  <xsl:variable name="CR" select="'&#xA;'" />
	<xsl:template match="/">
    <xsl:for-each select="//a[starts-with(@href, '/wiki/')]">
      <xsl:value-of select="concat(@href, $CR)" />
    </xsl:for-each>
  </xsl:template>
</xsl:transform>
