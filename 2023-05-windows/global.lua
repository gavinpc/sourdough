-- I want these functions to be available “no matter what”
-- i.e. even if you've overridden the default rules.

function shell_quote(s)
  return "'"..s:gsub("'", "'\"'\"'").."'"
end
