#!/bin/sh

# Convert dot to svg

set -e

input="$1"
output="$2"

dot -Tsvg -o "$output" "$input"

exit 0
