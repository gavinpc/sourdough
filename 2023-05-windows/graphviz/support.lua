graphviz_dir = tup.getcwd()

function all_dot_to_svg()
  tup.foreach_rule(
    '*.dot',
    "^ dot → svg %B^ sh "..graphviz_dir.."/dot-to-svg.sh '%f' '%o'",
    {'%B.svg'}
  )
end
