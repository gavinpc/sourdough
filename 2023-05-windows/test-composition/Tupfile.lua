-- List_of_years
-- List_of_decades,_centuries,_and_millennia

function wikipedia_article(lang, title, dir)
  local url = 'https://api.wikimedia.org/core/v1/wikipedia/'..lang..'/page/'..title..'/html'
  local file = (dir or '$(ROOT)/wiki/cache')..'/'..lang..'/'..title..'.html'
  local alias = 'wiki-'..lang..'-'..title
  return web_resource(url, file, alias)
end

function main()
  list = wikipedia_article('en', 'List_of_years')
  xslt1(list, 'wiki-time-period-links.xsl', 'list1')
end

main()
