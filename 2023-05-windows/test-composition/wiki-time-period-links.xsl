<?xml version="1.0" encoding="utf-8"?>
<xsl:transform version="1.0"
               xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
               xmlns:notebook="https://gavinpc.com/composition-notebook"
               >
  <xsl:output type="text" omit-xml-declaration="yes" />
  <xsl:include href="./timeline-seed/en.xsl" />

  <!-- Emit a two-column, space-delimited table of title,ISO -->
  <!-- for all of the timeline articles linked from this MediaWiki markup -->

  <xsl:variable name="CR" select="'&#xA;'" />

  <xsl:template match="/">
    <xsl:for-each select="//a[starts-with(@href, './')][not(contains(@href, '#'))]">
      <xsl:variable name="title" select="substring-after(@href, './')" />
      <xsl:variable name="iso" select="notebook:en-title-to-iso($title)" />
      <xsl:if test="$iso != ''">
        <xsl:value-of select="concat($title, ' ', $iso, $CR)" />
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

</xsl:transform>
