-- BUT this doesn't do any waiting!

web_cwd = tup.getcwd()

function powershell_quote(s)
  return s
end

-- This command *might* work in powershell 2, if you can actually run it.  But
-- in practice that seems to be a problem. https://stackoverflow.com/q/69161207
-- As a result, we need the NonInteractive hack
function web_resource_new(url, alias)
  file = ''..alias
  if is_windows then
    tup.definerule{
      command =
        'powershell -version 3.0 -nologo -noninteractive -noprofile -File '..web_cwd..'/get.ps1'
        ..' '..powershell_quote(url)
        ..' '..powershell_quote(alias),
      outputs = {file, '^StartupProfileData-NonInteractive'}
    }
  else
    tup.definerule{
      command =
        'curl '
        ..' --output '..shell_quote(file)
        ..' '..shell_quote(url),
      outputs = {file}
    }
  end
  return file
end
