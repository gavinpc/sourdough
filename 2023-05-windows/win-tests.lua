
tup.definerule{ command = 'echo words > %o', outputs = {'book'} }
tup.definerule{ command = 'echo hello | sh ./catty.sh > %o', outputs = {'book2'} }

-- tup error: File 'C:/Users/gcannizzaro/research/sourdough/2023-05-windows/e022582115c10879-16852-pipe-nt-0x1' was written to, but is not in .tup/db. You probably should specify it as an output
--  -- Delete: C:/Users/gcannizzaro/research/sourdough/2023-05-windows/e022582115c10879-16852-pipe-nt-0x1
-- 
-- tup.definerule{
--   command = 'echo world | sh ./hashy.sh > %o',
--   outputs = {'hashed'}
-- }

-- 'C:' is not recognized as an internal or external command,
-- 
-- tup.definerule{
--   command = 'echo wsl | C:/Windows/System32/bash.exe ./hashy.sh > %o',
--   outputs = {'bashed'}
-- }

-- tup error: File 'C:/Users/gcannizzaro/research/sourdough/2023-05-windows/e022582115c10879-9208-pipe-nt-0x1' was written to, but is not in .tup/db. You probably should specify it as an output
--  -- Delete: C:/Users/gcannizzaro/research/sourdough/2023-05-windows/e022582115c10879-9208-pipe-nt-0x1
-- 
-- tup.definerule{
--   command = "sh -c 'echo wsl | C:/Windows/System32/bash.exe ./hashy.sh > %o'",
--   outputs = {'quoted'}
-- }

-- tup error: File 'C:/Users/gcannizzaro/research/sourdough/2023-05-windows/e022582115c10879-11256-pipe-nt-0x1' was written to, but is not in .tup/db. You probably should specify it as an output
--  -- Delete: C:/Users/gcannizzaro/research/sourdough/2023-05-windows/e022582115c10879-11256-pipe-nt-0x1
-- tup error: File 'C:/Users/gcannizzaro/research/sourdough/2023-05-windows/e022582115c10879-11092-pipe-nt-0x1' was written to, but is not in .tup/db. You probably should specify it as an output
--  -- Delete: C:/Users/gcannizzaro/research/sourdough/2023-05-windows/e022582115c10879-11092-pipe-nt-0x1
-- 
-- tup.definerule{
--   command = "sh -c 'echo word | sh ./hashy.sh > %o'",
--   outputs = {'quoted'}
-- }

-- tup error: File 'C:/Users/gcannizzaro/AppData/Local/Microsoft/Windows/PowerShell/StartupProfileData-NonInteractive' was written to, but is not in .tup/db. You probably should specify it as an output
--  -- Delete: C:/Users/gcannizzaro/AppData/Local/Microsoft/Windows/PowerShell/StartupProfileData-NonInteractive
-- 
-- tup.definerule{
--   command = "powershell ./hexy.ps1 > %o",
--   outputs = {'poshy'}
-- }

-- tup error: Expected to write to file 'happen' from cmd 155 but didn't
-- but *does* in fact write the file (and generate its gitignore entry)
-- 
-- tup.definerule{
--   command = "bash -c 'echo anything > %o'",
--   outputs = {'happen'}
-- }

-- same as previous
-- 
-- tup.definerule{
--   command = "^b^ echo anything > %o",
--   outputs = {'happen'}
-- }

-- Expected to write to file 'away' from cmd 155 but didn't
-- and in fact doesn't
-- 
-- tup.definerule{
--   command = "sh write.sh > %o",
--   outputs = {'away'}
-- }

-- /bin/bash: -c: line 1: unexpected EOF while looking for matching `''
-- /bin/bash: -c: line 2: syntax error: unexpected end of file
-- 
-- tup.definerule{
--   command = "^b^ echo world | ./hashy.sh > %o",
--   outputs = {'forced'}
-- }

-- /bin/bash: -c: line 1: syntax error near unexpected token `)'
-- /bin/bash: -c: line 1: `/bin/bash -e -o pipefail -c 'echo shell = $0')'
-- 
-- tup.definerule{
--   command = '^b^ echo shell = $0',
-- }

tup.definerule{
  command = "sh -c 'echo anything > %o'",
  outputs = {'shellno'}
}
tup.definerule{
  command = "sh basic.sh",
  outputs = {'now'}
}
tup.definerule{
  command = 'echo $0 > %o',
  outputs = {'shelly'}
}
