www_cwd = tup.getcwd()

function web_resource(url, file, alias)
  tup.definerule{
    command =
      'sh '..www_cwd..'/get.sh'
      ..' '..shell_quote(url)
      ..' '..shell_quote(file)
      ..' '..shell_quote(alias),
    outputs = {file, '^pipe-nt', '^/def.inet/'}
  }
  return file
end
