define("~/methods/map/object", [], () => {
  // this is a method implementation
  const { create, keys } = Object;
  function map_object(input, f, type = Object) {
    const output = create(type);
    for (const key of keys(input)) {
      output[key] = f(input[key], key, input);
    }
    return output;
  }
  map_object.comment = `Map a function over an object's values into a new plain object.`;
  return map_object;
});

define("~/js/is_plain_object", [], () => {
  // JavaScript Object is a type of thing
  // we can refer to it here as the well-known object Object
  // but in "plain object" we also include ones with null prototype
  // not `Object` but `typeof` "object" identifies the things
  const { getPrototypeOf, prototype: Object_prototype } = Object;
  function is_plain_object(o) {
    if (o === null || typeof o !== "object") return false;
    const prototype = getPrototypeOf(o);
    return prototype === Object_prototype || prototype === null;
  }
  is_plain_object.comment = `Answer true if the argument is a “plain ol’ JavaScript object,” one with a standard or null prototype.`;
  return is_plain_object;
});

define("~/unfiled/map", ["~/js/is_plain_object"], is_plain_object => {
  const { isArray } = Array;
  function map(thing, f) {
    // @@species is deprecated and I'm not sure that's not wrong...
    // can we say that a mapped T will make sense as a T?
    if (isArray(thing)) return thing.map(f);
    if (is_plain_object(thing)) return map_object(thing, f);
    throw new NotImplemented(map, thing, f);
  }
  map.comment = `Distribute the given function synchronously over a collection.`;
  return map;
});

define("~/io/dom/write", ["~/js/is_plain_object"], is_plain_object => {
  function dom_write(thing) {
    if (is_plain_object) {
    }
  }
  dom_write.comment = `Notate the given value as an Element.  Where possible, the expression can be read to an equivalent object.`;
  return dom_write;
});

define("~/dom/describe", ["def"], def => {
  const { isa, amd } = def;
  // still a pseudo-generic
  // should recognize/require language
  function dom_describe(thing, ...rest) {
    if (rest.length > 0)
      throw new def.BadRequest(`Bad arity ${rest.length + 1}`);
    if (isa(thing, amd.quote.define)) {
      return `why it's an AMD definition!`;
    }
    const js_type = typeof thing;
    if (js_type === "string") {
      if (thing.length < 100) {
        const ele = document.createElement("span");
        const q = document.createElement("q");
        q.append(thing);
        ele.append(`the string `, q);
        return ele;
      }
    }
    return js_type;
  }
  return dom_describe;
});

define("~/js/entries", [], () => {
  // another generic
  const Object_entries = Object.entries;
  function* entries(thing) {
    if (thing !== null && typeof thing === "object") {
      // covers Set, Map, Array (and thus Tuples)
      if (typeof thing.entries === "function") yield* thing.entries();
      // only makes sense for POJO's at this point
      // prototype methods won't be enumerated here
      yield* Object_entries(thing);
    }
  }
  entries.comment = `Iterate the key-values in a collections, as two-tuples`;
  return entries;
});

define("~/explore/search-thing", ["~/js/entries"], entries => {
  function* search_thing(thing, matchable, path = [], options = null) {
    switch (typeof thing) {
      // undefined | boolean | bigint | number → ∅
      case "symbol": {
        const id = Symbol.keyFor(thing);
        if (id !== undefined) {
          const result = matchable.match(id);
          if (result) yield [path, result, thing];
        }
        break;
      }
      case "function": {
        const text = thing.toString();
        const result = matchable.match(text);
        if (result) yield [path, result, thing];
        if (typeof thing.comment === "string") {
          const result = matchable.match(thing.comment);
          if (result) yield [[...path, "comment"], result, thing.comment];
        }
        break;
      }
      case "string": {
        const result = matchable.match(thing);
        if (result) yield [path, result, thing];
        break;
      }
      case "object": {
        for (const [key, value] of entries(thing)) {
          // really want cons here
          yield* search_thing(value, matchable, [...path, key], options);
        }
      }
    }
  }
  search_thing.comment = `Apply a text matcher to an object using in-order traversal.`;
  return search_thing;
});

define("~/explore/search-world", ["~/explore/search-thing"], search_thing => {
  function* search_world(matchable, options) {
    if (!(define?.definitions instanceof Map))
      throw new Error(`I need define.definitions to be a Map for this to work`);
    // what you really want here is the cache
    for (const name of define.definitions.keys()) {
      try {
        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        // infinite regress if you invoke something you were in the middle of defining
        // const thing = require(name);
        // yield* search_thing(thing, [name], matchable, options);
      } catch {}
    }
  }
  return search_world;
});

define("~/dom/id", [], () => {
  function ensure_id(element) {
    if (element.id) return element.id;
    const prefix = `${element.localName}-`;
    let n = 0;
    let candidate;
    do candidate = `${prefix}${++n}`;
    while (document.getElementById(candidate));
    return (element.id = candidate);
  }
  ensure_id.comment = `Return the element's ID, assigning a valid one as needed`;
  return ensure_id;
});

define("~/explore/test-search-world", [
  "~/home/new",
  "~/explore/search-world",
  "~/dom/id",
], (settle, search_world, ensure_id) => {
  const element = settle();
  const pattern = /test/gi;
  const chat = document.createElement("div");
  const input = document.createElement("input");
  ensure_id(input);
  chat.classList.add("chat");
  chat.append(input);
  element.append(chat);
  function to_dom(value) {
    return null;
  }
  let i = 0;
  for (const finding of search_world(pattern)) {
    return;
    const dom = to_dom(finding);
    console.log("wtf");
    if (++i > 10) break;
  }
});

define("~/explore/syndication", ["~/home/new", "~/dom/describe"], (
  settle,
  dom_describe,
  make_expando
) => {
  const element = document.createElement("article");
  const header = document.createElement("header");
  header.append("a world, a unit of syndication");
  element.classList.add("syndication");

  make_expando({ element, size: document.documentElement.clientHeight / 2 });

  {
    const expando_footer = document.createElement("footer");

    const kill_button = document.createElement("button");
    kill_button.setAttribute("data-command", "kill");
    kill_button.append("✕");

    const duplicate_button = document.createElement("button");
    duplicate_button.setAttribute("data-command", "duplicate");
    duplicate_button.append("+");

    expando_footer.classList.add("inner");
    expando_footer.append(kill_button, duplicate_button);
    element.append(expando_footer);
  }

  const amd_section = document.createElement("section");
  amd_section.setAttribute("data-section", "amd");
  {
    const amd_header = document.createElement("header");
    amd_header.append("amd");

    const define_section = document.createElement("section");
    define_section.setAttribute("data-section", "define");
    {
      const define_header = document.createElement("header");
      define_header.append("define");

      const definitions_section = document.createElement("section");
      definitions_section.setAttribute("data-section", "definitions");
      if (!define.definitions) {
        definitions_section.append(`define.definitions is not defined!`);
      } else if (!(define.definitions instanceof Map)) {
        definitions_section.append(`define.definitions is not a Map!`);
      } else if (define.definitions.size === 0) {
        definitions_section.append(`define.definitions is empty!`);
      } else {
        const definitions_list = document.createElement("dl");
        definitions_list.classList.add("flex");
        for (const [name, definition] of define.definitions) {
          const div = document.createElement("div");
          const dt = document.createElement("dt");
          const dd = document.createElement("dd");
          dt.append(name);
          const description = dom_describe(definition);
          dd.append(description);
          div.append(dt, dd);
          definitions_list.append(div);
        }
        definitions_section.append(definitions_list);
      }

      define_section.append(define_header, definitions_section);
    }

    const require_section = document.createElement("section");
    require_section.setAttribute("data-section", "require");
    {
      const require_header = document.createElement("header");
      require_header.append("require");

      const goals_section = document.createElement("section");
      goals_section.setAttribute("data-section", "goals");
      if (!require.goals) {
        goals_section.append(`require.goals is falsy!`);
      } else if (!Array.isArray(require.goals)) {
        goals_section.append(`require.goals is not an array!`);
      } else if (require.goals.length === 0) {
        goals_section.append(`require.goals is empty!`);
      } else {
        const goals_list = document.createElement("menu");
        for (const it of require.goals) {
          const item = document.createElement("li");
          const representation = dom_describe(it);
          item.append(representation);
          goals_list.append(item);
        }
        goals_section.append(goals_list);
      }

      const completed_section = document.createElement("section");
      completed_section.setAttribute("data-section", "completed");
      if (!require.completed) {
        completed_section.append(`require.completed is falsy!`);
      } else if (!Array.isArray(require.completed)) {
        completed_section.append(`require.completed is not an array!`);
      } else if (require.completed.length === 0) {
        completed_section.append(`require.completed is empty!`);
      } else {
        const completed_list = document.createElement("ol");
        for (const it of require.completed) {
          const item = document.createElement("li");
          const representation = dom_describe(it);
          item.append(representation);
          completed_list.append(item);
        }
        completed_section.append(completed_list);
      }
      require_section.append(require_header, goals_section, completed_section);
    }
    amd_section.append(amd_header, define_section, require_section);
  }

  function interpret(thing) {
    return `you want me to interpret ${thing}`;
  }

  const code_section = document.createElement("section");
  code_section.setAttribute("data-section", "code");
  code_section.classList.add("chat");
  {
    const code_header = document.createElement("header");
    // the big question... dialog or persistent input?
    const code_input = document.createElement("input");
    code_input.oninput = event => {
      const text = event.target.value;
      const interpretation = interpret(text);
      const div = document.createElement("div");
      // this is an I/O operation
      // I mean, you could ask for less than “describe”
      // the minimum would be to notate in a roundtrippable way
      const representation = dom_describe(interpretation);
      div.append(representation);
      stage_section.append(div);
    };
    code_header.append("dialog");
    code_section.append(code_header, code_input);
  }

  const stage_section = document.createElement("section");
  stage_section.setAttribute("data-section", "stage");
  {
    const stage_header = document.createElement("section");
    stage_header.append("stage");
    stage_section.append(stage_header);
  }

  element.append(header, amd_section, code_section, stage_section);
  settle({ size: 16 }).append(element);
  // ummm... an engine-provided eq (and cmp) for ordinals
  // indexedDB.cmp([0, 1, 1, 4, ["james", 1]],[0, 1, 1, 4, ["james", 1]])
});

/* PENDING return of processes
define("~/explore/event-logger", [
  "~/home/new",
  "~/adt/ring_buffers",
  "~/process/channels",
  "~/io/dom/write",
], (settle, buffers, channels, dom_write) => {
  const { assert } = console;
  const { make_sliding_buffer } = buffers;
  const { make_channel } = channels;
  const INTERESTING = {
    storage: ["url", "key", "newValue", "oldValue"],
    keydown: ["key", "shiftKey", "ctrlKey", "metaKey"],
    keyup: ["key", "shiftKey", "ctrlKey", "metaKey"],
    input: ["data", "inputType"],
    scroll: [],
    click: ["clientX", "clientY", "shiftKey", "ctrlKey", "metaKey"],
  };
  const INTERESTING_entries = Object.entries(INTERESTING);
  const handlers = new Map();

  const buffer = make_sliding_buffer(16);
  const channel = make_channel(buffer);

  function pick(source, keys) {
    const out = {};
    for (const key of keys) if (key in source) out[key] = source[key];
    return out;
  }

  function make_input_event_sources() {
    for (const [type, keys] of INTERESTING_entries) {
      function handle(event) {
        assert?.(event.type === type, `${event.type}≠${type}`);
        const now = new Date();
        const timestamp = now.toISOString();
        const selection = pick(event, keys);
        const record = { timestamp, type, selection };
        // we can't await here, but we can know whether this was blocked
        channel.put(record);
      }
      handlers.set(type, handle);
      globalThis.addEventListener(type, handle);
    }
    function dispose() {
      for (const [type, handler] of handlers)
        globalThis.removeEventListener(type, handler);
    }
    return { dispose };
  }

  const element = settle();
  async function main() {
    while (true) {
      const taken = await channel.take();
      // WAIT: you don't get to dom_write unless you have a size control
      const representation = dom_write(taken);
      // element.append(taken);
    }
  }
  main();

  make_input_event_sources();
});
*/

define("~/home/new", ["~/dom/home", "~/ui/blocks/sizable"], (
  container,
  make_sizable_block
) => {
  function make_home(spec) {
    const tag = typeof spec === "string" ? spec : spec?.tag ?? "div";
    const size = typeof spec === "number" ? spec : spec?.size ?? 24;
    const element = document.createElement("div");
    const inner = document.createElement(tag);
    container.prepend(element);
    make_sizable_block({ element, inner, size });
    // they don't need to know about the expando
    return inner;
  }
  make_home.comment = `Provide housing in the current document.`;
  return make_home;
});

define("~/remember/inputs", [], () => {
  const MYKEY = "~/remember/inputs";

  function get_remembered() {
    try {
      return JSON.parse(localStorage.getItem(MYKEY));
    } catch {
      return {};
    }
  }

  function restore_inputs(container = document.documentElement) {
    for (const item of restore_inputs_sequence(container)) {
    }
  }
  function* restore_inputs_sequence(container) {
    const record = get_remembered();
    if (record && typeof record === "object") {
      const remembered_inputs = Object.entries(record);
      for (const [selector, text] of remembered_inputs) {
        if (typeof text !== "string") continue;
        const element = container.querySelector(selector);
        if (element instanceof HTMLInputElement) element.value = text;
        if (element instanceof HTMLTextAreaElement) element.textContent = text;
        yield element;
      }
    }
  }

  function selector_for(element) {
    if (element.id) return `[id="${element.id}"]`;
  }

  function start_remember_inputs() {
    // Note that setting text in a textbox will not cause reflow
    setTimeout(restore_inputs, 100); // HACK: id might not have been set synchronously
    function inputted(event) {
      const { target } = event;
      let text;
      if (target instanceof HTMLInputElement) text = target.value;
      if (target instanceof HTMLTextAreaElement) text = target.textContent;
      if (typeof text === "string") {
        const record = get_remembered() ?? {};
        const key = selector_for(target);
        if (key === undefined) {
          console.warn("couldn't determine a selector for", target);
          return;
        }
        record[key] = text;
        const json = JSON.stringify(record);
        localStorage.setItem(MYKEY, json);
      }
    }

    globalThis.addEventListener("input", inputted);
    return { dispose: () => globalThis.removeEventListener("input", inputted) };
  }
  start_remember_inputs.comment = `simplest thing that could possibly remember and restore input values`;
  start_remember_inputs();
});

define("~/tbd/rdfa", [], () => {
  function print(value) {
    const element = to_rdfa(value);
    if (element) {
      const container = find_container_for(element);
      if (container) container.append(element);
    }
  }
});

define("~/notes/web-namespaces", ["def"], def => {
  const foo = "https://developer.mozilla.org/en-US/docs/Web/API/StorageEvent";
  const WEB_API_NS = "https://developer.mozilla.org/en-US/docs/Web/API/";
  def.mint;
});

define("~/explore/pattern-action-rule", [], () => {
  function start_rule(spec) {
    const { pattern, action } = spec;
    const selector = pattern;
    document.querySelector(selector);
    match_now(selector, document);
    match_later(selector, document);
    // a > :first-child
    function dispose() {}
    return { dispose };
  }
});

define("~/explore/extend-timeframe", [
  "~/explore/pattern-action-rule",
], start_rule => {
  function css([x]) {
    return x;
  }

  // whenever a child is added with a timestamp
  // ensure that the timeframe includes it
  rule({
    pattern: css`[data-time]`,
    action(element) {
      const time = element.getAttribute("data-time");
      const scalar = parseFloat(time);
      if (isNaN(scalar)) return;
    },
  });
});
define("~/TRASH/ui/sizable-blocks", [], () => {
  `NO!!!!!!!!!!!!!!!!!!!!! this is the defunct one`;
  const { abs, min, max, floor } = Math;
  const ONCE = Object.freeze({ once: true });
  const is_number = x => typeof x === "number" && !isNaN(x);
  // const behavior = "smooth"
  const behavior = undefined;

  function make_sizable_block({ element }) {
    let _interval;
    const { style } = element;
    element.classList.add("sizable-block");
    const container = document.documentElement;
    const header = document.createElement("header");
    header.setAttribute("data-part", "controls");
    element.append(header);
    {
      const button = document.createElement("button");
      button.setAttribute("data-command", "sizable:grow");
      button.append("grow");
      header.append(button);
    }
    {
      const button = document.createElement("button");
      button.setAttribute("data-command", "sizable:release");
      button.append("release");
      header.append(button);
    }

    const alpha = 1 / 3;
    function grow() {
      const max_size = container.clientHeight;
      const box = element.getBoundingClientRect();
      const headroom = box.top;
      const footroom = max_size - box.bottom;
      const size = box.height;
      const delta = floor(size * alpha);
      let grow_by = NaN;
      let scroll_by = NaN;

      if (headroom < 0) {
        console.debug(`headroom ${headroom} < 0`);
        scroll_by = headroom;
      } else if (footroom < 0) {
        if (footroom > -1) {
          console.debug(`footroom ${footroom} ∈ (-1, 0)`);
          // we're within 1px of end
          const room = min(delta, headroom);
          if (room > 0) {
            grow_by = room;
            scroll_by = room;
          }
        } else {
          console.debug(`footroom ${footroom} ≤ -1`);
          scroll_by = -footroom;
        }
      } else if (footroom > 0) {
        if (delta <= footroom) {
          console.debug(`delta ${delta} ≤ footroom ${footroom}`);
          grow_by = delta;
        } else {
          console.debug(`delta ${delta} > footroom ${footroom}`);
          // but this could run afoul of header
          grow_by = delta;
          scroll_by = delta - footroom;
        }
      } else if (headroom > 0) {
        if (delta <= headroom) {
          console.debug(`delta ${delta} ≤ headroom ${headroom}`);
          grow_by = delta;
          scroll_by = -delta;
        } else {
          console.debug(`delta ${delta} > headroom ${headroom}`);
          grow_by = headroom;
          scroll_by = delta - headroom;
        }
      }

      if (is_number(scroll_by)) {
        container.scrollBy({ top: scroll_by, behavior });
      }
      if (is_number(grow_by)) {
        const new_size = size + grow_by;
        style.setProperty("--block-size-px", new_size);
        setTimeout(_recheck, 0); // next tick
        setTimeout(_recheck, 10); // next tick
        element.addEventListener("transitionend", _recheck, ONCE);
        element.addEventListener("animationend", _recheck, ONCE);
      }
    }

    function release() {
      style.removeProperty("--block-size-px");
    }

    function _recheck() {
      const box = element.getBoundingClientRect();
      const size = box.height;
      const max_size = container.clientHeight;
      const headroom = box.top;
      const footroom = max_size - box.bottom;
      console.debug(`recheck`, { headroom, footroom });
    }

    function mousedown(event) {
      if (event.target.closest(`[data-command="sizable:grow"]`)) {
        grow();
        _interval = setInterval(grow, 50);
      }
    }
    function mouseup(event) {
      clearInterval(_interval);
      if (event.target.closest(`[data-command="sizable:release"]`)) release();
    }

    function start() {
      element.addEventListener("mousedown", mousedown);
      element.addEventListener("mouseup", mouseup);
    }
    function stop() {
      clearInterval(_interval);
      element.removeEventListener("mousedown", mousedown);
      element.removeEventListener("mouseup", mouseup);
      element.removeEventListener("transitionend", _recheck);
      element.removeEventListener("animationend", _recheck);
    }
    return { start, stop };
  }

  return {
    redirectTo: "~/ui/blocks/sizable",
    comment: `this version scrolls to minimize headroom. impossible & hostile`,
  };
});

define("~/dom/ancestor", [], () => {
  return function dom_is_ancestor_of(a, b) {
    if (!a || !b) return false;
    const p = b.parentNode;
    return a === p || dom_is_ancestor_of(a, p);
  };
});

define("~/dom/devolve", ["~/dom/ancestor"], is_ancestor_of => {
  function dom_devolve(given) {
    const outer = given?.outer ?? document.createElement("div");
    const inner = given?.inner ?? document.createElement("div");
    const skip = given?.skip;
    if (!is_ancestor_of(outer, inner)) outer.append(inner);

    const observer = new MutationObserver(changes => {
      for (const { addedNodes, nextSibling } of changes) {
        for (const node of addedNodes) {
          if (node === inner) continue;
          if (nextSibling) inner.prepend(node);
          else inner.append(node);
        }
      }
    });
    {
      // start by shunting all existing... doesn't place around existing content though
      const childNodes = [...outer.childNodes];
      for (const child of childNodes) {
        if (child === inner) continue;
        if (skip && child instanceof Element && child.matches(skip)) continue;
        inner.appendChild(child);
      }
    }

    observer.observe(outer, { childList: true });
    return { dispose: () => observer.disconnect() };
  }
  dom_devolve.comment = `Shunts all incoming elements into a descendant container`;
  dom_devolve.seeAlso = `https://developer.mozilla.org/en-US/docs/Web/API/Element/attachShadow`;
  return dom_devolve;
});

define("~/ui/blocks/sizable", ["~/dom/devolve"], dom_devolve => {
  const { abs, min, max, floor } = Math;
  const is_number = x => typeof x === "number" && !isNaN(x);

  function make_sizable_block(given) {
    const outer = given.element ?? document.createElement("div");
    const inner = given.inner ?? document.createElement("div");

    let _interval = NaN;
    const console = given.console ?? globalThis.console;
    outer.classList.add("sizable");
    inner.classList.add("inner");

    const scroll_container = document.documentElement;
    {
      const button = document.createElement("button");
      button.setAttribute("data-command", "grow");
      button.classList.add("header");
      button.append("grow");
      button.onclick = event => event.preventDefault(); // in case it's on a form
      outer.prepend(button);
    }

    function set_size(new_size) {
      if (!is_number(new_size)) return console.warn(`expected number size`);
      const effective = max(min_size, new_size);
      outer.style.setProperty("--block-size-px", effective);
      // But we never unset this...
      if (!outer.getAttribute("data-grown"))
        outer.setAttribute("data-grown", new Date().toISOString());
    }

    const alpha = 1 / 11;
    const min_size = 24 + 24; // need extra for borders...
    const STEPS = 64;
    const STEP = 1 / STEPS;

    const identity = x => x;
    // provisional: monad over [0,1] to redistribute, say, towards ends
    const rescale = identity;
    // needs to orbit at 0
    // should be differentiable -- to one degree, for leverage
    // truthy means no shrinking was done, nor will until this situation changes
    function shrink() {
      const box = outer.getBoundingClientRect();
      const container_size = scroll_container.clientHeight;
      const start = box.top; // for block direction
      const size = box.height; // for block direction
      const max_size = container_size - start; // max it could get without scroll
      if (size > max_size) return;
      const size_range = max_size - min_size;
      const h = size - min_size;
      const t = h / size_range;
      if (t > 1)
        return console.warn("assert fail t>1", { start, size, max_size });
      const s = max(0, t - STEP);
      const r = rescale(s);
      const target = floor(min_size + size_range * r);
      if (target <= min_size) release();
      else set_size(target);
    }
    // truthy if fixpoint
    function grow() {
      const max_size = scroll_container.clientHeight;
      const box = outer.getBoundingClientRect();
      const headroom = box.top;
      const footroom = max_size - box.bottom;
      const size = box.height;
      let grow_by = NaN;

      if (headroom < 0) {
        // we begin before the viewport, our top is clipped
        // but scrolling would be chaotic, so fall through
        // console.debug(`headroom ${headroom} < 0`);
        stop_growing();
      }
      if (footroom < 0) {
        // we end after the viewport, our bottom is clipped
        if (footroom > -1) {
          // console.debug(`footroom ${footroom} ∈ (-1, 0)`);
        } else {
          // console.debug(`footroom ${footroom} ≤ -1`);
        }
      } else if (footroom > 0) {
        // console.debug(`footroom ${footroom}`);
        // we end before the view, we have room to grow
        // claim some portion of what footroom remains
        // this may lead to a reflow below
        grow_by = floor(footroom * alpha);
      } else if (headroom > 0) {
        // we start after the view, we have room to move up
        // but scrolling would move the bottom *away* from the viewport end
        // besides programmatic scrolling being generally bad
        stop_growing();
      }

      if (is_number(grow_by)) {
        const old_size = box.height;
        const new_size = size + grow_by;
        const e = Math.abs(new_size - old_size);
        set_size(new_size);
        return e < 1;
      }
    }

    function release() {
      outer.removeAttribute("data-grown");
      outer.style.setProperty("--block-size-px", min_size);
    }

    function grow_and_keep_growing(event) {
      if (isNaN(_interval)) {
        grow();
        _interval = setInterval(grow, 50);
      }
    }
    function shrink_and_keep_shrinking(event) {
      if (isNaN(_interval)) {
        shrink();
        _interval = setInterval(shrink, 50);
      }
    }
    function stop_growing(event) {
      clearInterval(_interval);
      _interval = NaN;
    }

    function keydown(event) {
      const button = event.target.closest(`[data-command="grow"]`);
      if (button) {
        if (event.key === "Enter") {
          if (event.shiftKey) shrink();
          // hmm, you get this for free with keyboard repeat
          // which uses the system preference for repeat delay & rate
          // but it won't match what mouse/touch does
          else grow(); //  grow_and_keep_growing();
          event.stopPropagation();
        }
        if (event.target.closest(`[data-command="release"]`)) release();
      }
    }
    function keyup(event) {
      if (event.key === "Enter") {
        if (event.target.closest(`[data-command="grow"]`)) stop_growing();
        event.preventDefault();
      }
    }

    function wheel_capture(event) {
      // if an outer sizable (such as yourself) gets a grow message and can,
      // obey it and stop propagation
      if (!event.shiftKey) return;
      const { deltaX, deltaY, shiftKey } = event;
      if (deltaX) return; // straight verticals ONLY
      if (deltaY < 0) {
        if (shrink()) event.stopPropagation();
      } else if (deltaY > 0) {
        if (!grow()) event.stopPropagation();
      }
    }
    function wheel(event) {
      if (!event.shiftKey) return;
      const { deltaX, deltaY, shiftKey } = event;
      if (deltaX) return; // straight verticals ONLY
      if (deltaY < 0) {
        if (shrink()) event.stopPropagation();
      } else if (deltaY > 0) grow();
    }
    function mousedown(event) {
      if (event.button !== 0) return;
      if (event.target.closest(`[data-command="grow"]`)) {
        if (event.shiftKey) shrink_and_keep_shrinking();
        else grow_and_keep_growing();
        event.stopPropagation();
      }
      if (event.target.closest(`[data-command="release"]`)) release();
    }
    function mouseup(event) {
      if (event.button !== 0) return;
      if (event.target.closest(`[data-command="grow"]`)) stop_growing();
      if (event.target.closest(`[data-command="release"]`)) release();
    }

    const devolver = dom_devolve({ outer, inner, skip: ".header" });
    outer.addEventListener("wheel", wheel);
    outer.addEventListener("wheel", wheel_capture, true);
    outer.addEventListener("mousedown", mousedown);
    outer.addEventListener("mouseup", mouseup);
    outer.addEventListener("keydown", keydown);
    outer.addEventListener("keyup", keyup);
    if (is_number(given.size)) set_size(given.size ?? min_size);
    function dispose() {
      devolver.dispose();
      stop_growing();
      outer.removeEventListener("wheel", wheel);
      outer.removeEventListener("keydown", keydown);
      outer.removeEventListener("keyup", keyup);
      outer.removeEventListener("mousedown", mousedown);
      outer.removeEventListener("mouseup", mouseup);
    }

    return { dispose, element: outer };
  }
  make_sizable_block.comment = `block size provider, i.e. a sizable block`;
  return make_sizable_block;
});

define("~/s-makes-sizable", ["~/ui/blocks/sizable"], make_sizable_block => {
  // it's “constructable”
  // but it's not a datatype, it's a dom trait of a BLOCK
  // entails grow and shrink commands
  // need to tell the system that it's a kind of thing that you can make
  // def.install.constructable;

  // up to and including
  function* following_sibling(from, to) {
    while (from) {
      yield from;
      if (from === to) break;
      from = from.nextElementSibling;
    }
  }

  function wrap_in_sizable(from, to, size) {
    const element = document.createElement("div");
    console.assert(from.parentNode === to.parentNode);
    const old_parent = to.parentNode;
    const old_sibling = to.nextElementSibling;
    const sizable = make_sizable_block({ element, size });
    element.append(...following_sibling(from, to));
    old_parent.insertBefore(element, old_sibling);
    return sizable;
  }

  function make_selection_sizable() {
    const selection = getSelection();
    if (selection.isCollapsed || selection.rangeCount < 1) return;
    const range = selection.getRangeAt(0);
    const common = range.commonAncestorContainer;
    let start = range.startContainer;
    let end = range.endContainer;
    if (start !== common)
      while (start.parentNode !== common) start = start.parentNode;
    if (end !== common) while (end.parentNode !== common) end = end.parentNode;
    range.setStartBefore(start);
    range.setEndAfter(end);
    const box = range.getBoundingClientRect();
    const size = box.height;
    const sizable = wrap_in_sizable(start, end, size);
    const grow_button = sizable.element.querySelector(`[data-command="grow"]`);
    grow_button?.focus();
  }
  make_selection_sizable.comment = `Wrap selection in a sizable block.  Rather than clipping to exact selection points, it first extends to encompass complete sibling blocks.`;

  function keyup(event) {
    if (event.key === "s") make_selection_sizable();
  }
  addEventListener("keyup", keyup);
});

define("~/sections-are-sizable", [
  "~/ui/blocks/sizable",
], make_sizable_block => {
  function one_time_type_rule(spec) {
    const { root, selector, type, rewrite } = spec;
    const TYPE = new RegExp(`\b${type}\b`);
    const member = element => !!element.getAttribute("typeof")?.matches(TYPE);
    const matches = root.querySelectorAll(selector);
    for (const element of matches) if (!member(element)) rewrite(element);
  }

  const type = `http://def.codes/ns/dom/sizable`;
  const spec = {
    root: document,
    selector: "main,section,article,aside",
    type,
    rewrite(element) {
      const current = element.getAttribute("typeof");
      const extended = current ? `${current} ${type}` : type;
      element.setAttribute("typeof", extended);
      make_sizable_block({ element, size: 20 });
    },
  };
  one_time_type_rule(spec);
  // or more generally, register interpreters of this type
});
define("~/explore/ui/record-tables", [], () => {
  let _id = 0;

  const { create } = Object;
  const { get } = Reflect;
  const { iterator } = Symbol;
  const { assert } = console;
  const { BadRequest } = def.errors;

  function key_from(label) {
    return label
      .trim()
      .replace(/ /g, "_")
      .replace(/[^a-z0-9_$]/gi, "");
  }

  // does appends which can cause reflow!
  function make_record_table(spec) {
    // but this should be additionally wrapped in something
    // for controls and meta stuff
    const table = spec?.table ?? document.createElement("table");
    const caption =
      table.querySelector(`:scope > caption`) ??
      table.insertBefore(document.createElement("caption"), table.firstChild);
    const head =
      table.querySelector(`:scope > thead`) ??
      table.insertBefore(document.createElement("thead"), table.firstChild);
    const body =
      table.querySelector(`:scope > tbody`) ??
      table.appendChild(document.createElement("tbody"));

    // The plain objects...  but if we don't make the user-facing thing a proxy,
    // then we'll lose the ability to intercept some operations, right?
    const schema = [];
    const records = [];

    function maybe_property(term) {
      // but this could be ambiguous
      return schema.find(meta => meta.includes(term));
    }

    const record_target = create(null);
    const record_proxy = new Proxy(record_target, {
      get(target, key, receiver) {
        if (!(key in target)) {
          const index = parseInt(key, 10);
          if (isNaN(index)) {
            const property = maybe_property(key);
            if (property) key = property;
            // else fall through and probably undefined
          } else if (index < schema.length && index >= -schema.length)
            key = schema.at(index); // look up by column number
        }
        return get(target, key, receiver);
      },
    });

    // name can change...
    // also munge text into an identifier
    const name = caption.textContent || `table${++_id}`;
    if (!caption.textContent) caption.textContent = name;
    const table_meta = { name, schema };

    if (!(table instanceof HTMLTableElement))
      throw BadRequest(`wanted a table`);

    // read the schema
    // ASSUMES a thead with one heading per cell
    for (const row of head.children) {
      for (const cell of row.children) {
        const label = cell.textContent; // but make it an identifier
        const key = key_from(label);
        const meta = { label, key };
        schema.push(meta);
      }
      break;
    }
    // if you were given a table, read it
    for (const row of body.children) {
      const record = create(record_proxy);
      const max = Math.min(row.children.length, schema.length);
      for (let i = 0; i < max; i++) {
        const cell = row.children[i];
        const value = cell.textContent; // datatype I/O goeth here
        const key = schema[i].key;
        record[key] = value;
      }
      records.push(record);
    }

    function push1(item) {
      if (item == null) return;
      if (typeof item !== "object") throw BadRequest(`expected object`);
      const row = document.createElement("tr");
      if (Array.isArray(item)) {
        const max = Math.min(schema.length, item.length);
        for (let i = 0; i < max; i++) {
          const { key } = schema[i];
          const cell = document.createElement("td");
          const value = item[i];
          cell.textContent = `${value}`;
          row.append(cell);
        }
      } else {
        for (let i = 0; i < schema.length; i++) {
          const { key } = schema[i];
          const cell = document.createElement("td");
          if (key in item) {
            const value = item[key];
            cell.textContent = `${value}`;
          }
          row.append(cell);
        }
      }
      body.append(row);
    }

    // need to do after the above
    Object.defineProperties(records, {
      $: { enumerable: true, value: table_meta },
      push: {
        enumerable: true,
        value(...items) {
          for (const item of items) push1(item);
        },
      },
    });

    /* not used yet
    const observer = new MutationObserver(changes => {
      for (const change of changes) {
        assert(change.type === "childList", "childLIst");
        console.warn("not implemented, update record from table");
        for (const added of change.addedNodes) {
          if (added instanceof HTMLTableRowElement) {
            console.debug("row added");
          } else if (added instanceof HTMLTableCellElement) {
            console.debug("cell added");
          }
        }
      }
    });
*/
    // but this would be covered by text mutations
    // and we'd need to listen for those anyway
    function inputted(event) {
      console.warn("not implemented, update record from table");
      console.debug("INPUTTED", event);
    }
    /*
    for (const element of [table, head, body])
      observer.observe(element, { childList: true });
*/
    table.addEventListener("input", inputted);
    function dispose() {
      // observer.disconnect();
      table.removeEventListener("input", inputted);
    }

    return { dispose, element: table, object: records };
  }

  return { make_record_table };
});

define("~/explore/def/tables", [], () => {
  const { create } = Object;
  const { ownKeys, get } = Reflect;
  const proxy = new Proxy(
    {},
    {
      ownKeys(target) {
        // we can look in the document for all (qualifying?) tables
        // where qualifying = not nested and not explicitly opt-out
        return ownKeys(target);
      },
      get(target, key, receiver) {
        if (!(key in target) && !(key in receiver)) {
          // we can look for a table that we know about with a similar name
          // we can look in the document for a table with a caption like this
          const needle = key.toLowerCase();
          for (const own of ownKeys(receiver)) {
            if (own.toLowerCase().includes(needle)) {
              return get(receiver, own, receiver);
            }
          }
        }
        return get(target, key, receiver);
      },
    }
  );
  const tables = create(proxy);
  return tables;
});

define("~/explore/ui/record-tables/read-all", [
  "~/explore/def/tables",
  "~/explore/ui/record-tables",
], (tables, record_tables) => {
  Object.assign(globalThis, { tables });

  // DUPLICATED above
  function key_from(label) {
    return label
      .trim()
      .replace(/ /g, "_")
      .replace(/[^a-z0-9_$]/gi, "");
  }

  return function main() {
    const { make_record_table } = record_tables;
    const selector = `table`; // all tables, sure why not
    const matches = document.querySelectorAll(selector);
    for (const table of matches) {
      const record_table = make_record_table({ table });
      const key = key_from(record_table.object.$.name);
      tables[key] = record_table.object;
    }
  };
});

define("~/explore/ui/record-tables/userland-create-command", [
  "~/explore/ui/record-tables",
], record_tables => {
  // BUT things created this way don't appear on the global
  const { make_record_table } = record_tables;
  function main() {
    function clicked(event) {
      const { target } = event;
      if (target.getAttribute("data-command") !== "create-record-table") return;
      const record_table = make_record_table();
      target.insertAdjacentElement("afterend", record_table.element);
      event.preventDefault();
    }
    addEventListener("click", clicked);
    return { dispose: () => removeEventListener("click", clicked) };
  }
  main.comment = `register handler to listen to command for creating a new record table`;
  return main;
});
define("~/explore/list-and-tree-maps/figures", [], () => {
  function make_figure(spec) {
    const element = spec?.element ?? document.createElement("figure");
    // choose a record collection
    // gives choices of which property to use as map scalar
    // a list map doesn't *have* to be on a record collection
    return { element };
  }
  return { make_figure };
});

define("~/populate-sections", [], () => {
  function get_section_label(element) {
    const label = element.querySelector(`:scope > :is(h1, h2, h3, h4, h5, h6)`);
    return label?.textContent;
  }
  function* document_top_level_sections() {
    for (const element of document.querySelectorAll("section")) {
      if (element.parentElement.closest("section")) continue;
      yield element;
    }
  }

  return function main() {
    const sections = [...document_top_level_sections()];
    const annotated_sections = sections.map(element => ({
      element,
      label: get_section_label(element),
    }));

    const selector = `[data-source="document-top-level-sections"]`;
    const matches = document.querySelectorAll(selector);
    for (const element of matches) {
      for (const { section, label } of annotated_sections) {
        const item = document.createElement("li");
        item.append(label);
        element.append(item);
      }
    }
  };
});

/* PENDING return of process/streams
define("~/explore/place_picker", ["~/process/streams"], streams => {
  const { make_subscribable } = streams;
  const locations = make_subscribable();
  function make_place_picker() {
    const element = document.createElement("menu");
    {
      // system places
      for (const key of Object.keys(globalThis)) {
        const item = document.createElement("li");
        const address = `javascript:${key}`;
        item.setAttribute("data-address", address);
        item.insertAdjacentHTML("afterbegin", `javascript:<b>${key}</b>`);
        element.append(item);
      }
    }
    function changed(location) {
      locations.next(location);
    }
    function dispose() {
      locations.dispose();
      element.remove();
    }
    return { dispose, element };
  }
  make_place_picker.comment = `a thing for selecting a location (or locations?) in the system/knowledge graph`;
  return make_place_picker;
});
*/

define("~/js/regexp/escape", [], () => {
  // https://github.com/tc39/proposal-regex-escaping/blob/main/polyfill.js
  // seeAlso https://github.com/tc39/proposal-regex-escaping/issues/37
  // This is safe for whole patterns.  The objections are to certain joins.
  const RegExp_escape = s => String(s).replace(/[\\^$*+?.()|[\]{}]/g, "\\$&");
  RegExp_escape.comment = `Answer with a RegExp pattern treating all input characters as literals`;
  return RegExp_escape;
});

define("~/def/generics/take2", [], () => {
  function make_generic(spec) {
    const methods_by_arity = new Map();

    const { name, signatures } = spec;
    if (typeof name !== "string" || !name)
      throw Error(`BadRequest: need string name`);
    if (
      !Array.isArray(signatures) ||
      signatures.some(it => typeof it !== "function")
    )
      throw Error(`BadRequest: ‘signatures’ must be an array of functions`);

    function is_applicable(method, args) {
      return false;
    }

    function* iterate_applicable_methods(args, methods) {
      for (const method of methods)
        if (is_applicable(method, args)) yield method;
    }

    function determine_method_or_die(args) {
      const arity = args.length;
      if (arity === 0) throw Error(`BadRequest: illegal arity ${arity}`);
      const methods = methods_by_arity.get(arity);
      if (!methods) throw Error(`NotImplemented: no methods for ‘${name}’`);
      const candidates = [...iterate_applicable_methods(args, methods)];
      if (candidates.length === 0) throw Error(`NotImplemented: ‘${name}’`);
      if (candidates.length === 1) return candidates[0];
      // multiple applicable methods
      // if we have a combinator, combine; else choose preferred
    }

    function dispatch(...args) {
      const method = determine_method_or_die(args);
      console.assert(typeof method === "function");
    }
    const generic = { [name]: dispatch }[name];

    function add_method(id, multimethod) {}
    function remove_method(id) {
      // find the collection and remove this one
    }

    const methods = Object.defineProperties(
      {},
      {
        add: { enumerable: true, value: add_method },
        remove: { enumerable: true, value: remove_method },
      }
    );
    Object.defineProperties(generic, {
      methods: { enumerable: true, value: methods },
    });
    return generic;
  }
  make_generic.comment = `A simpler approach to generics, this time with no notion of subsumption`;
  return { make_generic };
});

define("~/def/io", ["~/def/generics/take2"], generics => {
  // const { any } = ranges;
  const { make_generic } = generics;

  // syntax is a type, and you're expected to return that type from the method.
  // syntax is a "class" (instanceof target), NOT an arbitrary constraint

  const write = make_generic({
    name: "write",
    signatures: [(thing, syntax) => {}],
  });
  const read = make_generic({
    name: "read",
    signatures: [(thing, syntax) => {}],
  });

  return { read, write };
});

define("~/explore/tree-map-of-world", ["~/home/new", "~/explore/adt/tries"], (
  settle,
  tries
) => {
  const { make_trie } = tries;
  const directory = make_trie();
  for (const name of define.definitions.keys())
    directory.add(name.split(/[:/]/g));

  {
    function make_tree_map(spec) {
      // const { root, children, size, label } = spec;
      function make_node(it) {
        const element = document.createElement("div");
        // element.setAttribute("data-label", spec.label(it));
        let total = 0;
        for (const [name, child] of spec.children(it)) {
          const next = make_node(child);
          const size = spec.size(child);
          total += size;
          const label = document.createElement("span");
          label.textContent = name;
          element.setAttribute("data-name", name);
          // element.append(label);
          element.append(next);
        }
        for (const [name, child] of spec.children(it)) {
          const size = spec.size(child);
          element.style.setProperty("--size-portion", size / total);
        }
        return element;
      }
      const element = make_node(spec.root);
      element.classList.add("tree-map");
      return { element };
    }
  }

  const tree_map = make_tree_map({
    root: directory,
    children: node => node.children,
    size: node => node.size,
    // label: node => node.data,
  });
  settle({ size: 16 * 8 }).append(tree_map.element);
});

define("~/explore/search-world-dialog", ["~/home/new", "~/js/regexp/escape"], (
  settle,
  RegExp_escape
) => {
  const { assert } = console;
  const form = document.createElement("form");
  const input = document.createElement("input");
  const output = document.createElement("output");
  form.classList.add("chat");
  input.type = "search";
  form.append(input, output);

  function* search_in(map, rex) {
    for (const term of define.definitions.values()) {
      switch (term.length) {
        case 2: {
          const [name, value] = term;
          assert(typeof name === "string");
          // what assumptions do we make about value here?
          if (rex.test(name)) yield term;
          break;
        }
        case 3: {
          const [name, _needs, factory] = term;
          assert(typeof name === "string");
          assert(typeof factory === "function", "no f'n factory", term);
          if (rex.test(name) || rex.test(factory.toString())) yield term;
          break;
        }
      }
    }
  }

  function search_world_for(term) {
    const pattern = RegExp_escape(term);
    const rex = new RegExp(pattern);
    const MAX = 10;
    let count = 0;
    output.innerHTML = "";
    if (!term) return;
    console.clear();
    for (const match of search_in(define.definitions, rex)) {
      console.debug("found", match);
      count++;
      if (count >= MAX) break;
    }
  }

  settle({ size: 16 * 8 }).prepend(form);
  function inputted(event) {
    search_world_for(event.target.value);
  }
  form.addEventListener("input", inputted);
  return { dispose: () => form.removeEventListener("input", inputted) };
});

define("~/explore/browser", ["~/explore/place_picker"], make_place_picker => {
  function make_browser() {
    let _address;
    const element = document.createElement("form");
    const locator = document.createElement("div");
    const place_picker = make_place_picker();
    const stage = document.createElement("div");
    // a browser has a locator.  a browser has a stage
    element.append(locator, place_picker.element, stage);
    locator.append("the locator");
    stage.append("the stage");
    function go(location) {
      _address = location;
      locator.textContent = `you are at: ${_address}`;
    }
    go("~");
    function dispose() {
      place_picker.dispose();
      element.remove();
    }
    return { dispose, element };
  }
  make_browser.comment = `general thing for exploring what, system? world?`;
  return make_browser;
});
define("~/explore/adt/tries", [], () => {
  // the keys have to have reference (Map, Set) identity
  const { defineProperty, defineProperties, hasOwn } = Object;
  const { isArray } = Array;
  const { assert } = console;
  const make_node = () => ({ children: new Map(), size: 0 });

  // data properties are writable though...
  // https://xlinux.nist.gov/dads/HTML/trie.html
  class Trie {
    constructor(iterate, options) {
      const multi = options?.multi ?? false;
      const monotonic = options?.monotonic ?? false;
      this.functional = !multi;
      this.root = make_node();
      if (typeof iterate === "function") this.iterate = iterate;
    }
    has(item) {
      let node = this.root;
      for (const key of this.iterate ? this.iterate(item) : item)
        if (node.children.has(key)) node = node.children.get(key);
      return hasOwn(node, "data");
    }
    get(item) {
      let the = this.root;
      for (const key of this.iterate ? this.iterate(item) : item)
        if (the.children.has(key)) the = the.children.get(key);
      return the?.data;
    }
    add(thing, data) {
      // const path = [];
      const passed = [];
      let node = this.root;
      for (const key of this.iterate ? this.iterate(thing) : thing) {
        // path.push(key);
        passed.push(node);
        const { children } = node;
        if (children.has(key)) node = children.get(key);
        else children.set(key, (node = make_node()));
      }
      assert?.(node, "whither node?");
      if (hasOwn(node, "data")) {
        if (this.functional) return false; // reject/conflict
        assert?.(isArray(node?.data), "bad books");
        node.data.push(thing);
        for (const node of passed) node.size++;
        return true;
      }
      for (const node of passed) node.size++;
      const it = data === void 0 ? thing : data;
      const value = this.functional ? it : [it];
      defineProperty(node, "data", { value, enumerable: true });
      return true;
    }
    // like add but return the data instead of true/false
    // TODO: should avoid the double lookup
    ensure(thing, data) {
      if (this.add(thing)) return data === void 0 ? thing : data;
      return this.get(thing);
    }
    get size() {
      return this.root.size;
    }
    get children() {
      return this.root.children;
    }
    keys() {
      return this.root.children.keys();
    }
    [Symbol.iterator]() {
      return this.root.children.entries();
    }
  }

  function make_trie(...args) {
    return new Trie(...args);
  }
  return { make_trie: (...args) => new Trie(...args) };
});

define("~/explore/css/io", ["~/def/io"], write => {
  // EFFECTS
  // const { css } = def.lang;
  const { css } = def.mint.in("http://example/css/").tuples;
  write.methods.add(
    // this will apply only when this tag is used on a tuple
    // so do you need a separate definition when it's a record?
    [css.AttributeEquals, String],
    ([name, value]) => `[${name}="${value}"]`
  );
});

define("~/explore/notation/css-rules", [], () => {
  const { entries } = Object;
  const camel_to_kebab = s => s; // provisional
  // custom properties you have to include the hyphens
  // id -> prop -> value
  function style_properties(object) {
    const all = entries(object)
      .map(([name, properties]) => {
        const props = entries(properties)
          .map(([k, v]) => `${camel_to_kebab(k)}:${v}`)
          .join("; ");
        // const selector = css.AttributeEquals("id", id)
        return `[name="${name}"]{${props}}`;
      })
      .join("\n");
  }
});

define("~/explore/plain-old-world-view", [
  "~/home/new",
  "~/def/io",
  // "~/def/io/methods", // for effects...
], (settle, { write }) => {
  settle().prepend(write(define.definitions, "Element"));
});

define("~/explore/show-example-trie", [
  "~/home/new",
  "~/explore/adt/tries",
  "~/def/io",
  // "~/def/io/methods", // effects...
], (settle, { make_trie }, { write }) => {
  const trie = make_trie();
  // const text = document.documentElement.textContent;
  const text = "love low lose loose monkey potato monk money";
  const matches = text.matchAll(/\w+/g);
  for (const [token] of matches) {
    const normalized = token.toLowerCase();
    trie.add(normalized);
  }
  const representation = write(trie, "Element");
  settle({ size: 100 }).prepend(representation);
});

define("~/explore/show-requirements", [
  "~/dom/home",
  "~/def/io",
  // "~/def/io/methods", // effects...
], (container, { write }) => {
  container.append(write(require.goals, "Element"));
  container.append(write(require.completed, "Element"));
});

define("~/explore/trie-of-world", [
  "~/home/new",
  "~/explore/adt/tries",
  "~/def/io",
  // "~/def/io/methods", // for effects...
], (settle, tries, { write }) => {
  const { make_trie } = tries;
  const directory = make_trie();
  for (const name of define.definitions.keys())
    directory.add(name.split(/[:/]/g));
  settle({ size: 16 * 8 }).append(write(directory, "Element"));
});

define("~/explore/browser/test", ["~/home/new", "~/explore/browser"], (
  settle,
  make_browser
) => {
  const browser = make_browser();
  settle({ size: 16 * 10 }).append(browser.element);
});

define("~/explore/reflect-environment", [], () => {
  console.debug("Online? answer now and track");
  console.debug("and other similar things about the document");
});

require(["~/populate-sections"], main => main());

require(["~/explore/ui/record-tables/read-all"], main => main());
require(["~/explore/ui/record-tables/userland-create-command"], main => main());

require(["~/sections-are-sizable"], () => {});
require(["~/s-makes-sizable"], () => {});

define("~/dom/home", [], () => document.querySelector("body"));
// require(["def:self-tests"], run => run());
// require("~/explore/syndication");
require("~/explore/test-search-world");
// require("~/explore/event-logger");
require("~/explore/reflect-environment");
require("~/remember/inputs");
require("~/explore/browser/test");
require("~/explore/search-world-dialog");
require("~/explore/tree-map-of-world");
// all of these want generic write methods
// require("~/explore/trie-of-world");
// require("~/explore/plain-old-world-view");
// require("~/explore/show-requirements");
// require("~/explore/show-example-trie");
