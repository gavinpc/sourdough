-- THIS doesn't seem to make ROOT available to commands though
ROOT = tup.getcwd()
tup.export('ROOT')
print('ROOT = '..ROOT)

tup.creategitignore()

tup.include('common.lua')

-- doesn't also reroute output flie because that may be worked into the command
-- itself already
function export_to_bob(description)
  -- reroute_first_output(description, function(path) return '../bob/'..path end)
  ignore_first_output(description)
  return description
end

function testing()
  fetch('https://api.wikimedia.org/core/v1/wikipedia/en/page/2000/html', 'en-wiki-timeline-2000.html')
  fetch('https://willshake.net/plays/Ham/1.1', 'ws-plays-Ham-1.1.html')
end

function main()
  tup.definerule(
    web_resource(
      'https://api.wikimedia.org/core/v1/wikipedia/en/page/List_of_years/html',
      'en-wiki-List_of_years.html',
      'en-wiki-List_of_years.html'))

  tup.definerule(
    export_to_bob(
      xslt1_description(
        "en-wiki-List_of_years.html",
        "wiki-time-period-links.xsl",
        "../bob/en-xref")))
end
main
