-- DUPLICATED in bob

-- For use in commands
tup.export('OS')

-- For use in Lua
is_windows = tup.getconfig('TUP_PLATFORM') == 'win32'

-- Can these be included conditionally on their existence?
tup.include("web/support.lua")
tup.include("xsl/support.lua")

function shell_quote(s)
  return "'"..s:gsub("'", "'\"'\"'").."'"
end

function powershell_quote(s)
  return s
end

function ignore_first_output(description)
  assert(description.outputs, 'expected description to have outputs')
  assert(#description.outputs >= 1, 'expected at least 1 output')
  local first_char = string.sub(description.outputs[1], 1, 1)
  assert(first_char ~= '^', 'expected output not already ignored')
  description.outputs[1] = '^'..description.outputs[1]
  print('first output = '..description.outputs[1])
  return description
end

-- function reroute_first_output(description, map)
--   assert(description.outputs, 'expected description to have outputs')
--   assert(#description.outputs >= 1, 'expected at least 1 output')
--   local path = description.outputs[1]
--   print('first output = '..path)
--   local mapped = map(path)
--   print('first output mapped = '..mapped)
--   description.outputs[1] = mapped
--   return description
-- end
