#!/bin/sh

# Fetch with cache on local FS.
#
# Example:
#
# ./get.sh 'https://api.wikimedia.org/core/v1/wikipedia/en/page/AD_888/html' './wiki/cache/en/AD_888.html' 'wiki-en-AD_888'
#
# If the alias has an extension (e.g. `.html`) , that extension is added to the
# content cache file, which can make it easier to open in your environment.
#
# Generally avoiding piping and redirection to minimize complications running
# Tup under Windows.  *UPDATE*: Windows now uses `get.ps1`, which see.

set -e

set_env() {
    export WWW_CACHE=$HOME/def.inet
    if [ -z "$ROOT" ]
    then
        export WWW_CACHE="."
    else
        export WWW_CACHE="$ROOT/web/cache"
    fi
}

set_env

__diff_indices() {
    find ~/def.inet/index -printf '%P\n' | sed 's/\..*$//g' | sort > b
    find ~/def.inet/internet -printf '%P\n' | sed 's/\..*$//g' | sort > a
    diff a b
}


known_command() {
    command -v "$1" > /dev/null;
}

maybe_windows_path() {
    if known_command cygpath; then
        cygpath -w "$1"
    elif known_command wslpath; then
        wslpath -w "$1"
    else
        echo -n "$1"
    fi
}

make_symlink() {
    local from="$1"
    local to="$2"

    if [ "$OS" = Windows_NT ]
    then
        # Requires elevated privilege or developer mode
        local from_resolved="$(maybe_windows_path "$from")"
        local to_resolved="$(maybe_windows_path "$to")"
        echo "windows $from_resolved → $to_resolved"
        cmd /C mklink "$from_resolved" "$to_resolved"
    else
        echo "NOT windows $from → $to"
        ln --symbolic "$to" "$from"
    fi
}

get_resource() {
    local url="$1"
    local link="$2"
    local alias="$3"

    echo "url = ${url}"
    echo "link = ${link}"
    echo "alias = ${alias}"

    local cache="$WWW_CACHE"
    local internet_dir="$cache/internet"
    local sha_full="$(echo -n "$url" | sha1sum)"
    local hash="${sha_full%% *}"     # cut off the trailing space and hyphen
    # local base="$(basename "$link")"
    if [ "$alias" != "${alias%.*}" ]
    then
        local extension=".${alias##*.}"
    fi
    local response="$internet_dir/${hash}${extension}"
    local index_dir="$cache/index"
    local index_file="$index_dir/$hash"

    echo "cache = ${cache}"
    echo "internet_dir = ${internet_dir}"
    echo "sha_full = ${sha_full}"
    echo "hash = ${hash}"
    echo "base = ${base}"
    echo "extension = ${extension}"
    echo "response = ${response}"
    echo "index_dir = ${index_dir}"
    echo "index_file = ${index_file}"

    if [ -e "$response" ]; then
        if [ ! -f "$response" ]; then
            echo "Target exists and is not a file! $response"
            return 1
        fi
        # No-op: assume existing content files are good enough
    else
        mkdir -p "$internet_dir"
        echo "Getting $url"
        echo "hash: $hash"
        # wget --output-document="$response" "$url"
        if ! curl --fail --location "$url" --output "$response"; then
            rm "$response"
            return 1
        fi
        local did_fetch=yes
    fi

    if [ ! -f "$index_file" ]; then
        echo "Writing index for $hash"
        if [ ! -d "$index_dir" ]; then mkdir -p "$index_dir"; fi
        echo "$url" > "$index_file"
    fi

    # Rather than copying the file, we just link to it.
    if [ -e "$link" ]; then
        if [ ! -L "$link" ]; then
            echo "Out file exists but is not a symlink! $link"
            return 1
        fi
        # We could confirm here that the link points to what it should
        # but that would require a call to `realpath`...
    else
        echo "Linking $hash → $link"
        make_symlink "$link" "$response"
    fi

    if [ ! -z "$alias" ]; then
        local alias_dir="$cache/alias"
        local alias_file="$alias_dir/$alias"

        echo "alias_dir = ${alias_dir}"
        echo "alias_file = ${alias_file}"

        if [ -e "$alias_file" ]; then
            if [ ! -L "$alias_file" ]; then
                echo "Alias file exists but is not a symlink! $alias_file"
                return 1
            fi
            echo "Claims it's already a symlink? $alias_file"
            # See note above.
        else
            echo "Aliasing $hash as “$alias”"
            if [ ! -d "$alias_dir" ]; then mkdir -p "$alias_dir"; fi
            make_symlink "$alias_file" "$response"
        fi
    fi

    # When all changes are done, pause
    if [ "$did_fetch" = 'yes' ]; then
        local wait=$((5 + RANDOM % 10))
        echo "Waiting $wait seconds..."
        sleep $wait
    fi
}

get_resource "$@"

exit 0
