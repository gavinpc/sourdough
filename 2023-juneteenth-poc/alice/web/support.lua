web_cwd = tup.getcwd()

function web_resource(spec)
  local url = spec.url
  local file = spec.file
  local alias = spec.alias or ''
  
  local rule
  if is_windows then
    -- TODO: how to get this from environment & make available to script?
    -- using env variables with Tup complicates things
    -- local cache_dir = 'c:/Users/gcannizzaro/def.inet'
    local cache_dir = 'def.inet/'

    -- powershell 2 doesn't write to any profile (at least if you say
    -- -NoProfile) but it's so old that you can't run it because a .NET 2
    -- Framework probably isn't available.  Hence the NonInteractive hack.
    -- See https://stackoverflow.com/q/69161207
    rule = {
      command =
        'powershell -version 3.0 -nologo -noninteractive -noprofile -File '..web_cwd..'/get.ps1'
        ..' '..powershell_quote(url)
        ..' '..powershell_quote(file)
        ..' '..powershell_quote(alias),
      outputs = {file, '^StartupProfileData-NonInteractive', '^'..cache_dir}
    }
  else
    rule = {
      command =
        'sh '..web_cwd..'/get.sh'
        ..' '..shell_quote(url)
        ..' '..shell_quote(file)
        ..' '..shell_quote(alias),
      -- `pipe-nt` should be unneeded now assuming the above is used on windows
      outputs = {file, '^pipe-nt', '^/def.inet/'}
    }  
  end

  return {spec=spec, rule=rule}
end
