-- xsl_cwd = tup.getcwd()
-- it's not needed, but why is this, anyway?
-- print('at xsl/index load time xsl_cwd is '..xsl_cwd) -- ../xsl

function copy(input, output)
  tup.definerule {
    input = {input},
    command = 'cp '..shell_quote(input)..' '..shell_quote(output),
    outputs = {output}
  }
end

function xslt1_description(input, transform, output)
  if is_windows then
    return {
      inputs = {input},
      command = 'xsltproc'
        ..' --html'             -- TEMP for testing crawl
        ..' --output '..output
        ..' '..transform
        ..' '..input,
      outputs = {output}
    }
  end

  return {
    inputs = {input},
    command = 'xsltproc'
      ..' --html'             -- TEMP for testing crawl
      ..' --output '..shell_quote(output)
      ..' '..shell_quote(transform)
      ..' '..shell_quote(input),
    outputs = {output}
  }
end

-- run XSLT on a single input for a single output
function xslt1(input, transform, output)
  description = xslt1_description(input, transform, output)
  return tup.definerule(description)
end
