tup.creategitignore()

tup.include('common.lua')

function testing()
  tup.foreach_rule(
    "*.html",
    "sh -c 'grep title %f > %o'",
    "titles/%b"
  )
  tup.definerule{
    inputs={"en-xref"},
    command="sh -c 'grep -i century %f > %o'",
    outputs={"testing-centuries"},
  }
end

function test_web_resource()
  tup.definerule(
    web_resource({
        url='https://api.wikimedia.org/core/v1/wikipedia/en/page/2000/html',
        file='en-wiki-timeline-2000.html',
        alias='en-wiki-timeline-2000.html'
    }).rule
  )
  tup.definerule(
    web_resource({
        url='https://willshake.net/plays/Ham/1.1',
        file='ws-plays-Ham-1.1.html',
        alias='ws-plays-Ham-1.1.html'
    }).rule
  )
end

function test_web_resource_bad()
  -- should report something like
  --
  --     The remote name could not be resolved: 'example.foobar'
  tup.definerule(
    web_resource({
        url='https://example.foobar/nosuchthing',
        file='bad-example.html',
        alias='bad-example.html'
    }).rule
  )
end

-- testing()
test_web_resource()
-- test_web_resource_bad()

tup.include("wiki-timeline.lua")
