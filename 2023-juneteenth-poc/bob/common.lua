-- DUPLICATED in alice

-- For use in Lua
is_windows = tup.getconfig('TUP_PLATFORM') == 'win32'
is_macos = tup.getconfig('TUP_PLATFORM') == 'macosx'

-- Can these be included conditionally on their existence?
tup.include("web/support.lua")
tup.include("xsl/support.lua")

function debug_table(coll, label)
  if label then print(label) end
  print(string.format('%s items', #coll))
  for k, v in pairs(coll) do
    print(string.format("%s → %s", k, v))
  end
end

-- BOTH of these are for quoting *literals* only

-- THIS is really posix quote
function shell_quote(s)
  return "'"..s:gsub("'", "'\"'\"'").."'"
end

-- TODO: implement this.  And I'm not sure how this is supposed to work in Tup,
-- since we can only use powershell by calling it explicitly from the command,
-- which itself is interpreted how exactly?
function powershell_quote(s)
  return s
end

function is_nonblank(s)
  return s ~= nil and s ~= ''
end

function get_application_data_path(name)
  assert(is_nonblank(name))
  if is_windows then
    tup.import('LOCALAPPDATA')
    assert(is_nonblank(LOCALAPPDATA), "LOCALAPPDATA must be defined on Windows!")
    return LOCALAPPDATA..'/'..name
  end
  if is_macos then
    error('Not implemented: application_data_path for MacOS')
  end
  return HOME..'/.cache/'..name
end

function ignore_first_output(description)
  assert(description.outputs, 'expected description to have outputs')
  assert(#description.outputs >= 1, 'expected at least 1 output')
  local first_char = string.sub(description.outputs[1], 1, 1)
  assert(first_char ~= '^', 'expected output not already ignored')
  description.outputs[1] = '^'..description.outputs[1]
  print('first output = '..description.outputs[1])
  return description
end
