function map(coll, f)
  local o = {}
  for _,v in ipairs(coll) do
    table.insert(o, f(v))
  end
  return o
end

-- like above but pairs instead of ipairs
-- really this should be `map` and that one `map_indexed` or something
-- ALSO, expects 2-ary f
-- ALSO skips nils
function map_dictionary(coll, f)
  local o = {}
  for k,v in pairs(coll) do
    local value = f(v, k)
    if value ~= nill then
      o[k] = value
    end
  end
  return o
end

function foreach_line_in(file, process)
  local file = io.open(file, 'r')
  if file ~= nil then
    for line in file:lines() do
      process(line)
    end
    file:close()
  else
    -- no such file or couldn't open for read
  end
end

function identity(x) return x end
function buffer_lines_in(file, fun, skip_empty)
  local f = fun or identity
  local o = {}
  foreach_line_in(
    file,
    function(v)
      if (skip_empty and v == '') then return end
      table.insert(o, f(v))
    end
  )
  return o
end

-- return a table based on the lines in a file
-- fun maps a line to a key and value
-- ASSUMES that `fun` is effectively deterministic (returns equivalent for same key)
function dictionary_from_file(file, fun, skip_empty)
  local o = {}
  foreach_line_in(
    file,
    function(line)
      if skip_empty and (line == nil or line == '') then return end
      local key, value = fun(line)
      if o[key] == nil then -- Lua has no up/down member test for table, eh?
        o[key] = value
      else
        -- safety check
        -- print('SAFETY CHECKING key, value ', key, value)
        -- if o[key] ~= value then
        --   error('key '..key..' already has a different value than ‘'..value..'’: '..o[key])
        -- end
      end
    end
  )
  return o
end
