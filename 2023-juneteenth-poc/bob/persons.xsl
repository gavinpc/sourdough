<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0'
               xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
               xmlns:ex="http://exslt.org/common"
               xmlns:wtime="https://gavinpc.com/wiki-timeline"
               exclude-result-prefixes="ex wtime"
               >
  <xsl:output indent="yes" omit-xml-declaration="yes" />
  <xsl:include href="./wikipedia-timeline-item.xsl" />
  <!-- These templates are only valid for year -->
  <!-- WHY though? -->
  <xsl:param name="year" />

  <!-- the item is about the first wiki link *unless* it's a time -->
  <!-- 
       we strip the query from the href because names without articles have
       `?action=edit&redlink=1`.  This occurs in 0.05% of cases.  For our
       purposes it doesn't matter whether the article exists.
  -->
  
  <xsl:template mode="who-when" match="*">
    <xsl:param name="what" />
    <xsl:param name="iso-year" />
    <xsl:variable name="first" select=".//a/@wiki" />
    <xsl:variable name="second" select="(.//a/@wiki)[2]" />
    <xsl:variable name="specifically" select="wtime:when-in-year($first)" />
    <xsl:variable name="alternate" select="wtime:year($first)" />
    <xsl:variable name="text" select="normalize-space()" />
    <xsl:variable name="appos" select="substring-after($text, ', ')" />
    <xsl:variable name="label" select="substring-before(concat($appos, ' ('), ' (')" />
    <xsl:variable name="who">
      <xsl:choose>
        <xsl:when test="$specifically or $alternate"><xsl:value-of select="$second" /></xsl:when>
        <xsl:otherwise><xsl:value-of select="$first" /></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="not($first)">
        <xsl:message>WARN: No link for <xsl:value-of select="concat($iso-year, ' ', $text)" /></xsl:message>
      </xsl:when>
      <xsl:when test="$specifically and not($second)">
        <xsl:message>WARN: Within-year but no second link for <xsl:value-of select="concat($iso-year, ' ', $specifically, ' ', .)" /></xsl:message>
      </xsl:when>
      <xsl:otherwise>
        <xsl:element name="{$what}">
          <xsl:attribute name="who">
            <xsl:value-of select="wtime:strip-query($who)" />
          </xsl:attribute>
          <xsl:attribute name="when">
            <xsl:choose>
              <xsl:when test="$alternate">
                <xsl:value-of select="$alternate" />
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="$iso-year" />
              </xsl:otherwise>
            </xsl:choose>
            <xsl:if test="$specifically">
              <xsl:value-of select="concat('-', $specifically)" />
            </xsl:if>
          </xsl:attribute>
          <xsl:if test="$label != ''">
            <xsl:attribute name="summary">
              <xsl:value-of select="$label" />
            </xsl:attribute>
          </xsl:if>
        </xsl:element>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template mode="person-events" match="*">
    <xsl:param name="type" />
    <xsl:param name="iso-year" />
    <xsl:variable name="data">
      <!-- weak for avoiding nested lists, but not(descendant::li) is v inefficient --> 
      <xsl:apply-templates mode="normalize-time-item" select=".//li[not(ul)]" />
    </xsl:variable>
    <xsl:apply-templates mode="who-when" select="ex:node-set($data)">
      <xsl:with-param name="what" select="$type" />
      <xsl:with-param name="iso-year" select="$iso-year" />
    </xsl:apply-templates>
  </xsl:template>


  <xsl:template match="/">
    <persons>
      <xsl:apply-templates select="//section[h2/@id='Births' and .//li//node()]" mode="person-events">
        <xsl:with-param name="type" select="'birth'" />
        <xsl:with-param name="iso-year" select="$year" />
      </xsl:apply-templates>
      <xsl:apply-templates select="//section[h2/@id='Deaths' and .//li//node()]" mode="person-events">
        <xsl:with-param name="type" select="'death'" />
        <xsl:with-param name="iso-year" select="$year" />
      </xsl:apply-templates>
    </persons>
  </xsl:template>

</xsl:transform>
