# GOAL: a safe, reliable, host-friendly web fetch
#
# OBJECTIVES:
#
# - a request for a URL results in you getting content (possibly stale) for that
#   address
#
# - a request for a URL that you previously fetched results in no network
#   activity
#
# - we don't make more than one request for a particular host at a time
#
# ASSUMES that you can symlink
param (
    [string]$Url,
    [string]$Link,
    [string]$Alias,
    [string]$WebCacheDir
)

# c/o https://stackoverflow.com/a/818054
function Test-ReparsePoint([string]$path) {
    $file = Get-Item $path -Force -ea SilentlyContinue
    return [bool]($file.Attributes -band [IO.FileAttributes]::ReparsePoint)
}

$SHA1 = New-Object Security.Cryptography.SHA1CryptoServiceProvider
$UTF8 = [Text.Encoding]::UTF8

function Get-Sha1Sum([string]$text) {
    $data = $UTF8.GetBytes($text)
    $hash = $SHA1.ComputeHash($data)
    return [BitConverter]::ToString($hash).Replace("-", "").ToLower()
}

# This also works
# function Hash([string]$text) {
#     $stream = [IO.MemoryStream]::new([byte[]][char[]]$text)
#     $hash = Get-FileHash -InputStream $stream -Algorithm SHA1
#     return $hash
# }
# looking for a more direct way that doesn't assume converter format
# function Hash([string]$text) {
#     $data = $UTF8.GetBytes($text)
#     $hash = $SHA1.ComputeHash($data)
#     $mapped = $hash.ForEach($_.ToString("X2"));
#     $out = ($mapped -join "")
#     return $out
#     # return [BitConverter]::ToString($hash).Replace("-", "").ToLower()
# }

function Get-WebResource {
    param (
        [string]$Url,
        [string]$Link,
        [string]$Alias,
        [string]$WebCacheDir
    )
    $DEBUG = $False
    if ($DEBUG) {
        Write-Debug "url = $Url"
        Write-Debug "link = $Link"
        Write-Debug "alias = $Alias"
        Write-Debug "web cache dir = ${WebCacheDir}"
    }

    $internet_dir = "$WebCacheDir/internet"

    $hash = Get-Sha1Sum($Url)
    $base = [IO.Path]::GetFileNameWithoutExtension($Link)
    $extension = [IO.Path]::GetExtension($Alias)

    $response = "$internet_dir/${hash}${extension}"
    $index_dir = "$WebCacheDir/index"
    $index_file = "$index_dir/$hash"

    if ($DEBUG) {
        Write-Debug "internet_dir = ${internet_dir}"
        Write-Debug "sha_full = ${sha_full}"
        Write-Debug "hash = ${hash}"
        Write-Debug "base = ${base}"
        Write-Debug "extension = ${extension}"
        Write-Debug "response = ${response}"
        Write-Debug "index_dir = ${index_dir}"
        Write-Debug "index_file = ${index_file}"
    }

    if (Test-Path $response) {
        if (-Not (Test-Path $response -Type Leaf)) {
            throw "Target exists and is not a file! $response"
        }
        # No-op: assume existing content files are good enough
    } else {
        New-Item -ItemType Directory -Force -Path "$internet_dir"
        Write-Information "Getting $Url"
        Write-Information "hash: $hash"

        try {
            $webclient = New-Object Net.WebClient
            $webclient.DownloadFile($Url, $response)
        } catch {
            if (Test-Path $response) {
                Remove-Item $response
            }
            throw
        }

        $did_fetch=$True
    }

    if (-Not (Test-Path "$index_file" -Type Leaf)) {
        Write-Information "Writing index for $hash"
        if (-Not (Test-Path "$index_dir" -Type Container)) {
            New-Item -ItemType Directory -Force -Path "$index_dir"
        }
        Write-Output "$Url" > "$index_file"
    }

    # Rather than copying the file, we just link to it.
    if (Test-Path "$Link") {
        # unfortunately Test-Path -Type does not support links of any kind
        # apparently it looks to Test-Path like whatever the target is
        if (-Not (Test-ReparsePoint("$Link"))) {
            throw "Out file exists but is not a link! $Link"
        }
        # We could confirm here that the link points to what it should...
    } else {
        Write-Information "Linking $hash → $link"
        New-Item -Type HardLink -Path "$Link" -Value "$response"
        # HACK: convince Tup that we wrote this file; needed for all links
        $info = New-Object System.IO.DirectoryInfo($Link)
        $info.LastWriteTime = $info.LastWriteTime
    }

    # When all changes are done, pause
    if ($did_fetch) {
        $wait = Get-Random -Minimum 5 -Maximum 20
        Write-Information "Waiting $wait seconds..."
        Start-Sleep $wait
    }
}

Get-WebResource @PSBoundParameters
