#!/bin/sh

# Fetch with cache on local FS.
#
# ASSUMES we're on a real POSIX system. For Windows, see `get.ps1`
#
# Example:
#
# ./get.sh 'https://api.wikimedia.org/core/v1/wikipedia/en/page/AD_888/html' './wiki/cache/en/AD_888.html' 'wiki-en-AD_888'
#
# If the alias has an extension (e.g. `.html`) , that extension is added to the
# content cache file, which can make it easier to open in your environment.

set -e

# old stuff
__diff_indices() {
    find ~/def.inet/index -printf '%P\n' | sed 's/\..*$//g' | sort > b
    find ~/def.inet/internet -printf '%P\n' | sed 's/\..*$//g' | sort > a
    diff a b
}


get_resource() {
    local url="$1"
    local link="$2"
    local alias="$3"
    local cache="$4"

    if [ ! -z "$DEBUG" ]; then
        >&2 echo "url = ${url}"
        >&2 echo "link = ${link}"
        >&2 echo "alias = ${alias}"
    fi

    local internet_dir="$cache/internet"
    local sha_full="$(echo -n "$url" | sha1sum)"
    local hash="${sha_full%% *}"     # cut off the trailing space and hyphen
    # local base="$(basename "$link")"
    if [ "$alias" != "${alias%.*}" ]
    then
        local extension=".${alias##*.}"
    fi
    local response="$internet_dir/${hash}${extension}"
    local index_dir="$cache/index"
    local index_file="$index_dir/$hash"

    if [ ! -z "$DEBUG" ]; then
        >&2 echo "cache = ${cache}"
        >&2 echo "internet_dir = ${internet_dir}"
        >&2 echo "sha_full = ${sha_full}"
        >&2 echo "hash = ${hash}"
        >&2 echo "base = ${base}"
        >&2 echo "extension = ${extension}"
        >&2 echo "response = ${response}"
        >&2 echo "index_dir = ${index_dir}"
        >&2 echo "index_file = ${index_file}"
    fi

    if [ -e "$response" ]; then
        if [ ! -f "$response" ]; then
            >&2 echo "Target exists and is not a file! $response"
            return 1
        fi
        # No-op: assume existing content files are good enough
    else
        mkdir -p "$internet_dir"
        echo "Getting $url"
        echo "hash: $hash"
        # wget --output-document="$response" "$url"
        if ! curl --fail --location "$url" --output "$response"; then
            rm "$response"
            return 1
        fi
        local did_fetch=yes
    fi

    if [ ! -f "$index_file" ]; then
        echo "Writing index for $hash"
        if [ ! -d "$index_dir" ]; then mkdir -p "$index_dir"; fi
        echo "$url" > "$index_file"
    fi

    # Rather than copying the file, we just link to it.
    if [ -e "$link" ]; then
        if [ ! -L "$link" ]; then
            >&2 echo "Out file exists but is not a symlink! $link"
            return 1
        fi
        # We could confirm here that the link points to what it should
        # but that would require a call to `realpath`...
    else
        echo "Linking $hash → $link"
        ln --symbolic "$response" "$link"
    fi

    if [ ! -z "$alias" ]; then
        local alias_dir="$cache/alias"
        local alias_file="$alias_dir/$alias"

        echo "alias_dir = ${alias_dir}"
        echo "alias_file = ${alias_file}"

        if [ -e "$alias_file" ]; then
            if [ ! -L "$alias_file" ]; then
                >&2 echo "Alias file exists but is not a symlink! $alias_file"
                return 1
            fi
            echo "Claims it's already a symlink? $alias_file"
            # See note above.
        else
            echo "Aliasing $hash as “$alias”"
            if [ ! -d "$alias_dir" ]; then mkdir -p "$alias_dir"; fi
            ln --symbolic "$response" "$alias_file"
        fi
    fi

    # When all changes are done, pause
    if [ "$did_fetch" = 'yes' ]; then
        local wait=$((5 + RANDOM % 10))
        echo "Waiting $wait seconds..."
        sleep $wait
    fi
}

get_resource "$@"

exit 0
