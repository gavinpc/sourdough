web_cwd = tup.getcwd()

-- REQUIRES `get_application_data_path` and `is_nonblank` from common
function get_web_cache_path()
  tup.import('WEB_CACHE')
  -- Force value copy.  Without this concat, the returned variable is corrupt;
  -- specifically, `gsub` says it's nil, even though it concats and prints ok.
  if is_nonblank(WEB_CACHE) then return WEB_CACHE..'' end
  return get_application_data_path('sourdough-web')
end

function web_resource(spec)
  local url = spec.url
  local file = spec.file
  local alias = spec.alias or ''

  local web_cache_dir_raw = get_web_cache_path()
  -- normalize the path.  This is needed only for the ignore-output clause,
  -- but still may be better done in the function itself.
  local web_cache_dir = web_cache_dir_raw:gsub('\\', '/')

  local rule
  if is_windows then
    -- powershell 2 doesn't write to any profile (at least if you say
    -- -NoProfile) but it's so old that you can't run it because a .NET 2
    -- Framework probably isn't available.  Hence the NonInteractive hack.
    -- See https://stackoverflow.com/q/69161207
    rule = {
      command =
        'powershell -version 3.0 -nologo -noninteractive -noprofile -File '..web_cwd..'/get.ps1'
        ..' '..powershell_quote(url)
        ..' '..powershell_quote(file)
        ..' '..powershell_quote(alias)
        ..' '..powershell_quote(web_cache_dir),
      outputs = {file, '^'..web_cache_dir, '^StartupProfileData-NonInteractive'}
    }
  else
    rule = {
      command =
        'sh '..web_cwd..'/get.sh'
        ..' '..shell_quote(url)
        ..' '..shell_quote(file)
        ..' '..shell_quote(alias)
        ..' '..shell_quote(web_cache_dir),
      outputs = {file, '^'..web_cache_dir}
    }  
  end

  return {spec=spec, rule=rule}
end
