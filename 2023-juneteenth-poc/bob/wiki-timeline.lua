-- “slug” means the identifier within a wiki, e.g. 8th_century_BC

tup.include('helpers.lua')

-- ASSUMES web support
-- extends `web_resource` return value to include {lang, slug}
function wikipedia_article(lang, slug)
  local url = 'https://api.wikimedia.org/core/v1/wikipedia/'..lang..'/page/'..slug..'/html'
  -- local file = (dir or '$(ROOT)/wiki/cache')..'/'..lang..'/'..slug..'.html'
  -- this is relative to what?
  local file = 'wiki/cache/'..lang..'/'..slug..'.html'
  local alias = slug..'.html'
  local article = web_resource({url=url, file=file, alias=alias})
  article.lang = lang
  article.slug = slug
  return article
end

-- index is Table<slug, {}>
function project_persons(lang, index)
  function f(entry, slug)
    local about_time = entry.about_time
    local article = entry.article
    -- HACK: exclude non-years.  But these are language specific
    -- we could do this more accurately on the date value
    if (slug:match('.*s')) then return nil end -- decades, it turns out
    if (slug:match('.*mill')) then return nil end
    if (slug:match('.*century')) then return nil end
    local label = 'persons of '..slug
    local input = entry.spec.file -- hmmmmm
    local transform = 'persons.xsl'
    local params = {year = about_time}
    -- this is relative to what?
    local output = 'persons/'..lang..'/'..slug..'.xml'
    local xform = xslt1{
        input=input,
        transform=transform,
        output=output,
        params=params
    }
    return {rule=xform.rule, about_time=about_time, article=article}
  end
  return map_dictionary(index, f)
end

function wikipedia_timeline_persons(lang, index)
  local persons = project_persons(lang, index)

  -- actualize persons
  for _k,person in pairs(persons) do
    tup.definerule(person.rule)
  end

  -- this is relative to what?
  local all_xml = 'persons/index/'..lang..'-all.xml';
  -- not sure how else to test with partial build
  local TEMP = {"101_BC", "1663", "1737"}
  local files = map(TEMP, function(name) return 'persons/'..lang..'/'..name..'.xml' end)
  local file_list = nil
  for _,file in pairs(files) do
    if file_list == nil then file_list = file
    else file_list = file_list..' '..file
    end
  end
  local people_xml = 'persons/'..lang..'.xml'
  local all_people = xslt1{
    -- label = 'aggregate person events for '..lang,
    input = 'dummy.xml',
    transform = 'aggregate-person-events.xsl',
    parameters = {files = file_list or ''},
    output = people_xml
  }
  all_people.rule.extra_inputs = files
  tup.definerule(all_people.rule)

  local people_js = 'wiki-persons-'..lang..'.js'
  local people_loader = {
    inputs = {people_xml},
    command = "sh -c 'cat "..people_xml.." | ./make-loader.sh persons > "..people_js.."'",
    outputs = {people_js, '^pipe-nt'},
  }
  tup.definerule(people_loader)
end

-- file is lines of space-delimited Wikipedia slug and an ISO-8601 date
-- well, a four-digit year with `-` for BC and NO SIGN for CE
-- f is (slug, date) → (slug, T)
-- returns Table<slug, T>
function project_timeline_index(xref_file, f)
  function line_to_pair(line)
    local slug, date = line:match'(.*) (.*)'
    if slug == nil then error("no slug in line "..line.."\n") end
    if date == nil then error("no date in line "..line.."\n") end
    return slug, f(slug, date)
  end
  return dictionary_from_file(xref_file, line_to_pair, true) -- skip blank lines
end

-- returns Table<slug, {spec, rule, lang, slug, about_time}>
function wikipedia_timeline(lang)
  local xref_file = lang..'-xref'

  function relevant_article(slug, about_time)
    local article = wikipedia_article(lang, slug)
    article.about_time = about_time
    return article
  end
  local index = project_timeline_index(xref_file, relevant_article)

  -- actualize timeline
  for _,entry in pairs(index) do
    tup.definerule(entry.rule)
  end

  return index
end

function wikipedia_timeline_with_persons(lang)
  local index = wikipedia_timeline(lang)
  wikipedia_timeline_persons(lang, index)
end

wikipedia_timeline_with_persons('en')
