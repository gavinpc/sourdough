-- THIS now differs from alice version

-- xsl_cwd = tup.getcwd()
-- it's not needed, but why is this, anyway?
-- print('at xsl/index load time xsl_cwd is '..xsl_cwd) -- ../xsl

-- for cleaning up html output
-- 2> >(sed '/\\(ID.*already defined\\|Tag.*invalid\\|Unexpected end tag\\)/,+2d' >&2) > '%o'
-- 
-- e.g. we get a bunch of crap like this
-- 
-- wiki-en-1564.html:168: HTML parser error : Tag bdi invalid
-- 6036-1" title="Special:BookSources/978-0-19-286036-1" id="mwAfA"><bdi id="mwAfE"
--                                                                                ^
-- wiki-en-1564.html:168: HTML parser error : Tag bdi invalid
-- 7982-4" title="Special:BookSources/978-1-137-07982-4" id="mwAfs"><bdi id="mwAfw"

function xslt1(spec)
  local input = spec.input
  local transform = spec.transform
  local output = spec.output
  local parameters = spec.parameters -- the transform (not command) parameters

  local quote = shell_quote
  -- BUT just because it's windows doesn't mean it's powershell...
  -- this supposes that we're going to have a `.ps1` wrapper
  -- which we don't yet, but will eventually need
  if is_windows then quote = powershell_quote end

  local params = ''
  if parameters then
    for key,value in pairs(parameters) do
      -- params = params..' --stringparam '..quote(key)..' '..shell_quote(value)
      params = params..' --stringparam '..quote(key)..' "'..value..'"'
    end
  end

  local command= 'xsltproc'
    ..' --html'             -- TEMP for testing crawl, should be an option
    ..' --output '..quote(output)
    ..params
    ..' '..quote(transform)
    ..' '..quote(input)

  local rule = {
    inputs = {input},
    command = command,
    outputs = {output}
  }

  return {spec=spec, rule=rule}
end
