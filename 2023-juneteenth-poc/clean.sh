#!/bin/bash

# DELETE all ignored files except .tup directories.

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"
cd "$this_script_dir"

# -d means traverse into untracked directories
# https://git-scm.com/docs/git-clean#Documentation/git-clean.txt--d
# -x means use the `--exclude` options instead of the standard ignore rules.
# https://git-scm.com/docs/git-clean#Documentation/git-clean.txt--x
#
# Shouldn't delete .tup directories, but that doesn't reset partial state.
# git clean --force -x -d --exclude=.tup "$@"
#
# NOTE, I'm using -X instead of -x because I can't exclude `.tup` anyway and
# `-x` deletes untracked files as well, which is bad!
#
# ALSO, this would delete local download caches, which is a bad default
git clean --force -X -d "$@"

exit 0
