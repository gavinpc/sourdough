# Powershell equivalent of go.sh

# But how do I ensure that I'm in this directory?

$script_dir = '.'

try {
    pushd "$script_dir/alice"
    tup -j1
} finally {
    popd
}

try {
    pushd "$script_dir/bob"
    tup -j1
} finally {
    popd
}
