#!/bin/sh

# Build both Alice and Bob

set -e

script_dir="$(cd "$(dirname "$0")" && pwd)"

cd "$script_dir/alice"
tup -j1

# pass-through arguments so you can e.g. specify target
cd "$script_dir/bob"
tup -j1 "$@"

exit 0
