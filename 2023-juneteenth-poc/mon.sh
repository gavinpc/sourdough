#!/bin/sh

# Monitor both Alice and Bob.  Linux only

set -e

script_dir="$(cd "$(dirname "$0")" && pwd)"

cd "$script_dir"

# TODO: not this
pushd "alice" > /dev/null
tup monitor
popd > /dev/null

pushd "bob" > /dev/null
tup monitor
popd > /dev/null


exit 0
