tup.creategitignore()

local this_script_dir = tup.getcwd()

function powershell_quote(s)
  return s
end

function bat_quote(s)
  return s
end

function sleep_test()
  tup.definerule({
      command = 'powershell -version 6.0 -nologo -noninteractive -noprofile -File '..this_script_dir..'/sleep.ps1',
      outputs = {'dummy-sleep', '^StartupProfileData-NonInteractive'}
  })
end

function get(name, url)
  local output = name..'.html'
  tup.definerule({
      inputs = {name},
      command =
        'powershell -version 3.0 -nologo -noninteractive -noprofile -File '..this_script_dir..'/get.ps1'
        ..' '..powershell_quote(url)
        ..' '..powershell_quote(output),
      outputs = {output, '^StartupProfileData-NonInteractive'}

  })
end

function fetch_test()
  tup.definerule({
      command = "touch a b c d",
      outputs = {"a", "b", "c", "d"}
  })
  get('a', 'http://localhost:8088')
  get('b', 'http://localhost:8088')
  get('c', 'http://localhost:8088')
  get('d', 'http://localhost:8088')
end

function powershell_hard_link_test()
  local source = 'dummy_link_source'
  local link = 'a_hard_link'
  tup.definerule({
      inputs = {source},
      command = 'powershell -version 5.0 -nologo -noninteractive -noprofile -File '..this_script_dir..'/make-hard-link.ps1'
        ..' '..powershell_quote(link)
        ..' '..powershell_quote(source),
      outputs = {link, '^StartupProfileData-NonInteractive'}
  })
end

function symlink_test()
  local source = 'dummy_link_source'
  local link = 'a_symlink'
  tup.definerule({
      inputs = {source},
      command = 'powershell -version 5.0 -nologo -noninteractive -noprofile -File '..this_script_dir..'/make-symlink.ps1'
        ..' '..powershell_quote(link)
        ..' '..powershell_quote(source),
      outputs = {link, '^StartupProfileData-NonInteractive'}
  })
end

function symlink_cmd_via_powershell()
  local source = 'dummy_link_source'
  local link = 'a_symlink'
  tup.definerule({
      inputs = {source},
      command = 'powershell -version 5.0 -nologo -noninteractive -noprofile -File '..this_script_dir..'/make-symlink-via-cmd.ps1'
        ..' '..powershell_quote(link)
        ..' '..powershell_quote(source),
      outputs = {link, '^StartupProfileData-NonInteractive'}
  })
end

function bat_mklink_test()
  local source = 'dummy_link_source'
  local link = 'a_symlink_from_mklink'
  tup.definerule({
      inputs = {source},
      command = 'mklink.bat '
        ..' '..bat_quote(link)
        ..' '..bat_quote(source),
      outputs = {link}
  })
end

function bat_hard_link_test()
  local source = 'dummy_link_source'
  local link = 'a_hard_link_from_mklink'
  tup.definerule({
      inputs = {source},
      command = 'mklink-hard-link.bat '
        ..' '..bat_quote(link)
        ..' '..bat_quote(source),
      outputs = {link}
  })
end

-- fetch_test()
-- sleep_test()
powershell_hard_link_test()
-- symlink_test()
-- symlink_cmd_via_powershell()
-- bat_mklink_test()
-- bat_hard_link_test()
