param ($Link, $Target)

function Make-Hard-Link() {
    param ($Link, $Target)
    New-Item -Type HardLink -Path "$Link" -Value "$Target"
    # touch "$Link"
    # too heavy
    # Write-Output "" >> "$Link"
    $info = New-Object System.IO.DirectoryInfo($Link)
    $info.LastWriteTime = $info.LastWriteTime
}

Make-Hard-Link @PSBoundParameters
