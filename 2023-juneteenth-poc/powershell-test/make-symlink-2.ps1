# https://learn-powershell.net/2013/07/16/creating-a-symbolic-link-using-powershell/
param ($SymName, $Path)

Begin {
    Try {
        $null = [mklink.symlink]
    } Catch {
        Add-Type @"
        using System;
        using System.Runtime.InteropServices;
 
        namespace mklink
        {
            public class symlink
            {
                [DllImport("kernel32.dll")]
                public static extern bool CreateSymbolicLink(string lpSymlinkFileName, string lpTargetFileName, int dwFlags);
            }
        }
"@
    }
}

Process {
    #Assume target Symlink is on current directory if not giving full path or UNC
    If ($SymName -notmatch "^(?:[a-z]:\\)|(?:\\\\\w+\\[a-z]\$)") {
        $SymName = "{0}\{1}" -f $pwd,$SymName
    }
    $Flag = @{
        File = 0
        Directory = 1
    }
    # If ($PScmdlet.ShouldProcess($Path,'Create Symbolic Link')) {
        Try {
            # $return = [mklink.symlink]::CreateSymbolicLink($SymName,$Path,$Flag[$PScmdlet.ParameterSetName])
            $return = [mklink.symlink]::CreateSymbolicLink($SymName,$Path,$Flag["File"])
            If ($return) {
                $object = New-Object PSObject -Property @{
                    SymLink = $SymName
                    Target = $Path
                    Type = "File" # $PScmdlet.ParameterSetName
                }
                $object.pstypenames.insert(0,'System.File.SymbolicLink')
                $object
            } Else {
                Throw # "Unable to create symbolic link!"
            }
        } Catch {
            throw
            # Write-warning ("{0}: {1}" -f $path,$_.Exception.Message)
        }
    # }
}
