param ($Link, $Target)

function Make-SymbolicLink-Via-Cmd() {
    param ($Link, $Target)
    # I don't know whether these replaces are necessary
    # cmd.exe /c mklink $Link.Replace("/", "\") $Target.Replace("/", "\")
    # cmd.exe /c mklink $Link $Target
    $command = "cmd.exe / c mklink"
    Invoke-Expression "$command ""$Link"" ""$Target"""
}

Make-SymbolicLink-Via-Cmd @PSBoundParameters
