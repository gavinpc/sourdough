param ($Link, $Target)

function Make-SymbolicLink() {
    param ($Link, $Target)
    New-Item -Type SymbolicLink -Path "$Link" -Value "$Target"
}

Make-SymbolicLink @PSBoundParameters
