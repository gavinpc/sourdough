const { create_http_server, STATUS } = require("./simple-http-server");

const port = process.argv[2] ?? 8080;
const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
const server = create_http_server({
  port,
  handler: async () => {
    console.log("let me sleep first");
    await sleep(10000);
    console.log("okay what now?");
    return { ...STATUS.OK, body: "mmkay" };
  },
});
