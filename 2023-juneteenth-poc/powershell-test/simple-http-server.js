"use strict";
var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __copyProps = (to, from, except, desc) => {
  if ((from && typeof from === "object") || typeof from === "function") {
    for (let key of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to, key) && key !== except)
        __defProp(to, key, {
          get: () => from[key],
          enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable,
        });
  }
  return to;
};
var __toESM = (mod, isNodeMode, target) => (
  (target = mod != null ? __create(__getProtoOf(mod)) : {}),
  __copyProps(
    isNodeMode || !mod || !mod.__esModule
      ? __defProp(target, "default", { value: mod, enumerable: true })
      : target,
    mod
  )
);
var __toCommonJS = mod =>
  __copyProps(__defProp({}, "__esModule", { value: true }), mod);

// packages/interop-http-service/src/index.ts
var src_exports = {};
__export(src_exports, {
  MIME_TYPES: () => MIME_TYPES,
  STATUS: () => STATUS,
  create_http_server: () => create_http_server,
  with_constant: () => with_constant,
  with_error_boundary: () => with_error_boundary,
  with_node_modules: () => with_node_modules,
  with_path_mount: () => with_path_mount,
  with_static_files: () => with_static_files,
});
module.exports = __toCommonJS(src_exports);

// packages/interop-http-service/src/constants.ts
var STATUS = {
  OK: { status: 200, message: "OK" },
  BAD_REQUEST: { status: 400, message: "Bad request" },
  UNAUTHORIZED: { status: 403, message: "Not authorized" },
  NOT_FOUND: { status: 404, message: "Not found" },
  METHOD_NOT_ALLOWED: { status: 406, message: "Method not allowed" },
  INTERNAL_SERVER_ERROR: { status: 500, message: "Internal server error" },
  NOT_IMPLEMENTED: { status: 501, message: "Not implemented" },
};

// packages/interop-http-service/src/handlers/with_constant.ts
var DEFAULT_STATUS = STATUS.OK;
var DEFAULT_HEADERS = {
  "Content-type": "text/plain",
  "Cache-Control": "no-cache, no-store, must-revalidate",
  Pragma: "no-cache",
  Expires: "0",
};
{
}
var with_constant = options => _request => {
  const { response } = options;
  const { headers: headers2, ...rest } = response;
  return {
    ...DEFAULT_STATUS,
    headers: { ...DEFAULT_HEADERS, ...headers2 },
    ...rest,
  };
};

// packages/interop-http-service/src/handlers/with_error_handler.ts
var DEFAULT = {
  debug: true,
};
var with_error_boundary = options => async request => {
  const { handler, debug } = { ...DEFAULT, ...options };
  try {
    return handler(request);
  } catch (error) {
    console.log(error);
    return {
      ...STATUS.INTERNAL_SERVER_ERROR,
      ...(!debug ? {} : { body: error?.toString() ?? "UndefinedError" }),
    };
  }
};

// packages/interop-http-service/src/handlers/with_node_modules.ts
var fs = __toESM(require("fs"));
var import_path = require("path");
var import_util = require("util");
var exists2 = (0, import_util.promisify)(fs.exists);
var readFile2 = (0, import_util.promisify)(fs.readFile);
var SCRIPT_EXTENSION = /\.js$/i;
var headers = { "Content-type": "text/javascript" };
async function* candidates(root, path) {
  if (SCRIPT_EXTENSION.test(path)) yield (0, import_path.join)(root, path);
  const module_id = path.replace(SCRIPT_EXTENSION, "");
  const package_dir = (0, import_path.join)(root, module_id);
  const package_file = (0, import_path.join)(package_dir, "package.json");
  let package_candidates = [];
  if (await exists2(package_file))
    try {
      const metadata = require(package_file);
      package_candidates = [
        metadata["umd:main"],
        metadata.browser,
        metadata.module,
      ];
    } catch {}
  for (const name of [...package_candidates, "index.js", `${module_id}.js`])
    yield (0, import_path.join)(package_dir, name);
}
var with_node_modules =
  ({ root }) =>
  async ({ path }) => {
    for await (const file of candidates(root, path))
      if (await exists2(file))
        return { ...STATUS.OK, headers, body: await readFile2(file, "utf-8") };
    return { ...STATUS.NOT_FOUND, message: `Couldn't find module for ${path}` };
  };

// packages/interop-http-service/src/handlers/with_path_mount.ts
var SLASHES = /^\/+|\/+$/;
var trim_slashes = s => s.replace(SLASHES, "");
var trim_prefix = (a, prefix) =>
  a.startsWith(prefix) ? a.substring(prefix.length) : a;
var with_path_mount =
  ({ mappings }) =>
  request => {
    const mapping = mappings.find(
      ({ path }) => !path || request.path.startsWith(path)
    );
    if (!mapping) return STATUS.NOT_FOUND;
    return mapping.handler({
      ...request,
      path: trim_slashes(trim_prefix(request.path, mapping.path)),
    });
  };

// packages/interop-http-service/src/handlers/with_static_files.ts
var real_fs = __toESM(require("fs"));
var _path = __toESM(require("path"));
var MIME_TYPES = {
  ".avi": "video/avi",
  ".bmp": "image/bmp",
  ".css": "text/css",
  ".gif": "image/gif",
  ".htm": "text/html",
  ".html": "text/html",
  ".ico": "image/x-icon",
  ".jpeg": "image/jpeg",
  ".jpg": "image/jpeg",
  ".js": "text/javascript",
  ".json": "application/json",
  ".mjs": "text/javascript",
  ".mov": "video/quicktime",
  ".mp3": "audio/mpeg3",
  ".mpa": "audio/mpeg",
  ".mpeg": "video/mpeg",
  ".mpg": "video/mpeg",
  ".oga": "audio/ogg",
  ".ogg": "application/ogg",
  ".ogv": "video/ogg",
  ".pdf": "application/pdf",
  ".png": "image/png",
  ".svg": "image/svg+xml",
  ".tif": "image/tiff",
  ".tiff": "image/tiff",
  ".txt": "text/plain",
  ".wav": "audio/wav",
  ".xml": "text/xml",
};
var with_static_files = options => request => {
  const root = options.root ?? ".";
  const default_document = options.default_document ?? "index.html";
  const fs2 = options.fs ?? real_fs;
  const file_path = _path.resolve(root, request.path);
  let stat;
  try {
    stat = fs2.statSync(file_path);
  } catch (error) {
    if (error.code === "EACCES") return STATUS.UNAUTHORIZED;
    if (error.code === "ENOENT") return STATUS.NOT_FOUND;
    throw error;
  }
  let file_to_serve;
  if (stat.isFile()) file_to_serve = file_path;
  else if (stat.isDirectory()) {
    const index = _path.join(file_path, default_document);
    if (fs2.existsSync(index)) file_to_serve = index;
    else return STATUS.UNAUTHORIZED;
  } else {
    console.warn("Unsupported file system entry", stat);
    throw new Error("Unsupported file system entry");
  }
  const extension = _path.extname(file_to_serve);
  const mime_type = MIME_TYPES[extension] || "application/octet-stream";
  const file_content = fs2.readFileSync(file_to_serve);
  return {
    ...STATUS.OK,
    body: file_content,
    headers: {
      "Content-type": mime_type,
      "Cache-Control": "no-cache, no-store, must-revalidate",
      Pragma: "no-cache",
      Expires: "0",
    },
  };
};

// packages/interop-http-service/src/create_http_server.ts
var http = __toESM(require("http"));
var querystring = __toESM(require("querystring"));
var import_url = require("url");

// packages/interop-http-service/src/stream_read_to_end.ts
var stream_read_to_end = stream =>
  new Promise((resolve2, reject) => {
    const buffer = [];
    stream.setEncoding("utf-8");
    stream.on("data", chunk => buffer.push(chunk));
    stream.on("end", () => resolve2(buffer.join("")));
    stream.on("error", reject);
  });

// packages/interop-http-service/src/create_http_server.ts
var import_util2 = require("util");
var DEFAULT2 = {
  port: 8800,
  handler: () => STATUS.OK,
};
var trim_slashes2 = s => s.replace(/^\/+|\/+$/, "");
var create_http_server = options => {
  const { port, handler } = { ...DEFAULT2, ...options };
  const handle = with_error_boundary({ handler });
  const server = http.createServer((req, res) => {
    const respond = response => {
      res.statusCode = response.status;
      res.statusMessage = response.message;
      if (response.headers)
        for (let key of Object.keys(response.headers))
          res.setHeader(key, response.headers[key]);
      const content = response.body ?? (response.message || "");
      const buffer =
        typeof content === "string" ? new Buffer(content, "utf8") : content;
      res.setHeader("Content-length", buffer.length);
      res.end(buffer);
    };
    stream_read_to_end(req)
      .then(body => {
        if (!req.url || !req.method) return STATUS.BAD_REQUEST;
        const url = (0, import_url.parse)(req.url);
        const request = {
          method: req.method,
          path: trim_slashes2(url.pathname || ""),
          query: querystring.decode(url.query || ""),
          body,
        };
        return handle(request);
      })
      .then(respond);
  });
  try {
    server.listen(port);
  } catch (error) {
    console.log((0, import_util2.inspect)(error));
    console.log("http:server BindFailed", error);
  }
  server.on("error", error => {
    console.log((0, import_util2.inspect)(error));
    console.log("http:server BindFailed", error);
    throw error;
  });
  return {
    is_listening() {
      return server.listening;
    },
    on(event, listener) {
      if (event === "listening") {
        server.on("listening", listener);
      }
    },
    off(event, listener) {
      if (event === "listening") {
        server.off("listening", listener);
      }
    },
    dispose() {
      server.close();
    },
  };
};
// Annotate the CommonJS export names for ESM import in node:
0 &&
  (module.exports = {
    MIME_TYPES,
    STATUS,
    create_http_server,
    with_constant,
    with_error_boundary,
    with_node_modules,
    with_path_mount,
    with_static_files,
  });
//# sourceMappingURL=index.js.map
