echo started > .\dummy-sleep

# trap {
# 	echo "Error found: $_"
# 	echo "you're done"
# 	echo trapped >> .\dummy-sleep
# }

# $ErrorActionPreference = 'Break'

$myPid = [System.Diagnostics.Process]::GetCurrentProcess().Id
$watchdog = Start-Process 'powershell' "-c .\watchdog.ps1 $myPid" -PassThru -WindowStyle Hidden
sleep 10

echo ended >> .\dummy-sleep
