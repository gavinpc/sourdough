tup.creategitignore()

--========================================================

is_windows = tup.getconfig('TUP_PLATFORM') == 'win32'
is_macos = tup.getconfig('TUP_PLATFORM') == 'macosx'

function is_nonblank(s)
  return s ~= nil and s ~= ''
end

function get_application_data_path(name)
  assert(is_nonblank(name))
  if is_windows then
    tup.import('LOCALAPPDATA')
    assert(is_nonblank(LOCALAPPDATA), "LOCALAPPDATA must be defined on Windows!")
    return LOCALAPPDATA..'/'..name
  end
  if is_macos then
    error('Not implemented: application_data_path for MacOS')
  end
  return HOME..'/.cache/'..name
end

function get_web_cache_path()
  tup.import('WEB_CACHE')
  if is_nonblank(WEB_CACHE) then
    -- Force value copy.  Without this concat, the returned value is corrupt;
    -- specifically, `gsub` says it's nil, even though it concats and prints ok.
    return WEB_CACHE..''
  end
  return get_application_data_path('sourdough-web')
end

--========================================================

function initial_tests()
  tup.import('WEB_CACHE')
  if WEB_CACHE then
    print('at parse time, WEB_CACHE was found in environment '..WEB_CACHE)
    tup.export('WEB_CACHE')
  else
    print('at parse time, WEB_CACHE was NOT found in environment')
    tup.import('HOME')
    if HOME then
      print('at parse time, HOME was found in environment '..HOME)
      tup.import('WEB_CACHE='..HOME..'/.cache/codes.def/web')
    else
      print('at parse time, HOME was NOT found in environment!! that is a problem')
    end
  end
end

function test_dumping_env()
  output = 'all-env'
  -- env vars *per se* should not be platform-specific, but alas, everything else is
  if is_windows then
    tup.definerule({
        command = 'powershell -Version 3.0 -NoLogo -NoProfile -NonInteractive -File dump-env.ps1 '..output,
        outputs = {output, '^StartupProfileData-NonInteractive'}
    })
  else
    tup.definerule({
        command = 'dump-env.sh '..output,
        outputs = {output}
    })
  end
end

function test_using_web_cache_var()
  output = 'use-web-cache-result'
  if is_windows then
    tup.definerule({
        command = 'powershell -Version 3.0 -NoLogo -NoProfile -NonInteractive -File use-web-cache-var.ps1 '..output,
        outputs = {output, '^StartupProfileData-NonInteractive'}
    })
  else
    tup.definerule({
        command = 'use-web-cache-var.sh '..output,
        outputs = {output}
    })
  end
end

function test_using_home_var()
  output = 'use-home-result'
  if is_windows then
    tup.definerule({
        command = 'powershell -Version 3.0 -NoLogo -NoProfile -NonInteractive -File use-home-var.ps1 '..output,
        outputs = {output, '^StartupProfileData-NonInteractive'}
    })
  else
    tup.definerule({
        command = 'use-home-var.sh '..output,
        outputs = {output}
    })
  end
end

function test_getting_app_data_path()
  local app_data_path = get_application_data_path('app-stuff')
  print('application data path is: '..app_data_path)
end

function test_getting_web_cache_path()
  local web_cache_path = get_web_cache_path()
  print('web cache path is: '..web_cache_path)
end

function test_using_web_cache_path()
  local web_cache_path_raw = get_web_cache_path()
  local web_cache_path = web_cache_path_raw:gsub('\\', '/')
  local output = 'with-web-cache-path'
  if is_windows then
    tup.definerule({
        command = 'powershell -Version 3.0 -NoLogo -NoProfile -NonInteractive -File ensure-web-cache-directory.ps1 '
          ..web_cache_path..' '..output,
        outputs = {output, '^'..web_cache_path, '^StartupProfileData-NonInteractive'}
    })
  else
    tup.definerule({
        command = './ensure-web-cache-directory.sh '..web_cache_path..' '..output,
        outputs = {output}
    })
  end
end

function test_off_tree_file()
  local output = 'C:/Users/gcannizzaro/research/scratch/test-out-2'
  tup.definerule({
      command = 'touch '..output,
      outputs = {output}
  })
end
-- for the last time, no
-- test_off_tree_file()

test_dumping_env()
test_using_web_cache_var()
test_using_home_var()
test_getting_app_data_path()
test_getting_web_cache_path()
test_using_web_cache_path()
