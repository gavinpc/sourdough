param ($WebCachePath, $Output)

function Use-Web-Cache-Path($WebCachePath) {
    if (-Not $WebCachePath) {
        throw "I expected you to provide a web cache path!"
    }
    Write-Output "Okay so the web cache path is $WebCachePath"

    if (Test-Path -Path "$WebCachePath" -Type Container) {
        Write-Output "It looks like the web cache IS already a directory!"
    } else {
        Write-Warning "It looks like the web cache is NOT already a directory!"
        New-Item -ItemType Directory -Force -Path "$WebCachePath"
        Write-Output "testing 123" > "$WebCachePath/dummy"
    }
}

Use-Web-Cache-Path "$WebCachePath" > $Output
