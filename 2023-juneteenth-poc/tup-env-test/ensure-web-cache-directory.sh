#!/bin/sh

# Ensure that the web cache directory exists, and echo it

set -e

web_cache_dir="$1"
output="$2"

ensure_web_cache_directory() {
    if [ -z "$web_cache_dir" ]; then
        >&2 echo 'I expected you to provide a web cache path!'
        return 1
    fi

    echo "Okay so the web cache path is $web_cache_path"

    if [ -d "$web_cache_dir" ]; then
        echo "It looks like the web cache IS already a directory!"
    else
        >&2 echo 'It looks like the web cache is NOT already a directory!'
    fi
}

ensure_web_cache_directory > "$output"

exit 0
