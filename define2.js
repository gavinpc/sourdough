(function def_init() {
  // Userland object graph I/O (aka AMD)
  const { defineProperties } = Object;
  const { isArray } = Array;

  const is_special = need => need === "exports" || need === "module";

  function make_world(ns, defines = new Map()) {
    const cache = new Map();

    const amd = {};

    const requiring = [];
    function require_with_check(id) {
      if (requiring.includes(id))
        throw Error(`cycle! ${[...requiring, id].join(" → ")}`);
      requiring.push(id);
      try {
        return require(id);
      } finally {
        requiring.pop();
      }
    }

    function define_or_throw_conflict(name, term) {
      // RequireJS does not throw for a redefinition, but also doesn't redefine
      if (defines.has(name)) throw Error(`Conflict: Already defined: ${name}`);
      defines.set(name, term); // only place this is done
      revisit_goals();
    }

    function define(...term) {
      switch (term.length) {
        case 1:
          return; // no-op, as in requirejs.  could normalize name
        case 2: {
          const [name, formula] = term;
          if (typeof name !== "string") {
            // until there's a loader
            throw Error("NotImplemented: anonymous define");
          }
          // If formula is not a function, retain the host value eagerly (w/o require)
          if (typeof formula !== "function") {
            define_or_throw_conflict(name, term);
            return;
          }
          return define(name, [], formula);
        }
        case 3: {
          const [name, _needs, formula] = term;
          if (typeof formula !== "function")
            throw Error(`BadRequest: define₃ expects f'n formula`);
          // lazy: remember the form to evaluate later (even if no needs)
          define_or_throw_conflict(name, term);
          break;
        }
        default:
          throw Error(`BadRequest: no such arity ${term.length}`);
      }
    }

    function require1(it) {
      if (isArray(it)) return it.map(require1); // distribute over array
      if (typeof it !== "string")
        throw Error(`BadRequest: expected string or array`);
      if (cache.has(it)) return cache.get(it);
      if (!defines.has(it)) throw Error(`NotFound: ${it}`);

      const define_term = defines.get(it);
      let result;
      if (define_term.length === 2) {
        [, result] = define_term;
      } else if (define_term.length === 3) {
        const [, needs, formula] = define_term;
        result = amd_construct(needs, formula);
      }
      // shouldn't this also go under completed? if this was the top-level require
      cache.set(it, result); // only place this is done
      return result;
    }

    const evaluate_require = ([needs, formula]) =>
      formula(...needs.map(require1));

    function require(...term) {
      switch (term.length) {
        case 1:
          return require1(term[0]);
        case 2:
          return evaluate_require(term);
        case 3: {
          try {
            return evaluate_require(term);
          } catch (error) {
            const [needs, formula, errback] = term;
            if (typeof errback === "function") {
              console.warn(needs, formula, error);
              errback(error);
            } else {
              throw error;
            }
          }
          break;
        }
        default:
          throw Error(`BadRequest: no such arity ${term.length}`);
      }
    }

    function amd_construct(needs, factory, stack) {
      const exports = {};
      const module = { exports };
      // TODO: needed for AMD compatibility
      // but could also use to track dependencies dynamically
      //const require = make_contextualized_require(context);
      const special = { module, exports, require };
      const imports = needs.map(id => special[id] ?? require_with_check(id));
      const result =
        typeof factory === "function" ? factory(...imports) : factory;
      return needs.some(is_special) ? exports : result;
    }

    defineProperties(define, { amd: { enumerable: true, value: amd } });

    return { define, require };
  }

  const { define, require } = make_world();
  const def = define;
  const globals = { define, require, def };
  Object.assign(def, globals, { make_world });
  define("def", () => def);
})();

define("def:self-tests", [], () => {
  console.debug("so, you want def self tests, eh?");

  function assert(condition, message, ...data) {
    if (!condition) {
      if (data.length) console.error(`Assert failed`, message, ...data);
      throw new Error(`Assert failed: ${message}`);
    }
  }
  function assert_throws(thunk, message) {
    try {
      thunk();
      throw Error(message);
    } catch {}
  }

  async function test_world({ define, require }) {
    // unary define is a no-op
    assert_throws(() => define(), "define₀ throws");
    assert_throws(() => require(), "require₀ throws");
    assert_throws(() => require(3), "require₁ expects string");
    assert_throws(() => require(true), "require₁ expects string");
    assert_throws(() => require({}), "require₁ expects string");

    function resolve_case(from, path, expect) {
      const got = require.resolve(path, from);
      assert(
        got === expect,
        `if module "${from}" asks for "${path}", that resolves to "${expect}" (not ‘${got}’)`
      );
    }
    resolve_case("a/b/c", "d", "d");
    resolve_case("a/b/c", "d/e", "d/e");
    // following 2 cases are from https://github.com/amdjs/amdjs-api/blob/master/AMD.md#module-id-format-
    resolve_case("a/b/c", "../d", "a/d");
    resolve_case("a/b/c", "./e", "a/b/e");
    resolve_case("/plays/Ham", "./Mac", "/plays/Mac");
    // Not sure about either of these
    // resolve_case("a/b/c", ".", "a/b/c")
    // resolve_case("a/b/c", "..", "a/b")
    // WHAT about this? or is a/b/c/ not a valid module id?
    // resolve_case("a/b/c/", "./e", "a/b/e")
    // § 5.4.1 “Normal examples”, from https://datatracker.ietf.org/doc/html/rfc3986#section-5.4.1
    resolve_case("http://a/b/c/d;p?q", "./g", "http://a/b/c/g");
    resolve_case("http://a/b/c/d;p?q", ".", "http://a/b/c/");
    resolve_case("http://a/b/c/d;p?q", "./", "http://a/b/c/");
    resolve_case("http://a/b/c/d;p?q", "..", "http://a/b/");
    resolve_case("http://a/b/c/d;p?q", "../", "http://a/b/");
    resolve_case("http://a/b/c/d;p?q", "../g", "http://a/b/g");
    resolve_case("http://a/b/c/d;p?q", "../..", "http://a/");
    resolve_case("http://a/b/c/d;p?q", "../../", "http://a/");
    resolve_case("http://a/b/c/d;p?q", "../../g", "http://a/g");

    define("answer", 3);
    // this is optional on global, and in fact requirejs throws
    assert(require("answer") === 3, "sync define & require value");
  }

  function main() {
    // but `require` could be Node's...
    assert(typeof define === "function", "global `define` is a function");
    assert(typeof require === "function", "global `require` is a function");
    assert(typeof define.amd === "object", "global `define.amd` is an object");

    const world = def.make_world();
    test_world(world);

    describe("define & require basic properties (all worlds)", () => {
      console.info(`define("color", ["module"], module => {...})`);
      // THIS test is problematic after roundtripping the system
      // because `color` will have been defined at the top level and this will be conflict
      define("color", ["module"], module => {
        module.exports.blend = (a, b) => {};
        module.exports.lighten = (x, y) => {};
      });
      assert(
        typeof require("color") === "object",
        "definition importing `module` yields an object"
      );
      assert(
        typeof require("color").blend === "function",
        "color module has `blend` function"
      );
      assert(
        typeof require("color").lighten === "function",
        "color module has `lighten` function"
      );

      console.info(`define("math", ["exports"], exports => {...})`);
      define("math", ["exports"], exports => {
        exports.square = () => {};
        exports.cube = () => {};
      });
      assert(
        typeof require("math") === "object",
        "definition importing `exports` yields an object"
      );
      assert(
        typeof require("math").square === "function",
        "math module has `square` function"
      );
      assert(
        typeof require("math").cube === "function",
        "math module has `cube` function"
      );
    });

    const microtask = () => new Promise(resolve => queueMicrotask(resolve));

    describe("microtask stuff", async (_it, { define, require }) => {
      const resolved = new Map();

      console.info(`require.async("A").then(A => resolved.set("A", A))`);
      require.async("A").then(A => resolved.set("A", A));
      assert(!resolved.has("A"), `!resolved.has("A")`);

      console.info(`define("A", "amicus")`);
      define("A", "amicus");
      assert(!resolved.has("A"), `!resolved.has("A")`);

      await microtask(); // confirm that stuff that should be enqueued is enqueued

      assert(resolved.has("A"), `resolved.has("A")`);
      assert(resolved.get("A") === "amicus", `resolved.get("A") === "amicus"`);

      const hearts = (a, b) => `(${a}♥${b})`;
      define("C", ["D", "E"], hearts);
      define("B", ["C", "D"], hearts);
      define("E", () => "pepper");
      define("D", () => "mill");

      assert(require("A") === "amicus", "A def");
      assert(require("B") === "((mill♥pepper)♥mill)", "B def");
      assert(require("C") === "(mill♥pepper)", "C def");
      assert(require("D") === "mill", "D def");
      assert(require("E") === "pepper", "E def");
    });

    describe("define.definitions", (_it, { define, require }) => {
      assert(
        typeof define.definitions === "object",
        "define.definitions is an object"
      );
      assert(define.definitions instanceof Map, "define.definitions is a Map"); // shortcut
      function confirm_definition(name, needs, formula) {
        assert(
          define.definitions.has(name),
          "define.definitions remembers definition by name"
        );
        const definition = define.definitions.get(name);
        assert(Array.isArray(definition), "definition is an Array (a tuple)");
        assert(
          definition instanceof def.amd.quote.define,
          "definition is an AMD define term"
        );
        assert(definition[0] === name, "definition remembers name as 1st");
        assert(
          definition.length === 2 || definition.length === 3,
          "definition is a 2-tuple or a 3-tuple"
        );
        if (definition.length === 2) {
          assert(definition[1] === formula, "definition₃ has formula in 2nd");
        } else {
          // AND is equivalent to given needs...
          assert(Array.isArray(definition[1]), "definition₃ has needs in 2nd");
          assert(Array.isArray(definition[1]), "definition₃ has needs in 2nd");
          assert(definition[2] === formula, "definition₃ has formula in 3rd");
        }
      }
      function test_needless_definition(name, value) {
        console.info(`${name} → ${value}`);
        const start_size = define.definitions.size;
        if (define.definitions.has(name)) throw Error(`Conflict ${name}`);
        define(name, value);
        assert(
          define.definitions.size === start_size + 1,
          "define.definitions gains one entry immediately after a new definition"
        );
        // assert(define.definitions.get(name).needs.length === 0, "definition `needs` is empty if no requirements")
        confirm_definition(name, [], value);
      }
      function test_needful_definition(name, needs, formula) {
        console.info(`${name} ${needs} → ${formula}`);
        const start_size = define.definitions.size;
        if (define.definitions.has(name)) throw Error(`Conflict ${name}`);
        define(name, needs, formula);
        confirm_definition(name, needs, formula);
        assert(
          define.definitions.size === start_size + 1,
          "define.definitions gains one entry immediately after a new definition"
        );
        // assert(cheap_equals(needs, define.definitions.get(name).needs), "definition remembers needs")
        assert(
          cheap_equals(needs, define.definitions.get(name)[1]),
          "definition remembers needs"
        );
      }
      with_group_collapsed(
        `primitive value`,
        test_needless_definition,
        "foo",
        99
      );
      with_group_collapsed(
        `function value`,
        test_needless_definition,
        "bar",
        () => "banana"
      );
      with_group_collapsed(
        `needs`,
        test_needful_definition,
        "crops",
        ["rain", "soil"],
        function grow(rain, soil) {}
      );
    });

    const has_goal = (r, needle) =>
      r.goals.find(g => equivalent_tagged_tuples(g, needle));

    describe("a complete example", (_it, { define, require }) => {
      let { extend } = define;
      let G = require.goals.length;
      let Q = define.amd.quote;

      const traced = new Map();
      const trace =
        (k, f) =>
        (...t) => {
          const result = f(...t);
          traced.set(k, result);
          return result;
        };

      assert(!define.definitions.has("cookie"), "`cookie` is not defined");
      assert(!define.definitions.has("dough"), "`dough` is not defined");
      assert(!define.definitions.has("oven"), "`oven` is not defined");

      const check_oven = trace(
        "A",
        oven => typeof oven.heat_now === "function"
      );
      console.info(`require(["oven"], check_oven)`, check_oven);
      require(["oven"], check_oven);

      assert(require.goals.length === G + 1, "the world gained a goal");
      assert(
        has_goal(require, Q.require(["oven"], check_oven)),
        "the goal is to `check_oven` when `oven` is available"
      );
      assert(traced.size === 0, "but that has not happened");

      const bake = trace("B", (oven, dough) => `${oven} ⊗ ${dough}`);
      console.info(`require(["oven", "dough"], bake)`, bake);
      require(["oven", "dough"], bake);

      assert(require.goals.length === G + 2, "the world gained another goal");
      assert(
        has_goal(require, Q.require(["oven", "dough"], bake)),
        "the goal is to `bake` when `oven` and `dough` are available"
      );
      assert(traced.size === 0, "no goals have been achieved");

      const oven = {
        toString() {
          return "OVEN";
        },
        heat_now(it) {
          // dummy operation
          if (typeof it?.temperature === "number") it.temperature++;
        },
      };
      console.info(`define("oven", oven)`, oven);
      define("oven", oven);

      assert(require("oven") === oven, `require("oven") === oven`);
      assert(
        !has_goal(require, Q.require(["oven"], check_oven)),
        "goal to `check_oven` is gone"
      );

      assert(traced.has("A"), "callback requiring oven resolved");
      assert(traced.get("A") === true, "`check_oven` returned `true`");

      console.info(`define("dough", "boy")`);
      define("dough", "boy");

      assert(require("dough") === "boy", '`dough` is now defined as "boy"');
      assert(
        !has_goal(require, Q.require(["oven", "dough"], bake)),
        "goal to `bake` pending `oven` and `dough` is gone"
      );

      assert(traced.has("B"), "callback requiring oven and dough resolved");
      if (
        !assert(
          traced.get("B") === `OVEN ⊗ boy`,
          "`bake(oven, dough)` returned expected result"
        )
      )
        console.log(`traced.get("B")`, traced.get("B"));

      // I don't think the remaining tests add anything essential
      const bake_cookie = (oven, dough) => oven && dough && `🍪`;
      console.info(
        `define("cookie", ["oven", "dough"], bake_cookie)`,
        bake_cookie
      );
      define("cookie", ["oven", "dough"], bake_cookie);

      assert(require("cookie") === "🍪", "require(`cookie`) === 🍪");

      const eat = trace("D", (monster, cookie) => `${monster} ⊰ ${cookie}`);
      console.info(`require(["monster", "cookie"], eat)`, eat);
      require(["monster", "cookie"], eat);

      define("monster", "MONSTER");

      assert(
        traced.get("D") === `MONSTER ⊰ 🍪`,
        `traced.get("D") === "MONSTER ⊰ 🍪"`
      );
    });

    describe("amd basic", (_it, { define, require }) => {
      console.log("Show that definition function is called only once");
      let _i = 0;
      define("counter1", () => {
        ++_i;
        return { i: _i };
      });
      assert(require("counter1").i === 1, "counter is 1 on the first call");
      assert(require("counter1").i === 1, "counter is 1 on the second call");

      // I don't think this proves anything beyond the above
      // it just uses only closures instead of extended properties of define/require
      // so should be usable with other AMD implementations
      let _a, _b, _c, _ab, _bc, _ac, _abc;
      let G = require.goals.length;

      const requires = [
        [["a"], a => (_a = a)],
        [["b"], b => (_b = b)],
        [["c"], c => (_c = c)],
        [["a", "b"], (a, b) => (_ab = { a, b })],
        [["b", "c"], (b, c) => (_bc = { b, c })],
        [["a", "c"], (a, c) => (_ac = { a, c })],
        [["a", "b", "c"], (a, b, c) => (_abc = { a, b, c })],
      ];
      for (const [needs, factory] of requires) {
        require(needs, factory);
        console.info(`require(${JSON.stringify(needs)}, ${factory})`);
      }

      assert(require.goals.length === G + 7, "world has 7 new goals");

      define("a", ["b"], b => ({ alpha: b }));
      console.info(`define("a", ["b"], b => ({alpha: b}))`);
      assert(_a === undefined, "`_a` has not been set");
      assert(require.goals.length === G + 7, "world still has 7 new goals");
      define("b", ["c"], c => ({ beta: c }));
      console.info(`define("b", ["c"], c => ({beta: c}))`);
      assert(_a === undefined, "`_a` has still not been set");
      assert(_b === undefined, "`_b` has not been set");
      assert(_c === undefined, "`_c` has not been set");
      assert(require.goals.length === G + 7, "world still has 7 new goals");
      define("c", 3);
      console.info(`define("c", 3)`);
      // This doesn't technically prove that the new goals are the achieved ones
      assert(
        require.goals.length === G + 0,
        "all new goals have been achieved"
      );

      assert(require("c") === 3, `require("c") === 3`);
      assert(
        cheap_equals(require("b"), { beta: 3 }),
        `require("b") is {beta: 3}`
      );
      assert(
        cheap_equals(require("a"), { alpha: { beta: 3 } }),
        `require("a") is {alpha:{beta: 3}}`
      );

      assert(cheap_equals(_c, 3), "_c is set to 3");

      assert(cheap_equals(_b, { beta: 3 }), "_b is set to {beta: 3}");
      assert(
        cheap_equals(_a, { alpha: { beta: 3 } }),
        "_a is set to {alpha:{beta: 3}}"
      );

      assert(
        cheap_equals(_ab, { a: { alpha: { beta: 3 } }, b: { beta: 3 } }),
        `_ab is set to {a: {alpha: {beta: 3}}, b: {beta: 3}}`
      );
      assert(
        cheap_equals(_bc, { b: { beta: 3 }, c: 3 }),
        `_bc is set to {b: {beta: 3}, c: 3}`
      );
      assert(
        cheap_equals(_ac, { a: { alpha: { beta: 3 } }, c: 3 }),
        `_ac is set to {a: {alpha: {beta: 3}}, c: 3}`
      );

      assert(
        cheap_equals(_abc, { a: { alpha: { beta: 3 } }, b: { beta: 3 }, c: 3 }),
        `_abc is set to {a: {alpha: {beta: 3}}, b: {beta: 3}, c: 3}`
      );

      assert(
        cheap_equals(require("a"), { alpha: { beta: 3 } }),
        `require("a") is {alpha: {beta: 3}}`
      );
    });

    await describe("avoid infinite regress", (_it, { define, require }) => {
      // this will cause "too much recursion"
      // unless you forbid the requiring of X during the defining of X
      define("journey", () => {
        require("journey");
      });
      require("journey");
      // test separately for imported require...
      // test separately for indirect cycle
      // is nested define a thing?
      // in requireJS, was getting no clear policy on things like this
      /*
define("y", [], () => {
  define("z", [], () => ({z:1});
  define("./z", [], () => ({z:2});
  // return {y: 2}
}) 
require(['y'], a => console.log('y is', a))
require(['./z'], a => console.log('./z is', a))
*/
    });

    await describe("require.async basic properties", (_it, {
      define,
      require,
    }) => {
      console.log("Things about require.async that we can test synchronously");
      let G = require.goals.length;

      assert(
        typeof require.async === "function",
        "require.async is a function"
      );
      assert(
        typeof require.async("xyz").then === "function",
        "require.async(id) returns a thenable when `id` is unknown"
      );

      define("happy", true);
      console.info(`define("happy", true)`);
      assert(
        require.async("happy") === true,
        `require.async("happy") resolves immediately to true`,
        `successful async lookup is synchronous`
      );

      assert(
        throws(() => define("happy", true)),
        `attempting to redefine throws (Conflict) synchronously`
      );

      assert(!define.definitions.has("color"), "`color` is not defined");

      let _color;
      require.async("color").then(color => (_color = color));
      console.info(`require.async("color").then(color => _color = color)`);

      define("color", "green");
      console.info(`define("color", "green")`);
      queueMicrotask(() => assert(_color === "green", `_color is "green"`));

      define("weapon", "sword");
      define("horcrux", "locket");

      let _wh_result1;
      require(["weapon", "horcrux"], (weapon, horcrux) => {
        console.debug("CALLED WH CALLBACK 1");
        _wh_result1 = `1 ${weapon} ⋇ ${horcrux}`;
      });
      assert(
        _wh_result1 === "1 sword ⋇ locket",
        `_wh_result1 === "1 sword ⋇ locket"`
      );

      let _wh_result2;
      Promise.resolve(require.async(["weapon", "horcrux"])).then(
        ([weapon, horcrux]) => {
          console.debug("CALLED WH CALLBACK 2");
          _wh_result2 = `2 ${weapon} ⋇ ${horcrux}`;
        }
      );
      function prove_it() {
        assert(
          _wh_result2 === "2 sword ⋇ locket",
          `_wh_result2 === "2 sword ⋇ locket"`
        );
      }
      // prove_it() this fails, and that's okay
      queueMicrotask(prove_it);
    });

    // TODO: check for circularity.  AMD tests indicate this is handled through `exports`

    // End of synchronous tests
    // From here on we can't safely use console grouping
    // `await` is not very useful for writing tests anyway

    describe("require.async([], fn)", async (_it, { define, require }) => {
      assert(
        (await require.async([], () => 3)) === 3,
        "require.async resolves to callback's return value"
      );
    });

    describe("require.async(string[]) sync", async (_it, world) => {
      const { define, require } = world;
      define("color", "green");
      define("name", "J");
      assert(
        cheap_equals(require.async(["color", "name"]), ["green", "J"]),
        `require.async(string[]) resolves synchronously to array of predefined values`
      );
    });

    // currently there's no more direct way to ask this
    function assert_not_defined(require, what) {
      assert(
        throws(() => require(what)),
        `expected not defined ${what}`
      );
    }

    describe("require.async(string)", async (_it, { define, require }) => {
      assert_not_defined(require, "🚁");
      const promise = require.async("🚁");
      define("🚁", "HELICOPTER");
      await microtask();
      const resolved = await promise;
      assert(
        resolved === "HELICOPTER",
        "require.async(string) resolves to unary value defined afterwards"
      );
    });

    describe("require.async(string[])", async (_it, { define, require }) => {
      assert_not_defined(require, "simon");
      assert_not_defined(require, "garfunkel");
      const promise = require.async(["simon", "garfunkel"]);
      define("simon", "paul");
      define("garfunkel", "art");
      await microtask();
      const resolved = await promise;
      assert(
        cheap_equals(resolved, ["paul", "art"]),
        "require.async(string[]) resolves to array of values defined afterwards"
      );
    });

    console.log("ALL TESTS ISSUED =====================");
    setTimeout(() => {
      console.log(`${failed.length} assertions failed`);
      for (const failure of failed) console.warn(...failure);
    }, 1);
  }
  return main;
});

(function def_init_old() {
  globalThis[Symbol("https://def.codes")] = def_init;
  if (globalThis.def) {
    console.log("Pssst, conflict def");
    return;
  }

  const { create, assign, freeze, defineProperty, entries, hasOwn } = Object;
  const { construct, get } = Reflect;
  const { toPrimitive, toStringTag } = Symbol;
  const Symbol_for = Symbol.for;

  // name is an IRI (percent encoded, no space)
  // does not currently sanitize names, but the mint wrapper does
  const metaconstructor_tagged = metaclass => name => {
    const constructor = {
      [name]: function (...args) {
        return construct(metaclass, args, constructor);
      },
    }[name];
    const prototype = create(metaclass.prototype);
    defineProperty(constructor, "prototype", { value: prototype });
    defineProperty(prototype, "constructor", { value: constructor });
    defineProperty(prototype, toStringTag, { value: name });
    return constructor;
  };

  function coiner(fun) {
    const target = create(null);
    const prototype = new Proxy(target, {
      get(target, key, receiver) {
        if (typeof key === "string") {
          if (!hasOwn(target, key)) {
            const value = fun(key);
            defineProperty(target, key, { value, enumerable: true });
            return value;
          }
        }
        // system reserves the right to intercept symbols
        return get(target, key, receiver);
      },
    });
    return create(prototype);
  }
  coiner.comment = `Make an object where every (string) property is answered by the same function, which is given the requested key.  Once read, properties are remembered as readonly values.  The returned object thus has an “index signature” described by the function.`;

  function memoize1(fun, name = fun.name, cache = new Map()) {
    return {
      [name]: function (k) {
        if (cache.has(k)) return cache.get(k);
        const result = fun(k);
        cache.set(k, result);
        return result;
      },
    }[name];
  }
  memoize1.comment = `A no-eviction cache for synchronous unary functions.`;

  // does NOT recognize prefixes!  anything before colon is treated as scheme
  class Name extends URL {} // MUTABLE!!
  const name = s => new Name(s);

  const mint = {
    comment: `Some experimental tools`,
    in(ns) {
      name(ns); // throws if the name is not acceptable
      return {
        names: coiner(k => `${ns}${k}`),
        urls: coiner(k => name(`${ns}${k}`)),
        symbols: coiner(k => Symbol_for(`${ns}${k}`)),
        tuples: coiner(memoize1(k => TupleKind(`${ns}${k}`, k))),
        records: coiner(memoize1(k => RecordKind(`${ns}${k}`, k))),
        coiners: coiner(memoize1(k => coiner(`${ns}${k}`, k))),
        subclasses: superclass =>
          coiner(memoize1(metaconstructor_tagged(superclass))),
      };
    },
  };

  const errors = mint.in("https://def.codes/ns/errors/").subclasses(Error);
  // HTTP equiv 300, 400, 404, 409, 501
  // TODO: `prototype.name` property should be this local name
  // see also rdfjs
  const { MultipleChoices, BadRequest, NotFound, Conflict, NotImplemented } =
    errors;

  // described by, etc
  // https://www.rfc-editor.org/rfc/rfc9110.html#name-400-bad-request
  // once again, MDN does it right:
  // https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/100
  // hence
  // x∈HttpStatusCode → x describedBy `https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/${x}`
  function extend(o, ps) {
    for (const [k, value] of entries(ps))
      defineProperty(o, k, { value, enumerable: true });
    return o;
  }

  class Record {
    constructor(initial) {
      if (initial) assign(this, initial); // for read-only on just these, use extend
    }
  }
  class Tuple extends Array {
    constructor(...args) {
      // Unlike `Array` constructor, args are *always* elements, never `length`.
      super();
      this.push(...args);
    }
    // WIP
    toString(...args) {
      return `def.TupleKind("${name}")(${this.map(function (x) {
        return x.toString(...args);
      }).join(",")})`;
    }
    // Prevent crash on `map`, `filter`, etc due to object being frozen
    // But see warning at https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/@@species
    // also freeze is gone and will be opt-in
    /*
    static get [Symbol.species]() {
      return Array;
    }
    */
  }

  const RecordKind = metaconstructor_tagged(Record);
  const TupleKind = metaconstructor_tagged(Tuple);

  const AMD = "https://github.com/amdjs/amdjs-api/";
  // def.prefixes.amd = AMD

  const RELATIVE_PATH = /^\.\.?(?:$|\/)/;
  const is_relative_path = path => RELATIVE_PATH.test(path);
  is_relative_path.seeAlso = name(`${AMD}blob/master/AMD.md#module-id-format-`);
  is_relative_path.comment = `Answer whether the path is relative, according to the AMD definition of module id's.`;

  function combine_url_paths(path, from) {
    if (!is_relative_path(path)) return path;
    const url1 = new URL(`http://duty/${from}`);
    const url2 = new URL(`http://duty${url1.pathname}/../${path}`);
    return url2.pathname.replace(/^\//, "");
  }
  combine_url_paths.seeAlso = name(
    `${AMD}blob/master/AMD.md#module-id-format-`
  );
  combine_url_paths.comment = `Answer the qualified version of ‘path’ against ‘from’, using standard URL resolution`;

  const amd = { quote: mint.in(`${AMD}wiki/AMD#`).tuples };

  // the synchronous define/require doesn't speak unless spoken to
  function make_world(ns, defines = new Map()) {
    const goals = []; // public... should be a buffer with some limit
    const completed = []; // also let history roll off
    const defined = x => defines.has(x);

    const dlq = new Set();

    // discharge results where needs can now be met
    // called after a host value is added to the cache for some name.
    // could be more surgical knowing what name has been added
    function revisit_goals() {
      for (let i = goals.length - 1; i !== -1; i--) {
        const goal = goals[i];
        if (dlq.has(goal)) continue;
        if (goal instanceof amd.quote.require) {
          const [needs, formula] = goal;
          if (needs.every(defined)) {
            try {
              require(needs, formula);
              goals.splice(i, 1);
            } catch (error) {
              dlq.add(goal);
              console.error(goal, error);
            }
          }
        }
      }
    }

    const { isArray } = Array;

    const { get } = Reflect;
    return {
      define: new Proxy(extend(define, { amd, definitions: defines }), {
        get(target, key, receiver) {
          if (hasOwn(target, key)) return get(target, key, receiver);
          // undefined behavior
          return { "marry, what's a": key };
        },
      }),
      // goals & completed, and cache should be read-only views
      require: new Proxy(
        extend(require, {
          goals,
          completed,
          cache,
          resolve: combine_url_paths,
          async: make_require_async({ require }),
        }),
        {
          get(target, key, receiver) {
            if (hasOwn(target, key)) return get(target, key, receiver);
            // undefined behavior
            return { "I should like to find you a": key };
          },
        }
      ),
    };
  }

  function make_require_async({ require }) {
    const { isArray } = Array;
    const { assert } = console;
    function require_async(...term) {
      try {
        return require(...term);
      } catch {}
      switch (term.length) {
        case 1:
          return new Promise(resolve => {
            if (isArray(term[0])) {
              require(term[0], (...imports) => resolve(imports));
            } else {
              require(term, import_ => resolve(import_));
            }
          });
        // Just so `require` and `require.async` have the same semantics...
        case 2:
          return new Promise((resolve, reject) => {
            const [needs, callback] = term;
            assert(isArray(needs), `require async expected array needs`);
            require(needs, (...imports) =>
              resolve(callback(...imports)), reject);
          });
        case 3:
          return new Promise(resolve => {
            const [needs, callback, errback] = term;
            assert(isArray(needs), `require async expected array needs`);
            require(needs, (...imports) =>
              resolve(callback(...imports)), error => errback(error));
          });
      }
      throw BadRequest(`no such arity ${term.length}`);
    }
    require_async.comment =
      "an async extension of `require` that returns promises that resolves after all needs were met";
    return require_async;
  }

  // AMD terms -> JS interpretation
  function write_define_JS(term) {
    switch (term.length) {
      case 2: {
        const [id, value] = term;
        // HACK: we should `write` value here
        return `define(${JSON.stringify(id)}, ${JSON.stringify(value)});`;
      }
      case 3: {
        const [id, needs, formula] = term;
        return `define(${JSON.stringify(id)}, ${JSON.stringify(
          needs
        )}, ${formula});`;
      }
    }
  }
  function write_require_JS([needs, formula]) {
    return `def.require(${JSON.stringify(needs)}, ${formula});`;
  }

  function io_javascript_write(term) {
    if (term instanceof def.amd.quote.define) return write_define_JS(term);
    if (term instanceof def.amd.quote.require) return write_require_JS(term);
    throw NotImplemented(`JS i/o for ${term}`);
  }
  io_javascript_write.comment = `Answer with JavaScript code that represents the given term`;

  const io = { js: { write: io_javascript_write } };

  function prompt_download(file, data) {
    const a = document.createElement("a");
    a.style.display = "none";
    a.download = file;
    a.href = data;
    a.target = "_blank";
    document.body.appendChild(a);
    a.click();
    a.remove();
  }
  prompt_download.seeAlso = `https://gist.github.com/cosmospham/7330466`;
  prompt_download.comment = `Offer to download a file called ‘file’ containing the given data URL.`;

  // The file is going to be saved to the user's downloads folder (whatever that is)
  // we here have no control over that
  // In previous testing I recall I was using a file monitor to move these into another place
  function prompt_download_document(file = `uspace.html`) {
    // special preprocess: needed to persist changes to textarea content
    for (const textarea of document.querySelectorAll("textarea")) {
      try {
        textarea.replaceChildren(document.createTextNode(textarea.value));
      } catch (error) {
        console.warn("Couldn't persist textarea content", error);
      }
    }

    let url;
    const type = "text/html";
    // XMLSerializer incorrectly encodes script (and probably style) content
    const data0 = new XMLSerializer().serializeToString(document);
    const data = document.documentElement.outerHTML;
    const blob = new Blob([data], { type });
    try {
      url = window.URL.createObjectURL(blob);
      prompt_download(file, url);
    } finally {
      URL.revokeObjectURL(url);
    }
  }
  prompt_download_document.comment = `Offer the user a download of the current document.  Currently browser-only.`;

  function prompt_download_def(file = "def-reconstituted.js") {
    let url;
    const type = "application/javascript";
    const all_terms = [
      `(${def.init})();`,
      // HACK: will throw Conflict because def_init defines it
      ...Array.from(define.definitions.values())
        .filter(([name]) => name !== "def")
        .map(term => io_javascript_write(term)),
      // shouldn't this include goals?
      ...Array.from(require.completed.values(), io_javascript_write),
    ];
    const data = all_terms.join("\n\n");
    const blob = new Blob([data], { type });
    try {
      url = window.URL.createObjectURL(blob);
      prompt_download(file, url);
    } finally {
      URL.revokeObjectURL(url);
    }
  }
  prompt_download_document.comment = `Offer the user a download of the current document.  Currently browser-only.`;

  const userland = {
    prompt_download,
    prompt_download_document,
    prompt_download_def,
  };

  // no, this won't roundtrip
  // nested define cannot be a thing, if you want I/O
  // define("def:map", () => map);

  Object.assign(def, globals, {
    TupleKind, // needed so their toString can emit runnable JS
    errors,
    init: def_init,
    io,
    userland,
    make_world,
    mint,
    // PROVISIONAL: so you can use in method constraints
    Record,
    Tuple,
  });
  define("def", () => def);
  extend(globalThis, globals);
  // everything after this should use define
});
