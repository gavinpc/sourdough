<article>
  <h1><time datetime="2023-05-07T13:47:01Z"></time>timespace and processes</h1>
  <section>
    <h2><time datetime="2023-05-07T14:10:19Z"></time>introduction</h2>
    <p>Let's talk about exploring time and interface engineering.</p>
    <p>
      What do I mean by <q>exploring time</q>? That's the hard one, I'll come
      back to it.
    </p>
    <p>
      What about <q>interface engineering</q>? That one is easier. In short, I
      mean a <q>responsive</q> interface. People call software
      <q>responsive</q> when it evidently reacts to stimuli. Physical things
      don't generally pause before indicating awareness of events that they can
      perceive. Imagine if you sliced into an apple and heard no sound until a
      few seconds had passed. Or that, after taking a bite of the apple, it
      turned gray, indicating that it wasn't ready to be eaten again. If you're
      making software that works this way in the year 2023, you may have room to
      improve your interface engineering. This is especially true in the context
      of a single web page where all data is available locally, which is the
      case for this discussion.
    </p>
    <p>
      Okay, so what is <q>exploring time</q>? For the past ten years, I have
      been increasingly interested in the mapping of time. I have begun to see
      it as a substrate that will inevitably underlie any humane virtual space.
      It goes without saying that JARVIS could present Tony Stark with an
      intelligible, dynamic timeline about any topic. There is no
      <em>technical</em> reason why we cannot do this now, yet we cannot. To my
      continued surprise, there are no well known software applications for
      creating or exploring dynamic timelines, and my searches have never even
      turned up obscure ones. This is a design problem that entails basic
      questions—still considered research questions!—about how we expect
      information spaces to work generally. For problems of this sort, there
      also is no reason to think that an AI will succeed where we have
      failed.<time datetime="2023-05-07T14:09:54Z"></time>
    </p>
  </section>
  <section>
    <h2><time datetime="2023-05-07T14:11:08Z"></time>a corpus of lifespans</h2>
    <p>
      My present assay into this space starts with people, specifically people's
      lifespans. I have 94 thousand records like this:
    </p>
    <pre>
  &lt;person who="Salome_Þorkelsdóttir" summary="Icelandic politician" born="1927-07-03"/&gt;
  &lt;person who="Janet_Leigh" summary="American actress" born="1927-07-06" died="2004-10-03"/&gt;
  &lt;person who="Red_Kelly" summary="Canadian ice hockey player" born="1927-07-09" died="2019"/&gt;
  &lt;person who="Simone_Veil" summary="French lawyer and politician" born="1927-07-13" died="2017"/&gt;
  &lt;person who="Kurt_Masur" summary="German conductor" born="1927-07-18" died="2015"/&gt;
  &lt;person who="Zara_Mints" summary="Slavic literary scientist active in the University of Tartu" born="1927-07-24"/&gt;
  &lt;person who="John_Ashbery" summary="American poet and critic" born="1927-07-28" died="2017"/&gt;
  &lt;person who="Andreas_Dückstein" summary="Austrian chess player" born="1927-08-02"/&gt;
  &lt;person who="Dušan_Čkrebić" summary="Serbian politician, President 1984–86" born="1927-08-07"/&gt;
  &lt;person who="Giuseppe_Moioli" summary="Italian rower" born="1927-08-08"/&gt;
    </pre>
    <p>
      Where do they come from? Is a question that I cover quite exhaustingly
      elsewhere. Exactly <em>how</em> we got the data is not highly important
      here.
    </p>
    <p>
      However, it does matter <em>what</em> we have. For the moment I'll just
      say that
    </p>
    <ul>
      <li>
        the records are assembled from the <q>Births</q> and
        <q>Deaths</q> sections of
        <a href="https://en.wikipedia.org/wiki/List_of_years"
          >English Wikipedia's timeline pages</a
        >, which extend from antiquity to
        <time datetime="2023">the present year</time>.
      </li>
      <li>Those sections are moving, I don't want to talk about it.</li>
      <li>
        Wikipedia has a notion of
        <q
          ><a href="https://en.wikipedia.org/wiki/Wikipedia:Notability"
            >notability</a
          ></q
        >, which I would not endeavor to explain but which would usually be the
        answer to your question Why is so-and-so not listed?
      </li>
    </ul>
    <p>
      In summary, we have a corpus of records about people, indicating people's
      names, when they lived, and a brief phrase indicating their notability.
      The entries are roughly coextensive with recorded history and greatly
      concentrated in recent times. Among the people you've heard of, it
      includes most of the dead ones, and some of the living ones.<time
        datetime="2023-05-07T14:49:35Z"
      ></time>
    </p>
  </section>
  <section>
    <h2><time datetime="2023-05-07T15:07:27Z"></time>looking around</h2>
    <p>How do we think about this problem?</p>
    <p>I'm considering two main ways</p>
    <ul>
      <li>maps and atlases</li>
      <li>physical space</li>
    </ul>
    <p>
      Regarding the map metaphor. I don't mean paper maps exactly, but dynamic
      maps, which are relatively mature on web platforms.
    </p>
    <p>
      I would also like to extend this to a collection of maps, i.e. an atlas. I
      don't know of an existing analog for a collection of dynamic maps that
      would be akin to an atlas.
    </p>
    <p>
      The other guiding idea is that of physical space. How would we explore
      historical time in virtual space? Maps and atlases, of course, exist in
      physical space, but even virtual (computer-based) maps exist only in
      flatland. So in some sense, this exploration could inform how we think
      about virtual maps.
    </p>
    <p>
      In physical space, we have a <em>viewpoint</em>. The concept of a
      viewpoint, and the attendant concept of a <em>gaze</em> are ones that
      exist in augmented reality (AR) environments but which do not exist as
      such in the web platform.
    </p>
    <p>
      Can we connect the notions of viewpoint and gaze to anything that does
      exist in the web?
    </p>
    <p>
      A viewpoint is like a <em>location</em>. The location is a first-class
      concept in the web, referring to the URL of the current document. We can
      make the comparison that a location determines what is potentially within
      view. Unlike physical viewpoints, there is no formal relation between
      different web addresses as to what they might present. But from the
      perspective of a single location, we could say that you can
      <q>go there</q> and <q>look around</q>.
    </p>
    <p>
      It's less clear how to interpret the notion of gaze. If we understand a
      viewpoint as being <q>the place where you're standing</q>, the gaze is
      akin to the direction you're looking. Likewise, in a web document, you
      cannot usually see everything at once. In this regard, your gaze could be
      at least partly captured by your scroll position within the document. This
      is unsatisfactory in several ways. Scroll position doesn't
      <em>mean</em> anything and is not consistent across different viewports.
      An <em>anchor</em> would be a better candidate in that it is usually
      meaningful, it can be interpreted consistently in different viewers and
      (unlike scroll position) it can be communicated formally as part of the
      URL. Not all documents have anchors, and they represent more
      coarse-grained locations than scroll positions. Neither of these
      approaches is quite satisfactory, given that web applications can have all
      sorts of internal state that affects what you see, which cannot be
      captured and restored in any uniform way.
    </p>
    <p>
      It's worth asking these questions about the web platform. I would rather
      consider approaches that can apply to web environments generally than to
      some specific kind of thing. But the web still uses a primarily
      document-based world view. Applications that wish to take a more
      human-centric view must
      <q>start outside of themselves,</q> so to speak, and are currently on
      their own in doing so.<time datetime="2023-05-07T16:26:36Z"></time>
    </p>
  </section>
  <section>
    <h2><time datetime="2023-05-07T16:34:22Z"></time>status</h2>
    <p>
      What I have so far is a function that will present the first 20 records
      matching a given term in either direction (forwards or backwards) from
      some starting date. The results are arrayed according to birth dates along
      some (currently fixed) time perspective. When death date is also known,
      the <q>span</q> indicates this. When you select any of the items, the
      Wikipedia article about that person is inserted in an inline frame.<time
        datetime="2023-05-07T16:40:20Z"
      ></time>
    </p>
    <p>
      The interface is currently structured as a dialog. I am presented with an
      input, where I can enter a term, whereupon I am presented with results,
      followed by a new input, where I can repeat the process. It's basically a
      REPL with one operation.
    </p>
  </section>
  <section>
    <h2><time datetime="2023-05-07T16:45:10Z"></time>adventure</h2>
    <p>What's next?</p>
    <p>More programming, I'm afraid.</p>
    <p>
      But before I can answer the question of what to <em>program</em> next, I
      need to know at least something about what I want to see and do.
    </p>
    <p>
      The dialog approach is <q>monotonic</q>: it only adds and never changes or
      takes away what is already past. Like time, it goes forward only.
    </p>
    <p>
      Dialogs have other interesting properties that I may talk about later.
    </p>
    <p>
      But we're talking about <em>exploring</em>. Is monotonicity at odds with
      the goal of exploring?
    </p>
    <p>
      Not necessarily. Think about so-called role-playing games, going back to
      <a href="https://en.wikipedia.org/wiki/Colossal_Cave_Adventure"
        >Collosal Cave Adventure</a
      >. These games basically take the form of:
    </p>
    <figure>
      <p>
        <b>GAME</b>: You're in a dark passageway. Your lamp shows two tunnels,
        both alike.
      </p>
      <p><b>YOU</b>: Mark the left passageway.</p>
      <p>
        <b>GAME</b>: You're in a dark passageway. Your lamp shows two tunnels,
        one with a mark.
      </p>
    </figure>
    <p>And so on.</p>
    <p>
      Sure, that's not what we want. But does it tell us anything about what we
      mean by <q>exploring</q>?
    </p>
    <p>
      Yes, I think it does. It spells out what happens in formal worlds.
      Although modern virtual worlds may look like they transcend the apparently
      simple nature of an Adventure-style game, they are in essence algebraic
      reductions of a sequence of steps over some initial state.
    </p>
    <p>
      So suppose that we have an Adventure-style game where we can explore (an
      encyclopædic reduction of) human history. What would that look like?
    </p>
    <figure>
      <p>
        <b>GAME</b>: You're in a dark place called 2023. There are two
        passageways: backwards, which is a chaotic blur, and forwards, which is
        inscrutable. You cannot stay in your present location.
      </p>
      <p><b>YOU</b>: Go to 1945</p>
      <p>
        <b>GAME</b>: Wow, okay, that's a choice. You pass through 78 years to
        arrive at 1945. Looking back towards your former location, 2023, it now
        appears smaller and less distinct.
      </p>
      <p>
        You hear an ominous rumbling sound, either of something that just
        happened, or of something that's about to happen. There's a war on. FDR
        is president of the USA (again). This is the year that Hitler died. I
        mean, a lot of other things happened, what are you after?
      </p>
    </figure>
    <p>The details, of course could vary.</p>
    <p>
      The point here is that the two forms (visual and narrative) can be seen as
      fungible. The spatial view of this could be seen as a depiction of the
      narrative. Conversely, we could see a narrative view as a description of
      the visual representation. At least, a human could convert between the two
      forms, and even AI systems could now produce much entertainment by
      attempting this kind of roundtripping.
    </p>
    <p>
      The narrative and the visual forms are the ones that people would see. The
      web philosophy would be that <em>both</em> forms should be available.
    </p>
    <p>
      My instinct as a programmer is that, at least in the case of the machine's
      responses <em>neither</em> of those are the <q>real</q> form: that both of
      them are just projections of a formal representation that would not
      usually be seen by the human.
    </p>
    <p>
      The design task, then, not surprisingly, falls back to the familiar one,
      of devising a vocabulary of states and operations for exploring
      chronologies.<time datetime="2023-05-07T18:03:39Z"></time>
    </p>
  </section>
  <section>
    <h2><time datetime="2023-05-07T19:28:27Z"></time>data notes</h2>
    <p>
      <time datetime="2023-05-07T19:28:12Z"></time>I just noticed that these
      articles can redirect. For example,
      <a
        href="https://en.wikipedia.org/w/index.php?title=Lessie_Brown&redirect=no"
        >Lessie Brown</a
      >
      redirects to
      <a href="https://en.wikipedia.org/wiki/List_of_American_supercentenarians"
        >List of American supercentenarians</a
      >
      (of whom incidentally 97/100 are female).
    </p>
    <p>
      <time datetime="2023-05-07T19:32:21Z"></time>I also note that many entries
      are missing death dates even though Wikipedia <q>knows</q> when the person
      died. For example, the article for
      <a href="https://en.wikipedia.org/wiki/George_Bellas_Greenough"
        >George Bellas</a
      >
      says straightaway <q>18 January 1778 – 2 April 1855</q>; but his death
      date is missing here because the
      <a href="https://en.wikipedia.org/wiki/1855#Deaths"
        ><q>Deaths</q> section of the article about 1855</a
      >
      doesn't include him. The basic technical problem here is that I don't want
      to depend on 100K more articles, in addition to the roughly 3K that I
      already depend on. Of course, by getting each person's article, I could
      get other facts too (such as a selected portrait). But I think that such
      things would make more sense to ask of a knowledge graph outright, in
      which case I should be able to batch many together, i.e. When did these
      people die?
    </p>
  </section>
  <section>
    <h2><time datetime="2023-05-08T03:39:04Z"></time>playing</h2>
    <p>
      After playing with this a bit more, but mostly staring at it, I want to
      highlight a few things.
    </p>
    <p>
      I refuse to go any further without a process model. And that process model
      will be based on a low-power philosophy. Make the user keep pushing the
      button to get more, for instance, don't run things to exhaustion by
      default. This fits well with what we get from indexed DB, which in JS
      terms is an async iterable.
    </p>
    <p>
      Marginal space should be affordable. Just give the whole row to the thing
    </p>
    <p>
      OR, half the row, and do the backwards-from-here, forwards-from-here lists
      on separate sides. Those proceed as two independent processes.
    </p>
    <p>
      I like that idea but it does raise the consequence of the
      <q>exact center</q> of the display. There is no <q>point in time</q>.
    </p>
    <p>
      The list can <q>collapse</q> horizontally by compressing the time scale.
    </p>
    <p>
      Several lists should be visible simultaneously, by stacking vertically.
      Layers behind the current one might not (or might) be affordable as such
      (see eg point about letting things take up the whole row), but their shape
      can still be referenced and correlated with the present view (by way of
      their sharing a timeline parent context).
    </p>
    <p>
      When items that you found extend past the current time extent, well. What
      of that? I said that the extent would be defined passively. I have to add
      some margin on the right (inline end) so that start-aligned text would be
      in view. Remember, the viewport boundaries are not themselves indicating
      anything. That is, the timeline's extent need not contend with other
      constraints. This is a hard idea for me to shake somehow because the
      timeline most definitely <em>has</em> definite boundaries when viewed on a
      screen, and it is the basis for all calculations (as opposed to real-world
      metrics, which we have little hope of honoring on screens).
    </p>
    <p>The <q>rendering</q> model at the moment is static.</p>
    <p>
      Anyway, as this moment I want to see how to make this work with a
      user-facing cost model. Currently I just say "up to 20", and I can't get
      less or more.<time datetime="2023-05-08T03:56:00Z"></time>
    </p>
    <p>
      <time datetime="2023-05-08T03:58:50Z"></time>When you rip out the
      <q>max</q>, the whole rendering model has to change. You're not emitting
      just a single thing. But it's basically a monotonic sequence. There's no
      need to suppose that we'll try to go back and change history. The cursor
      (async iterator) gave us what it gave us. In other words, a log printing
      mode, where we just hold a reference to the target container. The target
      container and the mapping to element function can both be dynamic. Is this
      also true of the stream source? Sure though I don't have an immediate
      usage for that in mind.
    </p>
    <p>
      Anyway adding a word index should give me a kind of <q>embedded</q> way of
      perusing this. There's nothing timeline-specific about the above. The only
      time-related thing is that, on the element itself, we need the style
      properties set that tell its timeline affinity.
    </p>
    <p>
      <time datetime="2023-05-08T04:04:51Z"></time>Also TODO: include month and
      day in positioning when available. I believe this can be done compatibly
      with the current approach using the decimal part of the year value.
    </p>
    <p>
      <time datetime="2023-05-08T04:07:51Z"></time>I keep pondering about this
      but what's the problem? I think this fits exactly with the dialog-RIPL
      approach. Each stage in the conversation contains an input, which
      interpreted as an operation on the current state, and which operation may
      be associated with some output, subject to the number of times the person
      is ready to turn the crank. Those are (say for now) one-way processes.
      There's no need to worry about <q>leaving them running</q>, because they
      are stepwise generators that require human input. So they're cheap,
      potentially portable, and you can have as many as you can make room for
      without overcrowding any pool.
    </p>
    <p>
      <time datetime="2023-05-08T04:14:54Z"></time>I think what I'm trying to
      say is that each listing should be the result of a describable traversal.
      But even with the word index, a partial text match can't be done in terms
      of indexed DB alone.
    </p>
    <p>
      <time datetime="2023-05-08T04:17:31Z"></time>I also have to come back to
      this point about the viewpoint center. I mean, what should it be?
      Shouldn't it also be a computed result—a direct function of the bounds,
      which are given by the constituents? Yes, as Paul Manafort said, that's
      our position.
    </p>
    <p>
      <time datetime="2023-05-08T04:24:03Z"></time>Hmm, this means that all
      terms should be affordable, first names, last names, and every word in the
      summary. What happens when you touch one? You would have to
      <q>afford</q> it many times to know how fruitful it is. And indeed, to
      give an indication of how fruitful it might be (<q
        >how many things link to it</q
      >
      sort of stuff), that would complicate things a lot, requiring access to
      (and use of) an index during rendering. It would use a lot more power,
      unconditionally, for something you might not know about, care about, or
      think helpful.
    </p>
    <p>
      <time datetime="2023-05-08T04:35:08Z"></time>I mean you could also search
      from the outside in. You don't have to reference any specific points in
      time for that, just say from the start or end of the set.
    </p>
    <p>
      <time datetime="2023-05-08T04:39:42Z"></time>What if I wanted to know
      which keys in a multi index have the most entries?
    </p>
  </section>
</article>
