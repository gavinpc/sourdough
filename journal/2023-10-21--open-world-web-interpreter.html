<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="initial-scale=1" />
    <link rel="icon" href="data:," />
    <title>open world web interpreter, take 3</title>
    <style>
      html {
        background: hsl(190deg, 10%, 10%);
        color: hsl(30deg, 10%, 70%);
        font-size: 18pt;
      }
      article {
        inline-size: 65ch;
        max-inline-size: 100%;
        margin-inline: auto;
      }
      figcaption {
        font-style: italic;
      }
      a {
        color: inherit;
        font-weight: bold;
        text-decoration-color: LinkText;
        text-decoration-thickness: 2pt;
      }
      pre {
        white-space: pre-wrap;
      }
    </style>
  </head>
  <body>
    <main>
      <h1>
        <time datetime="2023-10-22T03:03:57Z"></time> open world web
        interpreter, take 3
      </h1>
      <p>
        <time datetime="2023-10-22T03:04:14Z"></time>I'm making another run at
        the world interpreter. The previous one was pretty good. I want to keep
        the basic approach of an RDF dataset under interpretation. Here are the
        main things I want to improve upon:
      </p>
      <ol>
        <li>don't need to RDF-ify everything in the world</li>
        <li>define and order interpreters on triple patterns</li>
        <li>portability / generational persistence</li>
      </ol>
      <p><time datetime="2023-10-22T03:21:07Z"></time>Where to begin?</p>
      <section>
        <h2><time datetime="2023-10-22T03:24:36Z"></time>the open world</h2>
        <p>
          <time datetime="2023-10-22T03:24:47Z"></time>Let's start with the
          <q>open world.</q> What is it?
        </p>
        <p>
          <time datetime="2023-10-22T03:28:14Z"></time>The open world, for our
          purposes here, is the situation we face in the browser: there's a
          human or two out there, who can run any plugin, load any script,
          delete half the page, bust the cache, go underground, have ideas. We,
          in our little system, are not in charge of the humans; we're in their
          service.
        </p>
        <p>
          <time datetime="2023-10-22T03:47:04Z"></time>A web document is an open
          world. Very little is <em>owned</em> by anything. We'll need to claim
          a little space to make this system, and we may need to
          <q>close the world</q> (that is, assume some falsities) to make
          certain things work.
        </p>
        <p>
          <time datetime="2023-10-22T03:38:07Z"></time>But in general we must
          avoid <em>making assumptions about what we don't know</em>.
        </p>
      </section>
      <section>
        <h2><time datetime="2023-10-22T03:58:49Z"></time>the interface</h2>
        <p>
          <time datetime="2023-10-22T03:59:02Z"></time>The previous interpreter
          defined four operations:
        </p>
        <dl>
          <div>
            <dt>register (driver)</dt>
            <dd>add an interpreter of a type</dd>
          </div>
          <div>
            <dt>unregister (driver)</dt>
            <dd>
              remove an interpreter and undo its interpretations where possible
            </dd>
          </div>
          <div>
            <dt>assert (triple)</dt>
            <dd>interpret a fact as true, if you can</dd>
          </div>
          <div>
            <dt>retract (triple)</dt>
            <dd>
              withdraw the effects of a fact's interpretation, where possible
            </dd>
          </div>
        </dl>
        <p>
          <time datetime="2023-10-22T04:11:37Z"></time>I believe this is a
          minimal design, modulo some room to define <q>interpreter.</q>
        </p>
        <p>
          <time datetime="2023-10-22T04:13:51Z"></time>The minimality was not
          ideal in all cases. For example, it's common that you want to
          <em>modify</em> a fact. This was always done as a retract+assert, but
          sometimes a modification can be done more directly. Interpreters did
          not have a chance to handle this case specially.
        </p>
        <p>
          <time datetime="2023-10-22T04:15:36Z"></time>This design also seems to
          require that everything be boiled down to triples before it can be
          interpreted. Is that so? Is one of the questions I mean to answer
          here.
        </p>
      </section>
      <section>
        <h2><time datetime="2023-10-22T04:21:26Z"></time>world composition</h2>
        <p>
          <time datetime="2023-10-22T04:24:02Z"></time>It's often the case that
          you want to make a <q>new world</q> that is like another world but
          with some small additions. See the paper on PIE by Goldstien and
          Bobrow, I think
          <a href="https://dl.acm.org/doi/10.1145/800227.806895">this one</a>.
        </p>
        <p>
          <time datetime="2023-10-22T04:21:51Z"></time>The previous interpreter
          did not have a notion of composite worlds <i>per se</i>, but some
          composition was implemented in userland using union graphs. That is,
          you could say (in a graph) that <var>C</var> is a supergraph of
          <var>B</var>, and this would ensure that everything that was true in
          <var>B</var> was also true in <var>C</var>.
        </p>
        <p>
          <time datetime="2023-10-22T04:35:09Z"></time>I was happy for such
          things to be self-hosting, but I was not happy with the
          implementation, which was very space inefficient. I would be better
          satisfied with something like JavaScript's prototype chain lookup,
          which resembles the network model for <q>contexts</q> used in PIE.
        </p>
      </section>
      <section>
        <h2><time datetime="2023-10-22T04:37:11Z"></time>record collections</h2>
        <p>
          <time datetime="2023-10-22T04:40:11Z"></time>When I say that
          everything shouldn't have to be RDF-ified in order to participate in
          the world, collections of uniformly-structured objects are a good
          example of why. They are already stored as efficiently as the host
          provides. But for them to <q>participate in the world</q> as far as
          the interpreter is concerned, they must be transmogrified into
          triples. At a minimum, this results in substantial duplication and
          string I/O.
        </p>
        <p>
          <time datetime="2023-10-22T04:47:20Z"></time>Yet, if we have enough
          knowledge to perform such a triplification ahead of time, we could
          presumably do it on-demand. For example, queries involving only unused
          predicates can be skipped in O(1).
        </p>
        <p>
          <time datetime="2023-10-22T04:53:54Z"></time>In the likely event that
          we want to access record-like things as records, we can have recourse
          to the <q>things themselves</q> without needing to fabricate such a
          collection from the graph (which was also implemented in userland of
          the v2 interpreter).
        </p>
      </section>
      <section>
        <h2><time datetime="2023-10-22T14:28:29Z"></time>userland notation</h2>
        <p>
          <time datetime="2023-10-22T14:28:36Z"></time>One critical question
          that was not addressed in the v2 interpreter is that of creating
          descriptions from userspace. That project instead focused on an LSP
          plugin that used the editor as the main source of interactive input.
        </p>
        <p>
          <time datetime="2023-10-22T14:41:06Z"></time>The v1 interpreter, which
          was not based on RDF proper and did not require qualified names, used
          a small
          <a
            href="https://gitlab.com/gavinpc/ucan/-/tree/master/packages/rdf-expressions?ref_type=heads"
            >expression reader</a
          >
          to transform gelatinous JavaScript expressions (such as
          <code>Bob.listensTo.Alice</code>) into RDF-like statements. This
          approach was useful for experimentation (and a
          <a
            href="https://gitlab.com/gavinpc/js-playgrounds/-/blob/master/proxy-magic-part-one.js"
            >fun</a
          >
          <a
            href="https://gitlab.com/gavinpc/js-playgrounds/-/blob/master/proxy-magic-part-two.js"
            >hack</a
          >), but it is not suitable for use in this phase since it does not
          provide for a robust mapping to a global graph.
        </p>
        <p>
          <time datetime="2023-10-22T14:59:24Z"></time>The v2 interpreter
          introduced a <q>description notation</q> that mapped JavaScript
          expressions into RDF triples in a well-defined way. Following is an
          extract from an
          <a
            href="https://gitlab.com/def.codes/prealpha/-/blob/main/packages/space/src/hogwarts.js?ref_type=heads"
            >example program</a
          >:
        </p>
        <figure>
          <figcaption>example of description notation</figcaption>
          <pre>
export const Hogwarts = a.owl.Thing({ label: "Hogwarts" });

export const houses = {
  Gryffindor: a.hogwarts.House(),
  Hufflepuff: a.hogwarts.House(),
  Ravenclaw: a.hogwarts.House(),
  Slytherin: a.hogwarts.House(),
};
const { Gryffindor, Hufflepuff, Ravenclaw, Slytherin } = houses;

export const students = {
  "Draco Malfoy": a.hogwarts.Student({ sortedTo: Slytherin }),
  "Pansy Parkinson": a.hogwarts.Student({ sortedTo: Slytherin }),
  "Harry Potter": a.hogwarts.Student({ sortedTo: Gryffindor }),
  // etc...
};

export const simulation = a.d3force.Simulation(
  space.simulation[def.Symbol.id],
  {
    alpha: a.rstream.Subscription(),
    forces: [
      a.d3force.ManyBodyForce({ strength: -1000 }),
      a.d3force.RadialForce({ radius: 200, strength: 0.7 }),
      a.d3force.XForce({ strength: 0.1 }),
      a.d3force.YForce({ strength: 0.1 }),
      ...space.links.map((_, i) =&gt;
        a.d3force.LinkForce(`/modules/src/hogwarts/link_force/${i}`, {
          linkId: (_) =&gt; _.id,
          links: _.edges,
          strength: 0.1,
          distance: 300,
        })
      ),
    ],
  }
);
        </pre
          >
        </figure>
        <p>
          <time datetime="2023-10-22T15:15:43Z"></time>Critically, the notation
          lets you freely <em>glue together</em> descriptions of things from
          different domains. Interpretations of those connections can be defined
          and redefined at any time.
        </p>
        <p>
          <time datetime="2023-10-22T15:22:40Z"></time>Description notation is
          basically sound and I want to keep it largely as-is. However, it is a
          JavaScript interface, not a userspace one. Like the v1 notation, It
          could be used to support basic in-browser experimentation (via dynamic
          eval), but only as a stopgap.
        </p>
        <section>
          <h3>
            <time datetime="2023-10-22T15:37:40Z"></time>proposal: RDFa Lite as
            interface
          </h3>
          <p>
            <time datetime="2023-10-22T15:37:45Z"></time
            ><a href="https://www.w3.org/TR/rdfa-lite/">RDFa Lite</a> is a W3C
            recommendation that defines a small vocabulary for notating RDF
            graphs in DOM documents (such as HTML). It is a subset of the more
            demanding
            <a href="https://www.w3.org/TR/rdfa-core/">RDFa specification</a>
            that covers the most common needs.
          </p>
          <p>
            <time datetime="2023-10-22T15:55:16Z"></time>In a sense, the main
            reason for using RDF in this project is <em>interop</em>. RDF is the
            only official choice if we want to decouple consumers from producers
            of descriptions of arbitrary things, in other words, make
            <q>tellers</q> independent from <q>askers.</q>
          </p>
          <p>
            <time datetime="2023-10-22T15:58:32Z"></time>Observe that JavaScript
            need not apply. If the teller provides RDF and the asker expects
            RDF, then there is never a need to answer the question, What does
            the JavaScript look like? Sure, JavaScript may be used in the
            mediation, but it's not essential.
          </p>
          <p>
            <time datetime="2023-10-22T16:02:21Z"></time>I said up front that I
            wanted to avoid having to RDF-ify all the things, and that's true.
            For things that are computed or notated <em>in JavaScript</em> to
            begin with, I don't want to have marshal through RDF if I can avoid
            it.
          </p>
          <p>
            <time datetime="2023-10-22T16:04:03Z"></time>But for expressions
            that originate in userland to begin with, RDF is the most direct way
            to get to the knowledge base. And again, such expressions have no
            counterpart in earlier systems.
          </p>
          <p>
            <time datetime="2023-10-22T16:12:22Z"></time>The proposal, then, is
            to define a <em>live</em> reader of RDFa Lite that submits the read
            descriptions to a knowledge graph, where <q>live</q> means that it
            not only reviews the given DOM tree but also listens for changes.
            Nothing about this proposal is specific to this system: anyone who
            is interested in reading RDFa might well be interested in such a
            thing. (Although I doubt whether anyone is producing
            <em>or</em> consuming RDFa dynamically). This proposal then has the
            advantage of not introducing any new concepts, modulo the fact that
            there is yet no official interface on the receiving end for writing
            to RDF Datasets (no, I don't count
            <a
              href="https://web.archive.org/web/20230127212101/https://rdf.js.org/dataset-spec/"
              >this thing</a
            >, which does the courtesy of warning you against using it).
          </p>
          <p>
            <time datetime="2023-10-22T16:41:25Z"></time>Actually I think I will
            need a few things beyond RDFa Lite:
          </p>
          <ul>
            <li>
              the <code>datatype</code> and <code>content</code> attributes,
              which RDFa proper defines, and
            </li>
            <li>
              a <code>graph</code> attribute, which RDFa does not define, but
              something like
              <a href="https://buzzword.org.uk/2009/rdfa4/spec-20090120.html"
                >this</a
              >
              will do
            </li>
          </ul>
          <p>
            <time datetime="2023-10-22T16:49:50Z"></time>Even without a live
            reader, this approach will allow me to start
            <em>making things without always writing JavaScript</em>, which has
            been a scourge. At the most basic level, this will allow me to
            create new things by describing them in markup, provided that the
            necessary interpreters exist.
          </p>
          <figure>
            <figcaption>example construction in RDFa</figcaption>
            <div class="rdfa-example">
              <div vocab="https://example.com/mygames">
                <div typeof="PacMan">PacMan goes here</div>
                <div typeof="Boggle">Boggle goes here</div>
              </div>
            </div>
            <style>
              .rdfa-example [vocab]::before,
              .rdfa-example [typeof]::before {
                font-family: monospace;
                background: CanvasText;
                color: Canvas;
                padding-inline: 1ch;
              }
              .rdfa-example [vocab]::before {
                content: "vocab: " attr(vocab);
              }
              .rdfa-example [typeof]::before {
                content: "typeof: " attr(typeof);
              }
            </style>
          </figure>
          <p>
            <time datetime="2023-10-22T16:59:10Z"></time>The interpreters
            themselves are another matter.
          </p>
        </section>
      </section>
      <section>
        <h2>
          <time datetime="2023-10-24T22:09:06Z"></time>the things formerly known
          as graphs
        </h2>
        <p>
          <time datetime="2023-10-24T21:35:22Z"></time>The v2 system imported
          the idea of an RDF dataset, which is a collection of RDF graphs. It
          also introduced the concept of a <q>world,</q> meaning an RDF graph
          under interpretation.
        </p>
        <p>
          <time datetime="2023-10-24T22:09:50Z"></time>In the new system, an RDF
          view will no longer dominate. So I don't want to carry over the term
          <q>graph</q> for collections of data.
        </p>
        <p>
          <time datetime="2023-10-24T21:46:22Z"></time>That said, I
          <em>do</em> want the structure of the knowledge base to be isomorphic
          with an RDF dataset at the graph level. That is, there should be a 1:1
          mapping between graphs and some-other-thing.
        </p>
        <p>
          <time datetime="2023-10-24T22:12:05Z"></time>In mathematics, a
          collection of statements in a formal language is called a
          <dfn>theory.</dfn> In this sense, every RDF graph is a theory.
        </p>
        <p>
          <time datetime="2023-10-24T22:20:45Z"></time>Likewise, an
          interpretation of a theory is called a <dfn>model.</dfn> In logical
          system, this interpretation usually consists of deciding whether the
          statements are true or false, hopefully in a way that is consistent
          with the axioms.
        </p>
        <p>
          <time datetime="2023-10-24T22:23:27Z"></time>This is not
          <em>quite</em> what I have meant in this system by
          <q>interpretation.</q> v2 basically accepted all statements as true.
          In that sense, every graph was also trivially a model. The job of the
          v2 interpreter was not to, say, find inconsistencies or prove the
          correctness of statements in a logical sense. Rather, it was
          predominantly to <em>make</em> the statements true by configuring the
          world around them. That is still the principal goal.
        </p>
        <p>
          <time datetime="2023-10-24T22:26:19Z"></time>However, I like the
          theory-model formulation. <q>Theory</q> emphasizes the fact that the
          knowledge base contains <em>descriptions</em>, not
          <em>implementations</em>; while <em>model</em> suggests a working
          thing. While the system's disposition will still be to accept facts as
          true (we still <strong>assert</strong> and <strong>retract</strong>),
          it is also possible to introduce contradictions under some entailment
          regimes, in which case we cannot rightly honor all claims as true at
          once. Therefore interpretations would do well to hold back when such
          conflicts are identified.
        </p>
        <p><time datetime="2023-10-24T22:33:39Z"></time>In conclusion,</p>
        <ul>
          <li>graph → <strong>theory</strong></li>
          <li>world → <strong>model</strong></li>
        </ul>
      </section>
      <section>
        <h2>
          <time datetime="2023-10-25T03:18:15Z"></time>the shapes of theories
        </h2>
        <p>
          <time datetime="2023-10-25T03:22:02Z"></time>Despite the new
          terminology, a theory is still equivalent to an RDF graph. This is
          what gives the thing its semantics.
        </p>
        <p>
          <time datetime="2023-10-25T03:26:54Z"></time>So what's different? The
          way that it is structured and interfaced in the host. As I said up
          front, things should not need to be <strong>provided</strong> as
          triples, and because of the description API, they often weren't. But
          they also should not need to be <em>stored</em> as triples.
        </p>
        <p>
          <time datetime="2023-10-25T03:34:44Z"></time>What
          <em>is</em> necessary, then?
        </p>
        <p>
          <time datetime="2023-10-25T04:08:47Z"></time>We need to know when a
          theory describes a state of affairs that we can make true.
        </p>
        <section>
          <h3><time datetime="2023-10-25T04:18:19Z"></time>construction</h3>
          <p>
            <time datetime="2023-10-25T04:11:29Z"></time>For instance (pun
            intended), we need to know when a theory describes a thing that we
            can construct.
          </p>
          <p>
            <time datetime="2023-10-25T04:19:08Z"></time>This is not the same as
            saying that something of a recognized type has been existentially
            quantified, which is how v2 thinks of it. But this turned out to be
            a source of some boilerplate that I would like to avoid this time
            around. Let me explain.
          </p>
          <p>
            <time datetime="2023-10-25T04:19:57Z"></time>v2 had a notion of
            <em>drivers</em>. A driver is a collection of interpreters for a
            particular (RDF) type. In practice, it's pretty close to a
            JavaScript class. If we would say,
            <q>There exists a force simulation</q>, then the driver for force
            simulations would be called upon to construct one.
          </p>
          <p>
            <time datetime="2023-10-25T04:27:56Z"></time>Sometimes, we needed
            more information in order to construct something. In JavaScript
            terms, these would be like required constructor arguments. For
            example, we can't construct a buffer if we don't know its size. But
            in the world where everything's a triple, we might get the
            <q>there exists</q> fact before hearing a separate fact telling us
            what its size is. Drivers handled cases like that by not allowing
            the resource to be <em>construed</em> as anything until all of the
            necessary facts had come in. (More about construals soon.) This led
            to boilerplate in cases where one or more properties was necessary
            before a construction could actually occur.
          </p>
          <p>
            <time datetime="2023-10-25T04:35:24Z"></time>This time, I would
            rather be able to talk directly about the conditions that must be
            met for a construction to proceed. It is, after all, a kind of
            pattern match: the same kind that is needed for inference rules.
          </p>
          <p>
            <time datetime="2023-10-25T04:42:08Z"></time>It's not strictly
            necessary that every thing in the world have a corresponding host
            object as its interpretation. Indeed, and somewhat maddeningly, I
            refused to provide direct access to these objects (which pretty much
            always <em>did</em> exist) through any kind of API. The only way to
            get at the <q>underlying</q> host object was through another driver.
            I think this was ultimately helpful in making sure that I stuck with
            the system rather than programming around it.
          </p>
          <section>
            <h4>
              <time datetime="2023-10-25T18:31:40Z"></time>example: making a
              function
            </h4>
            <p>
              <time datetime="2023-10-25T18:32:15Z"></time>So much depends upon
              functions.
            </p>
            <p>
              <time datetime="2023-10-25T18:44:34Z"></time>Functions can come
              about in many ways:
            </p>
            <ol>
              <li>evaluate some given code in a given language</li>
              <li>import from some module</li>
              <li>
                identify some well-known operation, such as
                <b>trigonometric sine</b> or
                <b>logical xor</b>
              </li>
              <li>
                compose two other functions together (like,
                <code>f(g(x))</code>)
              </li>
              <li>the result of evaluating another function</li>
              <li>computed to fit some given inputs and outputs</li>
            </ol>
            <p>
              <time datetime="2023-10-25T19:03:24Z"></time>All of these could be
              expressed as arguments to a constructor. JavaScript itself
              provides several function constructors that accept code, as in
              item 1:
            </p>
            <pre>new Function('x', 'y','return x + y')</pre>
            <p>
              <time datetime="2023-10-25T19:16:18Z"></time>What's important here
              is modeling how we <em>think</em> about functions and where they
              come from. The above list is not exhaustive. and there is much
              overlap. Indeed, when we consider how we might write these out as
              <q>theories,</q> it becomes clear that <em>all</em> of the cases
              can apply to a particular function, many times over.
            </p>
            <pre>
# Just say that it's a function and leave the rest to me
ex:Hypotenuse a js:Function.

# Identify with a known operation
ex:Hypotenuse owl:sameAs geom:HypotenuseFunction.

# Give the code directly
ex:Hypotenuse js:hasSource "(x, y) => Math.sqrt(x*x + y*y)".

# Get it from some place using the host's module system
ex:Hypotenuse
  js:importedFrom &lt;https://example.com/geom.js&gt;;
  js:importName "hypotenuse".

# or perhaps module identifiers can be used as locators
ex:Hypotenuse owl:sameAs &lt;https://example.com/geom.js#hypotenuse&gt;.
&lt;https://example.com/geom.js&gt; a js:Module.

# Combine two other functions using a well-known composition pipeline operator
ex:Hypotenuse fn:compositionOf (math:SquareRoot math:SumOfSquares).

# Get the function by invoking other functions
ex:Hypotenuse fn:resultOf [
  a fn:Invocation;
  fn:of [js:hasSource "() => (x, y) => Math.sqrt(x*x + y*y)"];
  fn:args ()
].

# Provide data to induce a function
ex:Hypotenuse fn:fits [fn:input (3 4); fn:output 5]
ex:Hypotenuse fn:fits [fn:input (5 12); fn:output 13]
ex:Hypotenuse fn:fits [fn:input (8 5); fn:output 17]
</pre
            >
            <p>
              <time datetime="2023-10-25T19:49:58Z"></time>These are imaginary
              vocabularies, but plausible. The v2 system defined a
              <a
                href="https://gitlab.com/def.codes/prealpha/-/blob/main/packages/core/src/javascript/vocab.ts?ref_type=heads"
                >vocabulary</a
              >
              and
              <a
                href="https://gitlab.com/def.codes/prealpha/-/blob/main/packages/core/src/javascript/function.ts?ref_type=heads"
                >driver</a
              >
              for describing functions via JavaScript code (case #1 above), as
              well as
              <q
                ><a
                  href="https://gitlab.com/def.codes/prealpha/-/tree/main/packages/core/src/host/result?ref_type=heads"
                  >result</a
                ></q
              >
              concepts that capture the return value of arbitrary function
              invocations (case #5 above).
            </p>
          </section>
        </section>
      </section>
      <section>
        <h2>
          <time datetime="2023-10-26T02:30:19Z"></time>more rooms for
          improvement
        </h2>
        <p>
          <time datetime="2023-10-26T02:32:37Z"></time>The smallest kind of
          thing that we can say, and the smallest kind of thing that we can
          <em>interpret</em>, is a triple, which is in the form
          <q>subject predicate object.</q> The v2 interpreter dealt exclusively
          with triples. It distinguished two kinds of triples: those with a
          predicate of <code>rdf:type</code>, and everything else. In other
          words, <q>is a</q> statements and all other kinds of relations.
        </p>
        <p>
          <time datetime="2023-10-26T02:43:35Z"></time>This makes some sense,
          especially considering how the system evolved.
        </p>
        <section>
          <h3>
            <time datetime="2023-10-26T02:59:10Z"></time>is <q>is a</q> special?
          </h3>
          <p>
            <time datetime="2023-10-26T02:44:15Z"></time>But it's not quite
            right. An <q>is a</q> statement, like <q>Foo is a Stream</q>, would
            invoke the Stream driver's constructor; while a statement like
            <q>Foo listens to Bar</q> would have the Stream driver try to
            interpret <q>listens to</q> for <var>Foo</var>. So
            <q>is a</q> statements were treated like existential quantifiers,
            even though <em>all</em> statements in RDF existentially quantify
            their subjects. There is nothing special about
            <code>rdf:type</code> in that regard.
          </p>
        </section>
        <section>
          <h3>
            <time datetime="2023-10-26T02:59:53Z"></time>multiple types weren't
          </h3>
          <p>
            <time datetime="2023-10-26T02:48:57Z"></time>In theory, the system
            would allow things to have multiple types. But in practice I don't
            think this was ever used. Multiple <q>is a</q> statements about the
            same subject would construct completely independent things, which
            would independently interpret all of the other statements about that
            thing. In practice, some cooperation among a thing's multiple types
            is likely to be needed, particularly if the types are related in
            some way.
          </p>
        </section>
        <section>
          <h3><time datetime="2023-10-26T03:07:11Z"></time>analysis</h3>
          <p>
            <time datetime="2023-10-26T03:07:18Z"></time>The system's job is to
            map between reality and descriptions of hypothetical realities. The
            only sliver of reality within the system's control is the host
            environment where it lives. The idea of the system, its viability,
            is based on the assumption that we can in fact describe software in
            logical terms at some level.
          </p>
        </section>
      </section>
      <p>
        <time datetime="2023-10-26T02:30:26Z"></time>The v2 interpreter was
        eager about construction, but lazy about construal.
      </p>
    </main>
  </body>
</html>
