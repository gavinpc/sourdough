/*
from ~/prealpha/packages/clearinghouse/src/newish.js
Access: (0664/-rw-rw-r--)  Uid: ( 1000/   gavin)   Gid: ( 1000/   gavin)
Access: 2023-09-21 23:40:40.675870629 -0400
Modify: 2022-09-20 22:36:11.244619795 -0400
Change: 2022-09-20 22:36:11.244619795 -0400
 Birth: 2022-09-20 22:36:11.244619795 -0400

*/
//
// Prove that `new` doesn't prove anything.
//
// What _might_ it prove?
//
// That its non-failure result is an object?
//
// Yes, it does appear to promise that.
// [edit: no, it can return functions, as shown below]
//
// That two successive `new` calls to the same constructor yield distinct objects?
//
// I'm not saying `new` was a mistake.  And I'm not saying `class` fixes anything.
//
// I'm saying `class` could have saved us from `new`.  We need a way to
// distinguish class construction from other invocations because it involves the
// additional work of creating an object instance and an associated prototype.
//
// But it turns out that we don't much care, when reading the code, whether an
// invocation is to a constructor or any other kind of function.  JavaScript had
// to provide this at the call site because the distinction was not in the
// _type_ of thing being invoked (it was always a `function`), but in the _mode
// of invocation_.  _We_ don't need the information that a construction is
// occurring, but the runtime needs it from us.
//
// Of course, if a function “is” a constructor, then it will always be called with `new`.
//
// And if it's not, then it will never be.
//
// Thus, it would be more efficient to syntactically distinguish the functions
// from constructors _once_ (at the time they're being written), rather than at
// every time they're used.  _That_ would have been a justification for having
// `class` in the language: that we could finally give up on `new` as a bad job.
//
// Instead, we have `class`, which is a longer way of writing a simple
// constructor, and which not only requires the use of `new` at every call site,
// but actually _crashes_ when called without it!
//
//
// Perhaps the value of `new` is that it signals the _intent_ to instantiate an
// object of the target type.  So, don't return a replacement object from the constructor.
// Which I doubt is done very often.
//
//

function assert(message, ...claims) {
  const falsy = claims.filter(it => !it);
  if (falsy.length)
    throw new Error(
      `Failed ${falsy.length} of ${claims.length} for ${message}`
    );
}

// Nope.

// How worthless is `new`?

// Can we trick `new` into yielding a non-object?
//
// TL;DR: no

// But

// prettier-ignore
assert(
  "constructor ignores primitive return value",
  typeof new (function() { return 3; })() === "object"
);

// You can almost weasel out of it this way:

// prettier-ignore
assert(
  "constructor respects boxed primitive return value",
  typeof new (function() { return new String("boo"); })() === "object",
  new (function() { return new String("boo"); })() instanceof String
);

// This time, the return value is not ignored as it was before.
//
// But you get back a boxed primitive.
//
// And boxed primitives are about the most worthless thing in JavaScript.

// You can't Proxy out of it either.  In fact, `Proxy` throws an explicit error if you try.
// TypeError: 'construct' on proxy: trap returned non-object ('3')

const Thing = new Proxy(function () {}, {
  construct(...args) {
    return 3;
  },
});

let threw = null;
// prettier-ignore
try { new Thing(); } catch (error) { threw = error; }
assert("Proxy forbids primitive constructed value");

// Like `function`, `class` will silently ignore non-object return values from
// the constructor, which I found surprising.

// prettier-ignore
class A {constructor() { return 3; }}
console.log("new A() = ", new A());
assert("class ignores primitive constructed", new A() instanceof A);

// To my further surprise, you _can_ in the new syntax (I still call it that)
// return an alternate object from a constructor.

// prettier-ignore
class B { constructor() { return new A(); } }

assert("wow, class can delegate", new B() instanceof A);
console.log("new B() = ", new B());

// Enough monkeying around.  The result of a `new` is an object.

// But that's pretty much where the value of `new` ends.

// Maybe the most obvious question is, does `new` tell us that the result is, well, new?

// No.

const CC = { a: {}, b: {} };
// prettier-ignore
class C { constructor(ab) { return CC[ab]; } }
assert(
  "class constructor can return arbitrary object",
  new C("a") === new C("a"),
  new C("b") === new C("b"),
  new C("a") !== new C("b")
);

// Does `new` tell us that an object with the target's prototype was created?

// We just saw that it doesn't, even with `class` syntax.

// prettier-ignore
assert(
  "constructor respects object return value",
  Array.isArray(new (function() { return []; })())
);
// prettier-ignore
class Foo { constructor() { return []; } }
assert(
  "function constructor can bypass target's prototype",
  new Foo() instanceof Array,
  !(new Foo() instanceof Foo)
);

const IT = { sorry: "charlie" };
function MySingleton() {
  return IT;
}

assert("new does not imply newness", new MySingleton() === new MySingleton());

// prettier-ignore
class Q { constructor() { return Q; } }
assert("class constructor can return itself", new Q() === Q);

// Of course it can
