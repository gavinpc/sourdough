
function make_concatenate_files(spec)
  local inputs= spec.inputs
  local output = spec.output
  local arguments = {output, table.unpack(inputs)}
  -- local arguments = {output, inputs[1], inputs[2], inputs[3]}
  local rule
  if Windows then
    rule = cmd_call_powershell {
      inputs = inputs,
      script = concatenate_cwd..'/concatenate.ps1',
      arguments = arguments,
      outputs = {output}
    }
  else
    rule = sh_call_sh {
      inputs = inputs,
      script = concatenate_cwd..'/concatenate.sh',
      arguments = arguments,
      outputs = {output}
    }
  end
  return {spec=spec, rule=rule}
end
