# Concatenate the list of files into the indicated output file
param (
    [Parameter(Mandatory, Position=0)]
    $OutFile,

    [Parameter(Position=1, ValueFromRemainingArguments)]
    $InputFiles
)

$ErrorActionPreference = 'Stop'

# There is also Set-Content, which is encoding-aware, but I think we want raw
Get-Content -LiteralPath $InputFiles -Raw | Out-File $OutFile
