#!/bin/bash

# Concatenate the list of files into the indicated output file

set -e

cat "${@:2}" | "$1"

exit 0
