const ISO8601 = (function () {
  // operations on extended ISO 8601 representations
  // Uses a four-year sign-optional format, WHICH is not really valid ISO 8601
  // supports:
  // - dates from year -9999 (10,000 BC) through 9999
  // - precision Day, Month, Year, Decade, Century
  // - Millennia as special units using ranges
  const { assert } = console;
  function self_tests() {
    // prettier-ignore
    const CASES = [
      ["day_in_year", ["-0004-03-01"], 31 + 29], // called 5 BC but a leap year
      ["day_in_year", ["-0003-03-01"], 31 + 28], // called 4 BC but not a leap year
      ["day_in_year", ["-0001-03-01"], 31 + 28],
      ["day_in_year", ["0000-03-01"], 31 + 29], // called 1 BC but a leap year
      ["day_in_year", ["0001-03-01"], 31 + 28],
      ["day_in_year", ["0003-03-01"], 31 + 28],
      ["day_in_year", ["0004-03-01"], 31 + 29],
      ["day_in_year", ["1920-01-01"], 0],
      ["day_in_year", ["1920-01-02"], 1],
      ["day_in_year", ["1920-02-01"], 31],
      ["day_in_year", ["1919-12-30"], 363],
      ["day_in_year", ["1920-12-31"], 365], // leap year
      ["day_in_year", ["1900-12-31"], 364], // NOT a leap year
      ["day_in_year", ["1600-12-31"], 365], // leap year
      // parse(-0429/0000) throws “ill-formed millennium” but it's not intended as one
      ["recognize", ["1920s-40"], "192/194"],
      // ohhhhh this looks like march 1990
      // ["recognize", ["1990-03"], "1993/2004"],
      // ["recognize", ["1978-2023"], "1978/2023"], // later
      ["label", ["-9999"], "10000 BC"],
      ["label", ["-9998"], "9999 BC"],
      ["label", ["-0002"], "3 BC"],
      ["label", ["-0001"], "2 BC"],
      ["label", ["-0000"], "1 BC"], // though -0000 isn't valid...
      ["label", ["0000"], "1 BC"],
      ["label", ["0001"], "1"], // could also be ‘1 CE’
      ["label", ["0002"], "2"], // etc
      ["label", ["0012"], "12"],
      ["label", ["0123"], "123"],
      ["label", ["9999"], "9999"],
      ["label", ["1564-04", "en"], "April 1564"],
      // rn day is using short months
      // ["label", ["-0009-12", "en"], "December 10 BC"],
      ["label", ["1957-05-06"], "May 6, 1957"],
      // ["label", ["1957-07-06"], "July 6, 1957"],
      ["label", ["-0400-07-14"], "Jul 14, 401 BC"],
      ["label", ["-1999/-1000", "en"], "2nd millennium BC"],
      ["label", ["-0999/0000", "en"], "1st millennium BC"],
      ["label", ["0001/1000", "en"], "1st millennium"],
      ["label", ["1001/2000", "en"], "2nd millennium"],
      ["label", ["1902-03/1988"], "March 1902 – 1988"], // EN DASH, not hyphen
      // FAILS (rn) getting “August 18, 294 BC”... is this some time zone bs?
      // ["label", ["-0294-08-19"], "August 19, 294 BC"],
      ["compare_units", ["Year", "Year"], 0],
      ["compare_units", ["Year", "Month"], 1],
      ["compare_units", ["Month", "Year"], -1],
      ["compare_units", ["Decade", "Year"], 1],
      ["compare_units", ["Century", "Year"], 2],
      ["compare", ["1234", "1234"], 0], // indeed, ∀ x compare(x,x)=0
      ["compare", ["0234", "1234"], -1],
      ["compare", ["0234", "0230"], 1],
      ["compare", ["-0234", "0230"], -1],
      ["compare", ["-0234", "-0230"], -1],
      ["compare", ["-0234", "-0238"], 1],
      ["compare", ["-0100-06", "-0100-07"], -1],
      ["compare", ["-0100-06", "-0100-05"], 1],
      ["compare", ["-0100-06-11", "-0100-06-15"], -1],
      ["compare", ["-0100-06-19", "-0100-06-15"], 1],
      ["compare", ["-0100-06-19", "-0099-06-15"], -1],
      ["between", ["1234", "-0234", "1238"], true],
      ["between", ["1234", "-0234", "-1238"], false],
      // ["distance", ["-12", "-04"], {unit: "Century", n: 8 }],
      // ["distance", ["-01", "05"], {unit: "Century", n: 5 }],
      ["distance", ["-01", "01"], { unit: "Century", n: 1 }],
      ["distance", ["01", "05"], { unit: "Century", n: 4 }],
      ["distance", ["07", "18"], { unit: "Century", n: 11 }],
      ["distance", ["18", "18"], { unit: "Century", n: 0 }],
      ["distance", ["-0001", "0001"], { unit: "Year", n: 2 }],
      ["distance", ["-0001", "0001"], { unit: "Year", n: 2 }],
      ["distance", ["-0001", "0000"], { unit: "Year", n: 1 }],
      ["distance", ["0000", "0001"], { unit: "Year", n: 1 }],
      ["distance", ["0024", "0142"], { unit: "Year", n: 118 }],
      ["distance", ["1234", "1234"], { unit: "Year", n: 0 }],
      ["distance", ["1234", "2345"], { unit: "Year", n: 1111 }],
      ["distance", ["1234-01", "1234-01"], { unit: "Month", n: 0 }],
      ["distance", ["1234-01", "1234-02"], { unit: "Month", n: 1 }],
      ["distance", ["1234-01", "1235-01"], { unit: "Month", n: 12 }],
      ["distance", ["-0700-06", "-0695-06"], { unit: "Month", n: 60 }],
      ["distance", ["-0700-06", "-0695-07"], { unit: "Month", n: 61 }],
      ["distance", ["-0000-01", "0001-01"], { unit: "Month", n: 12 }], // but -0000 is invalid
      ["distance", ["-0001-01", "0000-01"], { unit: "Month", n: 12 }],
      ["distance", ["1066-02-01", "1066-02-01"], { unit: "Day", n: 0 }],
      ["distance", ["1066-02-01", "1066-02-03"], { unit: "Day", n: 2 }],
      ["distance", ["1066-02-01", "1066-03-01"], { unit: "Day", n: 28 }],
      ["distance", ["1066-02-01", "1067-03-01"], { unit: "Day", n: 28 + 365 }],
      ["next", ["-0099-12-31"], "-0098-01-01"],
      ["next", ["-0009-02-28"], "-0009-03-01"],
      ["next", ["-0001-12-31"], "0000-01-01"],
      ["next", ["0099-12-31"], "0100-01-01"],
      ["next", ["1432-11-30"], "1432-12-01"],
      ["next", ["-0088-11"], "-0088-12"],
      ["next", ["-0088-12"], "-0087-01"],
      ["next", ["-0001-12"], "0000-01"],
      ["next", ["0000-01"], "0000-02"],
      ["next", ["1607-08"], "1607-09"],
      ["next", ["9998-12"], "9999-01"],
      ["next", ["-9999"], "-9998"],
      ["next", ["-0001"], "0000"],
      ["next", ["0000"], "0001"],
      ["next", ["9998"], "9999"],
      // ["next", ["9999"], undefined], // for worse or better this returns "10000"
      // THESE fail, but narrower/broader assume 0s & 0s BC -- how are they notated?
      // ["next", ["-001"], "-000"],
      // ["next", ["-000"], "000"],
      ["next", ["000"], "001"],
      ["next", ["-02"], "-01"],
      ["next", ["-01"], "00"],
      ["next", ["00"], "01"],
      ["next", ["01"], "02"],
      ["next", ["-2999/-2000"], "-1999/-1000"],
      ["next", ["-1999/-1000"], "-0999/0000"],
      ["next", ["-0999/0000"], "0001/1000"],
      ["next", ["0001/1000"], "1001/2000"],
      ["next", ["1001/2000"], "2001/3000"],
      ["broader", ["2023-05-13"], "2023-05"],
      ["broader", ["-0294-08-19"], "-0294-08"],
      ["broader", ["2023-05"], "2023"],
      ["broader", ["2023"], "202"],
      // FAILS with 000 but this is 1BC and that is 0's AD
      // ["broader", ["0000"], "-000"],
      ["broader", ["-0001"], "-000"],
      ["broader", ["202"], "20"],
      ["broader", ["000"], "00"],
      ["broader", ["-001"], "-01"],
      ["broader", ["-040"], "-05"],
      ["broader", ["01"], "0001/1000"],
      ["broader", ["07"], "0001/1000"],
      ["broader", ["19"], "1001/2000"],
      ["broader", ["20"], "2001/3000"],
      ["broader", ["-01"], "-0999/0000"],
      ["broader", ["-02"], "-0999/0000"],
      ["broader", ["-10"], "-0999/0000"],
      ["broader", ["-11"], "-1999/-1000"],
      ["broader", ["-19"], "-1999/-1000"],
      ["broader", ["-20"], "-1999/-1000"],
      ["broader", ["0001"], "000"],
      ["narrower", ["2001/3000"], "20 21 22 23 24 25 26 27 28 29".split(" ")],
      ["narrower", ["-0999/0000"], "-10 -09 -08 -07 -06 -05 -04 -03 -02 -01".split(" ")],
      ["narrower", ["20"], "200 201 202 203 204 205 206 207 208 209".split(" ")],
      ["narrower", ["-01"], "-009 -008 -007 -006 -005 -004 -003 -002 -001 -000".split(" ")],
      ["narrower", ["-08"], "-079 -078 -077 -076 -075 -074 -073 -072 -071 -070".split(" ")],
      ["narrower", ["001"], "0010 0011 0012 0013 0014 0015 0016 0017 0018 0019".split(" ")],
      ["narrower", ["202"], "2020 2021 2022 2023 2024 2025 2026 2027 2028 2029".split(" ")],
      ["narrower", ["-001"], "-0018 -0017 -0016 -0015 -0014 -0013 -0012 -0011 -0010 -0009".split(" ")],
      // SPECIAL CASES: “0-to-9” decades
      ["narrower", ["-000"], "-0008 -0007 -0006 -0005 -0004 -0003 -0002 -0001 0000".split(" ")],
      ["narrower", ["000"], "0001 0002 0003 0004 0005 0006 0007 0008 0009".split(" ")],
      ["narrower", ["0030"], "0030-01 0030-02 0030-03 0030-04 0030-05 0030-06 0030-07 0030-08 0030-09 0030-10 0030-11 0030-12".split(" ")],
      ["narrower", ["0000"], "0000-01 0000-02 0000-03 0000-04 0000-05 0000-06 0000-07 0000-08 0000-09 0000-10 0000-11 0000-12".split(" ")],
      ["narrower", ["0000-02"], "0000-02-01 0000-02-02 0000-02-03 0000-02-04 0000-02-05 0000-02-06 0000-02-07 0000-02-08 0000-02-09 0000-02-10 0000-02-11 0000-02-12 0000-02-13 0000-02-14 0000-02-15 0000-02-16 0000-02-17 0000-02-18 0000-02-19 0000-02-20 0000-02-21 0000-02-22 0000-02-23 0000-02-24 0000-02-25 0000-02-26 0000-02-27 0000-02-28 0000-02-29".split(" ")],
      ["narrower", ["-0003-02"], "-0003-02-01 -0003-02-02 -0003-02-03 -0003-02-04 -0003-02-05 -0003-02-06 -0003-02-07 -0003-02-08 -0003-02-09 -0003-02-10 -0003-02-11 -0003-02-12 -0003-02-13 -0003-02-14 -0003-02-15 -0003-02-16 -0003-02-17 -0003-02-18 -0003-02-19 -0003-02-20 -0003-02-21 -0003-02-22 -0003-02-23 -0003-02-24 -0003-02-25 -0003-02-26 -0003-02-27 -0003-02-28".split(" ")],
      ["coerce", ["00", "Year"], "0001"],
      // THIS fails with -0098 but it's close enough for positioning
      // ["coerce", ["-01", "Year"], "-0099"],
      // THIS fails with 2000 for a similar reason, the enclosures are irregular
      // ["coerce", ["2001/3000", "Year"], "2001"],
      ["coerce", ["12", "Year"], "1200"],
      ["coerce", ["12", "Year", { align: "start" }], "1200"],
      ["coerce", ["12", "Year", { align: "end" }], "1299"],
      ["coerce", ["123", "Year"], "1230"],
      ["coerce", ["123", "Year", { align: "end" }], "1239"],
      ["coerce", ["1234", "Year", { align: "end" }], "1234"],
      ["coerce", ["1656-02", "Day", {align: "end"}], "1656-02-29"],
      ["coerce", ["1234", "Decade"], "123"],
      // align has no effect when broadening
      ["coerce", ["1234", "Decade", { align: "end" }], "123"],
      ["coerce", ["1234", "Century"], "12"],
      ["coerce", ["12", "Decade"], "120"],
      ["coerce", ["2023", "Millennium"], "2001/3000"],
    ];
    const equals = (a, b) => a === b || JSON.stringify(a) === JSON.stringify(b);
    for (const [op, args, expect] of CASES) {
      let got = ISO8601[op](...args);
      if (op === "recognize") got = got.value;
      assert?.(
        equals(got, expect),
        `${op}(${args}) got ${JSON.stringify(got)} vs ${JSON.stringify(expect)}`
      );
      // all returned dates should parse
      // all dates should have labels
      // all labels should be recognized as the thing that was their label
      // ∀ x,y broader(x)=y → y∈narrower(x)
      if (op === "broader") {
        const [x] = args;
        const narr = ISO8601.narrower(got);
        assert?.(narr.includes(x), `${x} ∉ narr(${got})`);
      }
      // ∀ x,y,z compare(x, y)=z ↔ compare(y, x)=-z
      if (op === "compare") {
        const [x, y] = args;
        const inv = ISO8601.compare(y, x);
        assert?.(expect === -inv, `comp(${y},${x}) ≠ ${-inv}`);
      }
      // ∀ x,y distance(x,y)=d ↔ distance(y,x)=-d
      if (op === "distance") {
        const [x, y] = args;
        const d = expect;
        const inv = ISO8601.distance(y, x);
        assert?.(d.n === -inv.n, `dist(${y},${x}) ≠ ${JSON.stringify(inv)}`);
      }
      // TODO: when plus is implemented for more stuff
      // ∀ x,y: next(x)=y → plus(x, 1)=y
      // ∀ x,y: previous(x)=y → plus(x, -1)=y
      if (op === "next") {
        // ∀ x,y next(x)=y ↔ previous(y)=x (i.e. previous inverseOf next)
        {
          const [x] = args;
          const y = expect;
          const py = ISO8601.previous(y);
          assert?.(equals(py, x), `prev(${y}) = ${x}, got ${py}`);
        }
        // ∀ x,y next(x)=y → x<y
        {
          const [x] = args;
          const cmp = ISO8601.compare(x, got);
          assert?.(cmp < 0, `${x} ≮ ${got} = next(${x})`);
        }
      }
    }
  }

  const MONTH_NAME = new Intl.DateTimeFormat(undefined, {
    month: "long",
    timeZone: "UTC",
  });
  const DAY_LABEL = new Intl.DateTimeFormat(undefined, {
    year: "numeric",
    month: "short",
    day: "numeric",
    timeZone: "UTC",
  });

  const int = s => parseInt(s, 10);
  const pad = len => n =>
    (n < 0 ? "-" : "") + Math.abs(n).toString().padStart(len, "0");
  const pad2 = pad(2);
  const pad3 = pad(3);
  const pad4 = pad(4);
  const pad6 = pad(6);

  // see https://vocabs.acdh.oeaw.ac.at/unit_of_time/
  // and https://vocabs.acdh.oeaw.ac.at/date/
  const _TIME_UNITS = {
    Millisecond: { rank: 0 },
    Second: { rank: 1 },
    Minute: { rank: 2 },
    Hour: { rank: 3 },
    Day: { rank: 4 },
    Week: { rank: 5 },
    Month: { rank: 6 },
    Year: { rank: 7 },
    Decade: { rank: 8 },
    Century: { rank: 9 },
    Millennium: { rank: 10 },
  };
  // larger = broader
  function compare_units(a, b) {
    assert?.(a in _TIME_UNITS, `unknown unit ${a}`);
    assert?.(b in _TIME_UNITS, `unknown unit ${b}`);
    return _TIME_UNITS[a].rank - _TIME_UNITS[b].rank;
  }
  const EN_UNITS = Object.freeze({
    Millisecond: { singular: "millisecond", plural: "milliseconds" },
    Second: { singular: "second", plural: "seconds" },
    Minute: { singular: "minute", plural: "minutes" },
    Hour: { singular: "hour", plural: "hours" },
    Day: { singular: "day", plural: "days" },
    Week: { singular: "week", plural: "weeks" },
    Month: { singular: "month", plural: "months" },
    Year: { singular: "year", plural: "years" },
    Decade: { singular: "decade", plural: "decades" },
    Century: { singular: "century", plural: "centurys" },
    Millennium: { singular: "millennium", plural: "millenniums" },
  });
  function label_unit(unit, plural, lang) {
    if (lang !== "en") throw new Error(`Not implemented: lang ${lang}`);
    const record = EN_UNITS[unit];
    if (!record) throw new Error(`Bad request: unknown unit ${unit}`);
    return plural ? record.plural : record.singular;
  }

  // ====== I/O

  {
    const DATE =
      /^(?<day>(?<month>(?<year>(?<decade>(?<bc>-?)(?<century>\d\d)(?<d>\d)?)(?<y>\d)?)(?:-(?<mm>\d\d))?)(?:-(?<dd>\d\d))?)$/;
    const MILLENNIUM = /^((-?)\d{4,5})\/-?(\d0000?)$/;

    function parse(iso) {
      if (typeof iso !== "string") return;

      // e.g. 2nd millennium is 1001/2000
      const mill = iso.match(MILLENNIUM);
      if (mill) {
        const [, from, bc, to] = mill;
        const n = int(`${bc}${to}`);
        // and we know from pattern that “to” ends in 000
        if (n === int(from) + 999)
          // `millennium` is the ending year (so, 1000 for first millennium)
          return { input: iso, unit: "Millennium", bc, millennium: n };
      }

      // Um, for now millennia are special
      let end;
      if (iso.includes("/")) {
        const [from, to] = iso.split("/", 2);
        end = parse(to);
        iso = from; // rosary
      }

      const match = iso.match(DATE);
      if (!match) return;

      const { dd, mm, y, d } = match.groups;
      // prettier-ignore
      const unit = dd ? "Day"
          : mm ? "Month"
          : y ? "Year"
          : d ? "Decade"
          : "Century"

      return { input: iso, unit, end, ...match.groups }; // HOTSPOT
    }

    // return date in the signed, six-digit format used by `Date`
    // ASSUMES the date is a well-formed date string (4-digit year, sign optional)
    // ASSUMES unit >= Year
    function _to_Date_ISO(date) {
      // allow for signed four-year dates, though those aren't recognized elsewhere yet
      if (date.startsWith("-") || date.startsWith("+"))
        return `${date.charAt(0)}00${date.slice(1)}`;
      return `+00${date}`;
    }

    // ASSUMES same as `_to_Date_ISO`
    function Date_from(date) {
      const js_iso = _to_Date_ISO(date);
      const ticks = Date.parse(js_iso);
      if (isNaN(ticks)) return;
      return new Date(ticks);
    }
  }

  // ====== English recognizing and labeling

  const languages = (function () {
    function make_recognizer(recognizers) {
      return function recognize(message) {
        for (const [pattern, transform] of recognizers) {
          const match = message.match(pattern);
          if (!match) continue;
          const value = transform(match, message);
          if (value === undefined) continue;
          // I'm sorry but I need to do this
          return { input: message, format: "iso8601", value, match };
        }
      };
    }

    const ORDINAL_SUFFIXES_EN = { 1: "st", 2: "nd", 3: "rd" };
    const ordinal_en = z => {
      const n = Math.abs(z);
      const c = n % 100;
      const suffix =
        c >= 11 && c <= 13 ? "th" : ORDINAL_SUFFIXES_EN[n % 10] ?? "th";
      return `${n}${suffix}`;
    };

    const RECOGNIZERS_EN = [
      [{ [Symbol.match]: parse }, parsed => parsed.input],
      // Roman numerals...
      // even with caps this will overlap with term searches
      [/^M+$/, ([s]) => millennium(s.length)],
      // ranges...
      [
        /(?<from>-?\s*\d+)[ths]*\s*(c\w*)?\s*(-|to|through|thru|\.\.+)\s*(?<to>-?\s*\d+)\s*(c\w*)?/i,
        ({ groups: { from, to } }, text) => {
          if (!text.toLowerCase().includes("c")) return;
          const first = int(from);
          const second = int(to);

          if (first > 10 && second < 10)
            // e.g. 15-6c
            // prettier-ignore
            return `${pad2(first - 1)}/${pad2(first - (first % 10) + second - 1)}`;

          return `${pad2(first - 1)}/${pad2(second - 1)}`;
        },
      ],
      [
        /(?<from>-?\s*\d+)\s*(['"]*s)?\s*.*\b(-|to|through|thru|\.\.+)[-\D]*(?<to>-?\s*\d+)\s*(['"]*s)?/,
        (match, text) => {
          const { from, to } = match.groups;
          if (!text.toLowerCase().includes("s")) return;
          const first = int(from);
          const second = int(to);

          if (first > second) {
            if (first >= 100 && second < 100) {
              const yy = first % 100;
              if (yy < second) {
                // e.g. 1640-50's
                // prettier-ignore
                return `${pad3(first / 10)}/${pad3((first - yy + second) / 10)}`;
              }

              if (second < 10)
                // e.g. 1690s-2's → 1690s -1720s
                return `${pad3(first)}/${pad3(first - yy + 100 + second * 10)}`;
            }
            // this makes no sense though
            return `${pad3(second)}/${pad3(first)}`;
          }

          return `${pad3(first)}/${pad3(second)}`;
        },
      ],
      [
        /(?<from>-?\s*\d+)\s*.*(-|to|through|thru|\.\.+).*(?<to>-?\s*\d+)/,
        ({ groups: { from, to } }) => {
          const first = int(from);
          const second = int(to);

          if (first > second) {
            if (first >= 100 && second < 100) {
              if (second < 10)
                // e.g. 1941-5
                return `${pad4(first)}/${pad4(first - (first % 10) + second)}`;
              // e.g. 580-90
              return `${pad4(first)}/${pad4(first - (first % 100) + second)}`;
            }
            return `${pad4(second)}/${pad4(first)}`;
          }
          return `${pad4(first)}/${pad4(second)}`;
        },
      ],
      [
        /^\s*-?(first|second|third|fourth|fifth|sixth|seventh|\d?1st|\d?2nd|\d?3rd|\d?[04-9]th|\d{1,2})\s*c+(e(n(t(u(r(y)?)?)?)?)?)?\s*(b+c*e*)?$/,
        message => {
          const bc = message.includes("-") || message.includes("b");
          const n = Math.abs(int(message));
          const cc = bc ? n : n - 1;
          return `${bc ? "-" : ""}${pad2(cc)}`;
        },
      ],
      [
        /^\s*-?\d{1,4}'?s\s*(b+c*e*)?$/,
        message => {
          const bc = message.includes("-") || message.includes("b");
          const n = int(message);
          const year = bc && n > 0 ? -n : n;
          const decade = Math.floor(year / 10);
          return `${bc ? "-" : ""}${pad3(decade)}`;
        },
      ],
      [
        /^\s*-?\d{1,4}\s*(b+c*e*)?$/,
        message => {
          const bc = message.includes("-") || message.includes("b");
          const n = int(message);
          const year = bc && n > 0 ? -n : n;
          return `${bc ? "-" : ""}${pad4(year)}`;
        },
      ],
    ];

    function label_en(iso, simple) {
      const parsed = parse(iso);
      assert?.(parsed, `label en got bad date ${iso}`);
      // millennia is a special case of ranges which has its own unit handling
      if (!simple && iso.includes("/") && parsed.unit !== "Millennium") {
        const [lo, hi] = iso.split("/", 2);
        // TBD dependent ranges, e.g. 1920's – 40's, or 1st – 2nd millennia
        // else label independently (which note is not language specific)
        return `${label_en(lo, true)} – ${label_en(hi, true)}`;
      }
      const { unit, bc } = parsed;
      const maybe_bc = bc ? " BC" : ""; // yeah yeah BCE
      switch (unit) {
        case "Millennium": {
          const m = parsed.millennium / 1000;
          const mm = bc ? 1 - m : m;
          return `${ordinal_en(mm)} millennium${maybe_bc}`;
        }
        case "Century": {
          const c = Math.abs(int(parsed.century)) + (bc ? 0 : 1);
          return `${ordinal_en(c)} century${maybe_bc}`;
        }
        case "Decade":
          // maybe add (decade) for first decade of a century
          return `${Math.abs(int(parsed.decade)) * 10}’s${maybe_bc}`;

        case "Year": {
          const year = int(parsed.year);
          // could also put AD or CE for small numbers
          return year > 0 ? `${year}` : `${Math.abs(year) + 1} BC`;
        }
        case "Month": {
          const year_label = label_en(parsed.year);
          const iso = bc ? parsed.input.slice(1) : parsed.input;
          const ticks = Date.parse(iso);
          const first = new Date(ticks);
          const month_name = MONTH_NAME.format(first);
          return `${month_name} ${year_label}`;
        }
        case "Day": {
          const js_iso = _to_Date_ISO(iso);
          const ticks = Date.parse(js_iso);
          return `${DAY_LABEL.format(ticks)}${maybe_bc}`;
        }
      }
      // console.warn(`Not implemented, labeling ${unit}`);
      return iso;
    }

    const en_recognizer = make_recognizer(RECOGNIZERS_EN);

    function recognize(text, lang = "en") {
      if (lang === "en") return en_recognizer(text);
      console.info(`Not implemented ${lang}`);
    }

    function label(iso, lang = "en") {
      if (lang === "en") return label_en(iso);
      console.warn(`Language ‘${lang}’ is not supported for ISO labeling`);
      return iso; // fallback to ISO
    }

    return { recognize, label };
  })();

  const range = (n, f) => {
    const ret = Array(n);
    for (let i = 0; i < n; i++) ret[i] = f(i);
    return ret;
  };

  // inclusive --- though we generally want end exclusive
  const between = (date, a, b) =>
    compare(a, date) <= 0 && compare(b, date) >= 0;

  // >0 means sort a after b
  // <0 means sort a before b
  function compare(a, b) {
    if (a === b) return 0;
    if (a.startsWith("-")) {
      if (b.startsWith("-")) {
        // in BC comparisons, years (and *only* years) are reversed
        const a_year = a.slice(0, 5); // allocation
        // do a within-year comparison... and more allocation
        if (b.startsWith(a_year)) return a.slice(5) < b.slice(5) ? -1 : 1;
        return a < b ? 1 : -1;
      }
      return -1;
    }
    if (b.startsWith("-")) return 1;
    return a < b ? -1 : 1;
  }

  const get_c = parsed => int(`${parsed.bc}${parsed.century}`);
  const get_m = (parsed, c = get_c(parsed)) => Math.floor(c / 10) + 1;
  const m2 = e => `${pad4(e - 999)}/${pad4(e)}`;
  const millennium = m => m2(m * 1000);

  // ====== sequence: next & previous

  function next(date) {
    const the = parse(date);
    assert?.(the, `next got bad date`, date);
    switch (the.unit) {
      case "Millennium":
        return m2(the.millennium + 1000);
      case "Century":
        return pad2(get_c(the) + 1);
      case "Decade":
        return pad3(int(the.decade) + 1);
      case "Year":
        return pad4(int(the.year) + 1);
      case "Month":
        if (the.mm === "12") return `${next(the.year)}-01`;
        return `${the.year}-${pad2(int(the.mm) + 1)}`;
      case "Day": {
        const js_date = Date_from(date);
        js_date.setUTCDate(js_date.getUTCDate() + 1);
        return _Date_to_Day(js_date);
      }
    }
    throw new Error(`next not implemented for ${the.unit}`);
  }

  function _Date_to_Day(js_date) {
    const js_iso = js_date.toISOString();
    if (js_iso.startsWith("-") || js_iso.startsWith("+"))
      return `${js_iso.charAt(0)}${js_iso.substring(3, 13)}`;
    return js_iso.slice(0, 10);
  }

  function previous(date) {
    const the = parse(date);
    assert?.(the, `previous got bad date`, date);
    switch (the.unit) {
      case "Millennium":
        return m2(the.millennium - 1000);
      case "Century":
        return pad2(get_c(the) - 1);
      case "Decade":
        return pad3(int(the.decade) - 1);
      case "Year":
        return pad4(int(the.year) - 1);
      case "Month":
        if (the.mm === "01") return `${previous(the.year)}-12`;
        return `${the.year}-${pad2(int(the.mm) - 1)}`;
      case "Day": {
        // Same as `next` except `-1` instead of `+1`
        const js_date = Date_from(date);
        js_date.setUTCDate(js_date.getUTCDate() - 1);
        return _Date_to_Day(js_date);
      }
    }
    throw new Error(`previous not implemented for ${the.unit}`);
  }
  // ====== Taxonomical: broader & narrower

  function broader(iso) {
    const parsed = parse(iso);
    if (!parsed) return;
    switch (parsed.unit) {
      case "Century":
        return millennium(get_m(parsed));
      case "Decade":
        return pad2(parsed.bc ? -1 - int(parsed.century) : int(parsed.century));
      case "Year":
        return parsed.decade;
      case "Month":
        return parsed.year;
      case "Day":
        return parsed.month;
    }
  }

  function narrower(iso) {
    const parsed = parse(iso);
    if (!parsed) return undefined;
    switch (parsed.unit) {
      case "Millennium" /* → Centuries */: {
        const n = int(parsed.millennium) / 1000;
        if (n !== Math.floor(n)) throw new Error(`BAD MILLENNIUM ${iso}`);
        return range(10, i => pad2((n - 1) * 10 + i + (parsed.bc ? 0 : 0)));
      }
      case "Century" /* → Decades */: {
        const { bc, century } = parsed;
        const n = int(century);
        return range(10, i => `${bc}${pad3(10 * n + (bc ? -1 - i : i))}`);
      }
      case "Decade" /* → Years */: {
        if (parsed.input === "000") return range(9, i => pad4(i + 1)); // 0s
        if (parsed.input === "-000") return range(9, i => pad4(i - 8)); // 0s BC
        if (parsed.bc) {
          const n = int(parsed.decade);
          return range(10, i => pad4(10 * n - 8 + i));
        }
        return range(10, i => `${parsed.input}${i}`);
      }
      case "Year" /* → Months */:
        return range(12, i => `${parsed.input}-${pad2(i + 1)}`);

      case "Month" /* → Days */: {
        const ret = [];
        const year = int(parsed.year);
        const js_date = new Date(Date.UTC(year, int(parsed.mm) - 1));
        js_date.setUTCFullYear(year); // account for Date.UTC's quirk re 1900's
        const month = js_date.getUTCMonth();
        while (month === js_date.getUTCMonth()) {
          const day = js_date.getUTCDate();
          ret.push(`${parsed.month}-${pad2(day)}`);
          js_date.setUTCDate(day + 1);
        }
        return ret;
      }
    }
  }

  // returns {unit, n}.  is `n` an integer then?
  const distance = (iso_from, iso_to) => {
    const a = parse(iso_from);
    const b = parse(iso_to);
    const unit = a?.unit;
    if (unit !== b?.unit)
      throw new Error(`Not supported: ${iso_to}-${iso_from}`);

    // A surer way to maintain this property
    // as it is, the below must deal with a ≶ b
    // if (compare(iso_from, iso_to) > 0)
    //   return { unit, n: -distance(iso_to, iso_from).n };

    switch (unit) {
      case "Millennium":
        return { unit, n: int(b.millennium) / 1e3 - int(a.millennium) / 1e3 };
      case "Century": {
        // There is no 0th century; skip it
        // WRONG! there is a 0 century, we just don't call it that
        const aa = get_c(a);
        const bb = get_c(b);
        if (aa < 0 && bb > 0) return { unit, n: bb - aa - 1 };
        if (bb < 0 && aa > 0) return { unit, n: bb - aa + 1 };
        return { unit, n: bb - aa };
      }
      case "Decade":
        return { unit, n: int(b.decade) - int(a.decade) };
      case "Year":
        return { unit, n: int(b.year) - int(a.year) };
      case "Month": {
        const years = int(b.year) - int(a.year);
        return { unit, n: years * 12 + int(b.mm) - int(a.mm) };
      }
      case "Day": {
        const a_ticks = Date_from(a.input).getTime();
        const b_ticks = Date_from(b.input).getTime();
        return { unit, n: Math.floor((b_ticks - a_ticks) / TICKS_PER_DAY) };
      }
    }
    throw new Error(`Distance is not implemented for ${unit}`);
  };

  // PRELIMINARY
  function plus(date, units, in_unit) {
    const parsed = parse(date);
    if (!parsed) throw new Error(`Bad request: plus got bad date`);
    const unit = in_unit ?? parsed.unit;
    if (units !== Math.floor(units))
      throw new Error(`Bad request: +non-integer ${units}`);
    if (unit !== parsed.unit)
      throw new Error(`Not implemented: +${unit} for ${parsed.unit}`);

    switch (unit) {
      case "Day": {
        const js_date = Date_from(date);
        js_date.setUTCDate(js_date.getUTCDate() + units);
        return _Date_to_Day(js_date);
      }
    }
    throw new Error(`Not implemented: +${unit}`);
  }

  // return a date with `unit` precision related to date
  // if unit is less specific, then take the broader containing
  // if unit is more specific, then align start (i.e. take first narrower)
  // KNOWN ISSUE: gets first year of last decade when end-aligning from century to year
  function coerce(date, unit, options) {
    const align = options?.align ?? "start";
    assert?.(align === "start" || align === "end", `bad align ${align}`);
    const parsed = parse(date);
    if (!parsed) throw new Error(`Coerce got bad date ${date}`);
    if (parsed.unit === unit) return date;
    if (compare_units(parsed.unit, unit) < 0)
      return coerce(broader(date), unit, options);
    const parts = narrower(date);
    const index = align === "start" ? 0 : -1;
    const part = parts.at(index);
    if (!part) throw new Error(`Failed coercing ${date} to ${unit}`);
    return coerce(part, unit, options);
  }

  const TICKS_PER_DAY = 24 * 60 * 60 * 1000;
  function day_in_year(date) {
    const js_date = Date_from(date);
    const year = js_date.getUTCFullYear();
    // Unfortunately,
    //     new Date(Date.UTC(0, 0, 1)).toISOString()
    //     "1900-01-01T00:00:00.000Z"
    // even though
    //     new Date(Date.UTC(-1, 0, 1)).toISOString()
    //     "-000001-01-01T00:00:00.000Z"
    // const first_of_year = new Date(Date.UTC(year, 0, 1));
    // TODO: does this need +1 for BC?
    const first_of_year = Date_from(`${pad4(year)}-01-01`);
    const start_ticks = first_of_year.getTime();
    const end_ticks = js_date.getTime();
    const delta_ticks = end_ticks - start_ticks;
    const days = delta_ticks / TICKS_PER_DAY;
    const day = Math.round(days);
    return day;
  }
  // heavy... we could get close enough leap year with modulos
  // yes: https://tc39.es/ecma262/multipage/numbers-and-dates.html#sec-daysinyear
  function days_in_year(date) {
    const parsed = parse(date);
    const year_end = `${parsed.year}-12-31`; // ≍ coerce(date, "Day", {align: "end"})
    return day_in_year(year_end) + 1;
  }

  const io = { parse, Date_from };
  const ord = { compare, between, distance, plus }; // poset & metric space
  const seq = { next, previous };
  const skos = { broader, narrower };
  const units = { compare_units, label_unit };
  const calendar = { days_in_year, day_in_year }; // scalar support
  const other = { ...units, self_tests, coerce, ...calendar };
  const ISO8601 = { ...io, ...ord, ...seq, ...skos, ...languages, ...other };
  Object.assign(globalThis, { ISO8601 });
  return ISO8601;
})();

// indexeddb wrapper stuff
// good info about indexedDB state here https://stackoverflow.com/q/33441956

(function () {
  if (!indexedDB) {
    console.warn("Your browser doesn't support a stable version of IndexedDB!");
    return;
  }
  const { stringify } = JSON;
  const { keys } = Object;
  const { assert } = console;

  const NULL_BUFFER = { is_empty: () => true, is_full: () => true, length: 0 };
  const make_ring_buffer = size => {
    if (size === 0) return NULL_BUFFER;
    const _buffer = new Array(size);
    let start = 0; // aka head, where the first item is (when ≠ end)
    let end = 0; // aka tail, where the next item will go
    let length = 0;

    return {
      is_empty: () => length === 0,
      is_full: () => length === size,
      push(value) {
        assert?.(length < size, `Full!`);
        ++length;
        _buffer[end] = value;
        end = (end + 1) % size;
      },
      pop() {
        assert?.(length !== 0, `Empty!`);
        --length;
        const value = _buffer[start];
        start = (start + 1) % size;
        return value;
      },
      get length() {
        return length;
      },
    };
  };

  // ===== async helpers
  const sleep = ms => new Promise(r => setTimeout(r, ms));

  /**
   * @param options {{max_concurrent?: number}}
   */
  const async_queue = options => {
    const queue = []; // promise thunks
    const inflight = new Set(); // promises
    const max = options?.max_concurrent ?? 1;

    const launch = thunk => {
      const promise = thunk();
      inflight.add(promise);
      promise.finally(() => {
        inflight.delete(promise);
        if (queue.length > 0) launch(queue.shift());
      });
      return promise;
    };

    /**
     * @template T
     * @param thunk {() => Promise<T>}
     * @returns {Promise<T>}
     */
    return thunk => {
      if (inflight.size < max) {
        assert?.(
          !queue.length,
          `${queue.length} in queue ${inflight.size}<${max}!`
        );
        return launch(thunk);
      }
      return new Promise(resolve => {
        queue.push(() => {
          const promise = thunk();
          resolve(promise);
          return promise;
        });
      });
    };
  };

  /**
   * @template T
   * @param array {T[]}
   * @returns {array is T[] & { shift(): T; pop(): T }}
   */
  const is_not_empty = array => array.length > 0;

  /**
   * A minimal blocking queue.
   *
   * @param buffer {ReturnType<typeof make_ring_buffer>}
   */
  function make_channel(buffer) {
    const pending_put_values = [];
    const pending_put_resolves = [];
    const pending_takes = [];
    return {
      can_put_now: () => !buffer.is_full(),
      can_take_now: () => !buffer.is_empty(),
      put(value) {
        if (is_not_empty(pending_takes)) {
          const n = pending_takes.length;
          assert?.(buffer.is_empty(), `${n} pending takes but non-∅ buffer `);
          pending_takes.shift()(value);
        } else if (!buffer.is_full()) buffer.push(value);
        else
          return new Promise(resolve => {
            pending_put_values.push(value);
            pending_put_resolves.push(resolve);
            // if count exceeds expected # of (well-behaved) writers
            // console.debug(`${pending_put_resolves.length} pending puts`);
          });
      },
      take() {
        if (buffer.is_empty()) {
          const n = pending_put_values.length;
          assert?.(n === 0, `∅ buffer but ${n} pending puts `);
          return new Promise(resolve => pending_takes.push(resolve));
        }

        const value = buffer.pop();
        if (is_not_empty(pending_put_values)) {
          const resolve = pending_put_resolves.shift();
          const value = pending_put_values.shift();
          buffer.push(value);
          resolve();
        }
        return value;
      },
    };
  }

  // ASSUMES this is called during a versionchange transaction
  /**
   * @param db {IDBDatabase}
   * @param name {string}
   * @param description {IDBStoreDescription}
   */
  function create_store_from(db, name, description) {
    const { keyPath, autoIncrement, indexes } = description;
    // we could create the store with neither a key nor autoincrement
    // but we won't be able to use our I/O with it (which has no ID channel)
    if (keyPath == null && autoIncrement == null)
      throw new Error(`Out-of-line keys are not supported`);
    const store = db.createObjectStore(name, { keyPath, autoIncrement });
    if (indexes) {
      for (const index_name of keys(indexes)) {
        const spec = indexes[index_name];
        store.createIndex(index_name, spec.keyPath, spec);
      }
    }
    return store;
  }

  // ASSUMES this is called during a versionchange transaction
  /**
   * @param store {IDBObjectStore}
   * @param description {IDBStoreDescription}
   */
  function update_store_from(store, description) {
    const upgrades = [...diff_store(store, description)];
    for (const difference of upgrades) {
      if (difference.type === "missing") {
        const { index: index_name, description } = difference;
        const { keyPath, ...options } = description;
        store.createIndex(index_name, keyPath, options);
      } else throw new Error(`Not implemented: ${difference.type} diffs`);
    }
  }

  /**
   * @param store {IDBObjectStore}
   * @param description {IDBStoreDescription}
   */
  function* diff_store(store, description) {
    const { keyPath, autoIncrement, indexes } = description;

    if (
      store.autoIncrement !== (autoIncrement ?? false) ||
      stringify(store.keyPath) !== stringify(keyPath ?? null)
    )
      yield { type: "pk" };

    if (indexes) {
      for (const name of keys(indexes)) {
        const spec = indexes[name];
        if (!store.indexNames.contains(name)) {
          yield { type: "missing", index: name, description: spec };
          continue;
        }
        const index = store.index(name);
        if (
          index.unique !== (spec.unique ?? false) ||
          index.multiEntry !== (spec.multiEntry ?? false) ||
          stringify(index.keyPath) !== stringify(spec.keyPath)
        )
          yield { type: "index", index: name, description: spec };
      }
    }
  }

  const gate = async_queue({ max_concurrent: 1 });

  /**
   * The *only* place to open a connection.
   *
   * @param db_name {string}
   * @param version {number | undefined}
   * @param upgrade {((db: IDBDatabase, txn: IDBTransaction) => void) | undefined}
   * @returns {Promise<IDBDatabase>}
   */
  async function connect(db_name, version, upgrade) {
    return gate(() => {
      return new Promise(async (resolve, reject) => {
        await sleep(1); // avoid `versionchange` race condition in Chrome
        const request = indexedDB.open(db_name, version);
        request.onerror = error => reject(`${error}`);
        request.onblocked = e => {
          reject(`BLOCKED → ${e.newVersion}`);
        };
        request.onsuccess = () => {
          const db = request.result;
          db.addEventListener("versionchange", () => {
            db.close(); // the closest thing to a best practice in this API
          });
          resolve(db);
        };
        if (typeof upgrade === "function") {
          request.onupgradeneeded = () => {
            upgrade(request.result, request.transaction);
          };
        }
      });
    });
  }

  const target_versions = new Map();

  /**
   * Resolve to an open connection to the indicated database in which the
   * indicated store was definitely configured as described when you left it.
   *
   * @param db_name {string}
   * @param store_name {string}
   * @param description {IDBStoreDescription}
   * @returns {Promise<IDBDatabase>}
   */
  async function database_with(db_name, store_name, description) {
    // If you open at default version & get upgrade, the database didn't exist
    let created = false;
    const existing_db = await connect(db_name, undefined, db => {
      created = true;
      create_store_from(db, store_name, description);
    });
    if (created) return existing_db; // since we know we configured store

    // The database existed already
    if (existing_db.objectStoreNames.contains(store_name)) {
      const txn = existing_db.transaction(store_name);
      const store = txn.objectStore(store_name);
      const [any_difference] = diff_store(store, description);
      if (!any_difference) return existing_db;
    }

    // The store didn't exist or needs configuration
    existing_db.close();
    const last_version = target_versions.get(db_name) ?? existing_db.version;
    const next_version = last_version + 1;
    target_versions.set(db_name, next_version);

    return connect(db_name, next_version, (db, txn) => {
      if (db.objectStoreNames.contains(store_name)) {
        const store = txn.objectStore(store_name);
        update_store_from(store, description);
      } else {
        create_store_from(db, store_name, description);
      }
    });
  }

  /**
   * Resolve to a store in the indicated database that has the described
   * configuration, attached to a transaction in the indicated mode.
   *
   * @param db_name {string}
   * @param store_name {string}
   * @param description {IDBStoreDescription}
   * @param mode {Exclude<IDBTransactionMode, "versionchange">}
   * @returns {Promise<IDBObjectStore>}
   */
  async function get_store_with(db_name, store_name, description, mode) {
    const db = await database_with(db_name, store_name, description);
    const txn = db.transaction(store_name, mode); // NEW TRANSACTION
    return txn.objectStore(store_name);
  }

  // need cursor reader at least for backwards traversal
  // can do paginated for forwards smh
  // what a cluster https://github.com/w3c/IndexedDB/issues/130

  /**
   * Feed a cursor into a given channel.
   *
   * @param thunk {() => Promise<IDBRequest<IDBCursor>>}
   * @param channel {ReturnValue<typeof make_channel>}
   */
  async function pipe_cursor_to(thunk, channel, is_index) {
    let resolve, reject, cursor;

    function make_new_cursor_request() {
      const request = thunk();
      // have yet to see this happen though & most don't apply to cursors
      request.onerror = error => reject(`${error}`);
      request.onsuccess = event => resolve(event.target.result);
      return request;
    }

    const next_read = () =>
      new Promise((_resolve, _reject) => {
        resolve = _resolve;
        reject = _reject;
      });

    make_new_cursor_request();
    while (true) {
      cursor = await next_read();
      if (!cursor) break;
      const { value } = cursor; // HOTSPOT
      // remember in case we need to resume
      const frontier = { key: cursor.key, pk: cursor.primaryKey }; // HOTSPOT
      if (channel.can_put_now()) {
        channel.put(value);
        cursor.continue(); // HOTSPOT
        continue;
      }
      await channel.put(value);
      // we yielded so the transaction is liable to be closed
      const state_was = cursor.request.readyState;
      try {
        cursor.continue(); // HOTSPOT
        assert?.(state_was !== "done", "then how did we get here");
      } catch (error) {
        assert?.(state_was === "done", "then why did we get here");
        if (error.name === "TransactionInactiveError") {
          make_new_cursor_request();
          cursor = await next_read();
          if (!cursor) break;
          if (is_index) {
            cursor.continuePrimaryKey(frontier.key, frontier.pk);
          } else {
            cursor.continue(frontier.key);
          }
          // we returned to the item we already put, so ignore & move to the next
          if (!(await next_read())) break;
          cursor.advance(1);
        } else throw error;
      }
    }
    channel.put(DONE);
  }

  // unlike write, this is not responsible for the db/store existing
  // “batching” here amounts to read-ahead, which keeps txn alive between yields
  // still yields single records
  const DONE = Symbol("done");
  async function* read(spec) {
    const trace = false ? (...x) => console.debug(`ℝ`, ...x) : null;
    const db_name = spec.db;
    const store_name = spec.store;
    const index_name = spec.index;
    const batch_size = spec.batch_size ?? 16;
    const __buffer = make_ring_buffer(batch_size); // don't reference this though
    const channel = make_channel(__buffer);

    let connection = await connect(db_name);

    function make_cursor_request() {
      trace?.(`creating new transaction & everything`);
      const transaction = connection.transaction(store_name); // NEW TRANSACTION
      const store = transaction.objectStore(store_name);
      const source = index_name ? store.index(index_name) : store;
      const request = source.openCursor(spec.query, spec.dir);
      return request;
    }

    const is_index = !!index_name; // TEMP
    pipe_cursor_to(make_cursor_request, channel, is_index);

    // a generic channel consume
    try {
      while (true) {
        const result = channel.can_take_now()
          ? channel.take()
          : await channel.take();
        if (result === DONE) break;
        yield result;
      }
    } finally {
      connection.close();
    }
  }

  async function* write(source, destination, options) {
    const trace = false ? (...x) => console.debug("WRITE", ...x) : null;
    if (!source) throw new Error(`Write requires a source!`);
    if (!destination) throw new Error(`Write requires a destination!`);
    const { database: db, store: store_name, description } = destination;
    if (!description) throw new Error(`Destination should have a description!`);
    let size = options?.batch_size ?? 16;
    assert?.(typeof size === "number", `Write got a ${typeof size} batch size`);
    const store = await get_store_with(
      db,
      store_name,
      description,
      "readwrite"
    );
    let total_written = 0;
    let written_this_round = 0;
    let errors = null;
    try {
      // don't we need to do this at some point?  and check success?
      // store.transaction.commit();
      for (const item of source) {
        trace?.("item", item);
        try {
          store.add(item);
        } catch (error) {
          if (
            error.type === "TransactionInactiveError" ||
            error.toString().startsWith("TransactionInactiveError")
          ) {
            trace?.("caught TransactionInactiveError", error);
            // okay now what?
            throw error;
          }
          errors ??= [];
          errors.push(`${error}`);
          // shouldn't we still count this against size?
          continue;
        }
        written_this_round++;
        trace?.(`${written_this_round} written this round`);
        total_written++;
        if (written_this_round >= size) {
          const status = { total_written, written_this_round, errors };
          trace?.(`in loop yielding and resetting`, status);
          yield status;
          written_this_round = 0;
          errors = null;
        }
      }
      if (written_this_round > 0 || errors.length > 0) {
        const final_status = { total_written, written_this_round, errors };
        trace?.(`yielding final status`, final_status);
        yield final_status;
      }
    } finally {
      store.transaction.db.close();
      trace?.(`closed connection`);
    }
  }

  async function get_by_key(spec, key) {
    const connection = await connect(spec.db, undefined);
    const transaction = connection.transaction(spec.store);
    const store = transaction.objectStore(spec.store);
    const source = spec.index ? store.index(spec.index) : store;
    const request = source.get(key);
    return new Promise((resolve, reject) => {
      request.onerror = reject;
      request.onsuccess = () => resolve(request.result);
    });
  }

  async function count(spec) {
    const connection = await connect(spec.db);
    const transaction = connection.transaction(spec.store);
    const store = transaction.objectStore(spec.store);
    const source = spec.index ? store.index(spec.index) : store;
    const request = source.count(spec.query);
    return new Promise((resolve, reject) => {
      request.onerror = error => reject(`${error}`);
      request.onsuccess = () => resolve(request.result);
    });
  }

  const io = { read, write };
  const expedient = { get_by_key, count };
  const lldb = { async_queue, database_with, diff_store, connect }; // mainly for debug
  const proc = { make_ring_buffer, make_channel }; // for testing
  const IDB = { ...io, ...lldb, ...proc, ...expedient };
  Object.assign(globalThis, { IDB });
})();

function assert_equals(a, b, ...messages) {
  if (messages.length) console.log(...messages);
  const diffs = [...diff_any(a, b)];
  if (diffs.length === 0) {
    console.info(`Results match!`);
  } else {
    console.error(`Results do not match!`);
    console.group(`Diffs:`);
    for (const { type, path, ...diff } of diffs)
      console.log(`${type} @ ${path.join(".")}`, diff);
    console.groupEnd();
  }
}

// ===Diffing tools
const { is, keys } = Object;
const { isArray } = Array;

function* diff_objects(a, b, path = []) {
  if (a === null || b === null) {
    if (a !== null || b !== null) yield { type: "null", path, a, b };
    return;
  }
  if (isArray(a) !== isArray(b)) {
    yield { type: "array", path, a: isArray(a), b: isArray(b) };
    return;
  }
  const a_keys = isArray(a) ? [...a.keys()] : keys(a);
  const b_keys = isArray(b) ? [...b.keys()] : keys(b);
  if (a_keys.length !== b_keys.length) {
    yield { type: "key#", path, a: a_keys, b: b_keys };
  }
  for (const key of a_keys) {
    if (!b.hasOwnProperty(key)) yield { type: "key-", path, key };
  }
  for (const key of b_keys) {
    if (!a.hasOwnProperty(key)) yield { type: "key+", path, key };
  }
  for (const key of a_keys) {
    const a_item = a[key];
    const b_item = b[key];
    yield* diff_any(a_item, b_item, [...path, key]);
  }
}

function* diff_any(a, b, path = []) {
  if (is(a, b)) return;
  const a_type = typeof a;
  const b_type = typeof b;
  if (a_type !== b_type) {
    yield { type: "typeof", path, a: a_type, b: b_type };
    return;
  }
  if ((a === null) !== (b === null)) {
    yield { type: "null", path, a, b };
    return;
  }
  switch (a_type) {
    case "undefined":
    case "boolean":
    case "number":
    case "bigint":
    case "string":
    case "symbol":
      yield { type: "value", path, a, b }; // we already checked equals
      break;
    case "object":
      yield* diff_objects(a, b, path);
      break;
    case "function": {
      throw new Error(`Not supported: diffing function`);
    }
  }
}

(function test_catalog() {
  const { assert } = console;
  const { read, write } = IDB;

  const { stringify } = JSON;
  const cheap_equals = (a, b) => stringify(a) == stringify(b);

  // actually we have diff functions now...
  function assert_cheap_equals(a, b, ...messages) {
    if (cheap_equals(a, b)) return;
    console.error(...messages);
    throw new Error(`${a} ≠ ${b}`);
  }

  const sleep = ms => new Promise(r => setTimeout(r, ms));

  // US states example data
  const US_STATES = {
    keyPath: "fips",
    indexes: [
      ["name", "name", { unique: true }],
      // this is an ISO date, where do we say that?
      ["joined", "joined", { unique: false }],
    ],
    object: {
      AL: { name: "Alabama", joined: "1819-12-14" },
      AK: { name: "Alaska", joined: "1959-01-03" },
      AZ: { name: "Arizona", joined: "1912-02-14" },
      AR: { name: "Arkansas", joined: "1836-06-15" },
      CA: { name: "California", joined: "1850-09-09" },
      CO: { name: "Colorado", joined: "1876-08-01" },
      CT: { name: "Connecticut", joined: "1788-01-09" },
      DE: { name: "Delaware", joined: "1959-01-03" },
      FL: { name: "Florida", joined: "1845-03-03" },
      GA: { name: "Georgia", joined: "1788-01-02" },
      HI: { name: "Hawaii", joined: "1959-01-03" },
      ID: { name: "Idaho", joined: "1890-07-03" },
      IL: { name: "Illinois", joined: "1818-12-03" },
      IN: { name: "Indiana", joined: "1816-12-11" },
      IA: { name: "Iowa", joined: "1846-12-28" },
      KS: { name: "Kansas", joined: "1861-01-29" },
      KY: { name: "Kentucky", joined: "1792-06-01" },
      LA: { name: "Louisiana", joined: "1812-04-30" },
      ME: { name: "Maine", joined: "1820-03-15" },
      MD: { name: "Maryland", joined: "1788-04-28" },
      MA: { name: "Massachusetts", joined: "1788-01-02" },
      MI: { name: "Michigan", joined: "1837-01-26" },
      MN: { name: "Minnesota", joined: "1858-05-11" },
      MS: { name: "Mississippi", joined: "1817-12-10" },
      MO: { name: "Missouri", joined: "1821-08-10" },
      MT: { name: "Montana", joined: "1889-11-08" },
      NE: { name: "Nebraska", joined: "1867-03-01" },
      NV: { name: "Nevada", joined: "1864-10-31" },
      NH: { name: "New Hampshire", joined: "1788-06-21" },
      NJ: { name: "New Jersey", joined: "1787-12-18" },
      NM: { name: "New Mexico", joined: "1912-01-06" },
      NY: { name: "New York", joined: "1788-07-26" },
      NC: { name: "North Carolina", joined: "1789-11-21" },
      ND: { name: "North Dakota", joined: "1889-11-02" },
      OH: { name: "Ohio", joined: "1803-03-01" },
      OK: { name: "Oklahoma", joined: "1907-11-16" },
      OR: { name: "Oregon", joined: "1819-12-14" },
      PA: { name: "Pennsylvania", joined: "1819-12-14" },
      PR: { name: "Puerto Rico", joined: "1820-03-15" },
      RI: { name: "Rhode Island", joined: "1790-05-29" },
      SC: { name: "South Carolina", joined: "1788-05-23" },
      SD: { name: "South Dakota", joined: "1889-11-02" },
      TN: { name: "Tennessee", joined: "1796-06-01" },
      TX: { name: "Texas", joined: "1845-12-29" },
      UT: { name: "Utah", joined: "1896-01-04" },
      VT: { name: "Vermont", joined: "1791-03-04" },
      VA: { name: "Virginia", joined: "1819-12-14" },
      WA: { name: "Washington", joined: "1889-11-11" },
      WV: { name: "West Virginia", joined: "1863-06-20" },
      WI: { name: "Wisconsin", joined: "1848-05-29" },
      WY: { name: "Wyoming", joined: "1890-07-10" },
    },
    *read(object) {
      for (const [fips, rec] of Object.entries(object)) yield { fips, ...rec };
    },
  };

  async function ensure_test_data() {
    const place = { db: "test-facts", store: "us-states" };
    const source = [...US_STATES.read(US_STATES.object)];
    const database = place.db;
    const store = place.store;
    /** @type IDBStoreDescription */
    const description = {
      keyPath: "fips",
      indexes: { joined: { keyPath: "joined" } },
    };
    const destination = { database, store, description };
    const writer = write(source, destination);
    for await (const status of writer)
      console.debug("loading test data", status);
  }

  const BASIC_READER_TESTS = [
    {
      spec: {},
      expect: `AK AL AR AZ CA CO CT DE FL GA HI IA ID IL IN KS KY LA MA MD ME MI MN MO MS MT NC ND NE NH NJ NM NV NY OH OK OR PA PR RI SC SD TN TX UT VA VT WA WI WV WY`,
    },
    {
      spec: { index: "joined" },
      expect:
        "NJ GA MA CT MD SC NH NY NC RI VT KY TN OH LA IN MS IL AL OR PA VA ME PR MO AR MI FL TX IA WI CA MN KS WV NV NE CO ND SD MT WA ID WY UT OK NM AZ AK DE HI",
    },
    {
      label: "bound",
      spec: { query: IDBKeyRange.bound("G", "N") },
      expect: `GA HI IA ID IL IN KS KY LA MA MD ME MI MN MO MS MT`,
    },
    {
      label: "index + bound",
      spec: { query: IDBKeyRange.lowerBound("1900"), index: "joined" },
      expect: `OK NM AZ AK DE HI`,
    },
    {
      label: "upper bound",
      spec: { query: IDBKeyRange.upperBound("B") },
      expect: "AK AL AR AZ",
    },
    {
      label: "larger bound",
      spec: { query: IDBKeyRange.upperBound("L") },
      expect: "AK AL AR AZ CA CO CT DE FL GA HI IA ID IL IN KS KY",
    },
    {
      label: "upper bound, early return",
      spec: { query: IDBKeyRange.upperBound("B") },
      limit: 3,
      expect: "AK AL AR",
    },
  ];

  async function test_read() {
    const read = IDB.read;
    const TEST_DB = { db: "test-facts", version: 1 };
    const US_STATES = { ...TEST_DB, store: "us-states" };

    await ensure_test_data();

    async function test(title, thunk, open) {
      console[open ? "group" : "groupCollapsed"](title);
      // prettier-ignore
      try { await thunk() }
      catch (error) { console.error(error) }
      finally { console.groupEnd() }
    }
    // duplicated
    async function* async_take(seq, n) {
      for await (const item of seq) {
        if (n-- <= 0) break;
        yield item;
      }
    }

    async function basic_reader_test(what) {
      const { expect, pause, ...rest } = what;
      const { skip, query, limit, spec } = rest;
      const label = `pause ${pause}; ${spec.label ?? JSON.stringify(rest)}`;
      if (skip) return console.info(`SKIPPED!!! ${label}`);
      return test(label, async () => {
        const reader = read({ ...US_STATES, ...spec });
        const results = [];
        const source = limit === undefined ? reader : async_take(reader, limit);
        for await (const item of source) {
          console.debug(`Test got`, item.fips);
          if (pause) await sleep(typeof pause === "number" ? pause : 1);
          results.push(item);
        }
        console.log(`results`, results);
        const fips = results.map(x => x.fips);
        assert_equals(fips, expect.split(" "));
        // const got = fips.join(" ");
        // assert?.(got === expect, `Expected ${expect}; got ${got}`);
      });
    }

    for (const spec of BASIC_READER_TESTS)
      for (const pause of [false, 1, 100])
        await basic_reader_test({ pause, ...spec });

    console.log("All tests complete!");
  }

  const ROUNDTRIP_CASES = [
    { given: [{ a: 11 }, { a: 42 }], description: { keyPath: "a" } },
    {
      skip: `the read/write doesn't support out-of-line keys`,
      given: Array.from([8, 6, 7, 5, 3, 0, 9], a => ({ a })),
      description: { autoIncrement: true },
    },
    {
      comment: `read back in order by primary key`,
      given: Array.from([8, 6, 7, 5, 3, 0, 9], a => ({ a })),
      description: { keyPath: "a" },
      expect: Array.from([0, 3, 5, 6, 7, 8, 9], a => ({ a })),
    },
    {
      comment: `read back in order by autoincrementing key`,
      given: Array.from([8, 6, 7, 5, 3, 0, 9], b => ({ b })),
      description: { keyPath: "a", autoIncrement: true },
      expect: Array.from([8, 6, 7, 5, 3, 0, 9], (b, i) => ({ a: i + 1, b })),
    },
  ];

  async function test_roundtrip() {
    const any_only = Object.values(ROUNDTRIP_CASES).some(_ => _.only);
    if (any_only) console.info("Running only tests marked with `only`!");
    for (const spec of ROUNDTRIP_CASES) {
      if (spec.skip) {
        console.warn(`Skipping because`, spec.skip);
        continue;
      }
      if (any_only && !spec.only) continue;
      for (const batch_size of [undefined, 4]) {
        const options = { ...spec.options, batch_size };
        await test_roundtrip_case({ ...spec, options });
      }
    }

    console.info("Roundtrip tests complete");
  }

  async function test_roundtrip_case(test_spec) {
    const read = IDB.read;
    const { given, description, options, expect } = test_spec;
    const say = console.debug.bind(console, "TEST");
    say?.("Roundtrip test ===========================");

    // === write
    const now = new Date();
    const ticks = now.getTime();
    const iso = now.toISOString();
    const date = iso.substring(0, 10);
    const database = `test-${date}`;
    const store = `test-${ticks}-${Math.round(Math.random() * 1000)}`;
    const destination = { database, store, description };
    let total;
    say?.(`create writer`);
    const writer = write(given, destination, options);
    say?.(`begin iteration`);
    for await (const result of writer) {
      say?.(`write result:`, result);
      total = result.total_written;
    }
    assert?.(total === given.length, `reported ${total} vs ${given.length}`);

    // === now read back and ensure that you got the original records
    const read_back = [];
    const spec = { db: database, store };
    say?.(`read back...`);
    for await (const result of read(spec)) {
      say?.(`read result!`, result);
      // if (result.data) read_back.push(...result.data);
      read_back.push(result);
    }
    // check the result
    assert_equals(read_back, expect ?? given, `Read result`);
    /*
    if (description.autoIncrement) {
      console.warn(`Errr, need more work to check autoincrement`);
    } else {
      assert?.(
        JSON.stringify(read_back) === JSON.stringify(expect ?? given),
        `not the same`
      );
    }
*/
  }

  async function test_database_with(db_name, store_name, description) {
    let db;
    try {
      db = await IDB.database_with(db_name, store_name, description);
      db.transaction(store_name);
      assert_cheap_equals(db.name, db_name, `db name`);
      const has_store = db.objectStoreNames.contains(store_name);
      assert(has_store, `${store_name} ∉ ${db_name}`);
      const txn = db.transaction(store_name);
      const store = txn.objectStore(store_name);
      // note this makes no *negative* assertions
      const diffs = [...IDB.diff_store(store, description)];
      assert_cheap_equals(diffs.length, 0, `diffs`, ...diffs);
      console.debug(
        `completed test for ${db_name}:${store_name} ${stringify(description)}`
      );
    } catch (error) {
      console.error(
        `error in test for ${db_name}:${store_name} ${stringify(description)}`,
        error
      );
    } finally {
      if (db) {
        console.debug("Test CLOSING db, version", db.version);
        db.close();
      }
    }
  }

  /** @type {readonly (readonly [string, IDBStoreDescription])[]} */
  const DATABASE_WITH_CASES = [
    ["x", { keyPath: "a", indexes: { b: { keyPath: "b" } } }],
    ["x", { keyPath: "a", indexes: { c: { keyPath: "c" } } }],
    ["x", { keyPath: "a", indexes: { b: { keyPath: "b" } } }],
    ["x", { keyPath: "a", indexes: { d: { keyPath: "d" } } }],

    ["x", { keyPath: "a", indexes: { e: { keyPath: "e" } } }],
    ["x", { keyPath: "a", indexes: { f: { keyPath: "f" } } }],
    ["x", { keyPath: "a", indexes: { g: { keyPath: "g" } } }],
    ["statements", { autoIncrement: true }],
    ["statements", { autoIncrement: true }],
    ["statements", { autoIncrement: true }],
    ["statements", { autoIncrement: true, indexes: { s: { keyPath: "s" } } }],
    [
      "statements",
      {
        autoIncrement: true,
        indexes: { s: { keyPath: "s" }, p: { keyPath: "p" } },
      },
    ],
    ["statements", { autoIncrement: true, indexes: { s: { keyPath: "s" } } }],
    [
      "statements",
      {
        autoIncrement: true,
        indexes: {
          s: { keyPath: "s" },
          p: { keyPath: "p" },
          o: { keyPath: "o" },
        },
      },
    ],
    // Note this doesn't remove the earlier indexes!
    [
      "statements",
      {
        autoIncrement: true,
        indexes: {
          spo: { keyPath: ["s", "p", "o"] },
          pos: { keyPath: ["p", "o", "s"] },
        },
      },
    ],
    ["nooks", { keyPath: "id" }],
    ["nooks", { keyPath: "id" }],
    ["crannies", { keyPath: ["a", "id"] }],
    ["nooks", { keyPath: "id" }],
    ["crannies", { keyPath: ["a", "id"] }],
    ["merps", { autoIncrement: true, indexes: { who: { keyPath: "what" } } }],
  ];

  async function database_with_tests() {
    const random = Math.round(Math.random() * 1e12);
    const db_name = `test-db-with-${random}`;
    for (const [store_name, description] of DATABASE_WITH_CASES) {
      test_database_with(db_name, store_name, description);
    }
    console.info(`Launched dem tests`);
  }

  async function self_tests() {
    // await database_with_tests();
    await test_roundtrip();
    await test_read();
  }

  Object.assign(IDB, { self_tests });
})();

const { assert } = console;

{
  const { fromEntries, entries } = Object;
  function map_object(o, f) {
    return fromEntries(Array.from(entries(o), ([k, v]) => [k, f(v, k, o)]));
  }
}

// yes, for ad-hoc date stuff.  copped from ISO8601
const pad = len => n =>
  (n < 0 ? "-" : "") + Math.abs(n).toString().padStart(len, "0");
const pad4 = pad(4);
const pad2 = pad(2);

// ==== process primitives

function make_subscribable() {
  const subscribers = new Set();
  function subscribe(subscriber) {
    subscribers.add(subscriber);
    function unsubscribe() {
      subscribers.delete(subscriber);
    }
    const subscription = { unsubscribe };
    return subscription;
  }

  // 1. this operation MUST be synchronous
  // 2. one subscriber MUST NOT be able to break others
  // 3. errors MUST NOT be lost
  // 4. we SHOULD NOT have to look at subscriber.catch unless there's an error
  // 5. error SHOULD appear (in debugger) where it occurs
  //
  // This achieves all but #5.  In chrome & FF, error appears at re-throw
  //
  // In fact these are incompatible.  The only way the error can be thrown at
  // the original location is if it is not caught at all.  But an uncaught
  // error will break any loop where it occurs.  If you define “synchronous”
  // to mean “by the next tick” rather than “by the end of this call”, then
  // you could do all `next` calls from microtasks (without try/catch when
  // there is no subscriber catch), but that would incur high overhead.
  function next(value) {
    let errors = null;
    for (const subscriber of subscribers) {
      try {
        subscriber.next(value);
      } catch (error) {
        if (typeof subscriber.catch === "function") subscriber.catch(error);
        else {
          if (errors === null) errors = [error];
          else errors.push(error);
        }
      }
    }
    if (errors !== null) {
      console.error("uhandled errors in broadcast", ...errors);
      // This throws all errors without resorting to console, but it's
      // actually worse .  The console at least links you to the source.
      /*
        for (const error of errors) {
          queueMicrotask(() => {
            throw error;
          });
        }
        */
    }
  }
  const public = { subscribe };
  return { public, next };
}

// https://github.com/tc39/proposal-regex-escaping/blob/main/polyfill.js
// seeAlso https://github.com/tc39/proposal-regex-escaping/issues/37
// This is safe for whole patterns.  The objections are to certain joins.
const RegExp_escape = s => String(s).replace(/[\\^$*+?.()|[\]{}]/g, "\\$&");
// The grouping isn't needed as long as all parts are simple
// const RegExp_wrap = pattern => `(?:${pattern})`;
const RegExp_wrap = pattern => pattern;
const RegExp_alternatives = patterns =>
  Array.from(patterns, RegExp_wrap).join("|");

// async helpers

const sleep = ms => new Promise(r => setTimeout(r, ms));

async function* async_map(seq, f) {
  for await (const item of seq) yield f(item);
}
async function* async_filter(seq, p) {
  for await (const item of seq) if (p(item)) yield item;
}
async function* async_take(seq, n) {
  for await (const item of seq) {
    if (n-- <= 0) break;
    yield item;
  }
}

async function* async_merge_roundrobin(seqs) {
  if (seqs.length === 0) return;
  if (seqs.length === 1) seqs[0];
  const gens = Array.from(seqs, seq => seq[Symbol.asyncIterator]());

  while (true) {
    for (let i = gens.length - 1; i >= 0; i--) {
      const gen = gens[i];
      const { done, value } = await gen.next();
      if (done) gens.splice(i, 1);
      else yield value;
    }
    if (gens.length === 0) break;
  }
  // stop remaining? i.e.
  for (const gen of gens) gen.return();
}

// === markup tools

const ident = x => x;
function* match_and_mark_text(rex, text, map, doc) {
  const f = map ?? ident;
  if (!rex) {
    if (text) yield f(text);
    return;
  }
  let pos = 0;
  if (!text) return;
  for (const match of text.matchAll(rex)) {
    const sep = text.substring(pos, match.index);
    if (sep) yield f(sep);
    const mark = doc.createElement("mark");
    const [needle] = match;
    mark.textContent = f(needle);
    yield mark;
    pos = match.index + needle.length;
  }
  if (pos === 0) yield f(text);
  else if (pos < text.length) {
    const tail = text.substring(pos);
    if (tail) yield f(tail);
  }
}
// helper for below, from ./dom-stepper.js
function preorder_next(/** @type Node */ my) {
  if (!my) return;
  if (my.firstChild) return my.firstChild;
  do if (my.nextSibling) return my.nextSibling;
  while ((my = my.parentNode));
}
// mark matches on an element's text in-place
// Won't hit the unhandled cases b/c *predicates* don't match across markup...
/** @param rex {RegExp} */
function match_and_mark_html(rex, /** @type Element */ element, doc) {
  if (!rex) return;
  // THIS is schlemel the painter. could use state since we go only forward
  function slice_containing(index) {
    let node = element;
    let pos = 0;
    while (node) {
      if (node instanceof Text) {
        const len = node.textContent.length;
        if (pos + len > index) return { node, offset: index - pos };
        pos += len;
      }
      node = preorder_next(node);
    }
  }
  for (const match of element.textContent.matchAll(rex)) {
    const start_index = match.index;
    const end_index = match.index + match.length - 1;
    const start = slice_containing(start_index);
    const end = slice_containing(end_index);
    assert?.(start, `mark html expected to find start`);
    assert?.(end, `mark html expected to find end`);
    if (start.node === end.node) {
      const node_text = start.node.textContent;
      const match_text = match[0];
      const mark = doc.createElement("mark");
      mark.textContent = match_text;
      const text_before = node_text.substring(0, start.offset);
      const text_after = node_text.substring(end.offset + match_text.length);
      start.node.replaceWith(text_before, mark, text_after);
    } else if (start.node.parentElement === end.node.parentElement) {
      // a single mark spanning the html
      throw new Error(`Not implemented: match including markup`);
    } else {
      // need multiple marks broken up over tree
      throw new Error(`Not implemented: oblique match`);
    }
  }
}

// === application stuff

const wiki_href = (wiki, lang) => `https://${lang}.wikipedia.org/wiki/${wiki}`;
const wiki_unslug = s => s.replace(/_/g, " ");

// kind of general time thing
function timestamp(date, lang, label) {
  const time = document.createElement("time");
  time.dateTime = date;
  time.textContent = label ?? ISO8601.label(date, lang);
  return time;
}

{
  // idea to support the dynamic alteration of precision in presented dates
  // trouble is, this doesn't consider how Intl would have arranged the parts
  const { parse, Date_from } = ISO8601;
  const UTC = { timeZone: "UTC" };
  // should memoize by language
  const DAY = new Intl.DateTimeFormat(undefined, { ...UTC, day: "numeric" });
  const MONTH = new Intl.DateTimeFormat(undefined, { ...UTC, month: "long" });
  const YEAR = new Intl.DateTimeFormat(undefined, { ...UTC, year: "numeric" });

  function structured_timestamp(date, lang) {
    const parsed = parse(date);
    if (!parsed) throw new Error(`structured timestamp got bad date: ${date}`);

    const stamp = document.createElement("time");
    const year = parsed.y && document.createElement("span");
    const month = parsed.mm && document.createElement("span");
    const day = parsed.dd && document.createElement("span");

    stamp.dateTime = date;

    const js_date = Date_from(date);
    if (year) {
      year.textContent = YEAR.format(js_date);
      stamp.append(year, " ");
    }
    if (month) {
      month.textContent = MONTH.format(js_date);
      stamp.append(month, " ");
    }
    if (day) {
      day.textContent = DAY.format(js_date);
      stamp.append(day, " ");
    }
    return stamp;
  }
}

function timestamp(date, lang, label) {
  const time = document.createElement("time");
  time.dateTime = date;
  time.textContent = label ?? ISO8601.label(date, lang);
  return time;
}

const KNOWN_CATALOG = {
  en_wiki_events: {
    locale: "en",
    source: "wiki-events-en.js",
    xml_name: "XML_events",
    store: {
      keyPath: "id",
      autoIncrement: true,
      indexes: {
        aboutTime: { keyPath: "aboutTime" },
        topics: { keyPath: "topics", multiEntry: true },
        refs: { keyPath: "refs", multiEntry: true },
      },
    },
    // This should be done lazily too, there's no need to make all at once
    from_dom(/** @type Element */ dom) {
      const eles = dom.querySelectorAll("events > event");
      return Array.from(eles, ele => {
        const p = ele.querySelector("p");
        console.assert(p, `No content element for item`, ele);
        return {
          aboutTime: ele.getAttribute("date"),
          topics: Array.from(ele.querySelectorAll("topic"), _ => _.textContent),
          refs: Array.from(p.querySelectorAll("a[wiki]"), _ =>
            _.getAttribute("wiki")
          ),
          html: p.innerHTML,
        };
      });
    },
  },
  en_wiki_persons: {
    locale: "en",
    source: "wiki-persons-en.js",
    xml_name: "XML_persons",
    store: {
      keyPath: "wiki",
      indexes: {
        born: { keyPath: "born", unique: false },
        died: { keyPath: "died", unique: false },
        // "words", "words", { multiEntry: true }
      },
    },
    from_dom(/** @type Element */ dom) {
      const eles = dom.querySelectorAll("persons > person");
      return Array.from(eles, ele => ({
        wiki: ele.getAttribute("who"),
        summary: ele.getAttribute("summary"),
        born: ele.getAttribute("born"),
        died: ele.getAttribute("died"),
      }));
    },
  },
};

const WORD_BOUNDARY = /[-a-z0-9']+/i; // hyphens, yea or nay?
const SLUG_WORD_BOUNDARY = /_|\W+/g;
const words_in = text => text.split(WORD_BOUNDARY);

const with_words = p => ({ ...p, words: words_in(`${p.wiki} ${p.summary}`) });

// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
// Concepts

// TODO: use binary search
function first_child_after(date, container) {
  for (const child of container.children) {
    const other = child.dataset.time;
    if (other && ISO8601.compare(other, date) > 0) return child;
  }
}

{
  const { next } = ISO8601;
  function timeframe_bounding_dates(date) {
    return date.includes("/") ? date.split("/", 2) : [date, next(date)];
  }
}

function make_time_backdrop() {
  const doc = document;
  const segments = new Map();
  const element = doc.createElement("div");
  const backdrop = doc.createElement("div");
  element.classList.add("time-fixed-item-collection");
  element.append(backdrop);
  backdrop.classList.add("time-backdrop");

  function ensure_segment_for(date) {
    if (segments.has(date)) return segments.get(date);
    const parsed = ISO8601.parse(date);
    assert?.(date && parsed, `add segment got bad date ${date}`);

    // Two elements are needed because we want to set the position of the segment
    // *and* support (transparent) positioning of the content within it
    const segment = doc.createElement("div");
    const inner = doc.createElement("div");
    segment.append(inner);
    segment.classList.add("time-backdrop-segment");
    set_time_item_style(date, segment.style);
    set_timeframe_style(date, inner.style);
    segment.setAttribute("data-time", date);
    const ref = first_child_after(date, backdrop);
    backdrop.insertBefore(segment, ref);
    segments.set(date, segment);
    return inner;
  }
  return { element, ensure_segment_for };
}

function make_time_vantage_base(context) {
  let _sub = null;
  let scale, offset, units, purview; // for deduplication
  const { timeframe, unit } = context;

  // soft constraints
  const MIN_PERIODS = 3;
  // const MAX_PERIODS = 5; PROVISIONAL

  const MULTIPLIER = YEAR_MULTIPLIERS[unit];
  const MIN_UNITS_TO_VIEW = MULTIPLIER * MIN_PERIODS;

  const purview_stream = make_subscribable();
  const point_stream = make_subscribable();

  // TODO needs to deal in major units
  function _vantage_timeframe_scalars_changed([orig_from, orig_to]) {
    const real_size = orig_to - orig_from;
    const from = orig_from / YEARS_PER_MAJOR_HACK;
    const to = orig_to / YEARS_PER_MAJOR_HACK;
    const timeframe_size = to - from;
    const proposed_scale = timeframe_size / MIN_UNITS_TO_VIEW;
    const new_scale = Math.min(1, proposed_scale);
    const new_offset = 0;

    const midpoint = (from + to) / 2; // aka origin
    const scale_was_clamped = proposed_scale > new_scale;
    const diameter = scale_was_clamped ? timeframe_size : MIN_UNITS_TO_VIEW;
    const new_units = diameter / MULTIPLIER;
    if (new_scale !== scale || new_offset !== offset || new_units !== units) {
      scale = new_scale;
      offset = new_offset;
      units = new_units;
      // point_stream.next({ scale, offset, units, from, to });
    }
    assert?.(offset >= 0 && offset < 1, `offset must be in [0, 1)`, offset);
    assert?.(scale > 0 && scale <= 1, `scale must be in (0, 1]`, scale);
    assert?.(offset + scale <= 1, `offset + scale must be ≤ 1`, offset, scale);

    const radius = diameter / 2;
    const purview_from = midpoint - radius * (1 + offset);
    const purview_to = midpoint + radius * (1 - offset);

    point_stream.next({
      scale: new_scale,
      offset: new_offset,
      units: new_units,
      from: purview_from * YEARS_PER_MAJOR_HACK,
      to: purview_to * YEARS_PER_MAJOR_HACK,
    });

    let from_date, to_date;
    // We don't *want* any more precision for unit >= Year
    if (ISO8601.compare_units(unit, "Year") >= 0) {
      from_date = pad4(Math.floor(purview_from));
      to_date = pad4(Math.ceil(purview_to));
    } else {
      // TODO: there's sometimes still a small gap for Day
      // coercing these to `unit` should cover the above, but it's more work...
      from_date = major_scalar_to_day(purview_from * YEARS_PER_MAJOR_HACK);
      // re `next`: end bound is *exclusive*; this is like `ceil` above
      to_date = ISO8601.next(
        major_scalar_to_day(purview_to * YEARS_PER_MAJOR_HACK)
      );
    }
    const new_purview = `${from_date}/${to_date}`;
    if (new_purview !== purview) {
      purview = new_purview;
      purview_stream.next(purview);
    }
  }

  function start() {
    _sub = timeframe.watch_major_scalars(_vantage_timeframe_scalars_changed);
  }
  function stop() {
    _sub?.unsubscribe();
  }
  return {
    start,
    stop,
    purview: purview_stream.public,
    point: point_stream.public,
  };
}

function link_lens_with_element(lens, /** @type {Element} */ element) {
  assert?.(element instanceof Element, `link lens bad element`, element);
  const { style } = element;
  let point_sub;
  function start() {
    assert?.(!point_sub, `link lens already started`);
    point_sub = lens.point.subscribe({
      next({ scale, offset, units, from, to }) {
        // BUT for days in month, we can allow up to 31 markers
        if (units > 12)
          element.setAttribute("data-time-lens-state", "overloaded");
        else element.removeAttribute("data-time-lens-state");
        // style.setProperty("--timeframe-lens-scale", scale);
        // style.setProperty("--timeframe-lens-offset", offset);
        style.setProperty("--timeframe-from-major", from);
        style.setProperty("--timeframe-to-major", to);
      },
    });
  }
  function stop() {
    point_sub?.unsubscribe();
    point_sub = null;
  }
  return { start, stop };
}

function make_time_vantage(context) {
  const { timeframe, unit, visit, element } = context;

  const base_vantage = make_time_vantage_base(context);
  const chart = time_chart_by({ unit, visit, element });

  base_vantage.purview.subscribe({ next: chart.set_bounds });
  const lens_link = link_lens_with_element(base_vantage, chart.element);

  function start() {
    base_vantage.start();
    chart.start();
    lens_link.start();
  }
  function stop() {
    base_vantage.stop();
    chart.stop();
    lens_link.stop();
  }
  return { ...base_vantage, start, stop, element: chart.element };
}

const YEAR_MULTIPLIERS = {
  Millennium: 1000,
  Century: 100,
  Decade: 10,
  Year: 1,
  Month: 1 / 12,
  Day: 1 / 365, // this is for a rule of thumb, so edge case is immaterial
};

// === “major” scale
// for units ∈ [Millennium, Day]
// year is indeed the base unit, but *all conversions should use these functions*
// decoupling scale from year *per se* lets us get precision where we need it
// Firefox in particular runs out of floating point space in css vars at Day level
const YEARS_PER_MAJOR_HACK = 100;
{
  const _YEARS_PER_MAJOR = YEARS_PER_MAJOR_HACK;

  const int = s => parseInt(s, 10);
  const { parse, coerce, compare_units, day_in_year, days_in_year } = ISO8601;

  const _year_scalar_from = date => {
    const parsed = parse(date);
    assert?.(parsed, `major_scalar_from got bad date`, date);
    let year_text = parsed.year;
    if (compare_units(parsed.unit, "Year") > 0)
      year_text = coerce(date, "Year");
    const year = int(year_text);
    switch (parsed.unit) {
      case "Month":
      case "Day": {
        const day = day_in_year(date);
        const days = days_in_year(date);
        const fraction = day / days;
        return year + fraction;
      }
    }
    return year;
  };
  function major_scalar_from(date) {
    return _year_scalar_from(date) * _YEARS_PER_MAJOR;
  }

  // convert from a major scalar value to the day that contains it
  // THIS should roundtrip dates with `major_scalar_from` but doesn't always

  // Scalar year values:
  // 1 means Jan 1, 1 AD
  // 1.5 means the middle of 1 AD (~July 1 AD)
  // 0 means Jan 1, 1 BC
  // 0.5 means... ~July, 1 BC
  // -1 means Jan 1, 2 BC
  // -0.5 means ~July, 2 BC
  function major_scalar_to_day(major) {
    // not really worried about the precision problem here, so divide now
    const years = major / _YEARS_PER_MAJOR;
    // this is also wrong for BC
    const year = Math.floor(years);
    // THIS is still untested for BC
    const signed_yyyy = pad4(year < 1 ? year + 1 : year);
    const fraction = Math.abs(years) % 1;
    const year_days = days_in_year(signed_yyyy);
    const day_ratio = 1 / year_days;
    // Because of precision loss we may get something like
    //     0.3945205479451488 / 0.0027397260273972603
    //     143.9999999999793
    //
    // so to get the date “containing” this point, I'd expect `floor` to be the
    // right operation here, in that case, the result I want is 144, not 143.
    //
    // Right but what was the problem?
    const days = Math.floor(fraction / day_ratio);
    // const days = Math.round(fraction / day_ratio);
    const js_date = new Date(Date.UTC(year, 0, days + 1));
    js_date.setUTCFullYear(year); // account for Date.UTC's quirk re 1900's
    const month_number = js_date.getUTCMonth() + 1;
    const day_number = js_date.getUTCDate();
    return `${signed_yyyy}-${pad2(month_number)}-${pad2(day_number)}`;
  }

  // find a date given 2 scalars
  // defines an equivalence class between the continuous major scale
  // and the ISO dates we recognize as timeframe
  // completely partitions the space such that the named dates fall fully into view
  const { compare, previous, next } = ISO8601;
  const FACTORS = Object.entries(YEAR_MULTIPLIERS);

  function major_scalars_to_timeframe(from, to) {
    const size = to - from;
    if (size === 0) return;
    const from_day = major_scalar_to_day(from);
    const to_day = major_scalar_to_day(to);

    // you must *fully* encompass at least one unit, or keep looking
    for (const [unit, factor] of FACTORS) {
      // REPEATED every time....
      const partial = factor * _YEARS_PER_MAJOR > size;
      if (partial && unit !== "Day") continue;
      const container = coerce(from_day, unit);
      const boundary = from === major_scalar_from(container);
      const first = boundary ? container : next(container);
      if (partial) return first;

      const ends_in = coerce(to_day, unit);
      if (unit === "Day" && first === ends_in) return first;
      const last = previous(ends_in);
      const comparison = compare(first, last);
      if (comparison > 0) continue;
      if (comparison === 0) return first;
      return `${first}/${last}`;
    }
  }
  function test_major_scalars_to_timeframe() {
    const CASES = [
      [[123400, 123400], undefined], // division by zero
      [[123400, 123400.001], "1234-01-01"], // day is bottom
      [[123400, 123500], "1234"], // end exclusive
      [[123400, 123510], "1234"], // end extension
      [[123400, 123599], "1234"], // up to
      [[123400, 123499], "1234-01/1234-11"], // span multiple
      [[123400, 123600], "1234/1235"], // but not including next
      [[123390, 123510], "1234"], // start & end extension
      [[123410, 123510], "1234-03/1235-01"], // refinement
      [[123410, 123510.00001], "1234-03/1235-01"], // distance, not precision
      [[123400, 123499.9], "1234-01/1234-11"],
      [[123410, 123600], "1235"], // largest contained unit
      [[123410, 123610], "1235"], // right?
      [[123390, 123610], "1234/1235"],
      [[123400, 124400], "1234/1243"],
      [[123000, 124000], "123"],
      [[123000, 124100], "123"],
      [[123000, 124140], "123"],
      [[123000, 124150], "123"],
      [[123000, 133000], "123/132"],
      [[123000.01, 133000], "124/132"],
      [[120000, 130000], "12"],
      [[120000, 130100], "12"],
      [[119900, 130100], "12"],
      [[120100, 130100], "121/129"],
      [[158289.67465753408, 158289.98287671216], "1582-11-25"],
    ];
    for (const [[from, to], expect] of CASES) {
      const got = major_scalars_to_timeframe(from, to);
      assert(got === expect, `[${from}, ${to})→${expect} got ${got}`);
    }
  }
  // test_major_scalars_to_timeframe();
}

function render_person_into(person, /** @type Element */ element, rex) {
  const lang = "en";
  const doc = element.ownerDocument;
  const { wiki, summary, born, died } = person;
  const { dataset, style } = element;

  element.setAttribute("data-wiki", wiki);

  const time = document.createElement("time");
  {
    const born_label = born && ISO8601.label(born);
    const died_label = died && ISO8601.label(died);
    const dates = ` (${born_label ?? ""}${died ? ` – ${died_label}` : ""})`;
    const iso = born && died ? `${born}/${died}` : born || died;
    time.classList.add("dates");
    time.dateTime = iso;
    time.textContent = dates;
  }

  const heading = doc.createElement("a");
  {
    assert?.(wiki, `who has no wiki?`, person);
    heading.href = wiki_href(wiki, lang);
    heading.classList.add("title");
    heading.append(...match_and_mark_text(rex, wiki, wiki_unslug, doc));
  }

  const comment = doc.createElement("span");
  {
    comment.classList.add("summary");
    comment.append(...match_and_mark_text(rex, summary, null, doc));
  }

  element.append(heading, " ", time, " ", comment);
  return element;
}

// intensional is too lossy; extensional is too big
function hack_en_wiki_iso_to_timeline(wiki) {
  const parsed = ISO8601.parse(wiki);
  if (!parsed) return;
  switch (parsed.unit) {
    case "Millennium": {
      // get ordinal etc
      return parsed.input;
    }
    case "Century": {
      // get ordinal etc
      return parsed.input;
    }
    // current catalog doesn't have any decade-specific content though
    case "Decade": {
      const decade = parseInt(parsed.decade, 10);
      const year = decade * 10;
      return `${year}s`;
    }
    case "Year": {
      const year = parseInt(parsed.year, 10);
      if (parsed.bc) return `${Math.abs(year) + 1}_BC`;
      if (year < 50) return `${year}_AD`;
      return `${year}`;
    }
  }
  throw new Error(`Bad request: ${parsed.unit}`);
}

function render_event_into(event, /** @type Element */ element, rex) {
  const lang = "en";
  const doc = element.ownerDocument;
  const { aboutTime, topics, refs, html } = event;

  const time = timestamp(aboutTime, lang);
  const description = doc.createElement("blockquote");
  description.innerHTML = html; // hotspot for sure
  // marking up the html is more complicated
  match_and_mark_html(rex, description, doc);
  for (const wiki_ele of description.querySelectorAll("[wiki]")) {
    const wiki = wiki_ele.getAttribute("wiki");
    wiki_ele.href = wiki_href(wiki, lang);
  }

  const citation = doc.createElement("cite");
  const source_link = doc.createElement("a");
  // event SHOULD have a source reference (aka provenance), but in the present
  // catalog (timeline articles) title is *nearly* redundant with this date.
  let date = aboutTime;
  // There are no timeline articles more specific than year
  if (ISO8601.compare_units(ISO8601.parse(date).unit, "Year") < 0)
    date = ISO8601.coerce(date, "Year");
  const wiki = hack_en_wiki_iso_to_timeline(date);
  const href = wiki_href(wiki, lang);
  const title = doc.createElement("q");
  title.textContent = wiki_unslug(wiki);
  source_link.href = href;
  source_link.append(`from `, title, ` in English Wikipedia`);
  citation.append(source_link);
  description.cite = href; // does this do anything anywhere?

  let topic_list;
  if (topics?.length > 0) {
    topic_list = doc.createElement("div");
    topic_list.classList.add("topics");
    for (const topic of topics) {
      const item = doc.createElement("span");
      item.setAttribute("data-topic", topic);
      item.textContent = topic;
      topic_list.append(item, " ");
    }
  }

  // We don't want to do this for backdrops because it's redundant
  // getting that preference here is another matter
  const include_time = false;
  if (include_time) element.append(time, " ");
  element.append(description, citation);
  if (topic_list) element.append(topic_list);
  return element;
}

const METHODS = {
  get_id: {
    en_wiki_persons: record => `wiki:${record.wiki}`,
    en_wiki_events: record => `en_wiki_event:${record.id}`,
  },
  // iterator would be more general but more expensive huh?
  date_fields: {
    en_wiki_persons: ["born", "died"],
    en_wiki_events: ["aboutTime"],
  },
  regex_test: {
    en_wiki_persons: (rex, person) =>
      rex.test(person.wiki) || rex.test(person.summary),
    en_wiki_events: (rex, event) =>
      event.topics?.some(it => rex.test(it)) || rex.test(event.html),
  },
  make_scans: {
    *en_wiki_persons({ date, ref }) {
      if (typeof ref === "string") {
        // supersedes date.  will have 0 or 1 results
        yield { query: IDBKeyRange.only(ref) };
      } else if (typeof date === "string") {
        // 2 dates: list births after the min and deaths before the end
        const [lower, upper] = timeframe_bounding_dates(date);
        const query = safe_date_bounds(lower, upper);
        yield { index: "born", query, dir: "next" };
        yield { index: "died", query, dir: "prev" };
      } else {
        yield { index: "born", dir: "next" };
        yield { index: "died", dir: "prev" };
      }
    },
    *en_wiki_events({ date, ref }) {
      // IGNORES date filter when a ref is present
      if (typeof ref === "string") {
        yield { index: "refs", query: IDBKeyRange.only(ref) };
      } else if (typeof date === "string") {
        const [from, to] = timeframe_bounding_dates(date);
        const query = safe_date_bounds(from, to);
        yield { index: "aboutTime", dir: "next", query };
      } else {
        yield { index: "aboutTime", dir: "next" };
      }
    },
  },
  render_into: {
    en_wiki_persons: render_person_into,
    en_wiki_events: render_event_into,
  },
};

// === global event intercepts

function make_time_markers_navigational() {
  const scope = document.body;
  const what = "click";
  function handler(/** @type Event */ event) {
    const { target } = event;
    const date =
      target.closest("time")?.dateTime ??
      target.closest(`button[data-time]`)?.dataset.time;
    if (!date) return;
    const parsed = ISO8601.parse(date);
    if (!parsed) return console.warn(`time had bad date ${date}`);
    const timeframe = target.closest(`[data-timeframe]`);
    if (!timeframe) return console.warn(`okay ${date} but no timeframe`);
    timeframe.setAttribute("data-timeframe", date);
    // TODO: ctrl+click extends vs replaces
    // actually that might be a better default
  }
  function start() {
    scope.addEventListener(what, handler);
  }
  function stop() {
    scope.removeEventListener(what, handler);
  }

  return { start, stop };
}

// pan & zoom
//
// 1. you never get stuck
// 2. movement is stable (all operations are monotonic)
// 3. you never violate the limits (size and bounds)
// all operations reach a fixpoint (not NaN)
// start < end

// pan left reaches a fixpoint where start = lo
// pan right reaches a fixpoint where end = hi
// zoom in reaches a fixpoint where size = min_size
// zoom out reaches a fixpoint where size = max_size (or start=lo & end=hi)
// after fixpoint, the inverse operation is still possible
// after zoom-in fixpoint, pans are still possible (usually)
// after pan fixpoints, zooms are still possible (usually)

// if your starting point violates the constraints, coerce with warning

// if max_size = Infinity (or greater than hi - lo) then you can have 3 sides at fixpoint
//   and only zoom-in still operable
{
  const { broader, next, distance, coerce, plus } = ISO8601;
  const { max, min, sign } = Math;

  // pan should NOT start turning into zoom when you hit bound
  // zoom should NOT start turning into pan when you hit bound -- but it does rn
  function make_constrain(bounds) {
    const max_size = bounds.max_size ?? Infinity;
    const min_size = bounds.min_size ?? 0;
    const lo = bounds.lo ?? -Infinity;
    const hi = bounds.hi ?? Infinity;
    if (min_size < 0) throw new Error(`Bad request: need non-negative min`);
    if (hi - lo < min_size) throw new Error(`Bad request: hi-lo<min_size`);
    // but you could want to say this
    // if (hi - lo < max_size) throw new Error(`Bad request: hi-lo<max_size`);
    const actual_max = min(hi - lo, max_size);
    const max2 = actual_max / 2;
    const min2 = min_size / 2;
    return function constrain(from, to) {
      const size = to - from;
      if (size > max_size) {
        const mid = (from + to) / 2;
        return [max(lo, mid - max2), min(hi, mid + max2)];
      }
      if (size < min_size) {
        const mid = (from + to) / 2;
        return [max(lo, mid - min2), min(hi, mid + min2)];
      }
      if (from < lo) return [lo, lo + min(size, actual_max)];
      if (to > hi) return [hi - min(size, actual_max), hi];
      return [from, to];
    };
  }
  function make_mover(α1, α2, bounds) {
    const constrain = make_constrain(bounds);
    return function move(from, to, r = 0.5) {
      const size = to - from;
      const δ1 = α1 * r * size;
      const δ2 = α2 * (1 - r) * size;
      return constrain(from + δ1, to + δ2);
    };
  }
  function make_movers(spec) {
    // I mean you could want different alpha for pan vs zoom
    const α = spec.alpha ?? 1 / 8;
    const { bounds } = spec;
    return {
      zoom_in: make_mover(α, -α, bounds),
      zoom_out: make_mover(-α, α, bounds),
      pan_left: make_mover(-α, -α, bounds),
      pan_right: make_mover(α, α, bounds),
    };
  }

  // just binds the 4 movement operations to timeframe state
  function make_timeframe_mover(context, spec) {
    const { timeframe } = context;
    if (!timeframe) throw new Error(`Bad request: expected timeframe`);
    const ops = make_movers({ bounds: spec });
    const frame = () => timeframe.get_major_scalars();
    function submit(target) {
      if (target) timeframe.set_major_scalars(...target);
    }
    const api = map_object(
      ops,
      op =>
        (...args) =>
          submit(op(...frame(), ...args))
    );
    const get = () => timeframe.get_major_scalars();
    // `get` is for tests only
    return { ...api, get };
  }
}

// pan & zoom tests
{
  // was seeing floating-point artifacting in Firefox CSS variables...

  // monotonic: until reaching bounds,
  // - every pan moves both bounds strictly in alpha direction
  // - pans always move by the same amount
  // - 2nd order: successive zooms always move by increasing/descreasing amounts

  // α=1 makes identity, where everything is a fixpoint.  ∀ x identity(x) = x

  // pan left/right are inverses
  // zoom left/right are inverses
  // EXCEPT where you hit bounds...
  // ∀ x,y: pan_left(x, y) = is_fixpoint(pan_left(x, y)) ∨ pan_right(pan_left(x, y))
  // ∀ x,y: zoom_in(x, y) = is_fixpoint(zoom_in(x, y)) ∨ zoom_out(zoom_in(x, y))

  // the only reason you need a mock is that real timeframe has an element
  // though it should make no difference
  function _make_mock_mover(spec, [from, to]) {
    let [_from, _to] = [from, to];
    const get_major_scalars = () => [_from, _to];
    const set_major_scalars = (from, to) => {
      _from = from;
      _to = to;
    };
    const phony = { get_major_scalars, set_major_scalars };
    return make_timeframe_mover({ timeframe: phony }, spec);
  }

  const _is_fixpoint = (input, output) =>
    input[0] === output[0] && input[1] === output[1];

  function _is_place_within_bounds(place, bounds) {
    const [start, end] = place;
    const { lo, hi, min_size, max_size } = bounds;
    const size = end - start;
    if (end >= start) throw new Error(`${start} ≮ ${end}`);
    return start >= lo && end <= hi && size >= min_size && size <= max_size;
  }

  // const g = major_scalar_from("1582-11-29"); // 1582909.589041096;
  // const h = major_scalar_from("1582-11-30"); // 1582912.3287671232;

  const _DELTAS = {
    zoom_in: [1, -1],
    zoom_out: [-1, 1],
    pan_left: [-1, -1],
    pan_right: [1, 1],
  };

  // Interpretation: run the op to fixpoint (or max #)
  // These should always be safe when there are finite bounds
  const _CASES = [
    [["zoom_in"], ["zoom_out"]],
    [["zoom_out"], ["zoom_in"]],
    [["zoom_out"], ["pan_left"], ["pan_right"]],
    [["zoom_out"], ["pan_right"], ["pan_left"]],
    [["zoom_in"], ["pan_left", 100], ["pan_right", 100]],
  ];
  {
    const HARD_MAX = 500;
    function run_universal_case(steps) {
      const lo = -10;
      const hi = 10;
      const min_size = 0.1;
      const max_size = 100;
      const mover = _make_mock_mover({ lo, hi, min_size, max_size }, [0, 1]);

      let count = 0;
      let op = null;
      function set_op(name) {
        op = name;
        count = 0;
      }

      for (const [op, limit = Infinity] of steps) {
        set_op(op);
        while (true) {
          const input = mover.get();
          if (!input) debugger;
          mover[op]();
          const output = mover.get();
          if (_is_fixpoint(input, output)) break;

          const deltas = _DELTAS[op];
          // if (Math.sign(output[0] - input[0]) !== deltas[0]) debugger;
          // if (Math.sign(output[1] - input[1]) !== deltas[1]) debugger;
          // a few of these started failing when I added `r`
          assert(Math.sign(output[0] - input[0]) === deltas[0], `Δ₀`);
          assert(Math.sign(output[1] - input[1]) === deltas[1], `Δ₁`);

          ++count;
          if (count >= limit) break;
          if (count >= HARD_MAX) throw new Error(`killed ${op} @ ${count}`);
        }
      }
    }
  }
  function run_universal_cases() {
    _CASES.forEach(run_universal_case);
  }

  run_universal_cases();
}

// pan & zoom UI
// see also https://stackoverflow.com/q/54621987
// see also https://web.dev/mobile-touchandmouse/
// see also https://danburzo.github.io/ok-zoomer/demos/logger
// see also https://firefox-source-docs.mozilla.org/performance/scroll-linked_effects.html
{
  function support_timeframe_pan_and_zoom(context) {
    const { timeframe } = context;
    if (!timeframe) throw new Error(`Bad request: timeframe expected`);
    /** @type Element */ const element = timeframe.element;

    const spec = {
      pan_alpha: 1 / 16,
      zoom_in_alpha: 7 / 8,
      zoom_out_alpha: 9 / 8,
      max_size: 5000 * YEARS_PER_MAJOR_HACK,
      min_size: (1 / 365) * YEARS_PER_MAJOR_HACK,
      hi: 5000 * YEARS_PER_MAJOR_HACK,
      lo: -9000 * YEARS_PER_MAJOR_HACK,
    };
    const mover = make_timeframe_mover({ timeframe }, spec);

    let _pointing = false;
    function wheel(/** @type {WheelEvent} */ event) {
      // Ctrl+wheel indicates a zoom action (see https://kenneth.io/post/detecting-multi-touch-trackpad-gestures-in-javascript)
      // but preventing default here raises other issues (and can't be done reliably anyway)
      // also my system doesn't appear to support pinch &c
      // https://community.frame.work/t/osx-style-trackpad-gestures-in-linux/4689
      // also I never saw this happen on any browser on Windows either
      if (event.ctrlKey) {
        console.debug("CTRL WHEEL");
        return;
      }
      if (!event.shiftKey) return;

      const { deltaX, deltaY } = event;

      if (deltaX && deltaY) return; // ignore diagonals

      const [from, to] = timeframe.get_major_scalars();

      // zoom ops may use this.  see also support_timeframe_pointer_echo
      const frame = element.getBoundingClientRect();
      const offset = event.clientX - frame.left;
      const r = offset / frame.width;

      // Ignore all magnitudes from the event, as they are highly variant
      let target;
      if (deltaY < 0) target = mover.zoom_in(r);
      else if (deltaY > 0) target = mover.zoom_out(r);
      else if (deltaX < 0) target = mover.pan_left();
      else if (deltaX > 0) target = mover.pan_right();

      // if (deltaX < 0) target = mover.pan_left();
      // else if (deltaX > 0) target = mover.pan_right();
      // 158290.95890410958
      // 158290.94178082192
      // 158290.92465753425
      // [158290.94178082192 - 158290.95890410958, 158290.92465753425 - 158290.94178082192]
      //
      if (!target) return;
      const [target_from, target_to] = target;
      timeframe.set_major_scalars(target_from, target_to);
    }

    function start() {
      element.addEventListener("wheel", wheel, { passive: true });
    }
    function stop() {
      element.removeEventListener("wheel", wheel);
    }
    return { start, stop, mover };
  }
}

{
  const doc = document;
  const lang = "en";
  const { coerce, distance, plus, label } = ISO8601;

  function support_timeframe_pointer_echo(context) {
    let _sub;
    let _from_day = null;
    let _timeframe_days = NaN;
    const { timeframe } = context;
    if (!timeframe) throw new Error(`Bad request: timeframe expected`);
    /** @type Element */ const element = timeframe.element;

    const selector = '[data-part="caption"]';
    const caption = element.querySelector(selector);
    if (!caption) throw new Error(`Invalid state: expected caption`);
    const pointer_label = doc.createElement("p");
    caption.insertAdjacentElement("afterend", pointer_label);

    function timeframe_changed(date) {
      const [from, to] = timeframe_bounding_dates(date);
      _from_day = coerce(from, "Day");
      const to_day = coerce(to, "Day");
      const days = distance(_from_day, to_day);
      _timeframe_days = days.n - 1; // because exclusive
    }

    function pointermove(/** @type {PointerEvent} */ event) {
      if (isNaN(_timeframe_days) || _from_day === null)
        return console.warn(`timeframe not set`);
      // unlike clientLeft, accounts for padding
      // BUT that could be affected by box-sizing, so, need getClientRects?
      const frame = element.getBoundingClientRect();
      const timeframe_left = frame.left;
      const timeframe_width = frame.width;
      const x = event.clientX;
      const offset = x - timeframe_left;
      const r = offset / timeframe_width;
      // interpolate r between timeframe from & to
      const days_from_left = Math.round(r * _timeframe_days);
      const pointed_date = plus(_from_day, days_from_left, "Day");
      const pointed_date_label = label(pointed_date, lang);
      pointer_label.textContent = `${pointed_date_label} r = ${r}`;
    }

    function start() {
      const date = timeframe.get();
      if (date) timeframe_changed(date);
      _sub = timeframe.watch(timeframe_changed);
      element.addEventListener("pointermove", pointermove, { passive: true });
    }
    function stop() {
      _sub?.unsubscribe();
      element.removeEventListener("pointermove", pointermove);
    }
    return { start, stop };
  }
}

function hijack_wikipedia_links(ele = document.body, lang = "en") {
  function handler(/** @type MouseEvent */ event) {
    // `event.buttons` is giving 0 always in FF??
    if (event.ctrlKey || event.altKey || event.metaKey || event.shiftKey)
      return;
    const tagged = event.target.closest("[data-wiki],[wiki]");
    if (tagged) {
      const wiki = tagged.dataset.wiki ?? tagged.getAttribute("wiki");
      if (wiki) {
        event.preventDefault();

        if (context_refs.has(wiki)) context_refs.remove(wiki);
        else context_refs.add(wiki);

        const show_inline_wiki_iframe = false;
        if (show_inline_wiki_iframe) {
          const iframe = document.createElement("iframe");
          const url = wiki_href(wiki, lang);
          iframe.src = url;
          tagged.append(iframe);
        }
        event.preventDefault();
      }
    }
  }
  function start() {
    ele.addEventListener("click", handler);
  }
  function stop() {
    ele.addEventListener("click", handler);
  }

  return { start, stop };
}

// a drain
function CONSUME_POWER_at_rate(source, interval) {
  let stopped = false;
  let timeout = NaN;
  let resolve = null;

  async function go() {
    for await (const item of source) {
      if (stopped) break;
      const promise = new Promise(_resolve => {
        resolve = () => {
          timeout = NaN;
          resolve = null;
          _resolve();
        };
        timeout = setTimeout(resolve, interval);
      });
      await promise;
      if (stopped) break;
    }
  }

  function start() {
    go();
  }
  function stop() {
    stopped = true;
    if (!isNaN(timeout)) {
      clearTimeout(timeout);
      resolve();
    }
  }
  return { start, stop };
}

{
  const { coerce, compare, distance, next } = ISO8601;
  const trace = false ? (...args) => console.debug(`visitor`, ...args) : null;

  // start inclusive / end exclusive
  function time_range_visitor(spec) {
    const { unit, visit } = spec;
    const capacity = spec.capacity ?? 20;

    return function* scan(initial_bounds) {
      let current, lower;
      let [min, max] = initial_bounds?.split("/", 2) ?? [];

      while (true) {
        const new_bounds = yield current;

        if (new_bounds) {
          if (!new_bounds.includes("/")) return "need range";
          const [lo, hi] = new_bounds.split("/", 2);
          if (hi && hi !== max) max = hi;
          if (lo && lo !== min) min = lo;
        }

        const old_lower = lower;
        lower = coerce(min, unit);
        const upper = coerce(max, unit);

        assert?.(lower, `no lower for ${unit} ${min}`);
        assert?.(upper, `no upper for ${unit} ${max}`);

        const targets = distance(lower, upper);
        assert?.(targets, `${unit} distance failed`, { lower, upper });
        if (targets.n > capacity) {
          trace?.(`overloaded @ ${targets.n}`);
          return `overloaded`;
        }
        trace?.(`capacity ${targets.n} okay`);

        current ??= lower;

        // if the starting point moves, start over
        if (lower !== old_lower) {
          trace?.(`reset`, { old_lower, lower, upper, targets });
          current = lower; // undefined would also work
          continue;
        }

        assert?.(current, `No current?!`);
        visit(current, spec);

        const next_time = next(current);
        if (next_time === undefined) {
          trace?.(`escaped domain at ${current}`);
          return "edge";
        }

        current = next_time;

        // comparing to max can get one too many
        // comparing to upper can get one too few
        // upper is just for checking overload; it is not meant to change the goal
        if (compare(current, max) >= 0) {
          trace?.(`passed goal ${max} at ${current}`);
          return "done";
        }
      }
    };
  }
}

// THIS should use CONSUME_POWER_at_rate (or some actuator) vs its own timer
function time_chart_by(spec) {
  const { unit } = spec;
  const DELAY = 50;

  let timeout;
  let _timeframe;
  let gen;

  const element = spec.element ?? document.createElement("div");

  // // spec.visit.bind(null, element);
  const visit = (...args) => spec.visit(element, ...args);
  const unvisit = spec.unvisit?.bind(null, element);
  const step = time_range_visitor({ ...spec, visit });

  const trace = spec?.trace ? (...x) => console.debug(unit, ...x) : null;

  element.removeAttribute("data-chart-state");

  const restart_generator = () => {
    if (gen) gen.return();
    gen = step(_timeframe)[Symbol.iterator]();
    gen.next();
  };

  function maybe_work() {
    timeout = NaN;
    if (!gen) restart_generator();
    const { done, value } = gen.next(_timeframe);
    if (done) {
      trace?.(`generator returned ${value}`);
      if (value === "overloaded")
        element.setAttribute("data-chart-state", "overloaded");
      gen = null;
      return;
    }
    if (value === undefined) return;
    trace?.(`yielded ${value}...`);
    element.removeAttribute("data-chart-state");
    timeout = setTimeout(maybe_work, DELAY);
  }

  function set_bounds(date) {
    trace?.(`set bounds`, date);
    assert?.(date, `${unit} set bounds without timeframe`);
    // DUPLICATES logic in timeframe_bounding_dates
    if (date.includes("/")) _timeframe = date;
    else _timeframe = `${date}/${ISO8601.next(date)}`;
    if (!timeout) maybe_work();
  }

  let _started = false;
  function start() {
    trace?.(`start`);
    assert?.(!_started, `${unit} start was called again, bad!`);
    _started = true;
  }
  function stop() {
    if (gen) gen.return("stopped");
    clearTimeout(timeout);
    timeout = NaN;
  }

  return { start, stop, element, set_bounds };
}

// timeframe implies existence of a process
// visit:assert :: unvisit:retract
// traversing from the bounds of the timeframe X → Y
// when the cost of completing that process would be < max
// spend up to max credits every time timeframe changes
//
// example state sequence
// ∅
// start({unit: "Year", max: 12})
// - do nothing yet, because still no bounds
// set_bounds("19")
// which means 20c so [1900, 2000)
// compute distance = 100
// ✗ out of budget, so die
// --- but, we were also communicating "overloaded"
// set_bounds("184")
// which means 1840's so [1840, 1850)
// compute distance = 10
// ✓ within budget so start at 1840
// set_bounds("1840/1846")
// compute distance = 6
// within budget
// lower bound hasn't changed
// upper bound has changed
// if it's still going, then it's still within range
// if not, it should be done anyway

const UNITS = ["Millennium", "Century", "Decade", "Year", "Month", "Day"];
const REVERSE_UNITS = [...UNITS].reverse();
function make_time_walkers(spec) {
  const { timeframe, maker } = spec;
  const units = spec.units ?? UNITS;
  const tag = spec?.tag ?? "nav";
  const element = document.createElement(tag);
  const walks = UNITS.map(unit => maker({ unit, ...spec }));
  timeframe.watch(date => {
    for (const walk of walks) walk.set_bounds(date);
  });
  element.append(...walks.map(walk => walk.element));
  function start() {
    for (const walk of walks) walk.start();
  }
  function stop() {
    for (const walk of walks) walk.stop();
  }
  return { start, stop, element };
}

// in order of preference:
// scale + offset <= 1
// # of periods between min_periods & max periods
// prefer max bound < 2100

function attribute_effects(element, specs) {
  let observer;
  function start() {
    observer = new MutationObserver(function _attribute_effect(changes) {
      for (const { attributeName: name } of changes) {
        const value = element.getAttribute(name);
        specs[name](value);
      }
    });
    const names = Object.keys(specs);
    observer.observe(element, { attributeFilter: names });
  }
  function stop() {
    observer?.disconnect();
  }
  return { start, stop };
}

function _set_time_style(is_item, date, style) {
  const [start, end] = timeframe_bounding_dates(date).map(major_scalar_from);
  style.setProperty(is_item ? "--major" : "--timeframe-from-major", start);
  style.setProperty(is_item ? "--major-to" : "--timeframe-to-major", end);
}
const set_timeframe_style = _set_time_style.bind(null, false);
const set_time_item_style = _set_time_style.bind(null, true);

function set_time_style_and_attributes(element, at, to) {
  if (at) {
    element.setAttribute("data-time", at);
    element.style.setProperty("--major", major_scalar_from(at));
  }
  if (to) {
    element.setAttribute("data-time-to", to);
    element.style.setProperty("--major-to", major_scalar_from(to));
  }
}

function make_timeframe(context) {
  const tag = context?.tag ?? "figure";
  // NO! this is a bad idea, you don't get what you ask for
  const pad = context.pad ?? 0;
  let _watcher;
  let _date = ""; // empty ≍ “any time”; must always equal data-timeframe value
  let _from = ""; // cache of _date start
  let _to = ""; // cache of _date end
  let _from_major = NaN; // scalar timeframe start; always = --timeframe-from-major (when not NaN)
  let _to_major = NaN; // scalar timeframe end; always = --timeframe-to-major (when not NaN)
  const { parse, previous, next } = ISO8601;

  const date_changes = make_subscribable();
  const scalar_changes = make_subscribable();

  const element = document.createElement(tag);
  const { style } = element;

  element.setAttribute("data-timeframe", _date);

  function _scalars_and_dates_are_consistent() {
    if (isNaN(_from_major)) return false;
    assert?.(!isNaN(_to_major), `should be same as from`);
    const scalar_date = major_scalars_to_timeframe(_from_major, _to_major);
    return _date === scalar_date;
  }
  // we don't know whether this is from our change or anyone else's
  function _attr_changed(date) {
    if (_date === date) return console.info("no-op @", date);
    _date = date;
    [_from, _to] = timeframe_bounding_dates(_date);
    if (!_scalars_and_dates_are_consistent()) _set_scalars_from_dates();
    date_changes.next(_date);
  }
  function _update_scalars(from, to) {
    const size = to - from;
    const padding = pad * size;
    _from_major = from - padding;
    _to_major = to + padding;
    style.setProperty("--timeframe-from-major", _from_major);
    style.setProperty("--timeframe-to-major", _to_major);
    scalar_changes.next([_from_major, _to_major]);
  }

  function _set_scalars_from_dates() {
    if (!_date) return console.warn(`missing date`);
    assert?.(_from && _to, `expected dates`);
    _update_scalars(major_scalar_from(_from), major_scalar_from(_to));
  }

  function get() {
    return _date;
  }
  function set(date) {
    if (date !== _date) element.setAttribute("data-timeframe", date); // slight hotspot, b/c mutation observers?
  }
  function get_major_scalars() {
    if (isNaN(_from_major)) _set_scalars_from_dates();
    return [_from_major, _to_major];
  }
  function set_major_scalars(from, to) {
    _update_scalars(from, to);
    if (!_scalars_and_dates_are_consistent()) {
      const date = major_scalars_to_timeframe(_from_major, _to_major);
      if (!date) return console.warn(`no date for these scalars!?`, from, to);
      set(date);
    }
  }
  function start() {
    _watcher = attribute_effects(element, { "data-timeframe": _attr_changed });
    _watcher.start();
  }
  function stop() {
    _watcher?.stop();
  }
  function watch(callback) {
    return date_changes.public.subscribe({ next: callback });
  }
  function watch_major_scalars(callback) {
    return scalar_changes.public.subscribe({ next: callback });
  }

  const date_api = { get, set, watch };
  const scalar_api = {
    get_major_scalars,
    set_major_scalars,
    watch_major_scalars,
  };
  return { start, stop, element, ...date_api, ...scalar_api };
}

function make_timeframe_labeler(context) {
  const { timeframe, element } = context;
  if (!timeframe) throw new Error(`Bad request: missing timeframe`);
  if (!element) throw new Error(`Bad request: missing element`);
  let _sub;
  const lang = context.lang ?? "en";
  const { label } = ISO8601;
  function _labeler_timeframe_changed(date) {
    // what we want is the date in the center
    const date_to_show = date; // approximate_timeframe(date) ?? date;
    const text = label(date_to_show, lang);
    element.textContent = `Timeframe: ${text}`;
  }
  function start() {
    _sub = timeframe.watch(_labeler_timeframe_changed);
    element.textContent = `Timeframe: any time`;
  }
  function stop() {
    _sub?.unsubscribe();
  }
  return { start, stop };
}

// lexical order ≠ extended ISO order. index will complain with
// DOMException: Data provided to an operation does not meet requirements
// this lets us get key ranges that should be equivalent
// though to cover the general case of spanning neg/pos you'd need cat
{
  const { compare } = ISO8601;
  function safe_date_bounds(lower, upper) {
    if (lower.startsWith("-") !== upper.startsWith("-"))
      console.warn(`Not implemented mixed bounds ${lower}/${upper}`);
    const in_order = compare(lower, upper) <= 0;
    const range = in_order ? [lower, upper] : [upper, lower];
    return IDBKeyRange.bound(...range, !in_order, in_order);
  }
}

function make_timeframe_article(context) {
  const { timeframe } = context;
  if (!timeframe) throw new Error(`Bad request: missing timeframe`);
  const element = document.createElement("article");
  const controls = document.createElement("div");
  const content = document.createElement("div");
  const button = document.createElement("button");
  element.classList.add("timeframe-article");
  controls.classList.add("article-controls");
  content.classList.add("article-content");
  button.classList.add("article-button");
  button.append("♥");
  controls.append(button);
  element.append(controls, content);
  function start() {}
  function stop() {}
  return { start, stop, element, content };
}

// ALMOST superseded by make_dynamic_listing.  still used in backdrops
// a bit more streamlined to call
function make_listing(context) {
  assert?.(context && typeof context === "object", `bad arg`, context);
  const doc = document;
  const lang = context.lang ?? "en";
  const { collection } = context;
  if (!collection) throw new Error("Listing what collection?");

  function get_method(name) {
    const method = context[name] ?? METHODS[name][collection];
    if (!method) throw new Error(`Teach me to ${name} a ${collection}`);
    return method;
  }

  const render_into = get_method("render_into");
  const make_scans = get_method("make_scans");
  const regex_test = get_method("regex_test");

  const { date, visit } = context;
  const budget = context.budget ?? 10;
  const terms = context.terms ?? (context.term && [context.term]);
  let pattern;
  if (terms) {
    if (terms.length === 1) {
      pattern = RegExp_escape(terms[0]);
    } else if (terms.length > 1) {
      pattern = RegExp_alternatives(Array.from(terms, RegExp_escape));
    }
  }
  // we seem to need separate instances of this for search & marking result
  const rex = pattern && new RegExp(pattern, "gi");
  const rex2 = pattern && new RegExp(pattern, "gi");

  const predicate = context.predicate ?? (rex && regex_test.bind(null, rex));

  const element = doc.createElement("article");
  const details = doc.createElement("details");
  const summary = doc.createElement("summary");
  const list = doc.createElement("ol");

  const phrases = terms?.map(term => `“${term}”`).join(" and ");
  const what = phrases && `${phrases} in ${collection}`;
  summary.textContent = `Listing: ${phrases ?? collection}`;
  details.open = true;
  details.classList.add("listing"); // presentational
  details.append(summary, list);
  element.append(details);

  if (date) {
    const time = timestamp(date, lang);
    summary.append(" in ", time);
  }

  const partials = make_scans?.(context) ?? [];
  const sources = Array.from(partials, spec => {
    return search_collection({ db: "cronwall", store: collection, ...spec });
  });

  const merged = async_merge_roundrobin(sources);
  const limited = async_take(merged, budget);
  const dom_sink = async_map(limited, list_item);
  const scan = CONSUME_POWER_at_rate(dom_sink, 100);

  function list_item(record) {
    const li = doc.createElement("li");
    render_into(record, li, rex2);
    visit?.(record);
    list.append(li);
  }

  function start() {
    scan.start();
  }
  function stop() {
    scan.stop();
  }
  return { context, start, stop, element };
}

//  When multiple terms are bound with a particular type, we can combine them
//  into a single predicate.  Sort of equivalent to merging with separate
//  predicates.

// `term` is a shorhand notation; `terms` is the normal form
/**
 * @param search {{term?: string, terms?: string[]}}
 */
function collection_topic_search_to_predicate(search) {
  const { collection, term, ref, date, predicate } = search;
  const terms = search.terms ?? (term && [term]);

  if (typeof predicate === "function") return predicate; // kind of a hack

  // HERE we'd like a way to ignore markup
  let pattern;
  if (terms) {
    if (terms.length === 1) {
      pattern = RegExp_escape(terms[0]);
    } else if (terms.length > 1) {
      pattern = RegExp_alternatives(Array.from(terms, RegExp_escape));
    }
  }
  if (pattern) {
    // THIS should be a property of the type, if anything...
    const regex_test = METHODS.regex_test[collection];
    assert?.(typeof regex_test === "function", `terms without test`);
    const rex = new RegExp(pattern, "gi");
    return regex_test?.bind(null, rex);
  } else if (collection === "en_wiki_events" && ref && date) {
    // VERY SPECIAL case because we can't do date and ref at the same time
    // so we assume ref is already handled and do date here
    // (I'm assuming we get more out of the ref index than we would the date one)
    const [from, to] = timeframe_bounding_dates(date);
    const { compare } = ISO8601;
    return record => {
      const date = record.aboutTime;
      return date && compare(date, from) >= 0 && compare(date, to) < 0;
    };
  }
}

// creates a filtered scan over a catalog AND extends results with metadata
// parameters the same as read?
/** @param spec {typeof METHODS["make_scans"][keyof typeof METHODS["make_scans"]]} */
{
  const { defineProperty } = Object;
  function search_collection(query) {
    const { db, collection, ...rest } = query;
    const spec = { db, store: collection, ...rest };
    const reader = IDB.read(spec);
    const predicate = collection_topic_search_to_predicate(query);
    const filtered = predicate ? async_filter(reader, predicate) : reader;
    const tagged = async_map(filtered, record => {
      defineProperty(record, TYPE, { value: collection });
      return record;
    });
    return tagged;
  }
}

function _collection_search_sources(collection_query) {
  const { collection } = collection_query;
  assert?.(collection, `make_collection_search expected collection`);

  // THIS should be a property of the type, if anything...
  const make_scans = METHODS.make_scans[collection];
  assert?.(typeof make_scans === "function", `make_scans ain't a function`);

  const partials = make_scans?.(collection_query) ?? [];
  return Array.from(partials, clauses => {
    return search_collection({ ...collection_query, ...clauses });
  });
}

function make_collection_search(collection_query) {
  const sources = _collection_search_sources(collection_query);
  return async_merge_roundrobin(sources);
}

const TYPE = Symbol();

// generic
function render_into(...args) {
  const [record, element, highlight] = args;

  const local = record.render_into;
  if (typeof local === "function") return local(...args);

  const type = record[TYPE];
  const global = type && METHODS["render_into"][type];
  if (typeof global === "function") return global(...args);

  throw new Error(`generic render into not implemented`);
}

// HACK: stubs for generics
const get_start_date = record => {
  // prettier-ignore
  switch (record[TYPE]) {
      case "en_wiki_persons": return record.born;
      case "en_wiki_events": return record.aboutTime;
    }
};
const get_end_date = record => {
  // prettier-ignore
  switch (record[TYPE]) {
      case "en_wiki_persons": return record.died;
      case "en_wiki_events": return ISO8601.next(record.aboutTime);
    }
};
const get_date = record => record.aboutTime;
// well, it's not really a range...
const get_is_range = record => record[TYPE] === "en_wiki_persons"; // || record[TYPE] === "en_wiki_events";

function make_dynamic_listing(context) {
  let sub, scan;
  let cleanup = NaN; // timeout for cleaning up old list items
  const { timeframe, make_source, mark } = context;
  if (!timeframe) throw new Error(`Bad request: missing timeframe`);

  const doc = document;
  const lang = context.lang ?? "en";
  const budget = context.budget ?? 10; // should be dynamic though

  function do_cleanup() {
    const exit = list.querySelectorAll(`:scope > [data-member-state="exit"]`);
    for (const element of exit) element.remove();
    cleanup = NaN;
  }
  function queue_cleanup() {
    setTimeout(do_cleanup, 5000);
  }
  function bump_cleanup() {
    if (!isNaN(cleanup)) {
      clearTimeout(cleanup);
      queue_cleanup();
    }
  }

  // all of this is just so that we can provide a pattern for render to highlight
  let rex;
  if (mark) {
    // duplicates normalization
    const terms = mark.terms ?? (mark.term && [mark.term]);
    let pattern;
    if (terms) {
      if (terms.length === 1) {
        pattern = RegExp_escape(terms[0]);
      } else if (terms.length > 1) {
        pattern = RegExp_alternatives(Array.from(terms, RegExp_escape));
      }
      // we seem to need separate instances of this for search & marking result
      if (pattern) rex = new RegExp(pattern, "gi");
    }
  }

  const element = doc.createElement("article");
  const details = doc.createElement("details");
  const summary = doc.createElement("summary");
  const list = doc.createElement("ol");
  const trail = doc.createElement("span");
  const description = doc.createElement("span");
  element.classList.add("listing", "dynamic-listing", "resource-listing");
  element.classList.add("with-hover-item");
  element.append(details);
  list.classList.add("with-time-markers");
  details.open = true;
  details.append(summary, list);
  trail.classList.add("time-location-trail");
  summary.append(`Listing`, description, trail);

  // describe the listing source
  {
    const { terms, ref, refs } = context;
    const phrases =
      terms?.map(term => `“${term}”`).join(" and ") ??
      (ref ? [ref] : refs)?.map(ref => `${ref}`).join(" and ");
    description.textContent = phrases ?? " something ";
  }

  function list_item(record) {
    bump_cleanup();
    if (record === undefined) return;
    // these should either be generics or prototype methods
    const collection = record[TYPE];
    const get_id = METHODS.get_id[collection];
    const render_into = METHODS.render_into[collection];
    // deduplicating via dom lookup
    const id = get_id(record);
    const existing = list.querySelector(`:scope > [data-resource="${id}"]`);
    if (existing) {
      existing.setAttribute("data-member-state", "enter");
      return;
    }

    const item = doc.createElement("li");
    const resource = document.createElement("div");
    const article = document.createElement("article");
    const special = doc.createElement("div");
    const general = doc.createElement("div");

    let from, to, date;
    {
      const is_range = get_is_range(record);
      if (is_range) {
        from = get_start_date(record);
        to = get_end_date(record);
        set_time_style_and_attributes(item, from, to);
      } else {
        date = get_date(record);
        if (date) {
          const end = ISO8601.next(date);
          item.setAttribute("data-time", date);
          item.setAttribute("data-time-to", end); // well... not really but I want to mark it this way
          item.style.setProperty("--major", major_scalar_from(date));
          item.style.setProperty("--major-to", major_scalar_from(end));
        }
      }
    }

    resource.classList.add("resource-finding");
    article.classList.add("resource-article", "mark-time--margin");
    item.classList.add("item");
    item.setAttribute("data-member-state", "enter");
    item.setAttribute("data-resource", id);
    special.classList.add("resource-finding-special");
    special.append(article);
    general.classList.add("resource-finding-general");
    render_into(record, article, rex);
    {
      const marker = doc.createElement("div");
      marker.classList.add("time-block-marker", "mark-time--margin");
      if (date) {
        const label = ISO8601.label(date, lang);
        marker.textContent = label;
      } else if (from && to) {
        const from_label = ISO8601.label(from, lang);
        const to_label = ISO8601.label(to, lang);
        marker.textContent = `${from_label} – ${to_label} `;
      } else if (from) {
        marker.textContent = `${ISO8601.label(from, lang)} –`;
      } else if (to) {
        marker.textContent = `– ${ISO8601.label(to, lang)}`;
      } else {
        marker.textContent = `date unknown`;
      }
      general.append(marker);
    }
    resource.append(general, special);
    item.append(resource);
    list.append(item);
  }

  function _set_date(date) {
    if (date) {
      const time = doc.createElement("time");
      const label = ISO8601.label(date, lang);
      time.dateTime = date;
      time.textContent = ` in ${label}`;
      trail.append(time);
    }
    // support exit transition
    // list.innerHTML = "";
    queue_cleanup();
    for (const child of list.children) {
      child.setAttribute("data-member-state", "exit");
    }
    scan?.stop();

    const source = make_source({ date });
    const limited = async_take(source, budget);
    const dom_sink = async_map(limited, list_item);
    scan = CONSUME_POWER_at_rate(dom_sink, 100);
    scan.start();
  }

  function start() {
    sub = timeframe.watch(_set_date);
  }
  function stop() {
    sub?.unsubscribe();
    scan?.stop();
  }
  return { start, stop, element };
}

// describe scan
/*
      const phrases = terms?.map(term => `“${term}”`).join(" and ");
      const what = phrases && `${phrases} in ${collection}`;
      */

// or search?
function make_time_scan(context) {
  let scan;
  assert?.(context && typeof context === "object", `bad arg`, context);
  const { collection, visit } = context;
  if (!collection) throw new Error("Listing what collection?");
  if (typeof visit !== "function") throw new Error("Scan needs f'n visitor");

  function get_method(name) {
    const method = context[name] ?? METHODS[name][collection];
    if (!method) throw new Error(`Teach me to ${name} a ${collection}`);
    return method;
  }

  const make_scans = get_method("make_scans");
  const budget = context.budget ?? 10;
  const rate = context.rate ?? 100;

  const partials = make_scans?.(context) ?? [];
  const sources = Array.from(partials, spec => {
    return search_collection({ ...context, ...spec });
  });
  const merged = async_merge_roundrobin(sources);
  const limited = async_take(merged, budget); // SHOULD be caller's job
  const sink = async_map(limited, visit);

  function start() {
    scan = CONSUME_POWER_at_rate(sink, rate); // SHOULD be caller's job
    scan.start();
  }
  function stop() {
    scan?.stop();
  }
  return { start, stop };
}

// =====================================================
// text in the prompt area is interpreted either
// - non-monotonically: you're just “thinking” it
// - monotonically: you officially said it
{
  const WORD_BOUNDARY = /\W+/g;
  const lang = "en";
  // scrap code for removing things recognized as dates from prompt
  function remove_match(text, match) {
    const end = match.index + match[0].length;
    if (match.index === 0) return text.substring(end);
    const before = text.substring(0, match.index);
    if (end < text.length) return before;
    const after = text.substring();
    return `${before}${after}`;
  }
  let text = "";
  const recognized = ISO8601.recognize(text, lang);
  if (recognized) {
    const date = recognized.value;
    // pin(date);
    // text = remove_match(text, recognized.match);
  }
}

// =========================================

{
  const lang = "en";
  const { compare, coerce, distance, label } = ISO8601;

  const classify_date_and_range = (date, begin, end) => {
    if (!date) return "unknown";
    if (compare(date, begin) < 0) return "before";
    if (compare(date, end) > 0) return "after";
    return "during";
  };

  const years_apart = (date1, date2) => {
    const year1 = coerce(date1, "Year");
    const year2 = coerce(date2, "Year");
    const delta = distance(year1, year2);
    return Math.round(delta.n);
  };

  // classify relation, for alignment
  function describe_relation(born, died, from, to) {
    if (!born && !died) return `dates unknown`;
    const born_rel = classify_date_and_range(born, from, to);
    const died_rel = classify_date_and_range(died, from, to);
    let start_label, end_label;
    let years = NaN;
    if (died_rel === "before") {
      years = years_apart(died, from);
      start_label = `died ${years} years earlier`;
    } else if (born_rel === "after") {
      years = years_apart(to, born);
      end_label = `born ${years} years later`;
    } else {
      if (born) {
        if (born_rel === "before") {
          years = years_apart(born, from);
          start_label = `≈${years} y/o`;
        } else if (born_rel === "during") {
          start_label = `born ${label(born, lang)}`;
        } else assert?.(false, "unexpected born", { born, died, from, to });
      }
      if (died) {
        if (died_rel === "after") {
          years = years_apart(born, to);
          end_label = `≈${years} y/o`;
        } else if (died_rel === "during") {
          // what about at age...
          end_label = `died ${label(died, lang)}`;
        } else assert?.(false, "unexpected died", { born, died, from, to });
      }
    }

    const ret = { born_rel, died_rel, years };
    if (start_label) ret.start_label = start_label;
    if (end_label) ret.end_label = end_label;
    return ret;
  }
}

{
  const { broader, compare_units } = ISO8601;
  function approximate_timeframe(date) {
    const [from, to] = timeframe_bounding_dates(date);
    if (from.unit !== to.unit) {
      const narrower = compare_units(from.unit, to.unit);
    }
    // if it is a singular unit already...
    return broader(broader(date));
  }
}

{
  const doc = document;
  function make_time_range_block() {
    const block = doc.createElement("div");
    const start = doc.createElement("span");
    const end = doc.createElement("span");
    block.classList.add("time-range-block");
    block.append(start, end);
    start.setAttribute("data-affinity", "start");
    end.setAttribute("data-affinity", "end");
    return { block, start, end };
  }
}

{
  const { distance, parse, compare_units, label_units } = ISO8601;
  function time_comparative_label_en(date1, date2) {
    // compare at precision. need labeling of units with plural
    const parsed1 = parse(date1);
    if (!parsed1) throw new Error(`comparative label got bad date ${date1}`);
    const parsed2 = parse(date2);
    if (!parsed2) throw new Error(`comparative label got bad date ${date2}`);
    const result = compare_units(parsed1.unit, parsed2.unit);
    const greater_unit = result < 0 ? asdf : asdf;
    const coerced1 = coerce(greater_unit);
    const dist = distance(coerced1, coerced2);
    const z = dist.n;
    const count = Math.floor(Math.abs(z));
    const units = label_unit(unit, "en");
    const relation = z < 0 ? "earlier" : "later";
    return `${count} ${units} ${relation}`;
  }
}

function make_marker_block(...classes) {
  const block = document.createElement("div");
  const marker = document.createElement("span");
  block.append(marker);
  block.classList.add("time-marker-block");
  marker.classList.add(...classes);
  return { block, marker };
}

// dynamic marker representing a person's birth & death
// depicts and describes in terms of current timeframe
function make_person_lifetime_mark(context) {
  const { timeframe, wiki } = context;
  if (!timeframe) throw new Error(`Bad request: timeframe expected`);
  if (!wiki) throw new Error(`Bad request: (wiki) ref expected`);
  let _stopped = false;
  let _sub_scalars = null;
  let _sub_dates = null;
  let _born_major = NaN;
  let _died_major = NaN;
  let _person = null;
  let person_found;
  const person_promise = new Promise(r => (person_found = r));

  const doc = document;
  const lang = "en";
  const { coerce, compare, distance } = ISO8601;

  const element = doc.createElement("div");
  const label = make_marker_block("time-block-label");
  const marker = make_marker_block("time-block-marker");
  const dates = make_time_range_block();
  element.classList.add("person-lifetime");
  label.block.classList.add("label-block");
  label.marker.textContent = wiki_unslug(wiki);
  marker.block.classList.add("marker-block");
  dates.block.classList.add("dates-block");
  element.append(label.block, dates.block, marker.block);

  function classify_scalar_and_range(point, [from, to]) {
    if (typeof point !== "number" || isNaN(point)) return "unknown";
    if (point < from) return "before";
    if (point >= to) return "after";
    return "during";
  }

  function _scalars_changed(frame) {
    if (isNaN(_born_major)) {
      console.warn(`got scalar change before person`, wiki);
      return;
    }
    const born_rel = classify_scalar_and_range(_born_major, frame);
    const died_rel = classify_scalar_and_range(_died_major, frame);
    element.setAttribute("data-born-rel", born_rel);
    element.setAttribute("data-died-rel", died_rel);
  }

  function _dates_changed(date) {
    if (!ISO8601.parse(date)) return console.warn(`bad date`, date);
    if (_person === null) {
      console.warn(`got date change (${date}) before getting person`, wiki);
      return;
    }
    const { born, died } = _person;
    // do specific message updates, maybe?
  }

  async function start() {
    const person = await get_person(wiki);
    if (_stopped) return;
    if (!person) return console.warn(`no such person?`, wiki);
    person_found(person);
    _person = person;

    const { born, died, summary } = person;
    if (born) _born_major = major_scalar_from(born);
    if (died) _died_major = major_scalar_from(died);
    set_time_style_and_attributes(element, born, died);

    if (summary) {
      const tagline = document.createElement("span");
      tagline.classList.add("person-summary");
      tagline.append(summary);
      label.marker.append(", ", tagline);
    }

    _sub_scalars = timeframe.watch_major_scalars(_scalars_changed);
    // unused
    // _sub_dates = timeframe.watch(_dates_changed);
    // Also initialize to current values... watchers don't do this...
    {
      const scalars = timeframe.get_major_scalars();
      if (scalars) _scalars_changed(scalars);
      const date = timeframe.get();
      if (date) _dates_changed(date);
    }

    // fixed labels for born and died dates
    {
      const start_timestamp = born && structured_timestamp(born, lang);
      const end_timestamp = died && structured_timestamp(died, lang);

      dates.start.append(start_timestamp);
      dates.end.append(end_timestamp);
    }
  }
  function stop() {
    _stopped = true;
    _sub_dates?.unsubscribe();
    _sub_scalars?.unsubscribe();
  }
  return { start, stop, element };
}

// minimal same as above but for events
function make_event_mark(context) {
  const { timeframe, id } = context;
  if (!timeframe) throw new Error(`Bad request: timeframe expected`);
  if (!id) throw new Error(`Bad request: (event) id expected`);
  let _stopped = false;
  let _sub_scalars = null;
  let _sub_dates = null;
  let _aboutTime_major = NaN;
  let _event = null;
  let event_found;
  const record_promise = new Promise(r => (event_found = r));

  const doc = document;
  const lang = "en";
  const { coerce, compare, distance } = ISO8601;

  const element = doc.createElement("div");
  const label = make_marker_block("time-block-label");
  const marker = make_marker_block("time-block-marker");
  const dates = make_time_range_block();
  element.classList.add("person-lifetime");
  label.block.classList.add("label-block");
  label.marker.textContent = `event ${id}`;
  marker.block.classList.add("marker-block");
  dates.block.classList.add("dates-block");
  element.append(label.block, dates.block, marker.block);

  function classify_scalar_and_range(point, [from, to]) {
    if (typeof point !== "number" || isNaN(point)) return "unknown";
    if (point < from) return "before";
    if (point >= to) return "after";
    return "during";
  }

  function _scalars_changed(frame) {
    if (isNaN(_aboutTime_major)) {
      console.warn(`got scalar change before event`, id);
      return;
    }
    const aboutTime_rel = classify_scalar_and_range(_aboutTime_major, frame);
    element.setAttribute("data-about-time-rel", aboutTime_rel);
  }

  function _dates_changed(date) {
    if (!ISO8601.parse(date)) return console.warn(`bad date`, date);
    if (_event === null) {
      console.warn(`got date change (${date}) before getting event`, id);
      return;
    }
    const { aboutTime } = _event;
    // do specific message updates, maybe?
  }

  async function start() {
    const event = await get_event(id);
    if (_stopped) return;
    if (!event) return console.warn(`no such event?`, id);
    event_found(event);
    _event = event;

    render_event_into(event, element);
    return;
    const { aboutTime, description } = event;
    if (aboutTime) _aboutTime_major = major_scalar_from(aboutTime);
    set_time_style_and_attributes(element, aboutTime);

    if (description) {
      const tagline = document.createElement("span");
      tagline.classList.add("event-description");
      tagline.append(description);
      label.marker.append(", ", tagline);
    }

    _sub_scalars = timeframe.watch_major_scalars(_scalars_changed);
    // unused
    // _sub_dates = timeframe.watch(_dates_changed);
    // Also initialize to current values... watchers don't do this...
    {
      const scalars = timeframe.get_major_scalars();
      if (scalars) _scalars_changed(scalars);
      const date = timeframe.get();
      if (date) _dates_changed(date);
    }

    // fixed label for event date
    {
      const start_timestamp =
        aboutTime && structured_timestamp(aboutTime, lang);
      dates.start.append(start_timestamp);
    }
  }
  function stop() {
    _stopped = true;
    _sub_dates?.unsubscribe();
    _sub_scalars?.unsubscribe();
  }
  return { start, stop, element };
}

function make_person_at_age_listing(context) {
  const { wiki } = context;
  if (!wiki) throw new Error(`Bad request: (wiki) ref expected`);
  let event_listing;
  let person_found;
  const person_promise = new Promise(r => (person_found = r));

  const doc = document;
  const lang = "en";
  const { coerce, compare, distance } = ISO8601;

  const element = doc.createElement("article");
  const header = doc.createElement("header");
  const body = doc.createElement("div");

  element.classList.add("person-at-age-listing");

  const label = wiki_unslug(wiki);

  function list_events_with_age(person) {
    const { born, died } = person;
    const known_lifespan = born && died;
    const birth_year = parseFloat(ISO8601.coerce(born, "Year"));
    const { compare, coerce } = ISO8601;

    event_listing = make_listing({
      collection: "en_wiki_events",
      index: "refs",
      budget: 30,
      // ref: wiki, // with this, other default scans may be included
      *make_scans() {
        yield { index: "refs", query: IDBKeyRange.only(wiki) };
      },
      render_into(record, element, rex) {
        const date = record.aboutTime;
        set_time_style_and_attributes(element, date);

        const wrapper = doc.createElement("div");
        wrapper.setAttribute("data-part", "event");
        const age = doc.createElement("i");
        age.setAttribute("data-part", "age");
        if (
          known_lifespan &&
          compare(date, born) >= 0 &&
          compare(date, died) <= 0
        ) {
          // WRONG crossing BC
          const event_year = parseFloat(coerce(date, "Year"));
          const years = Math.floor(event_year - birth_year);
          const doc = element.ownerDocument ?? document;
          // could also say years after death...
          // ALSO, when event and birthdate units are Day, we don't need “about”
          const desc =
            years === 0
              ? `the year ${wiki_unslug(wiki)} was born `
              : `at about age ${years} `;
          age.textContent = desc;
        }
        element.append(age, wrapper);
        return METHODS.render_into.en_wiki_events(record, wrapper, rex);
      },
    });
    event_listing.start();
    event_listing.element.classList.add("at-age-listing");
    element.append(event_listing.element);
  }

  async function start() {
    const person = await get_person(wiki);
    person_found(person);
    if (!person) return console.warn(`no such person?`, wiki);
    const { born, died } = person;
    // set this on head only; body may have other time items
    set_time_style_and_attributes(header, born, died);
    list_events_with_age(person);
  }
  function stop() {
    event_listing?.stop();
  }

  return { start, stop, element };
}

// ===========================================

{
  const doc = document;

  // only tells you the script is *loaded*, not executed!
  function load_script(src) {
    return new Promise((resolve, reject) => {
      const script = document.createElement("script");
      script.onload = resolve;
      script.onerror = reject;
      script.src = src;
      doc.head.append(script);
    });
  }

  function get_definition_sync(name) {
    return globalThis[name];
  }
  function defined_now(name) {
    return name in globalThis;
  }
  let timeout;
  function when_defined(name, interval = 1000) {
    if (defined_now(name)) return get_definition_sync(name);
    return new Promise(() => {
      function tick() {
        if (defined_now(name)) return get_definition_sync(name);
        timeout = setTimeout(tick, interval);
      }
      tick();
    });
  }

  function loader_in_timeframe(spec) {
    let _stopped;
    const { source, disposition, schema } = spec;
    const { db, store } = disposition;

    async function start() {
      const items = "what";
      const spec = { db, store, items };
      const write_op = IDB.write(spec);
      for await (const result of write_op) {
        if (_stopped) break;
        say?.(`Progress! possibly...`, result);
      }
    }
    function stop() {
      _stopped = true;
    }
    return { start, stop };
  }
}

// ===========================================

/** @param spec {{db: string, version?: number}} */
function reify_indexeddb(db_name) {
  const doc = document;
  const container = doc.querySelector("main") ?? doc.body;

  const element = doc.createElement("nav");
  const header = doc.createElement("header");
  const heading = doc.createElement("h3");
  const description = doc.createElement("p");

  heading.textContent = `databases`;
  description.textContent = `This is a place where you can look at the contents of an (indexedDB) database`;
  header.append(heading, description);
  element.append(header);

  const _console = dom_console();
  element.append(_console.element);
  const say = _console.log.bind(_console);

  {
    const listings = new Map();
    function ensure_listing_for(store) {
      if (listings.has(store)) return listings.get(store);
      // collection ≠ right?  this is screwy
      const listing = make_listing({ collection: store });
      listing.start();
      listings.set(store, listing);
      return listing;
    }
    function stop_all_listings() {
      for (const listing of listings.values()) listing.stop();
    }
  }

  let db;
  async function start() {
    say?.(`So, you want to know about ${db_name}, eh?`);
    try {
      db = await IDB.connect(db_name);
      // process could be stopped now
      // also connection could die in between
      say?.(`I see here a database ${db.name} with version ${db.version}.`);

      const store_names = db.objectStoreNames;
      const count = store_names.length;
      const things = `store${count === 1 ? "" : "s"}`;
      say?.(`I claim that ${db_name} has ${count} ${things}`);
      for (const store_name of store_names) {
        say?.(`I'll create a listing for “${store_name}”`);
        const listing = ensure_listing_for(store_name);
        element.append(listing.element);
      }
    } catch (error) {
      say?.(`I had a problem connecting to ${db_name}: ${error}`);
    } finally {
      db.close();
    }
  }
  function stop() {
    stop_all_listings();
    db?.close();
  }

  container.append(element);
  start();
  return { start, stop, element };
}

// ===========================================

function reify_collection(db_name, store_name, spec) {
  const lang = "en";
  const doc = document;
  const container = doc.querySelector("main") ?? doc.body;

  const element = doc.createElement("div");

  const heading = doc.createElement("h2");
  const form = doc.createElement("form");
  const delete_button = doc.createElement("button");

  heading.append(`Collection: ${store_name}`);
  delete_button.textContent = "Delete this store!";
  delete_button.onclick = async event => {
    event.preventDefault();
    say?.(`Sigh.  You really want me to delete ${store_name}, eh?`);
    try {
      // DELETE operation not monotonic
      // this is all kinds of race condition etc
      let db = await IDB.connect(db_name);
      say?.(`Okay, well I have ${db_name} at version ${db.version}`);
      db.close();
      db = await IDB.connect(db_name, db.version + 1, db => {
        say?.(`I'm in an upgrade transaction!`);
        db.deleteObjectStore(store_name);
        say?.(`Aaaaand I guess that worked?`);
      });
      db?.close();
    } catch (error) {
      console.error(`Couldn't delete store`, error);
    }
  };
  form.append(delete_button);
  element.append(heading, form);

  const _console = dom_console(element);
  const say = _console.log.bind(_console);

  async function start() {
    // make sure it has the latest indices if any are added
    // so that is updated even if there is no data update
    const description = spec.store;
    const db = await IDB.database_with(db_name, store_name, description);
    db.close();
    await ensure_catalog_data(db_name, store_name, { ...spec, say });
  }

  function stop() {}
  return { start, stop, element };
}

function reify_catalog(database_name, expectations) {
  const doc = document;
  const container = doc.querySelector("main") ?? doc.body;

  const element = doc.createElement("article");
  const header = doc.createElement("header");
  const heading = doc.createElement("h2");
  const description = doc.createElement("p");

  const jobs = new Map();

  const _console = dom_console();
  const say = _console.log.bind(_console);

  heading.append(`Catalog:`);
  description.textContent = `I will try to bring the database ${database_name} in line with the given catalog description.  The catalog description is a dictionary of table descriptions which can be processed completely independently.`;
  header.append(heading, description);
  element.append(header, _console.element);

  function start() {
    for (const [store, description] of Object.entries(expectations)) {
      say?.(`So there's a thing called ${store}`);
      const job = reify_collection(database_name, store, description);
      job.start();
      element.append(job.element);
      jobs.set(store, job);
    }
  }
  function stop() {
    for (const job of jobs.values()) job.stop?.();
  }

  container.append(element);
  return { start, stop };
}

// ******************************************************************

function word_cloud(context) {
  const ctx = context && typeof context === "object" ? context : {};
  const lang = ctx.lang ?? "en";
  const doc = ctx.document ?? document;
  const container = ctx.container ?? doc.querySelector("main") ?? doc.body;

  const date = typeof context === "string" ? context : context.date;
  assert?.(date && ISO8601.parse(date), `word cloud got bad date ${date}`);
  const counts = new Map();

  const element = doc.createElement("figure");
  const heading = doc.createElement("figcaption");
  const cloud = doc.createElement("ol");
  const label = ISO8601.label(date, lang);

  heading.append(`Word cloud for ${label}:`);
  cloud.classList.add("word-cloud");
  element.append(heading, cloud);

  function add_to_word_cloud(wiki) {
    if (counts.has(wiki)) {
      // increment
      const item = cloud.querySelector(`[data-wiki="${wiki}"]`);
      const count = counts.get(wiki);
      const new_count = count + 1;
      counts.set(wiki, new_count);
      item.setAttribute("data-count", new_count);
      item.style.setProperty("--count", new_count);
      let cut = false;
      let sibling = item.previousElementSibling;
      if (sibling) {
        while (new_count > sibling.dataset.count) {
          cut = true;
          const prev = sibling.previousElementSibling;
          if (!prev) break;
          sibling = prev;
        }
        if (cut) item.parentElement.insertBefore(item, sibling);
      }
    } else {
      // add with count of 1
      const item = doc.createElement("li");
      item.setAttribute("data-wiki", wiki);
      item.textContent = `${wiki_unslug(wiki)}`; // domain knowledge
      cloud.append(item);
      counts.set(wiki, 1);
      item.setAttribute("data-count", 1);
      item.style.setProperty("--count", 1);
    }
  }

  async function start() {
    const read_spec = {
      db: "cronwall",
      store: "en_wiki_events",
      index: "aboutTime",
      query: IDBKeyRange.only(date),
    };
    let budget = 20;
    for await (const result of IDB.read(read_spec)) {
      if (!--budget) break;
      if (result.refs) {
        for (const wiki_ref of result.refs) add_to_word_cloud(wiki_ref);
        await sleep(50);
      }
    }
  }

  function stop() {}
  container.append(element);
  return { start, stop, element };
}

// ****************************************************************************

async function ensure_catalog_data(db_name, store_name, spec) {
  const say = spec.say ?? console.debug.bind(console);
  const { source, xml_name } = spec;
  say?.(`First of all, is there already data in ${db_name}:${store_name}?`);
  try {
    const count = await IDB.count({ db: db_name, store: store_name });
    if (count > 0) {
      say?.(`Yes! ${count} things, in fact.  So I say we're done here`);
      return;
    }
    say?.(`No.`);
  } catch (error) {
    console.error(`Well I had a problem counting`, error);
  }
  say?.(`Can I load the source?`);
  await load_script(source);
  say?.(`Well I loaded the source.  I'll wait now for ${xml_name}`);
  const xml = await when_defined(xml_name);
  if (xml) say?.(`Oh! That exists too now.  It's ${xml.length}`);
  else {
    say?.(`For some reason that still doesn't exist`);
    say?.(`K bye!`);
    return;
  }

  const parser = new DOMParser();
  let /** @type Document */ doc;
  try {
    // but this won't catch parse errors, see https://stackoverflow.com/a/70824527
    doc = parser.parseFromString(xml, "text/xml");
  } catch (error) {
    say?.(`Wouldn't you know but that failed with ${error}`);
    say?.(`Bye!`);
    return;
  }
  say?.(`Did that parse *actually* succeed?`);
  const parseerror = doc.querySelector("parseerror");
  if (parseerror) {
    say?.(`Well friend that failed with ${parseerror.textContent}`);
    say?.(`Bye!`);
    return;
  }
  say?.(`And I parsed it!  Now, do we have a reader?`);
  const reader = spec.from_dom;
  if (!reader) {
    say?.(`No, sadly we don't. Bye!`);
    return;
  }
  say?.(`Yes, we do.  Let's read it!`);
  const results = reader(doc.documentElement);
  say?.(`I just read ${results.length} things!`);
  say?.(`Now it just remains to write them to the store.`);
  const description = spec.store;
  const options = { batch_size: 100 };
  const destination = { database: db_name, store: store_name, description };
  for await (const status of IDB.write(results, destination, options)) {
    say(status);
  }
  say?.(`All done!`);
}

// ====================================

function test_dynamic_people_listing(needs) {
  const { timeframe } = needs;
  const term = needs.term ?? "singer";
  const doc = document;
  const container = doc.querySelector("main") ?? doc.body;

  const rex = term && new RegExp(RegExp_escape(term));
  const source = {
    db: "cronwall",
    collection: "en_wiki_persons",
    // index: "born", // hmm
  };
  const listing = make_dynamic_listing({
    timeframe,
    ...source,
    mark: { term },
    make_source({ date }) {
      return make_collection_search({ ...source, term, date });
    },
  });
  container.append(listing.element);
  listing.start();
}

function test_dynamic_event_listing(needs) {
  const { timeframe } = needs;
  const term = needs.term ?? "singer";
  const doc = document;
  const container = doc.querySelector("main") ?? doc.body;
  const rex = term && new RegExp(RegExp_escape(term));
  const source = {
    db: "cronwall",
    collection: "en_wiki_events",
    index: "aboutTime", // hmm
  };
  const listing = make_dynamic_listing({
    timeframe,
    mark: { term }, // hmm?
    make_source({ date }) {
      return make_collection_search({ ...source, term, date });
    },
  });
  container.append(listing.element);
  listing.start();
}

// ====================================

function make_future_underlay() {
  const element = document.createElement("div");
  const now = new Date();
  const today = now.toISOString().slice(0, 10);
  const date = `${today}/9999`;
  element.setAttribute("data-time-underlay", "future");
  set_time_item_style(date, element.style);
  return element;
}

function make_timeframe_underlay(context) {
  const { timeframe } = context;
  if (!timeframe) throw new Error(`Bad request: missing timeframe`);
  let _sub;
  const element = document.createElement("div");
  const { style } = element;
  element.setAttribute("data-time-underlay", "timeframe");

  function _underlay_scalars_changed([from, to]) {
    style.setProperty("--major", from);
    style.setProperty("--major-to", to);
  }
  function start() {
    _sub = timeframe.watch_major_scalars(_underlay_scalars_changed);
  }
  function stop() {
    _sub?.unsubscribe();
  }
  return { start, stop, element };
}

function ensure_marker_in(ruler, date) {
  const seen = !!ruler.querySelector(`:scope>[data-time="${date}"]`);
  if (!seen) {
    const button = document.createElement("button");
    const stamp = timestamp(date);
    button.append(stamp);
    button.setAttribute("data-time", date);
    set_time_item_style(date, button.style);
    const ref = first_child_after(date, ruler);
    ruler.insertBefore(button, ref);
  }
}

// ====================================

function make_time_layers(context) {
  const doc = document;
  assert?.(context && typeof context === "object", `expected object context`);
  const { timeframe } = context;
  assert?.(timeframe, `expected timeframe`);
  const units = context.units ?? UNITS;

  const listings = new Map();
  const backdrops = new Map();
  const underlays = new Map();
  const vantages = new Map();
  const rulers = new Map();

  const layers = new Map(
    units.map(unit => {
      const outer = doc.createElement("div");
      const inner = doc.createElement("div");
      outer.classList.add("time-layer");
      inner.classList.add("time-layer-bands");
      outer.append(inner);
      outer.setAttribute("data-time-layer", unit);
      return [unit, [outer, inner]];
    })
  );

  const element = doc.createElement("div");
  element.classList.add("time-layers", "time-layers--open");
  element.append(...Array.from(layers, ([, [ele]]) => ele));

  for (const [unit, [outer, inner]] of layers) {
    {
      const heading = doc.createElement("b");
      heading.append(`${unit}`);
      inner.append(heading);
    }

    // ruler
    {
      // breaks Day marker positioning
      // ruler_container.classList.add("time-fixed-item-collection");
      // ruler_container.classList.add("time-fixed-item-collection--margin");
      // ruler_container.classList.add("marker-band");
      // ruler_container.append(ruler);
      const ruler = doc.createElement("div");
      const ruler_container = doc.createElement("div");
      ruler.classList.add("time-markers");
      rulers.set(unit, ruler);
      inner.append(ruler);
    }

    // vantage
    {
      const lens_spec = { timeframe, unit, visit, element: outer };
      const vantage = make_time_vantage(lens_spec);
      vantages.set(unit, vantage);
    }

    // backdrop
    {
      const backdrop = make_time_backdrop();
      backdrops.set(unit, backdrop);
      inner.append(backdrop.element);
    }

    // underlays
    inner.classList.add("with-underlay");
    inner.append(make_future_underlay()); // this one will become a process too...

    // timeframe underlay
    {
      const timeframe_underlay = make_timeframe_underlay({ timeframe });
      underlays.set(unit, timeframe_underlay);
      inner.append(timeframe_underlay.element);
    }
  }

  function _ensure_finding_list_for(unit, group, element) {
    const selector = `[data-findings-unit="${unit}"][data-findings-group="${group}"]`;
    const existing = element.querySelector(selector);
    if (existing) return existing;
    const markers = doc.createElement("ul");
    markers.dataset.findingsUnit = unit;
    markers.dataset.findingsGroup = group;
    markers.classList.add("finding-markers");
    element.append(markers);
    return markers;
  }

  function visit_finding(group, record) {
    // HACK! need a generic method for this
    const date = record.aboutTime ?? record.born ?? record.died;
    if (!date) return console.warn(`no date`, record);
    for (const [unit, [outer, inner]] of layers) {
      // if (unit !== "Millennium") continue;
      const markers = _ensure_finding_list_for(unit, group, inner);
      const marker = doc.createElement("li");
      set_time_item_style(date, marker.style);
      // would like a smaller indicator than render_into
      const span = doc.createElement("span");
      span.dataset.part = "text";
      span.append(`@ ${date}`);
      marker.append(span);
      markers.append(marker);
    }
  }

  function visit(_____element, date) {
    const parsed = ISO8601.parse(date);
    assert?.(parsed, `hierarchy explorer visit got bad date ${date}`);
    if (listings.has(date)) return;
    const { unit } = parsed;
    assert?.(units.includes(unit), `hierarchy explorer bad unit`, date);

    // hardcoded here because we're interested in events specifically... yah
    const collection = "en_wiki_events";
    const index = "aboutTime";
    const query = IDBKeyRange.only(date);
    function* make_scans() {
      yield { index, query };
    }
    const listing = make_listing({ collection, make_scans });
    const time = timestamp(date);
    const div = doc.createElement("div");
    div.append(time);
    const backdrop = backdrops.get(unit);
    // DISABLED backdrops while working on other stuff
    if (false && ISO8601.compare_units(unit, "Year") >= 0) {
      const segment = backdrop.ensure_segment_for(date);
      segment.append(div, listing.element);
      listing.start();
      listings.set(date, listing);
    }

    ensure_marker_in(rulers.get(unit), date);
  }

  function start() {
    for (const vantage of vantages.values()) vantage.start();
    for (const underlay of underlays.values()) underlay.start();
  }
  function stop() {
    for (const vantage of vantages.values()) vantage.stop();
    for (const listing of listings.values()) listing.stop();
    for (const backdrop of backdrops.values()) backdrop.stop();
    for (const underlay of underlays.values()) underlay.stop();
  }

  return { start, stop, layers, element, visit, visit_finding };
}

// distribute context to start- and end-aligned layer sets
// runs only one scan at a time
function make_fringes(context) {
  const scans = new Map();
  const { timeframe, ...opts } = context;

  const head = opts.head ? make_time_layers({ timeframe }) : null;
  const foot = opts.foot
    ? make_time_layers({ timeframe, units: REVERSE_UNITS })
    : null;
  if (head) head.element.dataset.magnet = "start";
  if (foot) foot.element.dataset.magnet = "end";

  // what we're really adding is a plot (vs listing) of a growing set
  // TODO: this should be a subscription to ref deltas
  function add_ref(ref) {
    const group_name = ref; // using this as a proxy
    const scan = make_time_scan({
      // STILL HARDCODED... this should be the same as the listing source
      // except without date constraints
      db: "cronwall",
      collection: "en_wiki_events",
      ref,
      visit: visit_finding.bind(null, group_name),
      budget: 100,
      rate: 16,
    });
    scan.start();
    scans.set(ref, scan);
  }

  function visit_finding(...args) {
    head?.visit_finding(...args);
    foot?.visit_finding(...args);
  }

  function start() {
    head?.start();
    foot?.start();
  }
  function stop() {
    head?.stop();
    foot?.stop();
    for (const scan of scans.values()) scan.stop();
  }
  return { start, stop, head, foot, add_ref };
}

const get_person = (() => {
  const PERSONS = { db: "cronwall", store: "en_wiki_persons" };
  return wiki => IDB.get_by_key(PERSONS, wiki);
})();

const get_event = (() => {
  const EVENTS = { db: "cronwall", store: "en_wiki_events" };
  return id => IDB.get_by_key(EVENTS, id);
})();

// set timeframe to person's lifetime, if it's a person
async function maybe_visit_person(ref, timeframe) {
  // we have no in-memory test of whether this is in fact a person
  const maybe_person = await get_person(ref);
  if (maybe_person) {
    const { born, died } = maybe_person;
    if (born && died) {
      if (ISO8601.compare(born, died) > 0) {
        console.warn(ref, `has reversed dates:`, maybe_person);
        return;
      }
      const margin = 25; // years
      const born_year_date = ISO8601.coerce(born, "Year");
      const died_year_date = ISO8601.coerce(died, "Year");
      const born_year = parseInt(born_year_date, 10);
      const died_year = parseInt(died_year_date, 10);
      const age_years = died_year - born_year;
      const born_padded = pad4(born_year - margin);
      const died_padded = pad4(died_year + margin + age_years);
      timeframe.set(`${born_padded}/${died_padded}`);
    }
  }
}

function make_dynamic_set() {
  const set = new Set();
  const deltas = make_subscribable();
  const collection = make_subscribable();
  const has = key => set.has(key);
  function add(key) {
    if (!set.has(key)) {
      deltas.next({ delta: 1, key });
      set.add(key);
      collection.next(set);
    }
  }
  function remove(key) {
    if (set.has(key)) {
      deltas.next({ delta: -1, key });
      set.delete(key);
      collection.next(set);
    }
  }
  function dispose() {
    // close all connections
  }
  return {
    dispose,
    has,
    add,
    remove,
    deltas: deltas.public,
    stream: collection.public,
  };
}

function make_dynamic_map() {
  const map = new Map();
  const deltas = make_subscribable();
  const collection = make_subscribable();
  const has = key => map.has(key);
  const get = key => map.get(key);
  function set(key, value) {
    if (!map.has(key)) {
      deltas.next({ delta: 1, key, value });
      map.set(key, value);
      collection.next(map);
    }
  }
  function remove(key) {
    if (map.has(key)) {
      // wait till you need this...
      // const value = map.get(value)
      deltas.next({ delta: -1, key /*, value */ });
      map.delete(key);
      collection.next(map);
    }
  }
  function dispose() {
    // close all connections
  }
  return {
    dispose,
    has,
    get,
    set,
    remove,
    deltas: deltas.public,
    stream: collection.public,
    [Symbol.iterator]() {
      return map[Symbol.iterator]();
    },
  };
}

// support marking of links whose refs are a topic in the context
function support_marking_refs(context) {
  const { refs_stream, scope } = context;
  if (!refs_stream) throw new Error(`Bad request: missing refs`);
  if (!scope) throw new Error(`Bad request: missing scope`);
  let sub;

  const style = document.createElement("style");
  document.head.append(style);

  function refs_changed(/** @type Iterable<string> */ refs) {
    const selectors = Array.from(refs, ref => `${scope}[wiki="${ref}"]`);
    if (selectors.length > 0) {
      const selector = selectors.join(", ");
      const css = `${selector} { --selected: 1 }`;
      // can we not set css text directly
      style.innerHTML = css;
    } else style.innerHTML = "";
  }

  function start() {
    sub = refs_stream.subscribe({ next: refs_changed });
  }
  function stop() {
    sub?.unsubscribe();
  }
  return { start, stop };
}

// ====================================================

// For all values seen in the delta stream, keep the product of `make`
// if result is a process, start it on first sight (and stop with this lifecycle)
// if result has an element, add it to the given container
function memoize_over_key_changes(context) {
  const { deltas, make, element } = context;
  if (typeof make !== "function") throw new Error(`Bad request: no f'n make`);
  if (!(element instanceof Element)) throw new Error(`Bad request: !element`);
  let sub;
  const things = new Map();

  let i = 0;
  function _add(key) {
    // if (++i > 20) return console.error("excess");
    if (!key) throw new Error(`Key must be truthy`);
    if (things.has(key)) return console.info(key, `already a thing`);
    const thing = make(key);
    if (!thing) return console.warn(`I got nothing`, key);
    if (element && thing.element) element.append(thing.element);
    thing.start?.();
    things.set(key, thing);
  }
  function _remove(key) {
    // if (++i > 20) return console.error("excess remove");
    if (!things.has(key)) return console.info(key, `not a thing`);
    const thing = things.get(key);
    thing.element?.remove(); // REMOVES ABRUPTLY!
    thing.stop();
    things.delete(key);
  }

  function start() {
    if (deltas) {
      sub = deltas.subscribe({
        next(the) {
          // prettier-ignore
          switch (the.delta) {
            case 1: _add(the.key); break;
            case -1: _remove(the.key); break;
          }
        },
      });
    }
  }
  function stop() {
    for (const thing of things.values()) thing.stop?.();
    sub?.unsubscribe();
  }
  return { start, stop, map: things };
}

// a heaping mess that complects the state machine & dom representation.
// a sequence in which, unless it's empty, you have a position
// and you can step forward or backward
function make_sequence(get_reader) {
  let _all = [];
  let _stopped = false;
  let position = NaN; // which it is IFF _all is empty
  const stream = make_subscribable();
  const doc = document;
  const element = doc.createElement("div");
  const next = doc.createElement("button");
  const prev = doc.createElement("button");
  const item = doc.createElement("div");
  const nav = doc.createElement("nav");
  element.classList.add("time-sequence");
  item.classList.add("sequence-item");
  next.setAttribute("rel", "next"); // not official on `button`
  prev.setAttribute("rel", "prev"); // not official on `button`
  next.textContent = "next";
  prev.textContent = "prev";
  nav.classList.add("sequence-nav");
  const number = doc.createElement("span");
  /*
  const place = doc.createElement("span");
  place.append(`Item `, number);
  */
  nav.append(prev, next);
  element.append(nav, item);

  // const findings = document.createElement("")

  const bof = () => position <= 0;
  const eof = () => position >= _all.length - 1;
  const get_value = () => _all[position];

  function step_forward() {
    if (eof()) return;
    ++position;
    number.textContent = position + 1;
    stream.next({ position, value: get_value() });
  }
  function step_backward() {
    if (bof()) return;
    --position;
    number.textContent = position + 1;
    stream.next({ position, value: get_value() });
  }

  function clicked(event) {
    if (!(event.target instanceof Element)) return;
    const link = event.target.closest(`[rel]`);
    if (!link) return;
    const rel = link.getAttribute("rel");
    if (rel === "prev") step_backward();
    else if (rel === "next") step_forward();
    else console.info(`unknown rel`, rel);
  }

  async function start() {
    element.addEventListener("click", clicked);

    // HACK: just buffer the whole thing
    // doing this lazily introduces too many irrelevant complications
    // I just want to see what this is like
    // so I'm going to buffer the whole sequence
    // then it really doesn't need to know about how the sequence is made
    // and it doesn't need to be reversible at the source
    element.dataset.loading = "true";
    const reader = get_reader();
    for await (const result of reader) {
      if (_stopped) break;
      _all.push(result);
    }
    delete element.dataset.loading;
    if (_all.length > 0) {
      position = 0;
      stream.next({ position, value: get_value() });
    }
  }
  function stop() {
    _stopped = true;
    element.removeEventListener("click", clicked);
  }
  return { start, stop, element, visited: stream.public, item };
}

// =======================================

// create a slot in the first row where you have room
// makes elements for rows but not for slots (just blocks them)
function make_time_ribbons() {
  const rows = [];
  const doc = document;
  const { compare } = ISO8601;

  const element = doc.createElement("div");
  const ribbons = doc.createElement("div");
  element.append(ribbons);
  element.classList.add("time-fixed-item-collection");
  ribbons.classList.add("time-ribbons");

  function new_row(n) {
    const ribbon = doc.createElement("div");
    ribbon.classList.add("time-ribbon");
    ribbons.append(ribbon);
    const it = { element: ribbon, slots: [] };
    rows.push(it);
    return it;
  }

  const are_non_overlapping = (a_from, a_to, b_from, b_to) =>
    compare(a_to, b_from) <= 0 || compare(b_to, a_from) <= 0;

  function can_accept(row, from, to) {
    for (const slot of row.slots)
      if (!are_non_overlapping(slot.from, slot.to, from, to)) return false;
    return true;
  }

  function first_accepting_row(from, to) {
    for (const row of rows) if (can_accept(row, from, to)) return row;
  }

  /** block in the first available slot */
  function append(from, to) {
    const row = first_accepting_row(from, to) ?? new_row();
    row.slots.push({ from, to });
    return row.element;
  }

  // but this is not a process...
  function start() {}
  function stop() {}
  return { start, stop, element, append };
}

// but it doesn't recognize a dynamic collection
function with_people_ribbons_in_centuries(context) {
  const { timeframe, list } = context;
  if (!timeframe) throw new Error("Bad request: missing timeframe");
  if (!Array.isArray(list)) throw new Error("Bad request: need array list");
  let stopped = false;
  const PERSONS = Object.freeze({ db: "cronwall", store: "en_wiki_persons" });
  const doc = document;
  const ribbons = make_time_ribbons();

  async function start() {
    ribbons.start?.();
    for (const wiki of list) {
      if (stopped) break;
      const person = await IDB.get_by_key(PERSONS, wiki);
      if (!person) {
        console.warn("no such person?");
        continue;
      }
      const from = person.born;
      const to = person.died;
      if (!from || !to) {
        console.warn(person, "missing dates?");
        continue;
      }
      const row = ribbons.append(from, to);
      const link = doc.createElement("a");
      link.title = `${person.summary ?? ""} (${from} – ${to})`;
      link.setAttribute("wiki", wiki);
      link.textContent = wiki_unslug(wiki);
      row.append(link, " ");
      // dumb because it just takes these back apart...
      set_time_item_style(`${from}/${to}`, link.style);
      // but we know it's a person...
      // render_into()
    }
  }
  function stop() {
    ribbons.stop?.();
    stopped = true;
  }

  // const container = document.querySelector(".time-main-stage");
  const fringe = timeframe.element.querySelector(
    `[data-magnet="start"] [data-time-layer="Century"] .time-markers`
  );
  fringe?.insertAdjacentElement("afterend", ribbons.element);
  return { start, stop };
}

function visit_people_when_ref_added(context) {
  const { timeframe, ref_deltas } = context;
  if (!timeframe) throw new Error(`Bad request: timeframe expected`);
  if (!ref_deltas) throw new Error(`Bad request: ref deltas expected`);
  let sub;
  function start() {
    sub = ref_deltas.subscribe({
      next(the) {
        if (the.delta === 1) {
          maybe_visit_person(the.key, timeframe);
        }
      },
    });
  }
  function stop() {
    sub.unsubscribe();
  }
  return { start, stop };
}

async function rank_people_by_refs_impl() {
  const keys = [];
  const counts = new Map(); // wiki->ref count
  // get all people who have both birth and death dates
  const PERSONS = { db: "cronwall", store: "en_wiki_persons" };
  const EVENTS = { db: "cronwall", store: "en_wiki_events" };
  const query = IDBKeyRange.lowerBound(""); // seems to eliminate nulls
  const reader_spec = { ...PERSONS, index: "born", query };
  for await (const person of IDB.read(reader_spec)) {
    assert?.(person.born !== null, `missing birth date`, person);
    if (!person.died) continue;
    keys.push(person.wiki);
  }
  let i = 0;
  // should be a sorted set (by count)
  for (const wiki of keys) {
    i++;
    if (i % 1000 === 0) console.debug(i);
    const query = IDBKeyRange.only(wiki);
    const count_spec = { ...EVENTS, index: "refs", query };
    const count = await IDB.count(count_spec);
    counts.set(wiki, count);
  }
  const entries = [...counts];
  entries.sort(([, a], [, b]) => b - a);
  return entries;
}

async function rank_people_by_refs() {
  const KEY = "rank_people_by_refs";
  try {
    const text = localStorage.getItem(KEY);
    if (text) {
      const parsed = JSON.parse(text);
      if (parsed) return parsed;
    }
  } catch {}
  const results = await rank_people_by_refs_impl();
  try {
    const json = JSON.stringify(results);
    localStorage.setItem(KEY, json);
  } catch {}
  return results;
}

// ====================================

function tests() {
  ISO8601.self_tests();
  IDB.self_tests();
}

{
  const doc = document;

  /** @returns {string | Node} */
  function render(something) {
    return [`${something}`];
  }

  function dom_console(/** @type Element */ log) {
    log ??= document.createElement("ol");
    log.classList.add("console");
    const alt = map_object(
      console,
      name =>
        function (...args) {
          const item = doc.createElement("li");
          item.dataset.channel = name;
          item.append(...args.flatMap(render));
          log.append(item);
        }
    );
    Object.assign(alt, { element: log });
    return alt;
  }
}

function reify_persistence() {
  // reify_indexeddb("cronwall");
  reify_catalog("cronwall", KNOWN_CATALOG).start();
}

function make_main_stage(options) {
  const use_fringes = options?.fringes ?? false;
  const doc = document;
  const caption = doc.createElement("div");
  const timeframe = make_timeframe({ caption });
  const main_stage = doc.createElement("div");
  const fringes = use_fringes
    ? make_fringes({ timeframe, head: true, foot: false })
    : null;

  main_stage.classList.add("time-main-stage");
  if (fringes?.head) timeframe.element.append(fringes.head.element);
  timeframe.element.append(main_stage);
  if (fringes?.foot) timeframe.element.append(fringes.foot.element);

  function start() {
    timeframe.start();
    fringes?.start();
  }
  function stop() {
    timeframe.stop();
    fringes?.stop();
  }

  return { start, stop, timeframe, fringes, main_stage };
}

{
  // nominate people who lived during the current time
  // element is informational
  // - one scan at a time
  // - promise would be that the current period will eventually be covered?
  const lang = "en";
  const BUDGET = 5;
  const RATE = 150;
  const { compare, between, label } = ISO8601;
  function make_person_lifespan_nominator(context) {
    const { timeframe } = context;
    if (!timeframe) throw new Error(`Bad request: missing timeframe`);
    let _from = null;
    let _to = null;
    let _sub = null;
    let _scan = null;

    const nominations = make_dynamic_map();
    const element = document.createElement("aside");
    const header = document.createElement("header");
    const heading = document.createElement("h2");
    const description = document.createElement("p");
    const status = document.createElement("output");
    element.classList.add("person-nominator");
    description.append(`My job is to nominate people for view.`);
    header.append(heading, description, status);
    element.append(header);

    function _completely_in_timeframe(person) {
      const { born, died } = person;
      return (
        born && died && compare(died, _to) <= 0 && compare(_from, born) <= 0
      );
    }
    const _born_in_frame = you => you.born && between(you.born, _from, _to);
    const _died_in_frame = you => you.died && between(you.died, _from, _to);
    const _fully_in_frame = you => _born_in_frame(you) && _died_in_frame(you);
    function _audition(person) {
      if (_from === null) return false;
      assert?.(_to !== null, `should be same as from`);
      const { born, died } = person;
      // because we stop the scan and should expect no more
      // ...but these fail a lot
      // assert?.(born, `expected born date`);
      // assert?.(between(born, _from, _to), `born ${born} ∉ ${_from}/${_to}`);
      if (!born) {
        console.warn(`expected born date`);
        return false;
      }
      if (!between(born, _from, _to)) {
        // console.warn(`born ${born} ∉ ${_from}/${_to}`);
        return false;
      }
      return died && between(died, _from, _to);
    }
    function _nominate(person) {
      if (!nominations.has(person.wiki)) nominations.set(person.wiki, person);
    }

    const persons_born = {
      db: "cronwall",
      store: "en_wiki_persons",
      index: "born",
      dir: "next", // the default
    };
    function _populate_stage_jump_to(from, to) {
      _scan?.stop();
      [_from, _to] = [from, to];
      // BAD! only okay for small N but we should use a sorted map by date
      for (const [key, person] of nominations) {
        if (!(_born_in_frame(person) || _died_in_frame(person)))
          nominations.remove(key); // ABRUPT; should mark
      }
      const within_dates = { query: safe_date_bounds(_from, _to) };
      const source = IDB.read({ ...persons_born, ...within_dates });
      const filtered = async_filter(source, _audition);
      const limited = async_take(filtered, BUDGET);
      const sink = async_map(limited, _nominate);
      _scan = CONSUME_POWER_at_rate(sink, RATE);
      _scan.start();
    }
    function _timeframe_date_changed(date) {
      // but this doesn't mean we're changing the scan...
      const [from, to] = timeframe_bounding_dates(date);
      status.textContent = `looking for people in ${label(date, lang)}`;
      // if the current scan is subsumed by etc...
      const subsumed = false;
      if (!subsumed) _populate_stage_jump_to(from, to);
    }
    function start() {
      _sub = timeframe.watch(_timeframe_date_changed);
    }
    function stop() {
      _scan?.stop();
      _sub?.unsubscribe();
    }
    return { start, stop, element, collection: nominations };
  }
}

{
  // like above but for events
  const lang = "en";
  const BUDGET = 5;
  const RATE = 150;
  const { compare, between, label } = ISO8601;
  function make_event_date_nominator(context) {
    const { timeframe } = context;
    if (!timeframe) throw new Error(`Bad request: missing timeframe`);
    let _from = null;
    let _to = null;
    let _sub = null;
    let _scan = null;

    const nominations = make_dynamic_map();
    const element = document.createElement("aside");
    const header = document.createElement("header");
    const heading = document.createElement("h2");
    const description = document.createElement("p");
    const status = document.createElement("output");
    element.classList.add("event-nominator");
    description.append(`My job is to nominate events for view.`);
    header.append(heading, description, status);
    element.append(header);

    function _completely_in_timeframe(event) {
      const { aboutTime } = event;
      const [start, end] = timeframe_bounding_dates(aboutTime);
      return start && compare(aboutTime, _to) <= 0 && compare(_from, end) <= 0;
    }
    const _started_in_frame = it =>
      it.aboutTime && between(it.aboutTime, _from, _to);
    const _ended_in_frame = it =>
      it.aboutTime && between(it.aboutTime, _from, _to);
    const _fully_in_frame = it => _started_in_frame(it) && _ended_in_frame(it);
    function _audition(event) {
      if (_from === null) return false;
      assert?.(_to !== null, `should be same as from`);
      const { aboutTime } = event;
      // because we stop the scan and should expect no more
      // ...but these fail a lot
      // assert?.(born, `expected born date`);
      // assert?.(between(born, _from, _to), `born ${born} ∉ ${_from}/${_to}`);
      if (!aboutTime) {
        console.warn(`expected about time`);
        return false;
      }
      if (!between(aboutTime, _from, _to)) {
        // console.warn(`born ${born} ∉ ${_from}/${_to}`);
        return false;
      }
      return true; // died && between(died, _from, _to);
    }
    function _nominate(event) {
      if (!nominations.has(event.id)) nominations.set(event.id, event);
    }

    const events_about = {
      db: "cronwall",
      store: "en_wiki_events",
      index: "aboutTime",
      dir: "next", // the default
    };
    function _populate_stage_jump_to(from, to) {
      _scan?.stop();
      [_from, _to] = [from, to];
      // BAD! only okay for small N but we should use a sorted map by date
      for (const [key, event] of nominations) {
        if (!(_started_in_frame(event) || _ended_in_frame(event)))
          nominations.remove(key); // ABRUPT; should mark
      }
      const within_dates = { query: safe_date_bounds(_from, _to) };
      const source = IDB.read({ ...events_about, ...within_dates });
      const filtered = async_filter(source, _audition);
      const limited = async_take(filtered, BUDGET);
      const sink = async_map(limited, _nominate);
      _scan = CONSUME_POWER_at_rate(sink, RATE);
      _scan.start();
    }
    function _timeframe_date_changed(date) {
      // but this doesn't mean we're changing the scan...
      const [from, to] = timeframe_bounding_dates(date);
      status.textContent = `looking for events in ${label(date, lang)}`;
      // if the current scan is subsumed by etc...
      const subsumed = false;
      if (!subsumed) _populate_stage_jump_to(from, to);
    }
    function start() {
      _sub = timeframe.watch(_timeframe_date_changed);
    }
    function stop() {
      _scan?.stop();
      _sub?.unsubscribe();
    }
    return { start, stop, element, collection: nominations };
  }
}

{
  function with_show_person_nominations(context) {
    const { nominator } = context;
    if (!nominator) throw new Error(`Bad request: missing thing`);
    let _sub;
    const element = document.createElement("ul");
    element.classList.add("nomination-list");

    function make_item(wiki, person) {
      const item = document.createElement("li");
      const link = document.createElement("a");
      const { born, died } = person;
      item.setAttribute("wiki", wiki);
      link.setAttribute("wiki", wiki);
      link.textContent = `${wiki_unslug(wiki)} (${born} – ${died})`;
      item.append(link);
      return item;
    }

    function _delta(the) {
      const wiki = the.key;
      switch (the.delta) {
        case 1: {
          const person = the.value;
          const item = make_item(wiki, person);
          element.append(item);
          break;
        }
        case -1: {
          const existing = element.querySelector(`[wiki="${wiki}"]`);
          // console.debug(`REMOVE ${wiki}`, existing);
          assert?.(existing, `expected element for ${wiki}`);
          existing?.remove(); // abrupt, would go into exit group here
          break;
        }
        default:
          throw new Error(`Unexpected Δ: ${the.delta}`);
      }
    }
    function start() {
      _sub = nominator.collection.deltas.subscribe({ next: _delta });
    }
    function stop() {
      _sub?.unsubscribe();
    }
    return { start, stop, element };
  }
}
{
  function with_show_event_nominations(context) {
    const { nominator } = context;
    if (!nominator) throw new Error(`Bad request: missing thing`);
    let _sub;
    const element = document.createElement("ul");
    element.classList.add("nomination-list");

    function make_item(id, event) {
      const item = document.createElement("li");
      const link = document.createElement("a");
      const { aboutTime, description } = event;
      item.setAttribute("data-event", id);
      link.setAttribute("data-event", id);
      link.textContent = `event at ${aboutTime}`;
      item.append(link);
      return item;
    }

    function _delta(the) {
      const id = the.key;
      switch (the.delta) {
        case 1: {
          const event = the.value;
          const item = make_item(id, event);
          element.append(item);
          break;
        }
        case -1: {
          const existing = element.querySelector(`[data-event="${id}"]`);
          // console.debug(`REMOVE ${wiki}`, existing);
          assert?.(existing, `expected element for ${id}`);
          existing?.remove(); // abrupt, would go into exit group here
          break;
        }
        default:
          throw new Error(`Unexpected Δ: ${the.delta}`);
      }
    }
    function start() {
      _sub = nominator.collection.deltas.subscribe({ next: _delta });
    }
    function stop() {
      _sub?.unsubscribe();
    }
    return { start, stop, element };
  }
}

{
  const compare = (a, b) => (a < b ? -1 : a > b ? 1 : 0);
  const to_scalar = major_scalar_from;
  function extend_to_include(timeframe, date) {
    const [from, to] = timeframe_bounding_dates(timeframe).map(to_scalar);
    const [start, end] = timeframe_bounding_dates(date).map(to_scalar);
    const lesser = compare(start, from) < 0 ? start : from;
    const greater = compare(to, end) < 0 ? end : to;
    // return `${lesser}/${greater}`;
    return major_scalars_to_timeframe(lesser, greater);
  }
}

let context_refs; // this is referenced by link interception
async function main() {
  context_refs = make_dynamic_set();
  const doc = document;
  const main_thing = make_main_stage({ fringes: true });
  const { timeframe, main_stage } = main_thing;
  timeframe.element.id = `t${Math.floor(Math.random() * 1e12)}`; // to scope css

  if (true) {
    const caption = document.createElement("header");
    caption.dataset.part = "caption";
    main_stage.insertAdjacentElement("beforebegin", caption);
    make_timeframe_labeler({ timeframe, element: caption }).start();
  }

  const container = doc.querySelector("main") ?? doc.body;
  container.append(timeframe.element);

  main_thing.start();

  if (true) {
    const ref_deltas = context_refs.deltas;
    const it = visit_people_when_ref_added({ timeframe, ref_deltas });
    it.start();
  }

  if (true) {
    const scope = `#${timeframe.element.id} `;
    const refs_stream = context_refs.stream;
    const it = support_marking_refs({ refs_stream, scope });
    it.start();
  }

  if (false) {
    const SOURCES = ["en_wiki_persons", "en_wiki_events"];
    memoize_over_key_changes({
      element: main_stage,
      deltas: context_refs.deltas,
      make: ref => {
        return make_dynamic_listing({
          timeframe,
          mark: { term: wiki_unslug(ref) },
          make_source({ date }) {
            const all = SOURCES.flatMap(collection => {
              const query = { db: "cronwall", collection, ref, date };
              return _collection_search_sources(query);
            });
            return async_merge_roundrobin(all);
          },
        });
      },
    }).start();
  }

  if (false) {
    function make_source({ date }) {
      const query = { db: "cronwall", collection: "en_wiki_events", date };
      const sources = _collection_search_sources(query);
      return async_merge_roundrobin(sources);
    }
    const news = make_dynamic_listing({ timeframe, make_source });
    main_stage.append(news.element);
    news.start();
  }

  if (false) {
    memoize_over_key_changes({
      element: main_stage,
      deltas: context_refs.deltas,
      make: wiki => make_person_lifetime_mark({ timeframe, wiki }),
    }).start();
  }

  if (false) {
    memoize_over_key_changes({
      element: main_stage,
      deltas: context_refs.deltas,
      make: wiki => make_person_at_age_listing({ wiki }),
    }).start();
  }

  if (true) {
    memoize_over_key_changes({
      element: main_stage,
      deltas: context_refs.deltas,
      make: ref => {
        let _sub;
        const SOURCE = { db: "cronwall", collection: "en_wiki_events" };

        const life = make_person_lifetime_mark({ timeframe, wiki: ref });
        const sequence = make_sequence(() => {
          const query = IDBKeyRange.only(ref);
          const spec = { ...SOURCE, index: "refs", query };
          return search_collection(spec);
        });

        const element = doc.createElement("article");
        const time = make_marker_block("date-block");
        const marker = make_marker_block("time-block-marker");
        const article = make_timeframe_article({ timeframe });
        const body = article.content;
        element.classList.add("person-composition");
        article.element.classList.add("composition-body");
        element.append(life.element, sequence.element);
        sequence.item.append(time.block, marker.block, article.element);

        function start() {
          life.start(); // NEW PROCESS, we have no dispose here
          sequence.start(); // NEW PROCESS, we have no dispose here

          _sub = sequence.visited.subscribe({
            next(item) {
              const { position, value } = item;
              const date = get_date(value);
              if (date) {
                const extended = extend_to_include(timeframe.get(), date);
                if (extended) timeframe.set(extended);
                const [from, to] = timeframe_bounding_dates(date);
                set_time_style_and_attributes(sequence.item, from, to);
              }
              // could just change the date content...
              time.marker.innerHTML = "";
              time.marker.append(timestamp(date));
              body.innerHTML = "";
              render_into(value, body);
            },
          });
        }
        function stop() {
          life.stop();
          sequence.stop();
          _sub?.unsubscribe();
        }
        return { start, stop, element };
      },
    }).start();
  }

  // order matters: if you want the pinned one to be *outermost*
  if (false) {
    const results = await rank_people_by_refs();
    const list = results.slice(0, 500).map(entry => entry[0]);
    const it = with_people_ribbons_in_centuries({ timeframe, list });
    it.start();
  }

  if (false) {
    test_dynamic_people_listing();
    test_dynamic_event_listing("wine");
  }

  if (true) {
    const pinned = [];
    //    pinned.push("Emmett_Till");
    //    pinned.push("Rosa_Parks");
    // pinned.push("William_Shakespeare");
    // pinned.push("John_Lennon");
    // pinned.push("Elizabeth_I_of_England");
    // pinned.push("William_Blake");
    // pinned.push("Miles_Davis");
    // pinned.push("Galileo_Galilei");
    // pinned.push("Isaac_Newton");
    // pinned.push("Tycho_Brahe");
    // pinned.push("Nicolaus_Copernicus");
    // pinned.push("Johannes_Kepler");
    pinned.forEach(ref => context_refs.add(ref));
    if (!pinned.length) timeframe.set("17/21");
  }

  main_stage.classList.add("with-underlay", "time-main-stage--fixed");
  main_stage.append(make_future_underlay());

  // support_timeframe_pointer_echo({ timeframe }).start();
  const pan_and_zoom = support_timeframe_pan_and_zoom({ timeframe });
  pan_and_zoom.start();
  const mainiac = { timeframe, mover: pan_and_zoom.mover };
  Object.assign(globalThis, mainiac);

  if (true) {
    const deltas = make_subscribable(); // {delta: 1 | -1, key: wiki ref}
    const nominator = make_person_lifespan_nominator({ timeframe, deltas });
    const shower = with_show_person_nominations({ nominator });
    nominator.start();
    // nominator.element.append(shower.element);
    // shower.start();
    // main_stage.append(nominator.element);

    if (true) {
      memoize_over_key_changes({
        element: main_stage,
        deltas: nominator.collection.deltas,
        make: wiki => {
          return make_person_lifetime_mark({ timeframe, wiki });
        },
      }).start();
    }
  }
  // same as above but for events
  if (true) {
    const deltas = make_subscribable(); // {delta: 1 | -1, key: wiki ref}
    const nominator = make_event_date_nominator({ timeframe, deltas });
    const shower = with_show_event_nominations({ nominator });
    nominator.start();
    shower.start();
    if (true) {
      memoize_over_key_changes({
        element: main_stage,
        deltas: nominator.collection.deltas,
        make: id => {
          return make_event_mark({ timeframe, id });
        },
      }).start();
    }
  }
}

// reify_persistence();
make_time_markers_navigational().start();
hijack_wikipedia_links().start();
ISO8601.self_tests();
main();
// for (const date of "18 196 1564 -02 0572".split(" ")) word_cloud(date).start();
