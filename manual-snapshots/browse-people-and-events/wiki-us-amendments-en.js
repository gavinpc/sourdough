{
 globalThis.XML_us_amendments = `<us-constitution>
  <amendment wiki="First_Amendment_to_the_United_States_Constitution" number="1" proposed="1789-09-25" completed="1791-12-15">
    <description>Protects <a wiki="Freedom_of_religion_in_the_United_States">freedom of religion</a>, <a wiki="Freedom_of_speech_in_the_United_States">freedom of speech</a>, <a wiki="Freedom_of_the_press_in_the_United_States">freedom of the press</a>, <a wiki="Freedom_of_assembly">freedom of assembly</a> and the <a wiki="Right_to_petition">right to petition the government</a>.</description>
  </amendment>
  <amendment wiki="Second_Amendment_to_the_United_States_Constitution" number="2" proposed="1789-09-25" completed="1791-12-15">
    <description>Protects the <a wiki="Right_to_keep_and_bear_arms_in_the_United_States">right to keep and bear arms</a></description>
  </amendment>
  <amendment wiki="Third_Amendment_to_the_United_States_Constitution" number="3" proposed="1789-09-25" completed="1791-12-15">
    <description>Restricts the <a wiki="Quartering_Acts">quartering</a> of soldiers in private homes</description>
  </amendment>
  <amendment wiki="Fourth_Amendment_to_the_United_States_Constitution" number="4" proposed="1789-09-25" completed="1791-12-15">
    <description>Prohibits unreasonable <a wiki="Search_and_seizure">searches and seizures</a> and sets out requirements for <a wiki="Search_warrant">search warrants</a> based on <a wiki="Probable_cause">probable cause</a></description>
  </amendment>
  <amendment wiki="Fifth_Amendment_to_the_United_States_Constitution" number="5" proposed="1789-09-25" completed="1791-12-15">
    <description>Sets out rules for <a wiki="Indictment">indictment</a> by <a wiki="Grand_jury">grand jury</a> and <a wiki="Eminent_domain#United_States">eminent domain</a>, protects the right to <a wiki="Due_process">due process</a>, and prohibits <a wiki="Self-incrimination">self-incrimination</a> and <a wiki="Double_jeopardy">double jeopardy</a></description>
  </amendment>
  <amendment wiki="Sixth_Amendment_to_the_United_States_Constitution" number="6" proposed="1789-09-25" completed="1791-12-15">
    <description>Protects the right to a <a wiki="Speedy_trial">speedy</a> <a wiki="Public_trial">public</a> <a wiki="Jury_trial">trial by jury</a>, to notification of <a wiki="Criminal_accusation">criminal accusations</a>, to <a wiki="Confrontation_Clause">confront the accuser</a>, to <a wiki="Subpoena">obtain witnesses</a> and to retain <a wiki="Counsel">counsel</a></description>
  </amendment>
  <amendment wiki="Seventh_Amendment_to_the_United_States_Constitution" number="7" proposed="1789-09-25" completed="1791-12-15">
    <description>Provides for the right to a <a wiki="Jury_trial">jury trial</a> in <a wiki="Lawsuit">civil lawsuits</a></description>
  </amendment>
  <amendment wiki="Eighth_Amendment_to_the_United_States_Constitution" number="8" proposed="1789-09-25" completed="1791-12-15">
    <description>Prohibits excessive <a wiki="Fine_(penalty)">fines</a> and excessive <a wiki="Bail">bail</a>, as well as <a wiki="Cruel_and_unusual_punishment">cruel and unusual punishment</a></description>
  </amendment>
  <amendment wiki="Ninth_Amendment_to_the_United_States_Constitution" number="9" proposed="1789-09-25" completed="1791-12-15">
    <description>States that <a wiki="Unenumerated_rights#In_the_United_States">rights not enumerated</a> in the Constitution are retained by the people</description>
  </amendment>
  <amendment wiki="Tenth_Amendment_to_the_United_States_Constitution" number="10" proposed="1789-09-25" completed="1791-12-15">
    <description>States that the <a wiki="Federal_government_of_the_United_States">federal government</a> possesses only those powers delegated, or enumerated, to it through the Constitution, and that all other powers are reserved to the States, or to the people.</description>
  </amendment>
  <amendment wiki="Eleventh_Amendment_to_the_United_States_Constitution" number="11" proposed="1794-03-04" completed="1795-02-07">
    <description>Makes states immune from suits from out-of-state citizens and foreigners not living within the state borders; lays the foundation for <a wiki="Sovereign_immunity#State_sovereign_immunity">state sovereign immunity</a></description>
  </amendment>
  <amendment wiki="Twelfth_Amendment_to_the_United_States_Constitution" number="12" proposed="1803-12-09" completed="1804-06-15">
    <description>Revises <a wiki="United_States_presidential_election">presidential election</a> procedures by having the <a wiki="President_of_the_United_States">president</a> and vice president elected together as opposed to the <a wiki="Vice_President_of_the_United_States">vice president</a> being the runner up in the presidential election</description>
  </amendment>
  <amendment wiki="Thirteenth_Amendment_to_the_United_States_Constitution" number="13" proposed="1865-01-31" completed="1865-12-06">
    <description>Abolishes <a wiki="Slavery_in_the_United_States">slavery</a>, and <a wiki="Involuntary_servitude">involuntary servitude</a>, except <a wiki="Penal_labor_in_the_United_States">as punishment for a crime</a></description>
  </amendment>
  <amendment wiki="Fourteenth_Amendment_to_the_United_States_Constitution" number="14" proposed="1866-06-13" completed="1868-07-09">
    <description>Defines <a wiki="Citizenship_of_the_United_States">citizenship</a>, contains the <a wiki="Privileges_or_Immunities_Clause">Privileges or Immunities Clause</a>, the <a wiki="Fourteenth_Amendment_to_the_United_States_Constitution#Due_Process_Clause">Due Process Clause</a>, and the <a wiki="Equal_Protection_Clause">Equal Protection Clause</a>, and deals with post–<a wiki="American_Civil_War">Civil War</a> issues</description>
  </amendment>
  <amendment wiki="Fifteenth_Amendment_to_the_United_States_Constitution" number="15" proposed="1869-02-26" completed="1870-02-03">
    <description>Prohibits the denial of <a wiki="Suffrage">the right to vote</a> based on race, color or previous condition of servitude</description>
  </amendment>
  <amendment wiki="Sixteenth_Amendment_to_the_United_States_Constitution" number="16" proposed="1909-07-12" completed="1913-02-03">
    <description>Permits Congress to levy an <a wiki="Income_tax">income tax</a> without apportioning it among the various states or basing it on the <a wiki="United_States_Census" class="mw-redirect">United States Census</a></description>
  </amendment>
  <amendment wiki="Seventeenth_Amendment_to_the_United_States_Constitution" number="17" proposed="1912-05-13" completed="1913-04-08">
    <description>Establishes the direct election of <a wiki="United_States_Senate">United States senators</a> by <a wiki="Direct_election">popular vote</a></description>
  </amendment>
  <amendment wiki="Eighteenth_Amendment_to_the_United_States_Constitution" number="18" proposed="1917-12-18" completed="1919-01-16">
    <description>Prohibited the <a wiki="Prohibition_in_the_United_States">manufacturing or sale of alcohol</a> within the United States<br/>(Repealed December 5, 1933, via the 21st Amendment)</description>
  </amendment>
  <amendment wiki="Nineteenth_Amendment_to_the_United_States_Constitution" number="19" proposed="1919-06-04" completed="1920-08-18">
    <description>Prohibits the denial of <a wiki="Women's_suffrage_in_the_United_States">the right to vote based on sex</a></description>
  </amendment>
  <amendment wiki="Twentieth_Amendment_to_the_United_States_Constitution" number="20" proposed="1932-03-02" completed="1933-01-23">
    <description>Changes the dates on which the terms of the president and vice president, and of members of Congress, begin and end, to January 20 and January 3 respectively. States that if the <a wiki="President-elect_of_the_United_States">president-elect</a> dies before taking office, the <a wiki="Vice_President-elect_of_the_United_States">vice president–elect</a> is to be inaugurated as President.</description>
  </amendment>
  <amendment wiki="Twenty-first_Amendment_to_the_United_States_Constitution" number="21" proposed="1933-02-20" completed="1933-12-05">
    <description>Repeals the 18th Amendment and makes it a federal offense to transport or import intoxicating liquors into U.S. states and territories where such is prohibited by law</description>
  </amendment>
  <amendment wiki="Twenty-second_Amendment_to_the_United_States_Constitution" number="22" proposed="1947-03-21" completed="1951-02-27">
    <description><a wiki="Term_limits_in_the_United_States#President">Limits</a> the number of times a person can be elected president.</description>
  </amendment>
  <amendment wiki="Twenty-third_Amendment_to_the_United_States_Constitution" number="23" proposed="1960-06-16" completed="1961-03-29">
    <description>Grants the <a wiki="Washington,_D.C.">District of Columbia</a> electors in the <a wiki="United_States_Electoral_College">Electoral College</a></description>
  </amendment>
  <amendment wiki="Twenty-fourth_Amendment_to_the_United_States_Constitution" number="24" proposed="1962-09-14" completed="1964-01-23">
    <description>Prohibits the revocation of voting rights due to the non-payment of a <a wiki="Poll_tax_(United_States)" class="mw-redirect">poll tax</a> or any other tax</description>
  </amendment>
  <amendment wiki="Twenty-fifth_Amendment_to_the_United_States_Constitution" number="25" proposed="1965-07-06" completed="1967-02-10">
    <description>Addresses <a wiki="United_States_presidential_line_of_succession">succession to the presidency</a> and establishes procedures both for filling a vacancy in the office of the vice president and responding to presidential disabilities</description>
  </amendment>
  <amendment wiki="Twenty-sixth_Amendment_to_the_United_States_Constitution" number="26" proposed="1971-03-23" completed="1971-07-01">
    <description>Prohibits the denial of the right of US citizens, 18 years of age or older, to vote on account of age</description>
  </amendment>
  <amendment wiki="Twenty-seventh_Amendment_to_the_United_States_Constitution" number="27" proposed="1789-09-25" completed="1992-05-07">
    <description>Delays laws affecting Congressional salary from taking effect until after the next election of <a wiki="United_States_House_of_Representatives">representatives</a></description>
  </amendment>
</us-constitution>
`
}
