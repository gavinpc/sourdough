{
 globalThis.XML_us_presidents = `<us-presidents>
  <term number="1" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Gilbert_Stuart_Williamstown_Portrait_of_George_Washington.jpg/150px-Gilbert_Stuart_Williamstown_Portrait_of_George_Washington.jpg" incumbent="George_Washington" term-start="1789-04-30" term-end="1797-03-04" party=""><election ref="1788&#x2013;1789_United_States_presidential_election" dates="1788/1789"/>

<election ref="1792_United_States_presidential_election" dates="1792"/></term>
  <term number="2" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/0/07/John_Adams_A18236.jpg/150px-John_Adams_A18236.jpg" incumbent="John_Adams" term-start="1797-03-04" term-end="1801-03-04" party="Federalist_Party">
    <election ref="1796_United_States_presidential_election" dates="1796"/>
  </term>
  <term number="3" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1e/Thomas_Jefferson_by_Rembrandt_Peale%2C_1800.jpg/150px-Thomas_Jefferson_by_Rembrandt_Peale%2C_1800.jpg" incumbent="Thomas_Jefferson" term-start="1801-03-04" term-end="1809-03-04" party="Democratic-Republican_Party"><election ref="1800_United_States_presidential_election" dates="1800"/>

<election ref="1804_United_States_presidential_election" dates="1804"/></term>
  <term number="4" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1d/James_Madison.jpg/150px-James_Madison.jpg" incumbent="James_Madison" term-start="1809-03-04" term-end="1817-03-04" party="Democratic-Republican_Party"><election ref="1808_United_States_presidential_election" dates="1808"/>

<election ref="1812_United_States_presidential_election" dates="1812"/></term>
  <term number="5" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/James_Monroe_White_House_portrait_1819.jpg/150px-James_Monroe_White_House_portrait_1819.jpg" incumbent="James_Monroe" term-start="1817-03-04" term-end="1825-03-04" party="Democratic-Republican_Party"><election ref="1816_United_States_presidential_election" dates="1816"/>

<election ref="1820_United_States_presidential_election" dates="1820"/></term>
  <term number="6" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4f/John_Quincy_Adams_by_Charles_Osgood.jpg/150px-John_Quincy_Adams_by_Charles_Osgood.jpg" incumbent="John_Quincy_Adams" term-start="1825-03-04" term-end="1829-03-04" party="Democratic-Republican_Party">
    <election ref="1824_United_States_presidential_election" dates="1824"/>
  </term>
  <term number="7" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/4/43/Andrew_jackson_head.jpg/150px-Andrew_jackson_head.jpg" incumbent="Andrew_Jackson" term-start="1829-03-04" term-end="1837-03-04" party="Democratic_Party_(United_States)"><election ref="1828_United_States_presidential_election" dates="1828"/>

<election ref="1832_United_States_presidential_election" dates="1832"/></term>
  <term number="8" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Martin_Van_Buren_circa_1837_crop.jpg/150px-Martin_Van_Buren_circa_1837_crop.jpg" incumbent="Martin_Van_Buren" term-start="1837-03-04" term-end="1841-03-04" party="Democratic_Party_(United_States)">
    <election ref="1836_United_States_presidential_election" dates="1836"/>
  </term>
  <term number="9" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/William_Henry_Harrison_by_James_Reid_Lambdin%2C_1835_crop.jpg/150px-William_Henry_Harrison_by_James_Reid_Lambdin%2C_1835_crop.jpg" incumbent="William_Henry_Harrison" term-start="1841-03-04" term-end="1841-04-04" party="Whig_Party_(United_States)">
    <election ref="1840_United_States_presidential_election" dates="1840"/>
  </term>
  <term number="10" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/John_Tyler_crop.jpg/150px-John_Tyler_crop.jpg" incumbent="John_Tyler" term-start="1841-04-04" term-end="" party="Whig_Party_(United_States)">–</term>
  <term number="11" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/JKP.jpg/150px-JKP.jpg" incumbent="James_K._Polk" term-start="1845-03-04" term-end="1849-03-04" party="Democratic_Party_(United_States)">
    <election ref="1844_United_States_presidential_election" dates="1844"/>
  </term>
  <term number="12" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Zachary_Taylor_restored_and_cropped.jpg/150px-Zachary_Taylor_restored_and_cropped.jpg" incumbent="Zachary_Taylor" term-start="1849-03-04" term-end="1850-07-09" party="Whig_Party_(United_States)">
    <election ref="1848_United_States_presidential_election" dates="1848"/>
  </term>
  <term number="13" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Fillmore.jpg/150px-Fillmore.jpg" incumbent="Millard_Fillmore" term-start="1850-07-09" term-end="1853-03-04" party="Whig_Party_(United_States)">–</term>
  <term number="14" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/Mathew_Brady_-_Franklin_Pierce_-_alternate_crop_%28cropped%29.jpg/150px-Mathew_Brady_-_Franklin_Pierce_-_alternate_crop_%28cropped%29.jpg" incumbent="Franklin_Pierce" term-start="1853-03-04" term-end="1857-03-04" party="Democratic_Party_(United_States)">
    <election ref="1852_United_States_presidential_election" dates="1852"/>
  </term>
  <term number="15" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/f/fd/James_Buchanan.jpg/150px-James_Buchanan.jpg" incumbent="James_Buchanan" term-start="1857-03-04" term-end="1861-03-04" party="Democratic_Party_(United_States)">
    <election ref="1856_United_States_presidential_election" dates="1856"/>
  </term>
  <term number="16" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/a/ab/Abraham_Lincoln_O-77_matte_collodion_print.jpg/150px-Abraham_Lincoln_O-77_matte_collodion_print.jpg" incumbent="Abraham_Lincoln" term-start="1861-03-04" term-end="1865-04-15" party="Republican_Party_(United_States)"><election ref="1860_United_States_presidential_election" dates="1860"/>

<election ref="1864_United_States_presidential_election" dates="1864"/></term>
  <term number="17" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/Andrew_Johnson_photo_portrait_head_and_shoulders%2C_c1870-1880-Edit1.jpg/150px-Andrew_Johnson_photo_portrait_head_and_shoulders%2C_c1870-1880-Edit1.jpg" incumbent="Andrew_Johnson" term-start="1865-04-15" term-end="1869-03-04" party="National_Union_Party_(United_States)">–</term>
  <term number="18" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Ulysses_S_Grant_by_Brady_c1870-restored.jpg/150px-Ulysses_S_Grant_by_Brady_c1870-restored.jpg" incumbent="Ulysses_S._Grant" term-start="1869-03-04" term-end="1877-03-04" party="Republican_Party_(United_States)"><election ref="1868_United_States_presidential_election" dates="1868"/>

<election ref="1872_United_States_presidential_election" dates="1872"/></term>
  <term number="19" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/President_Rutherford_Hayes_1870_-_1880_Restored.jpg/150px-President_Rutherford_Hayes_1870_-_1880_Restored.jpg" incumbent="Rutherford_B._Hayes" term-start="1877-03-04" term-end="1881-03-04" party="Republican_Party_(United_States)">
    <election ref="1876_United_States_presidential_election" dates="1876"/>
  </term>
  <term number="20" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1f/James_Abram_Garfield%2C_photo_portrait_seated.jpg/150px-James_Abram_Garfield%2C_photo_portrait_seated.jpg" incumbent="James_A._Garfield" term-start="1881-03-04" term-end="1881-09-19" party="Republican_Party_(United_States)">
    <election ref="1880_United_States_presidential_election" dates="1880"/>
  </term>
  <term number="21" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/7/79/Chester_Alan_Arthur.jpg/150px-Chester_Alan_Arthur.jpg" incumbent="Chester_A._Arthur" term-start="1881-09-19" term-end="1885-03-04" party="Republican_Party_(United_States)">–</term>
  <term number="22" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Grover_Cleveland_-_NARA_-_518139_%28cropped%29.jpg/150px-Grover_Cleveland_-_NARA_-_518139_%28cropped%29.jpg" incumbent="Grover_Cleveland" term-start="1885-03-04" term-end="1889-03-04" party="Democratic_Party_(United_States)">
    <election ref="1884_United_States_presidential_election" dates="1884"/>
  </term>
  <term number="23" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f8/Benjamin_Harrison%2C_head_and_shoulders_bw_photo%2C_1896.jpg/150px-Benjamin_Harrison%2C_head_and_shoulders_bw_photo%2C_1896.jpg" incumbent="Benjamin_Harrison" term-start="1889-03-04" term-end="1893-03-04" party="Republican_Party_(United_States)">
    <election ref="1888_United_States_presidential_election" dates="1888"/>
  </term>
  <term number="24" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/f/f3/Grover_Cleveland_-_NARA_-_518139_%28cropped%29.jpg/150px-Grover_Cleveland_-_NARA_-_518139_%28cropped%29.jpg" incumbent="Grover_Cleveland" term-start="1893-03-04" term-end="1897-03-04" party="Democratic_Party_(United_States)">
    <election ref="1892_United_States_presidential_election" dates="1892"/>
  </term>
  <term number="25" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/6/6d/Mckinley.jpg/150px-Mckinley.jpg" incumbent="William_McKinley" term-start="1897-03-04" term-end="1901-09-14" party="Republican_Party_(United_States)"><election ref="1896_United_States_presidential_election" dates="1896"/>

<election ref="1900_United_States_presidential_election" dates="1900"/></term>
  <term number="26" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1c/President_Roosevelt_-_Pach_Bros.jpg/150px-President_Roosevelt_-_Pach_Bros.jpg" incumbent="Theodore_Roosevelt" term-start="1901-09-14" term-end="1909-03-04" party="Republican_Party_(United_States)">–

<election ref="1904_United_States_presidential_election" dates="1904"/></term>
  <term number="27" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/William_Howard_Taft_-_Harris_and_Ewing.jpg/150px-William_Howard_Taft_-_Harris_and_Ewing.jpg" incumbent="William_Howard_Taft" term-start="1909-03-04" term-end="1913-03-04" party="Republican_Party_(United_States)">
    <election ref="1908_United_States_presidential_election" dates="1908"/>
  </term>
  <term number="28" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Thomas_Woodrow_Wilson%2C_Harris_%26_Ewing_bw_photo_portrait%2C_1919.jpg/150px-Thomas_Woodrow_Wilson%2C_Harris_%26_Ewing_bw_photo_portrait%2C_1919.jpg" incumbent="Woodrow_Wilson" term-start="1913-03-04" term-end="1921-03-04" party="Democratic_Party_(United_States)"><election ref="1912_United_States_presidential_election" dates="1912"/>

<election ref="1916_United_States_presidential_election" dates="1916"/></term>
  <term number="29" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c4/Warren_G_Harding-Harris_%26_Ewing.jpg/150px-Warren_G_Harding-Harris_%26_Ewing.jpg" incumbent="Warren_G._Harding" term-start="1921-03-04" term-end="1923-08-02" party="Republican_Party_(United_States)">
    <election ref="1920_United_States_presidential_election" dates="1920"/>
  </term>
  <term number="30" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a3/Calvin_Coolidge_cph.3g10777_%28cropped%29.jpg/150px-Calvin_Coolidge_cph.3g10777_%28cropped%29.jpg" incumbent="Calvin_Coolidge" term-start="1923-08-02" term-end="1929-03-04" party="Republican_Party_(United_States)">–

<election ref="1924_United_States_presidential_election" dates="1924"/></term>
  <term number="31" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/President_Hoover_portrait.jpg/150px-President_Hoover_portrait.jpg" incumbent="Herbert_Hoover" term-start="1929-03-04" term-end="1933-03-04" party="Republican_Party_(United_States)">
    <election ref="1928_United_States_presidential_election" dates="1928"/>
  </term>
  <term number="32" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/FDR_1944_Color_Portrait.jpg/150px-FDR_1944_Color_Portrait.jpg" incumbent="Franklin_D._Roosevelt" term-start="1933-03-04" term-end="1945-04-12" party="Democratic_Party_(United_States)"><election ref="1932_United_States_presidential_election" dates="1932"/>

<election ref="1936_United_States_presidential_election" dates="1936"/>

<election ref="1940_United_States_presidential_election" dates="1940"/>

<election ref="1944_United_States_presidential_election" dates="1944"/></term>
  <term number="33" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/TRUMAN_58-766-06_%28cropped%29.jpg/150px-TRUMAN_58-766-06_%28cropped%29.jpg" incumbent="Harry_S._Truman" term-start="1945-04-12" term-end="1953-01-20" party="Democratic_Party_(United_States)">–

<election ref="1948_United_States_presidential_election" dates="1948"/></term>
  <term number="34" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/6/63/Dwight_D._Eisenhower%2C_official_photo_portrait%2C_May_29%2C_1959.jpg/150px-Dwight_D._Eisenhower%2C_official_photo_portrait%2C_May_29%2C_1959.jpg" incumbent="Dwight_D._Eisenhower" term-start="1953-01-20" term-end="1961-01-20" party="Republican_Party_(United_States)"><election ref="1952_United_States_presidential_election" dates="1952"/>

<election ref="1956_United_States_presidential_election" dates="1956"/></term>
  <term number="35" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/John_F._Kennedy%2C_White_House_color_photo_portrait.jpg/150px-John_F._Kennedy%2C_White_House_color_photo_portrait.jpg" incumbent="John_F._Kennedy" term-start="1961-01-20" term-end="1963-11-22" party="Democratic_Party_(United_States)">
    <election ref="1960_United_States_presidential_election" dates="1960"/>
  </term>
  <term number="36" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/37_Lyndon_Johnson_3x4.jpg/150px-37_Lyndon_Johnson_3x4.jpg" incumbent="Lyndon_B._Johnson" term-start="1963-11-22" term-end="1969-01-20" party="Democratic_Party_(United_States)">–

<election ref="1964_United_States_presidential_election" dates="1964"/></term>
  <term number="37" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Richard_Nixon_presidential_portrait_%281%29.jpg/150px-Richard_Nixon_presidential_portrait_%281%29.jpg" incumbent="Richard_Nixon" term-start="1969-01-20" term-end="1974-08-09" party="Republican_Party_(United_States)"><election ref="1968_United_States_presidential_election" dates="1968"/>

<election ref="1972_United_States_presidential_election" dates="1972"/></term>
  <term number="38" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/4/44/Gerald_Ford_presidential_portrait_%28cropped_3%29.jpg/150px-Gerald_Ford_presidential_portrait_%28cropped_3%29.jpg" incumbent="Gerald_Ford" term-start="1974-08-09" term-end="1977-01-20" party="Republican_Party_(United_States)">–</term>
  <term number="39" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/JimmyCarterPortrait2.jpg/150px-JimmyCarterPortrait2.jpg" incumbent="Jimmy_Carter" term-start="1977-01-20" term-end="1981-01-20" party="Democratic_Party_(United_States)">
    <election ref="1976_United_States_presidential_election" dates="1976"/>
  </term>
  <term number="40" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Official_Portrait_of_President_Reagan_1981.jpg/150px-Official_Portrait_of_President_Reagan_1981.jpg" incumbent="Ronald_Reagan" term-start="1981-01-20" term-end="1989-01-20" party="Republican_Party_(United_States)"><election ref="1980_United_States_presidential_election" dates="1980"/>

<election ref="1984_United_States_presidential_election" dates="1984"/></term>
  <term number="41" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/e/ee/George_H._W._Bush_presidential_portrait_%28cropped%29.jpg/150px-George_H._W._Bush_presidential_portrait_%28cropped%29.jpg" incumbent="George_H._W._Bush" term-start="1989-01-20" term-end="1993-01-20" party="Republican_Party_(United_States)">
    <election ref="1988_United_States_presidential_election" dates="1988"/>
  </term>
  <term number="42" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Bill_Clinton.jpg/150px-Bill_Clinton.jpg" incumbent="Bill_Clinton" term-start="1993-01-20" term-end="2001-01-20" party="Democratic_Party_(United_States)"><election ref="1992_United_States_presidential_election" dates="1992"/>

<election ref="1996_United_States_presidential_election" dates="1996"/></term>
  <term number="43" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/George-W-Bush.jpeg/150px-George-W-Bush.jpeg" incumbent="George_W._Bush" term-start="2001-01-20" term-end="2009-01-20" party="Republican_Party_(United_States)"><election ref="2000_United_States_presidential_election" dates="2000"/>

<election ref="2004_United_States_presidential_election" dates="2004"/></term>
  <term number="44" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/5/55/President_Barack_Obama%2C_2012_portrait_crop.jpg/150px-President_Barack_Obama%2C_2012_portrait_crop.jpg" incumbent="Barack_Obama" term-start="2009-01-20" term-end="2017-01-20" party="Democratic_Party_(United_States)"><election ref="2008_United_States_presidential_election" dates="2008"/>

<election ref="2012_United_States_presidential_election" dates="2012"/></term>
  <term number="45" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Donald_Trump_official_portrait_%28cropped%29.jpg/150px-Donald_Trump_official_portrait_%28cropped%29.jpg" incumbent="Donald_Trump" term-start="2017-01-20" term-end="2021-01-20" party="Republican_Party_(United_States)">
    <election ref="2016_United_States_presidential_election" dates="2016"/>
  </term>
  <term number="46" portrait="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Joe_Biden_presidential_portrait_%28cropped%29.jpg/150px-Joe_Biden_presidential_portrait_%28cropped%29.jpg" incumbent="Joe_Biden" term-start="2021-01-20" term-end="" party="Democratic_Party_(United_States)">
    <election ref="2020_United_States_presidential_election" dates="2020"/>
  </term>
</us-presidents>
`
}
