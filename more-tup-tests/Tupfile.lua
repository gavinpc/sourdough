tup.creategitignore()

Windows = tup.getconfig('TUP_PLATFORM') == 'win32'

function posix_quote(s)
  return "'"..s:gsub("'", "'\"'\"'").."'"
end

-- Does not work on Windows!
function test_with_hardcoded_quoting()
  if Windows then return end
  tup.definerule {
    inputs = {'input/some file'},
    command = "cat '%f' | tr '[:lower:]' '[:upper:]' > '%o'",
    outputs = {'output/%b'}
  }
end

-- Does not work on Windows!
function test_with_safe_quoting()
  if Windows then return end
  local name = 'some file'
  local in_dir = 'input'
  local out_dir = 'output'
  local input = in_dir..'/'..name
  local output = out_dir..'/'..name
  tup.definerule {
    inputs = {input},
    command = "cat "..posix_quote(input).." | tr '[:lower:]' '[:upper:]' > "..posix_quote(output),
    outputs = {output}
  }
end

function does_tup_call_sh()
  tup.definerule {
    command = 'dot graphviz/basic.dot -Tsvg -ographviz/basic-needle.svg -Glabel="needle"',
    outputs = {'graphviz/basic-needle.svg'}
  }
  tup.definerule {
    command = 'dot graphviz/basic.dot -Tsvg -ographviz/basic-42.svg -Glabel="$((41+1))"',
    outputs = {'graphviz/basic-42.svg'}
  }
  tup.definerule {
    command = 'dot graphviz/basic.dot -Tsvg -ographviz/basic-unix-path.svg -Glabel="${PATH}"',
    outputs = {'graphviz/basic-unix-path.svg'}
  }
  tup.definerule {
    command = 'dot graphviz/basic.dot -Tsvg -ographviz/basic-dos-path.svg -Glabel="%%PATH%%"',
    outputs = {'graphviz/basic-dos-path.svg'}
  }
  -- invokes WSL on this Windows, which lacks dot
  if not Windows then
  tup.definerule {
    command = '^b^ dot graphviz/basic.dot -Tsvg -ographviz/basic-bash-42.svg -Glabel="${PATH}"',
    outputs = {'graphviz/basic-bash-42.svg'}
  }
  end
end

function echo_path()
  tup.definerule {
    command = 'echo $PATH',
  }
end

function test_bash_version()
  tup.definerule {
    command = 'bash --version',
  }
  tup.definerule {
    command = 'which -a bash',
  }
  -- a cmd built-in
  -- tup.definerule {
  --   command = 'where bash',
  -- }
  tup.definerule {
    command = 'dot -v',
  }
  -- Crashes because no shell
  -- tup.definerule {
  --   command = 'dot -v ; which -a dot',
  -- }
end

test_with_hardcoded_quoting()
test_with_safe_quoting()
echo_path()
does_tup_call_sh()
test_bash_version()
