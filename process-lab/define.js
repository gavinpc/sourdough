(function def_init() {
  const { assign } = Object;
  const { isArray } = Array;

  const is_special = need => need === "exports" || need === "module";

  function make_world(ns, definitions = new Map()) {
    const goals = [];
    const cache = new Map();
    const dlq = new Set();

    const defined = x => definitions.has(x);
    const available = x => cache.has(x);

    const requiring = [];
    function require_with_check(id) {
      if (requiring.includes(id))
        throw Error(`cycle! ${[...requiring, id].join(" → ")}`);
      requiring.push(id);
      try {
        return require(id);
      } finally {
        requiring.pop();
      }
    }
    function amd_construct(needs, factory) {
      const exports = {};
      const module = { exports };
      const special = { module, exports, require };
      const imports = needs.map(id => special[id] ?? require_with_check(id));
      const result =
        typeof factory === "function" ? factory(...imports) : factory;
      return needs.some(is_special) ? exports : result;
    }

    // discharge results where needs can now be met
    function revisit_goals() {
      let repeat = false;
      do {
        repeat = false;
        for (let i = goals.length - 1; i !== -1; i--) {
          const goal = goals[i];
          if (dlq.has(goal)) continue;
          const [name, needs, factory] = goal;
          if (needs.every(available)) {
            try {
              const result = amd_construct(needs, factory);
              if (name !== null) cache.set(name, result);
              goals.splice(i, 1);
              repeat = true;
            } catch (error) {
              // this shouldn't be a not-found error: all deps were available
              dlq.add(goal);
              console.error(goal, error);
            }
          }
        }
      } while (repeat);
    }

    function define_or_throw_conflict(name, term) {
      // RequireJS does not throw for a redefinition, but also doesn't redefine
      if (defined(name)) throw Error(`Conflict: Already defined: ${name}`);
      definitions.set(name, term); // only place this is done
      revisit_goals();
    }

    function define(...term) {
      switch (term.length) {
        case 1:
          return; // no-op
        case 2: {
          const [name, formula] = term;
          if (typeof name !== "string") {
            throw Error(`NotImplemented: anonymous define`);
          }
          if (typeof formula !== "function") {
            cache.set(name, formula);
            define_or_throw_conflict(name, term);
            return;
          }
          return define(name, [], formula);
        }
        case 3: {
          const [name, needs, formula] = term;
          if (typeof formula !== "function")
            throw Error(`BadRequest: define₃ expects f'n formula`);
          if (!needs.every(available)) goals.push(term);
          else {
            const result = amd_construct(needs, formula);
            cache.set(name, result);
          }
          define_or_throw_conflict(name, term);
          break;
        }
        default:
          throw Error(`BadRequest: no such arity ${term.length}`);
      }
    }

    function require1(it) {
      if (isArray(it)) return it.map(require1); // distribute over array
      if (typeof it !== "string")
        throw Error(`BadRequest: need string or array`);
      if (cache.has(it)) return cache.get(it);
      if (definitions.has(it)) throw Error(`Unmet needs for ${it}`);
      throw Error(`NotFound: ${it}`);
    }

    function require(...term) {
      switch (term.length) {
        case 1:
          return require1(term[0]);
        case 2: {
          const [needs, factory] = term;
          if (needs.every(available)) return factory(...needs.map(require1));
          else goals.push([null, needs, factory]);
          return;
        }
        case 3: {
          const [needs, formula, errback] = term;
          if (typeof errback !== "function")
            throw Error(`BadRequest: expected f'n errback`);
          try {
            return formula(...needs.map(require1));
          } catch (error) {
            console.error(needs, formula, error);
            errback(error);
          }
          return;
        }
        default:
          throw Error(`BadRequest: no such arity ${term.length}`);
      }
    }

    return {
      define: assign(define, { definitions, goals, cache }),
      require,
    };
  }

  const { define, require } = make_world();
  assign(define, { amd: { make_world } });
  assign(globalThis, { define, require });
})();
