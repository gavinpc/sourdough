define("kb notes", [], () => {
  // theories and composition
  // subtheories and extensions
  // a “ground”  is just a collection of factables
  // a “composite” theory is a collection of other theories
  // the result should be a DAG of theories

  // regarding the separation of ground facts and inferences, some language at
  // https://en.wikipedia.org/wiki/Theory_(mathematical_logic)

  // in RDF, a graphs's entailments usually belong to the graph implicitly
  // though with e.g. sparql you could procedurally write inferences to another graph
  // in general though, inferences are indistinguishable from ground truths
  function make_theory() {}
  const namedTheories = new Map();
  const defaultTheory = make_theory();
  const kb = { defaultTheory, namedTheories };
});

define("~/remember/events", ["~/adt/ring_buffers", "~/process/channels"], (
  buffers,
  channels
) => {
  const { make_sliding_buffer } = buffers;
  const { EOF, make_channel } = channels;

  function start_remember_events() {
    const buffer = make_sliding_buffer(16);
    const channel = make_channel();

    async function remember_events() {
      while (true) {
        const value = await channel.take();
        if (value === EOF) break;
      }
    }
    remember_events();
    return { dispose: () => {} };
  }
});

define("~/examples/minimal-terminal-process", [
  "~/process/channels",
], channels => {
  const { EOF } = channels;
  async function reader(input_channel) {
    for (let i = 0; i < 10; i++) {
      const value = await input_channel.take();
      if (value === EOF) break;
      console.log("I read:", value);
    }
  }
  const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
  async function writer(input_channel) {
    while (true) {
      const value = Math.random();
      await input_channel.put(value);
      await sleep(1000);
    }
  }

  {
    const { make_channel, EOF } = channels;
    const a_buffer = null;
    const a_channel = make_channel(a_buffer);

    reader(a_channel);
    writer(a_channel);
  }
});

define("~/examples/naive-flip-dot-scripts", ["nonexistent"], () => {
  async function flip_dot_scripts(scope) {
    const query = `script[type="graphviz:dot"]`;
    const scripts = scope.querySelectorAll(query);
    for (const script of scripts) {
      const viz = (globalThis.viz ??= new Viz());
      const code = script.text;
      try {
        const diagram = await viz.renderSVGElement(code);
        script.insertAdjacentElement("beforebegin", diagram);
      } catch (error) {
        globalThis.viz = null;
        console.warn(`flip_dot_scripts says, ${error}`);
      }
    }
  }
  setTimeout(() => flip_dot_scripts(document.body), 1);
});

define("~/examples/minimal-flip-dot-scripts-process", [
  "~/process/channels",
  "nonexistent",
], channels => {
  async function reader(input_channel) {
    const { EOF } = channels;
    while (true) {
      const viz = (globalThis.viz ??= new Viz());
      const value = await input_channel.take();
      if (value === EOF) break;
      const [code, relation, reference_element] = value;
      try {
        const diagram = await viz.renderSVGElement(code);
        reference_element.insertAdjacentElement(relation, diagram);
      } catch (error) {
        globalThis.viz = null;
        console.warn(`flip_dot_scripts says, ${error}`);
      }
    }
  }
  const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

  async function visit_dot_scripts(input_channel) {
    await sleep(1);
    const scope = document.body;
    const query = `script[type="graphviz:dot"]`;
    const scripts = scope.querySelectorAll(query);
    for (const script of scripts) {
      const code = script.text;
      await input_channel.put([code, "beforebegin", script]);
    }
  }

  {
    const { make_channel } = channels;
    const a_channel = make_channel();
    reader(a_channel);
    visit_dot_scripts(a_channel);
  }
});

define("~/example/repeatedly-functions", ["~/process/channels"], channels => {
  const { EOF } = channels;
  async function read_repeatedly_to(input_channel, sink) {
    while (true) {
      const value = await input_channel.take();
      if (value === EOF) break;
      try {
        sink(value);
      } catch (error) {
        console.warn(`${sink.name} says, ${error}`);
      }
    }
  }
  async function async_iterate_to_channel(seq_thunk, channel) {
    // or check can_put_now and skip await
    for await (const result of seq_thunk()) await channel.put(result);
    channel.put(EOF);
  }

  return { read_repeatedly_to, async_iterate_to_channel };
});

define("~/examples/repeating-flip-dot-scripts-process", [
  "~/process/channels",
  "~/example/repeatedly-functions",
], (channels, repeatedly) => {
  async function render_script(value) {
    const viz = (globalThis.viz ??= new Viz());
    const [code, relation, reference_element] = value;
    let diagram;
    try {
      diagram = await viz.renderSVGElement(code);
    } catch (error) {
      globalThis.viz = null;
      throw error;
    }
    reference_element.insertAdjacentElement(relation, diagram);
  }
  const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));

  async function* visit_dot_scripts() {
    await sleep(1); // need async only for this now
    const scope = document.body;
    const query = `script[type="graphviz:dot"]`;
    const scripts = scope.querySelectorAll(query);
    for (const script of scripts) {
      const code = script.text;
      yield [code, "beforebegin", script];
    }
  }

  {
    const { read_repeatedly_to, async_iterate_to_channel } = repeatedly;
    const { make_channel } = channels;
    const a_channel = make_channel();
    read_repeatedly_to(a_channel, render_script);
    async_iterate_to_channel(visit_dot_scripts, a_channel);
  }
});

define("~/examples/repeating-code-listings-process", [
  "~/process/channels",
  "~/example/repeatedly-functions",
], (channels, repeatedly) => {
  const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
  const defined = name => new Promise(resolve => require([name], resolve));

  const { stringify } = JSON;
  async function render_listing(value) {
    const [name, relation, reference_element] = value;
    await defined(name);
    const details = document.createElement("details");
    const summary = document.createElement("summary");
    const listing = document.createElement("pre");
    const dependencies = document.createElement("ol");
    const term = define.definitions.get(name);
    if (term.length !== 3) throw new Error(`not supported`);
    const [_name, needs, factory] = term;
    const code = `${factory}`;
    details.id = name;
    dependencies.append(
      ...needs.map(ref => {
        const item = document.createElement("li");
        const link = document.createElement("a");
        link.href = `#${ref}`;
        link.append(ref);
        item.append(link);
        return item;
      })
    );
    summary.append(name);
    listing.append(code);
    details.append(summary, dependencies, listing);
    reference_element.insertAdjacentElement(relation, details);
  }

  async function* visit_listing_containers() {
    await sleep(1); // need async only for this now
    const scope = document.body;
    const query = `[data-amd-listing]`;
    const matches = scope.querySelectorAll(query);
    for (const element of matches) {
      const name = element.getAttribute("data-amd-listing");
      yield [name, "afterbegin", element];
    }
  }

  {
    const { read_repeatedly_to, async_iterate_to_channel } = repeatedly;
    const { make_channel } = channels;
    const a_channel = make_channel();
    read_repeatedly_to(a_channel, render_listing);
    async_iterate_to_channel(visit_listing_containers, a_channel);
  }
});
