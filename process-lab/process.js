define("~/process/streams", [], () => {
  const { assert } = console;
  const NOTHING = Symbol("∅");

  function make_subscribable() {
    let _value = NOTHING;
    const subscribers = new Set();

    function subscribe(subscriber) {
      subscribers.add(subscriber);
      function unsubscribe() {
        subscribers.delete(subscriber);
      }
      const subscription = { unsubscribe };
      return subscription;
    }

    // - synchronous
    // - one subscriber cannot break others
    function next(value) {
      _value = value;
      let errors = null;
      for (const subscriber of subscribers) {
        try {
          subscriber.next(value);
        } catch (error) {
          if (typeof subscriber.catch === "function") subscriber.catch(error);
          else {
            if (errors === null) errors = [error];
            else errors.push(error);
          }
        }
      }
      if (errors !== null) {
        throw AggregateError(errors, "uhandled errors in broadcast");
      }
    }
    function has_cache() {
      return _value !== NOTHING;
    }
    function cache() {
      return _value;
    }
    const public = { subscribe };
    function dispose() {
      subscribers.clear(); // and/or make `next` a no-op
    }
    return { dispose, public, next, has_cache, cache };
  }

  return { make_subscribable };
});

define("~/adt/ring_buffers", [], () => {
  class UnprocessableEntity extends Error {}
  const { assert } = console;
  // because there's no point in a zero-length buffer, right??
  // const NULL_BUFFER = { is_empty: () => true, is_full: () => true, length: 0 };

  // protocol:
  // - `is_full` tells you whether `push` will reject
  // - `is_empty` tells you whether `pop` will reject

  const make_fixed_buffer = capacity => {
    if (capacity === 0) throw new Error(`Zero-length buffer is no good!`);
    const buffer = new Array(capacity);
    let start = 0; // aka head, where the first item is (when ≠ end)
    let end = 0; // aka tail, where the next item will go
    let length = 0;

    return {
      is_empty: () => length === 0,
      is_full: () => length === capacity,
      push(value) {
        if (length === capacity) throw new UnprocessableEntity(`Buffer full!`);
        ++length;
        buffer[end] = value;
        end = (end + 1) % capacity;
      },
      pop() {
        if (length === 0) throw new UnprocessableEntity(`Buffer empty!`);
        --length;
        const value = buffer[start];
        start = (start + 1) % capacity;
        return value;
      },
      get capacity() {
        return capacity;
      },
      get length() {
        return length;
      },
    };
  };
  make_fixed_buffer.comment = `An unforgiving, fixed-length FIFO with an Array-like interface`;

  const make_dropping_buffer = capacity => {
    if (capacity === 0) throw new Error(`Zero-length buffer is no good!`);
    // if (capacity === 0) return NULL_BUFFER;
    const buffer = new Array(capacity);
    let start = 0; // aka head, where the first item is (when ≠ end)
    let end = 0; // aka tail, where the next item will go
    let length = 0;

    return {
      is_empty: () => length === 0,
      is_full: () => false, // always accepts
      push(value) {
        if (length === capacity) return; // DISCARD value
        ++length;
        buffer[end] = value;
        end = (end + 1) % capacity;
      },
      pop() {
        assert?.(length !== 0, `Empty!`);
        --length;
        const value = buffer[start];
        start = (start + 1) % capacity;
        return value;
      },
      get length() {
        return length;
      },
    };
  };
  make_dropping_buffer.comment = `A buffer that silently discards new applications when full`;

  const make_sliding_buffer = capacity => {
    if (capacity === 0) throw new Error(`Zero-length buffer is no good!`);
    // if (capacity === 0) return NULL_BUFFER;
    const buffer = new Array(capacity);
    let start = 0; // aka head, where the first item is (when ≠ end)
    let end = 0; // aka tail, where the next item will go
    let length = 0;

    return {
      is_empty: () => length === 0,
      is_full: () => false, // always accepts
      push(value) {
        if (length < capacity) ++length;
        else start = (start + 1) % capacity; // move to OVERWRITE
        buffer[end] = value;
        end = (end + 1) % capacity;
      },
      pop() {
        if (length === 0) throw new UnprocessableEntity(`Buffer empty!`);
        --length;
        const value = buffer[start];
        start = (start + 1) % capacity;
        return value;
      },
      get length() {
        return length;
      },
    };
  };
  make_sliding_buffer.comment = `An always-accepting buffer that overwrites old entries`;

  return { make_fixed_buffer, make_dropping_buffer, make_sliding_buffer };
});

define("~/process/channels", [], () => {
  const { assert } = console;
  // using `nil` as EOF is good enough for Clojure.  Why not use null or undefined for same?
  const EOF = Symbol();

  // `can_put_now` and `can_take_now` answer whether you can put or take
  // synchronously (w/o promise).

  // must `put` return a non-null value? so its okay with Promise.resolve?
  const NOW = Promise.resolve(true);

  function make_unbuffered_channel() {
    const pending_put_values = [];
    const pending_put_resolves = [];
    const pending_takes = [];
    return Object.freeze({
      EOF,
      can_put_now: () => pending_takes.length > 0,
      can_take_now: () => pending_put_values.length > 0,
      put(value) {
        if (pending_takes.length === 0)
          return new Promise(resolve => {
            pending_put_values.push(value);
            pending_put_resolves.push(resolve);
            // if count exceeds expected # of (well-behaved) writers
            // console.debug(`${pending_put_resolves.length} pending puts`);
          });
        pending_takes.shift()(value);
        return NOW;
      },
      take() {
        if (pending_put_values.length === 0)
          return new Promise(resolve => {
            pending_takes.push(resolve);
          });
        const resolve = pending_put_resolves.shift();
        const value = pending_put_values.shift();
        resolve(); // or... in microtask, so it is after receipt of take?
        return value;
      },
    });
  }

  function make_buffered_channel(buffer) {
    const pending_put_values = [];
    const pending_put_resolves = [];
    const pending_takes = [];
    return Object.freeze({
      EOF,
      can_put_now: () => pending_takes.length > 0 || !buffer.is_full(),
      can_take_now: () => pending_put_values.length > 0 || !buffer.is_empty(),
      put(value) {
        const n = pending_takes.length;
        if (n > 0) {
          assert?.(buffer.is_empty(), `${n} pending takes but non-∅ buffer`);
          pending_takes.shift()(value);
        } else if (!buffer.is_full()) buffer.push(value);
        else
          return new Promise(resolve => {
            pending_put_values.push(value);
            pending_put_resolves.push(resolve);
            // if count exceeds expected # of (well-behaved) writers
            // console.debug(`${pending_put_resolves.length} pending puts`);
          });
        return NOW;
      },
      take() {
        if (buffer.is_empty()) {
          if (assert) {
            const n = pending_put_values.length;
            assert(n === 0, `∅ buffer but ${n} pending puts `);
          }
          return new Promise(resolve => {
            pending_takes.push(resolve);
          });
        }

        const value = buffer.pop();
        if (pending_put_values.length > 0) {
          const resolve = pending_put_resolves.shift();
          const value = pending_put_values.shift();
          buffer.push(value);
          resolve();
        }
        return value;
      },
    });
  }

  function make_channel(buffer) {
    if (buffer == null) return make_unbuffered_channel();
    if (buffer.capacity === 0) throw new Error("Need positive capacity!");
    return make_buffered_channel(buffer);
  }
  make_channel.comment = `A minimal blocking queue.`;
  return { make_channel };
});

define("~/process/channels/tests", [
  "~/adt/ring_buffers",
  "~/process/channels",
], (buffers, channels) => {
  const { assert } = console;
  const { make_channel } = channels;
  const { make_fixed_buffer } = buffers;
  const resolve = Promise.resolve.bind(Promise);

  async function test_unbuffered_channel() {
    const channel = make_channel();
    assert(channel.can_put_now() === false);
    assert(channel.can_take_now() === false);
    resolve(channel.put("one")).then(what => console.log("put 1 says", what));
    resolve(channel.take()).then(what => console.log("take 1 says", what));
  }
  test_unbuffered_channel();
});
