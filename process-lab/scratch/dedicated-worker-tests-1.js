function worker_test_1() {
  function main(system) {
    // these both appear as "[object DedicatedWorkerGlobalScope]"
    // ah but this won't work in a worker... you have to post back
    // so what, you buffer the calls, then
    // const { console, element } = dom_console();
    // globalThis.console = console;
    console.info(`Worker here.  Hi. `, self, globalThis);
    postMessage("bruh");
  }
  const code = `
${main}
main()
`;
  const blob = new Blob([code], { type: "text/javascript" });
  const url = URL.createObjectURL(blob);
  const worker = new Worker(url);

  worker.onmessage = function handle_message(message) {
    console.info(`Oi! message from worker`, message);
  };
  worker.onerror = function handle_error(error) {
    console.error(
      `Worker reports a problem at line ${error.lineno}!`,
      error.message
    );
  };
  worker.onmessageerror = function handle_error(error) {
    console.error(`Worker reports a... messageerror?!`, error);
  };
}
