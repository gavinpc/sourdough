{
  // create a worker & an interactive process that exchanges messages with it
  function chat_with_worker() {
    const container = document.querySelector("main") ?? document.body;
    const element = document.createElement("div");
    element.classList.add("chat");
    const input = document.createElement("input");
    element.append(input);
    container.append(element);

    function think(something) {
      worker.postMessage(something);
    }
    function say(something) {
      const p = document.createElement("p");
      p.textContent = `${something}`;
      element.append(p);
      worker.postMessage(something);
    }

    input.oninput = function chat_input(event) {
      const text = event.target.value;
      think(text);
    };
    input.onkeypress = function input_keypress(event) {
      const { key } = event;
      if (key === "Enter") {
        const text = event.target.value;
        say(text);
      }
    };

    function chat() {
      // document, Document, Element are not in scope here & references crash
      console.info(self === globalThis);
      console.info(self.document);
      console.info(self.navigator);
      console.info(self.location);
      console.info(`from worker`, globalThis.Document);
      // Element; // crashes: Element is not defined
      onmessage = function worker_handle_message(event) {
        // queue or cue NON-DOM work
        const { data, origin, lastEventId, source } = event;
        // console.info({ data, origin, lastEventId, source });
        if (Array.isArray(data)) {
        }
        postMessage(`what I hear you saying is ‘${data}’. `);
      };
    }
    const code = `
${chat}
chat();
`;
    const blob = new Blob([code], { type: "text/javascript" });
    const url = URL.createObjectURL(blob);
    const worker = new Worker(url);
    // w/o this, errors are logged to console
    worker.onerror = function worker_onerror(event) {
      const { colno, error, filename, lineno, message } = event;
      const output = document.createElement("output");
      const p = document.createElement("p");
      output.append(p);
      p.textContent = `Error: ${message}`;
      element.append(output);
      event.preventDefault(); // prevents logging to console
    };
    worker.onmessage = function worker_onmessage(event) {
      const output = document.createElement("output");
      const { data, origin, lastEventId, source, timeStamp } = event;
      output.textContent = `Message: ${data}`;
      element.append(output);
    };
    // also available
    // worker.rejectionhandled = function worker_onrejectionhandled(event) {};
    // worker.unhandledrejection = function worker_onunhandledrejection(event) {};
    return { dispose: () => worker.terminate() };
  }
  chat_with_worker();
}
