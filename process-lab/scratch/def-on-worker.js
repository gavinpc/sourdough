function def_init() {
  globalThis[Symbol("https://def.codes")] = def_init;
  if (globalThis.def) {
    console.log("Pssst, conflict def");
    return;
  }

  // described by, etc
  // https://www.rfc-editor.org/rfc/rfc9110.html#name-400-bad-request
  // once again, MDN does it right:
  // https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/100
  // hence
  // x∈HttpStatusCode → x describedBy `https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/${x}`

  // HTTP equiv 400
  const { defineProperty } = Object;
  class BadRequest extends Error {}
  defineProperty(BadRequest.prototype, "name", {
    enumerable: true,
    value: "BadRequest",
  });
  // HTTP equiv 404
  class NotFound extends Error {}
  defineProperty(NotFound.prototype, "name", {
    enumerable: true,
    value: "NotFound",
  });
  // HTTP equiv 409
  class Conflict extends Error {}
  defineProperty(Conflict.prototype, "name", {
    enumerable: true,
    value: "Conflict",
  });
  // HTTP equiv 501
  class NotImplemented extends Error {}
  defineProperty(NotImplemented.prototype, "name", {
    enumerable: true,
    value: "NotImplemented",
  });

  {
    const { create, keys } = Object;
    function map_object(input, f, type = Object) {
      const output = create(type);
      for (const key of keys(input)) {
        output[key] = f(input[key], key, input);
      }
      return output;
    }
    map_object.comment = `Map a function over an object's values into a new plain object.`;
  }

  {
    const { getPrototypeOf, prototype: Object_prototype } = Object;
    function is_plain_object(o) {
      if (o === null || typeof o !== "object") return false;
      const prototype = getPrototypeOf(o);
      return prototype === Object_prototype || prototype === null;
    }
    is_plain_object.comment = `Answer true if the argument has Object or null prototype.`;
  }

  // pseudo-generic
  {
    const { isArray } = Array;
    function map(thing, f) {
      // @@species is deprecated and I'm not sure that's not wrong...
      // can we say that a mapped T will make sense as a T?
      if (isArray(thing)) return thing.map(f);
      if (is_plain_object(thing)) return map_object(thing, f);
      throw new NotImplemented(map, thing, f);
    }
  }

  function Map_memoize1(fun, name = fun.name, cache = new Map()) {
    return {
      [name]: function (k) {
        if (cache.has(k)) return cache.get(k);
        const result = fun(k);
        cache.set(k, result);
        return result;
      },
    }[name];
  }
  Map_memoize1.comment = `A no-eviction cache for synchronous unary functions.`;

  function coiner(fun) {
    return new Proxy(Object.create(null), {
      get(target, key, receiver) {
        if (typeof key === "string") return fun(key);
        // system reserves the right to intercept symbols for its own interest
        return Reflect.get(target, key, receiver);
      },
    });
  }
  coiner.comment = `Make an object where every (string) property is answered by the same function, which is given the requested key.  The returned object thus has an “index signature” described by the function.`;

  class Name extends URL {} // MUTABLE!!

  // does NOT recognize prefixes!  anything before colon is treated as scheme
  const name = s => new Name(s);

  const { create, assign, freeze } = Object;
  const { construct } = Reflect;
  const { toPrimitive, toStringTag } = Symbol;
  const Symbol_for = Symbol.for;
  const NO_ARGUMENTS = freeze([]);

  const mint = {
    comment: `Some experimental tools`,
    in(ns) {
      name(ns); // throws if the name is not acceptable
      return {
        names: coiner(k => `${ns}${k}`),
        urls: coiner(k => name(`${ns}${k}`)),
        symbols: coiner(k => Symbol_for(`${ns}${k}`)),
        tuples: coiner(Map_memoize1(k => TupleKind(`${ns}${k}`, k))),
        records: coiner(Map_memoize1(k => RecordKind(`${ns}${k}`, k))),
      };
    },
  };

  class TaggedRecord {}
  class TaggedTuple extends Array {
    constructor(...args) {
      // Unlike `Array` constructor, args are *always* elements, never `length`.
      super();
      this.push(...args);
    }
    // Prevent crash on `map`, `filter`, etc due to object being frozen
    // But see warning at https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/@@species
    // also freeze is gone and will be opt-in
    /*
    static get [Symbol.species]() {
      return Array;
    }
    */
    // WIP
    toString(...args) {
      return `def.TupleKind("${name}")(${this.map(function (x) {
        return x.toString(...args);
      }).join(",")})`;
    }
  }

  // expect percent-encoded here?
  // this and TupleKind do not currently sanitize names, but the mint wrapper does
  const RecordKind = function (name) {
    const constructor = {
      [name]: function (initial) {
        const it = construct(TaggedRecord, NO_ARGUMENTS, constructor);
        if (initial) assign(it, initial);
        return it;
      },
    }[name];
    const prototype = create(TaggedRecord.prototype);
    defineProperty(constructor, "prototype", { value: prototype });
    // Needed for my is_constructor test, but `instanceof` works without this...
    defineProperty(prototype, "constructor", { value: constructor });
    defineProperty(prototype, toStringTag, { value: name });
    return constructor;
  };

  const TupleKind = function (name) {
    const constructor = {
      [name]: function (...elements) {
        return construct(TaggedTuple, elements, constructor);
      },
    }[name];
    const prototype = create(TaggedTuple.prototype);
    defineProperty(constructor, "prototype", { value: prototype });
    // see above
    defineProperty(prototype, "constructor", { value: constructor });
    defineProperty(prototype, toStringTag, { value: name });
    return constructor;
  };
  TupleKind.comment = `Create a constructor of tuples with the given tag.`;

  const AMD = "https://github.com/amdjs/amdjs-api/";
  // def.prefixes.amd = AMD

  const RELATIVE_PATH = /^\.\.?(?:$|\/)/;
  const is_relative_path = function (path) {
    return RELATIVE_PATH.test(path);
  };
  is_relative_path.seeAlso = name(`${AMD}blob/master/AMD.md#module-id-format-`);
  is_relative_path.comment = `Answer whether the path is relative, according to the AMD definition of module id's.`;

  function combine_url_paths(path, from) {
    if (!is_relative_path(path)) return path;
    const url1 = new URL(`http://duty/${from}`);
    const url2 = new URL(`http://duty${url1.pathname}/../${path}`);
    return url2.pathname.replace(/^\//, "");
  }
  combine_url_paths.seeAlso = name(
    `${AMD}blob/master/AMD.md#module-id-format-`
  );
  combine_url_paths.comment = `Answer the qualified version of ‘path’ against ‘from’, using standard URL resolution`;

  const amd = { quote: mint.in(`${AMD}wiki/AMD#`).tuples };

  {
    const { defineProperties } = Object;
    function extend(o, p) {
      return defineProperties(
        o,
        map_object(p, value => ({ enumerable: true, value }))
      );
    }
  }

  // the synchronous define/require doesn't speak unless spoken to
  function make_world(ns, defines = new Map()) {
    const goals = []; // public... should be a buffer with some limit
    const completed = []; // also let history roll off
    const cache = new Map();
    const defined = x => defines.has(x);
    const require1 = x => require(x);

    const is_special = need => need === "exports" || need === "module";
    function amd_construct(needs, factory) {
      const exports = {};
      const module = { exports };
      // TODO: needed for AMD compatibility
      // but could also use to track dependencies dynamically
      //const require = make_contextualized_require(context);
      const special = { module, exports, require };
      const imports = needs.map(id => special[id] ?? require(id));
      // > If the factory argument is an object, that object should be assigned as
      // > the exported value of the module.
      // https://github.com/amdjs/amdjs-api/blob/master/AMD.md#factory-
      // Factory may write to `exports`; it must run even if result is about to be discarded
      const result =
        typeof factory === "function" ? factory(...imports) : factory;
      return needs.some(is_special) ? exports : result;
    }

    // discharge results where needs can now be met
    // called after a host value is added to the cache for some name.
    // could be more surgical knowing what name has been added
    function revisit_goals() {
      for (let i = goals.length - 1; i !== -1; i--) {
        const goal = goals[i];
        if (goal instanceof amd.quote.require) {
          const [needs, formula] = goal;
          if (needs.every(defined)) {
            try {
              require(needs, formula);
              goals.splice(i, 1);
            } catch (error) {
              // The definition was accepted; we must not reject now.  A failure
              // in one of these should also not stop us from trying the others.
              // Nor is this the `define` caller's business.
              console.error(goal, error);
            }
          }
        }
      }
    }

    function define_or_throw_conflict(name, term) {
      // RequireJS does not throw for a redefinition, but also doesn't redefine
      if (defines.has(name)) throw new Conflict(`Already defined: ${name}`);
      defines.set(name, amd.quote.define(...term)); // only place this is done
      revisit_goals();
    }

    function define(...term) {
      switch (term.length) {
        case 1:
          // PROVISIONAL: define₁ returns a normalized name
          return name(term[0]); // + base path & prefixes...
        case 2: {
          const [name, formula] = term;
          if (typeof name !== "string") {
            // until there's a loader
            throw new NotImplemented("anonymous define");
          }
          // If formula is not a function, retain the host value eagerly (w/o require)
          if (typeof formula !== "function") {
            define_or_throw_conflict(name, term);
            return;
          }
          return define(name, [], formula);
        }
        case 3: {
          const [name, _needs, formula] = term;
          if (typeof formula !== "function")
            throw new BadRequest(`define₃ expects f'n formula`);
          // lazy: remember the form to evaluate later (even if no needs)
          define_or_throw_conflict(name, term);
          break;
        }
        default:
          throw new BadRequest(`no such arity ${term.length}`);
      }
    }

    const { isArray } = Array;
    function require(...term) {
      switch (term.length) {
        case 1: {
          const [it] = term;
          if (isArray(it)) return it.map(require1); // distribute over array
          if (typeof it !== "string")
            throw new BadRequest("expected string or array");
          if (cache.has(it)) return cache.get(it);
          if (!defines.has(it)) throw new NotFound(it);

          const define_term = defines.get(it);
          let result;
          if (define_term.length === 2) {
            [, result] = define_term;
          } else if (define_term.length === 3) {
            const [, needs, formula] = define_term;
            result = amd_construct(needs, formula);
          }
          // shouldn't this also go under completed? if this was the top-level require
          cache.set(it, result); // only place this is done
          return result;
        }
        case 2:
        case 3: {
          const [needs, formula] = term;
          try {
            const result = formula(...needs.map(require1));
            completed.push(amd.quote.require(...term));
            return result;
          } catch (error) {
            const goal = amd.quote.require(needs, formula);
            // this can be resolved by later definitions
            if (error instanceof NotFound) {
              goals.push(goal);
              return;
            }
            const [, , errback] = term;
            if (typeof errback === "function") {
              console.warn(needs, formula, error);
              errback(error);
            } else {
              throw error;
            }
          }
          break;
        }
        default:
          throw new BadRequest(`no such arity ${term.length}`);
      }
    }

    return {
      define: extend(define, { amd, definitions: defines }),
      // goals & completed, and cache should be read-only views
      require: extend(require, {
        goals,
        completed,
        cache,
        resolve: combine_url_paths,
        async: make_require_async({ require }),
      }),
    };
  }

  function make_require_async({ require }) {
    const { isArray } = Array;
    const { assert } = console;
    function require_async(...term) {
      try {
        return require(...term);
      } catch {}
      switch (term.length) {
        case 1:
          return new Promise(resolve => {
            if (isArray(term[0])) {
              require(term[0], (...imports) => resolve(imports));
            } else {
              require(term, import_ => resolve(import_));
            }
          });
        // Just so `require` and `require.async` have the same semantics...
        case 2:
          return new Promise((resolve, reject) => {
            const [needs, callback] = term;
            assert(isArray(needs), `require async expected array needs`);
            require(needs, (...imports) =>
              resolve(callback(...imports)), reject);
          });
        case 3:
          return new Promise(resolve => {
            const [needs, callback, errback] = term;
            assert(isArray(needs), `require async expected array needs`);
            require(needs, (...imports) =>
              resolve(callback(...imports)), error => errback(error));
          });
      }
      throw new BadRequest(`no such arity ${term.length}`);
    }
    require_async.comment =
      "an async extension of `require` that returns promises that resolves after all needs were met";
    return require_async;
  }

  const { define, require } = make_world();

  // AMD terms -> JS interpretation
  function write_define_JS([id, needs, formula]) {
    return `define(${JSON.stringify(id)}, ${JSON.stringify(
      needs
    )}, ${formula});`;
  }
  function write_require_JS([needs, formula]) {
    return `def.require(${JSON.stringify(needs)}, ${formula});`;
  }

  function io_javascript_write(term) {
    if (term instanceof def.amd.quote.define) return write_define_JS(term);
    if (term instanceof def.amd.quote.require) return write_require_JS(term);
    throw new def.NotImplemented(`JS i/o for ${term}`);
  }
  io_javascript_write.comment = `Answer with JavaScript code that represents the given term`;

  const io = { js: { write: io_javascript_write } };

  function prompt_download(file, data) {
    const a = document.createElement("a");
    a.style.display = "none";
    a.download = file;
    a.href = data;
    a.target = "_blank";
    document.body.appendChild(a);
    a.click();
    a.remove();
  }
  prompt_download.seeAlso = `https://gist.github.com/cosmospham/7330466`;
  prompt_download.comment = `Offer to download a file called ‘file’ containing the given data URL.`;

  // The file is going to be saved to the user's downloads folder (whatever that is)
  // we here have no control over that
  // In previous testing I recall I was using a file monitor to move these into another place
  function prompt_download_document(file = `uspace.xhtml`) {
    // special preprocess: needed to persist changes to textarea content
    for (const textarea of document.querySelectorAll("textarea")) {
      try {
        textarea.replaceChildren(document.createTextNode(textarea.value));
      } catch (error) {
        console.warn("Couldn't persist textarea content", error);
      }
    }

    let url;
    const type = "text/html";
    // outerHTML works as well, and it doesn't do unwanted encoding of output
    // yet I refuse to use it here
    const data = new XMLSerializer().serializeToString(document);
    const blob = new Blob([data], { type });
    try {
      url = window.URL.createObjectURL(blob);
      prompt_download(file, url);
    } finally {
      URL.revokeObjectURL(url);
    }
  }
  prompt_download_document.comment = `Offer the user a download of the current document.  Currently browser-only.`;

  function prompt_download_def(file = "def-reconstituted.js") {
    let url;
    const type = "application/javascript";
    const all_terms = [
      `(${def.init})();`,
      // shouldn't this include goals?
      ...Array.from(require.completed.values(), io_javascript_write),
      ...Array.from(define.definitions.values(), io_javascript_write),
    ];
    const data = all_terms.join("\n\n");
    const blob = new Blob([data], { type });
    try {
      url = window.URL.createObjectURL(blob);
      prompt_download(file, url);
    } finally {
      URL.revokeObjectURL(url);
    }
  }
  prompt_download_document.comment = `Offer the user a download of the current document.  Currently browser-only.`;

  const def = define;
  const globals = { define, require, def };

  const userland = {
    prompt_download,
    prompt_download_document,
    prompt_download_def,
  };

  // no, this won't roundtrip
  // nested define cannot be a thing, if you want I/O
  // define("def:map", () => map);

  Object.assign(def, globals, {
    TupleKind, // needed so their toString can emit runnable JS
    BadRequest,
    NotFound,
    NotImplemented,
    init: def_init,
    io,
    userland,
    make_world,
    mint,
  });
  define("def", () => def);
  extend(globalThis, globals);
  // everything after this should use define
}

{
  // worker stuff
  {
    async function main(url) {
      console.info(`Worker here.  Hi. `, document, window, self, globalThis);
      const botany = def.mint.in("http://example.com/botany/").tuples;
      const { Berry } = botany;
      const banana = Berry();
      console.debug(botany);
      console.debug(Berry);
      console.debug(Berry[Symbol.toStringTag], "(tag)"); // should be on this too, huh?
      console.debug(banana);
      console.debug(banana[Symbol.toStringTag], "(tag)");
      console.debug(banana instanceof Berry);
      console.debug(banana instanceof Array);
      console.debug(Object.getPrototypeOf(banana));
      console.debug(Object.getPrototypeOf(banana).constructor);
      console.debug(Object.getPrototypeOf(banana).constructor.name);
      require(["~/process/channels"], channels => {
        console.debug("got channels", channels);
      });
      // so... do we send the actual things? and can we?
      // or do we send the code/defines?
      // maybe we *want* this world to be a “clean slate”
      // and maybe the things defined in the main realm don't work cross realm
    }
    const code = `
          (${def_init})();
          ${main}
          main();
          `;
    const blob = new Blob([code], { type: "text/javascript" });
    const url = URL.createObjectURL(blob);
    new Worker(url);
  }
}
