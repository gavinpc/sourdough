// WAT TIL https://developer.mozilla.org/en-US/docs/Web/API/Prioritized_Task_Scheduling_API
{
  function preamble() {
    const { apply } = Reflect;
    const EMPTY = Object.freeze([]);
    // never throws
    function invoke(fun, args, options) {
      const start_time = performance.now();
      let result, stop_time;
      try {
        result = apply(null, fun, args ?? EMPTY);
      } catch (error) {
        return { error };
      } finally {
        stop_time = performance.now();
      }
      return { result, start_time, stop_time };
    }
    return function make_local_system() {
      console.info(`hallooo from make_local_system`);
    };
  }
  /*
only for shared workers...
      onconnect = function worker_connect(event) {
        console.info("A new worker to exploit!", event);
      };
*/
}
