# Powershell equivalent of go.sh

# $PSScriptRoot requires Powershell 3.0 or higher
# Before that, it was
# $script_dir = Split-Path -Parent $MyInvocation.MyCommand.Definition

$ErrorActionPreference = "Stop"

$prep_failed = $false
try {
    Push-Location "$PSScriptRoot/prep"
    # This is the only way I've found to catch tup failure
    tup -j1
    $Success = "$?"
    if ($Success -eq $false) {
    # if (-Not ($Success)) {
        Write-Output "=============== PREP FAILED"
        $prep_failed = $true
        exit
    }
} catch {
    # Under default settings, we don't get here on a tup failure
    Write-Output "TUP ERROR CAUGHT"
    $prep_failed = $true
    throw
} finally {
    Pop-Location
}

if ($prep_failed) {
    Write-Warning "Prep failed"
    Write-Output "also wtf are we doing here if prep failed"
    exit
}

try {
    Push-Location "$PSScriptRoot/recipe"
    & tup -j1 $args
} finally {
    Pop-Location
}
