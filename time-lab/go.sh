#!/bin/sh

# Make recipe, including any preparation.

set -e

this_script_dir="$(cd "$(dirname "$0")" && pwd)"

cd "$this_script_dir/prep"
if ! tup -j1; then exit "$?"; fi

# pass-through arguments so you can e.g. specify target
cd "$this_script_dir/recipe"
# HACK: run build without parallelism.  This prevents the build from flooding
# remote hosts with requests.  It's a hack because, well, you might run tup
# yourself without -j1.
if tup "$@" -j1; then
    :
    # Run in `foreground` mode as a background job so signals are propagated.
    # trap 'tup stop' INT TERM
    # tup monitor --autoupdate --foreground "$@" &
    # monitor_pid="$!"
    # wait "$monitor_pid"
fi


exit 0
