tup.creategitignore()
tup.include('library/starter.lua')

function main()
  tup.include('wikipedia-timeline-seed/main.lua')
  -- This is done from the file itself, which see
  -- make_and_forward_timeline_xref('en')
end
main()
