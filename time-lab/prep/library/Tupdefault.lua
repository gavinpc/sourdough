if FORWARD_LIBRARY_TO then
  if not ROOT then
    error('To forward, you must also set ROOT!')
  end
  local rel = tup.getrelativedir(ROOT)
  local dir = ROOT..'/'..FORWARD_LIBRARY_TO..'/'..rel
  forward_this_dir_to(dir)
end
