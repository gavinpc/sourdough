-- HACK to share across Tup projects


function forwarding_rule(file, dest_dir)
  if Windows then
    local in_file = cmd_quote_literal(file)
    local out_dir_mixed = dest_dir:gsub('\\', '/')
    local out_dir = cmd_quote_literal(out_dir_mixed)
    return {
      inputs = {file},
      command =
        '(IF NOT EXIST '..out_dir..' (MD '..out_dir..')) '
        ..' && COPY '..in_file..' '..out_dir,
      -- with first version, forwarding of library stuff works, but not xref
      -- with second version, it's the opposite
      -- outputs = {'^'..out_dir_mixed}
      outputs = {'^'..file}
    }
  end
  local in_file = posix_quote_literal(file)
  local out_dir = posix_quote_literal(dest_dir)
  return {
    inputs = {file},
    command = "mkdir -p "..out_dir.." && cp "..in_file.." "..out_dir,
    outputs = {'^'..dest_dir}
  }
end

function forward_file_to(file, dest_dir)
  tup.definerule(forwarding_rule(file, dest_dir))
end

function forward_this_dir_to(dest_dir)
  local matches = tup.glob('*')
  for _, file in pairs(matches) do
    tup.definerule(forwarding_rule(file, dest_dir))
  end
end

------ portability stuff

-- If neither of these is true, assume Linux (which treat as POSIX)
Windows = tup.getconfig('TUP_PLATFORM') == 'win32'
MacOS = tup.getconfig('TUP_PLATFORM') == 'macosx'

function posix_quote_literal(s)
  return "'"..s:gsub("'", "'\"'\"'").."'"
end

-- https://ss64.com/nt/syntax-esc.html
-- is this even possible to do out of context?
-- like do we need to know whether this is a command option vs any other token?
function cmd_quote_literal(s)
  -- do we need to escape backslashes themselves here?
  return '"'..s:gsub('"', '\\"')..'"'
end

function powershell_quote_literal(s)
  return s                      -- TBD
end

-- Return a rule spec that encodes the described (sh) script invocation as an
-- `sh` expression.
--
-- ASSUMES that all arguments are literals

-- BUT it's not really necessary that you're calling an `sh` command, just that
-- the resulting expression is correct for `sh`, which is the default shell used
-- by Tup on non-Windows systems.  Indeed, we don't need to specify the command
-- interpreter at all, if we're willing to assume that the file is executable
-- with a shebang.  That said, there could be some advantage to not requiring
-- that programs be marked as executable, since permissions could be lost in
-- transit.
function sh_call_sh(spec)
  local inputs = spec.inputs
  local script = spec.script
  local arguments = spec.arguments
  local outputs = spec.outputs
  local list = ''
  if arguments ~= nil then
    for _,argument in ipairs(arguments) do
      list = list..' '..posix_quote_literal(argument)
    end
  end
  local command = 'sh '..script..' '..list
  return {inputs = inputs, command = command, outputs = outputs}
end

-- Return a rule spec that encodes the described `powershell` script invocation
-- as a `cmd` expression.
--
-- ASSUMES that all arguments are literals

-- FUTURE: use base64 to avoid quoting problems

-- unfortunately (here and for POSIX), we have to repeat the inputs and outputs

-- we also can't just write the command because we need to specify ignore
-- could think of it as a “partial” rule
-- there are other advantages, e.g. we can manage labeling
function cmd_call_powershell(spec)
  local inputs = spec.inputs
  local script = spec.script
  local arguments = spec.arguments
  local outputs = spec.outputs or {}
  local list = ''
  if arguments ~= nil then
    for _,argument in ipairs(arguments) do
      list = list..' '..argument
    end
  end

  -- WORKAROUND: powershell 2 doesn't write to any profile (at least if you say
  -- -NoProfile) but it's so old that you can't run it because a .NET 2
  -- Framework probably isn't available.  Hence the NonInteractive hack.  See
  -- https://stackoverflow.com/q/69161207
  table.insert(outputs, '^StartupProfileData-NonInteractive')

  local prefix = 'powershell.exe -version 3.0 -nologo -noninteractive -noprofile -File '
  local command = prefix..script..' '..list
  return {inputs = inputs, command = command, outputs = outputs}
end


function get_application_data_path(name)
  assert(is_nonblank(name))
  if Windows then
    tup.import('LOCALAPPDATA')
    assert(is_nonblank(LOCALAPPDATA), "LOCALAPPDATA must be defined on Windows!")
    return LOCALAPPDATA..'/'..name
  end
  if MacOS then
    error('Not implemented: application_data_path for MacOS')
  end
  tup.import('HOME')
  assert(is_nonblank(HOME), "HOME must be defined on POSIX!")
  return HOME..'/.cache/'..name
end


------ general utility

function is_nonblank(s)
  return s ~= nil and s ~= ''
end

function map_unordered(coll, f)
  local o = {}
  for _,v in ipairs(coll) do
    table.insert(o, f(v))
  end
  return o
end

function foreach_line_in(path, map)
  assert(path ~= nil, 'path is required')
  local file = io.open(path, 'r')
  if file ~= nil then
    for line in file:lines() do
      map(line)
    end
    file:close()
  else
    error("no such file or couldn't open for read: "..path)
  end
end

function identity(x) return x end
function buffer_lines_in(path, fun)
  assert(path ~= nil, 'path is required')
  local f = fun or identity
  local o = {}
  foreach_line_in(path, function(v) table.insert(o, f(v)) end)
  return o
end
