web_cwd = tup.getcwd()

-- REQUIRES `get_application_data_path` and `is_nonblank` from common
function get_web_cache_path()
  tup.import('WEB_CACHE')
  -- Force value copy.  Without this concat, the returned variable is corrupt;
  -- specifically, `gsub` says it's nil, even though it concats and prints ok.
  if is_nonblank(WEB_CACHE) then return WEB_CACHE..'' end
  return get_application_data_path('sourdough-web')
end

function web_resource(spec)
  local url = spec.url
  local file = spec.file
  local alias = spec.alias or ''

  local web_cache_dir_raw = get_web_cache_path()
  -- normalize the path.  This is needed only for the ignore-output clause,
  -- but still may be better done in the function itself.
  local web_cache_dir = web_cache_dir_raw:gsub('\\', '/')

  local rule
  if Windows then
    rule = cmd_call_powershell {
      script = web_cwd..'/get.ps1',
      arguments = { url, file, alias, web_cache_dir },
      outputs = {file, '^'..web_cache_dir}
    }
  else
    rule = sh_call_sh {
      script = web_cwd..'/get.sh',
      arguments = { url, file, alias, web_cache_dir },
      outputs = {file, '^'..web_cache_dir}
    }
  end

  return {spec=spec, rule=rule}
end
