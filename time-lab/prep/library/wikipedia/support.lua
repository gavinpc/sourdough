tup.include('../web/support.lua')

function wikipedia_article(lang, slug, dir)
  -- Default to the directory being processed for web files/links
  dir = dir or tup.getcwd()
  local url = 'https://api.wikimedia.org/core/v1/wikipedia/'..lang..'/page/'..slug..'/html'
  local file = dir..'/'..lang..'-wiki-'..slug..'.html'
  local alias = lang..'-wiki-'..slug..'.html'
  return web_resource {url=url, file=file, alias=alias}
end
