function xslt1(spec)
  local input = spec.input
  local transform = spec.transform
  local output = spec.output
  local parameters = spec.parameters -- the transform parameters
  local html = spec.html

  local quote = posix_quote_literal
  -- Because we're not asking for powershell (yet), so cmd is what we'll get
  if Windows then quote = cmd_quote_literal end

  local maybe_html = ''
  if html then maybe_html = ' --html' end

  local stringparams = ''
  if (parameters) then
    for name, value in pairs(parameters) do
      stringparams = stringparams..' --stringparam '..quote(name)..' '..quote(value)
    end
  end
  
  local rule = {
    inputs = {input},
    command = 'xsltproc'
      ..maybe_html
      ..' --output '..quote(output)
      ..stringparams
      ..' '..quote(transform)
      ..' '..quote(input),
    outputs = {output}
  }
  return {spec=spec, rule=rule}
end
