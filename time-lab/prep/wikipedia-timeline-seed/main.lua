tup.include('../library/xsl/support.lua')
tup.include('../library/wikipedia/support.lua')

function table.empty(self)
  return not next(self)
end

-- but transform is not actually used. en is imported
function make_and_forward_timeline_xref(lang, transform)
  local seed = 'lang/'..lang
  local sources = buffer_lines_in(seed)
  local dir = tup.getcwd()..'/web'
  local articles = map_unordered(
    sources,
    function(title)
      return wikipedia_article(lang, title, dir)
    end
  )

  local files = map_unordered(
    articles,
    function (article)
      return article.rule.outputs[1]
    end
  )
  local list = table.concat(files, ' ')
  local xref = 'xref-'..lang
  local dir = tup.getcwd()
  local it = xslt1 {
    input = dir .. '/dummy.xml',
    parameters = {files = list},
    transform = dir .. '/wikipedia-timeline-xref.xsl',
    output = xref
  }
  -- we also need to tell tup that the file list is the list of dependencies...
  if (table.empty(it.rule.inputs)) then it.rule.inputs = {} end
  it.rule.extra_inputs = files
  for _,the in ipairs(articles) do
    tup.definerule(the.rule)
  end
  tup.definerule(it.rule)

  if FORWARD_XREF_TO then
    if not ROOT then
      error('To forward, you must also set ROOT!')
    end
    local rel = tup.getrelativedir(ROOT)
    local dir = ROOT..'/'..FORWARD_XREF_TO..'/'..rel
    forward_file_to(xref, dir)
  end

  -- return {sources=sources, articles=articles, gray=gray}
end

-- This is a `main` because it does the work here, rather than being induced
-- from a containing directory.  This is how Tup prefers to be used (act on the
-- current directory's files when processing the current directory).  It's
-- *possible* to make it work the other way, but it becomes challenging to
-- determine file paths.
make_and_forward_timeline_xref('en')
