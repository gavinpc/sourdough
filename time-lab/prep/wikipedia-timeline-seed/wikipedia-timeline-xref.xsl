<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0'
               xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
               xmlns:ex='http://exslt.org/common'
               xmlns:str="http://exslt.org/strings"
               xmlns:notebook="https://gavinpc.com/composition-notebook"
               >
  <xsl:output type="text" omit-xml-declaration="yes" />
  <xsl:include href="./lang/en.xsl" />
  <xsl:param name="files" />

  <xsl:key name="xref" match="xref" use="@date" />

  <xsl:variable name="CR" select="'&#xA;'" />

  <!-- Emit a two-column, space-delimited cross-reference table of title,ISO -->
  <!-- for all of the timeline articles linked from the files in `$files` -->
  <!-- which is a space-delimited list of MediaWiki markup filenames -->

  <xsl:template mode="timeline-links" match="*">
    <xsl:for-each select="//a[starts-with(@href, './')][not(contains(@href, '#'))]">
      <xsl:variable name="title" select="substring-after(@href, './')" />
      <xsl:variable name="iso" select="notebook:en-title-to-iso($title)" />
      <xsl:if test="$iso != ''">
        <xref date="{$iso}" title="{$title}"/>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="/">
    <xsl:variable name="xref">
      <xsl:for-each select="str:split($files, ' ')">
        <xsl:apply-templates mode="timeline-links" select="document(.)" />
      </xsl:for-each>
    </xsl:variable>

    <!-- deduplicate -->
    <xsl:for-each select="ex:node-set($xref)">
      <xsl:for-each select="xref[generate-id() = generate-id(key('xref', @date)[1])]">
        <xsl:value-of select="concat(@title, ' ', @date, $CR)" />
      </xsl:for-each>
    </xsl:for-each>
  </xsl:template>

</xsl:transform>
