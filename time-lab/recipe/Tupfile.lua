tup.creategitignore()
tup.include('library/starter.lua')

function main()
  tup.include('us-presidents/main.lua')
  tup.include('us-constitution-amendments/main.lua')
  tup.include('wikipedia-timeline/main.lua')
end
main()
