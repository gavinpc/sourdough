# Make an AMD `define` script that packages a collection & the knowledge needed
# to read it.

# The `-Raw` and `-NoNewline` arguments prevent Windows-style line endings.

param (
    [string]$Name,
    [string]$Meta,
    [string]$DataFile,
    [string]$Output
)

$ErrorActionPreference = "Stop"

function Write-Define {
    param (
        [string]$Name,
        [string]$Meta,
        [string]$DataFile
    )
    $Data = Get-Content -Path "$DataFile" -Raw
    if ($Data -eq $null) {
        Write-Warning 'Data is empty!'
        $Data = ''
    }
    $EscapedName = $Name.Replace('"', '\"')
    $EscapedData = $Data.Replace('``', '\``')
    Write-Output @"
define(`"$EscapedName`", [], () => {
  const XML =``$EscapedData``;
"@
    Get-Content -Path "$Meta" -Raw
    Write-Output @'
return meta;
});
'@

}

function Make-Define {
    param (
        [string]$Name,
        [string]$Meta,
        [string]$DataFile,
        [string]$Output
    )
    Write-Define "$Name" "$Meta" "$DataFile" | Out-File "$Output" -NoNewline
}

Make-Define @PSBoundParameters
