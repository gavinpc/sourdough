#!/bin/sh

# Make an AMD `define` script that packages a collection & the knowledge needed
# to read it.

# Yes, should use pipefail

set -e

write_define() {
    local name="$1"
    local meta="$2"
    local data="$3"

    echo 'define("'$name'", [], () => {
  const XML =`'
    sed 's/`/\\`/g' "$data"
    echo '`'
  cat "$meta"
  echo '  return meta;
});'
}

make_define() {
    local output="$4"
    write_define "$@" > "$output"
}

make_define "$@"

exit 0
