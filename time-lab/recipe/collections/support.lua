-- requires starter

-- it's too late to set this by the time we get to `make_define`
local collections_dir = tup.getcwd()

function make_define(spec)
  local name = spec.name
  local meta = spec.meta
  local data = spec.data
  local output = spec.output

  -- The meta file is also an input but we assume it's a “regular file”
  local rule
  if Windows then
    rule = cmd_call_powershell {
      inputs = {data},
      script = collections_dir..'/make-define.ps1',
      arguments = {name, meta, data, output},
      outputs = {output}
    }
  else
    rule = sh_call_sh {
      inputs = {data},
      script = collections_dir..'/make-define.sh',
      arguments = {name, meta, data, output},
      outputs = {output}
    }
  end

  return {spec=spec, rule=rule}
end
