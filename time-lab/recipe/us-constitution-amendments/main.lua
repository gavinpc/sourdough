tup.include('../library/xsl/support.lua')
tup.include('../library/wikipedia/support.lua')
tup.include('../collections/support.lua')

function us_constitution_amendments_en()
  local lang = 'en'
  local title = 'List_of_amendments_to_the_Constitution_of_the_United_States'
  local here = tup.getcwd()
  local xml_file = here..'/out/us-constitution-amendments.xml'
  local web_dir = here..'/web'
  local article = wikipedia_article(lang, title, web_dir)
  local transform = xslt1 {
    input = article.spec.file,
    transform = here..'/extract-us-constitution-amendments.xsl',
    html = true,
    output = xml_file
  }
  local define = make_define {
      name = 'https://def.codes/sourdough/en-us-constitution-amendments',
      meta = here..'/us-constitution-amendments-meta.js',
      data = xml_file,
      output = here..'/out/en-us-constitution-amendments.js',
  }
  tup.definerule(article.rule)
  tup.definerule(transform.rule)
  tup.definerule(define.rule)
end

-- see note to ../../prep/wikipedia-timeline-seed/main.lua
us_constitution_amendments_en()
