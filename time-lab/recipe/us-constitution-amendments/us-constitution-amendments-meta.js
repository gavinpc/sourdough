const meta = {
  locale: "en",
  format: "xml",
  data: XML,
  store: {
    keyPath: "number",
    indexes: {
      wiki: { keyPath: "wiki" },
      refs: { keyPath: "refs", multiEntry: true },
      proposed: { keyPath: "proposed" },
      completed: { keyPath: "completed" },
    },
  },
  from_dom(/** @type Element */ dom) {
    const eles = dom.querySelectorAll("us-constitution > amendment");
    return Array.from(eles, ele => {
      const p = ele.querySelector("description");
      console.assert(p, `No content element for item`, ele);
      return {
        wiki: ele.getAttribute("wiki"),
        number: parseInt(ele.getAttribute("number"), 10),
        proposed: ele.getAttribute("proposed"),
        completed: ele.getAttribute("completed"),
        refs: Array.from(p.querySelectorAll("a[wiki]"), _ =>
          _.getAttribute("wiki")
        ),
        description: p.innerHTML,
      };
    });
  },
};
