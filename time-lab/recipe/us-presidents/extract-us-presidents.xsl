<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
  <xsl:output method="xml" omit-xml-declaration="yes" indent="yes" />

  <xsl:template match="/">
    <xsl:apply-templates select="(//*[@id='Presidents']/following-sibling::table)[1]" />
  </xsl:template>

  <xsl:template match="table">
    <us-presidents>
      <xsl:apply-templates mode="us-presidential-term" select="tbody/tr[td]" />
    </us-presidents>
  </xsl:template>

  <xsl:template match="*" mode="us-presidential-term">
    <xsl:variable name="dates" select="td[3]/span/@data-sort-value" />
    <!-- this little hack is for John Tyler's start date -->
    <xsl:variable name="prior" select="(preceding-sibling::*)[last()]" />
    <xsl:variable name="prev-end" select="($prior/td[3]/span/@data-sort-value)[2]" />
    <xsl:variable name="term-start">
      <xsl:apply-templates select="($dates[1] | $prev-end)[1]" mode="sort-to-iso4" />
    </xsl:variable>
    <xsl:variable name="term-end">
      <xsl:apply-templates select="$dates[2]" mode="sort-to-iso4" />
    </xsl:variable>
    <term
      number="{normalize-space(th[1])}"
      portrait="https:{td//img/@src}"
      incumbent="{substring-after(td[2]//a/@href, './')}"
      term-start="{$term-start}"
      term-end="{$term-end}"
      party="{substring-after(td[5]//a/@href, './')}">
      <xsl:apply-templates select="td[6]" mode="us-elections" />
    </term>
  </xsl:template>

  <xsl:template match="a[starts-with(@href, './')]" mode="us-elections">
    <election ref="{substring-after(@href, './')}" dates="{translate(., '-–', '//')}"/>
  </xsl:template>

  <xsl:template match="node()|@*" mode="sort-to-iso4">
    <xsl:value-of select="substring-before(substring-after(., '00000000'), '-0000')" />
  </xsl:template>

</xsl:transform>
