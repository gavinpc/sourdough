const meta = {
  locale: "en",
  format: "xml",
  data: XML,
  store: {
    keyPath: "number",
    indexes: {
      incumbent: { keyPath: "incumbent" },
      termStart: { keyPath: "termStart" },
      termEnd: { keyPath: "termEnd" },
      electionDates: { keyPath: "electionDates", multiEntry: true },
      party: { keyPath: "party" },
    },
  },
  from_dom(/** @type Element */ dom) {
    const eles = dom.querySelectorAll("us-presidents > term");
    return Array.from(eles, ele => {
      return {
        number: parseInt(ele.getAttribute("number"), 10),
        incumbent: ele.getAttribute("incumbent"),
        termStart: ele.getAttribute("term-start"),
        termEnd: ele.getAttribute("term-end"),
        portrait: ele.getAttribute("portrait"),
        party: ele.getAttribute("party"),
        // in addition to elections because we can index on this
        electionDates: Array.from(ele.querySelectorAll("election"), node =>
          node.getAttribute("dates")
        ),
        elections: Array.from(ele.querySelectorAll("election"), node => {
          const ref = node.getAttribute("ref");
          const date = node.getAttribute("dates");
          return { ref, date };
        }),
      };
    });
  },
};
