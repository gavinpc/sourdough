# Concatenate all files from a given directory into one file, in arbitrary order
param (
    [Parameter(Mandatory, Position=0)]
    [string]$InDir,

    [Parameter(Mandatory, Position=1)]
    [string]$OutFile
)

$ErrorActionPreference = 'Stop'

# There is also Set-Content, which is encoding-aware, but I think we want raw
Get-Content -Path "$InDir\*" -Raw | Out-File $OutFile
