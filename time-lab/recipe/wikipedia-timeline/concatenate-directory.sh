#!/bin/bash

# Concatenate all files from a given directory into one file, in arbitrary order

set -e

in_dir="$1"
out_file="$2"

cat "$in_dir"/* | "$out_file"

exit 0
