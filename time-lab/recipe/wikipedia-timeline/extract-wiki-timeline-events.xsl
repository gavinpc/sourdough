<?xml version="1.0" encoding="utf-8"?>
<xsl:transform version="1.0"
               xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
               xmlns:ex='http://exslt.org/common'
               exclude-result-prefixes='ex'
               >
  <xsl:output indent="no" encoding="utf-8" omit-xml-declaration="yes" media-type="string" />
  <xsl:param name="date" />
  <xsl:include href="./wikipedia-timeline-item.xsl" />

  <!-- Extract list items and their document context and wiki references. -->
  <!-- ASSUMES that the input document uses structured sections. -->
  
  <xsl:template mode="extract-events" match="@*" />
  <xsl:template mode="extract-events" match="text()" />
  <xsl:template mode="extract-events" match="*">
    <xsl:apply-templates mode="extract-events" select="node()|@*" />
  </xsl:template>
  
  <!-- The API returns markup for pages that are redirected.  Skip these. -->
  <!-- ACTUALLY I no longer get these -->
  <xsl:template mode="extract-events" match="/*[*[@class='redirectMsg']]">
    <xsl:message terminate="yes">Redirect!</xsl:message>
  </xsl:template>
  
  <!-- Each entry is associated with one list item. -->
  <xsl:template mode="extract-events" match="li">
    <!-- Support explicitly calling template with additional context. -->
    <xsl:param name="parent" />

    <xsl:variable name="sections-data">
      <xsl:apply-templates mode="sections" select="." />
    </xsl:variable>
    <xsl:variable name="sections" select="ex:node-set($sections-data)" />

    <xsl:variable
      name="skip"
      select="$sections/*[
              starts-with(@heading, 'Births') or
              starts-with(@heading, 'Deaths')
              ]"
      />
    
    <xsl:if test="not($skip)">
      <xsl:variable name="content-data">
        <xsl:apply-templates mode="normalize-time-item" select="node()|@*" />
      </xsl:variable>
      <xsl:variable name="content" select="ex:node-set($content-data)" />

      <!--
          If the first thing looks like a time refinement, then
          - add it to the date
          - remove it from the content
          - along with any typical delimiter following it
      -->
      <xsl:variable name="refinement">
        <xsl:for-each select="$content">
          <xsl:call-template name="read-date" />
        </xsl:for-each>
      </xsl:variable>
      <xsl:variable name="refinements" select="ex:node-set($refinement)" />
      <xsl:variable name="match-count" select="count($refinements/match)" />
      <xsl:if test="$match-count > 1">
        <xsl:message terminate="yes">
          <xsl:value-of select="$match-count" /> > 1 matches in <xsl:value-of select="$date" />
        </xsl:message>
      </xsl:if>

      <xsl:variable name="outer-match">
        <xsl:if test="$parent">
          <xsl:variable name="context-data">
            <xsl:apply-templates mode="normalize-time-item" select="$parent" />
          </xsl:variable>
          <xsl:for-each select="ex:node-set($context-data)">
            <xsl:call-template name="read-date" />
          </xsl:for-each>
        </xsl:if>          
      </xsl:variable>

      <xsl:variable name="inline-match" select="($refinements/match)[1]" />
      <xsl:variable name="matches">
        <xsl:copy-of select="$inline-match" />
        <xsl:copy-of select="$outer-match" />
      </xsl:variable>
      <xsl:variable name="match" select="ex:node-set($matches)/match[1]" />

      <xsl:variable name="refined-date">
        <xsl:choose>
          <xsl:when test="$match/@year">
            <xsl:value-of select="$match/@year" />
          </xsl:when>
          <xsl:when test="$match/@within-year">
            <!-- skip first char in case it's BC -->
            <xsl:if test="contains(substring($date, 2), '-')">
              <xsl:message>
                WARN: <xsl:value-of select="$date" /> is already specific <xsl:value-of select="$match/within-year" />
              </xsl:message>
            </xsl:if>
            <xsl:value-of select="concat($date, '-', $match/@within-year)" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="$date" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <event date="{$refined-date}">
        <xsl:apply-templates mode="significant-sections" select="$sections" />
        <p>
          <xsl:choose>
            <xsl:when test="$inline-match/@from-wiki">
              <!-- REFINED WIKI -->
              <xsl:apply-templates mode="skip-delimiters" select="$content/node()[1]" />
            </xsl:when>
            <xsl:when test="$inline-match/@from-text">
              <!-- REFINED TEXT -->
              <xsl:apply-templates mode="skip-delimiters" select="$content/text()[1]" />
            </xsl:when>
            <xsl:otherwise><xsl:copy-of select="$content" /></xsl:otherwise>
          </xsl:choose>
        </p>
      </event>
    </xsl:if>
  </xsl:template>
  
  <!-- Visit nested list items with additional context from the parent item. -->
  <xsl:template mode="extract-events" match="li[ul]">
    <xsl:apply-templates mode="extract-events" select="ul/li">
      <xsl:with-param name="parent" select="node()[not(self::ul)]" />
    </xsl:apply-templates>
  </xsl:template>
  
  <!-- Exclude document sections outside of the content proper. -->
  
  <!-- Slight optimization. Assumes ToC is excluded in the API request -->
  <!-- <xsl:template match="*[@id='toc']//*" /> -->

  <xsl:template
    mode="extract-events"
    match="*
           [ancestor-or-self::*[
           @role='navigation'
           or contains(@class, 'sidebar')
           or contains(@class, 'navbox')
           or contains(@class, 'infobox')
           or contains(@class, 'gallerybox')
           or contains(@class, 'reflist')
           or contains(@class, 'references')
           ]]" />
  
  <xsl:template mode="extract-events" match="li[@class = 'mw-empty-elt']" />
  <xsl:template mode="extract-events" match="section[descendant-or-self::*[@id='References']]" />

  <!-- If we somehow still end up with citations. -->
  <!-- This should cover the "reflist" rule in effect.  In rare cases, these are
       standalone, e.g. 11th_century -->
  <xsl:template mode="extract-events" match="li[starts-with(@id, 'cite_note-')]" />
  <xsl:template mode="extract-events" match="li[starts-with(@class, 'nv-')]" />
  <xsl:template mode="extract-events" match="li[cite]" />

  <!-- Omit transcluded content -->
  <!-- 
       For this application, transclusions represent redundant content, since
       the source article will also be in the collection.  Preferably, we would
       omit transclusions upstream, and indeed some are omitted at fetch time.
       The rest, we deal with here.
       
       Transcluded sections start with a template that says either “Transcluded
       articles” or “Transcluding articles” or “This section is transcluded
       from...”.  The template is the second element (the first one following
       the heading), and in practice is either a `div` or a `dl`.
  -->
  <!-- IS THIS STILL TRUE? -->
  <xsl:template mode="extract-events" match="section[*[2][contains(., 'ransclud')]]" />

  <!-- Sections: Context within document outline -->
  <xsl:template mode="sections" match="*">
    <xsl:apply-templates mode="parent-section" select="(ancestor::section)[last()]" />
  </xsl:template>
  <xsl:template mode="parent-section" match="section">
    <xsl:variable name="heading" select="(h1|h2|h3|h4|h5|h6|h7)[1]" />
    
    <xsl:apply-templates mode="parent-section" select="(ancestor::section)[last()]" />

    <!-- The anchor we want is sometimes preceded by an element with an internal
         id, so we must stipulate the CSS class. -->
    <section
      level="{number(substring-after(name($heading), 'h'))}"
      anchor="{$heading/descendant-or-self::*[@id][contains(@class, 'mw-headline')]/@id}">
      <xsl:attribute name="heading">
        <xsl:apply-templates mode="section-title" select="$heading" />
      </xsl:attribute>
    </section>
  </xsl:template>
  
  <!-- Significant sections: headings to treat as refining topics -->
  <xsl:template mode="significant-sections" match="node()|@*" />
  <!-- Assuming month names aren't needed for date refinement -->
  <!-- i.e. that they'd be repeated inline -->
  <xsl:template mode="significant-sections"
                match="*[@heading and
                       @heading!='Events' and
                       not(starts-with(@heading, 'Birth')) and
                       not(starts-with(@heading, 'Death')) and
                       not(starts-with(@heading, 'By ')) and
                       not(starts-with(@heading, 'Further')) and
                       not(starts-with(@heading, 'External')) and
                       not(starts-with(@heading, 'January')) and
                       not(starts-with(@heading, 'February')) and
                       not(starts-with(@heading, 'March')) and
                       not(starts-with(@heading, 'April')) and
                       not(starts-with(@heading, 'May')) and
                       not(starts-with(@heading, 'June')) and
                       not(starts-with(@heading, 'July')) and
                       not(starts-with(@heading, 'August')) and
                       not(starts-with(@heading, 'September')) and
                       not(starts-with(@heading, 'October')) and
                       not(starts-with(@heading, 'November')) and
                       not(starts-with(@heading, 'December'))
                       ]">
    <topic><xsl:value-of select="@heading" /></topic>
  </xsl:template>
  
  <!-- Section title: Extract title from section heading. -->
  <xsl:template mode="section-title" match="node()|@*">
    <xsl:variable name="content">
      <xsl:apply-templates mode="normalize-time-item" select="node()" />
    </xsl:variable>
    <xsl:value-of select="normalize-space($content)" />
  </xsl:template>
  <!-- Omit “edit” links. -->
  <xsl:template mode="section-title" match="*[@class='mw-editsection']" />
  
  <!-- main -->
  <xsl:template match="/">
    <xsl:apply-templates mode="extract-events" />
    <!-- HACK: if no events were found, xsltproc is not writing the file -->
    <xsl:text> </xsl:text>
  </xsl:template>
  
</xsl:transform>
