tup.include('../library/starter.lua') -- should already be loaded though
tup.include('../library/wikipedia/support.lua')

--------------------------------- helper

-- return a table based on the lines in a file
-- fun maps a line to a key and value
-- ASSUMES that `fun` is effectively deterministic (returns equivalent for same key)
function dictionary_from_file(file, fun, skip_empty)
  local o = {}
  foreach_line_in(
    file,
    function(line)
      if skip_empty and (line == nil or line == '') then return end
      local key, value = fun(line)
      if o[key] == nil then
        o[key] = value
      else
        -- safety check
        -- print('SAFETY CHECKING key, value ', key, value)
        -- if o[key] ~= value then
        --   error('key '..key..' already has a different value than ‘'..value..'’: '..o[key])
        -- end
      end
    end
  )
  return o
end
-------------------------------------------------------------------------

-- file is lines of space-delimited Wikipedia slug and an ISO-8601 date
-- well, a four-digit year with `-` for BC and NO SIGN for CE
-- f is (slug, date) → (slug, T)
-- returns Table<slug, T>
function project_timeline_index(xref_file, f)
  function line_to_pair(line)
    local slug, date = line:match'(.*) (.*)'
    if slug == nil then error("no slug in line "..line.."\n") end
    if date == nil then error("no date in line "..line.."\n") end
    return slug, f(slug, date)
  end
  return dictionary_from_file(xref_file, line_to_pair, true) -- skip blank lines
end

-- define a platform-independent rule for concatenating the given files to the
-- given output
concatenate_cwd = tup.getcwd()
-- but this doesn't create a dependency on the input files...
function make_concatenate_directory(spec)
  local input = spec.input      -- directory name
  local output = spec.output    -- output file
  local arguments = {input, output}
  local rule
  if Windows then
    rule = cmd_call_powershell {
      script = concatenate_cwd..'/concatenate-directory.ps1',
      arguments = arguments,
      outputs = {output}
    }
  else
    rule = sh_call_sh {
      script = concatenate_cwd..'/concatenate-directory.sh',
      arguments = arguments,
      outputs = {output}
    }
  end
  return {spec=spec, rule=rule}
end

--------------------------------------------------------------------------------

-- returns Table<slug, {spec, rule, lang, slug, about_time}>
function make_wikipedia_timeline(lang, xml_dir, xml_file)
  local xref_file = 'xref-'..lang
  local here = tup.getcwd()
  local dir = here..'/web'

  function article_and_transformation(slug, about_time)
    local article = wikipedia_article(lang, slug, dir)
    local article_events_xml_file = here..'/out/xml/'..slug..'.xml'
    article.about_time = about_time

    local transformation = xslt1 {
      input = article.spec.file,
      transform = here..'/extract-wiki-timeline-events.xsl',
      html = true,
      output = article_events_xml_file,
      parameters = {date=about_time}
    }
    return {article=article, transformation=transformation}
  end

  local index = project_timeline_index(xref_file, article_and_transformation)
  local events_files = {}

  -- actualize timeline
  for _,entry in pairs(index) do
    table.insert(events_files, entry.transformation.rule.outputs[1])
    tup.definerule(entry.article.rule)
    tup.definerule(entry.transformation.rule)
  end

  -- finally, concatenate all of above into out_xml file
  local concatenation = make_concatenate_directory {
    input = xml_dir,
    output = xml_file
  }
  concatenation.rule.inputs = events_files
  tup.definerule(concatenation.rule)
end

function wikipedia_timeline_en()
  local lang = 'en'
  local here = tup.getcwd()
  local xml_file = here..'/out/'..lang..'-wiki-timeline-events.xml'
  make_wikipedia_timeline(lang, here..'/out/xml', xml_file)
  local web_dir = here..'/web'
  local define = make_define {
    name = 'https://def.codes/sourdough/'..lang..'-wiki-timeline-events',
    -- but note language is baked into this metadata (and not used)
    meta = here..'/wiki-timeline-events-meta.js',
    data = xml_file,
    output = here..'/out/'..lang..'-wiki-timeline-events.js',
  }
  tup.definerule(define.rule)
end

-- see note to ../../prep/wikipedia-timeline-seed/main.lua
wikipedia_timeline_en()
