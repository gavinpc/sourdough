const meta = {
  locale: "en",
  format: "xml",
  data: XML,
  store: {
    keyPath: "id",
    autoIncrement: true,
    indexes: {
      aboutTime: { keyPath: "aboutTime" },
      topics: { keyPath: "topics", multiEntry: true },
      refs: { keyPath: "refs", multiEntry: true },
    },
  },
  // This should be done lazily too, there's no need to make all at once
  from_dom(/** @type Element */ dom) {
    const eles = dom.querySelectorAll("events > event");
    return Array.from(eles, ele => {
      const p = ele.querySelector("p");
      console.assert(p, `No content element for item`, ele);
      return {
        aboutTime: ele.getAttribute("date"),
        topics: Array.from(ele.querySelectorAll("topic"), _ => _.textContent),
        refs: Array.from(p.querySelectorAll("a[wiki]"), _ =>
          _.getAttribute("wiki")
        ),
        html: p.innerHTML,
      };
    });
  },
};
