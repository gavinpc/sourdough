<?xml version='1.0' encoding='utf-8'?>
<xsl:transform version='1.0'
               xmlns:xsl='http://www.w3.org/1999/XSL/Transform'
               xmlns:func="http://exslt.org/functions"
               xmlns:wtime="https://gavinpc.com/wiki-timeline"
               xmlns:str="http://exslt.org/strings"
               exclude-result-prefixes="wtime str"
               extension-element-prefixes="func">
  <xsl:output encoding="utf-8" />

  <!-- Normalize: drop meta stuff and use `@wiki` for internal links -->
  <xsl:template mode="normalize-time-item" match="@*" />
  <!-- We're dropping all attributes so spans are pass-through -->
  <xsl:template mode="normalize-time-item" match="span">
    <xsl:apply-templates mode="normalize-time-item" select="node()|@*" />
  </xsl:template>
  <!-- drop citations -->
  <xsl:template mode="normalize-time-item" match="sup[@about]" />
  <xsl:template mode="normalize-time-item" match="@href[starts-with(., './')]">
    <xsl:attribute name="wiki">
      <xsl:value-of select="substring-after(., './')" />
    </xsl:attribute>
  </xsl:template>
  <xsl:template mode="normalize-time-item" match="*">
    <xsl:copy>
      <xsl:apply-templates mode="normalize-time-item" select="node()|@*" />
    </xsl:copy>
  </xsl:template>

  <func:function name="wtime:strip-query">
    <xsl:param name="path" select="." />
    <xsl:choose>
      <xsl:when test="contains($path, '?')">
        <func:result select="substring-before($path, '?')" />
      </xsl:when>
      <xsl:otherwise>
        <func:result select="$path" />
      </xsl:otherwise>
    </xsl:choose>
  </func:function>

  <func:function name="wtime:pad0">
    <xsl:param name="length" />
    <xsl:param name="value" />
    <xsl:variable name="zeros" select="str:padding($length, '0')" />
    <xsl:variable name="text" select="string($value)" />
    <func:result select="str:align($text, $zeros, 'right')" />
  </func:function>

  <!-- Given a en wiki title (in slug form) return the MM-DD, if it's about one -->
  <func:function name="wtime:when-in-year">
    <xsl:param name="wiki" select="." />
    <xsl:variable name="s" select="translate($wiki, '_', ' ')" />
    <xsl:variable name="m" select="substring-before(concat($s, ' '), ' ')" />
    <xsl:variable name="mm">
      <xsl:choose>
        <xsl:when test="$m = 'January'">01</xsl:when>
        <xsl:when test="$m = 'February'">02</xsl:when>
        <xsl:when test="$m = 'March'">03</xsl:when>
        <xsl:when test="$m = 'April'">04</xsl:when>
        <xsl:when test="$m = 'May'">05</xsl:when>
        <xsl:when test="$m = 'June'">06</xsl:when>
        <xsl:when test="$m = 'July'">07</xsl:when>
        <xsl:when test="$m = 'August'">08</xsl:when>
        <xsl:when test="$m = 'September'">09</xsl:when>
        <xsl:when test="$m = 'October'">10</xsl:when>
        <xsl:when test="$m = 'November'">11</xsl:when>
        <xsl:when test="$m = 'December'">12</xsl:when>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="maybe-day" select="substring-after($s, ' ')" />
    <xsl:variable name="day" select="number($maybe-day)" />
    <xsl:if test="$mm !='' and translate($maybe-day, '0123456789', '') = ''">
      <func:result>
        <xsl:value-of select="$mm" />
        <xsl:if test="$day &gt; 0">
          <xsl:value-of select="concat('-', wtime:pad0(2, $day))" />
        </xsl:if>
      </func:result>
    </xsl:if>
  </func:function>

  <!-- Given a en wiki title (in slug form) return an ISO year if it looks like one -->
  <func:function name="wtime:year">
    <xsl:param name="wiki" select="." />
    <xsl:variable name="non-digits" select="translate($wiki, '0123456789', '')" />
    <xsl:if test="$non-digits='' or $non-digits = '_BC' or $non-digits='AD_'">
      <xsl:variable name="year" select="number(translate($wiki, '_BCAD', ''))" />
      <xsl:if test="$year &gt;= 0 and $year &lt; 9999">
        <func:result>
          <xsl:choose>
            <xsl:when test="contains($wiki, '_BC')">
              <xsl:value-of select="concat('-', wtime:pad0(4, $year - 1))" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="wtime:pad0(4, $year)" />
            </xsl:otherwise>
          </xsl:choose>
        </func:result>
      </xsl:if>
    </xsl:if>
  </func:function>

  <!-- The ultimate extractor of English dates from line items -->

  <!-- Forms to capture -->
  <wtime:example>
    <case desc="day + long month name in plain text followed by BC year">
      <li>13 September <a wiki="509_BC">509 BC</a>: The...</li>
      <expect date-ext="09-13" skip=": " /> <!--ummm-->
    </case>
    <case desc="year link followed by colon separator + content">
      <li><a wiki="595">595</a>: Pope Gregory...</li>
      <expect date="0595" skip=": " />
    </case>
    <case desc="same but with BC date">
      <li><a wiki="560_BC">560 BC</a>: <a wiki="Neriglissar">Neriglissar</a> succeeds...</li>
      <expect date="-0560" skip=": " />
    </case>
    <case desc="day-of-year in canonical form followed by dash separator + content">
      <li><a wiki="October_6">October 6</a> <span>–</span> <a wiki="Roman_Republic">Roman Republic</a> troops...</li>
      <expect date-ext="10-06" skip="–" />
    </case>
    <case desc="short month + day followed by hyphen + content">
      <!-- nonsense: this link probably redirects but it is not a canonical form -->
      <li><a wiki="Feb_16">Feb 16</a> - Pope...</li>
      <expect date-ext="02-16" skip=" - " />
    </case>
    <case desc="year range with latter linked followed by hyphen + content">
      <li>600-<a wiki="900">900</a> - <i>Palace</i> and...</li>
      <expect />                <!--shrug-->
    </case>
    <case desc="plain text (BC) year followed by colon + content">
      <li>668 BC: <a wiki="Nineveh">Nineveh</a>, capital of...</li>
    </case>
    <case desc="plain text month followed by delimiter">
      <!-- from 52 BC -->
      <li>March <span>–</span> Siege and capture of...</li>
    </case>

    <!-- apparent typo in 19th century: -->
    <li>1<a wiki="830">830</a>: <a wiki="July_Revolution">July Revolution</a> overthrew...</li>
    
    <!-- NEGATIVES -->
    <li><a wiki="Julius_Caesar">Gaius Julius Caesar</a> is a...</li>
    <li><a wiki="Augustine_of_Canterbury">Augustine of Canterbury</a> converts...</li>
    <li>Marcus Pupius Piso Frugi as...</li>
    <!-- should not be construed as a year, and the nbsp could help here -->
    <li>12<span> </span>million <a wiki="Peso">pesos</a> of silver...</li>
  </wtime:example>

  <!-- Always proceed sequentially -->
  <xsl:template name="read-date">
    <!-- optimization: only matches first nodes anyway -->
    <xsl:apply-templates mode="read-date-start" select="node()" />
  </xsl:template>
  
  <xsl:template mode="read-date-start" match="node()|@*" />

  <!-- first item plain text -->
  <xsl:template mode="read-date-start" match="node()[1][self::text() and .]">
    <xsl:variable name="year" select="wtime:year(.)" />
    <xsl:variable name="within-year" select="wtime:when-in-year(.)" />

    <xsl:choose>
      <xsl:when test="$year and $within-year">
        <xsl:message terminate="yes">
          wait what? <xsl:value-of select="concat($year, ' and ', $within-year)" />
        </xsl:message>
      </xsl:when>
      <xsl:when test="$year">
        <match year="{$year}" from-text="{.}" />
      </xsl:when>
      <xsl:when test="$within-year">
        <match within-year="{$within-year}" from-text="{.}" />
      </xsl:when>
      <xsl:otherwise>
        <nomatch from-text="{.}" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- first item wiki -->
  <xsl:template mode="read-date-start" match="node()[1][self::* and @wiki]">
    <xsl:variable name="year" select="wtime:year(@wiki)" />
    <xsl:variable name="within-year" select="wtime:when-in-year(@wiki)" />
    
    <xsl:choose>
      <xsl:when test="$year and $within-year">
        <xsl:message terminate="yes">
          wait what? <xsl:value-of select="concat($year, ' and ', $within-year)" />
        </xsl:message>
      </xsl:when>
      <xsl:when test="$year">
        <match year="{$year}" from-wiki="{@wiki}" />
      </xsl:when>
      <xsl:when test="$within-year">
        <match within-year="{$within-year}" from-wiki="{@wiki}" />
      </xsl:when>
      <xsl:otherwise>
        <nomatch from-wiki="{@wiki}" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- And finally, a way to skip the delimiters -->
  <xsl:template mode="skip-delimiters" match="*">
    <xsl:variable
      name="first"
      select="following-sibling::node()[translate(normalize-space(), ' -–—:', '')!=''][1]" />
    <xsl:choose>
      <xsl:when test="$first[self::text()]">
        <xsl:variable name="s" select="normalize-space($first)" />
        <xsl:choose>
          <xsl:when test="starts-with($s, ':')">
            <xsl:value-of select="substring-after($first, ':')" />
          </xsl:when>
          <xsl:when test="starts-with($s, '-')">
            <xsl:value-of select="substring-after($first, '-')" />
          </xsl:when>
          <xsl:when test="starts-with($s, '–')">
            <xsl:value-of select="substring-after($first, '–')" />
          </xsl:when>
          <xsl:when test="starts-with($s, '—')">
            <xsl:value-of select="substring-after($first, '—')" />
          </xsl:when>
          <xsl:when test="starts-with($first, ' ')">
            <xsl:value-of select="substring($first, 2)" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:copy-of select="$first" />
          </xsl:otherwise>
        </xsl:choose>
        <xsl:copy-of select="$first/following-sibling::node()" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy-of select="$first | $first/following-sibling::node()" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
</xsl:transform>
